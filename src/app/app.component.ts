/**
 * Main component. Used only as a skeleton for the various applications
 */

import {Component, ElementRef, ViewChild, AfterViewInit} from "@angular/core";
import {LoginService} from "./shared/login/service/login.service";
import {Router, ActivatedRoute} from "@angular/router";
import "hammerjs";
import {SideNavService} from "./shared/sidenav/service/sidenav.service";
import {App, AppsService} from "./apps/apps.service";
import {ConfigurationService} from "./shared/serionet/service/configuration.service";
import {DictionariesService} from "./shared/i18n/dictionaries.service";
import {LoggingService} from "./shared/logging/logging.service";
import {IdleService} from "./shared/idle/idle.service";
import {TranslationService} from "./shared/i18n/translation.service";
import {MessageService} from "./shared/material/message/message.service";
import {DomSanitizer} from "@angular/platform-browser";
import {parseSerionetRespToData} from "./shared/utils";
import {TranslatableComponent} from "./shared/components/base-component/translatable.component";
import {LoadingService} from "./shared/loading/loading.service";

@Component({
  selector: 'app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent extends TranslatableComponent implements AfterViewInit {

  apps: App[];
  server: string;
  appsListOpen = false;

  @ViewChild('myAppList') appListContainer: ElementRef;

  constructor(private loginService: LoginService, private router: Router, private sidenavService: SideNavService,
              private appsService: AppsService, private confServer: ConfigurationService, private dictService: DictionariesService,
              private loggingService: LoggingService, private activatedRoute: ActivatedRoute, private idleService: IdleService,
              private messageService: MessageService, protected translationService: TranslationService, private confService: ConfigurationService,
              private sanitizer:DomSanitizer, private loadingService:LoadingService
  ) {
    super(translationService);
    this.apps = [];
    this.dictService.getDictionary("base").subscribe(() => {
    });
  }

  ngAfterViewInit() {
    let container = this.appListContainer.nativeElement;
    document.querySelector("body").appendChild(container);
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['logLevel']) {
        this.loggingService.level = params['logLevel'];
      }
    });
    this.server = this.confServer.get("SERVER");
    if (!this.loginService.userIsLogged) {
      this.router.navigate(["/login"]);
    }
    else {
      this.reloadApps();
    }
    this.idleService.setRouter(this.router);
  }

  toggleLeftMenu() {
    this.sidenavService.leftMenu = !this.sidenavService.leftMenu;
  }

  openAppList() {
    this.appsListOpen = true;
  }

  closeAppList() {
    this.appsListOpen = false;
  }

  toggleAppList() {
    if (this.appsListOpen)
      this.closeAppList();
    else
      this.openAppList();
  }

  reset() {
    this.apps = [];
  }

  reloadApps() {
    this.loadingService.show();
    this.appsService.getAvailableApps().subscribe((data) => {
      this.loadingService.hide();
      this.apps = parseSerionetRespToData(data);
      if(this.apps.length==0){
        this.messageService.open({
          error: true,
          title: this.tr("ERROR"),
          message: this.tr("ERROR_IN_RETREIVING_APPS")
        })
      }
    });
  }

  goToSerionet() {
    window.open(this.confServer.get("SERVER"));
  }

  getHeaderColor(){
    return this.sanitizer.bypassSecurityTrustStyle("background-color: "+this.confService.getHeaderColor())
  }
}
