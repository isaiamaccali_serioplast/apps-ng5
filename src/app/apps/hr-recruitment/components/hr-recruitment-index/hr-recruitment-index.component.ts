import {Component} from "@angular/core";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Title} from "@angular/platform-browser";
import {HrRecruitmentQueryService} from "../../services/hr-recruitment-query-service";

@Component({
  selector: 'hr-recruitment-index',
  templateUrl: 'hr-recruitment-index.component.html',
  styleUrls: ['hr-recruitment-index.component.css']
})
export class HrRecruitmentIndexComponent {

  selectedIndex = 0;

  constructor(private sideNavService: SideNavService, private titleService: Title,
              private hrRecruitmentQueryService:HrRecruitmentQueryService) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - HR Recruitment");
  }

  ngOnInit(){
    this.hrRecruitmentQueryService.saveQuery({});
  }
}
