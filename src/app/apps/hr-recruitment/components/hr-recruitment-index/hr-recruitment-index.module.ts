import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {HrRecruitmentIndexComponent} from "./hr-recruitment-index.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {HrRecruitmentQueryService} from "../../services/hr-recruitment-query-service";
import {HRRecruitmentTabsModule} from "./tabs/hr-recruitment-tabs.module";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule,HRRecruitmentTabsModule],       // module dependencies
  declarations: [HrRecruitmentIndexComponent],   // components and directives
  exports:[],
  providers: [HrRecruitmentQueryService]                    // services
})
export class HRRecruitmentIndexModule { }


