import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {CompanyPosition, CompanyPositionService} from "../../../../_common/services/company-position.service";
import {ActivatedRoute, Router} from "@angular/router";
import {buildErrorMessage, cloneObject, fixValue, parseSerionetRespToData} from "../../../../../shared/utils";
import {DomSanitizer} from "@angular/platform-browser";
import {ContentTypeService} from "../../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {StatusService} from "../../../../_common/services/status.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {CompanyPositionNameService} from "../../../../_common/services/company-position-name.service";
import {PriorityLink} from "../../../../_common/services/priority-link.service";
import {HRProfile, HRProfileService} from "../../../../_common/services/hr-profiles.service";
import {HcpAdvertisement} from "../../../../_common/services/hcp-advertisement.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {DepartmentService} from "../../../../_common/services/department.service";
import {CurrencyService} from "../../../../_common/services/currency.service";
import {ContractType, ContractTypeService} from "../../../../_common/services/contract-type.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'company-position-detail',
  templateUrl: 'company-position-detail.component.html',
  styleUrls: ['company-position-detail.component.css']
})
export class CompanyPositionDetailComponent extends BaseComponent implements OnInit {

  ready = false;
  editCPOn = false;

  companyPosition: CompanyPosition;
  cpEdit: CompanyPosition;
  cpStatusLinks: StatusLink[] = [];
  hcpStatusLinks: StatusLink[] = [];
  cpAbortedStatusLink: StatusLink = new StatusLink();

  hasCompanyPositions: HasCompanyPosition[] = [];
  hasCompanyPosition: HasCompanyPosition = new HasCompanyPosition();

  hrReferences: HRProfile[] = [];
  currencies: any[] = [];
  contractTypes: ContractType[] = [];

  hcpViewMode = false;

  constructor(protected translationService: TranslationService, protected loadingService: LoadingService,
              private activatedRoute: ActivatedRoute, private companyPositionService: CompanyPositionService,
              private sanitizer: DomSanitizer, private contentTypeService: ContentTypeService,
              private statusLinkService: StatusLinkService, private statusService: StatusService,
              private router: Router, private hasCompanyPositionService: HasCompanyPositionService,
              private companyPositionNameService: CompanyPositionNameService, protected messageService: MessageService,
              private departmentService: DepartmentService, private hrProfileService: HRProfileService,
              private currenciesService: CurrencyService, private contractTypeService: ContractTypeService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    let count = 3;
    let callback = () => {
      count--;
      if (count <= 0)
        count = 0;
      if (count == 0) {
        fixValue(this.cpStatusLinks, this.companyPosition, 'status');
        this.hasCompanyPositions.forEach((hcp) => {
          fixValue(this.hcpStatusLinks, hcp, 'status');
        });
        this.loadingService.hide();
        this.ready = true;
      }
    };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.ready = false;
      this.createOn = false;
      this.editCPOn = false;
      this.companyPosition = new CompanyPosition();
      this.hasCompanyPosition = new HasCompanyPosition();
      this.loadingService.show();
      if (params['cp']) {
        this.companyPositionService.getCompanyPosition(parseInt(params['cp'], 10)).subscribe((cp) => {
          this.companyPosition = cp;
          this.companyPositionNameService.getCompanyPositionName(this.companyPosition.name.pk).subscribe((resp) => {
            this.companyPosition.name = resp;
          });
          this.hasCompanyPositionService.getHasCompanyPositions({company_position: this.companyPosition.pk, is_active: true}).subscribe((data) => {
            this.hasCompanyPositions = parseSerionetRespToData(data);
            if (params['hcp']) {
              this.hasCompanyPosition = this.hasCompanyPositions.filter((hcp) => hcp.pk == parseInt(params['hcp'], 10))[0];
            }

            if (this.cpStatusLinks.length > 0 && this.hcpStatusLinks.length > 0) {
              count = 1;
            }
            callback();
          });
        })
      }
    });

    this.contentTypeService.getContentTypes({model: 'companyposition', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {
        this.statusLinkService.getStatusLinks({is_active: true, app: resp['results'][0].pk}).subscribe((resp) => {
          this.cpStatusLinks = resp['results'].sort((a: StatusLink, b: StatusLink) => {
            return a.delta_rel - b.delta_rel;
          });
          let count = this.cpStatusLinks.length;
          let callback2 = () => {
            count--;
            if (count == 0) {
              this.cpAbortedStatusLink = this.cpStatusLinks.filter((sl) => sl.delta_rel == 4)[0];
              callback();
            }
          };

          this.cpStatusLinks.forEach((sl) => {
            this.statusService.getStatus(sl.status_id.pk).subscribe((s) => {
              sl.status_id = s;
              callback2();
            })
          })
        })
      }
    });

    this.contentTypeService.getContentTypes({model: 'hascompanyposition', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {
        this.statusLinkService.getStatusLinks({is_active: true, app: resp['results'][0].pk}).subscribe((resp) => {
          this.hcpStatusLinks = resp['results'].sort((a: StatusLink, b: StatusLink) => {
            return a.delta_rel - b.delta_rel;
          });
          let count = this.hcpStatusLinks.length;
          let callback2 = () => {
            count--;
            if (count == 0) {
              callback();
            }
          };

          this.hcpStatusLinks.forEach((sl) => {
            this.statusService.getStatus(sl.status_id.pk).subscribe((s) => {
              sl.status_id = s;
              callback2();
            })
          })
        });
      }
    });

    count++;
    this.departmentService.getDeparments({code: 'HR', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.hrProfileService.getHRProfiles({
          is_active: true,
          is_employee: true,
          department: data['results'][0].pk
        }).subscribe((data) => {
          this.hrReferences = parseSerionetRespToData(data);
          callback();
        });
      }
    });

    count++;
    this.currenciesService.getCurrencies({}).subscribe((resp) => {
      this.currencies = resp;
      callback();
    });

    count++;
    this.contractTypeService.getContractTypes({is_active: true}).subscribe((resp) => {
      this.contractTypes = parseSerionetRespToData(resp);
      callback();
    });
  }

  getStatusStyle() {
    let status_id = {
      color: 'white',
      text_color: 'black'
    };
    if (this.companyPosition && this.companyPosition.status && this.companyPosition.status.status_id)
      status_id = cloneObject(this.companyPosition.status.status_id);
    return this.sanitizer.bypassSecurityTrustStyle('padding: 0 10px; margin-left: 10px;text-align:center; font-weight:bold; height:30px; line-height:30px; color: ' + status_id.color + '; font-size:19px')
  }

  getBorderStyle() {
    let status_id = {
      color: 'white',
      text_color: 'black'
    };
    if (this.companyPosition && this.companyPosition.status && this.companyPosition.status.status_id)
      status_id = cloneObject(this.companyPosition.status.status_id);
    return this.sanitizer.bypassSecurityTrustStyle('border-color: ' + status_id.color + ';')
  }

  getStatusStyleHCP(hcp: HasCompanyPosition) {
    let status_id = {
      color: 'white',
      text_color: 'black'
    };
    if (hcp && hcp.status && hcp.status.status_id)
      status_id = cloneObject(hcp.status.status_id);
    return this.sanitizer.bypassSecurityTrustStyle('padding:5px;text-align:center; height:30px; line-height:30px; background-color: ' + status_id.color + '; color: ' + status_id.text_color)
  }

  back() {
    this.router.navigate(["/hr-recruitment", "index"]);
  }

  editHasCompanyPosition(hcp: HasCompanyPosition) {
    this.router.navigate(["hr-recruitment", "company-position-detail"], {queryParams: {cp: this.companyPosition.pk, hcp: hcp.pk}});
  }

  backFromHCP(hcpPK?: number) {
    this.router.navigate(["hr-recruitment", "company-position-detail"], {queryParams: {cp: this.companyPosition.pk, hcp: hcpPK || null, ts: ((new Date()).getTime())}});
  }

  createOn = false;

  createCandidate() {
    let hasCompanyPositionToCreate = new HasCompanyPosition();
    hasCompanyPositionToCreate.status = cloneObject(this.hcpStatusLinks.filter((sl) => sl.delta_rel == 0)[0]);
    hasCompanyPositionToCreate.company_position = cloneObject(this.companyPosition);
    hasCompanyPositionToCreate.pre_interview_rating = new PriorityLink();
    hasCompanyPositionToCreate.rating = new PriorityLink();
    hasCompanyPositionToCreate.user_ref = new HRProfile();
    hasCompanyPositionToCreate.advertisement = new HcpAdvertisement();
    hasCompanyPositionToCreate.profile_updated_truthfull = false;
    this.createOn = true;
    this.hcpViewMode = false;
    this.hasCompanyPosition = hasCompanyPositionToCreate;
  }

  canManageUsers() {
    return this.companyPosition.status && this.companyPosition.status.delta_rel > 0 && this.companyPosition.status.delta_rel != 6;
  }

  requestOpenPosition() {
    this.companyPositionService.advanceUD(this.companyPosition).subscribe((resp) => {
      if (resp.status && resp.status == 200) {
        this.backFromHCP();
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  setShortlist() {
    this.companyPositionService.setShortListStart(this.companyPosition).subscribe((resp) => {
      if (resp.status && resp.status == 200) {
        this.backFromHCP();
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  shouldShowShortlistButton() {
    return this.companyPosition && this.companyPosition.status && this.companyPosition.status.delta_rel == 1 && //e' open
      this.hasCompanyPositions.filter((hcp) => hcp.status.delta_rel == 1).length > 0 && // ha almeno un candidato in shortlist
      this.companyPosition.active_tasks == 'Wait for short list' //non è in stato di approvazione
  }

  canEditHCP(hcp: HasCompanyPosition) {
    return this.companyPosition.active_tasks == 'Wait for short list' && hcp.status.delta_rel == 0 || hcp.status.delta_rel == 1 && this.canAddCandidates();
  }

  canAbortCP() {
    return [4, 5].indexOf(this.companyPosition.status.delta_rel) == -1;
  }

  abortCP() {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_ABORT_CP'),
      autoClose: false,
      confirm: () => {
        let cp: CompanyPosition = cloneObject(this.companyPosition);
        cp.status = cloneObject(this.cpAbortedStatusLink);
        this.companyPositionService.saveCompanyPosition(cp).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.router.navigate(["hr-recruitment", "company-position-detail"], {queryParams: {cp: this.companyPosition.pk}});
          })
        })
      },
      deny: () => {

      }
    });
  }

  canEditCP() {
    return [3, 4, 5, 7].indexOf(this.companyPosition.status.delta_rel) == -1;
  }

  editCP() {
    this.cpEdit = cloneObject(this.companyPosition);
    this.editCPOn = true;
  }

  saveCP() {
    this.companyPositionService.saveCompanyPosition(this.cpEdit).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.router.navigate(["hr-recruitment", "company-position-detail"], {queryParams: {cp: this.companyPosition.pk, ts: ((new Date()).getTime())}});
      })
    })
  }

  undoCP() {
    this.editCPOn = false;
  }

  canAddCandidates() {

    this.hcpViewMode =  (this.companyPosition.active_tasks != 'Wait for short list');

    return (!this.hasCompanyPosition || !this.hasCompanyPosition.pk) && !this.createOn && [1, 6].indexOf(this.companyPosition.status.delta_rel) != -1 &&
      this.companyPosition.active_tasks == 'Wait for short list'
  }
}
