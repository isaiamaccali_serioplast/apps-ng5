import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {HrRecruitmentIndexComponent} from "./hr-recruitment-index.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {CompanyPositionDetailComponent} from "./company-position-detail.component";
import {ModalPersonalInfoModule} from "../../../../../shared/components/modal-personalinfo/modal-personalinfo.module";
import {HrRecruitmentHasCompanyPositionEditModule} from "./has-company-position-edit/has-company-position-edit.module";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule,ModalPersonalInfoModule,HrRecruitmentHasCompanyPositionEditModule],       // module dependencies
  declarations: [CompanyPositionDetailComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class CompanyPositionDetailModule { }


