"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var HrRecruitmentHasCompanyPositionEditComponent = (function () {
    function HrRecruitmentHasCompanyPositionEditComponent(hasCompanyPositionService, activatedRoute, companyPositionService, hrProfileService) {
        this.hasCompanyPositionService = hasCompanyPositionService;
        this.activatedRoute = activatedRoute;
        this.companyPositionService = companyPositionService;
        this.hrProfileService = hrProfileService;
    }
    HrRecruitmentHasCompanyPositionEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            if (params['hcp']) {
                _this.hasCompanyPositionService.getHasCompanyPosition(parseInt(params['hcp'], 10)).subscribe(function (hcp) {
                    _this.hcp = hcp;
                    _this.companyPositionService.getCompanyPosition(_this.hcp.company_position.pk).subscribe(function (cp) {
                        _this.hcp.company_position = cp;
                        _this.hrProfileService.getHrProfile(_this.hcp.user_ref.pk).subscribe(function (hrp) {
                            _this.hcp.user_ref = hrp;
                        });
                    });
                });
            }
        });
    };
    return HrRecruitmentHasCompanyPositionEditComponent;
}());
HrRecruitmentHasCompanyPositionEditComponent = __decorate([
    core_1.Component({
        selector: 'hr-recruitment-index',
        templateUrl: 'src/app/apps/hr-recruitment/components/company-position-detail/has-company-position-edit/hr-recruitment-has-company-position-edit.component.html',
        styleUrls: ['src/app/apps/hr-recruitment/components/company-position-detail/has-company-position-edit/hr-recruitment-has-company-position-edit.component.css']
    })
], HrRecruitmentHasCompanyPositionEditComponent);
exports.HrRecruitmentHasCompanyPositionEditComponent = HrRecruitmentHasCompanyPositionEditComponent;
