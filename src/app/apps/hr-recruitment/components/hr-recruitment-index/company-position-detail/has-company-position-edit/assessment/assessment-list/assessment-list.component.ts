import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {HasCompanyPosition} from "../../../../../../../_common/services/has-company-position.service";
import {TranslationService} from "../../../../../../../../shared/i18n/translation.service";
import {Title} from "@angular/platform-browser";
import {Assessment, AssessmentService} from "../../../../../../services/assessment.service";
import {
  average, cloneObject, fixValue,
  parseSerionetRespToData
} from "../../../../../../../../shared/utils";
import {
  HeaderConf, TableRowAction, TableRowActions
} from "../../../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {SkillAssessment, SkillsAssessmentService} from "../../../../../../services/skills-assessment.service";
import {PriorityLink, PriorityLinkService} from "../../../../../../../_common/services/priority-link.service";
import {StarsPipe} from "../../../../../../../../shared/pipes/stars.pipe";
import {Observable} from "rxjs";
import {ContentType, ContentTypeService} from "../../../../../../../_common/services/content-type.service";
import {DialogComponent} from "../../../../../../../../shared/material/dialog/dialog.component";
import {CompanyPositionNameSkillsService} from "../../../../../../../_common/services/company-position-name-skill.service";
import {MarkdownPipe} from "../../../../../../../../shared/pipes/markdown.pipe";
import {MarkdownService} from "ngx-md";
import {TranslatableComponent} from "../../../../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'assessment-list',
  templateUrl: 'assessment-list.component.html',
  styleUrls: ['assessment-list.component.css']
})
export class HRRecruitmentAssessmentListComponent extends TranslatableComponent implements OnInit {

  @Input('hcp') hcp = new HasCompanyPosition();
  @Input('mode') mode = "edit";
  @Output('createAssessment') createAssessment = new EventEmitter();
  @Output('editAssessment') editAssessment = new EventEmitter();
  @ViewChild('assessmentViewDialog') assessmentViewDialog: DialogComponent;

  assessmentToView: Assessment;

  availableActions = [];
  headerConf: HeaderConf[] = [];
  noItemsMessage = this.tr('NO_ASSESSMENT_AVAILABLE');
  skillRatings: PriorityLink[] = [];

  ready = false;

  constructor(protected translationService: TranslationService, private skillAssessmentService: SkillsAssessmentService,
              private titleService: Title, private assessmentService: AssessmentService,
              private contentTypeService: ContentTypeService, private priorityLinkService: PriorityLinkService,
              private companyPositionNameSkillsService: CompanyPositionNameSkillsService) {
    super(translationService);
    this.titleService.setTitle("Serioplast - HR Recruitment");
  }

  ngOnInit() {

    let actionView: TableRowAction = cloneObject(TableRowActions.VIEW);
    actionView.click = this.view.bind(this);
    this.availableActions = [actionView];

    if (this.mode == 'edit') {
      let actionEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
      actionEdit.click = this.edit.bind(this);
      this.availableActions.push(actionEdit);
    }

    // let mdPipe = new MarkdownPipe(this.markdownService);

    this.headerConf = [
      {
        label: this.tr("DATE"),
        valueKey: 'date'
      },
      {
        label: this.tr("INTERVIEWER"),
        valueKey: 'interviewer',
        useStr: true
      },
      {
        label: this.tr("PARTECIPANTS"),
        valueFunction: (a: Assessment) => {
          return a.participants.map((p) => p.__str__).join("; ")
        }
      },
      {
        label: this.tr("DESCRIPTION"),
        valueFunction: (a: Assessment) => {
          // if (a.description.length < 30)
          //           //   return mdPipe.transform(a.description);
          //           // else
          //           //   return mdPipe.transform(a.description.substring(0, 30) + "...");
          if (a.description.length < 30)
            return a.description;
          else
            return a.description.substring(0, 30) + "...";
        }
      },
      {
        label: this.tr("AVERAGE_SKILL_RATING"),
        valueFunction: (a: Assessment) => {
          let votes = a.skills_assessments.map((sa) => this.skillRatings.filter((skR) => skR.pk == sa.rating.pk)[0].delta_rel);
          if (votes.length > 0) {
            let sp = new StarsPipe();
            return sp.transform(average.apply(null, votes));
          }
          else {
            return "-"; //non dovrebbe succedere mai...
          }
        }
      }
    ];

    this.contentTypeService.getContentTypes({model: 'skillassessment', is_active:true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.priorityLinkService.getPriorityLinks({app: ct.pk, is_active:true}).subscribe((data) => {
          this.skillRatings = parseSerionetRespToData(data);
          this.ready = true;
        });
      }
    });
  }

  getAssessments(page: number, results: number) {
    let query = {
      page: page,
      results: results,
      has_company_position: this.hcp.pk,
      is_active: true,
      ordering: 'date'
    };

    return this.assessmentService.getAssessments(query);
  }

  create() {
    this.createAssessment.emit();
  }

  edit(a: Assessment) {
    this.editAssessment.emit(a);
  }

  expandData(assessments: Assessment[]) {
    return Observable.create((observer) => {
      let expand = (ass, index, callback) => {
        if (ass[index]) {
          let as = ass[index];
          let query = {is_active: true, assessment: as.pk};
          this.skillAssessmentService.getSkillAssessments(query).subscribe((data) => {
            let skillAssessments: SkillAssessment[] = parseSerionetRespToData(data);
            as.has_company_position = this.hcp;
            as.skills_assessments = skillAssessments;
            assessments[index] = as;

            let count = skillAssessments.length;

            if (count > 0) {
              let callback_ = () => {
                count--;
                if (count == 0) {
                  expand(assessments, index + 1, callback);
                }
              };

              skillAssessments.forEach((sa) => {
                this.companyPositionNameSkillsService.getSkills({
                  company_position_name: this.hcp.company_position.name.pk,
                  skill: sa.skill.pk,
                  is_active: true
                }).subscribe((resp) => {
                  let cpnSkills = parseSerionetRespToData(resp);
                  if (cpnSkills.length > 0) {
                    sa.cpnSkill = cpnSkills[0];
                    fixValue(this.skillRatings, sa.cpnSkill, 'level');
                    fixValue(this.skillRatings, sa, 'rating');
                    callback_();
                  }
                });
              })
            }
            else {
              expand(assessments, index + 1, callback);
            }
          });
        }
        else {
          callback();
        }
      };
      expand(assessments, 0, () => {
        observer.next(assessments);
        observer.complete();
      });
    });
  }

  view(a: Assessment) {
    this.assessmentToView = a;
    this.assessmentViewDialog.open();
  }

  getPartecipantsToView(a: Assessment) {
    return a.participants.map((p) => p.__str__).join("; ");
  }
}
