import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ContentType, ContentTypeService} from "../../../../../../../_common/services/content-type.service";
import {PriorityLink, PriorityLinkService} from "../../../../../../../_common/services/priority-link.service";
import {TranslationService} from "../../../../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../../../../shared/loading/loading.service";
import {MessageService} from "../../../../../../../../shared/material/message/message.service";
import {Title} from "@angular/platform-browser";
import {Assessment, AssessmentService} from "../../../../../../services/assessment.service";
import {arrayDiff, arrayIntersect, buildErrorMessage, fixValue, parseSerionetRespToData} from "../../../../../../../../shared/utils";
import {HRProfile, HRProfileService} from "../../../../../../../_common/services/hr-profiles.service";
import {AssessmentModality, AssessmentModalityService} from "../../../../../../services/assessment-modality.service";
import {CompanyPositionNameSkill, CompanyPositionNameSkillsService} from "../../../../../../../_common/services/company-position-name-skill.service";
import {SkillAssessment, SkillsAssessmentService} from "../../../../../../services/skills-assessment.service";
import {DepartmentService} from "../../../../../../../_common/services/department.service";
import {BaseComponent} from "../../../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'assessment-edit',
  templateUrl: 'assessment-edit.component.html',
  styleUrls: ['assessment-edit.component.css']
})
export class HRRecruitmentAssessmentEditComponent extends BaseComponent implements OnInit {

  @Input('assessment') assessment: Assessment = new Assessment();
  @Output('saveEvent') saveEvent = new EventEmitter();
  @Output('undoEvent') undoEvent = new EventEmitter();

  hrpInterviewer: HRProfile[] = [];

  modalities: AssessmentModality[] = [];

  cpnSkills: CompanyPositionNameSkill[] = [];
  cpnSkillsInactive: CompanyPositionNameSkill[] = [];
  skillsAssessments: SkillAssessment[] = [];

  skillRatings: PriorityLink[] = [];

  currencies: any[] = [];

  constructor(private contentTypeService: ContentTypeService,
              private priorityLinkService: PriorityLinkService, protected translationService: TranslationService,
              protected messageService: MessageService, protected loadingService: LoadingService,
              private titleService: Title, private assessmentService: AssessmentService,
              private hrProfileService: HRProfileService, private assessmentModalitiesService: AssessmentModalityService,
              private companyPositionNameSkillService: CompanyPositionNameSkillsService,
              private skillsAssessmentService: SkillsAssessmentService, private departmentService: DepartmentService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Recruitment");
  }

  ngOnInit() {
    this.loadCPNSkills(() => {
      if (this.assessment.pk) {
        this.skillsAssessmentService.getSkillAssessments({
          assessment: this.assessment.pk,
          is_active: true
        }).subscribe((data) => {
          this.skillsAssessments = parseSerionetRespToData(data);
          this.processSkills();
        });
      }
      else {
        this.skillsAssessments = [];
        this.processSkills();
      }
    });

    this.departmentService.getDeparments({code: 'HR'}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.hrProfileService.getHRProfiles({
          is_active: true,
          is_employee: true,
          department: data['results'][0].pk
        }).subscribe((data) => {
          this.hrpInterviewer = parseSerionetRespToData(data);
        });
      }
    });

    this.contentTypeService.getContentTypes({model: 'skillassessment', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.priorityLinkService.getPriorityLinks({app: ct.pk, is_active: true}).subscribe((data) => {
          this.skillRatings = parseSerionetRespToData(data);
        });
      }
    });

    this.assessmentModalitiesService.getAssessmentModalities({is_active: true}).subscribe((data) => {
      this.modalities = parseSerionetRespToData(data);
    })
  }

  loadCPNSkills(callback) {
    this.companyPositionNameSkillService.getSkills({
      company_position_name: this.assessment.has_company_position.company_position.name.pk,
      is_active: true
    }).subscribe((data) => {
      this.cpnSkills = parseSerionetRespToData(data);
      callback.call(this);
    });
  }

  processSkills() {
    let skillsFromCPN = this.cpnSkills.map((cps) => cps.skill.pk);
    let skillsFromSkillAssessment = this.skillsAssessments.map((skass) => skass.skill.pk);

    let intersect = arrayIntersect(skillsFromCPN, skillsFromSkillAssessment);
    intersect.forEach((skillPK) => {
      let skass = this.skillsAssessments.filter((skass) => skass.skill.pk == skillPK)[0];
      skass.cpnSkill = this.cpnSkills.filter((cps) => cps.skill.pk == skillPK)[0];
      this.fixSkillLevel(skass);
      fixValue(this.skillRatings, skass, 'rating');
    });

    let diff = arrayDiff(skillsFromCPN, skillsFromSkillAssessment);
    diff.forEach((skillPK) => {
      let skillAss = new SkillAssessment();
      skillAss.skill = this.cpnSkills.filter((cps) => cps.skill.pk == skillPK).map((cps) => cps.skill)[0];
      skillAss.assessment = this.assessment;
      skillAss.rating = new PriorityLink();
      skillAss.cpnSkill = this.cpnSkills.filter((cps) => cps.skill.pk == skillPK)[0];
      this.fixSkillLevel(skillAss);
      this.skillsAssessments.push(skillAss);
    });

    let diff2 = arrayDiff(skillsFromSkillAssessment, skillsFromCPN);
    if (diff2.length > 0) {
      // ci sono skill legate all'assessment che non sono più attive per la cpn (caso remoto ma possibile)
      this.companyPositionNameSkillService.getSkills({
        company_position_name: this.assessment.has_company_position.company_position.name.pk,
        is_active: false
      }).subscribe((data) => {
        this.cpnSkillsInactive = parseSerionetRespToData(data);
        this.skillsAssessments.forEach((skass, index) => {
          let match = this.cpnSkills.filter((cps) => cps.skill.pk == skass.skill.pk)[0];
          if (!match)
            match = this.cpnSkillsInactive.filter((cps) => cps.skill.pk == skass.skill.pk)[0];
          if (match) {
            skass.cpnSkill = match;
            this.fixSkillLevel(skass);
            fixValue(this.skillRatings, skass, 'rating');
          }
          else {
            this.skillsAssessments.splice(index, 1);
          }
        })
      });
    }
    else {
      this.skillsAssessments.forEach((skass) => {
        skass.cpnSkill = this.cpnSkills.filter((cps) => cps.skill.pk == skass.skill.pk)[0];
        this.fixSkillLevel(skass);
        fixValue(this.skillRatings, skass, 'rating');
      })
    }
  }

  fixSkillLevel(skAss: SkillAssessment) {
    if (skAss.cpnSkill.level) {
      this.priorityLinkService.getPriorityLink(skAss.cpnSkill.level.pk).subscribe((pl) => {
        skAss.cpnSkill.level = pl;
      });
    }
  }

  save() {
    this.assessmentService.saveAssessment(this.assessment).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.assessment.pk = resp.pk;
        if (this.skillsAssessments && this.skillsAssessments.length > 0) {
          this.skillsAssessments.forEach((ska) => ska.assessment = this.assessment);
          this.saveSkillAssessments(0, (resp) => {
            this.manageRespSave(resp, () => {
              this.saveEvent.emit();
            })
          })
        }
        else {
          this.saveEvent.emit();
        }
      }, true)
    })
  }

  saveSkillAssessments(index, callback, lastResponse?: any) {
    if (this.skillsAssessments[index]) {
      this.skillsAssessmentService.saveSkillAssessment(this.skillsAssessments[index]).subscribe((resp) => {
        if (resp.pk) {
          this.saveSkillAssessments(index + 1, callback, resp);
        }
        else {
          if (!resp.context)
            resp.context = "SERIONET";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      });
    }
    else {
      callback.call(this, lastResponse);
    }
  }

  getPartecipants(keyword: string) {
    return this.hrProfileService.getHRProfiles({
      search: keyword,
      results: 5,
      is_active: true,
      is_employee: true
    })
  }

  fixRating(skAss: SkillAssessment) {
    fixValue(this.skillRatings, skAss, 'rating');
  }

  undo() {
    this.undoEvent.emit();
  }

}
