import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../../_common/services/has-company-position.service";
import {CompanyPositionService} from "../../../../../_common/services/company-position.service";
import {HRProfile, HRProfileService, ProfileTag} from "../../../../../_common/services/hr-profiles.service";
import {StatusLink, StatusLinkService} from "../../../../../_common/services/status-link.service";
import {ContentType, ContentTypeService} from "../../../../../_common/services/content-type.service";
import {PriorityLink, PriorityLinkService} from "../../../../../_common/services/priority-link.service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {cloneObject, fixValue, parseSerionetRespToData} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {HrProfileTagService} from "../../../../../_common/services/hr-profile-tag.service";
import {DomSanitizer, Title} from "@angular/platform-browser";
import {TagLink} from "../../../../../_common/services/tag-link.service";
import {HcpAdvertisement, HcpAdvertisementService} from "../../../../../_common/services/hcp-advertisement.service";
import {CurrencyService} from "../../../../../_common/services/currency.service";
import {StatusService} from "../../../../../_common/services/status.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

export class TagFilter {
  tag: ProfileTag;
  active: boolean;
}

@Component({
  selector: 'hr-recruitment-hcp-edit',
  templateUrl: 'has-company-position-edit.component.html',
  styleUrls: ['has-company-position-edit.component.css']
})
export class HrRecruitmentHasCompanyPositionEditComponent extends BaseComponent implements OnInit {

  @Input('hcp') hcp: HasCompanyPosition;
  @Input('viewMode') viewMode = false;
  @Output('backEvent') backEvent = new EventEmitter();
  statuses: StatusLink[] = [];
  statusesAll: StatusLink[] = [];
  skillRating: PriorityLink[] = [];
  tags: TagLink[] = [];
  mode = 'edit';
  hcpRatings: PriorityLink[] = [];

  currencies = [];
  advertisements: HcpAdvertisement[] = [];
  tagFilters: TagFilter[] = [];

  constructor(private hasCompanyPositionService: HasCompanyPositionService, private companyPositionService: CompanyPositionService,
              private hrProfileService: HRProfileService, private statusesService: StatusService,
              private statusLinkService: StatusLinkService, private contentTypeService: ContentTypeService,
              private priorityLinkService: PriorityLinkService, protected translationService: TranslationService,
              private hrProfileTagService: HrProfileTagService, protected messageService: MessageService,
              protected loadingService: LoadingService, private titleService: Title, private sanitizer: DomSanitizer,
              private currenciesService: CurrencyService, private hcpAdvertisementService: HcpAdvertisementService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Recruitment");
  }

  ngOnInit() {

    if ((this.hcp.status.delta_rel == 0 || this.hcp.status.delta_rel == 1) && !this.viewMode) {
      this.mode = 'edit';
    }
    else {
      this.mode = 'view';
    }

    this.contentTypeService.getContentTypes({model: 'hascompanyposition', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, is_active: true}).subscribe((data) => {
          let statusesAll: StatusLink[] = parseSerionetRespToData(data);

          let count = statusesAll.length;
          let callback = () => {
            count--;
            if (count == 0) {
              this.statusesAll = statusesAll;
              this.statuses = data['results'].filter((sl: StatusLink) => sl.delta_rel == 0 || sl.delta_rel == 1);
            }
          };

          statusesAll.forEach((sl) => {
            this.statusesService.getStatus(sl.status_id.pk).subscribe((s) => {
              sl.status_id = s;
              callback();
            })
          })
        });
        this.priorityLinkService.getPriorityLinks({app: ct.pk, fieldname: 'rating', is_active: true}).subscribe((data) => {
          this.hcpRatings = parseSerionetRespToData(data);
        });
      }
    });
    this.contentTypeService.getContentTypes({model: 'skillassessment', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.priorityLinkService.getPriorityLinks({app: ct.pk, fieldname: 'rating', is_active: true}).subscribe((data) => {
          this.skillRating = parseSerionetRespToData(data);
        });
      }
    });

    this.companyPositionService.getCompanyPosition(this.hcp.company_position.pk).subscribe((cp) => {
      this.hcp.company_position = cp;
      if (this.hcp.user_ref.pk) {
        this.hrProfileService.getHrProfile(this.hcp.user_ref.pk).subscribe((hrp) => {
          this.hcp.user_ref = hrp;
          this.hrProfileTagService.getTags({is_active: true}).subscribe((data) => {
            let tags: ProfileTag[] = parseSerionetRespToData(data);
            this.tagFilters = tags.map((tl) => {
              let tf = new TagFilter();
              tf.tag = tl;
              if (this.hcp.user_ref && this.hcp.user_ref.tags)
                tf.active = this.hcp.user_ref.tags.filter((t) => t.pk == tl.pk).length > 0;
              else
                tf.active = false;
              return tf;
            });
          });
        })
      } else {
        this.hrProfileTagService.getTags({is_active: true}).subscribe((data) => {
          let tags: ProfileTag[] = parseSerionetRespToData(data);
          this.tagFilters = tags.map((tl) => {
            let tf = new TagFilter();
            tf.tag = tl;
            tf.active = false;
            return tf;
          });
        });
      }
    });

    this.currenciesService.getCurrencies({}).subscribe((resp) => {
      this.currencies = resp;
    });

    this.hcpAdvertisementService.getHcpAdvertisements({is_active: true}).subscribe((resp) => {
      this.advertisements = parseSerionetRespToData(resp);
    });

    this.hrProfileTagService.getTags({is_active: true}).subscribe((data) => {
      let tags: ProfileTag[] = parseSerionetRespToData(data);
      this.tagFilters = tags.map((tl) => {
        let tf = new TagFilter();
        tf.tag = tl;
        if (this.hcp.user_ref && this.hcp.user_ref.tags)
          tf.active = this.hcp.user_ref.tags.filter((t) => t.pk == tl.pk).length > 0;
        else
          tf.active = false;
        return tf;
      });
    });
  }

  save() {
    this.loadingService.show();

    this.hcp.user_ref.tags = this.tagFilters.filter((tf) => tf.active).map((tf) => tf.tag);

    this.hrProfileService.saveHrProfile(this.hcp.user_ref).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.hasCompanyPositionService.saveHasCompanyPosition(this.hcp).subscribe((resp_) => {
          this.manageRespSave(resp_, () => {
            this.backEvent.emit(this.hcp.pk)
          })
        });
      }, true)
    })
  }

  fixStatusChange() {
    fixValue(this.statuses, this.hcp, 'status');
  }

  getStatusStyleContainer() {
    let status_id = {
      color: 'white',
      text_color: 'black'
    };
    if (this.hcp.status.status_id)
      status_id = cloneObject(this.hcp.status.status_id);

    return this.sanitizer.bypassSecurityTrustStyle('color: ' + status_id.color + ';font-weight:bold');
  }

  goBack() {
    this.backEvent.emit();
  }

  canSaveHCP() {
    return this.hcp.user_ref && this.hcp.user_ref.pk;
  }

  undoEditHCP() {
    this.backEvent.emit();
  }

  userSelected(hrProfile: HRProfile) {
    this.hcp.user_ref = hrProfile;
    this.tagFilters.forEach((tf) => {
      tf.active = this.hcp.user_ref.tags.map((pt) => pt.pk).indexOf(tf.tag.pk) != -1;
    });
  }

  resetUser() {
    this.hcp.user_ref = new HRProfile();
  }

}
