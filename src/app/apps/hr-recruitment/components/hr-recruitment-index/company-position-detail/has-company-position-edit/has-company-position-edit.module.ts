import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HRRecruitmentAssessmentEditComponent} from "./assessment/assessment-edit/assessment-edit.component";
import {HRRecruitmentAssessmentListComponent} from "./assessment/assessment-list/assessment-list.component";
import {HRRecruitmentAssessmentComponent} from "./assessment/assessment.component";
import {HrRecruitmentHasCompanyPositionEditComponent} from "./has-company-position-edit.component";
import {SerioplastCommonModule} from "../../../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [HrRecruitmentHasCompanyPositionEditComponent,HRRecruitmentAssessmentComponent,HRRecruitmentAssessmentEditComponent,HRRecruitmentAssessmentListComponent],   // components and directives
  exports:[HrRecruitmentHasCompanyPositionEditComponent],
  providers: []                    // services
})
export class HrRecruitmentHasCompanyPositionEditModule{ }


