import {Component, Input} from "@angular/core";
import {HasCompanyPosition} from "../../../../../../_common/services/has-company-position.service";
import {Assessment} from "../../../../../services/assessment.service";
import {AssessmentModality} from "../../../../../services/assessment-modality.service";
import {HRProfile} from "../../../../../../_common/services/hr-profiles.service";
import {cloneObject} from "../../../../../../../shared/utils";

@Component({
  selector: 'assessment',
  templateUrl: 'assessment.component.html',
  styleUrls: ['assessment.component.css']
})
export class HRRecruitmentAssessmentComponent {

  @Input('hcp') hcp:HasCompanyPosition;
  @Input('mode') mode = "edit";
  editOn = false;
  assessment:Assessment;

  constructor() {

  }

  editAssessment(a:Assessment){
    this.assessment = cloneObject(a);
    this.editOn = true;
  }

  createAssessment(){
    this.assessment = new Assessment();
    this.assessment.has_company_position = this.hcp;
    this.assessment.modality = new AssessmentModality();
    this.assessment.participants = [];
    this.assessment.interviewer = new HRProfile();
    this.assessment.skills_assessments = [];
    this.editOn = true;
  }

  savedAssessment(){
    this.editOn = false;
  }

  undoEditAssessment(){
    this.editOn = false;
  }

}
