import {Component} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {MatTabGroup} from "@angular/material";
import {SideNavService} from "../../../../../shared/sidenav/service/sidenav.service";
import {HrRecruitmentQueryService} from "../../../services/hr-recruitment-query-service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";


@Component({
  selector: 'hr-recruitment-tabs',
  templateUrl: 'hr-recruitment-tabs.component.html',
  styleUrls: ['hr-recruitment-tabs.component.css']
})
export class HrRecruitmentTabsComponent extends TranslatableComponent{

  selectedIndex = 0;

  constructor(private sideNavService: SideNavService, private titleService: Title,
              protected translationService: TranslationService, private hrRecruitmentQueryService:HrRecruitmentQueryService) {
    super(translationService);
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - HR Recruitment");
  }

  ngOnInit(){
    let query = this.hrRecruitmentQueryService.getQuery();
    if(query){
      if(query['selectedIndex']){
        this.selectedIndex = query['selectedIndex'];
      }
    }
  }

  tabChange(tab:MatTabGroup){
    this.selectedIndex = tab.selectedIndex;
  }
}
