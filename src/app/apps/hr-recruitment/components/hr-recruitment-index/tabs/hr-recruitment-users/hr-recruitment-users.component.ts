import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {ContentType, ContentTypeService} from "../../../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../../../_common/services/status-link.service";
import {HRProfile, HRProfileService, ProfileTag} from "../../../../../_common/services/hr-profiles.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {
  HasCompanyPosition,
  HasCompanyPositionService
} from "../../../../../_common/services/has-company-position.service";
import {Router} from "@angular/router";
import {HrRecruitmentQueryService} from "../../../../services/hr-recruitment-query-service";
import {
  CompanyPositionName,
  CompanyPositionNameService
} from "../../../../../_common/services/company-position-name.service";
import {Site, SitesService} from "../../../../../_common/services/sites.service";
import {PriorityLink, PriorityLinkService} from "../../../../../_common/services/priority-link.service";
import {EducationType, EducationTypesService} from "../../../../../_common/services/education-type.service";
import {
  EducationStudyField,
  EducationStudyFieldService
} from "../../../../../_common/services/education-study-fields.service";
import {PEFunction, PEFunctionService} from "../../../../../_common/services/pefunction.service";
import {PEIndustry, PEIndustryService} from "../../../../../_common/services/peindustry.service";
import {cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {DjangoLanguagesService} from "../../../../../_common/services/django-languages.service";
import {HrProfileTagService} from "../../../../../_common/services/hr-profile-tag.service";
import {TranslatableComponent} from "../../../../../../shared/components/base-component/translatable.component";

export class StatusFilter {
  status: StatusLink;
  active: boolean;
}

export class TagFilter {
  tag: ProfileTag;
  active: boolean;
}

@Component({
  selector: 'hr-recruitment-users',
  templateUrl: 'hr-recruitment-users.component.html',
  styleUrls: ['hr-recruitment-users.component.css']
})

export class HRRecruitmentUsersComponent extends TranslatableComponent implements OnInit {

  statuses: StatusLink[] = [];
  statusesFilters: StatusFilter[] = [];
  tagFilters: TagFilter[] = [];
  profileFilter: string;

  currentPageUsers: number = 1;
  totalPagesUsers: number;
  intervalUsers: string;
  totalUsers: number;
  nResultsUsers = 20;
  hrProfiles: HRProfile[] = [];
  companyPositionNames: CompanyPositionName[] = [];
  sites: Site[] = [];
  languages: any[] = [];
  edTypes: EducationType[] = [];
  edFields: EducationStudyField[] = [];
  peFunctions: PEFunction[] = [];
  peIndustries: PEIndustry[] = [];
  langLevels: PriorityLink[] = [];

  queryFields = {
    employee_job_title: null,
    employee_site: null,
    language: null,
    language_level: null,
    education__degree: null,
    education__study_field: null,
    profexp__company_name: null,
    profexp__function: null,
    profexp__industry: null,
    registeredOnly: false
  };

  constructor(protected translationService: TranslationService, private contentTypeService: ContentTypeService,
              private statusLinkService: StatusLinkService, private loadingService: LoadingService,
              private hrProfileService: HRProfileService, private hasCompanyPositionService: HasCompanyPositionService,
              private router: Router, private hrRecruitmentQueryService: HrRecruitmentQueryService,
              private companyPositionNameService: CompanyPositionNameService, private sitesService: SitesService,
              private languagesService: DjangoLanguagesService, private priorityLinkService: PriorityLinkService,
              private educationTypeService: EducationTypesService, private educationStudyFieldService: EducationStudyFieldService,
              private peFunctionService: PEFunctionService, private peIndustryService: PEIndustryService,
              private hrProfileTagService: HrProfileTagService) {
    super(translationService);
  }

  ngOnInit() {

    let count = 0;
    this.loadingService.show();
    let callback = () => {
      count--;
      if (count == 0) {
        this.loadingService.hide();
        this.restoreQuery();
        this.retrieveDataUsers();
      }
    };

    count++;
    this.contentTypeService.getContentTypes({model: 'hascompanyposition', is_active:true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, is_active:true}).subscribe((data) => {
          this.statuses = parseSerionetRespToData(data);
          let preselectedDeltaRels = [0, 1];
          this.statuses.forEach((s: StatusLink) => {
            let sf = new StatusFilter();
            sf.status = s;
            sf.active = preselectedDeltaRels.indexOf(s.delta_rel) != -1;
            this.statusesFilters.push(sf);
          });
          callback();
        });
      }
    });

    count++;
    this.companyPositionNameService.getCompanyPositionNames({is_active: true}).subscribe((resp) => {
      this.companyPositionNames = parseSerionetRespToData(resp);
      callback();
    });

    count++;
    this.sitesService.getSites({is_active: true}).subscribe((resp) => {
      this.sites = parseSerionetRespToData(resp);
      callback();
    });

    count++;
    this.languagesService.getDjangoLanguages({is_active: true}).subscribe((resp) => {
      this.languages = resp;
      callback();
    });

    count++;
    this.educationTypeService.getEducationTypes({is_active: true}).subscribe((resp) => {
      this.edTypes = parseSerionetRespToData(resp);
      callback();
    });

    count++;
    this.educationStudyFieldService.getEducationStudyfields({is_active: true}).subscribe((resp) => {
      this.edFields = parseSerionetRespToData(resp);
      callback();
    });

    count++;
    this.peFunctionService.getPEFunctions({is_active: true}).subscribe((resp) => {
      this.peFunctions = parseSerionetRespToData(resp);
      callback();
    });

    count++;
    this.peIndustryService.getPEIndustries({is_active: true}).subscribe((resp) => {
      this.peIndustries = parseSerionetRespToData(resp);
      callback();
    });

    count++;
    this.contentTypeService.getContentTypes({model: 'language', is_active:true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        let query = {};
        query['app'] = ct.pk; // model Language
        query['fieldname'] = "level";
        query['is_active'] = true;

        this.priorityLinkService.getPriorityLinks(query).subscribe((resp) => {
          this.langLevels = parseSerionetRespToData(resp);
          callback();
        })
      }
    });

    count++;
    this.hrProfileTagService.getTags({is_active: true}).subscribe((data) => {
      let tags: ProfileTag[] = parseSerionetRespToData(data);
      this.tagFilters = tags.map((tl) => {
        let tf = new TagFilter();
        tf.tag = tl;
        tf.active = false;
        return tf;
      });
      callback();
    });
  }

  disableRegisteredOnly(sf: StatusFilter) {
    if (sf.active)
      this.queryFields.registeredOnly = false;
  }

  // selectAllStatuses() {
  //   this.statusesFilters.forEach((sf) => sf.active = true);
  //   this.queryFields.registeredOnly = false;
  // }

  registeredOnlyChange() {
    if (this.queryFields.registeredOnly) {
      this.statusesFilters.forEach((sf) => sf.active = false);
    }
  }

  restoreQuery() {
    let query = this.hrRecruitmentQueryService.getQuery();
    let queryUsers = {};
    if (query) {
      queryUsers = query['users'];
      if (queryUsers) {
        if (queryUsers['preselectedDeltaRels']) {
          let preselectedDeltaRels = queryUsers['preselectedDeltaRels'];
          this.statusesFilters.forEach((sf) => {
            sf.active = (preselectedDeltaRels.indexOf(sf.status.delta_rel) != -1)
          })
        }
        if (queryUsers['selectedTags']) {
          let selectedTags = queryUsers['selectedTags'];
          this.tagFilters.forEach((sf) => {
            sf.active = (selectedTags.indexOf(sf.tag.pk) != -1)
          })
        }
        if (queryUsers['search'])
          this.profileFilter = queryUsers['search'];
        if (queryUsers['page'])
          this.currentPageUsers = queryUsers['page'];
        if (queryUsers['results'])
          this.nResultsUsers = queryUsers['results'];
        for (let key in this.queryFields) {
          if (queryUsers.hasOwnProperty(key) && this.queryFields.hasOwnProperty(key)) {
            this.queryFields[key] = queryUsers[key];
          }
        }
      }
    }
  }

  retrieveDataUsers() {
    let query = {is_active:true};
    if (this.queryFields.registeredOnly) {
      query['no_hcp'] = 2;
    }
    else {
      query['profiles_by_hcp_status'] = this.statusesFilters.filter((sf) => sf.active).map((sf) => sf.status.pk).join("|");
    }

    query['search'] = this.profileFilter;
    query['page'] = this.currentPageUsers;
    query['results'] = this.nResultsUsers;

    query['tags__equal'] = this.tagFilters.filter((sf) => sf.active).map((sf) => sf.tag.pk).join("|");

    query = Object.assign(query, this.queryFields);

    if (query['language_level'] != null && query['language']) {
      query['language_level'] = [query['language'], query['language_level']].join("|")
    }
    else {
      if (query['language'])
        query['language_level'] = query['language'];
    }
    delete query['language'];
    delete query['registeredOnly'];


    this.hrProfiles = [];


    let queryToSave = cloneObject(this.hrRecruitmentQueryService.getQuery());
    queryToSave.users = {
      preselectedDeltaRels: this.statusesFilters.filter((sf) => sf.active).map((sf) => sf.status.delta_rel),
      search: this.profileFilter,
      page: this.currentPageUsers,
      results: this.nResultsUsers
    };


    queryToSave.users = Object.assign(queryToSave.users, this.queryFields);

    queryToSave.users.selectedTags = this.tagFilters.filter((sf) => sf.active).map((sf) => sf.tag.pk);

    this.hrRecruitmentQueryService.saveQuery(queryToSave);

    this.loadingService.show();
    this.hrProfileService.getHRProfiles(query).subscribe((data) => {
      this.loadingService.hide();
      let count = data['count'];
      this.totalUsers = count;
      if (count > 0) {
        this.hrProfiles = data['results'];

        let page = this.currentPageUsers;
        this.totalPagesUsers = Math.ceil(count / this.nResultsUsers);
        if (this.currentPageUsers != this.totalPagesUsers) {
          this.intervalUsers = (this.nResultsUsers * (page - 1) + 1) + "-" + (this.nResultsUsers * page);
        }
        else {
          this.intervalUsers = (Math.floor(count / this.nResultsUsers) * this.nResultsUsers + 1) + "-" + (count);
        }
        this.hrProfiles.forEach((hrp) => this.retrieveHCPByUser(hrp));
      }
    });
  }

  retrieveHCPByUser(hrp: HRProfile) {
    let query = {
      is_active: true,
      user_ref: hrp.pk
    };
    this.hasCompanyPositionService.getHasCompanyPositions(query).subscribe((data) => {
      hrp['hcp'] = parseSerionetRespToData(data);
    })
  }

  getHasCompanyPositionByUser(hrp: HRProfile): HasCompanyPosition[] {
    return hrp['hcp'] || [];
  }

  reloadUsers() {
    this.currentPageUsers = 1;
    this.retrieveDataUsers();
  }

  goToCompanyPosition(hcp: HasCompanyPosition) {
    let query = this.hrRecruitmentQueryService.getQuery();
    query['selectedIndex'] = 1;
    this.hrRecruitmentQueryService.saveQuery(query);
    this.router.navigate(["hr-recruitment", "company-position-detail"], {
      queryParams: {
        cp: hcp.company_position.pk,
        hcp: hcp.pk
      }
    });
  }

  goToFirstPage() {
    this.currentPageUsers = 1;
    this.retrieveDataUsers();
  }

  goToLastPage() {
    this.currentPageUsers = this.totalPagesUsers;
    this.retrieveDataUsers();
  }

  goToPrevPage() {
    if (this.currentPageUsers != 1) {
      this.currentPageUsers--;
      this.retrieveDataUsers();
    }
  }

  goToNextPage() {
    if (this.currentPageUsers != this.totalPagesUsers) {
      this.currentPageUsers++;
      this.retrieveDataUsers();
    }
  }
}
