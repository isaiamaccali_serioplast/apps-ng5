import {Component, OnInit, ViewChild} from "@angular/core";
import {StepperComponent} from "../../../../../../shared/material/stepper/stepper.component";
import {Organization, OrganizationService} from "../../../../../_common/services/organization.service";
import {Department, DepartmentService} from "../../../../../_common/services/department.service";
import {CompanyPositionName, CompanyPositionNameService} from "../../../../../_common/services/company-position-name.service";
import {Site} from "../../../../../_common/services/sites.service";
import {HRUser, HRUserService} from "../../../../../_common/services/hr-users.service";
import {PersonalInfo, PersonalInfoService} from "../../../../../_common/services/personalinfo.service";
import {Contract, ContractService} from "../../../../../_common/services/contracts.service";
import {LegalEntity, LegalEntityService} from "../../../../../_common/services/legal-entity.service";
import {DjangoLanguage, DjangoLanguagesService} from "../../../../../_common/services/django-languages.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../../_common/services/has-company-position.service";
import {StatusLink, StatusLinkService} from "../../../../../_common/services/status-link.service";
import {Badge, BadgeService} from "../../../../../_common/services/badges.service";
import {BadgeUserRef, BadgeUserRefService} from "../../../../../_common/services/badge-user-refs.service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {CompanyPosition, CompanyPositionService} from "../../../../../_common/services/company-position.service";
import {LoginService} from "../../../../../../shared/login/service/login.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {ContentType, ContentTypeService} from "../../../../../_common/services/content-type.service";
import {arrayAreDifferent, fixValue, getUrlDate, parseSerionetRespToData} from "../../../../../../shared/utils";
import {HRProfile} from "../../../../../_common/services/hr-profiles.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'direct-hire',
  templateUrl: 'direct-hire.component.html',
  styleUrls: ['direct-hire.component.css']
})

export class DirectHireComponent extends BaseComponent implements OnInit {

  @ViewChild('stepper') stepper: StepperComponent;
  organizations: Organization[] = [];
  organization: Organization = new Organization();

  departments: Department[] = [];
  department: Department = new Department();

  companyPositionNames: CompanyPositionName[] = [];
  companyPositionName: CompanyPositionName = new CompanyPositionName();

  sites: Site[] = [];
  site: Site = new Site();
  skipBadge: boolean = false;
  hrUser: HRUser = new HRUser();

  hrPersonalInfo: PersonalInfo = new PersonalInfo();

  contracts: Contract[] = [];
  contract: Contract = new Contract();

  legalEntities: LegalEntity[] = [];
  djangolanguages: DjangoLanguage[] = [];

  finish: boolean = false;
  newUser: boolean = false;
  shouldAddContract: boolean = false;

  previousHCP: HasCompanyPosition[] = [];

  ccStatus: StatusLink = new StatusLink();
  employeeStatus: StatusLink = new StatusLink();

  minContractDate: string = "";

  HR_MANAGER_POSITIONS = [57];
  useFilter = true;
  myHasCompanyPositions: HasCompanyPosition[] = [];
  badges: Badge[];
  badgeUserRef: BadgeUserRef = new BadgeUserRef();
  currentBadges: BadgeUserRef[] = [];

  constructor(private organizationService: OrganizationService, private departmentService: DepartmentService,
              private companyPositionNameService: CompanyPositionNameService, private hrUserService: HRUserService,
              protected translationService: TranslationService, private companyPositionService: CompanyPositionService,
              private hasCompanyPositionService: HasCompanyPositionService, private contentTypeService: ContentTypeService,
              private statusLinkService: StatusLinkService, protected messageService: MessageService,
              private contractService: ContractService, private languagesService: DjangoLanguagesService,
              private legalEntityService: LegalEntityService, protected loadingService: LoadingService,
              private loginService: LoginService, private personalinfoService: PersonalInfoService,
              private badgeService: BadgeService, private badgeuserrefService: BadgeUserRefService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    //get the list of serioplast organization
    this.organizationService.getOrganizations({is_active: true, is_serioplast_group: true}).subscribe((data) => {
      this.organizations = parseSerionetRespToData(data);
    });

    //get the status "close completed" of CP
    this.contentTypeService.getContentTypes({model: 'companyposition', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, delta_rel: 5, is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            this.ccStatus = data['results'][0];
          }
        });
      }
    });
    // get the status "staff" of HCP
    this.contentTypeService.getContentTypes({model: 'hascompanyposition', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, delta_rel: 2, is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            this.employeeStatus = data['results'][0];
          }
        });
      }
    });
    // set the default contract value
    this.contract.start_date = getUrlDate(new Date());
    this.contract.legal_entity = new LegalEntity();
    this.contract.hierarchical_superior = new HRUser();

    // get all the legale entites for contract LE select
    this.legalEntityService.getLegalEntities({is_active: true}).subscribe((data) => {
      this.legalEntities = parseSerionetRespToData(data);
    });

    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk, is_active: true}).subscribe((response) => {
      if (response['count'] && response['count'] > 0) {
        this.myHasCompanyPositions = response['results'];
        this.expandCompanyPositions(this.myHasCompanyPositions, 0, () => {
          let companyPositionNamePKs = this.myHasCompanyPositions.map((hcp) => hcp.company_position.name.pk);
          this.useFilter = !(arrayAreDifferent(companyPositionNamePKs, this.HR_MANAGER_POSITIONS));
        });
      }
    });
    //get the languages for badge
    this.languagesService.getDjangoLanguages({}).subscribe((data) => {
      for (let i = 0; i < data.length; i++) {
        let djangoLanguage = new DjangoLanguage();
        djangoLanguage.value = data[i][0];
        djangoLanguage.label = data[i][1];
        this.djangolanguages[i] = djangoLanguage;
      }
      // Remove empty label
      this.djangolanguages.shift();
    });

    //badgeuserref field
    this.badgeUserRef.badge = new Badge();

    this.badgeUserRef.notes = "";

  }

  expandCompanyPositions(hasCompanyPositions: HasCompanyPosition[], index, callback) {
    if (hasCompanyPositions[index]) {
      this.companyPositionService.getCompanyPosition(hasCompanyPositions[index].company_position.pk).subscribe((resp) => {
        if (resp.pk) {
          hasCompanyPositions[index].company_position = resp;
          this.expandCompanyPositions(hasCompanyPositions, index + 1, callback);
        }
      })
    }
    else {
      callback.call(this);
    }
  }

  getDepartments() {
    this.departmentService.getDeparments({
      is_active: true,
      organization_ref: this.organization.pk
    }).subscribe((data) => {
      this.departments = parseSerionetRespToData(data);
    })
  }

  getCPN() {
    let query = {
      is_active: true,
      department_ref: this.department.pk
    };
    if (this.useFilter)
      query['direct_hire'] = true;
    this.companyPositionNameService.getCompanyPositionNames(query).subscribe((data) => {
      this.companyPositionNames = parseSerionetRespToData(data);
    })
  }

  userSelect(hrp: HRProfile) {
    //once user is selected , load the previus HCP contract and personal info
    if (hrp && hrp.pk) {
      this.hrUser = hrp.user_id;
      this.previousHCP = [];
      this.contracts = [];
      this.badgeUserRef.user = hrp.user_id;
      this.loadCurrentBadges();
      this.loadHasCompanyPositions();
      this.loadContracts();
      this.loadPersonalInfo();
    }
    else {
      this.hrUser = new HRUser();
    }
  }

  loadCurrentBadges() {
    // get all the current badges
    this.badgeuserrefService.getBadgeUserRefs({is_active: true, user: this.hrUser.pk}).subscribe((data) => {
      this.currentBadges = parseSerionetRespToData(data);
    });
  }

  loadHasCompanyPositions() {
    //caricare CP e HCP dell'utente selezionato a fini informativi
    if (this.hrUser.pk) {
      this.hasCompanyPositionService.getHasCompanyPositions({
        user_ref: this.hrUser.pk,
        is_active: true
      }).subscribe((data) => {
        this.previousHCP = parseSerionetRespToData(data);
      });
    }
  }

  canSave() {
    //return this.site.pk && this.companyPositionName.pk && this.hrUser.username &&
    //(this.shouldAddContract ? (this.contract.start_date && this.contract.hierarchical_superior.pk) : true) &&
    //this.hrPersonalInfo.first_name && this.hrPersonalInfo.last_name && this.finish;
    return this.finish;
  }

  //FINAL SAVE FUNCTION
  saveAll() {
    // if new user & new employee (no previous hcp)
    if (this.previousHCP.length == 0) {
      this.hrUserService.saveHrUser(this.hrUser).subscribe((resp) => {
        this.manageRespSave(resp, () => {

          for (let key in this.hrPersonalInfo) {
            if (this.hrPersonalInfo[key] == "None") {
              this.hrPersonalInfo[key] = null;
            }
          }

          //SET THE NEW PK WHEN IT?S A NEW USER
          if (!this.hrPersonalInfo.pk) { // if is a new user copy the pk of just created user
            this.hrPersonalInfo.user_id = resp['pk'];
            this.hrPersonalInfo.pk = resp['pk'];
          }
          if (!this.hrUser.pk) { // if is a new user copy the pk of just created user
            this.hrUser.pk = resp['pk'];
          }

          //###################################
          this.personalinfoService.updatePersonalInfo(this.hrPersonalInfo).subscribe((resp) => {
            this.manageRespSave(resp, () => {
              this.saveForHcp();
            }, true)
          })
        }, true)
      })
    } else {
      this.saveForHcp();
    }
  }

  saveForHcp() {
    let cp = new CompanyPosition();
    cp.status = this.ccStatus;
    cp.site_ref = this.site;
    cp.name = this.companyPositionName;

    this.companyPositionService.saveCompanyPosition(cp).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        let hcp = new HasCompanyPosition();
        hcp.company_position = resp;
        hcp.status = this.employeeStatus;
        hcp.user_ref.pk = this.hrUser.pk;
        this.hasCompanyPositionService.saveHasCompanyPosition(hcp).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            if (this.shouldAddContract || this.newUser) {
              this.contract.user = this.hrUser;
              this.contractService.saveContract(this.contract).subscribe((resp) => {
                this.manageRespSave(resp, () => {
                  this.handleSaveBadge();
                });
              })
            }
            else {
              //salva badge
              this.handleSaveBadge()
            }

          }, this.shouldAddContract);
        })
      }, true)
    })
  }

  handleSaveBadge() {
    if (!this.skipBadge) {
      this.badgeUserRef.user = this.hrUser;
      this.badgeuserrefService.saveBadgeUserRef(this.badgeUserRef).subscribe((resp) => {
        this.manageRespSave(resp, () => {
          this.resetInterface();
        });
      })
    }
    else {
      this.resetInterface();
    }
  }

  ///////////////////

  resetInterface() {
    this.organization = new Organization();
    this.department = new Department();
    this.companyPositionName = new CompanyPositionName();
    this.hrUser = new HRUser();
    //this.newUser = false;
    this.site = new Site();
    this.contract = new Contract();
    this.contract.start_date = getUrlDate(new Date());
    this.contract.legal_entity = new LegalEntity();
    this.contract.hierarchical_superior = new HRUser();
    this.hrPersonalInfo = new PersonalInfo();
    this.currentBadges = [];

    //reset the previus contract and HCP
    this.previousHCP = [];
    this.contracts = [];
    this.stepper.setIndex(0);
    this.skipBadge = false;
  }

  hierSuperSelect(u: HRProfile) {
    if (u && u.pk) {
      this.contract.hierarchical_superior = u.user_id;
    }
    else {
      this.contract.hierarchical_superior = new HRUser();
    }
  }

  getHiersSupers(keyword: string) {
    return this.hrUserService.getHrUsers({search: keyword, is_active: true, results: 5});
  }

  loadContracts() {
    if (this.hrUser.pk) {
      this.contractService.getContracts({user: this.hrUser.pk, is_active: true}).subscribe((data) => {
        this.contracts = parseSerionetRespToData(data);
        if (this.contracts.length > 0) {
          this.shouldAddContract = this.checkPreviousContracts();
          if (!this.shouldAddContract)
            this.contract = this.contracts[0];
        }
        else {
          this.shouldAddContract = true;
          this.minContractDate = getUrlDate(new Date());
        }
      });

      //set badge date
      this.badgeUserRef.start_date = getUrlDate(new Date());

    }
  }

  loadPersonalInfo() {
    if (this.hrUser.pk) {
      this.personalinfoService.getPersonalInfo(this.hrUser.pk).subscribe((data) => {
        this.hrPersonalInfo = data;
      });
    }
  }

  //controlla che non ci siano contratti aperti, nel caso verifica se c'è spazio per crearne di nuovi, settando limiti alle date
  checkPreviousContracts() {
    //se ne ho uno aperto,non permetto la creazione
    if (this.contracts.length > 0) {
      let openContracts = this.contracts.filter((c) => c.end_date == null);
      if (openContracts.length > 0) {
        return false;
      }
      else {
        //non ho contratti aperti, setto la data minima sceglibile per il contratto alla end-date più recente
        let end_dates = this.contracts.filter((c) => c.end_date != null).map((c) => parseInt(getUrlDate(new Date(c.end_date), ""), 10));
        let max_date = Math.max.apply(Math, end_dates) + "";
        let d = new Date();
        d.setFullYear(parseInt(max_date.substring(0, 4), 10), parseInt(max_date.substring(4, 6), 10) - 1, parseInt(max_date.substring(6, 8), 10));
        d.setDate(d.getDate() + 1);
        this.minContractDate = getUrlDate(d);
        return true;
      }
    }
    else {
      return true;
    }
  }

  fixBadge() {
    fixValue(this.badges, this.badgeUserRef, 'badge');
  }

  loadBadges(s: Site) {
    if (s && s.pk) {
      let query = {
        location: s.pk,
        is_active: true,
        is_free: 2,
        guest: false
      };
      this.badgeService.getBadges(query).subscribe((data) => {
        this.badges = parseSerionetRespToData(data);
      });
    }
    else {
      this.badges = [];
    }

  }

  step1ProceedCondition() {
    return this.hrUser.username;
  }

  step2ProceedCondition() {
    return true;
  }

  //step2Previous() {
  //  this.hrUser = new HRUser();
  //  this.newUser = false;
  //}

  //step3Previous() {
  //  if(!this.hrUser.pk)
  //   this.hrPersonalInfo = new PersonalInfo();
  //}

  step3ProceedCondition() {
    return this.site.pk && this.organization.pk && this.companyPositionName.pk;
  }

  step4ProceedCondition() {
    return !((this.shouldAddContract || this.newUser ) && (!this.contract.hierarchical_superior.pk || !this.contract.legal_entity.pk || this.contract.start_date == ""));
  }

  //step4Previous(){
  //  this.site = new Site();
  //  this.companyPositionName = new CompanyPositionName();
  //  this.department = new Department();
  //  this.organization = new Organization();
  //}

  fixSte3Selects() {
    fixValue(this.sites, this, "site");
    fixValue(this.organizations, this, "organization");
    fixValue(this.companyPositionNames, this, "companyPositionName");
  }

  fixSte4Selects() {

    fixValue(this.legalEntities, this.contract, "legal_entity");

  }

  step5ProceedCondition() {
    if (!this.skipBadge) {
      return this.badgeUserRef.badge.pk && this.badgeUserRef.language && this.badgeUserRef.start_date;
    }
    else {
      return true;
    }
  }

  step5ProceedConfirm() {
    this.finish = true;
  }

  newUserChange() {
    this.resetInterface();
    //this.hrUser = new HRUser();
    //this.hrUsers = [];
  }

  selectSite(s: Site) {
    this.site = s;
    this.loadBadges(s)
  }

  backLastStep() {
    this.stepper.setIndex(4);
  }

}
