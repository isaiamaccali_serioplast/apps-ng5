import {NgModule} from "@angular/core";
import {DirectHireComponent} from "./direct-hire/direct-hire.component";
import {HRRecruitmentPositionsComponent} from "./hr-recruitment-positions/hr-recruitment-positions.component";
import {HRRecruitmentUsersComponent} from "./hr-recruitment-users/hr-recruitment-users.component";
import {HrRecruitmentQueryService} from "../../services/hr-recruitment-query-service";
import {HrRecruitmentTabsComponent} from "./hr-recruitment-tabs.component";
import {ModalPersonalInfoModule} from "../../../../../shared/components/modal-personalinfo/modal-personalinfo.module";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule,ModalPersonalInfoModule],       // module dependencies
  declarations: [HrRecruitmentTabsComponent,DirectHireComponent,HRRecruitmentUsersComponent,HRRecruitmentPositionsComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class HRRecruitmentTabsModule { }


