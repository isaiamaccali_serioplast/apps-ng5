import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {ContentType, ContentTypeService} from "../../../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../../../_common/services/status-link.service";
import {CompanyPositionName, CompanyPositionNameService} from "../../../../../_common/services/company-position-name.service";
import {Organization, OrganizationService} from "../../../../../_common/services/organization.service";
import {Department, DepartmentService} from "../../../../../_common/services/department.service";
import {LoginService} from "../../../../../../shared/login/service/login.service";
import {CompanyPosition, CompanyPositionService} from "../../../../../_common/services/company-position.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../../_common/services/has-company-position.service";
import {Site} from "../../../../../_common/services/sites.service";
import {StatusFilter} from "../hr-recruitment-users/hr-recruitment-users.component";
import {cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {Router} from "@angular/router";
import {TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Observable} from "rxjs";
import {HrRecruitmentQueryService} from "../../../../services/hr-recruitment-query-service";
import {TranslatableComponent} from "../../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'hr-recruitment-positions',
  templateUrl: 'hr-recruitment-positions.component.html',
  styleUrls: ['hr-recruitment-positions.component.css']
})

export class HRRecruitmentPositionsComponent extends TranslatableComponent implements OnInit {

  @ViewChild('table') table: TableWithPagesComponent;
  statusesFiltersCP: StatusFilter[] = [];

  ready = false;

  companyPositionNames: CompanyPositionName[] = [];
  companyPositionNameFilter: number;

  organizations: Organization[] = [];
  organization: Organization = new Organization();

  departments: Department[] = [];
  department: Department = new Department();

  siteFilter: number;
  siteRef: Site;

  candidateStatusHCP: StatusLink;
  shortlistedStatusHCP: StatusLink;

  noItemsMessage = "No positions for selected query";

  headerConf = [
    {
      label: this.tr("POSITION"),
      valueKey: '__str__'
    },
    {
      label: this.tr("STATUS"),
      valueKey: 'status',
      useStr: true
    },
    {
      label: this.tr("CANDIDATES"),
      valueFunction: (item: CompanyPosition) => {
        return item['cps_cand'].length + "";
      }
    },
    {
      label: this.tr("SHORTLISTED"),
      valueFunction: (item: CompanyPosition) => {
        return item['cps_slist'].length + "";
      }
    }
  ];

  siteReady = false;
  queryReady = false;

  constructor(protected translationService: TranslationService, private contentTypeService: ContentTypeService,
              private statusLinkService: StatusLinkService, private loginService: LoginService,
              private companyPositionService: CompanyPositionService,
              private hasCompanyPositionService: HasCompanyPositionService, private departmentsService: DepartmentService,
              private companyPositionNameService: CompanyPositionNameService, private organizationService: OrganizationService,
              private router: Router, private hrRecruitmentQueryService: HrRecruitmentQueryService) {
    super(translationService);
  }

  ngOnInit() {
    let query = this.hrRecruitmentQueryService.getQuery();
    let preselectedDeltaRels = [1, 6];
    let queryPositions = {};

    let count = 1;
    let callback = () => {
      count--;
      if (count == 0)
        this.queryReady = true;

      if (this.queryReady && this.siteReady) {
        this.ready = true;
      }

    };

    if (query) {
      queryPositions = query['positions'];
      if (queryPositions) {
        count++;
        if (queryPositions['site_ref']) {
          this.siteSelected(queryPositions['site_ref']);
        }

        if (queryPositions['preselectedDeltaRels']) {
          preselectedDeltaRels = queryPositions['preselectedDeltaRels'];
        }

        if (queryPositions['name']) {
          this.companyPositionNameFilter = queryPositions['name'];
          this.companyPositionNameService.getCompanyPositionName(this.companyPositionNameFilter).subscribe((cpn: CompanyPositionName) => {
            this.department = cpn.department_ref;
            this.departmentsService.getDepartment(this.department.pk).subscribe((d: Department) => {
              this.organization = d.organization_ref;
              this.getDepartments(() => {
                this.getCPN(() => {
                  callback()
                });

              });
            })
          })
        }
        else {
          callback();
        }
      }
    }
    this.contentTypeService.getContentTypes({model: 'companyposition', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            data['results'].forEach((s: StatusLink) => {
              let sf = new StatusFilter();
              sf.status = s;
              sf.active = preselectedDeltaRels.indexOf(s.delta_rel) != -1;
              this.statusesFiltersCP.push(sf);
              callback();
            });
          }
        });
      }
    });

    this.contentTypeService.getContentTypes({model: 'hascompanyposition', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, status_id__label: 'Candidate', is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            this.candidateStatusHCP = data['results'][0];
          }
        });
        this.statusLinkService.getStatusLinks({app: ct.pk, status_id__label: 'Shortlisted', is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            this.shortlistedStatusHCP = data['results'][0];
          }
        });
      }
    });

    //get the list of serioplast organization
    this.organizationService.getOrganizations({is_active: true, is_serioplast_group: true}).subscribe((data) => {
      this.organizations = parseSerionetRespToData(data);
    });
  }

  loadCompanyPositions(page: number, results: number) {
    let query_ = {
      status_list: this.statusesFiltersCP.filter((sf) => sf.active).map((sf) => sf.status.pk).join("|"),
      page: page,
      results: results,
      by_hr_sites: this.loginService.user.pk,
      is_active: true
    };

    if (this.companyPositionNameFilter)
      query_['name'] = this.companyPositionNameFilter;

    if (this.siteFilter) {
      query_['site_ref'] = this.siteFilter;
    }

    let queryToSave = cloneObject(this.hrRecruitmentQueryService.getQuery());
    queryToSave.positions = {
      name: this.companyPositionNameFilter || null,
      preselectedDeltaRels: this.statusesFiltersCP.filter((sf) => sf.active).map((sf) => sf.status.delta_rel),
      site_ref: this.siteRef
    };
    this.hrRecruitmentQueryService.saveQuery(queryToSave);

    return this.companyPositionService.getCompanyPositions(query_);
  }

  siteSelected(s: Site) {
    if (s && s.pk) {
      this.siteFilter = s.pk;
      this.siteRef = s;
      this.siteReady = true;
      if (this.queryReady && this.siteReady) {
        this.ready = true;
      }
    }
    else {
      this.siteFilter = null;
      this.siteRef = null;
    }
  }

  reload() {
    this.table.reload();
  }

  getDepartments(callback?: any) {
    this.departmentsService.getDeparments({
      is_active: true,
      organization_ref: this.organization.pk
    }).subscribe((data) => {
      this.departments = parseSerionetRespToData(data);
      if (callback)
        callback.call(this);
    })
  }

  getCPN(callback?: any) {
    this.companyPositionNameService.getCompanyPositionNames({
      is_active: true,
      department_ref: this.department.pk
    }).subscribe((data) => {
      this.companyPositionNames = parseSerionetRespToData(data);
      if (callback)
        callback.call(this);
    })
  }

  goToCompanyPosition(cp: CompanyPosition) {
    let query = this.hrRecruitmentQueryService.getQuery();
    query['selectedIndex'] = 0;
    this.hrRecruitmentQueryService.saveQuery(query);
    this.router.navigate(["hr-recruitment", "company-position-detail"], {queryParams: {cp: cp.pk}});
  }

  expandDataPosition(positions: CompanyPosition) {
    return Observable.create((observer) => {

      let expandPosition = (cps, index, callback) => {
        if (cps[index]) {
          let cp = cps[index];
          let query = {
            is_active: true,
            company_position: cp.pk
          };
          this.hasCompanyPositionService.getHasCompanyPositions(query).subscribe((data) => {
            cp['hcp'] = parseSerionetRespToData(data);
            cp['cps_cand'] = cp['hcp'].filter((hcp: HasCompanyPosition) => hcp.status.pk == this.candidateStatusHCP.pk);
            cp['cps_slist'] = cp['hcp'].filter((hcp: HasCompanyPosition) => hcp.status.pk == this.shortlistedStatusHCP.pk);
            positions[index] = cp;
            expandPosition(cps, index + 1, callback);
          });
        }
        else {
          callback();
        }
      };
      expandPosition(positions, 0, () => {
        observer.next(positions);
        observer.complete();
      })
    });
  }
}
