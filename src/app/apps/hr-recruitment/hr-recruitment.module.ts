import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {HRRecruitmentRoutes} from "./hr-recruitment.routes";
import {HRRecruitmentIndexModule} from "./components/hr-recruitment-index/hr-recruitment-index.module";
import {AssessmentService} from "./services/assessment.service";
import {AssessmentModalityService} from "./services/assessment-modality.service";
import {SkillsAssessmentService} from "./services/skills-assessment.service";
import {CompanyPositionDetailModule} from "./components/hr-recruitment-index/company-position-detail/company-position-detail.module";
import {HrRecruitmentQueryService} from "./services/hr-recruitment-query-service";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(HRRecruitmentRoutes), HRRecruitmentIndexModule,CompanyPositionDetailModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [AssessmentService,AssessmentModalityService,SkillsAssessmentService,HrRecruitmentQueryService]                    // services
})
export class HRRecruitmentModule { }

