"use strict";
var auth_guard_1 = require("../../auth-guard");
var hr_recruitment_index_component_1 = require("./components/hr-recruitment-index/hr-recruitment-index.component");
var hr_recruitment_has_company_position_edit_component_1 = require("./components/company-position-detail/has-company-position-edit/hr-recruitment-has-company-position-edit.component");
var hr_recruitment_company_position_view_component_1 = require("./components/hr-recruitment-company-position-view/hr-recruitment-company-position-view.component");
exports.HRRecruitmentRoutesRoot = [
    {
        path: 'hr-recruitment',
        loadChildren: './hr-recruitment.module.ts#HRRecruitmentModule',
        canActivate: [auth_guard_1.AuthGuard]
    }
];
// Route Configuration
exports.HRRecruitmentRoutes = [
    {
        path: '',
        component: hr_recruitment_index_component_1.HrRecruitmentIndexComponent,
        canActivate: [auth_guard_1.AuthGuard]
    },
    {
        path: 'companyposition/view/:cp',
        pathMatch: 'full',
        component: hr_recruitment_company_position_view_component_1.HrRecruitmentCompanyPositionViewComponent,
        canActivate: [auth_guard_1.AuthGuard],
    },
    {
        path: 'hascompanyposition/edit/:hcp',
        pathMatch: 'full',
        component: hr_recruitment_has_company_position_edit_component_1.HrRecruitmentHasCompanyPositionEditComponent,
        canActivate: [auth_guard_1.AuthGuard],
    }
];
