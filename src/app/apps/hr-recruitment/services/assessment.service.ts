import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";
import {HRProfile} from "../../_common/services/hr-profiles.service";
import {HasCompanyPosition} from "../../_common/services/has-company-position.service";
import {SkillAssessment} from "./skills-assessment.service";
import {AssessmentModality} from "./assessment-modality.service";

export class Assessment extends GenericObj{
  date?:string;
  interviewer?:HRProfile;
  participants?:HRProfile[];
  modality?:AssessmentModality;
  description?:string;
  has_company_position?:HasCompanyPosition;
  skills_assessments:SkillAssessment[]
}

@Injectable()
export class AssessmentService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Assessment";
    this.serviceURL = "hrpositions/assessments/"
  }

  getAssessments(query: any) {
    return this.getList(query);
  }

  getAssessment(id: number) {
    return this.getItem(id);
  }

  saveAssessment(a: Assessment) {
    return this.saveItem(a);
  }

  deleteAssessment(a: Assessment) {
    return this.deleteItem(a);
  }
}



