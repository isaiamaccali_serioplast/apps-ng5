import {Injectable} from "@angular/core";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";
import {CompanyPositionNameSkill} from "../../_common/services/company-position-name-skill.service";
import {Skill} from "../../_common/services/skill.service";
import {PriorityLink} from "../../_common/services/priority-link.service";
import {Assessment} from "./assessment.service";

export class SkillAssessment extends GenericObj{
  skill:Skill;
  assessment:Assessment;
  rating?: PriorityLink;

  //custom
  cpnSkill:CompanyPositionNameSkill
}

@Injectable()
export class SkillsAssessmentService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Skill Assessment";
    this.serviceURL = "hrpositions/skillassessments/"
  }

  getSkillAssessments(query?: any) {
    return this.getList(query);
  }

  getSkillAssessment(id: number) {
    return this.getItem(id);
  }

  saveSkillAssessment(skAss: SkillAssessment) {
    return this.saveItem(skAss);
  }
}



