import {Injectable} from "@angular/core";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";

export class AssessmentModality extends GenericObj{}

@Injectable()
export class AssessmentModalityService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Assessment Modality";
    this.serviceURL = "hrpositions/assessmentsmodality/"
  }

  getAssessmentModalities(query?: any) {
    return this.getList(query);
  }


}



