import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {HrRecruitmentIndexComponent} from "./components/hr-recruitment-index/hr-recruitment-index.component";
import {CompanyPositionDetailComponent} from "./components/hr-recruitment-index/company-position-detail/company-position-detail.component";
import {HrRecruitmentTabsComponent} from "./components/hr-recruitment-index/tabs/hr-recruitment-tabs.component";

// Route Configuration
export const HRRecruitmentRoutes: Routes = [
  {
    path: '',
    component: HrRecruitmentIndexComponent,
    canActivate:[AuthGuard],
    children:[
      {
        path: '',
        redirectTo:'index'
      },
      {
        path: 'index',
        pathMatch:'full',
        component: HrRecruitmentTabsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'company-position-detail',
        pathMatch:'full',
        component: CompanyPositionDetailComponent,
        canActivate: [AuthGuard],
      }
    ]
  },

];


