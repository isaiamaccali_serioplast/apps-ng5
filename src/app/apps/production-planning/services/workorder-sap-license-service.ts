import {Injectable} from "@angular/core";
import {LoginService} from "../../../shared/login/service/login.service";
import {ConfigurationService} from "../../../shared/serionet/service/configuration.service";
import {MatSnackBar} from "@angular/material";
import {AppPlantUsersService} from 'app/apps/_common/services/app-plant-users.service';
import {LoadingService} from "../../../shared/loading/loading.service";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable()
export class WorkorderSapLicenseService {

  ready = false;

  constructor(private loginService:LoginService, private confService:ConfigurationService,
              private appPlantUsersService:AppPlantUsersService, private snackBar:MatSnackBar,
              private router:Router, private loadingService:LoadingService) {
  }

  reset(){
    this.ready = false;
  }

  isLicenseReady(){
    return Observable.create((observer)=>{
      if(this.ready){
        observer.next(true);
        observer.complete()
      }
      else {
        if(this.loginService.user.sap_license){
          this.confService.set("SAP_USER",this.loginService.user.sap_license.user,true);
          this.confService.set("SAP_PASSWORD",this.loginService.user.sap_license.password,true);
          observer.next(true);
          this.ready = true;
          observer.complete();
        }
        else {
          let query = {
            app__app_root: 'production-planning',
            is_active:true
          };
          this.loadingService.show();
          this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
            this.loadingService.hide();
            if (response['results'] && response['results'].length == 1) {
              this.confService.set("SAP_USER",response['results'][0].user,true);
              this.confService.set("SAP_PASSWORD",response['results'][0].password,true);
              observer.next(true);
              this.ready = true;
              observer.complete();
              // this.router.navigate(["materials","materials"]);
            }else{
              this.router.navigate(["/index"]);
              this.snackBar.open("NO_SAP_LICENCE_FOR_APP_PRODUCTION_PLANNING", null, {duration: 5000, panelClass: ['error']});
            }
          });
        }
      }
    })
  }
}
