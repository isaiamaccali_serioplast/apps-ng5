import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {ProductionPlanningIndexComponent} from "./components/production-planning-index/production-planning-index.component";
import {ProductionPlanningCalendarViewComponent} from "./components/production-planning-index/production-planning-calendar-view/production-planning-calendar-view.component";
import {WorkorderEditComponent} from "./components/production-planning-index/workorder-edit/workorder-edit.component";
import {WorkorderViewComponent} from "./components/production-planning-index/workorder-view/workorder-view.component";

export const ProductionPlanningRoutesRoot: Routes = [
  {
    path: 'production-planning',
    loadChildren: './production-planning.module.ts#ProductionPlanningModule',
    canActivate: [AuthGuard]
  }
];

// Route Configuration
export const ProductionPlanningRoutes: Routes = [
  {
    path: '',
    component: ProductionPlanningIndexComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path:"",
        pathMatch:'full',
        redirectTo:'calendar/view'
      },
      {
        path:'calendar/view',
        component:ProductionPlanningCalendarViewComponent
      },
      {
        path:'workorder/create',
        component:WorkorderEditComponent
      },
      {
        path:'workorder/edit/:id',
        component:WorkorderEditComponent
      },
      {
        path:'workorder/view/:id',
        component:WorkorderViewComponent
      }
    ]
  }
];
