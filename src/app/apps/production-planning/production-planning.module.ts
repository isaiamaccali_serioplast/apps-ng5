import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {ProductionPlanningRoutes} from "./production-planning.routes";
import {ProductionPlanningIndexModule} from "./components/production-planning-index/production-planning-index.module";
import {ProductionPlanningCalendarViewModule} from "./components/production-planning-index/production-planning-calendar-view/production-planning-calendar-view.module";
import {WorkorderCalendarQueryService} from "./services/workorder-calendar-query-service";
import {WorkorderSapLicenseService} from "./services/workorder-sap-license-service";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(ProductionPlanningRoutes), ProductionPlanningIndexModule, ProductionPlanningCalendarViewModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [WorkorderCalendarQueryService,WorkorderSapLicenseService]                    // services
})
export class ProductionPlanningModule { }

