import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {WorkorderViewComponent} from "./workorder-view.component";
import {WorkorderViewSharedModule} from "../../../../../shared/components/workorder-view-shared/workorder-view-shared.module";


@NgModule({
  imports: [SharedModule, WorkorderViewSharedModule],       // module dependencies
  declarations: [WorkorderViewComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class WorkorderViewModule { }


