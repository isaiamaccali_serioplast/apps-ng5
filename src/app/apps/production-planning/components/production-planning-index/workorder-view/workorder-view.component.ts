import {Component, ViewChild} from "@angular/core";
import {WorkorderSerionet, WorkordersSerionetService} from "../../../../_common/services/workorders-serionet.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Site} from "../../../../_common/services/sites.service";
import {Routing} from "../../../../_common/services/routings.service";
import {TypeLink} from "../../../../_common/services/type-link.service";
import {PackingInstruction} from "../../../../_common/services/packing-instructions.service";
import {RoutingProductionVersion} from "../../../../_common/services/routing-production-version-service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {ControlPlan} from "../../../../_common/services/control-plans.service";
import {BOM} from "../../../../_common/services/bom.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {WorkorderSapLicenseService} from "../../../services/workorder-sap-license-service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

export class ControlPlanStructure {
  cp?: ControlPlan;
  active?: boolean;
}

@Component({
  selector: 'workorder-view',
  templateUrl: 'workorder-view.component.html',
  styleUrls: ['workorder-view.component.css']
})
export class WorkorderViewComponent extends TranslatableComponent{

  @ViewChild('pvShowDialog') pvShowDialog: DialogComponent;
  @ViewChild('routingShowDialog') routingShowDialog: DialogComponent;
  @ViewChild('bomShowDialog') bomShowDialog: DialogComponent;
  workorder: WorkorderSerionet;

  routings: Routing[] = [];
  productionVersions: RoutingProductionVersion[] = [];
  packingInstructions: PackingInstruction[] = [];
  controlPlans: ControlPlan[] = [];
  types: TypeLink[] = [];
  controlPlanStructures: ControlPlanStructure[] = [];
  boms: BOM[] = [];
  siteFilter: Site;
  workorderCT: number;

  ready = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private loadingService: LoadingService,
              private workorderSerionetService: WorkordersSerionetService, protected translationService: TranslationService,
              private workorderSapLicenseService: WorkorderSapLicenseService) {
    super(translationService);
  }

  ngOnInit() {

    this.workorderSapLicenseService.isLicenseReady().subscribe(() => {
      this.activatedRoute.params.subscribe((params) => {
        if (params['id']) {
          this.loadingService.show();
          this.workorderSerionetService.getWorkorderSerionet(params['id']).subscribe((wo) => {
            this.loadingService.hide();
            this.workorder = wo;
          })
        }
      })
    });
  }

  back() {
    this.router.navigate(["/production-planning"]);
  }

  editWorkorder() {
    this.router.navigate(["/production-planning", 'workorder', 'edit', this.workorder.pk]);
  }

}


