import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {DomSanitizer} from "@angular/platform-browser";
import {DialogComponent} from "../../../../../../shared/material/dialog/dialog.component";
import {dateDiffInDays, getUrlDate, sapDateToUrlDate} from "../../../../../../shared/utils";
import {WorkorderSerionet} from "../../../../../_common/services/workorders-serionet.service";
import {TranslatableComponent} from "../../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'workorder-label',
  templateUrl: 'workorder-label.component.html',
  styleUrls: ['workorder-label.component.css']
})
export class WorkorderLabelComponent extends TranslatableComponent{

  @Input('workorder') workorder:WorkorderSerionet;
  @Input('columnDate') columnDate:Date;
  @Input('nColumns') nColumns:number;
  @Input('columnIndex') columnIndex:number;
  @Output('change') change = new EventEmitter<any>();
  @Output('viewWorkorder') viewWorkorder = new EventEmitter<any>();

  @ViewChild('dialog') dialog:DialogComponent;

  hasFollowData = true;
  hasPreviousData = true;
  isFirstShownColumn = false;
  duration:number;
  isFirstColumn = false;
  isLastColumn = false;

  constructor(protected translationService:TranslationService, private sanitizer:DomSanitizer) {
    super(translationService);
  }

  ngOnInit() {
    if(this.workorder){
      this.isFirstColumn = this.columnIndex==0;
      this.isLastColumn = this.columnIndex==(this.nColumns-1);

      let startDate = null;
      let endDate = null;

      if(this.workorder.sap_actual_start && this.workorder.sap_actual_start!='00000000'){
        startDate = new Date(sapDateToUrlDate(this.workorder.sap_actual_start));
        if(this.workorder.sap_actual_end!='00000000')
          endDate = new Date(sapDateToUrlDate(this.workorder.sap_actual_end));
        else
          endDate = this.calcActualEndDate(this.workorder);
      }
      else {
        startDate = new Date(this.workorder.planned_start);
        endDate = new Date(this.workorder.planned_end);
      }

      this.hasPreviousData = this.isFirstColumn && startDate.getDate() != this.columnDate.getDate();
      this.hasFollowData = this.isLastColumn && endDate.getDate() != this.columnDate.getDate();

      if (this.hasPreviousData){
        this.isFirstShownColumn = true;
        this.duration = Math.min(dateDiffInDays(this.columnDate, endDate)+1,this.nColumns);
      }
      else {
        this.isFirstShownColumn = getUrlDate(startDate)==getUrlDate(this.columnDate);
        this.duration = Math.min(dateDiffInDays(startDate,endDate)+1, this.nColumns-this.columnIndex);
      }

      // if(this.hasPreviousData && this.hasFollowData){
      //
      // }
    }
  }

  calcActualEndDate(wo:WorkorderSerionet){
    let d = new Date(sapDateToUrlDate(wo.sap_actual_start));
    d.setDate(d.getDate()+dateDiffInDays(new Date(wo.planned_start),new Date(wo.planned_end)));
    return d;
  }

  getStyle(){
    if(this.workorder.status &&  this.workorder.status.status_id && this.workorder.status.status_id.color){
      return this.sanitizer.bypassSecurityTrustStyle('background: ' + this.getBackground() + ';');
    }

  }

  getBackground(){
    if(this.workorder.type.delta_rel==1){
      return this.workorder.status.status_id.color;
    }
    else {
      let rgb = this.hexToRgb(this.workorder.status.status_id.color);
      let rgbString = "rgb("+rgb.r+","+rgb.g+","+rgb.b+")";
      let rgbaString = "rgba("+rgb.r+","+rgb.g+","+rgb.b+",0.7)";
      return "repeating-linear-gradient(45deg,"+rgbString+","+rgbString+" 10px,"+rgbaString+" 10px, "+rgbaString+" 20px)";
    }
  }

  hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

  getArrowStyleLeft(){
    if(this.workorder.status &&  this.workorder.status.status_id && this.workorder.status.status_id.color)
      return this.sanitizer.bypassSecurityTrustStyle('border-right-color: ' + this.workorder.status.status_id.color + ';');
  }

  getArrowStyleRight(){
    if(this.workorder.status &&  this.workorder.status.status_id && this.workorder.status.status_id.color)
      return this.sanitizer.bypassSecurityTrustStyle('border-left-color: ' + this.workorder.status.status_id.color + ';');
  }

  getWidth(){
    return this.sanitizer.bypassSecurityTrustStyle('width: '+100*(this.duration)+'%;')
  }

  viewWorkorderInternal(){
    this.viewWorkorder.emit(this.workorder);
  }

  getTooltip(){
    let tooltip = [];
    if(this.workorder.work_order_nr){
      tooltip.push(this.workorder.work_order_nr);
    }
    tooltip.push(this.workorder.material.code);
    tooltip.push(this.workorder.material.description);
    tooltip.push(this.workorder.site.__str__);
    return tooltip.join(" - ")
  }
}


