import {Component, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {
  arrayDistinct, buildErrorMessage, dateDiffInDays, getUrlDate, parseSerionetRespToData,
  sapDateToUrlDate
} from "../../../../../shared/utils";
import {Workcenter} from "../../../../_common/services/workcenters.service";
import {Material} from "../../../../_common/services/materials.service";
import {WorkorderSerionet, WorkordersSerionetService} from "../../../../_common/services/workorders-serionet.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {Site} from "../../../../_common/services/sites.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {Router} from "@angular/router";
import {WorkorderCalendarQueryService} from "../../../services/workorder-calendar-query-service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {DomSanitizer} from "@angular/platform-browser";
import {StatusService} from "../../../../_common/services/status.service";
import {TypeLink, TypeLinkService} from "../../../../_common/services/type-link.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {CompanyPositionNameService} from "../../../../_common/services/company-position-name.service";
import {SiteEvent, SiteEventsService} from "../../../../_common/services/site-events.service";
import {HRProfile} from "../../../../_common/services/hr-profiles.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";
import {UserSitesComponent} from "../../../../../shared/components/user-sites/user-sites.component";

export class ProductionPlanningStructureCell {
  horKey?: string;
  data?: WorkorderSerionet[] = [];
  rowspan: number;
}

export class ProductionPlanningStructureSite {
  site:Site;
  structures:ProductionPlanningStructure[];
  verKeys:VerticalKey[];
}

export class ProductionPlanningStructure {
  date?: Date;
  cells?: ProductionPlanningStructureCell[];
  rows: number;
  isClosed = false;
  isPartiallyClosed = false;
  isToday = false;
  isSunday = false;
}

export class StatusStructure {
  sl?: StatusLink;
  active?: boolean;
}

export class VerticalKey {
  label: string;
  extendedLabel: string;
}

@Component({
  selector: 'app-production-planning-calendar-view',
  templateUrl: 'production-planning-calendar-view.component.html',
  styleUrls: ['production-planning-calendar-view.component.css']
})
export class ProductionPlanningCalendarViewComponent extends TranslatableComponent{

  @ViewChild('editDialog') editDialog: DialogComponent;
  @ViewChild('sitesCombo') sitesCombo: UserSitesComponent;

  verticalView = 'W';
  horizontalView = 'WEEK';
  structuresWithSites: ProductionPlanningStructureSite[] = [];
  currentDate = new Date();
  workordersSerionet: WorkorderSerionet[];
  // verKeys: VerticalKey[] = [];
  statuses: StatusLink[] = [];
  siteFilter: Site;
  prepopulate = false;
  ready = false;
  statusesStructure: StatusStructure[] = [];
  types: TypeLink[] = [];
  typeFilter: number = -1;
  onlySampling = false;
  isGlobal = false;

  closingDays:SiteEvent[] = [];

  availableSites:Site[] = [];

  constructor(protected translationService: TranslationService, private workordersSerionetService: WorkordersSerionetService,
              private loadingService: LoadingService, private router: Router, private workorderCalendarQueryService: WorkorderCalendarQueryService,
              private statusLinkService: StatusLinkService, private domSanitizer: DomSanitizer, private statusService: StatusService,
              private typeLinkService: TypeLinkService, private messageService: MessageService,
              private hasCompanyPositionService: HasCompanyPositionService, private loginService: LoginService,
              private cpnNameService: CompanyPositionNameService, private siteEventsService:SiteEventsService) {
    super(translationService);
  }

  ngOnInit() {
    this.statusLinkService.getStatusLinks({app__model: 'workorder', is_active: true}).subscribe((resp) => {
      this.statuses = parseSerionetRespToData(resp);

      let count = this.statuses.length;
      let cb = () => {
        count--;
        if (count == 0) {

          this.statusesStructure = this.statuses.filter((sl) => sl.delta_rel != 4).map((sl) => {
            let ss = new StatusStructure();
            ss.sl = sl;
            ss.active = [0, 1, 2, 3].indexOf(sl.delta_rel) != -1;
            return ss;
          });

          let query = this.workorderCalendarQueryService.getQuery();
          if (query && Object.keys(query).length > 0) {

            this.currentDate = query['currentDate'];
            this.horizontalView = query['horizontalView'];
            this.verticalView = query['verticalView'];
            if (query['status_delta_rel_in']) {
              let delta_rels = query['status_delta_rel_in'].split("|").map((dl) => parseInt(dl, 10));
              this.statusesStructure.forEach((ss) => {
                ss.active = delta_rels.indexOf(ss.sl.delta_rel) != -1;
              })
            }
            this.typeFilter = query['type__delta_rel'];

            if (query['site'] && query['site'].pk) {
              this.prepopulate = false;
              this.siteFilter = query['site'];
              this.siteFilterSelected(this.siteFilter);
            }
            else {
              this.prepopulate = true;
            }
            this.ready = true;
          }
          else {
            this.prepopulate = true;
            this.ready = true;
          }
        }
      };

      this.statuses.forEach((sl) => {
        this.statusService.getStatus(sl.status_id.pk).subscribe((resp) => {
          sl.status_id = resp;
          cb();
        });
      });

      this.typeLinkService.getTypeLinks({app__model: 'workorder', is_active: true}).subscribe((resp) => {
        this.types = parseSerionetRespToData(resp);
        cb();
      });
      count++;

      this.hasCompanyPositionService.getHasCompanyPositions({
        currents_for_user: this.loginService.user.pk,
        is_active: true
      }).subscribe((response) => {
        let hasCompanyPositions: HasCompanyPosition[] = parseSerionetRespToData(response);

        let count_ = hasCompanyPositions.length;
        let c = () => {
          count_--;
          if (count_ == 0) {
            let cpTypes = hasCompanyPositions.map((hcp) => hcp.get_company_position_obj.name.org_level.pk);
            this.isGlobal = (cpTypes.indexOf(1) != -1);
            cb();
          }
        };

        hasCompanyPositions.forEach((hcp) => {
          this.cpnNameService.getCompanyPositionName(hcp.get_company_position_obj.name.pk).subscribe((resp) => {
            hcp.get_company_position_obj.name = resp;
            c();
          })
        });
      });
      count++;
    })
  }

  sitesReady() {

  }

  siteFilterSelected(s: Site) {
    this.siteFilter = s;
    if (!s || !s.pk) {
      this.typeFilter = 0;
      this.onlySampling = true;
    }
    else {
      this.onlySampling = false;
    }
    this.redraw();
  }

  redraw() {
    this.structuresWithSites = [];
    this.loadingService.show();
    let query = this.getQuery();
    query['explode_all'] = true;
    let call = ()=>{
      this.workordersSerionetService.getWorkordersSerionet(query).subscribe((resp) => {
        this.workordersSerionet = parseSerionetRespToData(resp);
        if (!resp.status) {
          this.redrawVertical();
          this.loadingService.hide();
        }
        else {
          this.loadingService.hide();
          this.messageService.open({
            error: true,
            message: buildErrorMessage(resp),
            title: " ",
            autoClose: false
          });
        }
      })
    };

    if(query['site']){
      let qe = {};
      qe['is_active'] = true;
      qe['date_range'] = query['date_range'];
      qe['site'] = query['site'];
      this.siteEventsService.getSiteEvents(qe).subscribe((resp)=>{
        this.closingDays = parseSerionetRespToData(resp);
        // this.closingDays = resp;
        call();
      })
    }
    else {
      this.closingDays = [];
      call();
    }

  }

  redrawVertical() {
    this.extractSites();
    // this.verKeys = [];
    this.extractVerKeys();
    switch (this.horizontalView) {
      case 'WEEK':
        this.redrawByWeek();
        break;
      case 'MONTH':
        this.redrawByMonth();
        break;
      default:
        break;
    }
    this.getQuery(); //Per salvare
  }

  getQuery() {
    let query = {is_active: true};
    let startDate = new Date(this.currentDate);
    let endDate = new Date();
    switch (this.horizontalView) {
      case 'WEEK':
        // startDate.setDate(startDate.getDate() - this.currentDate.getDay() + 1);
        startDate.setDate(this.currentDate.getDate()-1);
        endDate = new Date(startDate);
        endDate.setDate(endDate.getDate() + 8);
        break;
      case 'MONTH':
        startDate.setDate(1);
        endDate = new Date(startDate);
        endDate.setDate(endDate.getDate() + 31);
        break;
      default:
        break;
    }
    query['date_range'] = getUrlDate(startDate) + "|" + getUrlDate(endDate);
    query['site'] = this.siteFilter.pk;
    if (!query['site']) {
      if (!this.isGlobal) {
        query['site_list'] = this.sitesCombo.sites.map((s) => s.pk).join("|")
      }
    }
    let selectedStatuses = this.statusesStructure.filter((ss) => ss.active).map((ss) => ss.sl.delta_rel).join("|");
    if (selectedStatuses)
      query['status_delta_rel_in'] = selectedStatuses;
    if (this.typeFilter != -1)
      query['type__delta_rel'] = this.typeFilter;

    let queryToSave = {
      horizontalView: this.horizontalView,
      verticalView: this.verticalView,
      currentDate: this.currentDate,
      site: this.siteFilter,
      status_delta_rel_in: selectedStatuses,
      type__delta_rel: this.typeFilter

    };

    this.workorderCalendarQueryService.saveQuery(queryToSave);

    return query;
  }

  extractVerKeys() {
    this.structuresWithSites =  [];
    this.availableSites.forEach((s)=>{
      let str = new ProductionPlanningStructureSite();
      str.site = s;
      str.structures = [];
      switch (this.verticalView) {
        case 'W':
          str.verKeys = this.extractByWorkcenter(s);
          break;
        case 'M':
          str.verKeys = this.extractByMaterial(s);
          break;
        case 'P':
          str.verKeys = this.extractByPeople(s);
          break;
        default:
          break;
      }
      this.structuresWithSites.push(str);
    });
  }

  extractSites(){
    this.availableSites = this.workordersSerionet.map((s) => s.site);

    let sitesPK = arrayDistinct(this.availableSites.map((s)=>s.pk));
    this.availableSites = sitesPK.map((sPK)=>this.sitesCombo.sites.find((s)=>s.pk==sPK));
    if(this.availableSites.length==0){
      this.availableSites = [new Site()];
    }
  }

  extractByWorkcenter(s:Site) {


    let workcentersMatrix: Workcenter[][] = this.workordersSerionet.filter((w)=>w.site.pk==s.pk).map((s) => s.routing.routing_operations.map((o) => o.work_center));
    let workcenters: Workcenter[] = [];
    workcentersMatrix.forEach((wcl) => {
      workcenters = workcenters.concat(wcl);
    });
    let wc_labels = workcenters.map((wc) => wc.__str__);
    wc_labels = arrayDistinct(wc_labels);
    return wc_labels.map((l) => {
      let vk = new VerticalKey();
      vk.label = l;
      vk.extendedLabel = workcenters.find((wc) => wc.__str__ == l).description;
      return vk;
    });
  }

  extractByMaterial(s:Site) {
    let materials: Material[] = this.workordersSerionet.filter((w)=>w.site.pk==s.pk).map((s) => s.material);
    let mat_labels = materials.map((m) => m.code);
    mat_labels = arrayDistinct(mat_labels);
    return mat_labels.map((l) => {
      let vk = new VerticalKey();
      vk.label = l;
      vk.extendedLabel = l + " " + materials.find((m) => m.code == l).description;
      return vk;
    });
  }

  extractByPeople(s:Site) {
    let teamMatrix: HRProfile[][] = this.workordersSerionet.filter((w)=>w.site.pk==s.pk).map((s) => s.team);
    let teams = [];
    teamMatrix.forEach((wcl) => {
      teams = teams.concat(wcl);
    });
    let team_labels = teams.map((user) => user.__str__);
    team_labels = arrayDistinct(team_labels);
    return team_labels.map((l) => {
      let vk = new VerticalKey();
      vk.label = l;
      vk.extendedLabel = l;
      return vk;
    });
  }

  redrawByWeek() {
    let startDate = new Date(this.currentDate);
    //Il +1 è per partire dal lunedì
    // startDate.setDate(startDate.getDate() - this.currentDate.getDay() + 1);
    startDate.setDate(startDate.getDate() - 1);

    this.processRedraw(startDate, 9, (sd) => {
      sd.setDate(sd.getDate() + 1);
      return sd;
    });
  }

  redrawByMonth() {
    let startDate = new Date(this.currentDate);
    startDate.setDate(1);

    let d = new Date(startDate);
    d.setMonth(d.getMonth() + 1);
    d.setDate(d.getDate() - 1);

    this.processRedraw(startDate, d.getDate(), (sd) => {
      sd.setDate(sd.getDate() + 1);
      return sd;
    });
  }

  processRedraw(startDate: Date, nColumns: number, incrementFunction: any) {
    let initDate = getUrlDate(startDate);
    this.structuresWithSites.forEach((ss)=>{
      let verKeysData = {};
      startDate = new Date(initDate);

      for (let i = 0; i < nColumns; i++) {
        let s = new ProductionPlanningStructure();
        s.date = new Date(startDate);
        s.isToday = getUrlDate(s.date)==getUrlDate(new Date());
        s.isSunday = s.date.getDay()==0;
        s.cells = [];
        ss.verKeys.forEach((vk) => {
          let cell = new ProductionPlanningStructureCell();
          cell.horKey = vk.label;
          cell.data = this.getCellData(s.date, vk.label, ss.site);
          s.cells.push(cell);
          if (!verKeysData[vk.label])
            verKeysData[vk.label] = [];
          cell.data.forEach((wo) => {
            if (verKeysData[vk.label].indexOf(wo.pk) == -1)
              verKeysData[vk.label].push(wo.pk);
          })
        });
        if(this.closingDays.length>0){
          let dateToSearch = new Date(getUrlDate(new Date(s.date)));
          let twelveHrs = 60*60*12*1000;
          dateToSearch.setHours(12);
          this.closingDays.forEach((se)=>{
            let d1 = new Date(se.start_date);
            let d2 = new Date(se.end_date);
            let start = new Date(getUrlDate(d1));
            start.setHours(0);
            start.setMinutes(0);
            let end = new Date(getUrlDate(d2));
            end.setHours(23);
            end.setMinutes(59);
            if(dateToSearch.getTime()>=start.getTime() && dateToSearch.getTime()<=end.getTime() ){
              //IN questa colonna una chiusura c'è
              let closingDuration = d2.getTime()-d1.getTime();
              if(closingDuration>twelveHrs)
                s.isClosed = true;
              else
                s.isPartiallyClosed = true;
            }
          });
        }

        ss.structures.push(s);
        startDate = incrementFunction(startDate);
      }

      let total = 0;
      ss.verKeys.forEach((vk, index) => {
        ss.structures.forEach((s) => {
          s.cells[index].rowspan = verKeysData[ss.verKeys[index].label].length;
        });

        total += verKeysData[ss.verKeys[index].label].length;
      });

      ss.structures.forEach((s) => {
        s.rows = total;
        s.cells.forEach((c, index) => {
          let newData = [];
          verKeysData[ss.verKeys[index].label].forEach((pk) => {
            newData.push(c.data.find((wo) => wo.pk == pk));
          });
          c.data = newData;
        })
      });
    });
  }

  getCellData(date, vk: string, site:Site) {
    return this.workordersSerionet.filter((w) => {
      if(w.site.pk==site.pk){
        if (!this.isWorkorderActiveInDate(w, date)) {
          return false;
        } else {
          switch (this.verticalView) {
            case 'W':
              return w.routing.routing_operations.map((o) => o.work_center).filter((wc) => wc.__str__ == vk).length > 0;
            case 'M':
              return w.material.code == vk;
            case 'P':
              return w.team.filter((u) => u.__str__ == vk).length > 0;
            default:
              break;
          }
        }
      }
      else {
        return false;
      }
    })
  }

  isWorkorderActiveInDate(s: WorkorderSerionet, d: Date) {

    let startDate = null;
    let endDate = null;

    if (s.sap_actual_start && s.sap_actual_start != '00000000') {
      startDate = new Date(sapDateToUrlDate(s.sap_actual_start));
      if (s.sap_actual_end != '00000000')
        endDate = new Date(sapDateToUrlDate(s.sap_actual_end));
      else
        endDate = this.calcActualEndDate(s);
    }
    else {
      startDate = new Date(s.planned_start);
      endDate = new Date(s.planned_end);
    }

    startDate.setHours(0);
    startDate.setMinutes(0);

    endDate.setHours(23);
    endDate.setMinutes(59);

    return d.getTime() >= startDate.getTime() && d.getTime() <= endDate.getTime();
  }

  calcActualEndDate(wo: WorkorderSerionet) {
    let d = new Date(sapDateToUrlDate(wo.sap_actual_start));
    d.setDate(d.getDate() + dateDiffInDays(new Date(wo.planned_start), new Date(wo.planned_end)));
    return d;
  }

  backDate() {
    switch (this.horizontalView) {
      case 'WEEK':
        this.currentDate.setDate(this.currentDate.getDate() - 7);
        break;
      case 'MONTH':
        this.currentDate.setMonth(this.currentDate.getMonth() - 1);
        break;
      default:
        break;
    }
    this.redraw();
  }

  forwardDate() {
    switch (this.horizontalView) {
      case 'WEEK':
        this.currentDate.setDate(this.currentDate.getDate() + 7);
        break;
      case 'MONTH':
        this.currentDate.setMonth(this.currentDate.getMonth() + 1);
        break;
      default:
        break;
    }
    this.redraw();
  }

  createWorkorder() {
    this.router.navigate(["production-planning", "workorder", "create"], {
      queryParams: {
        site: this.siteFilter.pk,
        current_date: getUrlDate(this.currentDate)
      }
    })
  }

  viewWorkorder(wo: WorkorderSerionet) {
    this.router.navigate(["production-planning", "workorder", "view", wo.pk])
  }

  getStatusStyle(s: StatusLink) {
    if (s.status_id && s.status_id.color) {
      return this.domSanitizer.bypassSecurityTrustStyle("background-color:" + s.status_id.color + "; color:black; padding:5px;margin-right:10px");
    }
  }

  getWoContainerHeight(s_) {
    let totRows = 0;
    this.structuresWithSites.forEach((s)=>{
      if(s_){

        if(s_.pk==s.site.pk){
          totRows = totRows+s.structures[0].rows;
        }
      }
      else {
        totRows = totRows+s.structures[0].rows;
      }
    });
    return this.domSanitizer.bypassSecurityTrustStyle("height:" + 32 * totRows + "px");
  }
}


