import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {ProductionPlanningCalendarViewComponent} from "./production-planning-calendar-view.component";
import {WorkorderLabelComponent} from "./workorder-label/workorder-label.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [ProductionPlanningCalendarViewComponent,WorkorderLabelComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class ProductionPlanningCalendarViewModule { }


