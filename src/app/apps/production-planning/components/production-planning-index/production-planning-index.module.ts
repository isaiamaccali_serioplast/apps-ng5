import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {ProductionPlanningIndexComponent} from "./production-planning-index.component";
import {WorkorderEditModule} from "./workorder-edit/workorder-edit.module";
import {WorkorderViewModule} from "./workorder-view/workorder-view.module";

@NgModule({
  imports: [SharedModule, WorkorderEditModule, WorkorderViewModule],       // module dependencies
  declarations: [ProductionPlanningIndexComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class ProductionPlanningIndexModule { }


