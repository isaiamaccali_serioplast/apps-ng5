import {Component, ViewChild} from "@angular/core";
import {WorkorderSerionet, WorkordersSerionetService} from "../../../../_common/services/workorders-serionet.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {Routing, RoutingsService} from "../../../../_common/services/routings.service";
import {TypeLink, TypeLinkService} from "../../../../_common/services/type-link.service";
import {Material} from "../../../../_common/services/materials.service";
import {PackingInstruction, PackingInstructionsService} from "../../../../_common/services/packing-instructions.service";
import {RoutingProductionVersion, RoutingProductionVersionService} from "../../../../_common/services/routing-production-version-service";
import {areDateAligned, arrayDistinct, buildErrorMessage, cloneObject, fixValue, getUrlDate, parseSerionetRespToData} from "../../../../../shared/utils";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {ControlPlan, ControlPlansService} from "../../../../_common/services/control-plans.service";
import {BOM, BOMService} from "../../../../_common/services/bom.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {PlantSAPService} from "../../../../_common/services/plant-flow-service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {HRProfile} from "../../../../_common/services/hr-profiles.service";
import {DomSanitizer} from "@angular/platform-browser";
import {ProjectTask} from "../../../../_common/services/project-task.service";
import {MaterialInfo, MaterialInfoService} from "../../../../_common/services/material-info.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {WorkorderTypeSAP, WorkorderTypeSapService} from "../../../../_common/services/workorder-type-sap.service";
import {WorkorderLabelSAP, WorkorderLabelSapService} from "../../../../_common/services/workorder-label-sap.service";
import {WorkorderSapLicenseService} from "../../../services/workorder-sap-license-service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

export class ControlPlanStructure {
  cp?: ControlPlan;
  active?: boolean;
}

@Component({
  selector: 'workorder-edit',
  templateUrl: 'workorder-edit.component.html',
  styleUrls: ['workorder-edit.component.css']
})
export class WorkorderEditComponent extends BaseComponent {

  @ViewChild('pvShowDialog') pvShowDialog: DialogComponent;
  @ViewChild('routingShowDialog') routingShowDialog: DialogComponent;
  @ViewChild('bomShowDialog') bomShowDialog: DialogComponent;
  @ViewChild('typeSelectDialog') typeSelectDialog: DialogComponent;
  @ViewChild('labelSelectionDialog') labelSelectionDialog: DialogComponent;
  workorder: WorkorderSerionet;

  routings: Routing[] = [];
  productionVersions: RoutingProductionVersion[] = [];
  packingInstructions: PackingInstruction[] = [];
  controlPlans: ControlPlan[] = [];
  types: TypeLink[] = [];
  controlPlanStructures: ControlPlanStructure[] = [];
  boms: BOM[] = [];
  siteFilter: Site;
  today = getUrlDate(new Date());
  hasCompanyPositions: HasCompanyPosition[] = [];
  ready = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, protected loadingService: LoadingService,
              private workorderSerionetService: WorkordersSerionetService, private sitesService: SitesService,
              private pvService: RoutingProductionVersionService, private bomService: BOMService,
              private routingsService: RoutingsService, private packingInstructionsService: PackingInstructionsService,
              private controlPlansService: ControlPlansService, private plantSapService: PlantSAPService,
              protected messageService: MessageService, protected translationService: TranslationService,
              private typeLinkService: TypeLinkService, private sanitizer: DomSanitizer, private materialInfoService: MaterialInfoService,
              private hasCompanyPositionService: HasCompanyPositionService, private loginService: LoginService,
              private workorderTypeSapService: WorkorderTypeSapService, private workorderLabelSapService: WorkorderLabelSapService,
              private workorderSapLicenseService: WorkorderSapLicenseService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    this.loadingService.show();
    this.typeLinkService.getTypeLinks({app__model: 'workorder', is_active: true}).subscribe((resp) => {
      this.types = parseSerionetRespToData(resp);
      this.hasCompanyPositionService.getHasCompanyPositions({
        currents_for_user: this.loginService.user.pk,
        is_active: true
      }).subscribe((response) => {
        this.hasCompanyPositions = parseSerionetRespToData(response);

        this.workorderSapLicenseService.isLicenseReady().subscribe(() => {
          this.activatedRoute.params.subscribe((params) => {
            if (params['id']) {
              this.workorderSerionetService.getWorkorderSerionet(params['id']).subscribe((wo) => {

                this.workorder = wo;
                if (!this.workorder.wbs)
                  this.workorder.wbs = new ProjectTask();
                this.siteChange(this.workorder.site, () => {
                  this.selectedMaterial(this.workorder.material, () => {
                    if (this.workorder.production_version.pk) {
                      fixValue(this.productionVersions, this.workorder, 'production_version');
                    }
                    else {
                      this.workorder.production_version = new RoutingProductionVersion();
                    }
                    if (this.workorder.bulk_packing_instruction) {
                      this.workorder.packing_instruction = new PackingInstruction();
                      this.workorder.packing_instruction.pk = 0;
                    }
                    else {
                      if (!this.workorder.packing_instruction || !this.workorder.packing_instruction.pk) {
                        this.workorder.packing_instruction = new PackingInstruction();
                        if (!this.workorder._newFlow)
                          this.workorder.packing_instruction.pk = -1;
                      }
                    }
                    this.loadingService.hide();
                    this.ready = true;
                  });
                });
              })
            }
            else {
              this.activatedRoute.queryParams.subscribe((params) => {
                if (params['site']) {
                  this.sitesService.getSite(params['site']).subscribe((site) => {
                    this.loadingService.hide();
                    this.siteFilter = site;
                    this.initWO(site, params['current_date']);
                  })
                }
              })
            }
          })
        });
      });
    });
  }

  initWO(s: Site, d: string) {
    let wo = new WorkorderSerionet();
    wo.routing = new Routing();
    wo.site = s;
    if (this.isAuthorizedEditSampling() && this.isAuthorizedEditProduction()) {
      wo.type = new TypeLink();
    }
    else {
      if (this.isAuthorizedEditSampling()) {
        wo.type = cloneObject(this.types.find((tl) => tl.delta_rel == 0));
      }
      else if (this.isAuthorizedEditProduction()) {
        wo.type = cloneObject(this.types.find((tl) => tl.delta_rel == 1));
      }
    }

    let proceed = () => {
      wo.material = new Material();
      wo.control_plans = [];
      wo.bulk_packing_instruction = false;
      wo.packing_instruction = new PackingInstruction();
      wo.production_version = new RoutingProductionVersion();
      wo.planned_start = d ? d : getUrlDate(new Date());
      wo.planned_end = d ? d : getUrlDate(new Date());
      wo.team = [];
      this.workorder = wo;
      this.siteChange(wo.site, () => {
        if(this.workorder._newFlow){
          this.workorder.packing_instruction.pk = 0;
        }
        else {
          this.workorder.packing_instruction.pk = -1;
        }
        this.ready = true;
      });
    };

    if (wo.type.pk) {
      this.checkTypePlant(wo, () => {
        proceed();
      })
    }
    else {
      proceed();
    }
  }

  typesToSelect: WorkorderTypeSAP[] = [];
  woForType: WorkorderSerionet;

  checkTypePlant(wo: WorkorderSerionet, callback) {
    if (!wo.type_sap_code) {
      this.loadingService.show();
      this.workorderTypeSapService.getTypes({
        Plant: wo.site.code,
        Sampling: wo.type.delta_rel == 0
      }).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          let types: WorkorderTypeSAP[] = resp.response.results;
          if (types.length > 0) {
            if (types.length == 1) {
              wo.type_sap_code = types[0].Type;
              callback();
            }
            else {
              this.typesToSelect = types;
              this.woForType = wo;
              let u = this.typeSelectDialog.onClose.subscribe(() => {
                u.unsubscribe();
                callback();
              });
              this.typeSelectDialog.open();
            }
          }
          else {
            this.messageService.open({
              error: true,
              message: "Selected plant does not support this workorder type",
              title: " ",
              autoClose: false
            });
          }
        }
        else {
          this.messageService.open({
            error: true,
            message: buildErrorMessage(resp),
            title: " ",
            autoClose: false
          });
        }
      })
    }
    else {
      callback();
    }

  }

  selectTypeAndClose(t: WorkorderTypeSAP) {
    this.woForType.type_sap_code = t.Type;
    this.typeSelectDialog.close();

  }

  selectedMaterial(m: Material, cb?: any) {
    this.routings = [];
    this.packingInstructions = [];
    this.productionVersions = [];
    this.controlPlans = [];

    if (m && m.pk) {


      let loadMaterialData = () => {

        this.loadingService.show();
        let count = 0;
        let callback = () => {
          count--;
          if (count == 0) {
            this.loadingService.hide();
            if (cb)
              cb();
          }
        };
        let query = {
          is_active: true,
          material: this.workorder.material.pk,
          material_id: this.workorder.material.pk,
          site: this.workorder.site.pk
        };

        if (this.workorder._newFlow) {
          let queryPV = {};
          Object.assign(queryPV, query);
          queryPV['routing__status_delta_rel_in'] = this.workorder.type.delta_rel == 0 ? "0" : "1|2";
          this.pvService.getRoutingProductionVersions(queryPV).subscribe((resp) => {
            this.productionVersions = parseSerionetRespToData(resp);
            if (this.productionVersions.length > 0) {
              let routings = arrayDistinct(this.productionVersions.map((pv) => pv.routing_header.pk));
              let routingsExpanded = [];
              let count = routings.length;
              let call_ = () => {
                count--;
                if (count == 0) {
                  this.productionVersions.forEach((pv) => {
                    pv.routing_header = routingsExpanded.find((r) => r.pk == pv.routing_header.pk);
                  });
                  let boms = arrayDistinct(this.productionVersions.map((pv) => pv.bom_header.pk));
                  let bomsExpanded = [];
                  let count = boms.length;
                  let call_ = () => {
                    count--;
                    if (count == 0) {
                      this.productionVersions.forEach((pv) => {
                        pv.bom_header = bomsExpanded.find((r) => r.pk == pv.bom_header.pk);
                        pv.routing_header.production_version = pv;
                      });
                      this.productionVersions.sort((pv1,pv2)=>{
                        return parseInt(pv1.routing_header.group,10)-parseInt(pv2.routing_header.group,10)
                      });
                      callback();
                    }
                  };

                  boms.forEach((rPK) => {
                    this.bomService.getBOM(rPK, null, {explode_all: true}).subscribe((resp) => {
                      bomsExpanded.push(resp);
                      call_();
                    })
                  });
                }
              };

              routings.forEach((rPK) => {
                this.routingsService.getRouting(rPK, null, {explode_all: true}).subscribe((resp) => {
                  let r: Routing = resp;
                  r.routing_operations.sort((ro1, ro2) => {
                    return parseInt(ro1.operation, 10) - parseInt(ro2.operation, 10);
                  });
                  routingsExpanded.push(r);
                  call_();
                })
              })
            }
            else {
              //NOTA: questo è un caso di errore/mancanza di dati.
              callback();
            }

          });
          count++;
        }
        else {
          let queryRouting = {};
          Object.assign(queryRouting, query);
          queryRouting['explode_all'] = true;
          queryRouting['status_delta_rel_in'] = this.workorder.type.delta_rel == 0 ? "0" : "1|2";
          this.routingsService.getRoutings(queryRouting).subscribe((resp) => {
            this.routings = parseSerionetRespToData(resp);
            this.routings.forEach((r) => {
              r.routing_operations.sort((ro1, ro2) => {
                return parseInt(ro1.operation, 10) - parseInt(ro2.operation, 10);
              });
            });
            this.routings.sort((r1,r2)=>{
              return parseInt(r1.group,10)-parseInt(r2.group,10)
            });
            callback();
          });
          count++;

          let queryPV = {};
          Object.assign(queryPV, query);
          queryPV['routing__status_delta_rel_in'] = this.workorder.type.delta_rel == 0 ? "0" : "1|2";

          this.pvService.getRoutingProductionVersions(queryPV).subscribe((resp) => {
            this.productionVersions = parseSerionetRespToData(resp);
            if (this.productionVersions.length > 0) {
              let routings = arrayDistinct(this.productionVersions.map((pv) => pv.routing_header.pk));
              let routingsExpanded = [];
              let count = routings.length;
              let call_ = () => {
                count--;
                if (count == 0) {
                  this.productionVersions.forEach((pv) => {
                    pv.routing_header = routingsExpanded.find((r) => r.pk == pv.routing_header.pk);
                  });
                  let boms = arrayDistinct(this.productionVersions.map((pv) => pv.bom_header.pk));
                  let bomsExpanded = [];
                  let count = boms.length;
                  let call_ = () => {
                    count--;
                    if (count == 0) {
                      this.productionVersions.forEach((pv) => {
                        pv.bom_header = bomsExpanded.find((r) => r.pk == pv.bom_header.pk);
                        pv.routing_header.production_version = pv;
                      });
                      this.productionVersions.sort((pv1,pv2)=>{
                        return parseInt(pv1.routing_header.group,10)-parseInt(pv2.routing_header.group,10)
                      });
                      callback();
                    }
                  };

                  boms.forEach((rPK) => {
                    this.bomService.getBOM(rPK, null, {explode_all: true}).subscribe((resp) => {
                      bomsExpanded.push(resp);
                      call_();
                    })
                  });
                }
              };

              routings.forEach((rPK) => {
                this.routingsService.getRouting(rPK, null, {explode_all: true}).subscribe((resp) => {
                  let r: Routing = resp;
                  r.routing_operations.sort((ro1, ro2) => {
                    return parseInt(ro1.operation, 10) - parseInt(ro2.operation, 10);
                  });
                  routingsExpanded.push(r);
                  call_();
                })
              })
            }
            else {
              callback();
            }
          });
          count++;
        }

        if(!this.workorder._newFlow){
          this.packingInstructions = [];
          let empty = new PackingInstruction();
          empty.pk = -1;
          empty.description = "---";
          let bulk = new PackingInstruction();
          bulk.pk = 0;
          bulk.description = "Bulk";
          this.packingInstructions.push(empty,bulk);
        }
        else {
          this.packingInstructionsService.getPackingInstructions(query).subscribe((resp) => {
            this.packingInstructions = parseSerionetRespToData(resp);
            let bulk = new PackingInstruction();
            bulk.pk = 0;
            bulk.description = "Bulk";
            this.packingInstructions.splice(0, 0, bulk);
            callback();
          });
          count++;
        }

        this.controlPlansService.getControlPlans(query).subscribe((resp) => {
          this.controlPlans = parseSerionetRespToData(resp);
          this.controlPlanStructures = this.controlPlans.map((cp) => {
            let cps = new ControlPlanStructure();
            cps.cp = cp;
            cps.active = this.workorder.control_plans.map((cp) => cp.pk).indexOf(cp.pk) != -1;
            return cps;
          });
          callback();
        });
        count++
      };

      this.loadingService.show();
      this.materialInfoService.getMaterialInfos({
        Material: m.code,
        Plant: this.workorder.site.code
      }).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          let results: MaterialInfo[] = resp.response.results;
          if (results.length != 0) {

            let info = results[0];

            if (info.GeneralStatus) {
              this.messageService.open({
                error: true,
                message: "General Status: " + info.GeneralStatusDescription + "(" + info.GeneralStatus + ")",
                title: " ",
                autoClose: false,
                onClose: () => {
                  this.workorder.material = new Material();
                }
              });
            }
            else {
              if (info.PlantStatus) {
                this.messageService.open({
                  error: true,
                  message: "Plant Status: " + info.PlantStatusDescription + "(" + info.PlantStatus + ")",
                  title: " ",
                  autoClose: false,
                  onClose: () => {
                    this.workorder.material = new Material();
                  }
                });
              }
              else {
                let ccst = parseFloat(info.CCSTPrice);
                if (ccst != 0) {
                  this.workorder.material = m;
                  loadMaterialData();
                }
                else {
                  this.messageService.open({
                    error: true,
                    message: "Material has not beed costed yet, can't be produced",
                    title: " ",
                    autoClose: false,
                    onClose: () => {
                      this.workorder.material = new Material();
                    }
                  });
                }
              }
            }
          }
          else {
            this.messageService.open({
              error: true,
              message: "ATTENTION: no material info for this material-plant",
              title: " ",
              autoClose: false,
              onClose: () => {
                this.workorder.material = new Material();
              }
            });
          }
        }
      });


    }
    else {
      this.workorder.material = null;
    }
  }

  siteChange(s: Site, callback?: any) {
    console.log(s);
    this.workorder.site = s;
    this.loadingService.show();
    this.plantSapService.getPlants({Code: this.workorder.site.code}).subscribe((resp) => {
      if (resp.status == 200) {
        this.workorder._newFlow = resp.response.results[0].NewFlow;
        this.loadingService.hide();
        if (callback)
          callback();
        else {
          this.workorder.material = new Material();
          this.packingInstructions = [];
          this.routings = [];
          this.productionVersions = [];
          this.workorder.packing_instruction = new PackingInstruction();
          this.workorder.routing = new Routing();
          this.workorder.production_version = new RoutingProductionVersion();
        }
      }
    })
  }

  fixType() {
    fixValue(this.types, this.workorder, 'type');
    this.workorder.type_sap_code = null;
    this.checkTypePlant(this.workorder, () => {

    });
  }

  saveWorkorder() {
    this.loadingService.show();
    this.workorder.control_plans = this.controlPlanStructures.filter((cps) => cps.active).map((cps) => cps.cp);
    this.workorderSerionetService.saveWorkorderSerionet(this.workorder).subscribe((resp) => {
      if (!this.workorder.labels_old) {
        this.manageRespSave(resp, () => {
          this.loadingService.hide();
          this.router.navigate(["production-planning", 'workorder', 'view', resp.pk]);
        })
      }
      else {
        this.manageRespSave(resp, () => {
          this.processLabels(() => {
            this.loadingService.hide();
            this.router.navigate(["production-planning", 'workorder', 'view', resp.pk]);
          })
        }, true);
      }
    })
  }

  processLabels(callback) {
    this.processDeleteLabels(() => {
      this.processSaveLabels(() => {
        callback();
      })
    })
  }

  processDeleteLabels(callback) {
    let count = this.workorder.labels_old.length;
    let cb = () => {
      count--;
      if (count == 0) {
        callback();
      }
    };

    if (count == 0) {
      count = 1;
      cb();
    }

    this.workorder.labels_old.forEach((l) => {
      this.workorderLabelSapService.deleteLabel(l).subscribe((resp) => {
        if (resp.status == 204) {
          cb();
        }
        else {
          this.loadingService.hide();
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      })
    })
  }

  processSaveLabels(callback) {
    let count = this.workorder.labels.length;
    let cb = () => {
      count--;
      if (count == 0) {
        callback();
      }
    };

    if (count == 0) {
      count = 1;
      cb();
    }

    this.workorder.labels.forEach((l) => {
      this.workorderLabelSapService.saveLabel(l).subscribe((resp) => {
        if (resp.status > 200 && resp.status < 300) {
          cb();
        }
        else {
          this.loadingService.hide();
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      })
    })
  }

  canSaveWorkorder() {

    let isMaterialDataOk = (wo: WorkorderSerionet) => {
      if (wo._newFlow) {
        return wo.routing && !!wo.routing.pk && wo.production_version && !!wo.production_version.pk && (wo.bulk_packing_instruction || wo.packing_instruction.pk>0);
      }
      else {
        return wo.routing && !!wo.routing.pk;
      }
    };

    return this.workorder && this.workorder.site && this.workorder.site.pk && this.workorder.quantity &&
      this.workorder.material && this.workorder.material.pk && this.workorder.type_sap_code && isMaterialDataOk(this.workorder) && this.isStartValid() && areDateAligned(this.workorder.planned_start, this.workorder.planned_end)
  }

  isStartValid() {
    let today = new Date();
    let start = new Date(this.workorder.planned_start);

    today.setHours(0);
    start.setHours(23);

    return start.getTime() > today.getTime();
  }

  selectUser(user: HRProfile) {
    this.workorder.team.push(user);
  }

  removeTeamUser(user: HRProfile) {
    this.workorder.team.splice(this.workorder.team.indexOf(user), 1);
  }

  canDeleteWorkorder() {
    return this.workorder.pk && !this.workorder.work_order_nr || (this.workorder.work_order_nr && [0, 1, 2].indexOf(this.workorder.status.delta_rel) != -1);
  }

  deleteWorkorder() {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.workorderSerionetService.deleteWorkorderSerionet(this.workorder).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.back();
          })
        })
      },
      deny: () => {
      }
    });
  }

  setPV() {
    fixValue(this.routings, this.workorder, 'routing');
    if (this.workorder.routing.production_version && this.workorder.routing.production_version.pk) {
      this.workorder.production_version.pk = this.workorder.routing.production_version.pk;
      fixValue(this.productionVersions, this.workorder, 'production_version');
    }

  }

  setRouting() {
    fixValue(this.productionVersions, this.workorder, 'production_version');
    this.workorder.routing = this.workorder.production_version.routing_header;
    fixValue(this.routings, this.workorder, 'routing');
  }

  showSelectedPV() {
    this.pvShowDialog.open();
  }

  showBOM() {
    this.bomShowDialog.open();
  }

  showSelectedRouting() {
    this.routingShowDialog.open();
  }

  back() {
    if (this.workorder.pk) {
      this.router.navigate(["production-planning", "workorder", "view", this.workorder.pk]);
    }
    else {
      this.router.navigate(["production-planning"]);
    }
  }

  getWorkorderStyle() {
    if (this.workorder.status && this.workorder.status.status_id && this.workorder.status.status_id.color)
      return this.sanitizer.bypassSecurityTrustStyle('background-color: ' + this.workorder.status.status_id.color + '; padding:0 20px');
  }

  handlePackingInstructionChange() {
    this.workorder.bulk_packing_instruction = (this.workorder.packing_instruction.pk == 0);
    if (this.workorder.work_order_nr) {
      this.handleLabels();
    }
  }

  availableLabels: WorkorderLabelSAP[] = [];

  handleLabels() {

    if(this.workorder.packing_instruction.pk!=-1){
      this.loadingService.show();
      this.workorderLabelSapService.getLabels({Plant: this.workorder.site.code}).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          this.availableLabels = resp.response.results;
          if (this.workorder.bulk_packing_instruction || !this.workorder.packing_instruction.pk) {
            this.availableLabels = this.availableLabels.filter((l) => l.Name == 'ZMES_BATCH_LABEL')
          }
          this.labelSelectionDialog.open();
        }
        else {
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      })
    }
    else {
      this.workorder.labels_old = cloneObject(this.workorder.labels);
      this.workorder.labels = [];
    }

  }

  editLabels() {
    this.loadingService.show();
    this.workorderLabelSapService.getLabels({Plant: this.workorder.site.code}).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        this.availableLabels = resp.response.results;
        if (this.workorder.bulk_packing_instruction) {
          this.availableLabels = this.availableLabels.filter((l) => l.Name == 'ZMES_BATCH_LABEL')
        }

        let av_lab_names = this.workorder.labels.map((l) => l.Name);

        this.availableLabels.forEach((l) => {
          if (av_lab_names.indexOf(l.Name) == -1)
            l.Preferred = false;
          else {
            let m = this.workorder.labels.find((l_) => l_.Name == l.Name);
            if (m) {
              l.Preferred = m.Preferred;
              l.Copies = m.Copies;
            }
          }
        });
        this.labelSelectionDialog.open();
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  saveLabelEditing() {

    if (!this.workorder.labels_old) {
      this.workorder.labels_old = cloneObject(this.workorder.labels);
    }
    this.workorder.labels = this.availableLabels.filter((l) => l.Preferred);
    this.workorder.labels.forEach((l) => {
      if (!l.Plant)
        l.Plant = " ";
      if (!l.WorkOrder)
        l.WorkOrder = this.workorder.work_order_nr;
    });

    this.labelSelectionDialog.close();
  }

  wbsSelected(wbs: ProjectTask) {
    this.workorder.wbs = wbs;
  }

  static DEVELOPERS = [
    1, //Serioplast Software Developer
    10, //Software Development Manager
    7, //ERP Manager
    8 //ERP Support
  ];

  /*  CREATE, CHANGE STATUS e UPDATE:
  - R&D Logistics manager e logistic assistant (per gli ordini di tipo Sampling)
  - Production managers and assistants, logistic managers and assistant (per gli ordini di tipo Production)*/

  static SAMPLING_EDITORS = [
    48, //R&D Logistics manager
    162, //R&D Logistics assistant
  ];

  static PRODUCTION_EDITORS = [
    65, //Production managers
    66, //Production assistants
    72, //Logistic manager
    73  //Logistic assistant
  ];

  isAuthorizedEditSampling() {
    let userCompanyPositionNames = this.hasCompanyPositions.map((item: HasCompanyPosition) => {
      return item.get_company_position_obj.name.pk;
    });
    return WorkorderEditComponent.DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 ||
      userCompanyPositionNames.filter((item) => WorkorderEditComponent.SAMPLING_EDITORS.indexOf(item) !== -1).length > 0;
  }

  isAuthorizedEditProduction() {
    let userCompanyPositionNames = this.hasCompanyPositions.map((item: HasCompanyPosition) => {
      return item.get_company_position_obj.name.pk;
    });
    return WorkorderEditComponent.DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 ||
      userCompanyPositionNames.filter((item) => WorkorderEditComponent.PRODUCTION_EDITORS.indexOf(item) !== -1).length > 0;
  }

  getRoutingStatuses() {
    return this.workorder.type.delta_rel == 0 ? "0" : "1|2"
  }

  getTeamAdditionalQuery(){
    let positions = [
      // 5, //Quality Manager,
      61, //Plant Manager
      62, //Technical Manager
      63, //Technologist
      64, //Technician
      78, //Quality Manager (Local)
      79, //Quality Controller (Local)
      123, //R&D Process Engineer
      // 141, //Quality Control Manager
      // 142, //Quality Control Engineer
      // 143, //Continuous Improvement Manager
      // 144, //Continuous Improvement Engineer
      // 145, //Quality Assurance Manager
      // 146, //Quality Assurance Engineer,
      148, //R&D Lab Manager,
      149, //R&D Lab Technician
      150, //R&D Process Manager
      151, //R&D Technologist
      152, //R&D Junior Technologist
      163, //Quality Assurance and Continuous Improvement Officer (Local)
      164 //Senior Technologist
    ];

    return {employee_job_title_in:positions.join("|")}
  }
}


