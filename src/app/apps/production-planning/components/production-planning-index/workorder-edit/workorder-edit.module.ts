import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {WorkorderEditComponent} from "./workorder-edit.component";
import {BomDisplayModule} from "../../../../../shared/components/bom-display/bom-display.module";
import {RoutingDisplayModule} from "../../../../../shared/components/routing-display/routing-display.module";

@NgModule({
  imports: [SharedModule, BomDisplayModule, RoutingDisplayModule],       // module dependencies
  declarations: [WorkorderEditComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class WorkorderEditModule { }


