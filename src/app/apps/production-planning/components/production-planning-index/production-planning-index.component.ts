import {Component} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {QualityStockBatchListQueryService} from "../quality-stock-batch-list/quality-stock-batch-list-query-service";
import {WorkorderSapLicenseService} from "../../services/workorder-sap-license-service";

@Component({
  selector: 'app-production-planning-index',
  templateUrl: 'production-planning-index.component.html',
  styleUrls: []
})
export class ProductionPlanningIndexComponent{

  constructor(private sidenavService: SideNavService, private titleService: Title,
              private workorderSapLicenseService:WorkorderSapLicenseService) {
    this.sidenavService.leftMenuEnabled = false;
    this.sidenavService.leftMenu = false;
    this.sidenavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Production Planning");
    this.workorderSapLicenseService.reset();
  }
}


