import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {hrTrainingsRoutes} from "./hr-trainings.routes";
import {TrainingListModule} from "./components/training-list/training-list.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(hrTrainingsRoutes),TrainingListModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class HRTrainingsModule {}

