import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {TrainingListComponent} from "./components/training-list/training-list.component";


// Route Configuration
export const hrTrainingsRoutes: Routes = [
  {
    path: '',
    component: TrainingListComponent,
    canActivate: [AuthGuard]
  }
];
