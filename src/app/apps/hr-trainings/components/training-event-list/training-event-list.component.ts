import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent,} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {Site} from "../../../_common/services/sites.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {Title} from "@angular/platform-browser";
import {EventAttendee, HrEventExport, HrEventService} from "../../../_common/services/hr-events.service";
import {HRProfile, HRProfileService} from "../../../_common/services/hr-profiles.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {MessageService} from "../../../../shared/material/message/message.service";
import {AutocompleteComponent} from "../../../../shared/material/autocomplete/autocomplete.component";
import {User} from "../../../_common/services/users.service";
import {Training} from "../../../_common/services/training.service";
import {TrainingUser} from "../../../_common/services/training-user.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

let TABLEACTIONGETATTACHMENTS = new TableRowAction();
TABLEACTIONGETATTACHMENTS.icon = 'visibility';
TABLEACTIONGETATTACHMENTS.tooltip = 'See attachments';

// export let TRAINING_USER_STATUS_CREATED = {pk:41,__str__:"CREATED"}; // Status link pk "CREATED for TRAINING USER"

@Component({
  selector: 'trainig-event-list',
  templateUrl: 'training-event-list.component.html',
  styleUrls: ['training-event-list.component.css']
})

export class TrainingEventListComponent extends BaseComponent implements OnInit {

  event: HrEventExport = new HrEventExport();
  eventCreateOpen: boolean;
  onCreate: boolean;
  isFullDay: boolean;
  currentUser: User = new User();
  userPartecipant: HRProfile = new HRProfile();
  userPartecipants: HRProfile[] = [];
  sites: Site[] = [];
  employees: HRProfile[] = [];
  nameTimeout: any;
  availableEventsActions = [];
  @ViewChild('hrEventsTrainingTable') hrEventsTrainingTable: TableWithPagesComponent;
  @ViewChild('userPartecipantEventAc') userPartecipantEventAc: AutocompleteComponent;
  noItemsMessageEvents = this.tr("NO_EVENTS");
  headerConfEvents = [
    /*    {
          label: this.tr("ORGANIZER"),
          valueKey: 'organizer',
        },*/
    {
      label: this.tr("TITLE"),
      valueKey: 'title',
    },

    {
      label: this.tr("START_DATE"),
      valueFunction: (item: HrEventExport) => {
        if (item['start_date']) {
          let date = new Date(item['start_date']);
          return date.toString()
        }
        else {
          return null;
        }
      }
    },
    {
      label: this.tr("END_DATE"),
      valueFunction: (item: HrEventExport) => {
        if (item['start_date']) {
          let date = new Date(item['start_date']);
          return date.toString()
        }
        else {
          return null;
        }
      }
    },
    {
      label: this.tr("FULL_DAY"),
      valueKey: 'full_day_event',
    },
    {
      label: this.tr("STATUS"),
      valueKey: 'status',
    }
  ];
  @ViewChild('currentAttachments') currentAttachments: DialogComponent;
  eventSelected: HrEventExport = new HrEventExport();

  format: string = "xlsx";
  montEventFilter: string;
  @Input() training: Training;
  @Input() trainingUsers: TrainingUser[];

  constructor(private loginService: LoginService,
              protected translationService: TranslationService,
              private titleService: Title,
              private hrEventService: HrEventService,
              private hrProfileService: HRProfileService,
              protected messageService: MessageService,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Trainings");
    this.eventCreateOpen = false;
    this.onCreate = false;
    this.isFullDay = false;
  }

  ngOnInit() {
    this.currentUser = this.loginService.user;
    this.initTableEventsActions();
  }

  getAttachments(item: HrEventExport) {

    this.eventSelected = cloneObject(item);
    this.currentAttachments.open();
  }

  initTableEventsActions() {

    let actionEditTrainingEvent: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditTrainingEvent.click = this.editTrainingEvent.bind(this);

    let actionGetAttachments: TableRowAction = cloneObject(TABLEACTIONGETATTACHMENTS);
    actionGetAttachments.click = this.getAttachments.bind(this);
    actionGetAttachments.visibilityCondition = (item: HrEventExport) => {
      return item.attachments.length > 0;
    };

    this.availableEventsActions = [actionEditTrainingEvent, actionGetAttachments];
  }

  loadHrEvents(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    this.setFiltersEvents(query);

    return this.hrEventService.getHrEvents(query);
  }

  setFiltersEvents(filters?: any) {

    let query = (filters) ? filters : {};

    query['is_active'] = true;
    query['ordering'] = "start_date";
    query['user_sites'] = this.loginService.user.pk;
    query['training'] = this.training.pk;

    if (this.montEventFilter) {
      query['year_month'] = this.montEventFilter;
    }
    return query;
  }

  downloadEvents() {
    this.loadingService.show();
    let query = {};
    this.setFiltersEvents(query);
    this.hrEventService.getHrEvents(query, this.format).subscribe(() => {
      this.loadingService.hide();
    });
  }

  monthFilterEventChange() {
    if (this.nameTimeout)
      clearTimeout(this.nameTimeout);
    this.nameTimeout = setTimeout(() => {
      this.loadHrEvents(1, 10);
    }, 300)
  }

  createEvent() {
    this.event = new HrEventExport();
    this.event.user_obj = new HRProfile();
    let currentDate = new Date();
    this.event.start_date = currentDate.toISOString();
    this.event.end_date = currentDate.toISOString();
    this.eventCreateOpen = true;
    this.onCreate = true;
    this.event.full_day_event = this.isFullDay;
    this.event.attendees = [];
    this.initEventAttendees();
  }

  initEventAttendees() {
    // proposal attendees
    this.trainingUsers = this.trainingUsers.filter((x) => x.is_active == true);

    for (let trainingUser of this.trainingUsers) {
      if (trainingUser.user_profile.calendar && trainingUser.user_profile.calendar.__str__) {
        let eventAttendee = new EventAttendee();
        eventAttendee.user_profile = trainingUser.user_profile;
        eventAttendee.user = trainingUser.user_profile;
        eventAttendee.mail = trainingUser.user_profile.calendar.__str__;
        eventAttendee.organizer = false;
        this.event.attendees.push(eventAttendee)
      }
    }
  }

  saveEvent() {

    //this.event.calendar_google_id = this.event.user_obj.calendar? this.event.user_obj.calendar.__str__ : null;
    this.event.calendar_google_id = this.loginService.user.calendar ? this.loginService.user.calendar.__str__ : null;
    this.event.organizer = this.loginService.user.calendar ? this.loginService.user.calendar.__str__ : null;
    this.event.training = this.training.pk;
    this.event.status = "pending"; //

    // TODO TYPE "GENERIC" ??

    this.event.attendees_calendars = this.event.attendees.map((item) => {
      return item.mail
    });

    this.hrEventService.saveHrEvent(this.event).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        // notify parent on save event
        this.createEvent();
        this.eventCreateOpen = false;
      })
    })

  }

  loadUsersPartecipants(keyword?: string) {
    let query = {};
    query['is_active'] = true;
    query['profiles_by_hr'] = this.loginService.user.pk;

    if (keyword) {
      query['search'] = keyword;
    }

    this.hrProfileService.getEmployeesProfile(query).subscribe((response) => {
      this.userPartecipants = parseSerionetRespToData(response);
    });
  }

  selectedUserPartecipant(user: HRProfile) {
    if (user && user.pk) {
      let attendees = cloneObject(this.event.attendees);
      let findUser = attendees.filter((x) => x.user.pk == user.pk);
      if (findUser.length == 0) {
        this.userPartecipant = user;
      } else {
        this.messageService.open({
          title: " ",
          message: "User is already a participant!"
        });
        this.userPartecipantEventAc.reset();
      }
    }
    else {
      this.userPartecipant = null;
    }
  }

  addEventAttendee() {

    if (this.userPartecipant.calendar && this.userPartecipant.calendar.__str__) {
      let eventAttendee = new EventAttendee();
      eventAttendee.user = this.userPartecipant;
      eventAttendee.user_profile = this.userPartecipant;
      eventAttendee.mail = this.userPartecipant.calendar.__str__;
      eventAttendee.organizer = false;

      this.event.attendees.push(eventAttendee);
      this.userPartecipant = new HRProfile();
      this.userPartecipantEventAc.reset();

    } else {
      this.messageService.open({
        title: " ",
        message: "User haven't calendar"
      });
    }

  }

  deleteEventAttendee(item: EventAttendee) {

    this.event.attendees = this.event.attendees.filter((x) => x.user != item.user);
  }

  editTrainingEvent(trainingEvent: HrEventExport) {
    this.event = cloneObject(trainingEvent);
    this.eventCreateOpen = true;
    this.onCreate = false;
    this.event.attendees = this.event.attendees ? this.event.attendees : [];
    this.isFullDay = this.event.full_day_event;
  }

  undoTrainingEvent() {
    this.eventCreateOpen = false;
  }

  resetFilters() {
    this.montEventFilter = null;
    this.monthFilterEventChange();
  }

}
