import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {TrainingEventListComponent} from "./training-event-list.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [TrainingEventListComponent],   // components and directives
  exports:[TrainingEventListComponent],
  providers: []                    // services
})
export class TrainingEventListModule { }


