import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {
  TableWithPagesComponent,
  TableRowActions, TableRowAction
} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, buildErrorMessage, truncateWithEllipsis, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TrainingArea, TrainingAreaService} from "../../../_common/services/training-area.service";
import {Training, TrainingService} from "../../../_common/services/training.service";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {LocalPackageItem, LocalPackagesService} from "../../../_common/services/localpackages.service";
import {HRUser, HRUserService} from "../../../_common/services/hr-users.service";
import {MeasureUnit, MeasureUnitService} from "../../../_common/services/measure-unit.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {Title} from "@angular/platform-browser";
import {TrainingUser, TrainingUserService} from "../../../_common/services/training-user.service";
import {HRProfile, HRProfileService} from "../../../_common/services/hr-profiles.service";
import {StatusLink, StatusLinkService} from "../../../_common/services/status-link.service";
import {PriorityLink, PriorityLinkService} from "../../../_common/services/priority-link.service";
import {TrainingEventListComponent} from "../training-event-list/training-event-list.component";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {UserSearchComponent} from "../../../../shared/components/user-search/user-search.component";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

export let TYPE_UOM_TIME = "T"; // UOM TYPE FIELD == T (TIME)
export let TRAINING = 67; // DjangoContentType model Training PK
export let TRAINING_TYPE = 222; // DjangoContentType model TrainingType PK
export let TRAINING_USER = 223; // DjangoContentType model TrainingUser PK
export let TRAINING_USER_STATUS_CREATED = {pk: 41, __str__: "CREATED"}; // Status link pk "CREATED for TRAINING USER"

export class TrainingUserPartecipant {
  trainingUser?: TrainingUser;
  partecipant?: boolean;
}

@Component({
  selector: 'trainingtype-list',
  templateUrl: 'training-list.component.html',
  styleUrls: ['training-list.component.css']
})

export class TrainingListComponent extends BaseComponent implements OnInit {
  trainingCreateOpen: boolean;
  editTrainingRelated: boolean;
  training: Training = new Training();
  trainingFilter: Training;
  search: string;
  trainingAreas: TrainingArea[] = [];
  sitePackageItems: LocalPackageItem[] = [];
  uoms: MeasureUnit[] = [];
  nameTimeout: any;
  active: boolean = true;
  isExternalTrainer: boolean;
  availableActions = [];
  trainingCT: number = TRAINING;
  trainingUserCT: number = TRAINING_USER;
  @ViewChild('trainingEventList') trainigEventList: TrainingEventListComponent;
  @ViewChild('trainingTable') trainingTable: TableWithPagesComponent;
  @ViewChild('userPartecipantAc') userPartecipantAc: UserSearchComponent;
  noItemsMessageTrainings = this.tr("NO_TRAINING");
  headerConfTrainings = [
    {
      label: this.tr("NAME"),
      valueKey: 'name'
    },
    {
      label: this.tr("DESCRIPTION"),
      valueFunction: (item: Training)=> {
        if (item["description"]) {
          return truncateWithEllipsis(item["description"], ["5"])
        } else {
          return "-"
        }
      }
    },
    {
      label: this.tr("TYPE"),
      valueFunction: (item: Training) => {
        if (item.type && item.site && item.site.__str__) {
          return item.type.__str__ + "(" + item.site.__str__ + ")"
        }
      }
    },
    {
      label: this.tr("DURATION"),
      valueFunction: (item: Training) =>{
        if (item.duration_uom && item.duration_uom.__str__) {
          return item.duration + "(" + item.duration_uom.__str__ + ")"
        }
      }
    },
    {
      label: this.tr("TRAINER"),
      valueFunction: (item: Training) =>{
        if (item.trainer_ext) {
          return item.trainer_ext
        } else {
          if (item.trainer && item.trainer.__str__) {
            return item.trainer.__str__
          }
        }
      }
    },
    {
      label: this.tr("TRAINER_ORGANIZATION"),
      valueKey: 'trainer_organization',
      useStr: true
    },
    {
      label: this.tr("NOTES"),
      valueFunction: (item: Training) => {
        if (item["notes"]) {
          return truncateWithEllipsis(item["notes"], ["5"])
        } else {
          return "-"
        }
      }
    },
  ];
  trainingUsers: TrainingUser[] = [];
  trainingUserPartecipants: TrainingUserPartecipant[] = [];
  trainingUser: TrainingUser = new TrainingUser();
  userPartecipant: HRProfile = new HRProfile();
  onEditTrainingUser: boolean;
  openDisabledTrainingUserParticipants: boolean;
  statusLinks: StatusLink[] = [];
  priorityLinks: PriorityLink[] = [];

  constructor(private trainingService: TrainingService,
              private trainingAreaService: TrainingAreaService,
              private sitePackageItemService: LocalPackagesService,
              private organizationService: OrganizationService,
              private measureUnitService: MeasureUnitService,
              private hrUserService: HRUserService,
              private hrProfileService: HRProfileService,
              protected translationService: TranslationService,
              private sitesService: SitesService,
              private trainingUserService: TrainingUserService,
              private loginService: LoginService,
              private statusLinkService: StatusLinkService,
              private priorityLinkService: PriorityLinkService,
              protected messageService: MessageService,
              private titleService: Title,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Trainings");
    this.trainingFilter = new Training();
    this.trainingCreateOpen = false;
    this.isExternalTrainer = false;
    this.onEditTrainingUser = false;
    this.openDisabledTrainingUserParticipants = false;
    this.editTrainingRelated = false;
  }

  ngOnInit() {
    this.loadTrainingAreas();
    this.loadSitePackageItems();
    this.loadMeasureUnits();
    this.initTableActions();
  }

  loadTrainingAreas() {
    this.trainingAreaService.getTrainingAreas({is_active: true}).subscribe((data) => {
        this.trainingAreas = data['results']
      }
    )
  }

  loadStatusLinks() {
    let query = {};
    query['is_active'] = true;
    query['fieldname'] = "status";
    query['app'] = TRAINING_USER;

    this.statusLinkService.getStatusLinks(query).subscribe((data) => {
      this.statusLinks = parseSerionetRespToData(data);
    })
  }

  loadPriorityLinks() {
    let query = {};
    query['is_active'] = true;
    query['fieldname'] = "final_mark";
    query['app'] = TRAINING_USER;

    this.priorityLinkService.getPriorityLinks(query).subscribe((data) => {
      this.priorityLinks = parseSerionetRespToData(data);
    })
  }

  loadTrainingUsers() {
    let query = {};
    query['training'] = this.training.pk;

    this.trainingUserService.getTrainingUsers(query).subscribe((data) => {
        this.trainingUsers = parseSerionetRespToData(data);
        this.trainingUserPartecipants = this.trainingUsers.map((item: TrainingUser) => {
          let trainingUserPartecipant = new TrainingUserPartecipant();
          trainingUserPartecipant.partecipant = item.is_active;
          trainingUserPartecipant.trainingUser = item;
          return trainingUserPartecipant;
        })
      }
    )
  }

  loadSitePackageItems() {
    let query = {};
    query['is_active'] = true;
    query['content_type'] = TRAINING_TYPE;

    this.sitesService.getSites({user: this.loginService.user.pk, is_active: true}).subscribe((response) => {
      if (response['count'] && response['count'] > 0) {

        query['site__in'] = response['results'].map((item: Site)=> {
          return item.pk

        }).join(",");

        this.sitePackageItemService.getLocalPackages(query).subscribe((data) => {
          this.sitePackageItems = parseSerionetRespToData(data);
        })
      }
    })
  }

  loadMeasureUnits() {
    this.measureUnitService.getMeasureUnits({is_active: true, type: TYPE_UOM_TIME}).subscribe((data) => {
      this.uoms = parseSerionetRespToData(data);
    });
  }

  loadTrainerOrganizations(keyword?: string) {
    let query = {};
    query['is_active'] = true;

    if (keyword) {
      query['search'] = keyword;
    }

    query['results'] = 500;

    return this.organizationService.getOrganizations(query);
  }

  loadTrainers(keyword?: string) {
    let query = {};
    query['is_active'] = true;

    if (keyword) {
      query['search'] = keyword;
    }

    query['results'] = 5;

    return this.hrUserService.getHrUsers(query);
  }

  loadUsersPartecipants(keyword?: string) {
    let query = {};
    query['is_active'] = true;
    query['profiles_by_hr'] = this.loginService.user.pk;

    if (keyword) {
      query['search'] = keyword;
    }

    query['results'] = 5;

    return this.hrProfileService.getEmployeesProfile(query);
  }

  selectedTrainer(user: HRUser) {
    if (user && user.pk) {
      this.training.trainer = user;
    }
    else {
      this.training.trainer = null;
    }
  }

  selectedUserPartecipant(user: HRProfile) {
    if (user && user.pk) {
      let trainingUserPartecipants: TrainingUser[] = cloneObject(this.trainingUsers);
      let findUser = trainingUserPartecipants.filter((x) => x.user_profile.pk == user.pk);
      if (findUser.length == 0) {
        this.userPartecipant = user;
      } else {
        this.messageService.open({
          title: " ",
          message: "User is already a participant!"
        });
        this.userPartecipantAc.reset();
      }
    }
    else {
      this.userPartecipant = new HRProfile();
    }
  }

  selectedTrainerOrganization(organization: Organization) {
    if (organization && organization.pk) {
      this.training.trainer_organization = organization;
    }
    else {
      this.training.trainer_organization = null;
    }
  }

  initTableActions() {
    let actionEditTraining: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditTraining.click = this.editTraining.bind(this);
    let actionDeleteTraining: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteTraining.click = this.deleteTraining.bind(this);
    actionDeleteTraining.visibilityCondition = (item: Training) => {
      return item.is_active;
    };
    let actionRestoreTraining: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreTraining.click = this.restoreTraining.bind(this);
    actionRestoreTraining.visibilityCondition = (item: Training) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditTraining, actionRestoreTraining, actionDeleteTraining];
  }

  loadTrainings(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.search) {
      query['search'] = this.search;
    }

    return this.trainingService.getTrainings(query);
  }

  searchChange() {
    if (this.nameTimeout)
      clearTimeout(this.nameTimeout);
    this.nameTimeout = setTimeout(() => {
      this.trainingTable.reload();
    }, 300)
  }

  deleteTraining(training: Training) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.trainingService.deleteTraining(training).subscribe((resp) => {
          this.manageRespDelete(resp, ()=> {
            this.trainingTable.reload();
          }
        )
          ;
        })
      },
      deny: () => {

      }
    });
  }

  restoreTraining(training: Training) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.trainingService.restoreTraining(training).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.trainingTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveTraining() {
    this.trainingService.saveTraining(this.training).subscribe((resp) => {

      if (resp.pk) {
        this.messageService.open({
          message: this.tr('SAVE_COMPLETED'),
          title: " ",
          autoClose: false,
        });
        //this.training = cloneObject(resp);
        //this.editTrainingRelated = true;
        this.createTraining();
        this.trainingCreateOpen = false;
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  saveUserParticipants() {
    // let self = this;
    // SET TRAINING FOR TRAINING USERS
    // this.trainingUserPartecipants.map(function (item:TrainingUserPartecipant) {
    //   if (!item.trainingUser.training){
    //     item.trainingUser.training = this.training;
    //   }
    //   return item;
    // });

    // IF UPDATE A TRAINING USER REPLACE IT ON TRAINING-USER-PARTECIPANTS
    // if(this.onEditTrainingUser){
    //   let trainingUsers = this.trainingUsers.filter((x) => x.pk == this.trainingUser.pk);
    //   if (trainingUsers.length > 0){
    //     let index = this.trainingUsers.findIndex((x) => x..pk == this.trainingUser.pk);
    //     trainingUsers[0].trainingUser = this.trainingUser;
    //     this.trainingUserPartecipants[index] = trainingUsers[0];
    //   }
    // }

    this.saveTrainingUsers();
  }

  createTraining() {
    this.training = new Training();
    this.training.type = new LocalPackageItem();
    this.training.duration_uom = new MeasureUnit();
    this.trainingCreateOpen = true;
    this.trainingUserPartecipants = [];
    this.trainingUser = new TrainingUser();
    this.trainingUser.status = new StatusLink();
    this.trainingUser.final_mark = new PriorityLink();
    this.trainingUser.time_attendee_uom = new MeasureUnit();
    this.onEditTrainingUser = false;
    this.editTrainingRelated = false;
  }

  editTraining(training: Training) {
    this.training = cloneObject(training);
    this.trainingCreateOpen = true;
    this.openDisabledTrainingUserParticipants = false;
    this.trainingUserPartecipants = [];
    this.isExternalTrainer = this.training.trainer_ext != null;
    this.loadTrainingUsers();
    this.editTrainingRelated = true;
  }

  editOnlyTraining() {
    this.editTrainingRelated = false;
  }

  undoTraining() {
    this.createTraining();
    this.trainingCreateOpen = false;
  }

  addTrainingUserPartecipant() {
    let trainingUser = new TrainingUser();
    trainingUser.user = this.userPartecipant;
    trainingUser.status = TRAINING_USER_STATUS_CREATED;
    trainingUser.user_profile = this.userPartecipant;
    trainingUser.is_active = true;
    trainingUser.training = this.training;
    this.trainingUsers.push(trainingUser);
    this.userPartecipant = new HRProfile();
    this.userPartecipantAc.reset();
  }

  editTrainingUser(item: TrainingUser) {

    this.loadStatusLinks();
    this.loadPriorityLinks();
    this.trainingUser = item;
    this.trainingUser.status = (this.trainingUser.status && this.trainingUser.status.pk) ? this.trainingUser.status : new StatusLink();
    this.trainingUser.time_attendee_uom = (this.trainingUser.time_attendee_uom && this.trainingUser.time_attendee_uom.pk) ? this.trainingUser.time_attendee_uom : new MeasureUnit();
    this.trainingUser.final_mark = (this.trainingUser.final_mark && this.trainingUser.final_mark.pk) ? this.trainingUser.final_mark : new PriorityLink();
    // this.trainingUser.date_completed = "2016-12-01";
    this.onEditTrainingUser = true;
  }

  undoEditTrainingUser() {
    this.onEditTrainingUser = false;
    this.trainingUser = new TrainingUser();
  }

  deleteTrainingUser(item: TrainingUser) {
    if (item.pk)
      item.is_active = false;
    else
      this.trainingUsers.splice(this.trainingUsers.indexOf(item), 1);
  }

  restoreTrainingUser(item: TrainingUser) {
    item.is_active = true;
  }

  saveTrainingUsers() {

    this.loadingService.show();
    this.processTrainingUser(this.trainingUsers, 0, [], (failedSaves: TrainingUser[]) => {
      this.loadingService.hide();
      if (failedSaves.length == 0) {
        this.messageService.open({
          message: this.tr('SAVE_COMPLETED'),
          title: " ",
          autoClose: true
        });
        this.loadTrainingUsers();
      }
      else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.buildFailedSavesMessage(failedSaves),
          autoClose: false
        });
      }
    })
  }

  buildFailedSavesMessage(failedSaves: TrainingUser[]) {
    let content = [];
    content.push("<div>The following users were not saved correctly:</div>");
    content.push("<ul>");
    failedSaves.forEach((item: TrainingUser) => {
      content.push("<li><b>" + item.user.__str__ + "</b><br/>" + JSON.stringify(item.errorResponse) + "</li>");
    });
    content.push("</ul>");
    return content.join("");
  }

  processTrainingUser(trainingUsers: TrainingUser[], index, failedSaves, callback) {
    if (trainingUsers[index]) {
      let trainingUser = trainingUsers[index];

      //Gestendo manualmente l'is_active, la save si occupa sia di aggiornare, che di creare che di eliminare gli elementi

      this.trainingUserService.saveTrainingUser(trainingUser).subscribe((resp) => {
        if (!resp.pk) {
          trainingUser.errorResponse = resp;
          failedSaves.push(trainingUser);
        }
        this.processTrainingUser(trainingUsers, index + 1, failedSaves, callback);
      });
    }
    else {
      callback.call(this, failedSaves);
    }
  }

  saveTrainingUser() {
    this.trainingUserService.saveTrainingUser(this.trainingUser).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.undoEditTrainingUser();
      })
    })
  }

  onTabClick() {
    // Reset tabs on click
    // reset Users tab
    this.editTraining(this.training);
    // reset trainingEventList component
    this.trainigEventList.createEvent();
    this.trainigEventList.undoTrainingEvent();
    this.trainigEventList.resetFilters();

  }

}
