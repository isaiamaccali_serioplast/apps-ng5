import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {TrainingListComponent} from "./training-list.component";
import {TrainingEventListModule} from "../training-event-list/training-event-list.module";
import {DateAdapter} from "@angular/material";
import {SPDateAdapter} from "../../../../shared/material/date/date-adapter";

@NgModule({
  imports: [SharedModule,TrainingEventListModule],       // module dependencies
  declarations: [TrainingListComponent],   // components and directives
  exports:[TrainingListComponent],
  providers: [{provide: DateAdapter, useClass: SPDateAdapter}]                    // services
})
export class TrainingListModule { }


