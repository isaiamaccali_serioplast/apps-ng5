import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {PackagesIndexComponent} from "./components/packages-index/packages-index.component";

// Route Configuration
export const hrDashboardRoutes: Routes = [
  {
    path: '',
    component: PackagesIndexComponent,
    canActivate: [AuthGuard]
  }
];
