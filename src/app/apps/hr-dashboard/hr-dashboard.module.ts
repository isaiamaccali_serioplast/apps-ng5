import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {PackagesIndexModule} from "./components/packages-index/packages-index.module";
import {hrDashboardRoutes} from "./hr-dashboard.routes";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(hrDashboardRoutes),PackagesIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class HRDashboardModule {}

