import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {ClockProfilesComponent} from "./clock-profiles.component";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [ClockProfilesComponent],   // components and directives
  exports: [ClockProfilesComponent],
  providers: []                    // services
})
export class ClockProfilesModule {
}


