import {Component, OnInit, ViewChild} from "@angular/core";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../shared/utils";
import {ClockProfile, ClockProfileService} from "../../../_common/services/clock-profile.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'clock-profiles',
  templateUrl: 'clock-profiles.component.html',
  styleUrls: ['clock-profiles.component.css']
})
export class ClockProfilesComponent extends BaseComponent implements OnInit {

  profilesActive = true;
  profileCreateOpen = false;
  clockProfile: ClockProfile;

  @ViewChild('profilesTable') profilesTable: TableWithPagesComponent;
  noItemsMessageSubtypes = this.tr("NO_CLOCK_PROFILES_AVAILABLE");
  headerConfSubtypes = [
    {
      label: this.tr("NAME"),
      valueKey: 'name'
    },
    {
      label: this.tr("SHIFT"),
      valueKey: 'shift',
      isBoolean: true
    },
    {
      label: this.tr("OVERTIME"),
      valueKey: 'overtime',
      isBoolean: true
    },
    {
      label: this.tr("BREAK"),
      valueKey: 'break_period',
      isBoolean: true
    },
    {
      label: this.tr("START_TIME"),
      valueKey: 'start_time'
    },
    {
      label: this.tr("END_TIME"),
      valueKey: 'end_time'
    },
    {
      label: this.tr("WORKING_HOURS"),
      valueKey: 'tot_work_hours'
    }
  ];
  availableActions = [];

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private snackBar: MatSnackBar, private clockProfileService: ClockProfileService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions() {
    let actionEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEdit.click = this.editClockProfile.bind(this);
    actionEdit.visibilityCondition = (item: ClockProfile) => {
      return item.is_active;
    };
    let actionDelete: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDelete.click = this.deleteClockProfile.bind(this);
    actionDelete.visibilityCondition = (item: ClockProfile) =>{
      return item.is_active;
    };
    let actionRestore: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestore.click = this.restoreClockProfile.bind(this);
    actionRestore.visibilityCondition = (item: ClockProfile) =>{
      return !item.is_active;
    };
    this.availableActions = [actionEdit, actionDelete, actionRestore];
  }

  saveClockProfile() {
    let displayMsg = false;
    if (this.clockProfile.is_active == false) {
      displayMsg = true;
    }
    this.clockProfileService.saveClockProfile(this.clockProfile).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.profileCreateOpen = false;
        this.profilesTable.reload();
        this.createClockProfile();
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  editClockProfile(d: ClockProfile) {
    this.clockProfile = cloneObject(d);
    this.profileCreateOpen = true;
  }

  undoEditClockProfile() {
    this.createClockProfile();
    this.profileCreateOpen = false;
  }

  deleteClockProfile(d: ClockProfile) {

    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.clockProfileService.deleteClockProfile(d).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.profilesTable.reload();
          });
        })
      },
      deny: () =>{

      }
    });
  }

  restoreClockProfile(s: ClockProfile) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.clockProfileService.restoreClockProfile(s).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.profilesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  createClockProfile() {
    this.clockProfile = new ClockProfile();
  }

  loadClockProfiles(page: number, results: number) {

    let query = {};
    query['is_active'] = this.profilesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    return this.clockProfileService.getClockProfiles(query);
  }
}

