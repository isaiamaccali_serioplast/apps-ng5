import {Component, OnInit, Input, ViewChild} from "@angular/core";
import {LoggingService} from "../../../../../shared/logging/logging.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {CompanyPositionName} from "../../../../_common/services/company-position-name.service";
import {LocalCpnPackageItem} from "../../../../_common/services/localcpnpackageitem.service";

@Component({
  selector: 'cpnpackage-dialog',
  templateUrl: 'cpnpackage-dialog.component.html',
  styleUrls: ['cpnpackage-dialog.component.css']
})

export class CpnPackageDialogComponent implements OnInit {

  @ViewChild('currentCpnPackages') currentCpnPackages:DialogComponent;
  @Input() cpn: CompanyPositionName;
  localCpnPackagesItems: LocalCpnPackageItem[];

  constructor(private loggerservice: LoggingService) {
    this.cpn = new CompanyPositionName();
    this.localCpnPackagesItems = [];
  }

  ngOnInit() {
    this.localCpnPackagesItems = this.cpn.get_sitecpnpackageitems;
  }

  openDialog(){
    this.currentCpnPackages.open();
  }
}

