import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {CpnPackageDialogComponent} from "./cpnpackage-dialog.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [CpnPackageDialogComponent],   // components and directives
  exports:[CpnPackageDialogComponent],
  providers: []                    // services
})
export class CpnPackageDialogModule { }
