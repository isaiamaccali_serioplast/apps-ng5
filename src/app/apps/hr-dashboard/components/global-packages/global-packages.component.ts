import {Component, OnInit, ViewChild} from "@angular/core";
import {LoggingService} from "../../../../shared/logging/logging.service";
import {Department, DepartmentService} from "../../../_common/services/department.service";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {CompanyPositionName, CompanyPositionNameService} from "../../../_common/services/company-position-name.service";
import {PackageName, PackagesNameService} from "../../../_common/services/packages-name.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'global-packages',
  templateUrl: 'global-packages.component.html',
  styleUrls: ['global-packages.component.css']
})

export class GlobalPackagesComponent extends TranslatableComponent implements OnInit {
  @ViewChild('filterPackagesNames') filterPackagesNames: DialogComponent;

  companypositionNames: CompanyPositionName[] = [];
  companypositionNamesToSave: CompanyPositionName[] = [];
  packages: PackageName[] = [];
  packageNames: PackageName[] = [];
  organizations: Organization[] = [];
  packagesAll: PackageName[] = [];
  departments: Department[] = [];
  loading: Boolean;
  organization_ref: number;
  department_ref: number;
  packageName: number;
  departmentDisabled: boolean;
  changeDept: boolean;
  deptPack: boolean;

  constructor(private loggerservice: LoggingService, private companyPositionNameService: CompanyPositionNameService,
              private packagesNameService: PackagesNameService, private organizationservice: OrganizationService,
              private departmentservice: DepartmentService, private loadingService: LoadingService,
              protected translationService:TranslationService, private messageService:MessageService) {
    super(translationService);
    this.loading = false;
    this.departmentDisabled = true;
    this.changeDept = false;
    this.deptPack = false;
  }

  ngOnInit() {

    this.packagesNameService.getPackagesNames({is_active:true}).subscribe((data)=> {
      this.packagesAll = parseSerionetRespToData(data);
    });

    this.loadOrganizations();
    this.loadDepartments();
    // Load packageNames for filter

  }

  loadOrganizations() {
    let query = {};
    query['is_active'] = true;
    query['is_serioplast_group'] = 1;
    query['ordering'] = 'organization_ref';
    this.organizationservice.getOrganizations(query).subscribe((data)=> {
      this.organizations = parseSerionetRespToData(data);
    });
  }

  loadDepartments() {
    let query = {};
    query['is_active'] = true;
    if (this.organization_ref)
      query['organization_ref'] = this.organization_ref;

    query['ordering'] = 'organization_ref';
    this.departmentservice.getDeparments(query).subscribe((data)=> {
      this.departments = parseSerionetRespToData(data);
    });
  }

  organizationChange() {
    this.departmentDisabled = (this.organization_ref==null);
    this.department_ref = null;
    this.deptPack = false;
    this.loadDepartments();
  }

  deptChange() {
    this.changeDept = true;
    this.deptPack = false;
  }

  loadCompanyPositionNames() {
    let query = {};
    query['is_active'] = true;

    if (this.department_ref)
      query['department_ref'] = this.department_ref;

    query['ordering'] = 'name';
    this.companypositionNames = [];

    this.companyPositionNameService.getCompanyPositionNames(query).subscribe((data)=> {
      if(data && data['count'] && data['count']>0){
        this.renderCPN(data['results'], 5);
        this.changeDept = false;
      }
    });
  }

  renderCPN(cpn: CompanyPositionName[], groupSize: number) {
    let groups = [];
    let group = [];
    let groupIndex = 0;

    // Build groups
    // Add one cpn foreach group
    for (let i = 0; i < cpn.length; i++) {
      if (groupIndex < groupSize) {
        group.push(cpn[i]);
        groupIndex++;
      }
      else {
        groupIndex = 0;
        groups.push(group);
        group = [];
        i--;
      }
    }
    if (group.length > 0)
      groups.push(group);
    this.loadingService.show();
    this.printCPN(groups, 0, () => {
      this.loadingService.hide();
    })
  }

  printCPN(groups, index, callback) {
    if (groups[index]) {
      this.companypositionNames = this.companypositionNames.concat(groups[index]);

      setTimeout(() => {
        this.printCPN(groups, index + 1, callback)
      }, 200)
    }
    else {
      callback();
    }
  }

  filterCompanyPositionNames() {
    this.loadCompanyPositionNames();
    this.deptPack = true;
  }


  onChangePackageCheckbox(packagesSelected: PackageName[]) {
    this.packages = packagesSelected;
  }

  saveCompanyPositionNames() {
    this.processCPN(this.companypositionNamesToSave, 0, [], (failedSaves: CompanyPositionName[]) =>{
      if (failedSaves.length == 0) {
        this.companypositionNamesToSave = [];
        this.messageService.open({
          message: this.tr('SAVE_COMPLETED'),
          title: " ",
          autoClose: true
        });
      }
      else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.buildFailedSavesMessage(failedSaves),
          autoClose: false
        });
      }
    })
  }

  buildFailedSavesMessage(failedSaves:CompanyPositionName[]){
    let content = [];
    content.push("<div>The following packages were not saved correctly:</div>");
    content.push("<ul>");
    failedSaves.forEach((cpn) => {
      content.push("<li><b>"+cpn.__str__+"</b><br/>"+JSON.stringify(cpn.errorResponse)+"</li>");
    });
    content.push("</ul>");
    return content.join("");
  }

  processCPN(companypositionNamesToSave,index,failedSaves,callback){
    if(companypositionNamesToSave[index]){
      let cpn = companypositionNamesToSave[index];
      this.companyPositionNameService.updateCompanyPositionName(cpn).subscribe((resp) => {
        if(!resp.pk){
          cpn.errorResponse = resp;
          failedSaves.push(cpn);
        }
        this.processCPN(companypositionNamesToSave,index+1,failedSaves,callback);
      });
    }
    else {
      callback.call(this,failedSaves);
    }
  }

  isPackageEnabled(cpn:CompanyPositionName, pn:PackageName){
    return cpn.packages.map((p:PackageName) =>{
        return p.pk
      }).indexOf(pn.pk)!=-1;
  }

  togglePackageName(cpn:CompanyPositionName, package_name:PackageName) {
    let index = cpn.packages.map((pn:PackageName) => {return pn.pk}).indexOf(package_name.pk);
    if(index==-1) {
     cpn.packages.push(package_name);
    } else {
     cpn.packages.splice(index, 1);
    }
    this.loggerservice.warn(cpn.name + " with packageNames: " +
      cpn.packages.map((p:PackageName) =>{
        return p.__str__||p.name;
      }).toString());
    let indexToSave = this.companypositionNamesToSave.map((cpn:CompanyPositionName)=>{return cpn.pk}).indexOf(cpn.pk);
    if(indexToSave==-1){
      this.companypositionNamesToSave.push(cpn);
    }
    else {
      this.companypositionNamesToSave[indexToSave] = cpn;
    }
  }
}
