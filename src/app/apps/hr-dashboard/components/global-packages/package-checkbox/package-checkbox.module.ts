import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {PackageCheckboxComponent} from "./package-checkbox.component";

@NgModule({
  imports: [SharedModule,],       // module dependencies
  declarations: [PackageCheckboxComponent],   // components and directives
  exports:[PackageCheckboxComponent],
  providers: []                    // services
})
export class PackageCheckboxModule { }
