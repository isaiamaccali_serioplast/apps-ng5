import {Component, OnInit, Input, EventEmitter, Output} from "@angular/core";
import {PackageName} from "../../../../_common/services/packages-name.service";

@Component({
  selector: 'package-checkbox',
  templateUrl: 'package-checkbox.component.html',
  styleUrls: ['package-checkbox.component.css']
})

export class PackageCheckboxComponent implements OnInit {

  @Input() package_name: PackageName;
  @Input() packagesNamesSelected: PackageName[];
  @Output() onChangePackageCheckbox = new EventEmitter<PackageName[]>();

  id: string;
  checked: boolean;

  constructor() {
    this.package_name = new PackageName();
    this.checked = false;
  }

  ngOnInit() {
    this.isChecked();
  }

  isChecked() {

    let contains = this.packagesNamesSelected.filter((x) => (x.pk) == this.package_name.pk);
    if (contains.length) {
      this.checked = true;
    }
  }

  isChange() {
    this.checked = !this.checked;

    if (this.checked) {
      this.packagesNamesSelected.push(this.package_name);
    } else {
      let index = this.packagesNamesSelected.indexOf(this.package_name);
      this.packagesNamesSelected.splice(index, 1);
    }

    this.onChangePackageCheckbox.emit(this.packagesNamesSelected);
  }
}

