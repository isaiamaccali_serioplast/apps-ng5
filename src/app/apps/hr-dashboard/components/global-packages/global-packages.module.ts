import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {GlobalPackagesComponent} from "./global-packages.component";
import {CpnPackageDialogModule} from "./cpnpackage-dialog/cpnpackage-dialog.module";
import {PackageCheckboxModule} from "./package-checkbox/package-checkbox.module";

@NgModule({
  imports: [SharedModule,CpnPackageDialogModule,PackageCheckboxModule],       // module dependencies
  declarations: [GlobalPackagesComponent],
  exports:[GlobalPackagesComponent],
  providers: []                    // services
})
export class GlobalPackagesModule { }
