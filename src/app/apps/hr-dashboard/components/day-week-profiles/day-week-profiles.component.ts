import {Component, OnInit, ViewChild} from "@angular/core";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../shared/utils";
import {DayWeekInterval, DayWeekProfile, DayWeekProfileService} from "../../../_common/services/day-week-profile-service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'day-week-profiles',
  templateUrl: 'day-week-profiles.component.html',
  styleUrls: ['day-week-profiles.component.css']
})
export class DayWeekProfilesComponent extends BaseComponent implements OnInit {

  profilesActive = true;
  profileCreateOpen = false;
  dayWeekProfile: DayWeekProfile;

  @ViewChild('profilesTable') profilesTable: TableWithPagesComponent;
  noItemsMessageSubtypes = this.tr("NO_DAY_WEEK_PROFILES_AVAILABLE");
  headerConfSubtypes = [
    {
      label: this.tr("NAME"),
      valueKey: 'name'
    }
  ];
  availableActions = [];

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private snackBar: MatSnackBar, private dayWeekProfileService: DayWeekProfileService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions() {
    let actionEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEdit.click = this.editDayWeekProfile.bind(this);
    actionEdit.visibilityCondition = (item: DayWeekProfile) => {
      return item.is_active;
    };
    let actionDelete: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDelete.click = this.deleteDayWeekProfile.bind(this);
    actionDelete.visibilityCondition = (item: DayWeekProfile) => {
      return item.is_active;
    };
    let actionRestore: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestore.click = this.restoreDayWeekProfile.bind(this);
    actionRestore.visibilityCondition = (item: DayWeekProfile) => {
      return !item.is_active;
    };
    this.availableActions = [actionEdit, actionDelete, actionRestore];
  }

  saveDayWeekProfile() {
    let displayMsg = false;
    if (this.dayWeekProfile.is_active == false) {
      displayMsg = true;
    }
    this.dayWeekProfileService.saveDayWeekProfile(this.dayWeekProfile).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.profileCreateOpen = false;
        this.profilesTable.reload();
        this.createDayWeekProfile();
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  editDayWeekProfile(d: DayWeekProfile) {
    this.dayWeekProfile = cloneObject(d);
    this.profileCreateOpen = true;
  }

  undoEditDayWeekProfile() {
    this.createDayWeekProfile();
    this.profileCreateOpen = false;
  }

  deleteDayWeekProfile(d: DayWeekProfile) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.dayWeekProfileService.deleteDayWeekProfile(d).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.profilesTable.reload();
          });
        })
      },
      deny: () => {
      }
    });
  }

  restoreDayWeekProfile(s: DayWeekProfile) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.dayWeekProfileService.restoreDayWeekProfile(s).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.profilesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  createDayWeekProfile() {
    this.dayWeekProfile = new DayWeekProfile();
    this.dayWeekProfile.intervals = [];
  }

  loadDayWeekProfile(page: number, results: number) {

    let query = {};
    query['is_active'] = this.profilesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    return this.dayWeekProfileService.getDayWeekProfiles(query);
  }

  addInterval() {
    let i = new DayWeekInterval();
    i.holiday = false;
    this.dayWeekProfile.intervals.push(i);
  }

  deleteInterval(i: DayWeekInterval) {
    this.dayWeekProfile.intervals.splice(this.dayWeekProfile.intervals.indexOf(i), 1);
  }
}
