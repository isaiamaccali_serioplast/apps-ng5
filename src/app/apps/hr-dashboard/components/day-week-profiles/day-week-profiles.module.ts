import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {DayWeekProfilesComponent} from "./day-week-profiles.component";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [DayWeekProfilesComponent],   // components and directives
  exports: [DayWeekProfilesComponent],
  providers: []                    // services
})
export class DayWeekProfilesModule {
}


