import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {TrainingTypeListComponent} from "./trainingtype-list.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [TrainingTypeListComponent],   // components and directives
  exports:[TrainingTypeListComponent],
  providers: []                    // services
})
export class TrainingTypeListModule { }


