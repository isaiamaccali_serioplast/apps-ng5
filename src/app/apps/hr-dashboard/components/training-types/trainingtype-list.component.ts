import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {MatSnackBar} from "@angular/material";
import {TrainingType, TrainingTypeService} from "../../../_common/services/training-type.service";
import {TrainingArea, TrainingAreaService} from "../../../_common/services/training-area.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'trainingtype-list',
  templateUrl: 'trainingtype-list.component.html',
  styleUrls: ['trainingtype-list.component.css']
})

export class TrainingTypeListComponent extends BaseComponent implements OnInit {
  trainingTypeCreateOpen: boolean;
  onCreate: boolean;
  trainingType: TrainingType = new TrainingType();
  trainingTypeFilter: TrainingType;
  trainingAreas: TrainingArea[] = [];
  nameTimeout: any;
  active = true;
  availableActions = [];
  @ViewChild('trainingTypesTable') trainingTypesTable: TableWithPagesComponent;
  noItemsMessageTrainingTypes = this.tr("NO_TRAINING_TYPES");
  headerConfTrainingTypes = [
    {
      columnId: 'name',
      label: this.tr("NAME"),
      valueKey: 'label'
    },
    {
      columnId: 'area',
      label: this.tr("AREA"),
      valueKey: 'area',
      useStr: true
    },
  ];

  constructor(private trainingTypeService: TrainingTypeService,
              private trainingAreaService: TrainingAreaService,
              protected translationService: TranslationService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.trainingTypeFilter = new TrainingType();
    this.trainingTypeCreateOpen = false;
    this.onCreate = false;
  }

  ngOnInit() {
    this.loadTrainingAreas();
    this.initTableActions();
  }

  loadTrainingAreas() {
    this.trainingAreaService.getTrainingAreas({is_active: true}).subscribe((data) => {
      this.trainingAreas = parseSerionetRespToData(data)
    })
  }

  initTableActions() {
    let actionEditTrainingType: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditTrainingType.click = this.editTrainingType.bind(this);
    let actionDeleteTrainingType: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteTrainingType.click = this.deleteTrainingType.bind(this);
    actionDeleteTrainingType.visibilityCondition = (item: TrainingType) => {
      return item.is_active;
    };
    let actionRestoreTrainingType: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreTrainingType.click = this.restoreTrainingType.bind(this);
    actionRestoreTrainingType.visibilityCondition = (item: TrainingType) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditTrainingType, actionRestoreTrainingType, actionDeleteTrainingType];
  }

  loadTrainingTypes(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.trainingTypeFilter.label)
      query['search'] = this.trainingTypeFilter.label;

    if (this.trainingTypeFilter.area)
      query['area'] = this.trainingTypeFilter.area;

    return this.trainingTypeService.getTrainingTypes(query);
  }

  labelChange() {
    if (this.nameTimeout)
      clearTimeout(this.nameTimeout);
    this.nameTimeout = setTimeout(() => {
      this.trainingTypesTable.reload();
    }, 300)
  }

  deleteTrainingType(trainingType: TrainingType) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.trainingTypeService.deleteTrainingType(trainingType).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.trainingTypesTable.reload();
          });
        })
      },
      deny: () => {
      }
    });
  }

  restoreTrainingType(trainingType: TrainingType) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.trainingTypeService.restoreTrainingType(trainingType).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.trainingTypesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveTrainingType() {
    let displayMsg = false;
    if (this.trainingType.is_active == false) {
      displayMsg = true;
    }

    this.trainingTypeService.saveTrainingType(this.trainingType).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createTrainingType();
        this.trainingTypeCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  createTrainingType() {
    this.trainingType = new TrainingType();
    this.trainingType.area = new TrainingArea();
    this.trainingTypeCreateOpen = true;
    this.onCreate = true;
  }

  editTrainingType(trainingType: TrainingType) {
    this.trainingType = cloneObject(trainingType);
    this.trainingTypeCreateOpen = true;
    this.onCreate = false;
  }

  undoTrainingType() {
    this.createTrainingType();
    this.trainingTypeCreateOpen = false;
  }

}
