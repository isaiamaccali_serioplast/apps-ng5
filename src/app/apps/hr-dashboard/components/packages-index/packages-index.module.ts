import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {PackagesIndexComponent} from "./packages-index.component";
import {PackageItemModule} from "../package-item/package-item.module";
import {GlobalPackagesModule} from "../global-packages/global-packages.module";
import {JobTitlesModule} from "../job-titles/job-titles.module";
import {SkillListModule} from "../skill-list/skill-list.module";
import {CpnSkillsModule} from "../cpn-skills/cpn-skills.module";
import {EventSubtypesModule} from "../event-subtypes/event-subtypes.module";
import {EventSubtypesLinksModule} from "../event-subtype-links/event-subtypes-links.module";
import {TrainingTypeListModule} from "../training-types/trainingtype-list.module";
import {DayWeekProfilesModule} from "../day-week-profiles/day-week-profiles.module";
import {ClockProfilesModule} from "../clock-profiles/clock-profiles.module";

@NgModule({
  imports: [SharedModule, JobTitlesModule, PackageItemModule, GlobalPackagesModule, SkillListModule,EventSubtypesLinksModule,EventSubtypesModule,TrainingTypeListModule,DayWeekProfilesModule,ClockProfilesModule],       // module dependencies
  declarations: [PackagesIndexComponent],   // components and directives
  exports: [PackagesIndexComponent],
  providers: []                    // services
})
export class PackagesIndexModule {
}


