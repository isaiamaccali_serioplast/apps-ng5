import {Component, OnInit, ViewChild} from "@angular/core";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Title} from "@angular/platform-browser";
import {MatTabGroup} from "@angular/material";
import {PackageName} from "../../../_common/services/packages-name.service";

@Component({
  selector: 'app-packages-index',
  templateUrl: 'packages-index.component.html',
  styleUrls: ['packages-index.component.css']
})

export class PackagesIndexComponent implements OnInit {
  @ViewChild('tab') tab: MatTabGroup;

  constructor(private sideNavService: SideNavService, private titleService: Title) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - HR Dashboard");
  }

  ngOnInit() {
  }

  onSavePackage(packageName: PackageName) {
    //
  }

  selectedIndex = 0;

  handleTabChange(){
    this.selectedIndex = this.tab.selectedIndex;
  }
}
