import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, truncateWithEllipsis} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {MatSnackBar} from "@angular/material";
import {Skill, SkillsService} from "../../../_common/services/skill.service";
import {SkillType, SkillTypeService} from "../../../_common/services/skill-types.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'skill-list',
  templateUrl: 'skill-list.component.html',
  styleUrls: ['skill-list.component.css']
})

export class SkillListComponent extends BaseComponent implements OnInit {
  skillCreateOpen: boolean;
  onCreate: boolean;
  skill: Skill = new Skill();
  skillFilter: Skill;
  nameTimeout: any;
  active = true;
  availableActions = [];
  skillTypes: SkillType[] = [];
  @ViewChild('skillsTable') skillsTable: TableWithPagesComponent;
  noItemsMessageSkills = this.tr("NO_SKILLS");
  headerConfSkills = [
    {
      columnId: 'name',
      label: this.tr("NAME"),
      valueKey: 'name'
    },
    {
      columnId: 'type',
      label: this.tr("TYPE"),
      valueKey: 'type'
    },
    {
      columnId: 'description',
      label: this.tr("DESCRIPTION"),
      valueFunction: (item: Skill) => {
        if (item["description"]) {
          return truncateWithEllipsis(item["description"], ["5"])
        } else {
          return "-"
        }
      }
    },
  ];

  constructor(private skillTypeService: SkillTypeService,
              protected loadingService: LoadingService,
              private skillservice: SkillsService,
              protected translationService: TranslationService,
              protected messageService: MessageService,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.skillFilter = new Skill();
    this.skillCreateOpen = false;
    this.onCreate = false;
  }

  ngOnInit() {
    this.loadSkillTypes();
    this.initTableActions();
  }

  loadSkillTypes() {
    this.skillTypeService.getSkillTypes({is_active: true}).subscribe((data) => {

        for (let i = 0; i < data.length; i++) {
          let type = new SkillType();
          type.value = data[i][0];
          type.label = data[i][1];
          this.skillTypes[i] = type;
        }
        // Remove empty label
        this.skillTypes.shift();
      }
    )
  }

  initTableActions() {
    let actionEditSkill: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditSkill.click = this.editSkill.bind(this);
    let actionDeleteSkill: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteSkill.click = this.deleteSkill.bind(this);
    actionDeleteSkill.visibilityCondition = (item: Skill) => {
      return item.is_active;
    };
    let actionRestoreSkill: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreSkill.click = this.restoreSkill.bind(this);
    actionRestoreSkill.visibilityCondition = (item: Skill) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditSkill, actionRestoreSkill, actionDeleteSkill];
  }

   loadSkills(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.skillFilter.name)
      query['name_icontains'] = this.skillFilter.name;

    if (this.skillFilter.type)
      query['type'] = this.skillFilter.type;

    return this.skillservice.getSkills(query);
  }

  codeChange() {
    if (this.nameTimeout)
      clearTimeout(this.nameTimeout);
    this.nameTimeout = setTimeout(() => {
      this.skillsTable.reload();
    }, 300)
  }

  deleteSkill(wh: Skill) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.skillservice.deleteSkill(wh).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.skillsTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreSkill(wh: Skill) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.skillservice.saveSkill(wh).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.skillsTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveSkill() {
    let displayMsg = false;
    if (this.skill.is_active == false) {
      displayMsg = true;
    }

    this.skillservice.saveSkill(this.skill).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createSkill();
        this.skillCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  createSkill() {
    this.skill = new Skill();
    this.skillCreateOpen = true;
    this.onCreate = true;
  }

  editSkill(wh: Skill) {
    this.skill = cloneObject(wh);
    this.skillCreateOpen = true;
    this.onCreate = false;
  }

  undoSkill() {
    this.createSkill();
    this.skillCreateOpen = false;
  }

}
