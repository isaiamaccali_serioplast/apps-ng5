import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SkillListComponent} from "./skill-list.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SkillListComponent],   // components and directives
  exports:[SkillListComponent],
  providers: []                    // services
})
export class SkillListModule { }


