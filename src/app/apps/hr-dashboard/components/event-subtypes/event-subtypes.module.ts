import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {EventSubtypesComponent} from "./event-subtypes.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {TableWithPagesModule} from "../../../../shared/material/table-with-pages/table-with-pages.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule, TableWithPagesModule],       // module dependencies
  declarations: [EventSubtypesComponent],   // components and directives
  exports: [EventSubtypesComponent],
  providers: []                    // services
})
export class EventSubtypesModule {
}


