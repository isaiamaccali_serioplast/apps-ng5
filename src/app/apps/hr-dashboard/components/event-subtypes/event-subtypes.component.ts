import {Component, EventEmitter, Input, OnInit, Output, SimpleChange, ViewChild} from "@angular/core";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {HREventSubtype, HREventSubtypesService} from "../../../_common/services/hr-event-subtype.service";
import {cloneObject} from "../../../../shared/utils";
import {TypeLink} from "../../../_common/services/type-link.service";
import {MaterialSubType} from "../../../_common/services/material-subtype.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'event-subtypes',
  templateUrl: 'event-subtypes.component.html',
  styleUrls: ['event-subtypes.component.css']
})
export class EventSubtypesComponent extends BaseComponent implements OnInit {

  @Input() complete = true;

  eventsActive = true;
  eventCreateOpen = false;
  eventSubtype: HREventSubtype;

  @ViewChild('subtypesTable') subtypesTable: TableWithPagesComponent;
  noItemsMessageSubtypes = this.tr("NO_SUBTYPES_AVAILABLE");
  headerConfSubtypes = [
    {
      columnId: 'name',
      label: this.tr("NAME"),
      valueKey: 'label'
    }
  ];
  availableActionsSubtypes = [];

  @Output() subtypeSelected = new EventEmitter();

  @Input() typeToFilter: TypeLink;
  @Input() countryToFilter: string;

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private snackBar: MatSnackBar, private eventSubtypesService: HREventSubtypesService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions() {
    let actionEditST: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditST.click = this.editEventSubtype.bind(this);
    let actionDeleteST: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteST.click = this.deleteEventSubtype.bind(this);
    actionDeleteST.visibilityCondition = (item: HREventSubtype) => {
      return item.is_active;
    };
    let actionRestoreST: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreST.click = this.restoreEventSubtype.bind(this);
    actionRestoreST.visibilityCondition = (item: MaterialSubType) => {
      return !item.is_active;
    };
    if (this.complete)
      this.availableActionsSubtypes = [actionEditST, actionDeleteST, actionRestoreST];
  }

  saveEventSubtype() {
    let displayMsg = false;
    if (this.eventSubtype.is_active == false) {
      displayMsg = true;
    }
    this.eventSubtypesService.saveEventSubtype(this.eventSubtype).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.eventCreateOpen = false;
        this.subtypesTable.reload();
        this.createSubtype();
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  editEventSubtype(s: HREventSubtype) {
    this.eventSubtype = s;
    this.eventCreateOpen = true;
  }

  undoEditSubtype() {
    this.createSubtype();
    this.eventCreateOpen = false;
  }

  deleteEventSubtype(s: HREventSubtype) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.eventSubtypesService.deleteEventSubtype(s).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.subtypesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreEventSubtype(s: MaterialSubType) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.eventSubtypesService.restoreEventSubtype(s).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.subtypesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  createSubtype() {
    this.eventSubtype = new HREventSubtype();
  }

  loadEventSubtypes(page: number, results: number) {

    let query = {};
    query['is_active'] = this.eventsActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    if (this.typeToFilter && this.countryToFilter) {
      query['not_app_type'] = this.typeToFilter.pk + "|" + this.countryToFilter;
      //query['not_app_type'] = this.typeToFilter.pk;
    }

    return this.eventSubtypesService.getEventSubtypes(query);
  }

  selectSubtypeFromClick(ms: MaterialSubType) {
    this.subtypeSelected.emit(ms);
  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['countryToFilter']) {
      this.countryToFilter = changes['countryToFilter'].currentValue;
    }
  }
}

