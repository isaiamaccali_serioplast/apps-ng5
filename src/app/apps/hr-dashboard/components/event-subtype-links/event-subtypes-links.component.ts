import {Component, OnInit, ViewChild} from "@angular/core";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSubtypesLinkService} from "../../../../_common/services/material-subtype-link.service";
import {MainMaterialLinksService} from "../../../../_common/services/main-material-links.service";
import {CountriesService} from "../../../_common/services/countries.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {ContentTypeService} from "../../../_common/services/content-type.service";
import {TypeLink, TypeLinkService} from "../../../_common/services/type-link.service";
import {HREventSubtypeLink, HREventSubtypesLinkService} from "../../../_common/services/hr-event-subtype-link.service";
import {HREventSubtype} from "../../../_common/services/hr-event-subtype.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";
import {LoadingService} from "../../../../shared/loading/loading.service";

@Component({
  selector: 'event-subtypes-links',
  templateUrl: 'event-subtypes-links.component.html',
  styleUrls: ['event-subtypes-links.component.css']
})
export class EventSubtypesLinksComponent extends BaseComponent implements OnInit {

  countries: any[] = [];
  eventSubtypeLink = new HREventSubtypeLink();
  editOpen = false;
  @ViewChild('subtypeLinkSubtypeDialog') subtypeLinkSubtypeDialog: DialogComponent;

  eventTypes: TypeLink[] = [];

  subtypeDisabled = false;

  constructor(private eventSubtypeLinksService: HREventSubtypesLinkService, protected translationService: TranslationService,
              protected messageService: MessageService, private countriesService: CountriesService,
              private contentTypeService: ContentTypeService, private typeLinkService: TypeLinkService, protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.countriesService.getCountries({plants_only: 1}).subscribe((data) => {
      this.countries = data;
    });

    this.contentTypeService.getContentTypes({model: 'event', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct = data['results'][0];
        this.typeLinkService.getTypeLinks({app: ct.pk, fieldname: 'type', is_active: true}).subscribe((data) => {
          this.eventTypes = parseSerionetRespToData(data);
        })
      }
    })
  }

  createEventSubtypeLink(estl: HREventSubtypeLink) {
    this.eventSubtypeLink = estl;
    this.editOpen = true;
    this.subtypeDisabled = false;
  }

  editEventSubtypeLink(estl: HREventSubtypeLink) {
    this.eventSubtypeLink = estl;
    this.editOpen = true;
  }

  undoCreateSubtypeLink() {
    this.editOpen = false;
  }

  selectedSubtypeSubtypeLink(s: HREventSubtype) {
    let s_ = this.subtypeLinkSubtypeDialog.onClose.subscribe(() => {
      if (s) {
        this.eventSubtypeLink.subtype_id = cloneObject(s);
      }
      else {
        this.eventSubtypeLink.subtype_id = null;
        this.subtypeDisabled = true;
      }
      s_.unsubscribe();
    });
    this.subtypeLinkSubtypeDialog.close();
  }

  saveEventSubtypeLink() {
    this.eventSubtypeLink.delta_rel = 0;
    this.eventSubtypeLinksService.saveEventSubtypeLink(this.eventSubtypeLink).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.editOpen = false;
      })
    })
  }

  handleCountryChange() {
    this.eventSubtypeLink.subtype_id = null;
    this.subtypeDisabled = true;
    setTimeout(() => {
      this.subtypeDisabled = false;
    }, 500)
  }

}

