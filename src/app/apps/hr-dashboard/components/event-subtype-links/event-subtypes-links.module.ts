import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {EventSubtypesLinksCardComponent} from "./event-subtype-links-card/event-subtypes-links-card.component";
import {EventSubtypesLinksComponent} from "./event-subtypes-links.component";
import {EventSubtypesModule} from "../event-subtypes/event-subtypes.module";

@NgModule({
  imports: [SharedModule,EventSubtypesModule],       // module dependencies
  declarations: [EventSubtypesLinksComponent,EventSubtypesLinksCardComponent],   // components and directives
  exports: [EventSubtypesLinksComponent,EventSubtypesLinksCardComponent],
  providers: []                    // services
})
export class EventSubtypesLinksModule {}


