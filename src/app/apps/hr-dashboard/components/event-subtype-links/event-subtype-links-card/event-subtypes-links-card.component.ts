import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {HREventSubtypeLink, HREventSubtypesLinkService} from "../../../../_common/services/hr-event-subtype-link.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../../shared/utils";
import {TypeLink} from "../../../../_common/services/type-link.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'event-subtypes-links-card',
  templateUrl: 'event-subtypes-links-card.component.html',
  styleUrls: ['event-subtypes-links-card.component.css']
})
export class EventSubtypesLinksCardComponent extends BaseComponent implements OnInit {

  @Input() type: TypeLink;
  @Output() editEventSubtypeLink = new EventEmitter();
  @Output() createEventSubtypeLink = new EventEmitter();

  subtypeLinksActive = true;

  @ViewChild('table') table: TableWithPagesComponent;

  noItemsMessageSubtypeLinks = this.tr("NO_SUBTYPES_LINKS_AVAILABLE");
  headerConfSubtypeLinks = [
    {
      columnId: 'subtype',
      label: this.tr("SUBTYPE"),
      valueKey: 'subtype_id',
      useStr: true
    },
    {
      columnId: 'country',
      label: this.tr("COUNTRY"),
      valueKey: 'country'
    },
    {
      columnId: 'short_description',
      label: this.tr("LOCAL_NAME"),
      valueKey: 'short_description'
    },
    {
      columnId: 'hasdocs',
      label: this.tr("HAS_DOCS"),
      valueKey: 'has_docs',
      isBoolean: true
    }
  ];
  availableActionsSubtypeLinks = [];

  constructor(private eventSubtypeLinksService: HREventSubtypesLinkService, protected translationService: TranslationService,
              protected messageService: MessageService, protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions() {
    let actionEditST: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditST.click = this.editSubtypeLink.bind(this);
    let actionDeleteST: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteST.click = this.deleteSubtypeLink.bind(this);
    actionDeleteST.visibilityCondition = (item: HREventSubtypeLink) => {
      return item.is_active;
    };
    let actionRestoreST: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreST.click = this.restoreSubtype.bind(this);
    actionRestoreST.visibilityCondition = (item: HREventSubtypeLink) => {
      return !item.is_active;
    };
    this.availableActionsSubtypeLinks = [actionEditST, actionDeleteST, actionRestoreST];
  }

  editSubtypeLink(s: HREventSubtypeLink) {
    this.editEventSubtypeLink.emit(s);
  }

  deleteSubtypeLink(s: HREventSubtypeLink) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.eventSubtypeLinksService.deleteEventSubtypeLink(s).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.table.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreSubtype(s: HREventSubtypeLink) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.eventSubtypeLinksService.restoreEventSubtypeLink(s).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.table.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  createSubtypeLink() {
    let eventSubtypeLink = new HREventSubtypeLink();
    eventSubtypeLink.type = this.type;
    eventSubtypeLink.has_docs = false;
    this.createEventSubtypeLink.emit(eventSubtypeLink)
  }

  loadEventSubtypeLinks(page: number, results: number) {
    let query = {};
    query['is_active'] = this.subtypeLinksActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['type'] = this.type.pk;
    return this.eventSubtypeLinksService.getEventSubtypeLinks(query);
  }

  reload() {
    this.table.reload();
  }
}
