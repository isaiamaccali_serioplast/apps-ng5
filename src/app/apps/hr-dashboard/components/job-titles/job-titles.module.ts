import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {JobTitlesComponent} from "./job-titles.component";
import {CpnSkillsListModule} from "../cpn-skills-list/cpn-skills-list.module";
import {SerioplastCommonModule} from "../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [JobTitlesComponent],   // components and directives
  exports:[JobTitlesComponent],
  providers: []                    // services
})
export class JobTitlesModule { }


