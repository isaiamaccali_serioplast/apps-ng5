import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {HeaderConf, TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, fixValue, parseSerionetRespToData, truncateWithEllipsis} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {Department, DepartmentService} from "../../../_common/services/department.service";
import {OrganizationLevel, OrganizationLevelService} from "../../../_common/services/organization-level.service";
import {CompanyPositionHierarchy, CompanyPositionHierarchyService} from "../../../_common/services/company-position-hierarchy.service";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {CompanyPositionName, CompanyPositionNameService} from "../../../_common/services/company-position-name.service";
import {Skill, SkillsService} from "../../../_common/services/skill.service";
import {CompanyPositionNameSkill, CompanyPositionNameSkillsService} from "../../../_common/services/company-position-name-skill.service";
import {PriorityLink, PriorityLinkService} from "../../../_common/services/priority-link.service";
import {ContentType, ContentTypeService} from "../../../_common/services/content-type.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {Area, AreasService} from "../../../_common/services/areas.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

export class CpnSkillStatus {
  cpn_skill: CompanyPositionNameSkill;
  status: string;
}

@Component({
  selector: 'job-titles',
  templateUrl: 'job-titles.component.html',
  styleUrls: ['job-titles.component.css']
})

export class JobTitlesComponent extends BaseComponent implements OnInit {
  companyPositionNameCreateOpen: boolean;
  onCreate: boolean;
  companyPositionName: CompanyPositionName = new CompanyPositionName();
  companyPositionHierarchy: CompanyPositionHierarchy = new CompanyPositionHierarchy();
  organizationFilter: Organization = new Organization();
  companyPositionNames: CompanyPositionName[] = [];
  organizations: Organization[] = [];
  organizationsLevels: OrganizationLevel[] = [];
  departments: Department[] = [];
  departmentsAll: Department[] = [];
  companyPositionNameFilter: CompanyPositionName;
  nameTimeout: any;
  active: boolean = true;
  availableActions = [];
  @ViewChild('companyPositionNamesTable') companyPositionNamesTable: TableWithPagesComponent;
  noItemsMessagecompanyPositionNames = this.tr("No job titles.");
  headerConfcompanyPositionNames: HeaderConf[] = [
    {
      columnId: 'name',
      label: this.tr("Name"),
      valueKey: 'name'
    },
    {
      columnId: 'job_description',
      label: this.tr("Job Description"),
      valueFunction: (item: CompanyPositionName) => {
        if (item["job_description"]) {
          return truncateWithEllipsis(item["job_description"], ["5"])
        } else {
          return "-"
        }
      }
    },
    {
      columnId: 'department',
      label: this.tr("Department"),
      valueKey: 'department_ref',
      useStr: true
    },
    {
      label: this.tr("AREA"),
      valueKey: 'area_ref',
      useStr: true
    },
    {
      label: this.tr("Org. Level"),
      valueKey: 'org_level',
      useStr: true
    },
    {
      columnId: 'report_to',
      label: this.tr("Report to"),
      valueFunction: (obj: CompanyPositionName) => {
        return (obj.get_company_position_hierarchy) ? obj.get_company_position_hierarchy.__str__ : "-";
      }
    },
  ];
  skills: Skill[] = [];

  cpnSkills: CpnSkillStatus[] = [];
  cpnSkillStatus: CpnSkillStatus = new CpnSkillStatus();
  priorityLinks: PriorityLink[] = [];
  cpnSkillEditOpen: boolean;

  companyPositionHierarchyOrganization: number;
  companyPositionHierarchyDepartment: number;
  companyPositionNamesHierarchy: CompanyPositionName[] = [];
  departmentsHierarchy: Department[] = [];
  organizationsHierarchy: Organization[] = [];

  areas: Area[] = [];
  areasFilter: Area[] = [];

  constructor(private companyPositionNameService: CompanyPositionNameService, private skillService: SkillsService,
              private companyPositionNameSkillsService: CompanyPositionNameSkillsService,
              private companypositionhierarchyservice: CompanyPositionHierarchyService,
              private organizationservice: OrganizationService, private departmentservice: DepartmentService,
              private orglevelservice: OrganizationLevelService, protected translationService: TranslationService,
              private priorityLinkService: PriorityLinkService, protected messageService: MessageService,
              private contentTypeService: ContentTypeService, protected loadingService: LoadingService,
              private areaService: AreasService){
    super(translationService, messageService, loadingService);
    this.companyPositionNameFilter = new CompanyPositionName();
    this.companyPositionNameFilter.department_ref = new Department();
    this.companyPositionNameFilter.area_ref = new Area();
    this.companyPositionNameFilter.org_level = new OrganizationLevel();
    this.companyPositionNameCreateOpen = false;
    this.onCreate = false;
  }

  ngOnInit() {
    this.loadCompanyPositionNamesAll();
    this.loadOrganizations();
    this.loadDepartments();
    this.loadOrganizationsLevels();
    this.initTableActions();
    this.loadPriorityLinks();
    this.loadSkills();
  }

  loadPriorityLinks() {

    this.contentTypeService.getContentTypes({model: 'skillassessment', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.priorityLinkService.getPriorityLinks({app: ct.pk, is_active: true}).subscribe((data) => {
          this.priorityLinks = parseSerionetRespToData(data);
        });
      }
    });
  }

  loadSkills() {
    this.skillService.getSkills({is_active: true}).subscribe((data) => {
      this.skills = parseSerionetRespToData(data);
    })
  }

  loadDepartments() {
    this.departmentservice.getDeparments({is_active: true, ordering: 'organization_ref'}).subscribe((data) => {
      this.departmentsAll = parseSerionetRespToData(data);
      this.departments = this.departmentsAll;
    })
  }

  loadOrganizations() {
    this.organizationservice.getOrganizations({is_active: true, is_serioplast_group: true}).subscribe((data) => {
      this.organizations = parseSerionetRespToData(data);
      this.organizationsHierarchy = parseSerionetRespToData(data);
    })
  }

  loadOrganizationsLevels() {
    this.orglevelservice.getOrganizationLevels({is_active: true}).subscribe((data) => {
      this.organizationsLevels = parseSerionetRespToData(data);
    });
  }

  initTableActions() {
    let actionEditCompanyPositionName: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditCompanyPositionName.click = this.editCompanyPositionName.bind(this);
    let actionDeleteCompanyPositionName: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteCompanyPositionName.click = this.deleteCompanyPositionName.bind(this);
    actionDeleteCompanyPositionName.visibilityCondition = (item: CompanyPositionName) => {
      return item.is_active;
    };
    let actionRestoreCompanyPositionName: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreCompanyPositionName.click = this.restoreCompanyPositionName.bind(this);
    actionRestoreCompanyPositionName.visibilityCondition = (item: CompanyPositionName) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditCompanyPositionName, actionRestoreCompanyPositionName, actionDeleteCompanyPositionName];
  }

  loadCompanyPositionNames(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.companyPositionNameFilter.name)
      query['name'] = this.companyPositionNameFilter.name;

    if (this.companyPositionNameFilter.job_description)
      query['job_description'] = this.companyPositionNameFilter.job_description;

    if (this.companyPositionNameFilter.department_ref && this.companyPositionNameFilter.department_ref.pk)
      query['department_ref'] = this.companyPositionNameFilter.department_ref.pk;

    if (this.areasFilter.length > 0 && this.companyPositionNameFilter.area_ref && this.companyPositionNameFilter.area_ref.pk)
      query['area_ref'] = this.companyPositionNameFilter.area_ref.pk;

    if (this.companyPositionNameFilter.org_level && this.companyPositionNameFilter.org_level.pk)
      query['org_level'] = this.companyPositionNameFilter.org_level.pk;

    return this.companyPositionNameService.getCompanyPositionNames(query);
  }

  /**
   * For Edit Hierarchy
   */
  loadCompanyPositionNamesAll() {
    let query = {};
    query['is_active'] = true;
    query['ordering'] = 'name';

    this.companyPositionNameService.getCompanyPositionNames(query).subscribe((data) => {
      this.companyPositionNames = parseSerionetRespToData(data);
    });
  }

  reload() {
    this.companyPositionNamesTable.reload();
  }

  reset(){
    setTimeout(()=>{
      this.reload();
    },500)
  }

  organizationFilterChange() {
    if(this.organizationFilter && this.organizationFilter.pk){
      if (this.companyPositionNameFilter) {
        this.companyPositionNameFilter.department_ref = new Department();
      }
      let query = {};
      query['is_active'] = true;

      query['organization_ref'] = this.organizationFilter.pk;

      this.departmentservice.getDeparments(query).subscribe((data) => {
        this.departments = parseSerionetRespToData(data);
      })
    }
    else {
      this.departments = [];
      this.companyPositionNameFilter.department_ref = new Department();
    }

  }

  deleteCompanyPositionName(cpn: CompanyPositionName) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.companyPositionNameService.deleteCompanyPositionName(cpn).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.companyPositionNamesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreCompanyPositionName(cpn: CompanyPositionName) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.companyPositionNameService.restoreCompanyPositionName(cpn).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.companyPositionNamesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  createCompanyPositionName() {
    this.companyPositionName = new CompanyPositionName();
    this.companyPositionName.department_ref = new Department();
    this.companyPositionName.org_level = new OrganizationLevel();
    this.companyPositionName.area_ref = new Area();
    this.companyPositionHierarchy = new CompanyPositionHierarchy();
    this.companyPositionNameCreateOpen = true;
    this.companyPositionHierarchyOrganization = null;
    this.companyPositionHierarchyDepartment = null;
    this.cpnSkills = [];
    this.onCreate = true;
  }

  setCompanyPositionHierarchy() {
    let query = {is_active: true};

    if (this.companyPositionName.pk) {
      query['company_position'] = this.companyPositionName.pk;
    }

    this.companypositionhierarchyservice.getCompanyPositionHierarchies(query).subscribe((resp) => {
      if (resp['results'] && resp['results'].length > 0) {
        this.companyPositionHierarchy = resp['results'][0];
        if (this.companyPositionHierarchy.parent) {

          // get object cpn hierarchy
          this.companypositionhierarchyservice.getCompanyPositionHierarchies({
            id: this.companyPositionHierarchy.parent,
            is_active: true
          }).subscribe((resp) => {
            if (resp['results'] && resp['results'].length > 0) {
              let parentCpn = resp['results'][0].company_position.pk;
              this.companyPositionNameService.getCompanyPositionName(parentCpn).subscribe((cpn: CompanyPositionName) => {
                this.companyPositionHierarchy.parent = cpn.pk;

                this.departmentservice.getDepartment(cpn.department_ref.pk).subscribe((d: Department) => {
                  this.companyPositionHierarchyOrganization = d.organization_ref.pk;
                  this.loadCPHDepartments(() => {
                    this.companyPositionHierarchyDepartment = cpn.department_ref.pk;
                    this.loadCPHCPN();
                  });
                });
              })
            }
          })
        }
        else {
          this.companyPositionHierarchyOrganization = null;
          this.companyPositionHierarchyDepartment = null;
        }
      }
    });
  }

  editCompanyPositionName(cpn: CompanyPositionName) {
    this.companyPositionName = cloneObject(cpn);
    if (!this.companyPositionName.area_ref)
      this.companyPositionName.area_ref = new Area();
    this.loadAreas();
    this.setCompanyPositionHierarchy();
    this.companyPositionNameCreateOpen = true;
    this.loadCompanyPositionNameSkills();
  }

  undoCompanyPositionName() {
    this.createCompanyPositionName();
    this.companyPositionNameCreateOpen = false;
  }

  loadCompanyPositionNameSkills() {
    let query = {};
    query['company_position_name'] = this.companyPositionName.pk;
    query['is_active'] = true;

    this.loadingService.show();
    this.companyPositionNameSkillsService.getSkills(query).subscribe((data) => {
      this.loadingService.hide();
      this.cpnSkills = parseSerionetRespToData(data).map((cpns: CompanyPositionNameSkill) => {
        let cpnSkillStatus = new CpnSkillStatus();
        cpnSkillStatus.cpn_skill = cpns;
        cpnSkillStatus.status = "";
        return cpnSkillStatus;
      })
    });
  }

  deleteCompanyPositionNameSkillStatus(cpnSkillStatus: CpnSkillStatus) {
    cpnSkillStatus.status = "to_delete";
    cpnSkillStatus.cpn_skill.is_active = false;
  }

  restoreCompanyPositionNameSkillStatus(cpnSkillStatus: CpnSkillStatus) {
    cpnSkillStatus.status = "to_save";
    cpnSkillStatus.cpn_skill.is_active = true;
  }

  saveCompanyPositionNameSkillStatus(cpnSkillStatus: CpnSkillStatus) {
    if (cpnSkillStatus.status == "new") {
      this.cpnSkills.push(cpnSkillStatus);
    }
    cpnSkillStatus.status = "to_save";
    this.undoEditCompanyPositionNameSkill();
  }

  createCompanyPositionNameSkill() {
    this.cpnSkillStatus = new CpnSkillStatus();
    this.cpnSkillStatus.cpn_skill = new CompanyPositionNameSkill();
    this.cpnSkillStatus.cpn_skill.company_position_name = this.companyPositionName;
    this.cpnSkillStatus.cpn_skill.level = new PriorityLink();
    this.cpnSkillStatus.cpn_skill.skill = new Skill();
    this.cpnSkillStatus.cpn_skill.is_active = true;
    this.cpnSkillStatus.status = "new";
    this.cpnSkillEditOpen = true;
  }

  editCompanyPositionNameSkillStatus(cpnSkillStatus: CpnSkillStatus) {
    this.cpnSkillStatus = cpnSkillStatus;
    this.cpnSkillEditOpen = true;
  }

  undoEditCompanyPositionNameSkill() {
    //this.createCompanyPositionNameSkill();
    this.cpnSkillEditOpen = false;
  }

  getActiveCPNSkills(): CpnSkillStatus[] {
    return this.cpnSkills.filter((cpn) => {
      return cpn.cpn_skill.is_active
    })
  }

  getInactiveCPNSkills(): CpnSkillStatus[] {
    return this.cpnSkills.filter((cpn) => {
      return !cpn.cpn_skill.is_active
    })
  }

  fixCpnSkillSkill() {
    fixValue(this.skills, this.cpnSkillStatus.cpn_skill, 'skill');
  }

  fixCpnSkillLevel() {
    fixValue(this.priorityLinks, this.cpnSkillStatus.cpn_skill, 'level');
  }

  saveCPN() {
    this.loadingService.show();
    // save companypositionname
    this.companyPositionNameService.saveCompanyPositionName(this.companyPositionName).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.companyPositionName.pk = resp.pk;
        let parent = this.companyPositionHierarchy.parent;

        // get companypositionhiearchy with parent null and pk valuated (on serionet are created after cpn by signal)
        let query = {};
        if (this.companyPositionName.pk) {
          query['company_position'] = this.companyPositionName.pk;
          query['is_active'] = true;
        }

        this.companypositionhierarchyservice.getCompanyPositionHierarchies(query).subscribe((resp) => {
          if (resp['results'].length > 0) {
            let companyPositionHierarchyParent = resp['results'][0];

            // get parent id
            this.companypositionhierarchyservice.getCompanyPositionHierarchies({
              company_position: parent,
              is_active: true
            }).subscribe((resp) => {

              if (resp['results'].length > 0) {
                companyPositionHierarchyParent.parent = resp['results'][0];
              } else {
                companyPositionHierarchyParent.parent = null;
              }

              this.companypositionhierarchyservice.saveCompanyPositionHierarchy(companyPositionHierarchyParent).subscribe((cph) => {
                this.manageRespSave(cph, () => {
                  this.saveSkills(() => {
                    this.loadingService.hide();
                    this.messageService.open({
                      message: this.tr('SAVE_COMPLETED'),
                      title: " ",
                      autoClose: true,
                      onClose: () => {
                        this.companyPositionNameCreateOpen = false;
                      }
                    });

                  })
                }, true);
              });
            });

          }
        });
      }, true);
    })
  }

  saveSkills(callback) {

    let cpnSkillsToSave = this.cpnSkills.filter((cpns) => cpns.status == "to_save").map((cpns) => cpns.cpn_skill);
    let cpnSkillsToDelete = this.cpnSkills.filter((cpns) => cpns.status == "to_delete" && cpns.cpn_skill.pk).map((cpns) => cpns.cpn_skill);

    this.saveCPNSkills(cpnSkillsToSave, 0, () => {
      this.deleteCPNSkills(cpnSkillsToDelete, 0, () => {
        callback.call(this);
      })
    });
  }

  saveCPNSkills(cpnSkillsToSave, index, callback) {
    if (cpnSkillsToSave[index]) {
      let cpns = cpnSkillsToSave[index];
      this.companyPositionNameSkillsService.saveCompanyPositionNameSkill(cpns).subscribe((resp) => {
        this.manageRespSave(resp, () => {
          this.saveCPNSkills(cpnSkillsToSave, index + 1, callback);
        }, true)
      })
    }
    else {
      callback.call(this);
    }
  }

  deleteCPNSkills(cpnSkillsToDelete, index, callback) {
    if (cpnSkillsToDelete[index]) {
      let cpns = cpnSkillsToDelete[index];
      this.companyPositionNameSkillsService.deleteCompanyPositionNameSkill(cpns).subscribe((resp) => {
        this.manageRespDelete(resp, () => {
          this.deleteCPNSkills(cpnSkillsToDelete, index + 1, callback);
        }, true)
      })
    }
    else {
      callback.call(this);
    }
  }

  loadCPHDepartments(callback?: any) {
    this.departmentservice.getDeparments({
      is_active: true,
      organization_ref: this.companyPositionHierarchyOrganization
    }).subscribe((data) => {
      this.departmentsHierarchy = parseSerionetRespToData(data);
      if (callback)
        callback.call(this);
    })
  }

  loadCPHCPN() {
    this.companyPositionNameService.getCompanyPositionNames({
      is_active: true,
      department_ref: this.companyPositionHierarchyDepartment
    }).subscribe((data) => {
      this.companyPositionNamesHierarchy = parseSerionetRespToData(data);
    })
  }

  loadAreas() {
    this.areaService.getAreas({
      is_active: true,
      department_ref: this.companyPositionName.department_ref.pk
    }).subscribe((resp) => {
      this.areas = parseSerionetRespToData(resp);
    })
  }

  loadAreasFilter() {
    if(this.companyPositionNameFilter.department_ref.pk){
      this.areaService.getAreas({
        is_active: true,
        department_ref: this.companyPositionNameFilter.department_ref.pk
      }).subscribe((resp) => {
        this.areasFilter = parseSerionetRespToData(resp);
        this.companyPositionNameFilter.area_ref = new Area();
      })
    }
    else {
      this.areasFilter = [];
      this.companyPositionNameFilter.area_ref = new Area();
    }

  }
}
