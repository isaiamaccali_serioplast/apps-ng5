import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {MatSnackBar} from "@angular/material";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {WorkToolType, WorkToolTypeService} from "../../../_common/services/worktooltype.service";
import {PackageName, PackagesNameService} from "../../../_common/services/packages-name.service";
import {PackageItem} from "../../../_common/services/packages-item.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'package-item',
  templateUrl: 'package-item.component.html',
  styleUrls: ['package-item.component.css']
})

export class PackageItemComponent extends BaseComponent implements OnInit {
  worktooltypes: WorkToolType[];
  packageNames: PackageName[];
  selectedPackageName: PackageName;
  formShow: boolean;
  packageName: PackageName;
  OpenAll: boolean;
  activePackages: boolean = true;
  @Output() onSavePackage = new EventEmitter<PackageName>();
  mode_edit: boolean;

  constructor(private worktooltypeservice: WorkToolTypeService, private packageNameService: PackagesNameService,
              private snackBar: MatSnackBar, protected messageService: MessageService,
              protected translationService: TranslationService, protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.worktooltypes = [];
    this.mode_edit = false;
  }

  ngOnInit() {
    this.loadWorkToolTypes();
    this.loadPackages();
    this.formShow = false;
    this.packageName = new PackageName();
    this.OpenAll = true;
  }

  changeOpentab() {
    this.OpenAll = false;
    setTimeout(()=>{
      this.OpenAll = true;
    },100)

    // this.loadWorkToolTypes();
  }

  loadWorkToolTypes() {
    this.loadingService.show();
    this.worktooltypeservice.getWorkToolTypes({is_active: true}).subscribe((data) => {
      this.loadingService.hide();
      this.worktooltypes = parseSerionetRespToData(data);
    });
  }

  loadPackages() {
    this.loadingService.show();
    this.packageNameService.getPackagesNames({is_active: this.activePackages}).subscribe((data) => {
      this.loadingService.hide();
      this.packageNames = parseSerionetRespToData(data);
      if (!this.selectedPackageName) {
        this.selectedPackageName = this.packageNames[0];
        this.selectedPackageName.selected = true;
      }
      else {
        this.packageNames.forEach((pn) => {
          if (pn.pk == this.selectedPackageName.pk) {
            pn.selected = true;
            this.selectedPackageName = pn;
          }
        })
      }
    });
  }

  selectPackage(selected_package: PackageName) {
    this.HideForm();
    this.packageNames.forEach((pn: PackageName) => {
      pn.selected = false;
    });
    selected_package.selected = true;
    this.selectedPackageName = selected_package;
  }

  showForm() {
    this.formShow = true;
  }

  HideForm() {
    this.formShow = false;
    this.packageName = new PackageName();
  }

  onEditPackageName(packageName: PackageName) {
    this.mode_edit = true;
    this.packageName = packageName;
    this.showForm();
  }

  createPackageName() {
    this.packageNameService.savePackageName(this.packageName).subscribe((resp) => {
      this.manageRespSave(resp, () => {

        this.loadPackages();
        if (this.mode_edit) {
          this.snackBar.open('PackageName succesfully updated', null, {
            duration: 3000
          });
          this.mode_edit = false;
          this.packageName = new PackageName();
        }
        else {
          this.snackBar.open('PackageName succesfully created', null, {
            duration: 3000
          });
        }

        this.HideForm();
      })
    });
  }

  // When enabled/disabled package
  onSavePackageObj(packagename: PackageName) {
    this.onSavePackage.emit(packagename);
  }

  selected_item(item: PackageItem) {
    if (this.selectedPackageName) {
      //I check whether the item is already added to the package or not (to avoid duplicates).
      let match = this.selectedPackageName.objects.filter((item_: PackageItem) => {
        return item_.object_id == item.object_id;
      });
      if (match.length == 0) {
        let itemToAdd = cloneObject(item);
        itemToAdd.package_name = cloneObject(this.selectedPackageName);
        itemToAdd.status = "toSave";
        this.selectedPackageName.objects.push(itemToAdd);
      }
      else {
        if (match[0].status != "toDelete")
          this.showMessage("Item already set on this package name");
        else {
          //The Item was removed and now restored. It will be saved in the end.
          match[0].status = "toSave";
          match[0].mandatory = false;
        }
      }
    }
    else {
      this.showMessage("Select a package name");
    }
  }

  showMessage(message: string) {
    this.messageService.open({
      message: message
    });
  }
}
