import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {PackageObjListComponent} from "./package-obj-list.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [PackageObjListComponent],   // components and directives
  exports:[PackageObjListComponent],
  providers: []                    // services
})
export class PackageObjListModule { }


