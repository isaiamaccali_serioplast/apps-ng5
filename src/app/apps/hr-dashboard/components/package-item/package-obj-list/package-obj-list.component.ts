import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange} from "@angular/core";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../../shared/utils";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {PackageName, PackagesNameService} from "../../../../_common/services/packages-name.service";
import {PackageItem, PackagesItemService} from "../../../../_common/services/packages-item.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'package-obj-list',
  templateUrl: 'package-obj-list.component.html',
  styleUrls: ['package-obj-list.component.css']
})

export class PackageObjListComponent extends BaseComponent implements OnInit, OnChanges {
  @Input('packageName') packageName: PackageName;
  @Output() selected_package = new EventEmitter<PackageName>();
  @Output() load_packages = new EventEmitter();
  @Output() onSavePackageObj = new EventEmitter<PackageName>();
  @Output() onEditPackageName = new EventEmitter<PackageName>();

  show: boolean = false;

  constructor(private packageNameService: PackagesNameService,
              protected messageService: MessageService, protected loadingService: LoadingService,
              private packagesItemService: PackagesItemService, protected translationService: TranslationService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
  }

  selectPackage(selected_package: PackageName) {
    this.selected_package.emit(selected_package);
  }

  removeItemFromPackage(pi: PackageItem) {
    pi.status = "toDelete";
  }

  updatePackageName(packageName: PackageName) {
    this.onEditPackageName.emit(cloneObject(packageName));
  }

  togglePackageName(pn: PackageName) {
    //The state is already changed when the function is called
    return (pn.is_active) ? this.restorePackageName(pn) : this.removePackageName(pn);
  }

  removePackageName(packageName: PackageName) {
    this.messageService.open({
      title: this.tr("ATTENTION"),
      message: this.tr("CONFIRM_DELETE"),
      confirm: () => {
        this.packageNameService.removePackageName(packageName).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.load_packages.emit();
          })
        });
      },
      deny: () => {
      }
    })

  }

  restorePackageName(packageName: PackageName) {
    this.messageService.open({
      title: this.tr("ATTENTION"),
      message: this.tr("RESTORE_CONFIRM"),
      confirm: () => {
        this.packageNameService.restorePackageName(packageName).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.load_packages.emit();
          })
        });
      },
      deny: () => {
      }
    })
  }

  togglePackageItemAsMandatory(pi: PackageItem) {
    pi.mandatory = !pi.mandatory;
    pi.status = "toSave";
  }

  savePackageNameItems(p: PackageName) {
    let piToManage = p.objects.filter((pi: PackageItem) => {
      return pi.status == "toSave" || pi.status == "toDelete";
    });

    this.processPackageItems(piToManage, 0, () => {
      this.messageService.open({
        message: this.tr('SAVE_COMPLETED'),
        title: " ",
        autoClose: true
      });
      this.load_packages.emit();
    })
  }

  processPackageItems(piToManage, index, callback) {
    if (piToManage[index]) {
      let pi = piToManage[index];
      if (pi.status == "toSave") {
        this.packagesItemService.savePackageItem(pi).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.processPackageItems(piToManage, index + 1, callback);
          }, true)
        })
      }
      else if (pi.status == "toDelete") {
        if (pi.pk) {
          this.packagesItemService.removePackageItem(pi).subscribe((resp) => {
            this.manageRespDelete(resp, () => {
              this.processPackageItems(piToManage, index + 1, callback);
            }, true)
          })
        }
        else {
          this.processPackageItems(piToManage, index + 1, callback);
        }

      }
    }
    else {
      callback.call(this);
    }
  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['packageName'])
      this.packageName = changes['packageName'].currentValue;
  }

}
