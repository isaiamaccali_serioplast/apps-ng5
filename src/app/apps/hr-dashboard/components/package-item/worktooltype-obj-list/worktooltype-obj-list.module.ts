import {NgModule} from "@angular/core";
import {WorkToolTypeCreateModule} from "./worktool-type-create/worktool-type-create.module";
import {DepartmentService} from "../../../_common/services/department.service";
import {WorkToolTypeObjListComponent} from "./worktooltype-obj-list.component";
import {SharedModule} from "../../../../../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [WorkToolTypeObjListComponent],   // components and directives
  exports:[WorkToolTypeObjListComponent],
  providers: []                    // services
})
export class WorkToolTypeObjListModule { }


