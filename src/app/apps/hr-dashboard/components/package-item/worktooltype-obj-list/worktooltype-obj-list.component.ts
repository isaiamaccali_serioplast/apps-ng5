import {Component, EventEmitter, Input, OnInit, Output, SimpleChange} from "@angular/core";
import {LoggingService} from "../../../../shared/logging/logging.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {WorkToolType} from "../../../../_common/services/worktooltype.service";
import {PackageItem} from "../../../../_common/services/packages-item.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'worktooltype-obj-list',
  templateUrl: 'worktooltype-obj-list.component.html',
  styleUrls: ['worktooltype-obj-list.component.css']
})

export class WorkToolTypeObjListComponent extends TranslatableComponent implements OnInit {
  @Input() worktooltype: WorkToolType;
  @Output() selected_item = new EventEmitter();
  @Input() OpenAll: boolean;
  show: boolean;
  packageitem: PackageItem = new PackageItem();

  constructor(protected translationService: TranslationService) {
    super(translationService);
  }

  ngOnInit() {
    this.show = this.OpenAll;
  }

  addObjToPackage(obj) {
    this.packageitem.type = this.worktooltype;
    this.packageitem.content_type = this.worktooltype.model_reference;
    this.packageitem.object_id = obj.pk;
    this.packageitem.__str__ = obj.__str__;
    this.selected_item.emit(this.packageitem);
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if(changes['OpenAll'])
      this.show = changes['OpenAll'].currentValue;
  }
}

