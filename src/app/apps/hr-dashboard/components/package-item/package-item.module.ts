import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {WorkToolTypeComponent} from "./worktool-type.component";
import {WorkToolTypeCreateModule} from "./worktool-type-create/worktool-type-create.module";
import {PackageItemComponent} from "./package-item.component";
import {WorkToolTypeObjListModule} from "./worktooltype-obj-list/worktooltype-obj-list.module";
import {PackageObjListModule} from "./package-obj-list/package-obj-list.module";

@NgModule({
  imports: [SharedModule,WorkToolTypeObjListModule,PackageObjListModule],       // module dependencies
  declarations: [PackageItemComponent],   // components and directives
  exports:[PackageItemComponent],
  providers: []                    // services
})
export class PackageItemModule { }


