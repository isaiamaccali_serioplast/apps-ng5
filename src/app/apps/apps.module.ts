import {NgModule} from "@angular/core";

let AppModules = [];

@NgModule({
  imports:AppModules,
  exports:AppModules,
  declarations: [],   // components and directives
  providers: []                    // services
})
export class AppsModule { }
