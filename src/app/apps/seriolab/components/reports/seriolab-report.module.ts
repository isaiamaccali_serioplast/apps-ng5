import {NgModule} from "@angular/core";
import {SeriolabReportIndexModule} from "./seriolab-report-index/seriolab-report-index.module";

@NgModule({
  imports: [SeriolabReportIndexModule],       // module dependencies
  exports:[SeriolabReportIndexModule],
})
export class SeriolabReportModule { }


