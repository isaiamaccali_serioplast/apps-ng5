"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var seriolab_report_index_module_1 = require("./seriolab-report-index/seriolab-report-index.module");
var SeriolabReportModule = (function () {
    function SeriolabReportModule() {
    }
    return SeriolabReportModule;
}());
SeriolabReportModule = __decorate([
    core_1.NgModule({
        imports: [seriolab_report_index_module_1.SeriolabReportIndexModule],
        exports: [seriolab_report_index_module_1.SeriolabReportIndexModule],
    })
], SeriolabReportModule);
exports.SeriolabReportModule = SeriolabReportModule;
