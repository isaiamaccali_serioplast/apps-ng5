import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../../shared/sidenav/service/sidenav.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {DictionariesService} from "../../../../../shared/i18n/dictionaries.service";
import {parseSerionetRespToData} from "../../../../../shared/utils";

@Component({
  selector: 'app-seriolab-report-index',
  templateUrl: 'seriolab-report-index.component.html',
  styleUrls: ['seriolab-report-index.component.css']
})
export class SeriolabReportIndexComponent implements OnInit {

  siteId: number;
  site: Site;
  sites: Site[];

  constructor(private sideNavService: SideNavService, private loginService: LoginService,
              private sitesService: SitesService, private router: Router,
              private titleService: Title, private dictService:DictionariesService) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Seriolab");
    this.dictService.getDictionary("seriolab").subscribe((dict)=>{});
  }

  ngOnInit() {

    this.sitesService.getSites({is_active:true}).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
    });
  }

  siteChange(e) {
    this.router.navigate([location.pathname.substring(1)]);
  }


  logout() {
    this.loginService.doLogout().first().subscribe((logoutResponse) => {
      this.router.navigate(["/login"]);
    });
  }
}
