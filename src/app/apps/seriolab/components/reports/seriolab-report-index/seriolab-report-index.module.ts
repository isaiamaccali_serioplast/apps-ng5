import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabReportIndexComponent} from "./seriolab-report-index.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SeriolabReportIndexComponent],   // components and directives
  exports:[SeriolabReportIndexComponent],
  providers: []                    // services
})
export class SeriolabReportIndexModule { }


