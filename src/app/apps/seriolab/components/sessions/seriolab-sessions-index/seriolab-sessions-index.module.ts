import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabSessionsIndexComponent} from "./seriolab-sessions-index.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SeriolabSessionsIndexComponent],   // components and directives
  exports:[SeriolabSessionsIndexComponent],
  providers: []                    // services
})
export class SeriolabSessionsIndexModule { }


