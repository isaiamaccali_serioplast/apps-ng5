import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../../shared/sidenav/service/sidenav.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {DictionariesService} from "../../../../../shared/i18n/dictionaries.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-seriolab-index',
  templateUrl: 'seriolab-sessions-index.component.html',
  styleUrls: ['seriolab-sessions-index.component.css']
})
export class SeriolabSessionsIndexComponent extends TranslatableComponent implements OnInit {

  constructor(private sideNavService: SideNavService,
              private loginService: LoginService,
              private router: Router,
              protected translationService: TranslationService,
              private titleService: Title,
              private dictService:DictionariesService) {
    super(translationService);
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Seriolab");
    this.dictService.getDictionary("seriolab").subscribe(()=>{});
  }

  ngOnInit() {

  }

  logout() {
    this.loginService.doLogout().first().subscribe(() => {
      this.router.navigate(["/login"]);
    });
  }

  reload() {
    this.router.navigateByUrl(this.router.url + "");
  }
}
