"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var SeriolabSessionsIndexComponent = (function () {
    function SeriolabSessionsIndexComponent(sideNavService, loginService, activatedRoute, sitesService, router, translationService, titleService, dictService) {
        this.sideNavService = sideNavService;
        this.loginService = loginService;
        this.activatedRoute = activatedRoute;
        this.sitesService = sitesService;
        this.router = router;
        this.translationService = translationService;
        this.titleService = titleService;
        this.dictService = dictService;
        this.sideNavService.leftMenuEnabled = false;
        this.sideNavService.leftMenu = false;
        this.sideNavService.appMenuEnabled = true;
        this.titleService.setTitle("Serioplast - Seriolab");
        this.dictService.getDictionary("seriolab").subscribe(function (dict) { });
    }
    SeriolabSessionsIndexComponent.prototype.ngOnInit = function () {
    };
    SeriolabSessionsIndexComponent.prototype.logout = function () {
        var _this = this;
        this.loginService.doLogout().first().subscribe(function (logoutResponse) {
            _this.router.navigate(["/login"]);
        });
    };
    SeriolabSessionsIndexComponent.prototype.reload = function () {
        this.router.navigateByUrl(this.router.url + "");
    };
    SeriolabSessionsIndexComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    return SeriolabSessionsIndexComponent;
}());
SeriolabSessionsIndexComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-index',
        templateUrl: 'seriolab-sessions-index.component.html',
        styleUrls: ['seriolab-sessions-index.component.css']
    })
], SeriolabSessionsIndexComponent);
exports.SeriolabSessionsIndexComponent = SeriolabSessionsIndexComponent;
