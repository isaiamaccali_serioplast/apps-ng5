"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var sample_measure_1 = require("../../../../_common/models/sample-measure");
var measure_session_sample_1 = require("../../../../_common/models/measure-session-sample");
var cavity_1 = require("../../../../_common/models/cavity");
var status_link_1 = require("../../../../_common/models/status-link");
var utils_1 = require("../../../../../shared/utils");
var SeriolabSessionDetailComponent = (function () {
    function SeriolabSessionDetailComponent(sessionsService, activatedRoute, router, messageService, propertySetService, propertyService, contentTypeService, statusLinkService, controlPlanMeasureService) {
        this.sessionsService = sessionsService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.messageService = messageService;
        this.propertySetService = propertySetService;
        this.propertyService = propertyService;
        this.contentTypeService = contentTypeService;
        this.statusLinkService = statusLinkService;
        this.controlPlanMeasureService = controlPlanMeasureService;
        this.edited = false;
        this.viewMode = false;
        this.overrideMode = false;
        this.showOverride = false;
        this.statusLinkCavityCompleted = new status_link_1.StatusLink();
        this.statusLinkCavityWorking = new status_link_1.StatusLink();
        this.statusLinkSessionCompleted = new status_link_1.StatusLink();
        this.statusLinkSessionWorking = new status_link_1.StatusLink();
        this.statusLinkCPMeasureCompleted = new status_link_1.StatusLink();
        this.statusLinkCPMeasureWorking = new status_link_1.StatusLink();
    }
    SeriolabSessionDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            if (params['session']) {
                var self_1 = _this;
                _this.sessionsService.getSession(parseInt(params['session'], 10)).subscribe(function (session) {
                    var count = 3;
                    var callback = function () {
                        count--;
                        if (count == 0) {
                            self_1.fixSession(session);
                        }
                    };
                    _this.contentTypeService.getContentTypes({ model: 'measuresession' }).subscribe(function (data) {
                        if (data && data['count'] && data['count'] > 0) {
                            var ct = data['results'][0];
                            _this.statusLinkService.getStatusLinks({ app: ct.pk }).subscribe(function (data) {
                                if (data && data['count'] && data['count'] > 0) {
                                    data['results'].forEach(function (sl) {
                                        if (sl.delta_rel == 1)
                                            self_1.statusLinkSessionWorking = sl;
                                        else if (sl.delta_rel == 2)
                                            self_1.statusLinkSessionCompleted = sl;
                                    });
                                    callback();
                                }
                            });
                        }
                    });
                    _this.contentTypeService.getContentTypes({ model: 'controlplansessionmeasure' }).subscribe(function (data) {
                        if (data && data['count'] && data['count'] > 0) {
                            var ct = data['results'][0];
                            _this.statusLinkService.getStatusLinks({ app: ct.pk }).subscribe(function (data) {
                                if (data && data['count'] && data['count'] > 0) {
                                    data['results'].forEach(function (sl) {
                                        if (sl.delta_rel == 1)
                                            self_1.statusLinkCPMeasureWorking = sl;
                                        else if (sl.delta_rel == 2)
                                            self_1.statusLinkCPMeasureCompleted = sl;
                                    });
                                    callback();
                                }
                            });
                        }
                    });
                    _this.contentTypeService.getContentTypes({ model: 'measuresessioncavity' }).subscribe(function (data) {
                        if (data && data['count'] && data['count'] > 0) {
                            var ct = data['results'][0];
                            _this.statusLinkService.getStatusLinks({ app: ct.pk }).subscribe(function (data) {
                                if (data && data['count'] && data['count'] > 0) {
                                    data['results'].forEach(function (sl) {
                                        if (sl.delta_rel == 1)
                                            self_1.statusLinkCavityWorking = sl;
                                        else if (sl.delta_rel == 2)
                                            self_1.statusLinkCavityCompleted = sl;
                                    });
                                    callback();
                                }
                            });
                        }
                    });
                });
            }
        });
        this.activatedRoute.queryParams.subscribe(function (params) {
            if (params['view']) {
                _this.viewMode = true;
            }
            else {
                if (params['edit']) {
                    _this.viewMode = false;
                    if (params['override']) {
                        _this.overrideMode = true;
                    }
                }
            }
        });
    };
    SeriolabSessionDetailComponent.prototype.fixSession = function (session) {
        var _this = this;
        if (session.status.pk == this.statusLinkSessionCompleted.pk) {
            session.status = this.statusLinkSessionCompleted;
        }
        else {
            if (session.status.pk == this.statusLinkSessionWorking.pk) {
                session.status = this.statusLinkSessionWorking;
                this.controlPlanMeasureService.getControlPlanMeasures({ control_plan: session.control_plan.pk, is_active: true }).subscribe(function (data) {
                    var measures = utils_1.parseSerionetRespToData(data);
                    var self = _this;
                    var count = measures.length;
                    var callback = function () {
                        count--;
                        if (count == 0) {
                            session.controlplansessionmeasure_set = measures;
                            if (session.measuresessionsample_set) {
                                session.measuresessionsample_set.forEach(function (s) {
                                    var current_measures = utils_1.cloneObject(s.samplemeasure_set);
                                    s.samplemeasure_set = [];
                                    var cPK = s.cavity;
                                    session.controlplansessionmeasure_set.forEach(function (cpm) {
                                        var m = new sample_measure_1.SampleMeasure();
                                        m.property = cpm.property_set.property;
                                        //m.delta = cpm.delta; //TODO
                                        var match = current_measures.filter(function (sm) {
                                            return m.property.pk == parseInt(sm.property + "", 10);
                                        });
                                        if (match && match.length > 0) {
                                            var savedMeasure = match[0];
                                            m.pk = savedMeasure.pk;
                                            m.value = savedMeasure.value;
                                            m.overridden_value = savedMeasure.overridden_value;
                                            m.notes = savedMeasure.notes;
                                        }
                                        s.samplemeasure_set.push(m);
                                    });
                                    s.cavity = session.measuresessioncavity_set.filter(function (c) { return c.pk == parseInt(cPK + "", 10); })[0];
                                });
                            }
                            self.session = session;
                        }
                    };
                    measures.forEach(function (m) {
                        delete m.pk;
                        delete m.__str__;
                        delete m.control_plan;
                        session.controlplansessionmeasure_set.push(m);
                        self.propertySetService.getPropertySet(m.property_set.pk).subscribe(function (ps) {
                            m.property_set = ps;
                            self.propertyService.getProperty(m.property_set.property.pk).subscribe(function (p) {
                                m.property_set.property = p;
                                callback();
                            });
                        });
                    });
                });
            }
        }
    };
    SeriolabSessionDetailComponent.prototype.pause = function () {
        this.save(false);
    };
    SeriolabSessionDetailComponent.prototype.save = function (exit) {
        var _this = this;
        if (exit === void 0) { exit = false; }
        this.sessionsService.saveSession(this.session).subscribe(function (resp) {
            if (resp.pk) {
                if (exit) {
                    _this.router.navigate(["seriolab", "sessions"]);
                }
                else {
                    //let session = resp;
                    _this.sessionsService.getSession(resp.pk).subscribe(function (session) {
                        _this.fixSession(session);
                    });
                }
            }
            else {
            }
        });
    };
    SeriolabSessionDetailComponent.prototype.back = function () {
        if (this.edited) {
            var self_2 = this;
            this.messageService.open({
                confirm: function () {
                    self_2.router.navigate(["seriolab", "sessions"]);
                },
                deny: function () {
                },
                title: 'ATTENTION',
                message: 'ATTENTION_DATA_LOSS'
            });
        }
        else {
            this.router.navigate(["seriolab", "sessions"]);
        }
    };
    SeriolabSessionDetailComponent.prototype.addColumn = function () {
        if (this.allowAdd()) {
            var sample = new measure_session_sample_1.MeasureSessionSample();
            //sample.pk = this.session.measuresessionsample_set.length;
            sample.samplemeasure_set = [];
            for (var _i = 0, _a = this.session.controlplansessionmeasure_set; _i < _a.length; _i++) {
                var m = _a[_i];
                var measure = new sample_measure_1.SampleMeasure();
                measure.property = m.property_set.property;
                sample.samplemeasure_set.push(measure);
            }
            sample.cavity = new cavity_1.Cavity();
            this.session.measuresessionsample_set.push(sample);
        }
    };
    SeriolabSessionDetailComponent.prototype.allowAdd = function () {
        var samples = this.session.measuresessionsample_set;
        if (samples.length == 0) {
            return true;
        }
        else {
            var lastSample = samples[samples.length - 1];
            for (var _i = 0, _a = lastSample.samplemeasure_set; _i < _a.length; _i++) {
                var m = _a[_i];
                if (m.value != null) {
                    return true;
                }
            }
            return false;
        }
    };
    SeriolabSessionDetailComponent.prototype.checkAll = function () {
        this.edited = true;
    };
    SeriolabSessionDetailComponent.prototype.toggleCavity = function (c) {
        c.active = !c.active;
    };
    SeriolabSessionDetailComponent.prototype.handleCavityChange = function (s) {
        var resetMeasures = false;
        for (var _i = 0, _a = this.session.measuresessioncavity_set; _i < _a.length; _i++) {
            var c = _a[_i];
            if (c.pk == s.cavity.pk) {
                if (c.status.pk == this.statusLinkCavityCompleted.pk) {
                    resetMeasures = true;
                    break;
                }
            }
        }
        if (resetMeasures) {
            s.samplemeasure_set.forEach((function (m) {
                m.value = null;
            }));
        }
    };
    SeriolabSessionDetailComponent.prototype.calcInsertedMeasures = function (cavityId, pId) {
        var count = 0;
        for (var _i = 0, _a = this.session.measuresessionsample_set; _i < _a.length; _i++) {
            var s = _a[_i];
            if (cavityId) {
                if ((s.cavity.pk == cavityId)) {
                    count = this.countMeasures(count, s, pId);
                }
            }
            else {
                count = this.countMeasures(count, s, pId);
            }
        }
        if (!cavityId) {
            for (var _b = 0, _c = this.session.controlplansessionmeasure_set; _b < _c.length; _b++) {
                var cm = _c[_b];
                if (cm.property_set.property.pk == pId) {
                    if (count == (cm.repeats * (this.session.measuresessioncavity_set.length || 1))) {
                        cm.status = this.statusLinkCPMeasureCompleted;
                    }
                    else {
                        cm.status = this.statusLinkCPMeasureWorking;
                    }
                }
            }
        }
        return count;
    };
    SeriolabSessionDetailComponent.prototype.countMeasures = function (count, s, pId) {
        for (var _i = 0, _a = s.samplemeasure_set; _i < _a.length; _i++) {
            var m = _a[_i];
            if (m.property.pk == pId) {
                if (m.value != null) {
                    count++;
                }
            }
        }
        return count;
    };
    SeriolabSessionDetailComponent.prototype.checkPropertyCompleted = function (pId) {
        for (var _i = 0, _a = this.session.controlplansessionmeasure_set; _i < _a.length; _i++) {
            var m = _a[_i];
            if (m.property_set.property.pk == pId) {
                this.calcInsertedMeasures(null, m.property_set.property.pk);
                return m.status.pk == this.statusLinkCPMeasureCompleted.pk;
            }
        }
    };
    SeriolabSessionDetailComponent.prototype.checkCavityCompleted = function (c) {
        var incompletedProperties = this.session.controlplansessionmeasure_set.length;
        for (var _i = 0, _a = this.session.controlplansessionmeasure_set; _i < _a.length; _i++) {
            var m = _a[_i];
            var count = this.calcInsertedMeasures(c.pk, m.property_set.property.pk);
            if (count == m.repeats) {
                incompletedProperties--;
            }
        }
        if (incompletedProperties == 0) {
            c.status = this.statusLinkCavityCompleted;
            return true;
        }
        else {
            c.status = this.statusLinkCavityCompleted;
            return false;
        }
    };
    SeriolabSessionDetailComponent.prototype.checkAllCompleted = function () {
        var incompletedMeasures = this.session.controlplansessionmeasure_set.length;
        for (var _i = 0, _a = this.session.controlplansessionmeasure_set; _i < _a.length; _i++) {
            var m = _a[_i];
            if (this.calcInsertedMeasures(null, m.property_set.property.pk) == m.repeats * (this.session.measuresessioncavity_set.length || 1)) {
                incompletedMeasures--;
            }
        }
        if (incompletedMeasures == 0) {
            this.session.status = this.statusLinkSessionCompleted;
        }
        else {
            this.session.status = this.statusLinkSessionWorking;
        }
        return incompletedMeasures == 0;
    };
    SeriolabSessionDetailComponent.prototype.calcPercentage = function (a, b) {
        return a / b;
    };
    SeriolabSessionDetailComponent.prototype.disableInput = function (s, m) {
        if (this.session.measuresessioncavity_set.length > 0 && !s.cavity.pk) {
            return true;
        }
        else {
            if (this.checkPropertyCompleted(m.property.pk) && m.value == null) {
                return true;
            }
            else {
                var repeats = 0;
                for (var _i = 0, _a = this.session.controlplansessionmeasure_set; _i < _a.length; _i++) {
                    var cm = _a[_i];
                    if (cm.property_set.property.pk == m.property.pk) {
                        repeats = cm.repeats;
                    }
                }
                return ((this.calcInsertedMeasures(s.cavity.pk, m.property.pk) == repeats) && m.value == null);
            }
        }
    };
    SeriolabSessionDetailComponent.prototype.edit = function () {
        this.viewMode = false;
    };
    SeriolabSessionDetailComponent.prototype.editOverride = function () {
        this.viewMode = false;
        this.overrideMode = true;
        this.showOverride = true;
    };
    SeriolabSessionDetailComponent.prototype.askOverride = function (m) {
        var self = this;
        if (this.overrideMode && !m.overridden_value) {
            this.messageService.open({
                confirm: function () {
                    self.showOverride = true;
                    m.overridden_value = m.value;
                },
                deny: function () {
                },
                title: 'ATTENTION',
                message: 'CONFIRM_OVERRIDE'
            });
        }
    };
    SeriolabSessionDetailComponent.prototype.updateModel = function (value, m) {
        if (this.showOverride) {
            m.overridden_value = value;
        }
        else {
            m.value = value;
        }
    };
    SeriolabSessionDetailComponent.prototype.isShowingOverride = function (m) {
        return this.showOverride && m.overridden_value != null;
    };
    SeriolabSessionDetailComponent.prototype.annotateSample = function (s) {
        this.sampleToAnnotate = s;
        this.sampleNotes.open();
    };
    SeriolabSessionDetailComponent.prototype.annotateMeasure = function (m, s) {
        this.measureToAnnotate = m;
        this.measureToAnnotateSample = s;
        this.measureNotes.open();
    };
    SeriolabSessionDetailComponent.prototype.clearMeasureToAnnotate = function () {
        this.measureToAnnotate = null;
        this.measureToAnnotateSample = null;
    };
    SeriolabSessionDetailComponent.prototype.clearSampleToAnnotate = function () {
        this.sampleToAnnotate = null;
    };
    SeriolabSessionDetailComponent.prototype.parseInt = function (n) {
        return parseInt(n, 10);
    };
    SeriolabSessionDetailComponent.prototype.toFixed = function (n) {
        return n.toFixed(3);
    };
    return SeriolabSessionDetailComponent;
}());
__decorate([
    core_1.ViewChild('sampleNotes')
], SeriolabSessionDetailComponent.prototype, "sampleNotes", void 0);
__decorate([
    core_1.ViewChild('measureNotes')
], SeriolabSessionDetailComponent.prototype, "measureNotes", void 0);
SeriolabSessionDetailComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-session-detail',
        templateUrl: 'seriolab-session-detail.component.html',
        styleUrls: ['seriolab-session-detail.component.css']
    })
], SeriolabSessionDetailComponent);
exports.SeriolabSessionDetailComponent = SeriolabSessionDetailComponent;
