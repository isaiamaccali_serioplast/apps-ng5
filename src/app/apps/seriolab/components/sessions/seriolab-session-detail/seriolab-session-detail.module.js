"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var shared_module_1 = require("../../../../../shared/shared.module");
var seriolab_session_detail_component_1 = require("./seriolab-session-detail.component");
var common_module_1 = require("../../../../_common/common.module");
var SeriolabSessionDetailModule = (function () {
    function SeriolabSessionDetailModule() {
    }
    return SeriolabSessionDetailModule;
}());
SeriolabSessionDetailModule = __decorate([
    core_1.NgModule({
        imports: [shared_module_1.SharedModule, common_module_1.SerioplastCommonModule],
        declarations: [seriolab_session_detail_component_1.SeriolabSessionDetailComponent],
        exports: [seriolab_session_detail_component_1.SeriolabSessionDetailComponent],
        providers: [] // services
    })
], SeriolabSessionDetailModule);
exports.SeriolabSessionDetailModule = SeriolabSessionDetailModule;
