import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabSessionDetailComponent} from "./seriolab-session-detail.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {SessionDetailModule} from "../../../../../shared/components/session-detail/session-detail.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule,SessionDetailModule],       // module dependencies
  declarations: [SeriolabSessionDetailComponent],   // components and directives
  exports:[SeriolabSessionDetailComponent],
  providers: []                    // services
})
export class SeriolabSessionDetailModule { }


