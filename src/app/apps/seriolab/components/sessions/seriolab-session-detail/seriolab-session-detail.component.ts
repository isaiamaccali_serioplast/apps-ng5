import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {MeasureSession, MeasureSessionService} from "../../../../_common/services/measure-session.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";

@Component({
  selector: 'app-seriolab-session-detail',
  templateUrl: 'seriolab-session-detail.component.html',
  styleUrls: ['seriolab-session-detail.component.css']
})
export class SeriolabSessionDetailComponent implements OnInit {

  session: MeasureSession;
  viewMode = false;


  constructor(private sessionsService: MeasureSessionService, private activatedRoute: ActivatedRoute,
              private router: Router, private loadingService:LoadingService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['session']) {
        this.loadingService.show();
        this.sessionsService.getSession(parseInt(params['session'], 10)).subscribe((session) => {
          this.loadingService.hide();
          this.session = session;
        })
      }
    });

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['view']) {
        this.viewMode = true;
      }
      else {
        if (params['edit']) {
          this.viewMode = false;
        }
      }
    });
  }

  back() {
    this.router.navigate(["seriolab", "sessions"]);
  }
}

