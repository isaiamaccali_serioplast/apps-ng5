"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var measure_session_1 = require("../../../../_common/models/measure-session");
var site_1 = require("../../../../_common/models/site");
var material_1 = require("../../../../_common/models/material");
var control_plan_1 = require("../../../../_common/models/control-plan");
var utils_1 = require("../../../../../shared/utils");
var routing_1 = require("../../../../_common/models/routing");
var measure_session_cavity_1 = require("../../../../_common/models/measure-session-cavity");
var SeriolabSessionCreateComponent = (function () {
    function SeriolabSessionCreateComponent(sessionsService, sitesService, controlPlanService, router, translationService, batchesService, controlPlanMeasuresService, messageService, userSitesService, loginService, routingService) {
        this.sessionsService = sessionsService;
        this.sitesService = sitesService;
        this.controlPlanService = controlPlanService;
        this.router = router;
        this.translationService = translationService;
        this.batchesService = batchesService;
        this.controlPlanMeasuresService = controlPlanMeasuresService;
        this.messageService = messageService;
        this.userSitesService = userSitesService;
        this.loginService = loginService;
        this.routingService = routingService;
        this.sites = [];
        this.userSites = [];
        this.batches = [];
        this.cavities = [];
        this.useAsMaterialFilter = true;
        this.materialAdditionalQuery = {};
        this.routings = [];
        this.mouldsets = [];
        this.session = new measure_session_1.MeasureSession();
        this.session.site = new site_1.Site();
        this.session.measuresessioncavity_set = [];
        this.session.measuresessionsample_set = [];
        this.session.control_plan = new control_plan_1.ControlPlan();
        this.session.routing = new routing_1.Routing();
        this.session.mouldset = [new material_1.Material()];
        this.session.batch_dummy = false;
        this.session.notes = "Test";
        this.session.material = new material_1.Material();
        var fromDateObj = new Date();
        fromDateObj.setMonth(fromDateObj.getMonth() - 1);
        this.fromDate = fromDateObj.toISOString().slice(0, 16);
        this.toDate = (new Date()).toISOString().slice(0, 16);
    }
    SeriolabSessionCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sitesService.getSitesList({ is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.sites = data['results'];
            }
        });
        this.userSitesService.getUserSites(this.loginService.user.pk).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.userSites = data['results'];
            }
        });
    };
    SeriolabSessionCreateComponent.prototype.loadControlPlans = function () {
        var _this = this;
        this.controlPlanService.getControlPlans({ material: this.session.material.pk, site: this.cpSite }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.controlPlans = data['results'];
            }
            else {
                _this.controlPlans = [];
                _this.session.control_plan = new control_plan_1.ControlPlan();
                _this.messageService.open({
                    confirm: function () { },
                    title: 'ATTENTION',
                    message: 'NO_CONTROL_PLAN_AVAILABLE'
                });
            }
        });
    };
    SeriolabSessionCreateComponent.prototype.loadBatches = function () {
        var _this = this;
        this.batchesService.getBatches({
            Material: this.session.material.code,
            Plant: this.session.site.code,
            from: this.fromDate.split("-").join(""),
            to: this.toDate.split("-").join("")
        }).subscribe(function (data) {
            if (data.status == 200) {
                _this.batches = data['response']['results'];
            }
            else {
                _this.batches = [];
            }
        });
    };
    SeriolabSessionCreateComponent.prototype.createSession = function () {
        var _this = this;
        this.sessionsService.saveSession(this.session).subscribe(function (session) {
            _this.handleSaveOrUpdate(session, function () {
                this.router.navigate(["seriolab", "sessions", "detail", session.pk]);
            });
        });
    };
    SeriolabSessionCreateComponent.prototype.tr = function (value) {
        return this.translationService.translate(value, null, null, null);
    };
    SeriolabSessionCreateComponent.prototype.back = function () {
        this.router.navigate(["seriolab", "sessions"]);
    };
    SeriolabSessionCreateComponent.prototype.selectedMaterial = function (material) {
        if (material != null) {
            this.session.material = material;
        }
        else {
            this.session.material = new material_1.Material();
            this.session.control_plan = new control_plan_1.ControlPlan();
            this.controlPlans = [];
        }
    };
    SeriolabSessionCreateComponent.prototype.handleSiteChange = function () {
        utils_1.fixValue(this.sites, this.session, 'site');
        this.useAsMaterialFilter = true;
        this.setSiteFilter();
    };
    SeriolabSessionCreateComponent.prototype.handleSiteCPChange = function () {
        this.loadControlPlans();
    };
    SeriolabSessionCreateComponent.prototype.handleBatchDateChange = function () {
        if (this.fromDate && this.toDate)
            this.loadBatches();
    };
    SeriolabSessionCreateComponent.prototype.handleBatchSelection = function () {
        var _this = this;
        this.batches.forEach(function (b_s) {
            if (b_s.BatchNumber == _this.session.batch) {
                _this.session.batch_sap = b_s;
            }
        });
    };
    SeriolabSessionCreateComponent.prototype.loadCavities = function () {
        var _this = this;
        if (this.session.mouldset && this.session.mouldset[0].pk) {
            this.cavities = this.session.mouldset[0]['sub_model']['plm_revision_cavities'].split(",").map(function (c_) {
                var c = new measure_session_cavity_1.MeasureSessionCavity();
                c.active = false;
                c.name = c_;
                c.mouldset = _this.session.mouldset[0];
                return c;
            });
        }
        else {
            this.cavities = [];
            this.session.measuresessioncavity_set = [];
        }
    };
    SeriolabSessionCreateComponent.prototype.handleControlPlanChange = function () {
        var _this = this;
        utils_1.fixValue(this.controlPlans, this.session, 'control_plan');
        this.controlPlanMeasuresService.getControlPlanMeasures({ control_plan: this.session.control_plan.pk }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.session.control_plan.measures = data['results'];
            }
        });
    };
    SeriolabSessionCreateComponent.prototype.setSiteFilter = function () {
        if (this.useAsMaterialFilter)
            this.materialAdditionalQuery['site_set'] = this.session.site.pk;
        else
            this.materialAdditionalQuery['site_set'] = null;
    };
    SeriolabSessionCreateComponent.prototype.showCPDetail = function (pk) {
        this.cpToShow = null;
        var self = this;
        setTimeout(function () {
            self.cpToShow = pk;
            self.cpDetailDialog.open();
        }, 500);
    };
    SeriolabSessionCreateComponent.prototype.loadRoutings = function () {
        var _this = this;
        //non specifico l'is_active, perchè il batch può essere stato prodotto anche con un routing disattivato
        this.routingService.getRoutings({ material: this.session.material.pk, site: this.session.site.pk }).subscribe(function (resp) {
            _this.routings = utils_1.parseSerionetRespToData(resp);
        });
    };
    SeriolabSessionCreateComponent.prototype.fixRouting = function () {
        utils_1.fixValue(this.routings, this.session, 'routing');
    };
    SeriolabSessionCreateComponent.prototype.handleMouldsetChange = function () {
        var _this = this;
        this.mouldsets.forEach(function (m) {
            if (_this.session.mouldset[0].pk == m.pk) {
                _this.session.mouldset[0] = m;
            }
        });
        this.loadCavities();
    };
    /*
     * Step 1: Material & Site Selection
     */
    SeriolabSessionCreateComponent.prototype.step1ProceedCondition = function () {
        return this.session.material && this.session.material.pk && this.session.site && this.session.site.pk;
    };
    SeriolabSessionCreateComponent.prototype.step1ProceedConfirm = function () {
        if (!this.cpSite)
            this.cpSite = this.session.site.pk;
        this.loadControlPlans();
        this.loadRoutings();
    };
    /*
     * Step 2: Control Plan selection
     */
    SeriolabSessionCreateComponent.prototype.step2ProceedCondition = function () {
        return this.session.control_plan && this.session.control_plan.pk;
    };
    SeriolabSessionCreateComponent.prototype.step2ProceedConfirm = function () {
        var _this = this;
        this.controlPlanMeasuresService.getControlPlanMeasures({ control_plan: this.session.control_plan.pk }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                data['results'].forEach(function (cpm) {
                    delete cpm.pk;
                    delete cpm.control_plan;
                    delete cpm.__str__;
                });
                _this.session.controlplansessionmeasure_set = data['results'];
            }
        });
        this.toDate = utils_1.getUrlDate(new Date());
        var d = new Date();
        d.setDate(d.getDate() - 7);
        this.fromDate = utils_1.getUrlDate(d);
    };
    SeriolabSessionCreateComponent.prototype.step2Previous = function () {
        this.session.control_plan = new control_plan_1.ControlPlan();
        this.session.routing = new routing_1.Routing();
    };
    /*
     * Step 3: Batch & Routing
     */
    SeriolabSessionCreateComponent.prototype.step3ProceedCondition = function () {
        if (this.session.batch_dummy) {
            return this.session.batch && this.session.batch_date;
        }
        else {
            return this.session.batch && this.session.batch_sap && this.session.routing && this.session.routing.pk;
        }
    };
    SeriolabSessionCreateComponent.prototype.step3ProceedConfirm = function () {
        if (!this.session.batch_dummy) {
            this.session.batch_date = utils_1.sapDateToUrlDate(this.session.batch_sap.CreationDate);
            this.session.routing_date = utils_1.sapDateToUrlDate(this.session.batch_sap.CreationDate);
        }
    };
    SeriolabSessionCreateComponent.prototype.step3Previous = function () {
        this.session.control_plan = new control_plan_1.ControlPlan();
        this.session.routing = new routing_1.Routing();
    };
    /*
     * Step 4: Mouldset & Cavities
     */
    SeriolabSessionCreateComponent.prototype.step4ProceedCondition = function () {
        return this.session.mouldset && this.session.mouldset.length > 0;
    };
    SeriolabSessionCreateComponent.prototype.step4ProceedConfirm = function () {
        this.session.measuresessioncavity_set = this.cavities;
    };
    SeriolabSessionCreateComponent.prototype.step4Previous = function () {
        this.session.mouldset = [];
    };
    SeriolabSessionCreateComponent.prototype.loadRoutingVersion = function () {
        var _this = this;
        this.routingService.getRouting(this.session.routing.pk, this.session.routing_date).subscribe(function (data) {
            _this.session.routing_data = JSON.stringify(data);
            var mouldsets_set = data['routing_operations'].map(function (o) { return o.prt; });
            _this.mouldsets = [];
            mouldsets_set.forEach(function (set) {
                if (set && set.length > 0) {
                    _this.mouldsets = _this.mouldsets.concat.apply(_this.mouldsets, set);
                }
            });
        });
    };
    SeriolabSessionCreateComponent.prototype.getUrlDate = function (date) {
        return utils_1.getUrlDate(new Date(date));
    };
    SeriolabSessionCreateComponent.prototype.makeCavities = function () {
        var _this = this;
        if (this.cavities_dummy.length > 0) {
            this.cavities = this.cavities_dummy.split(",").map(function (c_) {
                var c = new measure_session_cavity_1.MeasureSessionCavity();
                c.name = c_;
                var match = _this.cavities.filter(function (cav) { return cav.name == c.name; });
                c.active = (match && match[0] && match[0].active);
                c.mouldset = _this.session.mouldset[0];
                return c;
            });
        }
        else {
            this.cavities = [];
        }
    };
    SeriolabSessionCreateComponent.prototype.handleSaveOrUpdate = function (resp, callback) {
        var self = this;
        if (resp.pk) {
            self.messageService.open({
                message: self.tr('SAVE_COMPLETED'),
                title: " ",
                autoClose: false,
                onClose: function () {
                    if (callback)
                        callback.call(self);
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    return SeriolabSessionCreateComponent;
}());
__decorate([
    core_1.ViewChild('cpDetailDialog')
], SeriolabSessionCreateComponent.prototype, "cpDetailDialog", void 0);
SeriolabSessionCreateComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-session-create',
        templateUrl: 'seriolab-session-create.component.html',
        styleUrls: ['seriolab-session-create.component.css']
    })
], SeriolabSessionCreateComponent);
exports.SeriolabSessionCreateComponent = SeriolabSessionCreateComponent;
