import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabSessionCreateComponent} from "./seriolab-session-create.component";
import {SeriolabControlPlanViewModule} from "../../control-plans/seriolab-control-plan-view/seriolab-control-plan-view.module";


@NgModule({
  imports: [SharedModule,SeriolabControlPlanViewModule],       // module dependencies
  declarations: [SeriolabSessionCreateComponent],   // components and directives
  exports:[SeriolabSessionCreateComponent],
  providers: []                    // services
})
export class SeriolabSessionCreateModule { }


