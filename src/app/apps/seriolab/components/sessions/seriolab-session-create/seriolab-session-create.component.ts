import {Component, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {
  GroupingFactor,
  MeasureSession, MeasureSessionService
} from "../../../../_common/services/measure-session.service";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {ControlPlan, ControlPlansService} from "../../../../_common/services/control-plans.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {Batch, BatchesService} from "../../../../_common/services/batches.service";
import {
  arrayDistinct,
  buildErrorMessage, fixValue, getUrlDate, parseSerionetRespToData,
  sapDateToUrlDate
} from "../../../../../shared/utils";
import {
  ControlPlanMeasure,
  ControlPlanMeasuresService
} from "../../../../_common/services/control-plan-measures.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {Routing, RoutingsService} from "../../../../_common/services/routings.service";
import {ContentType, ContentTypeService} from "../../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {Material} from "../../../../_common/services/materials.service";
import {RoutingOperation, RoutingsOperationsService} from "../../../../_common/services/routings-operations.service";
import {CavitiesService, Cavity} from "../../../../_common/services/cavities.service";
import {FactorLevel} from "../../../../_common/services/factor-levels.service";
import {StepperComponent} from "../../../../../shared/material/stepper/stepper.component";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-seriolab-session-create',
  templateUrl: 'seriolab-session-create.component.html',
  styleUrls: ['seriolab-session-create.component.css']
})
export class SeriolabSessionCreateComponent extends BaseComponent implements OnInit {

  @ViewChild('cpDetailDialog') cpDetailDialog: DialogComponent;
  @ViewChild('stepper') stepper: StepperComponent;
  session: MeasureSession;
  sites: Site[] = [];
  userSites: Site[] = [];
  material: Material;
  controlPlans: ControlPlan[];
  batches: Batch[] = [];
  fromDate: string;
  toDate: string;
  cpSite: number;

  useAsMaterialFilter: boolean = true;
  materialAdditionalQuery = {};

  cpToShow: number;

  statusLinkSessionWorking = new StatusLink();

  constructor(private sessionsService: MeasureSessionService, private sitesService: SitesService,
              private controlPlanService: ControlPlansService, private router: Router,
              protected translationService: TranslationService, private batchesService: BatchesService,
              private controlPlanMeasuresService: ControlPlanMeasuresService,
              protected messageService: MessageService, private routingService: RoutingsService,
              private contentTypeService: ContentTypeService, private statusLinkService: StatusLinkService,
              protected loadingService: LoadingService, private routingOperationsService: RoutingsOperationsService,
              private cavitiesService: CavitiesService) {
    super(translationService, messageService, loadingService);
    this.session = new MeasureSession();
    this.session.site = new Site();
    this.session.groupingfactor_set = [];
    this.session.measuresessionsample_set = [];
    this.session.control_plan = new ControlPlan();
    this.session.notes = "";
    this.session.material = new Material();
    let fromDateObj = new Date();
    fromDateObj.setMonth(fromDateObj.getMonth() - 1);
    this.fromDate = fromDateObj.toISOString().slice(0, 16);
    this.toDate = (new Date()).toISOString().slice(0, 16);
  }

  ngOnInit() {
    this.sitesService.getSites({is_active: true}).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
    });

    this.contentTypeService.getContentTypes({model: 'measuresession', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk}).subscribe((data) => {
          let sls = parseSerionetRespToData(data);
          sls.forEach((sl: StatusLink) => {
            if (sl.delta_rel == 1) {
              this.statusLinkSessionWorking = sl;
              this.session.status = this.statusLinkSessionWorking;
            }
          });
        });
      }
    });
  }

  loadControlPlans() {
    this.controlPlanService.getControlPlans({
      material: this.session.material.pk,
      site: this.cpSite,
      is_active: true
    }).subscribe((data) => {
      this.controlPlans = parseSerionetRespToData(data);
      if (this.controlPlans.length == 0) {
        this.session.control_plan = new ControlPlan();
        this.messageService.open({
          confirm: () => {
          },
          title: this.tr('ATTENTION'),
          message: this.tr('NO_CONTROL_PLAN_AVAILABLE')
        })
      }
    });
  }

  loadBatches() {
    this.loadingService.show();
    this.batchesService.getBatches({
      Material: this.session.material.code,
      Plant: this.session.site.code,
      from: getUrlDate(new Date(this.fromDate),""),
      to: getUrlDate(new Date(this.toDate),"")
    }).subscribe((data) => {
      this.loadingService.hide();
      if (data.status == 200) {
        this.batches = data['response']['results'];
        if (this.batches.length == 0) {
          this.messageService.open({
            autoclose: true,
            title: " ",
            message: this.tr('NO_BATCHES_AVAILABLE')
          })
        }
      }
      else {
        this.batches = [];
        data.context = "SAP";
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(data),
          autoclose: false,
          confirm: () => {
          }
        })
      }
    });
  }

  createSession() {
    this.loadingService.show();
    this.sessionsService.saveSession(this.session).subscribe((session) => {
      this.manageRespSave(session, () => {
        this.loadingService.hide();
        this.router.navigate(["seriolab", "sessions", "detail", session.pk]);
      })
    })
  }

  back() {
    this.router.navigate(["seriolab", "sessions"]);
  }

  backLastStep() {
    this.stepper.setIndex(3);
    this.step3ProceedConfirm(true);
  }

  selectedMaterial(material: Material) {
    if (material != null) {
      this.session.material = material;
    }
    else {
      this.session.material = new Material();
      this.session.control_plan = new ControlPlan();
      this.controlPlans = [];
    }
  }

  handleSiteChange(s: Site) {
    this.session.site = s;
    this.useAsMaterialFilter = true;
    this.setSiteFilter();
  }

  handleSiteCPChange() {
    this.loadControlPlans();
  }

  handleBatchSelection() {
    this.batches.forEach((b_s) => {
      if (b_s.BatchNumber == this.session.batch) {
        this.session.batch_date = sapDateToUrlDate(b_s.CreationDate);
        this.session.batch_sap = b_s;
        // this.session.batch_sap.RoutingGroup = "01";
      }
    })
  }

  handleControlPlanChange() {
    fixValue(this.controlPlans, this.session, 'control_plan');
    this.controlPlanMeasuresService.getControlPlanMeasures({control_plan: this.session.control_plan.pk}).subscribe((data) => {
      this.session.control_plan.measures = parseSerionetRespToData(data);
    })
  }

  setSiteFilter() {
    if (this.useAsMaterialFilter)
      this.materialAdditionalQuery['site_set'] = this.session.site.pk;
    else
      this.materialAdditionalQuery['site_set'] = null;
  }

  showCPDetail(pk: number) {
    this.cpToShow = null;
    setTimeout(() => {
      this.cpToShow = pk;
      this.cpDetailDialog.open();
    }, 500);
  }

  /*
   * Step 1: Material & Site Selection
   */
  step1ProceedCondition() {
    return this.session.material && this.session.material.pk && this.session.site && this.session.site.pk;
  }

  step1ProceedConfirm() {
    if (!this.cpSite)
      this.cpSite = this.session.site.pk;
    this.loadControlPlans();
  }

  /*
   * Step 2: Control Plan selection
   */
  step2ProceedCondition() {
    return this.session.control_plan && this.session.control_plan.pk;
  }

  step2ProceedConfirm() {
    this.controlPlanMeasuresService.getControlPlanMeasures({control_plan: this.session.control_plan.pk}).subscribe((data) => {
      this.session.controlplansessionmeasure_set = parseSerionetRespToData(data);
      this.session.controlplansessionmeasure_set.forEach((cpm: ControlPlanMeasure) => {
        delete cpm.pk;
        delete cpm.control_plan;
        delete cpm.__str__;
      });
    });
    this.toDate = getUrlDate(new Date());
    let d = new Date();
    d.setDate(d.getDate() - 7);
    this.fromDate = getUrlDate(d);
  }

  step2Previous() {
    this.session.control_plan = new ControlPlan();
  }

  /*
   * Step 3: Batch & Routing
   */
  step3ProceedCondition() {
    return this.session.batch && this.session.sampling_time;
  }

  step3ProceedConfirm(skipCreate = false) {
    if (!skipCreate) {
      this.preloadAgeGroupingFactor();
      if (!this.session.external && this.session.batch_sap.RoutingGroup) {
        this.loadingService.show();
        this.routingService.getRoutings({material: this.session.material.pk, is_active: true, group: this.session.batch_sap.RoutingGroup}).subscribe((data) => {
          let routings: Routing[] = parseSerionetRespToData(data);
          if (routings.length > 0) {
            let routing = routings[0];
            this.routingOperationsService.getRoutingOperations({routing_header: routing.pk, is_active: true}).subscribe((resp) => {
              let ops = parseSerionetRespToData(resp);
              if (ops.length > 0) {
                ops.forEach((o: RoutingOperation) => {
                  o.prt.forEach((m) => {
                    let getCavities = (): Promise<Cavity[]> =>{
                      return new Promise((resolve) => {
                        this.cavitiesService.getCavities({mouldset: m.pk}).subscribe((resp) => {
                          let cavities: Cavity[] = parseSerionetRespToData(resp);
                          resolve(cavities);
                        });
                      });
                    };

                    async function callAsync() {
                      return await getCavities();
                    }

                    callAsync().then((cavities) => {
                      let gf = new GroupingFactor();
                      gf.name = m.__str__;
                      gf.level_string = cavities.map((c) => c.name).join(",");
                      this.session.groupingfactor_set.push(gf);
                      this.makeFactorLevels(gf);
                      gf.factorlevel_set.forEach((fl) => fl.active = true);
                    });
                  });
                });
                this.loadingService.hide();
              }
              else {
                this.loadingService.hide();
              }
            });
          }
          else {
            this.loadingService.hide();
          }
        })
      }
    }

  }

  preloadAgeGroupingFactor() {
    let gf = new GroupingFactor();
    gf.name = "Age";
    gf.level_string = "0,12,24,36";
    this.session.groupingfactor_set.push(gf);
    this.makeFactorLevels(gf);
  }

  step3Previous() {
    this.session.control_plan = new ControlPlan();
  }

  /*
   * Step 4: Grouping Factors & Factor Levels
   */
  step4ProceedCondition() {
    let isOk = true;

    this.session.groupingfactor_set.forEach((gf) => {
      if (!this.isGFOK(gf)) {
        isOk = false;
      }
    });

    return isOk;
  }

  step4ProceedConfirm() {

  }

  step4Previous() {
    this.session.groupingfactor_set = [];
  }

  getUrlDate(date) {
    return getUrlDate(new Date(date));
  }

  makeFactorLevels(gf: GroupingFactor) {
    if (gf.level_string.length > 0) {
      gf.factorlevel_set = gf.level_string.split(",").map((c_: string) => {
        let match = gf.factorlevel_set.filter((fl) => fl.name == c_);
        if (match && match[0]) {
          return match[0];
        }
        else {
          let f = new FactorLevel();
          f.name = c_;
          f.active = false;
          return f;
        }
      });
    }
    else {
      gf.factorlevel_set = [];
    }
  }

  deleteGroupingFactor(gf: GroupingFactor) {
    this.session.groupingfactor_set.splice(this.session.groupingfactor_set.indexOf(gf), 1);
  }

  printFl(gf: GroupingFactor) {
    return gf.factorlevel_set.map((fl) => fl.active ? ("<b>" + fl.name + "</b>") : fl.name).join(", ");
  }

  addGroupingFactor() {
    let gf = new GroupingFactor();
    gf.factorlevel_set = [];
    gf.name = "";
    this.session.groupingfactor_set.push(gf);
  }

  selectAll(gf: GroupingFactor) {
    gf.factorlevel_set.forEach((fl) => fl.active = true);
  }

  isGFOK(gf: GroupingFactor) {
    let isOk = true;
    if (!gf.name || gf.factorlevel_set.length == 0) {
      isOk = false;
      gf.message = "Missing name of empty factor level set";
    }
    else {
      let inactiveFL = gf.factorlevel_set.filter((fl) => !fl.active);
      if (inactiveFL.length == gf.factorlevel_set.length) {
        isOk = false;
        gf.message = "No factor level selected";
      }
      else {
        let names = gf.factorlevel_set.map((fl) => {
          return fl.name.toUpperCase()
        });
        let names_dist = arrayDistinct(names);
        if (names_dist.length != names.length) {
          isOk = false;
          gf.message = "Duplicate factor level";
        }
        else {
          let names = this.session.groupingfactor_set.map((gf_) => {
            return gf_.name.toUpperCase()
          });
          if (names.filter((n) => n == gf.name.toUpperCase()).length > 1) {
            isOk = false;
            gf.message = "Duplicate grouping factor name";
          }
        }
      }
    }
    return isOk;
  }
}
