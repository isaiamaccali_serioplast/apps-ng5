import {NgModule} from "@angular/core";
import {SeriolabSessionDetailModule} from "./seriolab-session-detail/seriolab-session-detail.module";
import {SeriolabSessionsListModule} from "./seriolab-sessions-list/seriolab-sessions-list.module";
import {SeriolabSessionsIndexModule} from "./seriolab-sessions-index/seriolab-sessions-index.module";
import {SeriolabSessionCreateModule} from "./seriolab-session-create/seriolab-session-create.module";

@NgModule({
  imports: [SeriolabSessionCreateModule, SeriolabSessionDetailModule, SeriolabSessionsIndexModule,SeriolabSessionsListModule],       // module dependencies
  exports:[SeriolabSessionCreateModule, SeriolabSessionDetailModule, SeriolabSessionsIndexModule,SeriolabSessionsListModule],
})
export class SeriolabSessionsModule { }


