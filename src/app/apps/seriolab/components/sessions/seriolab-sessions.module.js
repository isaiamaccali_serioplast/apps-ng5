"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var seriolab_session_detail_module_1 = require("./seriolab-session-detail/seriolab-session-detail.module");
var seriolab_sessions_list_module_1 = require("./seriolab-sessions-list/seriolab-sessions-list.module");
var seriolab_sessions_index_module_1 = require("./seriolab-sessions-index/seriolab-sessions-index.module");
var seriolab_session_create_module_1 = require("./seriolab-session-create/seriolab-session-create.module");
var SeriolabSessionsModule = (function () {
    function SeriolabSessionsModule() {
    }
    return SeriolabSessionsModule;
}());
SeriolabSessionsModule = __decorate([
    core_1.NgModule({
        imports: [seriolab_session_create_module_1.SeriolabSessionCreateModule, seriolab_session_detail_module_1.SeriolabSessionDetailModule, seriolab_sessions_index_module_1.SeriolabSessionsIndexModule, seriolab_sessions_list_module_1.SeriolabSessionsListModule],
        exports: [seriolab_session_create_module_1.SeriolabSessionCreateModule, seriolab_session_detail_module_1.SeriolabSessionDetailModule, seriolab_sessions_index_module_1.SeriolabSessionsIndexModule, seriolab_sessions_list_module_1.SeriolabSessionsListModule],
    })
], SeriolabSessionsModule);
exports.SeriolabSessionsModule = SeriolabSessionsModule;
