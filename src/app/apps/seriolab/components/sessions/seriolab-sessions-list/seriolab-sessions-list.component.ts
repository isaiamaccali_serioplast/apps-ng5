import {Component, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {MeasureSession, MeasureSessionService} from "../../../../_common/services/measure-session.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, getUrlDateTime} from "../../../../../shared/utils";
import {ContentType, ContentTypeService} from "../../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {Material} from "../../../../_common/services/materials.service";
import {Site} from "../../../../_common/services/sites.service";
import {StatusService} from "../../../../_common/services/status.service";
import {AppsService} from "../../../../apps.service";
import {SerionetGenericObjectService} from "../../../../_common/services/serionet-generic-objects.service";
import {GenericObj} from "../../../../_common/services/_generic.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";
import {ReportsComponent} from "../../../../../shared/components/reports/reports.component";
import {MatTabGroup} from "@angular/material";

@Component({
  selector: 'app-seriolab-sessions-list',
  templateUrl: 'seriolab-sessions-list.component.html',
  styleUrls: ['seriolab-sessions-list.component.css']
})
export class SeriolabSessionsListComponent extends BaseComponent implements OnInit {

  @ViewChild('workingSessionsTable') workingSessionsTable:TableWithPagesComponent;
  @ViewChild('closedSessionsTable') closedSessionsTable:TableWithPagesComponent;
  @ViewChild('reportsComponent') reportsComponent:ReportsComponent;
  @ViewChild('tab') tab:MatTabGroup;

  sites: Site[] = [];
  materialWorking:Material = new Material();
  materialCompleted:Material = new Material();
  siteIdWorking: number;
  siteIdCompleted: number;
  statusDeltaRelWorking:string = "0|1";

  noItemsMessageWorking = this.tr("NO_WORKING_SESSIONS_AVAILABLE");
  noItemsMessageClosed = this.tr("NO_CLOSED_SESSIONS_AVAILABLE");
  headerConfWorking = [
    {
      label:this.tr("MATERIAL"),
      valueKey:'material',
      useStr:true
    },
    {
      label:this.tr("BATCH"),
      valueKey:'batch',
      useStr:false
    },
    {
      label:this.tr("DATE"),
      valueFunction:(item:MeasureSession) => {

        return getUrlDateTime(new Date(item['date'])).split("T").join(" ");
      }
    },
    {
      label:this.tr("STATUS"),
      valueFunction:(item:MeasureSession)=>{
        let status:StatusLink = null;
        if(item.status.pk==this.statusLinkSessionWorking.pk){
          status = cloneObject(this.statusLinkSessionWorking);
        } else if(item.status.pk==this.statusLinkSessionCompleted.pk){
          status = cloneObject(this.statusLinkSessionCompleted);
        }
        return '<span class="circle" title="'+item.status.__str__+'" style="background-color:'+status.status_id.color+'"></span>';
      }
    },
    {
      label:this.tr("CONTROL_PLAN"),
      valueKey:'control_plan',
      useStr:true
    }
  ];

  headerConfCompleted = [
    {
      label:this.tr("MATERIAL"),
      valueKey:'material',
      useStr:true
    },
    {
      label:this.tr("BATCH"),
      valueKey:'batch',
      useStr:false
    },
    {
      label:this.tr("DATE"),
      valueFunction:function(item:MeasureSession){
        return getUrlDateTime(new Date(item['date'])).split("T").join(" ");
      }
    },
    {
      label:this.tr("CONTROL_PLAN"),
      valueKey:'control_plan',
      useStr:true
    }
  ];

  availableActions = [];
  availableActionsCompleted = [];

  controlPlanNameWorking:string;
  controlPlanNameCompleted:string;

  fromDateCompleted:any;
  toDateCompleted:any;

  statusLinkSessionWorking = new StatusLink();
  statusLinkSessionCompleted = new StatusLink();
  statusLinkSessionClosedCompleted = new StatusLink();

  ready = false;

  workingReady = false;
  completedReady = false;

  selectedTab = 0;

  constructor(private sessionsService:MeasureSessionService, private appsService:AppsService,
              protected translationService:TranslationService, private router:Router,
              protected messageService:MessageService, private serionetGenericObjService:SerionetGenericObjectService,
              private contentTypeService:ContentTypeService, private statusLinkService:StatusLinkService,
              protected loadingService:LoadingService, private statusService:StatusService
  ) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    let count = 1;

    let callback = ()=>{
      count--;
      if(count==0){
        this.ready = true;
      }
    };

    this.contentTypeService.getContentTypes({model: 'measuresession', is_active:true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, is_active:true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            data['results'].forEach((sl: StatusLink) => {
              if (sl.delta_rel == 0)
                this.statusLinkSessionWorking = sl;
              else if (sl.delta_rel == 1)
                this.statusLinkSessionCompleted = sl;
              else if (sl.delta_rel == 2)
                this.statusLinkSessionClosedCompleted = sl;
            });

            this.statusService.getStatus(this.statusLinkSessionWorking.status_id.pk).subscribe((resp)=>{
              this.statusLinkSessionWorking.status_id = resp;
              this.statusService.getStatus(this.statusLinkSessionCompleted.status_id.pk).subscribe((resp)=>{
                this.statusLinkSessionCompleted.status_id = resp;
                callback();
              });
            });
          }
        });
      }
    });

    let actionEdit:TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEdit.click = this.resumeSession.bind(this);
    let actionDelete:TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDelete.click = this.deleteSession.bind(this);
    actionDelete.visibilityCondition = (item:MeasureSession) =>{return item.is_active;};
    this.availableActions.push(actionEdit,actionDelete);
    let actionView:TableRowAction = cloneObject(TableRowActions.VIEW);
    actionView.click = this.viewSession.bind(this);
    actionView.visibilityCondition = (item:MeasureSession) => {return item.is_active;};
    let actionReport:TableRowAction = new TableRowAction();
    actionReport.icon = "equalizer";
    actionReport.tooltip = "Report";
    actionReport.click = this.setReportsArgs.bind(this);
    this.availableActionsCompleted.push(actionView,actionReport,actionDelete)
  }

  loadWorkingSessions(){
    let query = {
      site:this.siteIdWorking,
      material:this.materialWorking.pk,
      control_plan__description:this.controlPlanNameWorking,
      status_delta_rel_list:this.statusDeltaRelWorking,
      control_plan__type__delta_rel:0,
      is_active:true,
      ordering:"-date"
    };
    return this.sessionsService.getSessions(query);
  }

  loadClosedSessions(){
    let query = {
      site:this.siteIdCompleted,
      material:this.materialCompleted.pk,
      control_plan__description:this.controlPlanNameCompleted,
      status:this.statusLinkSessionClosedCompleted.pk,
      date__range_0:this.fromDateCompleted,
      date__range_1:this.toDateCompleted,
      control_plan__type__delta_rel:0,
      is_active:true,
      ordering:"-date"
    };
    return this.sessionsService.getSessions(query);
  }

  create(){
    this.router.navigate(["seriolab","sessions","create"]);
  }

  goToControlPlans(){
    this.router.navigate(["seriolab","control-plans"]);
  }

  goToReports(){
    this.router.navigate(["seriolab","report"]);
  }

  resumeSession(session:MeasureSession){
    // this.messageService.open({
    //   confirm:function(){
    this.router.navigate(["seriolab","sessions",'detail',session.pk]);
    //   },
    //   deny:function(){
    //
    //   },
    //   title:'ATTENTION',
    //   message:'SESSION_RESUME_WARN'
    // });
  }

  deleteSession(s:MeasureSession){

    let isCompleted = false;
    if(s.status.pk==this.statusLinkSessionClosedCompleted.pk){
      isCompleted = true;
    }
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () =>{
        this.sessionsService.deleteSession(s).subscribe((resp)=>{
          this.manageRespDelete(resp,() =>{
            if(!isCompleted)
              this.workingSessionsTable.reload();
            else
              this.closedSessionsTable.reload();
          });
        })
      },
      deny:() =>{

      }
    });
  }

  viewSession(session:MeasureSession){
    this.router.navigate(["seriolab","sessions",'detail',session.pk], {queryParams:{view:true}});
  }

  selectedMaterialWorking(m:Material){
    if(m){
      this.materialWorking = m;
    }
    else {
      this.materialWorking = new Material();
    }
  }

  selectedMaterialCompleted(m:Material){
    if(m){
      this.materialCompleted = m;
    }
    else {
      this.materialCompleted = new Material();
    }
  }

  siteChange(s:Site){
    if(s && s.pk) {
      this.siteIdWorking = s.pk;
      this.workingReady = true;
    }
    else {
      this.siteIdWorking = null;
    }

  }

  siteChangeCompleted(s:Site){
    if(s && s.pk){
      this.siteIdCompleted = s.pk;
      this.completedReady = true;
    }
    else {
      this.siteIdCompleted = null;
    }

  }


  reportTags:GenericObj[] = [];
  reportsConfig: any = [];

  setReportsArgs(s) {
    this.reportTags = [];
    this.reportsConfig = [];

    let externalAppObj = this.appsService.getAppData("seriolab");
    // TAGS = [Seriolab]
    this.serionetGenericObjService.getSerionetGenericObjects({
      content_type: 110,
      object_id: externalAppObj['pk'],
      is_active: true
    }).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.reportTags.push(data['results'][0]['pk']);
      }

      let SERIOLAB_SESSION_STANDARD_REPORT = {
        python_func: 'SeriolabSessionStandard',
        params:
          [
            {
              name: "main_grouping_factor",
              label: "Select grouping factor for analysis",
              disabled:false,
              type:"select",
              value:null,
              value_field_display:'name', // default __str__
              results:s.groupingfactor_set,
              required:true,
              visible:true,
            }
          ],
        filters:
          [
            {
              name: "sample__session",
              label: "measuresession__id",
              is_disabled: false,
              type: "text",
              value: s.pk,
              visible:false,
            },
            {
              name: "control_plan",
              label: "control_plan__id",
              is_disabled: false,
              type: "text",
              value: s.control_plan.pk,
              visible:false,
            },
            {
              name: "sample__session__external",
              label: "measuresession__external",
              is_disabled: false,
              type: "boolean",
              value: false,
              visible:false,
            },
            {
              name: "created_on__range",
              label1: "created_on (start)",
              label2: "created_on (end)",
              is_disabled: false,
              type: "datetimerange",
              value1: "",
              value2: "",
              visible:false,
            },
          ],
      };
      let SERIOLAB_SESSION_INTERNAL_REPORT = {
        python_func: 'SeriolabInternalReport',
        params:
          [
            {
              name: "main_grouping_factor",
              label: "Select grouping factor for analysis",
              disabled:false,
              type:"select",
              value:null,
              value_field_display:'name', // default __str__
              results:s.groupingfactor_set,
              required:true,
              visible:true,
            }
          ],
        filters:
          [
            {
              name: "sample__session",
              label: "measuresession__id",
              is_disabled: false,
              type: "text",
              value: s.pk,
              visible:false,
            },
            {
              name: "control_plan",
              label: "control_plan__id",
              is_disabled: false,
              type: "text",
              value: s.control_plan.pk,
              visible:false,
            },
            {
              name: "sample__session__external",
              label: "measuresession__external",
              is_disabled: false,
              type: "boolean",
              value: false,
              visible:false,
            },
            {
              name: "created_on__range",
              label1: "created_on (start)",
              label2: "created_on (end)",
              is_disabled: false,
              type: "datetimerange",
              value1: "",
              value2: "",
              visible:false,
            },
          ],
      };

      this.reportsConfig = [SERIOLAB_SESSION_STANDARD_REPORT,SERIOLAB_SESSION_INTERNAL_REPORT];
      this.reportsComponent.init();
    });

  }

  tabChange(){
    this.selectedTab = this.tab.selectedIndex;
  }
}
