"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var material_1 = require("../../../../_common/models/material");
var table_with_pages_component_1 = require("../../../../../shared/material/table-with-pages/table-with-pages.component");
var utils_1 = require("../../../../../shared/utils");
var intl_1 = require("@angular/common/src/pipes/intl");
var status_link_1 = require("../../../../_common/models/status-link");
var SeriolabSessionsListComponent = (function () {
    function SeriolabSessionsListComponent(sessionsService, userSitesService, translationService, router, messageService, loginService, contentTypeService, statusLinkService) {
        this.sessionsService = sessionsService;
        this.userSitesService = userSitesService;
        this.translationService = translationService;
        this.router = router;
        this.messageService = messageService;
        this.loginService = loginService;
        this.contentTypeService = contentTypeService;
        this.statusLinkService = statusLinkService;
        this.sites = [];
        this.materialWorking = new material_1.Material();
        this.materialCompleted = new material_1.Material();
        this.noItemsMessageWorking = this.tr("NO_WORKING_SESSIONS_AVAILABLE");
        this.noItemsMessageClosed = this.tr("NO_CLOSED_SESSIONS_AVAILABLE");
        this.headerConf = [
            {
                label: this.tr("BATCH"),
                valueKey: 'batch',
                useStr: false
            },
            {
                label: this.tr("DATE"),
                valueFunction: function (item) {
                    return intl_1.DateFormatter.format(new Date(item['date']), 'en-US', 'yyyy-MM-dd HH:mm');
                }
            },
            {
                label: this.tr("MATERIAL"),
                valueKey: 'material',
                useStr: true
            },
            {
                label: this.tr("CONTROL_PLAN"),
                valueKey: 'control_plan',
                useStr: true
            }
        ];
        this.availableActions = [];
        this.availableActionsCompleted = [];
        this.statusLinkSessionWorking = new status_link_1.StatusLink();
        this.statusLinkSessionCompleted = new status_link_1.StatusLink();
    }
    SeriolabSessionsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this.contentTypeService.getContentTypes({ model: 'measuresession' }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                var ct = data['results'][0];
                _this.statusLinkService.getStatusLinks({ app: ct.pk }).subscribe(function (data) {
                    if (data && data['count'] && data['count'] > 0) {
                        data['results'].forEach(function (sl) {
                            if (sl.delta_rel == 1)
                                self.statusLinkSessionWorking = sl;
                            else if (sl.delta_rel == 2)
                                self.statusLinkSessionCompleted = sl;
                        });
                    }
                });
            }
        });
        this.userSitesService.getUserSites(this.loginService.user.pk).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.sites = data['results'];
                _this.siteIdWorking = _this.siteIdCompleted = _this.sites[0].pk;
            }
        });
        var actionEdit = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.EDIT);
        actionEdit.click = this.resumeSession.bind(this);
        var actionDelete = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.DELETE);
        actionDelete.click = this.deleteSession.bind(this);
        actionDelete.visibilityCondition = function (item) { return item.is_active; };
        this.availableActions.push(actionEdit, actionDelete);
        var actionView = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.VIEW);
        actionView.click = this.viewSession.bind(this);
        actionView.visibilityCondition = function (item) { return item.is_active; };
        this.availableActionsCompleted.push(actionView, actionDelete);
    };
    SeriolabSessionsListComponent.prototype.loadWorkingSessions = function () {
        var _this = this;
        var query = {
            siteId: this.siteIdWorking,
            material: this.materialWorking.pk,
            control_plan__name__icontains: this.controlPlanNameWorking,
            status: this.statusLinkSessionWorking.pk,
            is_active: true
        };
        this.sessionsService.getSessions(query).subscribe(function (data) {
            _this.workingSessionsTable.handleResp(data);
        });
    };
    SeriolabSessionsListComponent.prototype.loadClosedSessions = function () {
        var _this = this;
        var query = {
            siteId: this.siteIdCompleted,
            material: this.materialCompleted.pk,
            control_plan__name__icontains: this.controlPlanNameCompleted,
            status: this.statusLinkSessionCompleted.pk,
            date_from: this.fromDateCompleted,
            date_to: this.toDateCompleted,
            is_active: true
        };
        this.sessionsService.getSessions(query).subscribe(function (data) {
            _this.closedSessionsTable.handleResp(data);
        });
    };
    SeriolabSessionsListComponent.prototype.tr = function (value) {
        return this.translationService.translate(value, null, null, null);
    };
    SeriolabSessionsListComponent.prototype.create = function () {
        this.router.navigate(["seriolab", "sessions", "create"]);
    };
    SeriolabSessionsListComponent.prototype.goToControlPlans = function () {
        this.router.navigate(["seriolab", "control-plans"]);
    };
    SeriolabSessionsListComponent.prototype.goToReports = function () {
        this.router.navigate(["seriolab", "report"]);
    };
    SeriolabSessionsListComponent.prototype.resumeSession = function (session) {
        var self = this;
        this.messageService.open({
            confirm: function () {
                self.router.navigate(["seriolab", "sessions", 'detail', session.pk]);
            },
            deny: function () {
            },
            title: 'ATTENTION',
            message: 'SESSION_RESUME_WARN'
        });
    };
    SeriolabSessionsListComponent.prototype.siteChangeWorking = function () {
        this.workingSessionsTable.reload();
    };
    SeriolabSessionsListComponent.prototype.siteChangeCompleted = function () {
        this.closedSessionsTable.reload();
    };
    SeriolabSessionsListComponent.prototype.deleteSession = function (s) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('CONFIRM_DELETE'),
            autoClose: false,
            confirm: function () {
                self.sessionsService.deleteSession(s).subscribe(function (resp) {
                    self.handleDelete(resp, function () {
                        self.loadWorkingSessions();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    SeriolabSessionsListComponent.prototype.handleDelete = function (resp, callback) {
        var self = this;
        if (resp.status == 204) {
            self.messageService.open({
                title: " ",
                message: self.tr('DELETE_COMPLETED'),
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    SeriolabSessionsListComponent.prototype.viewSession = function (session) {
        this.router.navigate(["seriolab", "sessions", 'detail', session.pk], { queryParams: { view: true } });
    };
    SeriolabSessionsListComponent.prototype.selectedMaterialWorking = function (m) {
        if (m) {
            this.materialWorking = m;
        }
        else {
            this.materialWorking = new material_1.Material();
        }
        this.workingSessionsTable.reload();
    };
    SeriolabSessionsListComponent.prototype.controlPlanNameWorkingChange = function () {
        if (this.timeoutNameWorking) {
            clearTimeout(this.timeoutNameWorking);
        }
        var self = this;
        this.timeoutNameWorking = setTimeout(function () {
            self.workingSessionsTable.reload();
        }, 500);
    };
    SeriolabSessionsListComponent.prototype.controlPlanNameCompletedChange = function () {
        if (this.timeoutNameCompleted) {
            clearTimeout(this.timeoutNameCompleted);
        }
        var self = this;
        this.timeoutNameCompleted = setTimeout(function () {
            self.closedSessionsTable.reload();
        }, 500);
    };
    SeriolabSessionsListComponent.prototype.selectedMaterialCompleted = function (m) {
        if (m) {
            this.materialCompleted = m;
        }
        else {
            this.materialCompleted = new material_1.Material();
        }
        this.closedSessionsTable.reload();
    };
    SeriolabSessionsListComponent.prototype.handleDateCompletedChange = function () {
        //TODO
    };
    return SeriolabSessionsListComponent;
}());
__decorate([
    core_1.ViewChild('workingSessionsTable')
], SeriolabSessionsListComponent.prototype, "workingSessionsTable", void 0);
__decorate([
    core_1.ViewChild('closedSessionsTable')
], SeriolabSessionsListComponent.prototype, "closedSessionsTable", void 0);
SeriolabSessionsListComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-sessions-list',
        templateUrl: 'seriolab-sessions-list.component.html',
        styleUrls: ['seriolab-sessions-list.component.css']
    })
], SeriolabSessionsListComponent);
exports.SeriolabSessionsListComponent = SeriolabSessionsListComponent;
