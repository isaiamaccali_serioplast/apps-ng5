import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabSessionsListComponent} from "./seriolab-sessions-list.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SeriolabSessionsListComponent],   // components and directives
  exports:[SeriolabSessionsListComponent],
  providers: []                    // services
})
export class SeriolabSessionsListModule { }


