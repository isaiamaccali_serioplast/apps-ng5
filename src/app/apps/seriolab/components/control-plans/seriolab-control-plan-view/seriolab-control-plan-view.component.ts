import {Component, OnInit, Input} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ControlPlan, ControlPlansService} from "../../../../_common/services/control-plans.service";
import {PropertySet, PropertySetService} from "../../../../_common/services/property-set.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {
  ControlPlanMeasure,
  ControlPlanMeasuresService
} from "../../../../_common/services/control-plan-measures.service";
import {PropertiesService, Property} from "../../../../_common/services/properties.service";
import {ContentType, ContentTypeService} from "../../../../_common/services/content-type.service";
import {Site} from "../../../../_common/services/sites.service";
import {Material} from "../../../../_common/services/materials.service";
import {parseSerionetRespToData} from "../../../../../shared/utils";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-seriolab-control-plan-view',
  templateUrl: 'seriolab-control-plan-view.component.html',
  styleUrls: ['seriolab-control-plan-view.component.css']
})
export class SeriolabControlPlanViewComponent extends TranslatableComponent implements OnInit {

  @Input('cp') cp:number;
  controlPlan: ControlPlan;
  propertySets:PropertySet[] = [];
  controlPlanCT = new ContentType();

  constructor(private activatedRoute: ActivatedRoute, private controlPlanService: ControlPlansService,
              private router: Router, private propertySetsService: PropertySetService,
              protected translationService: TranslationService, private controlPlanMeasureService: ControlPlanMeasuresService,
              private propertiesService: PropertiesService, private contentTypeService:ContentTypeService) {
    super(translationService);
    this.controlPlan = new ControlPlan();
    this.controlPlan.site = new Site();
    this.controlPlan.measures = [];
  }

  ngOnInit() {
    this.contentTypeService.getContentTypes({model:'controlplan', is_active:true}).subscribe((data)=>{
      if(data && data['count'] && data['count']>0){
        this.controlPlanCT = data['results'][0];
      }
    });

    this.activatedRoute.params.subscribe((params) => {
      if (params['cp']) {
        this.controlPlanService.getControlPlan(parseInt(params['cp'] + "", 10)).subscribe((cp) => {
          if (cp.pk) {
            this.controlPlan = cp;
            this.reloadPS();
          }
          else {
            this.router.navigate(["seriolab", "control-plans"]);
          }
        })
      }
    });

    if(this.cp){
      this.controlPlanService.getControlPlan(this.cp).subscribe((cp) => {
        if (cp.pk) {
          this.controlPlan = cp;
          this.reloadPS();
        }
        else {
          this.router.navigate(["seriolab", "control-plans"]);
        }
      })
    }
  }

  back() {
    this.router.navigate(["seriolab", "control-plans"]);
  }


  reloadPS() {
    this.controlPlan.measures = [];
    this.controlPlanMeasureService.getControlPlanMeasures({control_plan:this.controlPlan.pk, is_active:true}).subscribe((data) => {
      let cpms = parseSerionetRespToData(data);
      this.propertySetsService.getPropertySets({
        material__pk: this.controlPlan.controlplanmaterial_set.map((cpm)=>cpm.material.pk).join("|"),
        is_active: true
      }).subscribe((data) => {
        this.propertySets = parseSerionetRespToData(data);
        this.fixMeasures(cpms, 0, ()=>{
          this.controlPlan.measures = cpms;
        });
      });
    });
  }

  fixMeasures(cpms: ControlPlanMeasure[], index, callback) {
    if (cpms[index]) {
      this.propertiesService.getProperty(cpms[index].property.pk).subscribe((p) => {
        cpms[index].property = p;
        this.fixMeasures(cpms, index + 1, callback);
      });
    }
    else {
      callback.call(this);
    }
  }

  doesMaterialHaveProperty(p:Property,m:Material){
    return this.propertySets.filter((ps)=>ps.property.pk==p.pk && ps.material.pk==m.pk).length>0
  }

}
