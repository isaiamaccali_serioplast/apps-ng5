"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var control_plan_1 = require("../../../../_common/models/control-plan");
var site_1 = require("../../../../_common/models/site");
var contenttype_1 = require("../../../../_common/models/contenttype");
var SeriolabControlPlanViewComponent = (function () {
    function SeriolabControlPlanViewComponent(activatedRoute, controlPlanService, router, propertySetsService, translationService, controlPlanMeasureService, propertiesService, contentTypeService) {
        this.activatedRoute = activatedRoute;
        this.controlPlanService = controlPlanService;
        this.router = router;
        this.propertySetsService = propertySetsService;
        this.translationService = translationService;
        this.controlPlanMeasureService = controlPlanMeasureService;
        this.propertiesService = propertiesService;
        this.contentTypeService = contentTypeService;
        this.propertySets = [];
        this.controlPlanCT = new contenttype_1.ContentType();
        this.controlPlan = new control_plan_1.ControlPlan();
        this.controlPlan.site = new site_1.Site();
        this.controlPlan.measures = [];
    }
    SeriolabControlPlanViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contentTypeService.getContentTypes({ model: 'controlplan' }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.controlPlanCT = data['results'][0];
            }
        });
        this.activatedRoute.params.subscribe(function (params) {
            if (params['cp']) {
                _this.controlPlanService.getControlPlan(parseInt(params['cp'] + "", 10)).subscribe(function (cp) {
                    if (cp.pk) {
                        _this.controlPlan = cp;
                        _this.reloadPS();
                    }
                    else {
                        _this.router.navigate(["seriolab", "control-plans"]);
                    }
                });
            }
        });
        if (this.cp) {
            this.controlPlanService.getControlPlan(this.cp).subscribe(function (cp) {
                if (cp.pk) {
                    _this.controlPlan = cp;
                    _this.reloadPS();
                }
                else {
                    _this.router.navigate(["seriolab", "control-plans"]);
                }
            });
        }
    };
    SeriolabControlPlanViewComponent.prototype.back = function () {
        this.router.navigate(["seriolab", "control-plans"]);
    };
    SeriolabControlPlanViewComponent.prototype.reloadPS = function () {
        var _this = this;
        var self = this;
        this.propertySetsService.getPropertySets({
            material: this.controlPlan.material.pk,
            is_active: true
        }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                var ps_1 = data['results'].sort(function (ps1, ps2) {
                    return ps1.delta - ps2.delta;
                });
                _this.fixPropertySets(ps_1, 0, function () {
                    var _this = this;
                    this.propertySets = ps_1;
                    if (this.controlPlan.pk) {
                        this.controlPlan.measures = [];
                        this.controlPlanMeasureService.getControlPlanMeasures({ control_plan: this.controlPlan.pk, is_active: true }).subscribe(function (data) {
                            if (data && data['count'] && data['count'] > 0) {
                                var cpms_1 = data['results'];
                                self.fixMeasures(cpms_1, 0, function () {
                                    self.controlPlan.measures = cpms_1;
                                });
                            }
                            else {
                                _this.controlPlan.measures = [];
                            }
                        });
                    }
                });
            }
        });
    };
    SeriolabControlPlanViewComponent.prototype.fixPropertySets = function (ps, index, callback) {
        var _this = this;
        if (ps[index]) {
            this.propertySetsService.getPropertySet(ps[index].pk).subscribe(function (ps_) {
                ps[index] = ps_;
                _this.fixPropertySets(ps, index + 1, callback);
            });
        }
        else {
            callback.call(this);
        }
    };
    SeriolabControlPlanViewComponent.prototype.fixMeasures = function (cpms, index, callback) {
        var _this = this;
        if (cpms[index]) {
            var ps = this.propertySets.filter(function (ps) {
                return ps.pk == cpms[index].property_set.pk;
            });
            if (ps.length == 1) {
                //se ce l'ho già (maggior parte dei casi) evito la query e arricchisco il dato
                cpms[index].property_set = ps[0];
                this.fixMeasures(cpms, index + 1, callback);
            }
            else {
                this.propertySetsService.getPropertySet(cpms[index].property_set.pk).subscribe(function (data) {
                    cpms[index].property_set = data;
                    _this.propertiesService.getProperty(cpms[index].property_set.property.pk).subscribe(function (p) {
                        cpms[index].property_set.property = p;
                        _this.fixMeasures(cpms, index + 1, callback);
                    });
                });
            }
            this.controlPlan.measures = cpms;
        }
        else {
            callback.call(this);
        }
    };
    SeriolabControlPlanViewComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    return SeriolabControlPlanViewComponent;
}());
__decorate([
    core_1.Input('cp')
], SeriolabControlPlanViewComponent.prototype, "cp", void 0);
SeriolabControlPlanViewComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-control-plan-view',
        templateUrl: 'seriolab-control-plan-view.component.html',
        styleUrls: ['seriolab-control-plan-view.component.css']
    })
], SeriolabControlPlanViewComponent);
exports.SeriolabControlPlanViewComponent = SeriolabControlPlanViewComponent;
