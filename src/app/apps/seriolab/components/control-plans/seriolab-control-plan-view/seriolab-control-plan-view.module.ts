import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabControlPlanViewComponent} from "./seriolab-control-plan-view.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SeriolabControlPlanViewComponent],   // components and directives
  exports:[SeriolabControlPlanViewComponent],
  providers: []                    // services
})
export class SeriolabControlPlanViewModule { }


