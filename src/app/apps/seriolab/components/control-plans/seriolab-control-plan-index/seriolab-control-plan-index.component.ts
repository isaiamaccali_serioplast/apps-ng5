import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {LoginService} from "../../../../../shared/login/service/login.service";

@Component({
  selector: 'app-seriolab-control-plan-index',
  templateUrl: 'seriolab-control-plan-index.component.html',
  styleUrls: ['seriolab-control-plan-index.component.css']
})
export class SeriolabControlPlanIndexComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {}

   logout() {
    this.loginService.doLogout().first().subscribe(() => {
      this.router.navigate(["/login"]);
    });
  }
}
