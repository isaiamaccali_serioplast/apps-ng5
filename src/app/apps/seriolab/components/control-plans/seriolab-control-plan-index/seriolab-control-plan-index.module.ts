import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabControlPlanIndexComponent} from "./seriolab-control-plan-index.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SeriolabControlPlanIndexComponent],   // components and directives
  exports:[SeriolabControlPlanIndexComponent],
  providers: []                    // services
})
export class SeriolabControlPlanIndexModule { }


