"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var control_plan_measure_1 = require("../../../../_common/models/control-plan-measure");
var control_plan_1 = require("../../../../_common/models/control-plan");
var utils_1 = require("../../../../../shared/utils");
var site_1 = require("../../../../_common/models/site");
var contenttype_1 = require("../../../../_common/models/contenttype");
var SeriolabControlPlanEditComponent = (function () {
    function SeriolabControlPlanEditComponent(activatedRoute, controlPlanService, router, messageService, loginService, propertySetsService, translationService, controlPlanMeasureService, frequencyService, propertiesService, userSitesService, contentTypeService) {
        this.activatedRoute = activatedRoute;
        this.controlPlanService = controlPlanService;
        this.router = router;
        this.messageService = messageService;
        this.loginService = loginService;
        this.propertySetsService = propertySetsService;
        this.translationService = translationService;
        this.controlPlanMeasureService = controlPlanMeasureService;
        this.frequencyService = frequencyService;
        this.propertiesService = propertiesService;
        this.userSitesService = userSitesService;
        this.contentTypeService = contentTypeService;
        this.edited = false;
        this.propertySets = [];
        this.materials = [];
        this.site = new site_1.Site();
        this.sites = [];
        this.materialTypes = [];
        this.frequencies = [];
        this.controlPlanCT = new contenttype_1.ContentType();
        this.isCloned = false;
        this.controlPlan = new control_plan_1.ControlPlan();
        this.controlPlan.site = new site_1.Site();
        this.controlPlan.measures = [];
    }
    SeriolabControlPlanEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            if (params['cp']) {
                _this.controlPlanService.getControlPlan(parseInt(params['cp'] + "", 10)).subscribe(function (cp) {
                    if (cp.pk) {
                        _this.controlPlan = cp;
                        _this.reloadPS();
                    }
                    else {
                        _this.router.navigate(["seriolab", "control-plans"]);
                    }
                });
            }
        });
        this.activatedRoute.queryParams.subscribe(function (params) {
            if (params['model']) {
                _this.controlPlanService.getControlPlan(parseInt(params['model'] + "", 10)).subscribe(function (cp) {
                    if (cp.pk) {
                        _this.controlPlan = utils_1.cloneObject(cp);
                        _this.isCloned = true;
                        var self_1 = _this;
                        _this.reloadPS(function () {
                            delete self_1.controlPlan['pk'];
                            self_1.controlPlan.measures.forEach(function (cpm, index) {
                                var cpm_ = utils_1.cloneObject(cpm);
                                if (cpm_.pk) {
                                    delete cpm_['pk'];
                                }
                                cpm_.object_id = null;
                                cpm_.status_custom = "toSave";
                                self_1.controlPlan.measures[index] = cpm_;
                            });
                        });
                    }
                    else {
                        _this.router.navigate(["seriolab", "control-plans"]);
                    }
                });
            }
        });
        this.userSitesService.getUserSites(this.loginService.user.pk).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.sites = data['results'];
                if (!_this.controlPlan.site)
                    _this.controlPlan.site = _this.sites[0];
            }
        });
        this.frequencyService.getFrequencies({ is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.frequencies = data['results'];
            }
        });
        this.contentTypeService.getContentTypes({ model: 'controlplan' }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.controlPlanCT = data['results'][0];
            }
        });
    };
    SeriolabControlPlanEditComponent.prototype.saveControlPlan = function () {
        var _this = this;
        var cp = utils_1.cloneObject(this.controlPlan);
        var measures = cp.measures;
        delete cp['measures'];
        var self = this;
        this.controlPlanService.saveControlPlan(cp).subscribe(function (resp) {
            _this.handleSaveOrUpdate(resp, function () {
                measures.forEach(function (m) {
                    m.control_plan = resp;
                });
                self.processMeasures(measures, 0, function (isOK, errorResp) {
                    if (isOK) {
                        self.messageService.open({
                            message: self.tr('SAVE_COMPLETED'),
                            title: " ",
                            autoClose: false,
                            onClose: function () {
                                self.router.navigate(["seriolab", "control-plans"]);
                            }
                        });
                    }
                    else {
                        errorResp.context = "SERIONET";
                        self.messageService.open({
                            title: 'Error',
                            error: true,
                            message: utils_1.buildErrorMessage(errorResp),
                            autoClose: false
                        });
                    }
                });
            }, true);
        });
    };
    SeriolabControlPlanEditComponent.prototype.processMeasures = function (measures, index, callback, lastResponse) {
        var _this = this;
        if (measures[index]) {
            var m = measures[index];
            var s = m.status_custom;
            switch (s) {
                case "toSave":
                    this.controlPlanMeasureService.saveControlPlanMeasure(m).subscribe(function (resp) {
                        if (resp.pk) {
                            _this.processMeasures(measures, index + 1, callback, resp);
                        }
                        else {
                            callback.call(null, false, resp);
                        }
                    });
                    break;
                case "toDelete":
                    this.controlPlanMeasureService.deleteControlPlanMeasure(m).subscribe(function (resp) {
                        if (resp.status == 204) {
                            _this.processMeasures(measures, index + 1, callback, resp);
                        }
                        else {
                            callback.call(null, false, resp);
                        }
                    });
                    break;
                default:
                    this.processMeasures(measures, index + 1, callback, lastResponse);
            }
        }
        else {
            callback.call(null, true);
        }
    };
    SeriolabControlPlanEditComponent.prototype.back = function () {
        if (this.edited) {
            var self_2 = this;
            this.messageService.open({
                confirm: function () {
                    self_2.router.navigate(["seriolab", "control-plans"]);
                },
                deny: function () {
                },
                title: 'ATTENTION',
                message: 'ATTENTION_DATA_LOSS'
            });
        }
        else {
            this.router.navigate(["seriolab", "control-plans"]);
        }
    };
    SeriolabControlPlanEditComponent.prototype.isPSAddedToMeasures = function (ps) {
        return this.controlPlan.measures.filter(function (m) {
            return m.is_active;
        }).map(function (m) {
            return m.property_set.pk;
        }).indexOf(ps.pk) != -1;
    };
    SeriolabControlPlanEditComponent.prototype.togglePSAsMeasure = function (ps) {
        var existing = this.controlPlan.measures.filter(function (element) {
            return element.property_set.pk == ps.pk;
        });
        if (this.isPSAddedToMeasures(ps)) {
            //Sto togliendo una property, recupero l'instanza esistente le la preparo per il salvataggio (se non ha pk verrà skippata)
            existing[0].is_active = false;
            if (existing[0].pk) {
                existing[0].status_custom = "toDelete";
            }
        }
        else {
            if (existing.length == 0) {
                var measure = new control_plan_measure_1.ControlPlanMeasure();
                measure.is_active = true;
                measure.status_custom = "toSave";
                measure.property_set = ps;
                this.controlPlan.measures.push(measure);
            }
            else {
                existing[0].is_active = true;
                existing[0].status_custom = "toSave";
            }
        }
    };
    SeriolabControlPlanEditComponent.prototype.moveMeasureUp = function (m) {
        utils_1.moveUpElementInList(m, this.controlPlan.measures);
    };
    SeriolabControlPlanEditComponent.prototype.moveMeasureDown = function (m) {
        utils_1.moveDownElementInList(m, this.controlPlan.measures);
    };
    SeriolabControlPlanEditComponent.prototype.removeMeasure = function (m) {
        m.is_active = false;
        m.status_custom = "toDelete";
    };
    SeriolabControlPlanEditComponent.prototype.reloadPS = function (callback) {
        var _this = this;
        var self = this;
        this.propertySetsService.getPropertySets({
            material: this.controlPlan.material.pk,
            is_active: true
        }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                var ps_1 = data['results'].sort(function (ps1, ps2) {
                    return ps1.delta - ps2.delta;
                });
                _this.fixPropertySets(ps_1, 0, function () {
                    var _this = this;
                    this.propertySets = ps_1;
                    if (this.controlPlan.pk) {
                        this.controlPlan.measures = [];
                        this.controlPlanMeasureService.getControlPlanMeasures({ object_id: this.controlPlan.pk, control_plan: this.controlPlan.pk, is_active: true }).subscribe(function (data) {
                            if (data && data['count'] && data['count'] > 0) {
                                var cpms_1 = data['results'];
                                self.fixMeasures(cpms_1, 0, function () {
                                    self.controlPlan.measures = cpms_1;
                                    if (callback)
                                        callback.call(self);
                                });
                            }
                            else {
                                _this.controlPlan.measures = [];
                                if (callback)
                                    callback.call(self);
                            }
                        });
                    }
                });
            }
        });
    };
    SeriolabControlPlanEditComponent.prototype.fixPropertySets = function (ps, index, callback) {
        var _this = this;
        if (ps[index]) {
            this.propertySetsService.getPropertySet(ps[index].pk).subscribe(function (ps_) {
                ps[index] = ps_;
                _this.propertiesService.getProperty(ps[index].property.pk).subscribe(function (p) {
                    ps[index].property = p;
                    _this.fixPropertySets(ps, index + 1, callback);
                });
            });
        }
        else {
            callback.call(this);
        }
    };
    SeriolabControlPlanEditComponent.prototype.fixMeasures = function (cpms, index, callback) {
        var _this = this;
        if (cpms[index]) {
            var ps = this.propertySets.filter(function (ps) {
                return ps.pk == cpms[index].property_set.pk;
            });
            if (ps.length == 1) {
                //se ce l'ho già (maggior parte dei casi) evito la query e arricchisco il dato
                cpms[index].property_set = ps[0];
                this.fixMeasures(cpms, index + 1, callback);
            }
            else {
                this.propertySetsService.getPropertySet(cpms[index].property_set.pk).subscribe(function (data) {
                    cpms[index].property_set = data;
                    _this.propertiesService.getProperty(cpms[index].property_set.property.pk).subscribe(function (p) {
                        cpms[index].property_set.property = p;
                        _this.fixMeasures(cpms, index + 1, callback);
                    });
                });
            }
            this.controlPlan.measures = cpms;
        }
        else {
            callback.call(this);
        }
    };
    SeriolabControlPlanEditComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    SeriolabControlPlanEditComponent.prototype.selectedMaterial = function (m) {
        if (m && m.pk) {
            this.controlPlan.material = m;
            this.controlPlan.measures = [];
            this.reloadPS();
        }
        else {
            this.controlPlan.material = null;
        }
    };
    SeriolabControlPlanEditComponent.prototype.siteChange = function () {
        utils_1.fixValue(this.sites, this.controlPlan, 'site');
    };
    SeriolabControlPlanEditComponent.prototype.handleSaveOrUpdate = function (resp, callback, disableMessage) {
        if (disableMessage === void 0) { disableMessage = false; }
        var self = this;
        if (resp.pk) {
            if (!disableMessage) {
                self.messageService.open({
                    message: self.tr('SAVE_COMPLETED'),
                    title: " ",
                    autoClose: false,
                    onClose: function () {
                        if (callback)
                            callback();
                    }
                });
            }
            else {
                if (callback)
                    callback();
            }
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    SeriolabControlPlanEditComponent.prototype.handleDelete = function (resp, callback) {
        var self = this;
        if (resp.status == 204) {
            self.messageService.open({
                title: " ",
                message: self.tr('DELETE_COMPLETED'),
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    return SeriolabControlPlanEditComponent;
}());
SeriolabControlPlanEditComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-control-plan-edit',
        templateUrl: 'seriolab-control-plan-edit.component.html',
        styleUrls: ['seriolab-control-plan-edit.component.css']
    })
], SeriolabControlPlanEditComponent);
exports.SeriolabControlPlanEditComponent = SeriolabControlPlanEditComponent;
