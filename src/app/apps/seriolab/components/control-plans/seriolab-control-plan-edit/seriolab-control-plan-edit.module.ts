import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabControlPlanEditComponent} from "./seriolab-control-plan-edit.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SeriolabControlPlanEditComponent],   // components and directives
  exports:[SeriolabControlPlanEditComponent],
  providers: []                    // services
})
export class SeriolabControlPlanEditModule { }


