import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ControlPlan, ControlPlanMaterial, ControlPlansService} from "../../../../_common/services/control-plans.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {PropertySet, PropertySetService} from "../../../../_common/services/property-set.service";
import {arrayDistinct, arrayIntersect, buildErrorMessage, cloneObject, fixValue, moveDownElementInList, moveUpElementInList, parseSerionetRespToData} from "../../../../../shared/utils";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {ControlPlanMeasure, ControlPlanMeasuresService} from "../../../../_common/services/control-plan-measures.service";
import {Frequency, FrequencyService} from "../../../../_common/services/frequency.service";
import {PropertiesService, Property} from "../../../../_common/services/properties.service";
import {ContentType, ContentTypeService} from "../../../../_common/services/content-type.service";
import {TypeLink, TypeLinkService} from "../../../../_common/services/type-link.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {Criteria, CriteriaService} from "../../../../_common/services/criteria.service";
import {MaterialType} from "../../../../_common/services/material-type.service";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {ReleaseCriteria} from "../../../../_common/services/release-criteria.service";
import {Site} from "../../../../_common/services/sites.service";
import {ShapesService} from "../../../../_common/services/shapes.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-seriolab-control-plan-edit',
  templateUrl: 'seriolab-control-plan-edit.component.html',
  styleUrls: ['seriolab-control-plan-edit.component.css']
})
export class SeriolabControlPlanEditComponent extends BaseComponent implements OnInit {

  @ViewChild('allContainer') allContainer: HTMLDivElement;
  controlPlan: ControlPlan;
  edited: boolean = false;
  propertySets: PropertySet[] = [];
  materials: Material[] = [];
  materialTypes: MaterialType[] = [];
  materialType: number;
  frequencies: Frequency[] = [];
  controlPlanCT = new ContentType();
  isCloned = false;
  types: TypeLink[] = [];
  criteria: Criteria[] = [];
  measureEditOn = false;
  measureToEdit: ControlPlanMeasure;
  properties: Property[];

  constructor(private activatedRoute: ActivatedRoute, private controlPlanService: ControlPlansService,
              private router: Router, protected messageService: MessageService,
              private propertySetsService: PropertySetService, protected translationService: TranslationService,
              private controlPlanMeasureService: ControlPlanMeasuresService, private frequencyService: FrequencyService,
              private propertiesService: PropertiesService, private shapesService: ShapesService,
              private contentTypeService: ContentTypeService, private typeLinkService: TypeLinkService,
              protected loadingService: LoadingService, private criteriaService: CriteriaService,
              private materialsService: MaterialsService) {
    super(translationService, messageService, loadingService);
    this.controlPlan = new ControlPlan();
    this.controlPlan.type = new TypeLink();
    this.controlPlan.site = new Site();
    this.controlPlan.frequency = new Frequency();
    this.controlPlan.measures = [];
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['cp']) {
        this.controlPlanService.getControlPlan(parseInt(params['cp'] + "", 10)).subscribe((cp) => {
          if (cp.pk) {
            this.controlPlan = cp;
            this.loadingService.show();
            this.materialsService.getMaterial(this.controlPlan.controlplanmaterial_set[0].material.pk).subscribe((m: Material) => {
              this.loadMaterials(m, (materials: Material[]) => {
                let currentSet: ControlPlanMaterial[] = cloneObject(this.controlPlan.controlplanmaterial_set);
                this.controlPlan.controlplanmaterial_set = [];
                materials.forEach((m) => {
                  let match = currentSet.filter((cpm) => cpm.material.pk == m.pk);
                  if (match && match[0]) {
                    this.controlPlan.controlplanmaterial_set.push(match[0]);
                  }
                  else {
                    let cpm = new ControlPlanMaterial();
                    cpm.material = m;
                    cpm.is_active = false;
                    this.controlPlan.controlplanmaterial_set.push(cpm);
                  }
                });
                this.reloadPS(() => {
                  this.loadingService.hide();
                });
              });
            })

          }
          else {
            this.router.navigate(["seriolab", "control-plans"]);
          }
        })
      }
    });
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['model']) {
        this.controlPlanService.getControlPlan(parseInt(params['model'] + "", 10)).subscribe((cp) => {
          if (cp.pk) {
            this.controlPlan = cloneObject(cp);
            this.isCloned = true;
            this.handleCloneCP();
          }
          else {
            this.router.navigate(["seriolab", "control-plans"]);
          }
        })
      }
    });

    this.frequencyService.getFrequencies({is_active: true}).subscribe((data) => {
      this.frequencies = parseSerionetRespToData(data);
    });

    this.contentTypeService.getContentTypes({model: 'controlplan', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.controlPlanCT = data['results'][0];
        this.typeLinkService.getTypeLinks({is_active: true, app: this.controlPlanCT.pk}).subscribe((resp) => {
          this.types = parseSerionetRespToData(resp);
        })
      }
    });

    this.criteriaService.getCriterias({is_active: true}).subscribe((resp) => {
      this.criteria = parseSerionetRespToData(resp);
    })
  }

  saveControlPlan(callback?: any) {
    let cp = cloneObject(this.controlPlan);
    delete cp['measures'];
    let isAlreadySaved = cp.pk;
    this.loadingService.show();
    this.controlPlanService.saveControlPlan(cp).subscribe((resp) => {
      this.loadingService.hide();
      this.manageRespSave(resp, () => {
        if (isAlreadySaved)
          this.router.navigate(["seriolab", "control-plans"]);
        else {
          this.controlPlan.pk = resp.pk;
          if (callback)
            callback.call(this);
          else
            this.router.navigate(["seriolab", "control-plans", "edit", resp.pk]);
        }
      });
    });
  }

  back() {
    if (this.edited) {
      this.messageService.open({
        confirm: () => {
          this.router.navigate(["seriolab", "control-plans"]);
        },
        deny: () => {

        },
        title: 'ATTENTION',
        message: 'ATTENTION_DATA_LOSS'
      })
    }
    else {
      this.router.navigate(["seriolab", "control-plans"]);
    }
  }

  isPropAddedToMeasures(p: Property) {
    return this.controlPlan.measures.filter((m) => {
      return m.is_active
    }).map((m) => {
      return m.property.pk
    }).indexOf(p.pk) != -1;
  }

  togglePropAsMeasure(p: Property) {
    let existing = this.controlPlan.measures.filter((element) => {
      return element.property.pk == p.pk;
    });
    if (existing.length > 0) {
      //Sto togliendo una property, recupero l'instanza esistente le la preparo per il salvataggio (se non ha pk verrà skippata)
      this.removeMeasure(existing[0]);
    }
    else {
      let addProp = () => {
        let measure = new ControlPlanMeasure();
        measure.is_active = true;
        measure.property = p;
        measure.release_criteria_list = [];
        measure.or_operator = false;
        measure.control_plan = this.controlPlan;
        measure.repeats = 1;
        this.editMeasure(measure);
        let div = this.allContainer['nativeElement'];
        div.scrollTop = document.querySelector(".properties-container")['offsetTop'] - 100;
      };
      // if(!p.calculated){
      addProp();
      // }
      // else {
      //   let setProperties = this.controlPlan.measures.map((cpm)=>cpm.property.pk);
      //   let isAllSet = true;
      //   p.parameter_list.forEach((p)=>{
      //     if(setProperties.indexOf(p.property)==-1){
      //       isAllSet = false;
      //
      //     }
      //   });
      // }

    }
  }

  moveMeasureUp(m: ControlPlanMeasure) {
    this.loadingService.show();
    this.controlPlanMeasureService.moveUpItem(m).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.pk) {
        let measures = moveUpElementInList(m, this.controlPlan.measures);
        this.controlPlan.measures = [];
        setTimeout(() => {
          this.controlPlan.measures = measures;
        }, 200)
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  moveMeasureDown(m: ControlPlanMeasure) {
    this.loadingService.show();
    this.controlPlanMeasureService.moveDownItem(m).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.pk) {
        let measures = moveDownElementInList(m, this.controlPlan.measures);
        this.controlPlan.measures = [];
        setTimeout(() => {
          this.controlPlan.measures = measures;
        }, 200)

      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  removeMeasure(m: ControlPlanMeasure) {
    this.messageService.open({
      confirm: () => {
        this.controlPlanMeasureService.deleteControlPlanMeasure(m).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.reloadCPM();
          })
        })
      },
      deny: () => {

      },
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE')
    })
  }

  editMeasure(m: ControlPlanMeasure) {
    this.measureToEdit = cloneObject(m);
    this.measureToEdit.release_criteria_list.forEach((rc) => {

      this.fixCriteria(rc);
    });
    this.measureEditOn = true;
  }

  reloadPS(callback?: any) {
    this.propertySetsService.getPropertySets({
      material__pk: this.controlPlan.controlplanmaterial_set.map((cpm) => cpm.material.pk).join("|"),
      is_active: true
    }).subscribe((data) => {
      let ps = parseSerionetRespToData(data);
      this.fixPropertySets(ps, 0, () => {
        this.propertySets = ps;
        let propertiesPK = arrayDistinct(this.propertySets.map((ps) => ps.property.pk));
        this.properties = propertiesPK.map((pPK) => {
          return this.propertySets.filter((ps) => ps.property.pk == pPK).map((ps) => ps.property)[0];
        });
        this.checkMaterials();
        if (this.controlPlan.pk) {
          this.reloadCPM(callback);
        }
      })
    });
  }

  checkMaterials() {
    this.controlPlan.controlplanmaterial_set.forEach((cpm) => {
      if (this.propertySets.map((ps) => ps.material.pk).indexOf(cpm.material.pk) == -1) {
        cpm.is_active = false;
        cpm.disabled = true;
      }
    })
  }

  reloadCPM(callback?: any) {
    this.controlPlan.measures = [];
    this.controlPlanMeasureService.getControlPlanMeasures({control_plan: this.controlPlan.pk, is_active: true}).subscribe((data) => {
      let cpms = parseSerionetRespToData(data);
      this.fixMeasures(cpms, 0, () => {
        this.controlPlan.measures = cpms;
        if (callback)
          callback.call(this);
      });
    })
  }

  fixPropertySets(ps: PropertySet[], index, callback) {
    if (ps[index]) {
      this.propertySetsService.getPropertySet(ps[index].pk).subscribe((ps_) => {
        ps[index] = ps_;
        this.propertiesService.getProperty(ps[index].property.pk).subscribe((p) => {
          ps[index].property = p;
          this.fixPropertySets(ps, index + 1, callback);
        });
      })
    }
    else {
      callback.call(this);
    }
  }

  fixMeasures(cpms: ControlPlanMeasure[], index, callback) {
    if (cpms[index]) {
      let ps = this.properties.filter((p) => {
        return p.pk == cpms[index].property.pk;
      });
      if (ps.length == 1) {
        //se ce l'ho già (maggior parte dei casi) evito la query e arricchisco il dato
        cpms[index].property = ps[0];
        this.fixMeasures(cpms, index + 1, callback);
      }
      else {
        this.propertiesService.getProperty(cpms[index].property.pk).subscribe((p) => {
          cpms[index].property = p;
          this.fixMeasures(cpms, index + 1, callback);
        });
      }
      this.controlPlan.measures = cpms;
    }
    else {
      callback.call(this);
    }
  }

  selectedMaterial(m: Material) {
    if (m && m.pk) {

      this.loadMaterials(m, (materials) => {
        this.controlPlan.controlplanmaterial_set = materials.map((m) => {
          let cpm = new ControlPlanMaterial();
          cpm.material = m;
          cpm.is_active = false;
          return cpm;
        })
      });

      this.controlPlan.measures = [];
      // this.reloadPS();
    }
    else {
      //this.controlPlan.material = null;
    }
  }

  loadMaterials(m: Material, callback: any) {
    this.materials = [];
    if (m.sub_model && m.sub_model.shape) {
      this.shapesService.getShape(parseInt(m.sub_model.shape + "", 10)).subscribe((resp) => {
        callback(resp.material_list);
      });
    }
    else {
      callback([m]);
    }
  }

  siteChange(s: Site) {
    if (s && s.pk) {
      this.controlPlan.site = s;
    }
    else {
      this.controlPlan.site = new Site();
    }
  }

  typeChange() {
    fixValue(this.types, this.controlPlan, 'type');
  }

  canAddRC(measureToEdit: ControlPlanMeasure) {
    let isOk = true;
    measureToEdit.release_criteria_list.forEach((rc) => {
      if (!rc.criteria) isOk = false;
    });
    return isOk;
  }

  addRC(measureToEdit: ControlPlanMeasure) {
    let rc = new ReleaseCriteria();
    rc.parameters = "";
    measureToEdit.release_criteria_list.push(rc);
  }

  removeRC(measureToEdit: ControlPlanMeasure, rc: ReleaseCriteria) {
    measureToEdit.release_criteria_list.splice(measureToEdit.release_criteria_list.indexOf(rc), 1);
  }

  undoEditMeasure() {
    this.measureEditOn = false;
  }

  saveEditMeasure() {
    this.loadingService.show();
    this.controlPlanMeasureService.saveControlPlanMeasure(this.measureToEdit).subscribe((resp) => {
      this.loadingService.hide();
      this.manageRespSave(resp, () => {
        this.measureEditOn = false;
        this.reloadCPM();
      })
    })
  }

  canSaveMeasure() {
    return this.measureToEdit && (this.measureToEdit.property.calculated ? true : this.measureToEdit.repeats > 0)
      && this.measureToEdit.release_criteria_list.length > 0 && this.canAddRC(this.measureToEdit)
  }

  handleCloneCP() {
    this.loadingService.show();
    this.materialsService.getMaterial(this.controlPlan.controlplanmaterial_set[0].material.pk).subscribe((m: Material) => {
      this.loadMaterials(m, (materials: Material[]) => {
        let currentSet: ControlPlanMaterial[] = cloneObject(this.controlPlan.controlplanmaterial_set);
        this.controlPlan.controlplanmaterial_set = [];
        materials.forEach((m) => {
          let match = currentSet.filter((cpm) => cpm.material.pk == m.pk);
          if (match && match[0]) {
            this.controlPlan.controlplanmaterial_set.push(match[0]);
          }
          else {
            let cpm = new ControlPlanMaterial();
            cpm.material = m;
            cpm.is_active = false;
            this.controlPlan.controlplanmaterial_set.push(cpm);
          }
        });

        this.controlPlan.controlplanmaterial_set.forEach((cpm) => {
          delete cpm.pk;
          delete cpm.control_plan;
        });

        this.controlPlanMeasureService.getControlPlanMeasures({control_plan: this.controlPlan.pk, is_active: true}).subscribe((data) => {
          let cpms = parseSerionetRespToData(data);
          delete this.controlPlan.pk;
          this.controlPlan.description += "_cloned";
          this.saveControlPlan(() => {
            cpms.forEach((cpm: ControlPlanMeasure) => {
              delete cpm.pk;
              cpm.control_plan.pk = this.controlPlan.pk;
            });
            this.loadingService.show();
            this.processClonedMeasures(cpms, 0, () => {
              this.loadingService.hide();
              this.router.navigate(["seriolab", "control-plans", "edit", this.controlPlan.pk]);
            })
          });
        });
      });
    })
  }

  processClonedMeasures(measures: ControlPlanMeasure[], index: number, callback: any) {
    if (measures[index]) {
      this.controlPlanMeasureService.saveControlPlanMeasure(measures[index]).subscribe((resp) => {
        this.manageRespSave(resp, () => {
          this.processClonedMeasures(measures, index + 1, callback);
        }, true);
      })
    }
    else {
      callback.call(this);
    }
  }

  doesMaterialHaveProperty(p: Property, m: Material) {
    return this.propertySets.filter((ps) => ps.property.pk == p.pk && ps.material.pk == m.pk).length > 0
  }

  canSave() {
    return this.controlPlan &&
      this.controlPlan.controlplanmaterial_set && this.controlPlan.controlplanmaterial_set.length > 0 && this.controlPlan.controlplanmaterial_set.filter((cpm) => cpm.is_active).length > 0 &&
      (this.controlPlan.pk && this.controlPlan.measures ? this.controlPlan.measures.length > 0 : true)
  }

  fixCriteria(rc: ReleaseCriteria) {
    rc.criteria_full = this.criteria.filter((c) => c.pk == rc.criteria)[0];
    let split = rc.parameters.split(",");
    rc.parameters_list = [];
    for (let i = 0; i < rc.criteria_full.nr_of_parameters; i++) {
      if (split[i]) {
        rc.parameters_list.push({value: split[i]});
      }
      else {
        rc.parameters_list.push({value: ""});
      }
    }
  }

  fixCriteriaName(name: string) {
    let name_fixed_spaces = name.split("_").join(" ");
    return name_fixed_spaces.charAt(0).toUpperCase() + name_fixed_spaces.substring(1);
  }

  materialHasNoPropertiesSet(cpm: ControlPlanMaterial) {
    if (this.controlPlan.measures) {
      let materialProps = this.propertySets.filter((ps) => ps.material.pk == cpm.material.pk).map((ps) => ps.property.pk);
      let measuresProps = this.controlPlan.measures.map((cpm_) => cpm_.property.pk);
      return arrayIntersect(materialProps, measuresProps).length == 0;
    }
  }
}
