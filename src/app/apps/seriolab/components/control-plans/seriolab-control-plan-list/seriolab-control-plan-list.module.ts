import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SeriolabControlPlansListComponent} from "./seriolab-control-plan-list.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SeriolabControlPlansListComponent],   // components and directives
  exports:[SeriolabControlPlansListComponent],
  providers: []                    // services
})
export class SeriolabControlPlansListModule { }


