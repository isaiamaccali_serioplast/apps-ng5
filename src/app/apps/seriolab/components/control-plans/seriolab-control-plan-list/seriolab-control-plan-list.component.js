"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var site_1 = require("../../../../_common/models/site");
var material_1 = require("../../../../_common/models/material");
var table_with_pages_component_1 = require("../../../../../shared/material/table-with-pages/table-with-pages.component");
var utils_1 = require("../../../../../shared/utils");
var frequency_pipe_1 = require("../../../../../shared/pipes/frequency.pipe");
var SeriolabControlPlansListComponent = (function () {
    function SeriolabControlPlansListComponent(sitesService, loginService, controlPlanService, translationService, sessionsService, router, messageService, userSitesService) {
        this.sitesService = sitesService;
        this.loginService = loginService;
        this.controlPlanService = controlPlanService;
        this.translationService = translationService;
        this.sessionsService = sessionsService;
        this.router = router;
        this.messageService = messageService;
        this.userSitesService = userSitesService;
        this.site = new site_1.Site();
        this.sites = [];
        this.userSites = [];
        this.material = new material_1.Material();
        this.noItemsMessageControlPlans = this.tr("NO_CONTROL_PLANS_AVAILABLE");
        this.headerConfControlPlans = [];
        this.availableActionsControlPlans = [];
    }
    SeriolabControlPlansListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this.headerConfControlPlans = [
            {
                label: this.tr("DESCRIPTION"),
                valueKey: 'description'
            },
            {
                label: this.tr("MATERIAL"),
                valueKey: 'material',
                useStr: true
            },
            {
                label: this.tr("FREQUENCY"),
                valueFunction: function (item) {
                    var fPipe = new frequency_pipe_1.FrequencyPipe(self.translationService);
                    return fPipe.transform(parseInt(item.frequency, 10));
                }
            },
            {
                label: this.tr("SITE"),
                valueKey: 'site',
                useStr: true
            }
        ];
        this.sitesService.getSitesList({ is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.sites = data['results'];
            }
        });
        this.userSitesService.getUserSites(this.loginService.user.pk).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.userSites = data['results'].map(function (s) { return s.pk; });
            }
        });
        var actionView = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.VIEW);
        actionView.click = this.viewControlPlan.bind(this);
        var actionEdit = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.EDIT);
        actionEdit.click = this.editControlPlan.bind(this);
        actionEdit.visibilityCondition = function (item) {
            return self.userSites.indexOf(item.site.pk) != -1;
        };
        var actionDelete = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.DELETE);
        actionDelete.click = this.deleteControlPlan.bind(this);
        actionDelete.visibilityCondition = function (item) {
            return item.is_active && self.userSites.indexOf(item.site.pk) != -1;
        };
        var actionRestore = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.RESTORE);
        actionRestore.click = this.restoreControlPlan.bind(this);
        actionRestore.visibilityCondition = function (item) {
            return !item.is_active && self.userSites.indexOf(item.site.pk) != -1;
        };
        var actionClone = new table_with_pages_component_1.TableRowAction();
        actionClone.tooltip = "Clone";
        actionClone.icon = "content_copy";
        actionClone.click = this.cloneControlPlan.bind(this);
        actionClone.visibilityCondition = function (item) {
            return item.is_active && self.userSites.indexOf(item.site.pk) != -1;
        };
        this.availableActionsControlPlans = [actionView, actionEdit, actionClone, actionDelete, actionRestore];
    };
    SeriolabControlPlansListComponent.prototype.loadControlPlans = function () {
        var _this = this;
        var query = {
            site: this.siteId,
            material: this.material.pk,
            is_active: true,
            description_icontains: this.nameFilter
        };
        this.controlPlanService.getControlPlans(query).subscribe(function (data) {
            _this.controlPlansTable.handleResp(data);
        });
    };
    SeriolabControlPlansListComponent.prototype.tr = function (value) {
        return this.translationService.translate(value, null, null, null);
    };
    SeriolabControlPlansListComponent.prototype.create = function () {
        this.router.navigate(["seriolab", "control-plans", "create"]);
    };
    SeriolabControlPlansListComponent.prototype.goToSessions = function () {
        this.router.navigate(["seriolab", "sessions"]);
    };
    SeriolabControlPlansListComponent.prototype.editControlPlan = function (cp) {
        var _this = this;
        var query = {
            controlPlanId: cp.pk,
            is_active: true
        };
        this.sessionsService.getSessions(query).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                var self_1 = _this;
                _this.messageService.open({
                    title: _this.tr('ATTENTION'),
                    message: "Modificare questo piano di controllo avrà effetto su " + data['count'] + " sessioni attive, proseguire?",
                    confirm: function () {
                        self_1.router.navigate(["seriolab", "control-plans", "edit", cp.pk]);
                    },
                    deny: function () {
                    }
                });
            }
            else {
                _this.router.navigate(["seriolab", "control-plans", "edit", cp.pk]);
            }
        });
    };
    SeriolabControlPlansListComponent.prototype.viewControlPlan = function (cp) {
        this.router.navigate(["seriolab", "control-plans", "view", cp.pk]);
    };
    SeriolabControlPlansListComponent.prototype.restoreControlPlan = function (cp) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.controlPlanService.restoreControlPlan(cp).subscribe(function (resp) {
                    self.handleSaveOrUpdate(resp, function () {
                        self.controlPlansTable.reload();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    SeriolabControlPlansListComponent.prototype.deleteControlPlan = function (cp) {
        var _this = this;
        var query = {
            control_plan: cp.pk
        };
        this.sessionsService.getSessions(query).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.messageService.open({
                    title: _this.tr('ATTENTION'),
                    message: "Questo piano di controllo è in uso da " + data['count'] + " sessioni, impossibile proseguire" //TODO Translate
                });
            }
            else {
                var self_2 = _this;
                _this.messageService.open({
                    title: _this.tr('ATTENTION'),
                    message: _this.tr('CONFIRM_DELETE'),
                    confirm: function () {
                        self_2.controlPlanService.deleteControlPlan(cp).subscribe(function () {
                            self_2.loadControlPlans();
                        });
                    },
                    deny: function () {
                    }
                });
            }
        });
    };
    SeriolabControlPlansListComponent.prototype.handleSaveOrUpdate = function (resp, callback) {
        var self = this;
        if (resp.pk) {
            self.messageService.open({
                message: self.tr('SAVE_COMPLETED'),
                title: " ",
                autoClose: false,
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    SeriolabControlPlansListComponent.prototype.handleDelete = function (resp, callback) {
        var self = this;
        if (resp.status == 204) {
            self.messageService.open({
                title: " ",
                message: self.tr('DELETE_COMPLETED'),
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    SeriolabControlPlansListComponent.prototype.selectedMaterial = function (material) {
        this.material = material;
    };
    SeriolabControlPlansListComponent.prototype.cloneControlPlan = function (cp) {
        var extras = {
            queryParams: {
                model: cp.pk
            }
        };
        this.router.navigate(["seriolab", "control-plans", "create"], extras);
    };
    return SeriolabControlPlansListComponent;
}());
__decorate([
    core_1.ViewChild('controlPlansTable')
], SeriolabControlPlansListComponent.prototype, "controlPlansTable", void 0);
SeriolabControlPlansListComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-control-plan-list',
        templateUrl: 'seriolab-control-plan-list.component.html',
        styleUrls: ['seriolab-control-plan-list.component.css']
    })
], SeriolabControlPlansListComponent);
exports.SeriolabControlPlansListComponent = SeriolabControlPlansListComponent;
