import {Component, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {Site} from "../../../../_common/services/sites.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, fixValue, parseSerionetRespToData} from "../../../../../shared/utils";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {ControlPlan, ControlPlansService} from "../../../../_common/services/control-plans.service";
import {ContentType, ContentTypeService} from "../../../../_common/services/content-type.service";
import {TypeLink, TypeLinkService} from "../../../../_common/services/type-link.service";
import {MeasureSessionService} from "../../../../_common/services/measure-session.service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {Material} from "../../../../_common/services/materials.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";
import {UserSitesComponent} from "../../../../../shared/components/user-sites/user-sites.component";

@Component({
  selector: 'app-seriolab-control-plan-list',
  templateUrl: 'seriolab-control-plan-list.component.html',
  styleUrls: ['seriolab-control-plan-list.component.css']
})
export class SeriolabControlPlansListComponent extends BaseComponent implements OnInit {

  siteId: number;
  types: TypeLink[] = [];
  material = new Material();
  nameFilter: string;
  typeFilter: TypeLink = new TypeLink();

  statusLinkSessionWorking: StatusLink = new StatusLink();

  @ViewChild('controlPlansTable') controlPlansTable: TableWithPagesComponent;
  @ViewChild('userSites') userSites: UserSitesComponent;
  noItemsMessageControlPlans = this.tr("NO_CONTROL_PLANS_AVAILABLE");
  headerConfControlPlans = [];
  availableActionsControlPlans = [];

  cpListReady = false;

  constructor(private controlPlanService: ControlPlansService, protected translationService: TranslationService,
              private sessionsService: MeasureSessionService, private router: Router, protected messageService: MessageService,
              private contentTypeService: ContentTypeService, protected loadingService: LoadingService,
              private typeLinkService: TypeLinkService, private statusLinkService: StatusLinkService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.headerConfControlPlans = [
      {
        label: this.tr("DESCRIPTION"),
        valueKey: 'description'
      },
      {
        label: this.tr("MATERIALS"),
        valueFunction: (item: ControlPlan) => {
          return '<div class="materials">' + item.controlplanmaterial_set.filter((cpm) => cpm.is_active).map((cpm) => cpm.material.__str__).join("<br/>") + '</div>';
        }
      },
      {
        label: this.tr("FREQUENCY"),
        valueFunction: (item: ControlPlan) => {
          return this.tr(item.frequency.__str__);
        }
      },
      {
        label: this.tr("SITE"),
        valueKey: 'site',
        useStr: true
      },
      {
        label: this.tr("TYPE"),
        valueKey: 'type',
        useStr: true
      }
    ];

    let actionView: TableRowAction = cloneObject(TableRowActions.VIEW);
    actionView.click = this.viewControlPlan.bind(this);

    let actionEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEdit.click = this.editControlPlan.bind(this);
    actionEdit.visibilityCondition = (item: ControlPlan) => {
      return this.userSites.sites.map((s)=>s.pk).indexOf(item.site.pk) != -1;
    };
    let actionDelete: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDelete.click = this.deleteControlPlan.bind(this);
    actionDelete.visibilityCondition = (item: ControlPlan) => {
      return item.is_active && this.userSites.sites.map((s)=>s.pk).indexOf(item.site.pk) != -1;
    };
    let actionRestore: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestore.click = this.restoreControlPlan.bind(this);
    actionRestore.visibilityCondition = (item: ControlPlan) => {
      return !item.is_active && this.userSites.sites.map((s)=>s.pk).indexOf(item.site.pk) != -1;
    };
    let actionClone: TableRowAction = new TableRowAction();
    actionClone.tooltip = "Clone";
    actionClone.icon = "content_copy";
    actionClone.click = this.cloneControlPlan.bind(this);
    actionClone.visibilityCondition = (item: ControlPlan) => {
      return item.is_active && this.userSites.sites.map((s)=>s.pk).indexOf(item.site.pk) != -1;
    };
    this.availableActionsControlPlans = [actionView, actionEdit, actionClone, actionDelete, actionRestore];

    this.contentTypeService.getContentTypes({model: 'controlplan', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let controlPlanCT = data['results'][0];
        this.typeLinkService.getTypeLinks({is_active: true, app: controlPlanCT.pk}).subscribe((resp) => {
          this.types = parseSerionetRespToData(resp);
        })
      }
    });

    this.contentTypeService.getContentTypes({model: 'measuresession', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, is_active: true}).subscribe((data) => {
          let sls = parseSerionetRespToData(data);
          sls.forEach((sl: StatusLink) => {
            if (sl.delta_rel == 1)
              this.statusLinkSessionWorking = sl;
          });
        });
      }
    });
  }

  loadControlPlans(page: number, results: number) {
    let query = {
      site: this.siteId,
      material: this.material ? this.material.pk : null,
      is_active: true,
      description_icontains: this.nameFilter,
      type__delta_rel: this.typeFilter.delta_rel,
      page: page,
      results: results,
      ordering: 'description'
    };
    return this.controlPlanService.getControlPlans(query);
  }

  create() {
    this.router.navigate(["seriolab", "control-plans", "create"]);
  }

  goToSessions() {
    this.router.navigate(["seriolab", "sessions"]);
  }

  editControlPlan(cp: ControlPlan) {

    let query = {
      control_plan: cp.pk,
      is_active: true,
      status_delta_rel_list: "0|1"
    };

    this.sessionsService.getSessions(query).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.messageService.open({
          title: this.tr('ATTENTION'),
          message: this.tr('CONTROL_PLAN_IN_USE', [{count: data['count']}]),
          confirm: () => {
            this.router.navigate(["seriolab", "control-plans", "edit", cp.pk]);
          },
          deny: () => {
          }
        });
      }
      else {
        this.router.navigate(["seriolab", "control-plans", "edit", cp.pk]);
      }
    })
  }

  viewControlPlan(cp: ControlPlan) {

    this.router.navigate(["seriolab", "control-plans", "view", cp.pk]);
  }

  restoreControlPlan(cp: ControlPlan) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.controlPlanService.restoreControlPlan(cp).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.controlPlansTable.reload();
          })
        })
      },
      deny: () => {
      }
    });
  }

  deleteControlPlan(cp: ControlPlan) {
    let query = {
      control_plan: cp.pk,
      is_active: true,
      status_delta_rel_list: "0|1"
    };

    this.sessionsService.getSessions(query).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.messageService.open({
          title: this.tr('ATTENTION'),
          message: "Questo piano di controllo è in uso da " + data['count'] + " sessioni, impossibile proseguire" //TODO Translate
        });
      }
      else {
        this.messageService.open({
          title: this.tr('ATTENTION'),
          message: this.tr('CONFIRM_DELETE'),
          confirm: () => {
            this.controlPlanService.deleteControlPlan(cp).subscribe(() => {
              this.controlPlansTable.reload();
            })
          },
          deny: () => {
          }
        });
      }
    })
  }

  selectedMaterial(material: Material) {
    this.material = material;
  }

  cloneControlPlan(cp: ControlPlan) {
    let extras = {
      queryParams: {
        model: cp.pk
      }
    };
    this.router.navigate(["seriolab", "control-plans", "create"], extras);
  }

  fixType() {
    fixValue(this.types, this, 'typeFilter');
  }

  selectedSite(s:Site){
    if(s && s.pk) {
      this.siteId = s.pk;
      this.cpListReady = true;
    }
    else {
      this.siteId = null;
    }
  }
}
