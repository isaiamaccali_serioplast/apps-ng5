import {NgModule} from "@angular/core";
import {SeriolabControlPlanIndexModule} from "./seriolab-control-plan-index/seriolab-control-plan-index.module";
import {SeriolabControlPlansListModule} from "./seriolab-control-plan-list/seriolab-control-plan-list.module";
import {SeriolabControlPlanEditModule} from "./seriolab-control-plan-edit/seriolab-control-plan-edit.module";
import {SeriolabControlPlanViewModule} from "./seriolab-control-plan-view/seriolab-control-plan-view.module";

@NgModule({
  imports: [SeriolabControlPlanIndexModule,SeriolabControlPlansListModule,SeriolabControlPlanEditModule,SeriolabControlPlanViewModule],       // module dependencies
  exports:[SeriolabControlPlanIndexModule,SeriolabControlPlansListModule,SeriolabControlPlanEditModule,SeriolabControlPlanViewModule],
})
export class SeriolabControlPlansModule { }


