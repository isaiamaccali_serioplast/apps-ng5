"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var seriolab_control_plan_index_module_1 = require("./seriolab-control-plan-index/seriolab-control-plan-index.module");
var seriolab_control_plan_list_module_1 = require("./seriolab-control-plan-list/seriolab-control-plan-list.module");
var seriolab_control_plan_edit_module_1 = require("./seriolab-control-plan-edit/seriolab-control-plan-edit.module");
var seriolab_control_plan_view_module_1 = require("./seriolab-control-plan-view/seriolab-control-plan-view.module");
var SeriolabControlPlansModule = (function () {
    function SeriolabControlPlansModule() {
    }
    return SeriolabControlPlansModule;
}());
SeriolabControlPlansModule = __decorate([
    core_1.NgModule({
        imports: [seriolab_control_plan_index_module_1.SeriolabControlPlanIndexModule, seriolab_control_plan_list_module_1.SeriolabControlPlansListModule, seriolab_control_plan_edit_module_1.SeriolabControlPlanEditModule, seriolab_control_plan_view_module_1.SeriolabControlPlanViewModule],
        exports: [seriolab_control_plan_index_module_1.SeriolabControlPlanIndexModule, seriolab_control_plan_list_module_1.SeriolabControlPlansListModule, seriolab_control_plan_edit_module_1.SeriolabControlPlanEditModule, seriolab_control_plan_view_module_1.SeriolabControlPlanViewModule],
    })
], SeriolabControlPlansModule);
exports.SeriolabControlPlansModule = SeriolabControlPlansModule;
