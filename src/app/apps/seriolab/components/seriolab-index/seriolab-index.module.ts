import {NgModule} from "@angular/core";
import {MesIndexComponent} from "./mes-index.component";
import {SeriolabIndexComponent} from "./seriolab-index.component";
import {SharedModule} from "../../../../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SeriolabIndexComponent],   // components and directives
  exports:[SeriolabIndexComponent],
  providers: []                    // services
})
export class SeriolabIndexModule { }


