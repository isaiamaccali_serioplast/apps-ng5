"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var SeriolabIndexComponent = (function () {
    function SeriolabIndexComponent(sideNavService, loginService, router, titleService, dictService) {
        this.sideNavService = sideNavService;
        this.loginService = loginService;
        this.router = router;
        this.titleService = titleService;
        this.dictService = dictService;
        this.sideNavService.leftMenuEnabled = false;
        this.sideNavService.leftMenu = false;
        this.sideNavService.appMenuEnabled = true;
        this.titleService.setTitle("Serioplast - Seriolab");
        this.dictService.getDictionary("seriolab").subscribe(function (dict) { });
    }
    SeriolabIndexComponent.prototype.ngOnInit = function () {
        this.router.navigate(["seriolab", "sessions"]);
    };
    return SeriolabIndexComponent;
}());
SeriolabIndexComponent = __decorate([
    core_1.Component({
        selector: 'app-seriolab-index',
        templateUrl: 'seriolab-index.component.html',
        styleUrls: ['seriolab-index.component.css']
    })
], SeriolabIndexComponent);
exports.SeriolabIndexComponent = SeriolabIndexComponent;
