import {Component, OnInit} from "@angular/core";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {ConfigurationService} from "../../../../shared/serionet/service/configuration.service";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-seriolab-index',
  templateUrl: 'seriolab-index.component.html',
  styleUrls: ['seriolab-index.component.css']
})
export class SeriolabIndexComponent implements OnInit {

  constructor(private sideNavService: SideNavService,
              private router: Router,
              private titleService: Title,
              private appPlantUsersService:AppPlantUsersService,
              private confService:ConfigurationService,
              private snackBar:MatSnackBar
  ) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Seriolab");
  }

  ngOnInit() {
    let query = {
      app__app_root: 'seriolab',
      is_active:true
    };

    this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
      if (response['results'] && response['results'].length == 1) {
        this.confService.set("SAP_USER",response['results'][0].user, true);
        this.confService.set("SAP_PASSWORD",response['results'][0].password, true);
        this.router.navigate(["seriolab", "sessions"]);
      }else{
        this.router.navigate(["/index"]);
        this.snackBar.open("NO_SAP_LICENCE_FOR_APP_MATERIALS", null, {duration: 5000, panelClass: ['error']});
      }
    });

  }
}
