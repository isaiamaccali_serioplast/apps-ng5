"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var seriolab_routes_1 = require("./seriolab.routes");
var shared_module_1 = require("../../shared/shared.module");
var seriolab_index_module_1 = require("./components/seriolab-index/seriolab-index.module");
var seriolab_sessions_module_1 = require("./components/sessions/seriolab-sessions.module");
var seriolab_control_plans_module_1 = require("./components/control-plans/seriolab-control-plans.module");
var seriolab_report_module_1 = require("./components/reports/seriolab-report.module");
var common_module_1 = require("../_common/common.module");
var SeriolabModule = (function () {
    function SeriolabModule() {
    }
    return SeriolabModule;
}());
SeriolabModule = __decorate([
    core_1.NgModule({
        imports: [shared_module_1.SharedModule, common_module_1.SerioplastCommonModule, router_1.RouterModule.forChild(seriolab_routes_1.seriolabRoutes), seriolab_index_module_1.SeriolabIndexModule, seriolab_sessions_module_1.SeriolabSessionsModule, seriolab_control_plans_module_1.SeriolabControlPlansModule, seriolab_report_module_1.SeriolabReportModule],
        declarations: [],
        providers: [] // services
    })
], SeriolabModule);
exports.SeriolabModule = SeriolabModule;
