import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {SeriolabIndexComponent} from "./components/seriolab-index/seriolab-index.component";
import {SeriolabSessionsIndexComponent} from "./components/sessions/seriolab-sessions-index/seriolab-sessions-index.component";
import {SeriolabSessionsListComponent} from "./components/sessions/seriolab-sessions-list/seriolab-sessions-list.component";
import {SeriolabSessionCreateComponent} from "./components/sessions/seriolab-session-create/seriolab-session-create.component";
import {SeriolabControlPlanIndexComponent} from "./components/control-plans/seriolab-control-plan-index/seriolab-control-plan-index.component";
import {SeriolabControlPlansListComponent} from "./components/control-plans/seriolab-control-plan-list/seriolab-control-plan-list.component";
import {SeriolabSessionDetailComponent} from "./components/sessions/seriolab-session-detail/seriolab-session-detail.component";
import {SeriolabReportIndexComponent} from "./components/reports/seriolab-report-index/seriolab-report-index.component";
import {SeriolabControlPlanEditComponent} from "./components/control-plans/seriolab-control-plan-edit/seriolab-control-plan-edit.component";
import {SeriolabControlPlanViewComponent} from "./components/control-plans/seriolab-control-plan-view/seriolab-control-plan-view.component";

/**
 * Root route, uses lazy loading to improve performance
 * @type {{path: string; loadChildren: string; canActivate: AuthGuard[]}[]}
 */
export const seriolabRoutesRoot: Routes = [
  {
    path: 'seriolab',
    loadChildren: './seriolab.module.ts#SeriolabModule',
    //canActivate: [AuthGuard]
  }
];

// Route Configuration
export const seriolabRoutes: Routes = [
  {
    path: '',
    component: SeriolabIndexComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch:'full',
        component: SeriolabIndexComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'sessions',
        component: SeriolabSessionsIndexComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            pathMatch:'full',
            component: SeriolabSessionsListComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'create',
            component: SeriolabSessionCreateComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'detail/:session',
            component: SeriolabSessionDetailComponent,
            canActivate: [AuthGuard],
          }
        ]
      },
      {
        path: 'control-plans',
        component: SeriolabControlPlanIndexComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            pathMatch:'full',
            component: SeriolabControlPlansListComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'create',
            component: SeriolabControlPlanEditComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'edit/:cp',
            component: SeriolabControlPlanEditComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'view/:cp',
            component: SeriolabControlPlanViewComponent,
            canActivate: [AuthGuard],
          }
        ]
      },
      {
        path: 'report',
        component: SeriolabReportIndexComponent,
        canActivate: [AuthGuard],
        children: [
          // {
          //   path: '',
          //   pathMatch:'full',
          //   component: SeriolabControlPlansListComponent,
          //   canActivate: [AuthGuard],
          // },
          // {
          //   path: 'create',
          //   component: SeriolabControlPlanCreateComponent,
          //   canActivate: [AuthGuard],
          // },
          // {
          //   path: 'detail/:cp',
          //   component: SeriolabControlPlanDetailComponent,
          //   canActivate: [AuthGuard],
          // }
        ]
      }
    ]
  }

];
