import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {seriolabRoutes} from "./seriolab.routes";
import {SharedModule} from "../../shared/shared.module";
import {SeriolabIndexModule} from "./components/seriolab-index/seriolab-index.module";
import {SeriolabSessionsModule} from "./components/sessions/seriolab-sessions.module";
import {SeriolabControlPlansModule} from "./components/control-plans/seriolab-control-plans.module";
import {SeriolabReportModule} from "./components/reports/seriolab-report.module";
import {SerioplastCommonModule} from "../_common/common.module";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule, RouterModule.forChild(seriolabRoutes),SeriolabIndexModule, SeriolabSessionsModule, SeriolabControlPlansModule, SeriolabReportModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class SeriolabModule { }

