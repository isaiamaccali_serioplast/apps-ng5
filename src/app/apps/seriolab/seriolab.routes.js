"use strict";
var auth_guard_1 = require("../../auth-guard");
var seriolab_index_component_1 = require("./components/seriolab-index/seriolab-index.component");
var seriolab_sessions_index_component_1 = require("./components/sessions/seriolab-sessions-index/seriolab-sessions-index.component");
var seriolab_sessions_list_component_1 = require("./components/sessions/seriolab-sessions-list/seriolab-sessions-list.component");
var seriolab_session_create_component_1 = require("./components/sessions/seriolab-session-create/seriolab-session-create.component");
var seriolab_control_plan_index_component_1 = require("./components/control-plans/seriolab-control-plan-index/seriolab-control-plan-index.component");
var seriolab_control_plan_list_component_1 = require("./components/control-plans/seriolab-control-plan-list/seriolab-control-plan-list.component");
var seriolab_session_detail_component_1 = require("./components/sessions/seriolab-session-detail/seriolab-session-detail.component");
var seriolab_report_index_component_1 = require("./components/reports/seriolab-report-index/seriolab-report-index.component");
var seriolab_control_plan_edit_component_1 = require("./components/control-plans/seriolab-control-plan-edit/seriolab-control-plan-edit.component");
var seriolab_control_plan_view_component_1 = require("./components/control-plans/seriolab-control-plan-view/seriolab-control-plan-view.component");
/**
 * Root route, uses lazy loading to improve performance
 * @type {{path: string; loadChildren: string; canActivate: AuthGuard[]}[]}
 */
exports.seriolabRoutesRoot = [
    {
        path: 'seriolab',
        loadChildren: './seriolab.module.ts#SeriolabModule',
    }
];
// Route Configuration
exports.seriolabRoutes = [
    {
        path: '',
        component: seriolab_index_component_1.SeriolabIndexComponent,
        canActivate: [auth_guard_1.AuthGuard],
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: seriolab_index_component_1.SeriolabIndexComponent,
                canActivate: [auth_guard_1.AuthGuard],
            },
            {
                path: 'sessions',
                component: seriolab_sessions_index_component_1.SeriolabSessionsIndexComponent,
                canActivate: [auth_guard_1.AuthGuard],
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        component: seriolab_sessions_list_component_1.SeriolabSessionsListComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    },
                    {
                        path: 'create',
                        component: seriolab_session_create_component_1.SeriolabSessionCreateComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    },
                    {
                        path: 'detail/:session',
                        component: seriolab_session_detail_component_1.SeriolabSessionDetailComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    }
                ]
            },
            {
                path: 'control-plans',
                component: seriolab_control_plan_index_component_1.SeriolabControlPlanIndexComponent,
                canActivate: [auth_guard_1.AuthGuard],
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        component: seriolab_control_plan_list_component_1.SeriolabControlPlansListComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    },
                    {
                        path: 'create',
                        component: seriolab_control_plan_edit_component_1.SeriolabControlPlanEditComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    },
                    {
                        path: 'edit/:cp',
                        component: seriolab_control_plan_edit_component_1.SeriolabControlPlanEditComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    },
                    {
                        path: 'view/:cp',
                        component: seriolab_control_plan_view_component_1.SeriolabControlPlanViewComponent,
                        canActivate: [auth_guard_1.AuthGuard],
                    }
                ]
            },
            {
                path: 'report',
                component: seriolab_report_index_component_1.SeriolabReportIndexComponent,
                canActivate: [auth_guard_1.AuthGuard],
                children: []
            }
        ]
    }
];
