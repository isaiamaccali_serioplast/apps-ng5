import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {QualityStockIndexComponent} from "./components/quality-stock-index/quality-stock-index.component";
import {QualityStockBatchListComponent} from "./components/quality-stock-batch-list/quality-stock-batch-list.component";
import {QualityStockBatchDetailComponent} from "./components/quality-stock-batch-detail/quality-stock-batch-detail.component";
import {QualityStockBatchDetailWhereUsedComponent} from "./components/quality-stock-batch-detail-where-used/quality-stock-batch-detail-where-used.component";

export const QualityStockRoutesRoot: Routes = [
  {
    path: 'quality-stock',
    loadChildren: './quality-stock.module.ts#QualityStockModule',
    canActivate: [AuthGuard]
  }
];

// Route Configuration
export const QualityStockRoutes: Routes = [
  {
    path: '',
    component: QualityStockIndexComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path:"",
        pathMatch:'full',
        redirectTo:'batch/list'
      },
      {
        path:'batch/list',
        component:QualityStockBatchListComponent
      },
      {
        path:'batch/detail',
        component:QualityStockBatchDetailComponent
      },
      {
        path:'batch/traceability',
        component:QualityStockBatchDetailWhereUsedComponent
      }
    ]
  }
];
