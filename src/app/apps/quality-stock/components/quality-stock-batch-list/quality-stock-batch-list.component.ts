import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {Router} from "@angular/router";
import {TableRowAction, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {QualityBatch, QualityBatchService} from "../../services/quality-batch.service";
import {getUrlDate} from "../../../../shared/utils";
import {SapTimePipe} from "../../../../shared/pipes/sap-time.pipe";
import {QualityStockBatchListQueryService} from "./quality-stock-batch-list-query-service";
import {Material} from "../../../_common/services/materials.service";
import {Site} from "../../../_common/services/sites.service";
import {_HUQualityStatus} from "../../../_common/services/handling-units-quality-sap.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

declare let sap: any;

@Component({
  selector: 'app-quality-stock-batch-list',
  templateUrl: 'quality-stock-batch-list.component.html',
  styleUrls: ['quality-stock-batch-list.component.css']
})
export class QualityStockBatchListComponent extends TranslatableComponent implements OnInit {

  ready = false;
  @ViewChild('batchTable') batchTable:TableWithPagesComponent;
  HUQualityStatus = _HUQualityStatus;

  availableActions = [];
  headerConf = [
    {label: this.tr("BATCH"), valueKey: 'Batch'},
    {label: this.tr("MATERIAL"), valueKey: 'Material'},
    {label: this.tr("DESCRIPTION"), valueKey: 'MaterialDescription'},
    {label: this.tr("CREATION_DATE"), valueFunction: (item) =>{let d = item.CreationDate; return [d.substring(0,4),d.substring(4,6),d.substring(6,8)].join("-")}},
    {label: this.tr("CREATION_TIME"), valueFunction: (item) =>{
      let p = new SapTimePipe();
      return p.transform(item['CreationTime']);
    }},
    {label: this.tr("QUANTITY"), valueFunction: (item) => {return parseFloat(item.Quantity) + " "+ item['Uom'];}},
    {label: this.tr("SUPPLIER_BATCH"), valueKey: 'SupplierBatch'},
    {label: this.tr("SUPPLIER"), valueKey: 'Supplier'},
    {label: this.tr("TEXT"), action: (item,event)=>{this.showTextLink(item)}, icon:'assignment', actionCondition:(item:QualityBatch) =>item.TextLink['results'].length>0}
  ];

  site:Site;
  material:Material;
  fromDate:any;
  toDate:any;
  batchQuery:string;
  supplierBatchCode:string;
  statusFilter:any;
  workorderFilter:string;
  deliveryFilter:string;

  additionalQueryMaterial = {};

  stockOnly = true;

  constructor(protected translationService:TranslationService, private router:Router,
              private qualityBatchService:QualityBatchService, private queryService:QualityStockBatchListQueryService,
              private messageService:MessageService
  ) {
    super(translationService);
  }

  showTextLink(b:QualityBatch){
    this.messageService.open({
      message: b.TextLink['results'].map((tl)=>tl.Line).join("<br/>"),
      confirm:()=>{}
    })
  }

  ngOnInit() {
    let actionView:TableRowAction = new TableRowAction();
    actionView.icon = "visibility_on";
    actionView.tooltip = "View Details";
    actionView.click = this.batchClicked.bind(this);

    let actionWhereUsed:TableRowAction = new TableRowAction();
    actionWhereUsed.icon = "device_hub";
    actionWhereUsed.tooltip = "Traceability";
    actionWhereUsed.click = this.batchWhereUsed.bind(this);

    this.availableActions = [actionView,actionWhereUsed];
  }

  restoreQuery(query:any) {
    if(query['site']){
      this.site = query['site'];
    }
    if(query['material']){
      this.material = query['material'];
    }
    if(query['from']){
      this.fromDate = query['from'];
    }
    if(query['to']){
      this.toDate = query['to'];
    }
    if(query['Batch']){
      this.batchQuery = query['Batch'];
    }
    if(query['SupplierBatch']){
     this.supplierBatchCode = query['SupplierBatch'];
    }
    if(query['Status']){
      this.statusFilter = query['Status'];
    }
    if(query['WorkOrder']){
      this.workorderFilter = query['WorkOrder'];
    }
    if(query['Delivery']){
      this.deliveryFilter = query['Delivery'];
    }
    if(query['page'])
      this.batchTable.setPage(query['page']);

    if(query['stockOnly'])
      this.stockOnly = true;
    else
      this.stockOnly = false;

    this.ready = true;
  }

  batchClicked(b:QualityBatch){
    this.router.navigate(["quality-stock","batch","detail"],{queryParams:{Batch:b.Batch,Plant:b.Plant,Material:b.Material}});
  }

  batchWhereUsed(b:QualityBatch){
    this.router.navigate(["quality-stock","batch","traceability"],{queryParams:{Batch:b.Batch,Plant:b.Plant,Material:b.Material}});
  }


  reload(){
    setTimeout(()=>{
      this.batchTable.setPage(1);
      this.batchTable.reload();
    },500);
  }

  getBatches(page:number,results:number){
    if(this.checkQuery(page,results)){
      return this.qualityBatchService.getQualityBatches(this.buildQuery(page,results));
    }
  }

  selectedSite(s:Site){
    this.site = s;
    this.additionalQueryMaterial = {site_set:(s?s.pk:null)};
    if(s.pk){
      if(!this.ready){
        let query = this.queryService.getQuery();
        if(query && Object.keys(query).length>0){
          this.restoreQuery(query);
        }
        else {
          let date = new Date();
          this.toDate = getUrlDate(date);
          date.setDate(date.getDate()-7);
          this.fromDate = getUrlDate(date);
          this.ready = true;
        }
      }
    }
  }

  selectedMaterial(m:Material){
    this.material = m;
  }

  buildQuery(page?:number,results?:number){
    let query = {};
    if(this.site){
      query['Plant'] = this.site.code;
      query['site'] = this.site;
    }
    if(this.material){
      query['Material'] = this.material.code;
      query['material'] = this.material;
    }
    if(this.fromDate){
      let d = new Date(this.fromDate);
      query['fromDate'] = getUrlDate(d,"");
      query['from'] = getUrlDate(d,"-");
    }

    if(this.toDate){
      let d = new Date(this.toDate);
      query['toDate'] = getUrlDate(d,"");
      query['to'] = getUrlDate(d,"-");
    }
    if(this.batchQuery){
      query['Batch'] = this.batchQuery;
    }
    if(this.supplierBatchCode){
      query['SupplierBatch'] = this.supplierBatchCode;
    }

    if(this.statusFilter){
      query['Status'] = this.statusFilter;
    }
    if(this.deliveryFilter){
      query['Delivery'] = this.deliveryFilter;
    }
    if(this.workorderFilter){
      query['WorkOrder'] = this.workorderFilter;
    }

    query['stockOnly']  = this.stockOnly;

    query['page'] = page;
    query['results'] = results;

    if(page && results)
      this.queryService.saveQuery(query);
    return query;
  }

  checkQuery(page,results){
    let query = this.buildQuery(page,results);

    let isValid = false;
    if(query['Plant']){
      if(query['fromDate'] && query['toDate']){
        isValid = true;
      }
      if(query['Material']){
        isValid = true;
      }
      if(query['Batch']){
        isValid = true;
      }
    }
    return isValid;
  }
}

