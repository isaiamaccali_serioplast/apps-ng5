import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {QualityStockBatchListComponent} from "./quality-stock-batch-list.component";
import {QualityStockBatchListQueryService} from "./quality-stock-batch-list-query-service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [QualityStockBatchListComponent],   // components and directives
  exports:[],
  providers: [QualityStockBatchListQueryService]                    // services
})
export class QualityStockBatchListModule { }


