import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {Router} from "@angular/router";
import {BatchTreeStructure} from "../quality-stock-batch-detail-where-used.component";
import {BatchTree} from "../../../services/quality-batch.service";
import {
  WorkorderRelatedBatch,
  WorkorderRelatedBatchSapService
} from "../../../../_common/services/workorder-related-batches-sap.service";
import {
  HeaderConf,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-quality-stock-batch-detail-where-used-leaf',
  templateUrl: 'batch-detail-where-used-leaf.component.html',
  styleUrls: ['batch-detail-where-used-leaf.component.css']
})
export class QualityStockBatchDetailWhereUsedLeafComponent extends TranslatableComponent{

  @Input() batchStructure:BatchTreeStructure;
  @ViewChild('workorderDialog') workorderDialog:DialogComponent;
  exploded = false;

  constructor(protected translationService: TranslationService, private router:Router,
              private workorderRelatedBatchSapService:WorkorderRelatedBatchSapService
  ) {
    super(translationService);
  }

  goToBatch(b:BatchTree){
    this.router.navigate(["/quality-stock","batch","traceability"],{queryParams:{Batch:b.FSBatch,Plant:b.FSPlant,Material:b.FSMaterial}});
  }

  parseFloat(n){
    return parseFloat(n);
  }

  openWorkorderDetails(){
    this.workorderDialog.open();
  }

  headerConf:HeaderConf[] = [
    {
      label:'Material',
      valueFunction:(item:WorkorderRelatedBatch)=>{
        return item.Material+" - "+item.Description;
      }
    },
    {
      label:'Batch',
      valueKey:'Batch'
    },
    {
      label:'Quantity',
      valueFunction:(item:WorkorderRelatedBatch)=>{
        return parseFloat(item.Quantity)+" "+item.Uom;
      }
    }
  ];

  getBatches(page:number, results:number,table:TableWithPagesComponent){
    let query = {
      WorkOrder:this.batchStructure.batch.WorkOrder
    };
    return this.workorderRelatedBatchSapService.getBatches(query);
  }


}

