import {Component, Input, OnInit} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BatchTree, QualityBatch, QualityBatchService} from "../../services/quality-batch.service";
import {ActivatedRoute} from "@angular/router";
import {MessageService} from "../../../../shared/material/message/message.service";
import {buildErrorMessage} from "../../../../shared/utils";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

export class BatchTreeStructure {
  batch:BatchTree;
  children?:BatchTreeStructure[];
  father:boolean;
  parent:BatchTree
}

@Component({
  selector: 'app-quality-stock-batch-detail-where-used',
  templateUrl: 'quality-stock-batch-detail-where-used.component.html',
  styleUrls: ['quality-stock-batch-detail-where-used.component.css']
})
export class QualityStockBatchDetailWhereUsedComponent extends TranslatableComponent implements OnInit {

  @Input() batch:QualityBatch;
  btsFathers:BatchTreeStructure[];
  btsSons:BatchTreeStructure[];

  queryParams:any;
  constructor(protected translationService: TranslationService, private loadingService:LoadingService,
              private qualityBatchService:QualityBatchService, private activatedRoute:ActivatedRoute,
              private messageService:MessageService
  ) {
    super(translationService);
  }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe((params) => {

      if (params['Batch'] && params['Plant'] && params['Material']) {
        this.queryParams = params;
        this.loadingService.show();
        this.qualityBatchService.getQualityBatch("Batch='" + params['Batch'] + "',Plant='" + params['Plant'] + "',Material='" + params['Material'] + "'",{urlParameters:{$expand:'BatchSon,BatchFather,BatchSon/BatchOps,BatchFather/BatchOps,BatchSon/BatchOps/OpsPRT,BatchFather/BatchOps/OpsPRT'}}).subscribe((resp) => {
          this.loadingService.hide();
          if (resp.status == 200) {
            let fullBatch = resp['response'];
            let b = new QualityBatch();
            for(let key in fullBatch){
              if(fullBatch.hasOwnProperty(key)){
                b[key] = fullBatch[key];
              }
            }
            this.batch = b;
            this.loadingService.show();
            this.buildTreeStructure();
            this.loadingService.hide();
          }
          else {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      }
    })
  }

  buildTreeStructure(){
    let btsFather = new BatchTreeStructure();

    btsFather.batch = null;
    btsFather.father = true;
    btsFather.children = [];


    if(this.batch.BatchFather.results.length>0)
      this.processTree(btsFather,this.batch.BatchFather.results);

    let btsSons = new BatchTreeStructure();

    btsSons.batch = null;
    btsSons.father = false;
    btsSons.children = [];

    if(this.batch.BatchSon.results.length>0)
      this.processTree(btsSons,this.batch.BatchSon.results);

    this.btsFathers = btsFather.children;
    this.btsSons = btsSons.children;

  }

  processTree(parent:BatchTreeStructure,items:BatchTree[]){
    let item = items[0];

    let bts = new BatchTreeStructure();
    bts.batch = item;
    bts.children = [];
    bts.father = parent.father;
    parent.children.push(bts);


    let itemsToProcess = items.slice(1);
    let nextIndex = undefined;
    itemsToProcess.forEach((bt:BatchTree,index:number)=>{
      if((bt.Level==item.Level) && (typeof nextIndex=="undefined")) {
        nextIndex = index;
      }
    });

    let candidates = itemsToProcess.slice(0,nextIndex);

    if(candidates.length>0){
      this.processTree(bts,candidates);
    }

    if(typeof nextIndex!='undefined'){
      let nextList = itemsToProcess.slice(nextIndex);
      this.processTree(parent,nextList);
    }
  }
}

