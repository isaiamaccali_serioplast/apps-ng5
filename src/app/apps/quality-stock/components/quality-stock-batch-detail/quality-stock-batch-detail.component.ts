import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {ActivatedRoute} from "@angular/router";
import {QualityBatch, QualityBatchService} from "../../services/quality-batch.service";
import {buildErrorMessage, cloneObject} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {BatchWarehouse, BatchWarehouseService} from "../../services/batch-warehouse.service";
import {BatchStock, BatchStockService} from "../../services/batch-stock.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {PrinterSAP, PrinterSapService} from "../../../_common/services/printers-sap.service";
import {_HUQualityStatus} from "../../../_common/services/handling-units-quality-sap.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-quality-stock-batch-detail',
  templateUrl: 'quality-stock-batch-detail.component.html',
  styleUrls: ['quality-stock-batch-detail.component.css']
})
export class QualityStockBatchDetailComponent extends TranslatableComponent implements OnInit {

  @ViewChild('lockDialog') lockDialog: DialogComponent;
  @ViewChild('unlockDialog') unlockDialog: DialogComponent;
  @ViewChild('printDialog') printDialog: DialogComponent;
  batch: QualityBatch;

  batchWarehouses: BatchWarehouse[] = [];

  headerConf = [];
  availableActions = [];

  bsToLock: BatchStock;
  bsToUnlock: BatchStock;

  bToPrint: QualityBatch;

  printers: PrinterSAP[] = [];

  selectedIndex = 0;

  queryParams: any;

  constructor(protected translationService: TranslationService, private activatedRoute: ActivatedRoute,
              private qualityBatchService: QualityBatchService, private messageService: MessageService,
              private batchWarehousesService: BatchWarehouseService, private batchStockService: BatchStockService,
              private loadingService: LoadingService, private sapPrintersService: PrinterSapService) {
    super(translationService);
  }

  ngOnInit() {
    this.headerConf = [
      {
        label: this.tr("HU"),
        valueKey: 'SSCC',
        active: true,
        mandatory: true
      },
      {
        label: this.tr("WORK_ORDER"),
        valueKey: 'WorkOrder',
        active: true
      },
      {
        label: this.tr("STOCK_STATUS"),
        valueFunction: (item) => {
          return this.tr(item.getStatus())
        },
        active: true,
        mandatory: true
      },
      {
        label: this.tr("PALLET"),
        valueKey: 'PalletNr',
        active: true
      },
      {
        label: this.tr("QUANTITY"),
        valueFunction: (item) => {
          return parseFloat(item.Quantity);
        },
        active: true
      },
      {
        label: this.tr("STORAGE_LOCATION"),
        valueKey: 'Warehouse',
        active: true
      },
      {
        label: this.tr("PALLETIZATION"),
        valueKey: 'Palletization',
        active: true
      },
      {
        label: this.tr("DELIVERY"),
        valueKey: 'Delivery',
        active: true
      },
      {
        label: this.tr("WAREHOUSE_DESCRIPTION"),
        valueKey: 'WarehouseDescription',
        active: true
      }
    ];

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['Batch'] && params['Plant'] && params['Material']) {
        this.queryParams = params;
        this.qualityBatchService.getQualityBatch("Batch='" + params['Batch'] + "',Plant='" + params['Plant'] + "',Material='" + params['Material'] + "'").subscribe((resp) => {
          if (resp.status = 200) {
            this.batch = resp['response'];
            this.loadWarehouses();
          }
          else {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      }
    })
  }

  lockBulkMaterial(bs: BatchStock) {
    this.lockDialog.onClose.subscribe(() => {
      this.bsToLock = null;
    });
    this.bsToLock = cloneObject(bs);
    this.bsToLock.Quantity = parseFloat(this.bsToLock.Quantity + "");
    this.lockDialog.open();
  }

  doLock() {
    let bs: BatchStock = cloneObject(this.bsToLock);
    this.lockDialog.close();
    this.loadingService.show();
    this.batchStockService.lockBatchStock(bs).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status >= 200 && resp.status <= 300) {
        this.reloadWarehouse(bs.Warehouse);
      }
      else {

        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  unlockBulkMaterial(bs: BatchStock) {
    this.lockDialog.onClose.subscribe(() => {
      this.bsToUnlock = null;
    });
    this.bsToUnlock = cloneObject(bs);
    this.bsToUnlock.Quantity = parseFloat(this.bsToUnlock.Quantity + "");
    this.unlockDialog.open();
  }

  doUnlock() {
    let bs: BatchStock = cloneObject(this.bsToUnlock);
    this.unlockDialog.close();
    this.loadingService.show();
    this.batchStockService.unlockBatchStock(bs).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status >= 200 && resp.status <= 300) {
        this.reloadWarehouse(bs.Warehouse);
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  reloadWarehouse(warehouseCode: string) {
    this.batchWarehousesService.getBatchWarehouse("Batch='" + this.batch.Batch + "',Plant='" + this.batch.Plant + "',Material='" + this.batch.Material + "',Warehouse='" + warehouseCode + "'").subscribe((resp) => {
      if (resp.status == 200) {
        this.batchWarehouses.forEach((bw, index) => {
          if (bw.Warehouse == warehouseCode) {
            this.batchWarehouses[index] = resp['response'];
          }
        })
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  openPrintDialog() {
    this.printDialog.open();
  }

  doPrint() {
    this.printDialog.close();
  }

  loadWarehouses() {
    this.batchWarehousesService.getBatchWarehouses({
      Batch: this.batch.Batch,
      Plant: this.batch.Plant,
      Material: this.batch.Material
    }).subscribe((resp) => {
      if (resp.status == 200) {
        this.batchWarehouses = resp['response']['results'];
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  getBWLabel(bw: BatchWarehouse) {
    let label = bw.WarehouseDescription + " (" + bw.Warehouse + ") - ";

    let quantity = this.getBWTotalQuantity(bw);

    label += " " + quantity + " " + this.batch.Uom;

    return label;

  }

  getBWTotalQuantity(bw: BatchWarehouse) {
    let quantity = 0;

    bw.WBatchStock.results.forEach((bs) => {
      quantity += parseFloat(bs.Quantity + "");
    });
    return quantity;
  }

  getStatus(stockType: string) {
    switch (stockType) {

      case "R":
        return _HUQualityStatus.RETURN_STOCK;
      case "X":
      case "S":
        return _HUQualityStatus.LOCKED;
      case "Q":
        return _HUQualityStatus.QUALITY;
      case "":
      case null:
      default:
        return _HUQualityStatus.FREE;
    }
  }

  canLock(stockType: string) {
    let status = this.getStatus(stockType);
    switch (status) {
      case _HUQualityStatus.FREE:
      case _HUQualityStatus.RETURN_STOCK:
      case _HUQualityStatus.QUALITY:
        return true;
      case _HUQualityStatus.LOCKED:
      default:
        return false;
    }
  }

  canUnlock(stockType: string) {
    let status = this.getStatus(stockType);
    switch (status) {
      case _HUQualityStatus.FREE:
        return false;
      case _HUQualityStatus.RETURN_STOCK:
      case _HUQualityStatus.QUALITY:
      case _HUQualityStatus.LOCKED:
        return true;
      default:
        return false;
    }
  }

  isRawMaterial() {
    let hasHU = false;
    this.batchWarehouses.forEach((bw) => {
      if (bw.HasHandlingUnit)
        hasHU = true;
    });
    return !hasHU;
  }

  printBatch(batch: QualityBatch) {
    this.bToPrint = new QualityBatch();

    for (let key in batch) {
      if (batch.hasOwnProperty(key)) {
        this.bToPrint[key] = batch[key];
      }
    }

    this.loadingService.show();
    this.sapPrintersService.getPrinters({Plant: this.bToPrint.Plant, Thermal: true, RFID: false}).subscribe((response) => {
      if (response.status == 200) {
        this.printers = response['response']['results'];
        this.openDialogPrint();
      }
      else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.tr('NO_PRINTERS_AVAILABLE'),
          autoClose: false,
          confirm: () => {}
        });
      }
    })
  }

  openDialogPrint() {
    this.printDialog.open();
  }

  doPrintBatch() {
    this.printDialog.close();
    this.loadingService.show();
    this.qualityBatchService.saveQualityBatch(this.bToPrint).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 204 || resp.status == 201) {
        this.messageService.open({
          title: ' ',
          error: false,
          message: this.tr("REQUEST_COMPLETED"),
          autoClose: false
        })
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  handleTabChange(event) {
    this.selectedIndex = event.index;
  }
}
