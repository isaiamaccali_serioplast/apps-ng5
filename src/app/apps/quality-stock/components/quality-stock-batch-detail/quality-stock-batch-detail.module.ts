import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {QualityStockBatchDetailComponent} from "./quality-stock-batch-detail.component";
import {QualityStockBatchDetailHUListComponent} from "./quality-stock-batch-detail-hu-list/quality-stock-batch-detail-hu-list.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {QualityStockBatchDetailWhereUsedComponent} from "../quality-stock-batch-detail-where-used/quality-stock-batch-detail-where-used.component";
import {QualityStockBatchDetailWhereUsedLeafComponent} from "../quality-stock-batch-detail-where-used/quality-stock-batch-detail-where-used-leaf/batch-detail-where-used-leaf.component";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [QualityStockBatchDetailComponent,QualityStockBatchDetailHUListComponent,QualityStockBatchDetailWhereUsedLeafComponent,QualityStockBatchDetailWhereUsedComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class QualityStockBatchDetailModule { }


