import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {buildErrorMessage, cloneObject, getUrlDate, getUrlTime} from "../../../../../shared/utils";
import {
  _HUQualityStatus,
  HandlingUnitsQualitySapService,
  HUQuality
} from "../../../../_common/services/handling-units-quality-sap.service";
import {
  TableRowAction,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {PrinterSAP, PrinterSapService} from "../../../../_common/services/printers-sap.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {HULabel, LabelHandlingUnitsQualitySapService} from "../../../../_common/services/label-hu-quality-sap.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-quality-stock-batch-detail-hu-list',
  templateUrl: 'quality-stock-batch-detail-hu-list.component.html',
  styleUrls: ['quality-stock-batch-detail-hu-list.component.css']
})
export class QualityStockBatchDetailHUListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('huTable') huTable: TableWithPagesComponent;
  @ViewChild('printDialog') printDialog: DialogComponent;
  @Input() batch: string;
  @Input() plant: string;
  @Input() material: string;
  @Input() warehouse: string;
  @Input() fromDate: string;
  @Input() fromTime: string;

  // fromDate:any;

  originalFromDate: string;
  originalFromTime: string;

  toDate: string;
  toTime: string;
  huFilter: string;

  fromPallet: number;
  toPallet: number;

  statusFilter: string = "ALL";

  availableActions = [];
  headerConf = [];
  statusCol: any;

  HUQualityStatus = _HUQualityStatus;

  huToPrint: HUQuality;
  printers: PrinterSAP[] = [];
  huLabels: HULabel[] = [];

  ready = false;

  constructor(protected translationService: TranslationService, private huQualityService: HandlingUnitsQualitySapService,
              private loadingService: LoadingService, private messageService: MessageService,
              private sapPrintersService: PrinterSapService, private labelService: LabelHandlingUnitsQualitySapService) {
    super(translationService)
  }

  ngOnInit() {

    let date = new Date();
    this.toDate = getUrlDate(date);
    // date.setDate(date.getDate()-7);
    // this.fromDate = getUrlDate(date);
    this.toTime = getUrlTime(date);

    this.originalFromDate = this.fromDate;
    this.originalFromTime = this.fromTime;

    this.ready = true;
    let actionLock: TableRowAction = new TableRowAction();
    actionLock.icon = "lock";
    actionLock.tooltip = "Lock";
    actionLock.click = this.lockHU.bind(this);
    actionLock.visibilityCondition = (hu: HUQuality) => {
      return this.canLock(hu.StockType);
    };
    let actionUnlock: TableRowAction = new TableRowAction();
    actionUnlock.icon = "lock_open";
    actionUnlock.tooltip = "Unlock";
    actionUnlock.click = this.unlockHU.bind(this);
    actionUnlock.visibilityCondition = (hu: HUQuality) => {
      return this.canUnlock(hu.StockType);
    };

    let actionPrint: TableRowAction = new TableRowAction();
    actionPrint.icon = "print";
    actionPrint.tooltip = "Print";
    actionPrint.click = this.reprintHU.bind(this);

    this.availableActions = [actionLock, actionUnlock, actionPrint];

    this.headerConf = [
      {label: this.tr("SSCC"), valueKey: 'SSCC'},
      {label: this.tr("BATCH"), valueKey: 'Batch'},
      {label: this.tr("MATERIAL"), valueKey: 'Material'},
      {label: this.tr("DESCRIPTION"), valueKey: 'MaterialDescription'},
      {
        label: this.tr("CREATION_DATE"), valueFunction: (item) => {
        let d = item.CreationDate;
        let t = item.CreationTime;
        return [d.substring(0, 4), d.substring(4, 6), d.substring(6, 8)].join("-") + " " + [t.substring(0, 2), t.substring(2, 4), t.substring(4, 6)].join(":")

      }
      },
      // {label: this.tr("CREATION_TIME"), valueKey: 'CreationTime'},
      {
        label: this.tr("QUANTITY"), valueFunction: (item) => {
        return parseFloat(item.Quantity) + " " + item['Uom'];
      }
      },
      // {label: this.tr("SUPPLIER_BATCH"), valueKey: 'SupplierBatch'},
      // {label: this.tr("SUPPLIER"), valueKey: 'Supplier'},
      {label: this.tr("PALLET"), valueKey: 'PalletNr'},
      {label: this.tr("DELIVERY"), valueKey: 'Delivery'},
      {
        label: this.tr("STATUS"), valueFunction: (item) => {
        return this.tr(this.getStatus(item.StockType))
      }
      },
      {
        label: this.tr("MESSAGE"), valueFunction: (item) => {
        return item.message ? '<span class="' + item.messageClass + '">' + item.message + '</span>' : ""
      }
      }
    ];

    this.statusCol = this.headerConf[9];
  }

  getStatus(stockType: string) {
    switch (stockType) {
      case "R":
        return this.HUQualityStatus.RETURN_STOCK;
      case "X":
      case "S":
        return this.HUQualityStatus.LOCKED;
      case "Q":
        return this.HUQualityStatus.QUALITY;
      case "":
      case null:
      default:
        return this.HUQualityStatus.FREE;
    }
  }

  canLock(stockType: string) {
    let status = this.getStatus(stockType);
    switch (status) {
      case this.HUQualityStatus.FREE:
      case this.HUQualityStatus.RETURN_STOCK:
      case this.HUQualityStatus.QUALITY:
        return true;
      case this.HUQualityStatus.LOCKED:
      default:
        return false;
    }
  }

  canUnlock(stockType: string) {
    let status = this.getStatus(stockType);
    switch (status) {
      case this.HUQualityStatus.FREE:
        return false;
      case this.HUQualityStatus.RETURN_STOCK:
      case this.HUQualityStatus.QUALITY:
      case this.HUQualityStatus.LOCKED:
        return true;
      default:
        return false;
    }
  }

  reset() {
    this.fromDate = this.originalFromDate;
    this.fromTime = this.originalFromTime;
    let date = new Date();
    this.toDate = getUrlDate(date);
    // date.setDate(date.getDate()-7);
    // this.fromDate = getUrlDate(date);
    this.toTime = getUrlTime(date);
    this.huFilter = "";
    this.statusFilter = "ALL";
    this.toPallet = null;
    this.fromPallet = null;
    this.huTable.reload();
  }

  reload() {
    this.huTable.reload();
  }

  buildQuery(page?: number, results?: number) {
    let query = {};
    query['Plant'] = this.plant;
    query['Material'] = this.material;
    if (this.fromDate) {
      let d = new Date(this.fromDate);
      query['from'] = getUrlDate(d, "");

    }
    if (this.fromTime) {
      query['fromTime'] = this.fromTime.split(":").join("");
    }

    if (this.toDate) {
      let d = new Date(this.toDate);
      query['to'] = getUrlDate(d, "");
    }

    if (this.toTime) {
      query['toTime'] = this.toTime.split(":").join("");
    }
    query['Batch'] = this.batch;
    query['Warehouse'] = this.warehouse;

    if (this.statusFilter) {
      query['Status'] = this.statusFilter;
    }

    if (this.fromPallet) {
      query['fromPallet'] = this.fromPallet;
    }

    if (this.toPallet) {
      query['toPallet'] = this.toPallet;
    }
    if (this.huFilter) {
      query['SSCC'] = this.huFilter;
    }

    query['page'] = page;
    query['results'] = results;

    return query;
  }

  getHUQualities(page: number, results: number) {
    return this.huQualityService.getHandlingUnitsQuality(this.buildQuery(page, results));
  }

  lockHU(hu: HUQuality, callback?: any) {
    if (!callback) {
      this.huTable.getItems().forEach((hu: HUQuality) => {
        hu.message = "";
      });
    }
    let doUpdate = () => {
      hu.Method = "L";
      this.loadingService.show();
      this.huQualityService.saveHandlingUnitsQuality(hu).subscribe((resp) => {

        if (resp.status == 204) {
          this.huQualityService.getHandlingUnitQuality("'" + hu.SSCC + "'").subscribe((resp) => {
            this.loadingService.hide();
            if (resp.status == 200) {
              let updatedHU: HUQuality = resp['response'];
              hu.StockType = updatedHU.StockType;
              hu.message = "OK";
              hu.messageClass = "success";
              hu.selected = false;
              this.statusCol.active = false;
              this.statusCol.active = true;
              if (callback)
                callback.call(this)
            }
          });
        }
        else {
          this.handleError(resp, hu);
          if (callback)
            callback.call(this)
        }
      });
    };
    if (!callback) {
      this.messageService.open({
        title: this.tr("CONFIRM"),
        message: this.tr("CONFIRM_LOCK_CONTENT"),
        confirm: () => {
          doUpdate();
        },
        deny: () => {
        }
      })
    }
    else {
      doUpdate();
    }
  }

  handleError(resp, hu) {
    hu.messageClass = "error";
    hu.message = resp.message;
    hu.selected = false;
    this.loadingService.hide();
  }

  unlockHU(hu: HUQuality, callback?: any) {
    if (!callback) {
      this.huTable.getItems().forEach((hu: HUQuality) => {
        hu.message = "";
      });
    }
    let doUpdate = () => {
      hu.Method = "U";
      this.loadingService.show();
      this.huQualityService.saveHandlingUnitsQuality(hu).subscribe((resp) => {

        if (resp.status == 204) {
          this.huQualityService.getHandlingUnitQuality("'" + hu.SSCC + "'").subscribe((resp) => {
            this.loadingService.hide();
            if (resp.status == 200) {
              let updatedHU: HUQuality = resp['response'];
              hu.StockType = updatedHU.StockType;
              hu.message = "OK";
              hu.messageClass = "success";
              hu.selected = false;
              this.statusCol.active = false;
              this.statusCol.active = true;
              if (callback)
                callback.call(this)
            }
          });
        }
        else {
          this.handleError(resp, hu);
          if (callback)
            callback.call(this)
        }

      });
    };
    if (!callback) {
      this.messageService.open({
        title: this.tr("CONFIRM"),
        message: this.tr("CONFIRM_UNLOCK_CONTENT"),
        confirm: () => {
          doUpdate();
        },
        deny: () => {
        }
      })
    }
    else {
      doUpdate();
    }
  }

  hasSelectedHU() {
    let selectedItems = this.huTable.getSelectedItems();
    return selectedItems.length > 0;
  }

  lockAllSelected() {
    let huToProcess: HUQuality[] = this.huTable.getSelectedItems() as HUQuality[];

    if (this.checkHUToProcess(huToProcess, 'lock')) {
      this.loadingService.show();
      this.huTable.getItems().forEach((hu: HUQuality) => {
        hu.message = "";
        hu.messageClass = ""
      });
      this.lockHUs(huToProcess, 0, () => {
        this.loadingService.hide();
        this.messageService.open({
          title: "",
          message: this.tr("SAVE_COMPLETED")
        })
      })
    }
    else {
      this.messageService.open({
        title: 'Error',
        error: true,
        message: this.tr("UNABLE_TO_PERFORM_ACTION"),
        autoClose: false
      });
    }
  }

  lockHUs(huToProcess, index, callback) {
    if (huToProcess[index]) {
      this.lockHU(huToProcess[index], () => {
        this.lockHUs(huToProcess, index + 1, callback);
      });
    }
    else {
      callback();
    }
  }

  unlockAllSelected() {
    let huToProcess: HUQuality[] = this.huTable.getSelectedItems() as HUQuality[];
    if (this.checkHUToProcess(huToProcess, 'unlock')) {
      this.loadingService.show();
      this.huTable.getItems().forEach((hu) => {
        hu.message = "";
        hu.messageClass = ""
      });
      this.unlockHUs(huToProcess, 0, () => {
        this.loadingService.hide();
        this.messageService.open({
          title: "",
          message: this.tr("SAVE_COMPLETED")
        })
      })
    }
    else {
      this.messageService.open({
        title: 'Error',
        error: true,
        message: this.tr("UNABLE_TO_PERFORM_ACTION"),
        autoClose: false
      });
    }
  }

  unlockHUs(huToProcess, index, callback) {
    if (huToProcess[index]) {
      this.unlockHU(huToProcess[index], () => {
        this.unlockHUs(huToProcess, index + 1, callback);
      });
    }
    else {
      callback();
    }
  }

  checkHUToProcess(huToProcess: HUQuality[], action: string) {
    let firstHU = huToProcess[0];
    let status = this.getStatus(firstHU.StockType);
    let canLock = this.canLock(firstHU.StockType);
    let canUnlock = this.canUnlock(firstHU.StockType);
    if (action == 'lock' && !canLock)
      return false;
    if (action == 'unlock' && !canUnlock)
      return false;
    let canProcess = true;
    huToProcess.forEach((hu) => {
      let huStatus = this.getStatus(hu.StockType);
      if (huStatus != status) {
        canProcess = false;
      }
    });
    return canProcess;
  }

  reprintHU(hu: HUQuality) {
    this.huToPrint = cloneObject(hu);
    this.loadingService.show();
    this.sapPrintersService.getPrinters({Plant: hu.Plant, Thermal: true, RFID: false}).subscribe((response) => {
      if (response.status == 200) {
        this.printers = response['response']['results'];
        this.labelService.getLabelsHandlingUnitsQuality({Plant: hu.Plant, SSCC: hu.SSCC}).subscribe((response) => {
          if (response.status == 200) {
            this.huLabels = response['response']['results'];
            this.huToPrint.HULabelLink = [new HULabel()];
            this.loadingService.hide();
            this.openDialogPrint();
          }
          else {
            this.messageService.open({
              title: 'Error',
              error: true,
              message: this.tr('NO_LABELS_AVAILABLE'),
              autoClose: false,
              confirm: () => {
              }
            });
          }
        })
      }
      else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.tr('NO_PRINTERS_AVAILABLE'),
          autoClose: false,
          confirm: () => {
          }
        });
      }
    })
  }

  openDialogPrint() {
    this.printDialog.open();
  }

  handleLabelChange() {
    for (let l of this.huLabels) {
      if (this.huToPrint.HULabelLink[0].LabelName == l.LabelName) {
        //lo clono in modo da poter modificare le copie senza problemi
        this.huToPrint.HULabelLink[0] = cloneObject(l);
        this.huToPrint.HULabelLink[0].SSCC = this.huToPrint.SSCC;
      }
    }
  }

  doPrintHU() {
    this.loadingService.show();
    this.huQualityService.createHandlingUnitsQuality(this.huToPrint).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 201) {
        this.printDialog.close();
      }
      else {
        this.printDialog.close();
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  };
}
