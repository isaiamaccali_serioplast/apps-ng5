import {Component} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {QualityStockBatchListQueryService} from "../quality-stock-batch-list/quality-stock-batch-list-query-service";

@Component({
  selector: 'app-quality-stock-index',
  templateUrl: 'quality-stock-index.component.html',
  styleUrls: []
})
export class QualityStockIndexComponent{

  constructor(private sidenavService: SideNavService, private titleService: Title, private queryService:QualityStockBatchListQueryService) {
    this.sidenavService.leftMenuEnabled = false;
    this.sidenavService.leftMenu = false;
    this.sidenavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Quality Stock");
    this.queryService.saveQuery({})
  }
}


