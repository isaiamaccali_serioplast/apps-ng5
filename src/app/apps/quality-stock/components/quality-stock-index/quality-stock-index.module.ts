import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {QualityStockIndexComponent} from "./quality-stock-index.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [QualityStockIndexComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class QualityStockIndexModule { }


