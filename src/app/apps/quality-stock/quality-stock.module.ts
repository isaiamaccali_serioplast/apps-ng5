import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {QualityStockIndexModule} from "./components/quality-stock-index/quality-stock-index.module";
import {QualityStockBatchListModule} from "./components/quality-stock-batch-list/quality-stock-batch-list.module";
import {QualityStockBatchDetailModule} from "./components/quality-stock-batch-detail/quality-stock-batch-detail.module";
import {QualityBatchService} from "./services/quality-batch.service";
import {BatchWarehouseService} from "./services/batch-warehouse.service";
import {BatchStockService} from "./services/batch-stock.service";
import {QualityStockRoutes} from "./quality-stock.routes";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(QualityStockRoutes), QualityStockIndexModule,QualityStockBatchListModule,QualityStockBatchDetailModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [QualityBatchService,BatchWarehouseService,BatchStockService]                    // services
})
export class QualityStockModule { }

