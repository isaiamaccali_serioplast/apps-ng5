import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {BatchStock} from "./batch-stock.service";

export class BatchWarehouse {
  Batch:string;
  Material:string;
  Plant:string;
  Warehouse:string;
  WarehouseDescription:string;
  HasHandlingUnit:boolean;
  WBatchStock:{
    results:BatchStock[];
  }
}

declare let sap: any;

@Injectable()
export class BatchWarehouseService extends GenericSAPService {

  protected serviceName_SAP = "zhuquality_srv";
  protected entitySetName_SAP = "BatchWarehouseSet";

  constructor(protected sapService:SAPService) {
    super(sapService);
  }

  getBatchWarehouses(query: any) {
    let filters = [];

    if(query['Plant']){
      let plantFilter = new sap.ui.model.Filter({
        path: "Plant",
        operator: sap.ui.model.FilterOperator.EQ,
        value1: query['Plant']
      });
      filters.push(plantFilter);
    }

    if(query['Material']){
      let materialFilter = new sap.ui.model.Filter({
        path: "Material",
        operator: sap.ui.model.FilterOperator.EQ,
        value1: query['Material']
      });
      filters.push(materialFilter);
    }

    if(query['Batch']){
      let batchFilter = this.getFilterByLength("Batch",query['Batch'],10);
      filters.push(batchFilter);
    }

    let params = {};
    params['filters'] = filters;
    params['urlParameters'] = {$expand: 'WBatchStock'};

    params = this.checkPagination(params, query);

    return this.getList(params);
  }

  getBatchWarehouse(id: string) {
    let params = {};
    params['urlParameters'] = {$expand: 'WBatchStock'};
    return this.getItem(id,params);
  }
}

