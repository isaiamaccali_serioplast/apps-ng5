import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {Observable} from "rxjs";

export class BatchStock {
  Batch:string;
  Material:string;
  Plant:string;
  Warehouse:string;
  StockType:string;
  Quantity:any;
  Uom:string;
  Action?:string;
}

declare let sap: any;

@Injectable()
export class BatchStockService extends GenericSAPService {

  protected serviceName_SAP = "zhuquality_srv";
  protected entitySetName_SAP = "BatchStockSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  lockBatchStock(bs: BatchStock) {
    return Observable.create((observer) => {
      bs.Action = "L";
      if(bs.StockType=="")
        bs.StockType = " ";
      bs.Quantity = bs.Quantity.toExponential();
      this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, bs).subscribe((response) => {
        observer.next(response);
        observer.complete();
      });
    })
  }

  unlockBatchStock(bs: BatchStock) {
    return Observable.create((observer) => {
      bs.Action = 'U';
      if(bs.StockType=="")
        bs.StockType = " ";
      bs.Quantity = bs.Quantity.toExponential();
      this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, bs).subscribe((response) => {
        observer.next(response);
        observer.complete();
      });
    })
  }

}
