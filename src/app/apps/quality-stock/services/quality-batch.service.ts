import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {_HUQualityStatus} from "../../_common/services/handling-units-quality-sap.service";
import {cloneObject} from "../../../shared/utils";
import {Observable} from "rxjs";

declare let sap: any;

export class BatchTree {
  Level: string;
  Plant: string;
  Batch: string;
  ID: string;
  Material: string;
  MaterialDescription: string;
  PurchaseItem: string;
  PurchaseOrder: string;
  SupplierName: string;
  Supplier: string;
  SupplierBatch: string;
  Uom: string;
  Quantity: string;
  CustomerName: string;
  Delivery: string;
  DeliveryItem: string;
  FSMaterial: string;
  Customer: string;
  FSBatch: string;
  FSPlant: string;
  Type: string;
  WorkOrder: string;
  Bom: string;
  OriginalQuantity: string;
  UsedQuantity: string;
  CreationDate: string;
  CreationTime: string;

  BatchOps: {
    results: WOOperation[]
  }
}

export class WOOperation {
  Level: string;
  Plant: string;
  Material: string;
  Batch: string;
  ID: string;
  Operation: string;
  WorkOrder: string;
  WorkCenter: string;

  OpsPRT: {
    results: WOPRT[]
  }
}

export class WOPRT {
  PRT: string;
  Operation: string;
  WorkOrder: string;
  Level: string;
  Plant: string;
  Material: string;
  Batch: string;
  ID: string;
}

export class BatchText {
  Row: number;
  Plant: string;
  Material: string;
  Batch: string;
  Line: string;
}

export class QualityBatch {
  Plant: string;
  Material: string;
  MaterialDescription?: string;
  Batch: string;
  Uom?: string;
  Quantity?: any;
  OriginalQuantity?: any;
  SupplierBatch?: number;
  CreationDate?: string;
  CreationTime?: string;
  Supplier?: string;
  SupplierName?: string;
  Printer: string;
  Copies: number;
  BatchSon?: {
    results: BatchTree[]
  };
  BatchFather?: {
    results: BatchTree[]
  };
  TextLink?: BatchText[];

  getId() {
    return "Plant='" + this.Plant + "',Material='" + this.Material + "',Batch='" + this.Batch + "'";
  }
}

@Injectable()
export class QualityBatchService extends GenericSAPService {

  protected serviceName_SAP = "zhuquality_srv";
  protected entitySetName_SAP = "QualityBatchSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  getQualityBatches(query: any, params?: any) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        let filters = [];

        if (query['fromDate'] && query ['toDate']) {
          let dateFilter = new sap.ui.model.Filter({
            path: "CreationDate",
            operator: sap.ui.model.FilterOperator.BT,
            value1: query['fromDate'],
            value2: query ['toDate']
          });
          filters.push(dateFilter);
        }

        if (query['Plant']) {
          let plantFilter = new sap.ui.model.Filter({
            path: "Plant",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(plantFilter);
        }

        if (query['Material']) {
          let materialFilter = new sap.ui.model.Filter({
            path: "Material",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Material']
          });
          filters.push(materialFilter);
        }

        if (query['Batch']) {
          let batchFilter = this.getFilterByLength("Batch", query['Batch'], 10);
          filters.push(batchFilter);
        }

        if (query['SupplierBatch']) {
          let sBatchFilter = this.getFilterByLength("SupplierBatch", query['SupplierBatch'], 30);
          filters.push(sBatchFilter);
        }

        if (query['stockOnly']) {
          let quantityFilter = new sap.ui.model.Filter({
            path: "Quantity",
            operator: sap.ui.model.FilterOperator.GT,
            value1: 0
          });
          filters.push(quantityFilter);
        }

        if (query['WorkOrder']) {
          let workorderFilter = this.getFilterByLength("WorkOrder", query['WorkOrder'], 12);
          filters.push(workorderFilter);
        }

        if (query['Delivery']) {
          let deliveryFilter = this.getFilterByLength("Delivery", query['Delivery'], 10);
          filters.push(deliveryFilter);
        }

        if (query['Status']) {
          let statusFilter;
          switch (query['Status']) {
            case _HUQualityStatus.FREE:
              statusFilter = new sap.ui.model.Filter({
                path: "StockType",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: " "
              });
              break;
            case _HUQualityStatus.QUALITY:
              statusFilter = new sap.ui.model.Filter({
                path: "StockType",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: "Q"
              });
              break;
            case _HUQualityStatus.RETURN_STOCK:
              statusFilter = new sap.ui.model.Filter({
                path: "StockType",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: "R"
              });
              break;
            case _HUQualityStatus.LOCKED:
              let filter1 = new sap.ui.model.Filter({
                path: "StockType",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: "X"
              });
              let filter2 = new sap.ui.model.Filter({
                path: "StockType",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: "S"
              });
              statusFilter = new sap.ui.model.Filter({
                filters: [filter1, filter2],
                and: false
              });
              break;
            default:
              break;
          }

          if (statusFilter)
            filters.push(statusFilter);
        }


        params = params ? params : {};
        params['filters'] = filters;

        params['sorters'] = [
          new sap.ui.model.Sorter("CreationDate", true),
          new sap.ui.model.Sorter("CreationTime", true)
        ];

        params = this.checkPagination(params, query);
        this.getList(params).subscribe((resp) => {
          observer.next(resp);
          observer.complete();
        });
      })
    })
  }

  saveQualityBatch(i: QualityBatch, forceCreate?: boolean) {
    return this.saveItem(i, forceCreate);
  }

  getQualityBatch(id: string, params?: any) {
    return this.getItem(id, params);
  }

  clearItem(i: QualityBatch) {
    let i_ = cloneObject(i);
    if (!i_.Copies)
      i_.Copies = 0;
    for (let key in i_) {
      if (i_.hasOwnProperty(key)) {
        if (i_[key] == "" && typeof i_[key] == 'string')
          i_[key] = " ";
      }
    }
    delete i_.BatchSon;
    delete i_.BatchFather;
    delete i_.__metadata;
    return i_;
  }
}

