import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Component} from "@angular/core";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-project-management-index',
  templateUrl: 'project-management-index.component.html',
  styleUrls: ['project-management-index.component.css']
})
export class ProjectManagementIndexComponent{

  constructor(private sidenavService: SideNavService, private titleService: Title) {
    this.sidenavService.leftMenuEnabled = false;
    this.sidenavService.leftMenu = false;
    this.sidenavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Project Management");
  }
}
