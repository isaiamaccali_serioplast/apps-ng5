import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {ProjectManagementIndexComponent} from "./project-management-index.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [ProjectManagementIndexComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class ProjectManagementIndexModule { }


