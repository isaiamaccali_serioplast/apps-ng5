import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {ProjectManagementProjectListComponent} from "./project-management-project-list.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [ProjectManagementProjectListComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class ProjectManagementProjectListModule { }


