import {TranslationService} from "../../../../shared/i18n/translation.service";
import {Component, OnInit, ViewChild} from "@angular/core";
import {Project, ProjectService} from "../../services/project.service";
import {TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {Router} from "@angular/router";
import {ContentTypeService} from "../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../_common/services/status-link.service";
import {StatusService} from "../../../_common/services/status.service";
import {buildErrorMessage, parseSerionetRespToData} from "../../../../shared/utils";
import {ProjectType, ProjectTypesService} from "../../services/project-types.service";
import {HRProfile} from "../../../_common/services/hr-profiles.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

export class ProjectTypeFilter {
  pt: ProjectType;
  active: boolean
}

export class StatusFilter {
  status: StatusLink;
  active: boolean;
}

@Component({
  selector: 'app-project-management-index',
  templateUrl: 'project-management-project-list.component.html',
  styleUrls: ['project-management-project-list.component.css']
})
export class ProjectManagementProjectListComponent extends TranslatableComponent implements OnInit {

  availableActions = [];
  headerConf = [
    {
      label: this.tr("NAME"),
      valueKey: 'name'
    },
    {
      label: this.tr("LEADER"),
      valueKey: 'leader',
      useStr: true
    },
    {
      label: this.tr("TYPES"),
      valueFunction: (p: Project) => p.project_types.map((pt) => pt.__str__).join("; ")
    },
    {
      label: this.tr("STATUS"),
      valueKey: 'status',
      useStr: true
    }
  ];
  noItemsMessage = this.tr("NO_PROJECT_AVAILABLE");
  @ViewChild('projectsTable') projectsTable: TableWithPagesComponent;

  nResults = 10;
  projectTypes: ProjectType[] = [];
  projectStatusLinks: StatusLink[] = [];

  nameFilter: string;
  idFilter: string;
  typesFilters: ProjectTypeFilter[] = [];
  statusFilters: StatusFilter[] = [];
  leaderFilter: number;
  ready = false;

  constructor(protected translationService: TranslationService, private projectService: ProjectService,
              private router: Router, private contentTypeService: ContentTypeService,
              private projectTypesService: ProjectTypesService, private statusLinkService: StatusLinkService,
              private statusService: StatusService, private loadingService:LoadingService,
              private messageService:MessageService) {
    super(translationService)
  }

  ngOnInit() {
    this.contentTypeService.getContentTypes({model: 'project', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {

        this.projectTypesService.getProjectTypes({is_active: true}).subscribe((resp) => {
          this.projectTypes = parseSerionetRespToData(resp);
          this.typesFilters = this.projectTypes.map((pt) => {
            let f = new ProjectTypeFilter();
            f.pt = pt;
            f.active = false;
            return f;
          })
        });

        this.statusLinkService.getStatusLinks({is_active: true, app: resp['results'][0].pk}).subscribe((resp) => {
          this.projectStatusLinks = resp['results'].sort((a: StatusLink, b: StatusLink)=> {
            return a.delta_rel - b.delta_rel;
          });

          this.projectStatusLinks.forEach((sl) => {
            this.statusService.getStatus(sl.status_id.pk).subscribe((s) => {
              sl.status_id = s;
            })
          });

          this.statusFilters = this.projectStatusLinks.map((s) => {
            let f = new StatusFilter();
            f.status = s;
            f.active = false;
            return f;
          });

          this.statusFilters.forEach((sf) => {
            sf.active = [0, 1, 3].indexOf(sf.status.delta_rel) != -1;
          });

          this.ready = true;
        })
      }
    });
  }

  getProjects(page: number, results: number) {

    let query = {
      results: results,
      page: page,
      is_active: true
    };

    if (this.nameFilter)
      query['search'] = this.nameFilter;

    if (this.idFilter)
      query['id'] = this.idFilter;

    if (this.typesFilters.filter((tf) => tf.active).length > 0)
      query['project_type_list'] = this.typesFilters.filter((tf) => tf.active).map((tf) => tf.pt.pk).join("|");

    if (this.statusFilters.filter((tf) => tf.active).length > 0)
      query['status_deltarel_in'] = this.statusFilters.filter((tf) => tf.active).map((tf) => tf.status.delta_rel).join("|");

    if (this.leaderFilter)
      query['leader__user_id'] = this.leaderFilter;

    return this.projectService.getProjects(query);
  }

  editProject(p: Project) {
    this.router.navigate(['project-management', 'detail', p.pk]);
  }

  reloadProjects() {
    this.projectsTable.setPage(1);
    this.projectsTable.reload();
  }

  leaderSelected(user:HRProfile){
    if(user && user.pk){
      this.leaderFilter = user.pk;
    }
    else {
      this.leaderFilter = null;
    }
  }

  downloadCSV(){
    this.loadingService.show();
    let query = {
      is_active: true
    };

    if (this.nameFilter)
      query['search'] = this.nameFilter;

    if (this.idFilter)
      query['id'] = this.idFilter;

    if (this.typesFilters.filter((tf) => tf.active).length > 0)
      query['project_type_list'] = this.typesFilters.filter((tf) => tf.active).map((tf) => tf.pt.pk).join("|");

    if (this.statusFilters.filter((tf) => tf.active).length > 0)
      query['status_deltarel_in'] = this.statusFilters.filter((tf) => tf.active).map((tf) => tf.status.delta_rel).join("|");

    if (this.leaderFilter)
      query['leader__user_id'] = this.leaderFilter;

    this.projectService.getProjects(query, 'csv', 120000).subscribe((resp)=> {
      this.loadingService.hide();
      if(resp.status!=200){
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }
}
