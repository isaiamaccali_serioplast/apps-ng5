import {Component, Input, OnInit, SimpleChange} from "@angular/core";
import {CostStructure} from "../project-management-project-detail.component";
import {WBSCostRevenue} from "../../../../_common/services/project-task.service";
import {sumByKey} from "../../../../../shared/utils";
import {CurrencyPipe} from "@angular/common";
import {TranslationService} from "../../../../../shared/i18n/translation.service";

@Component({
  selector: 'app-cost-structure',
  templateUrl: 'cost-structure.component.html',
  styleUrls: ['cost-structure.component.css']
})
export class CostStructureComponent implements OnInit {

  @Input('costStructures') costStructures: CostStructure[];
  pipe: CurrencyPipe = null;

  constructor(protected translationService: TranslationService) {
    this.pipe = new CurrencyPipe(this.translationService.language);
  }

  ngOnInit() {
  }

  calculateTotal(rows: WBSCostRevenue[], key) {
    return sumByKey(rows, key);
  }

  calculateTotalFull(key) {
    let total = 0;
    this.costStructures.forEach((cs) => {
      cs.rows.forEach((r) => {
        total += r[key];
      });
    });
    return total;
  }

}
