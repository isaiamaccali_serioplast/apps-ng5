import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ProjectTask} from "../../../../_common/services/project-task.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {DomSanitizer} from "@angular/platform-browser";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-project-management-task',
  templateUrl: 'task.component.html',
  styleUrls: ['task.component.css']
})
export class TaskComponent extends TranslatableComponent implements OnInit {

  @Input('task') task:ProjectTask;
  @Input('taskct') contentType:number;
  @Input('disableClick') disableClick:boolean;
  @Input('isFullViewMode') isFullViewMode = false;
  @Output() notifyDelete = new EventEmitter();
  @Output() taskSelected = new EventEmitter();
  ready = false;

  constructor(protected translationService:TranslationService,
              private sanitizer:DomSanitizer) {
    super(translationService);
  }

  ngOnInit() {}

  ngAfterViewInit(){
    setTimeout(()=>{
      this.ready = true;
    },500)

  }

  extractFileName(filePath) {
    let pieces = filePath.split("/");
    return pieces[pieces.length - 1];
  }

  selectTask(){
    this.taskSelected.emit(this.task);
  }

  getSubTaskStyle(s:ProjectTask){
    return this.sanitizer.bypassSecurityTrustStyle('background-color: '+s.status.status_id.color+';')
  }

  getBgColor(){
    if(this.isFullViewMode)
      return this.sanitizer.bypassSecurityTrustStyle('border-bottom-color: '+this.task.text_color+';')
    else
      return this.sanitizer.bypassSecurityTrustStyle('')
  }

  isOnHold(){
    return this.task.status.delta_rel==2;
  }
  isAborted(){
    return this.task.status.delta_rel==5;
  }
}
