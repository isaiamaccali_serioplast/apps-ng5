import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Project, ProjectService} from "../../services/project.service";
import {ProjectMember, ProjectMembersService} from "../../services/project-member.service";
import {aggregate, arrayDistinct, buildErrorMessage, cloneObject, dateDiffInDays, fixValue, getUrlDate, parseSerionetRespToData, randomColorMaterial, sapDateToUrlDate, sumByKey} from "../../../../shared/utils";
import {ProjectTask, ProjectTasksService, WBSCostRevenue, WBSElement} from "../../../_common/services/project-task.service";
import {ContentTypeService} from "../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../_common/services/status-link.service";
import {TypeLink, TypeLinkService} from "../../../_common/services/type-link.service";
import {HRUser} from "../../../_common/services/hr-users.service";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {Department, DepartmentService} from "../../../_common/services/department.service";
import {StatusService} from "../../../_common/services/status.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {PeriodRow} from "../../../../shared/components/period-timeline/period-timeline.component";
import {AvailableStatusesService} from "../../services/available-statuses.service";
import {CommentsService, SerionetComment} from "../../../_common/services/comments.service";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {ConfigurationService} from "../../../../shared/serionet/service/configuration.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {ProjectType, ProjectTypesService} from "../../services/project-types.service";
import {CommentListComponent} from "../../../post-it/components/comment-list/comment-list.component";
import {Observable} from "rxjs";
import {WorkorderSerionet, WorkordersSerionetService} from "../../../_common/services/workorders-serionet.service";
import {HeaderConf, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {WbsCostrevenueSapService} from "../../../_common/services/wbs-costrevenue-sap.service";
import {MatSnackBar, MatTabGroup} from "@angular/material";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

export class StatusButton {
  icon: string;
  action: any;
  tooltip: string;
  index: number;
  dest_delta_rel: number;
  disabled: boolean = true;
}

export class CostStructure {
  year: number;
  rows: WBSCostRevenue[]
}

export class CostStructureCompany {
  company: string;
  costStructure: CostStructure[]
}

@Component({
  selector: 'app-project-management-index',
  templateUrl: 'project-management-project-detail.component.html',
  styleUrls: ['project-management-project-detail.component.css']
})
export class ProjectManagementProjectDetailComponent extends BaseComponent implements OnInit {

  @ViewChild('taskEditDialog') taskEditDialog: DialogComponent;
  @ViewChild('taskViewDialog') taskViewDialog: DialogComponent;
  @ViewChild('projectStatusDialog') projectStatusDialog: DialogComponent;
  @ViewChild('taskStatusDialog') taskStatusDialog: DialogComponent;
  @ViewChild('membersEditDialog') membersEditDialog: DialogComponent;
  @ViewChild('assigneeEditDialog') assigneeEditDialog: DialogComponent;
  @ViewChild('commentList') commentList: CommentListComponent;
  project: Project = new Project();
  projectToEdit: Project = new Project();
  projectToEditMembers: Project = new Project();
  taskStatusLinks: StatusLink[] = [];
  projectStatusLinks: StatusLink[] = [];
  memberTypes: TypeLink[] = [];
  projectTypes: TypeLink[] = [];

  users: HRUser[] = [];
  memberToEdit: ProjectMember;

  taskCT: number;
  projectCT: number;

  taskToEdit: ProjectTask;
  taskToView: ProjectTask;

  ready = false;
  projectEdit = true;

  levels: number[] = [];

  showAllTasks: boolean = false;

  taskEditOn = false;

  wbs: WBSElement;

  costStructureTask: CostStructure[] = [];
  costStructureProject: CostStructure[] = [];
  costStructureCompaniesProject: CostStructureCompany[] = [];
  costStructureCompaniesTask: CostStructureCompany[] = [];

  projectStatusChangeRequestMessage: string;
  availableStatusButtonsProject: StatusButton[] = [];
  projectTaskStatusChangeRequestMessage: string;
  availableStatusButtonsTask: StatusButton[] = [];

  projectApprovalProcessCTPK: number;

  taskToChangeStatus: ProjectTask;

  showComments = true;

  userSites: Site[] = [];

  costsProject: WBSCostRevenue[] = [];

  taskShouldUseEURMode = false;
  projectShouldUseEURMode = false;

  constructor(protected messageService: MessageService, protected translationService: TranslationService,
              protected loadingService: LoadingService, private activatedRoute: ActivatedRoute,
              private projectService: ProjectService, private projectMembersService: ProjectMembersService,
              private projectTasksService: ProjectTasksService, private contentTypeService: ContentTypeService,
              private statusLinkService: StatusLinkService, private typeLinkService: TypeLinkService,
              private departmentsService: DepartmentService,
              private router: Router, private statusService: StatusService, private sanitizer: DomSanitizer,
              private sitesService: SitesService, private availableStatusesService: AvailableStatusesService,
              private commentsService: CommentsService, private appPlantUsersService: AppPlantUsersService,
              private confService: ConfigurationService, private snackBar: MatSnackBar, private loginService: LoginService,
              private projectTypesService: ProjectTypesService, private workordersSerionetService: WorkordersSerionetService,
              private wbsCostrevenueSapService: WbsCostrevenueSapService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    let count = 5;
    this.loadingService.show();
    let callback = () => {
      count--;
      if (count == 0) {
        this.loadingService.hide();
        this.activatedRoute.params.subscribe((params) => {
          let query = {
            app__app_root: 'project-management',
            is_active: true
          };
          this.loadingService.show();
          this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
            this.loadingService.hide();
            if (response['results'] && response['results'].length == 1) {
              this.confService.set("SAP_USER", response['results'][0].user, true);
              this.confService.set("SAP_PASSWORD", response['results'][0].password, true);

              if (params['p']) {
                if (params['p'] == 'create') {
                  this.projectEdit = true;
                  this.projectToEdit.type = this.projectTypes.filter((pt) => pt.delta_rel == 1)[0];
                  this.projectToEdit.members = [];
                  let leader = new ProjectMember();
                  leader.type = cloneObject(this.memberTypes.find((tl) => tl.delta_rel == 0));
                  leader.user = this.loginService.user;
                  leader.custom_status = "toSave";
                  this.projectToEdit.members.push(leader);
                  this.projectToEdit.leader = leader;
                  this.projectToEdit.project_types = [];
                  this.projectTypes.forEach((pt) => {
                    let pt_: ProjectType = cloneObject(pt);
                    pt_.is_active = false;
                    this.projectToEdit.project_types.push(pt_);
                  });
                  this.ready = true;
                }
                else {
                  this.loadProject(parseInt(params['p'], 10));
                }
              } else {
                this.router.navigate(["/index"]);
                this.snackBar.open("NO_SAP_LICENCE_FOR_APP_PROJECT_MANAGEMENT", null, {
                  duration: 5000,
                  panelClass: ['error']
                });
              }
            }
          })
        });
      }
    };

    this.contentTypeService.getContentTypes({model: 'projecttask', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {
        this.taskCT = resp['results'][0].pk;
        this.statusLinkService.getStatusLinks({is_active: true, app: resp['results'][0].pk}).subscribe((resp) => {
          this.taskStatusLinks = resp['results'].sort((a: StatusLink, b: StatusLink) => {
            return a.delta_rel - b.delta_rel;
          });
          let count = this.taskStatusLinks.length;
          let callback2 = () => {
            count--;
            if (count == 0) {
              callback();
            }
          };

          this.taskStatusLinks.forEach((sl) => {
            this.statusService.getStatus(sl.status_id.pk).subscribe((s) => {
              sl.status_id = s;
              callback2();
            })
          })
        })
      }
    });

    this.contentTypeService.getContentTypes({model: 'projectmember', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {
        this.typeLinkService.getTypeLinks({is_active: true, app: resp['results'][0].pk}).subscribe((resp) => {
          this.memberTypes = resp['results'].sort((a: StatusLink, b: StatusLink) => {
            return a.delta_rel - b.delta_rel;
          });
          callback();
        });

      }
    });

    this.contentTypeService.getContentTypes({model: 'project', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {

        this.projectCT = resp['results'][0].pk;

        let count = 2;
        let callback3 = () => {
          count--;
          if (count == 0) {
            callback();
          }
        };

        this.projectTypesService.getProjectTypes({is_active: true}).subscribe((resp) => {
          this.projectTypes = parseSerionetRespToData(resp);
          callback3();
        });
        callback3();

        this.statusLinkService.getStatusLinks({is_active: true, app: this.projectCT}).subscribe((resp) => {
          this.projectStatusLinks = resp['results'].sort((a: StatusLink, b: StatusLink) => {
            return a.delta_rel - b.delta_rel;
          });
          let count = this.projectStatusLinks.length;
          let callback2 = () => {
            count--;
            if (count == 0) {
              callback3();
            }
          };

          this.projectStatusLinks.forEach((sl) => {
            this.statusService.getStatus(sl.status_id.pk).subscribe((s) => {
              sl.status_id = s;
              callback2();
            })
          })
        })
      }
    });

    this.contentTypeService.getContentTypes({model: 'projectapprovalprocess', is_active: true}).subscribe((resp) => {
      if (resp && resp['count'] && resp['count'] > 0) {
        this.projectApprovalProcessCTPK = resp['results'][0].pk;
        callback();
      }
    });

    this.sitesService.getSites({user: this.loginService.user.pk}).subscribe((resp) => {
      this.userSites = parseSerionetRespToData(resp);
      callback();
    });
  }

  loadProject(projectPk: number) {
    this.loadingService.show();
    this.projectService.getProject(projectPk).subscribe((project) => {
      if (project.pk) {
        this.project = project;
        fixValue(this.projectStatusLinks, this.project, 'status');
        this.projectEdit = false;
        this.loadProjectMembers(() => {
          this.loadProjectTasks(() => {
            this.sitesService.getSite(this.project.site.pk).subscribe((s) => {
              this.project.site = s;
              let types: number[] = cloneObject(this.project.project_types).map((pt) => pt.pk);
              this.project.project_types = [];
              this.projectTypes.forEach((pt) => {
                let pt_: ProjectType = cloneObject(pt);
                pt_.is_active = types.indexOf(pt_.pk) != -1;
                this.project.project_types.push(pt_);
              });
              this.setReadonlyByProject();
              this.loadingService.hide();
              this.ready = true;
              this.activatedRoute.queryParams.subscribe((params) => {
                if (params['task']) {
                  let taskPK = parseInt(params['task'], 10);
                  let task = this.project.tasksAll.filter((pt) => pt.pk == taskPK)[0];
                  this.taskSelected(task);
                }
              });
            });
          });
        });
      }
      else {
        this.loadingService.hide();
        this.router.navigate(["project-management"]);
      }
    })
  }

  loadProjectMembers(callback) {
    this.projectMembersService.getProjectMembers({project: this.project.pk}).subscribe((resp) => {
      this.project.members = parseSerionetRespToData(resp);
      this.project.members.forEach((m) => {
        m.type = cloneObject(this.memberTypes.filter((t) => t.pk == m.type.pk)[0]);
      });
      fixValue(this.project.members, this.project, 'leader');
      callback.call(this);
    })
  }

  loadProjectTasks(callback) {
    this.projectTasksService.getProjectTasks({project: this.project.pk, is_active: true}).subscribe((resp) => {
      this.project.tasks = parseSerionetRespToData(resp);
      this.buildTaskStructures();
      this.project.tasksAll.forEach((t) => {
        t.assignee = cloneObject(this.project.members.filter((m) => m.pk == t.assignee.pk)[0]);
        t.status = cloneObject(this.taskStatusLinks.filter((s) => s.pk == t.status.pk)[0]);
      });
      callback.call(this);
    })
  }

  buildTaskStructures() {
    let allTasks = [];
    this.processTasks(allTasks, this.project.tasks);
    this.project.tasksAll = allTasks;
  }

  processTasks(allTasks: ProjectTask[], tasksToProcess: ProjectTask[]) {
    tasksToProcess.forEach((t) => {
      allTasks.push(t);
      if (t.children)
        this.processTasks(allTasks, t.children);
    });
  }

  selectUser(u: HRUser) {
    if (u && u.pk) {
      this.memberToEdit.user = u;
    }
    else {
      this.memberToEdit.user = new HRUser();
    }
  }

  createTask(parent?: ProjectTask) {
    this.taskToEdit = new ProjectTask();
    this.taskToEdit.project = this.project;
    this.taskToEdit.status = cloneObject(this.taskStatusLinks.filter((sl) => sl.delta_rel == 0)[0]);
    this.taskToEdit.assignee = new ProjectMember();
    this.taskToEdit.department = new Department();
    if (parent) {
      this.taskToEdit.parent = parent;
      this.taskToEdit.text_color = parent.text_color;
      if (parent.feasibility)
        this.taskToEdit.feasibility = true;
    }
    else {
      let currentColors = this.project.tasksAll.map((pt) => pt.text_color);
      let proceed = true;
      let attempts = 250;
      while (proceed) {
        let color = randomColorMaterial();
        if (currentColors.indexOf(color) == -1) {
          this.taskToEdit.text_color = color;
          proceed = false;
        }
        attempts--; //Per sicurezza
        if (attempts == 0)
          proceed = false;
      }
    }
    this.taskToEdit.site = cloneObject(this.project.site);

    this.taskEditDialog.open();
  }

  selectedTabIndex = 0;

  saveTaskToEdit() {

    this.loadingService.show();
    if (!this.taskToEdit.forecast_end_date || !this.taskToEdit.pk)
      this.taskToEdit.forecast_end_date = this.taskToEdit.original_end_date;
    if (!this.taskToEdit.forecast_start_date || !this.taskToEdit.pk)
      this.taskToEdit.forecast_start_date = this.taskToEdit.original_start_date;
    this.projectTasksService.saveProjectTask(this.taskToEdit).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.taskEditDialog.close();
        this.loadProjectTasks(() => {
          this.loadingService.hide();
          this.taskToEdit = null;
          this.selectedTabIndex = 1;
        })
      });
    })
  }

  loadAssigneeDepartments() {
    fixValue(this.project.members, this.taskToEdit, 'assignee');
    this.departmentsService.getDeparments({
      is_active: true,
      user_id: this.taskToEdit.assignee.user.pk
    }).subscribe((resp) => {
      this.taskToEdit.assignee.departments = parseSerionetRespToData(resp);
      if (this.taskToEdit.assignee.departments.length == 1) {
        this.taskToEdit.department = this.taskToEdit.assignee.departments[0];
      }
    })
  }

  savingProject = false;

  saveProject() {

    let isCreate = !this.projectToEdit.pk;
    this.savingProject = true;
    if (!this.projectToEdit.pk) {
      this.projectToEdit.status = cloneObject(this.projectStatusLinks.filter((sl) => sl.delta_rel == 0)[0]);
    }

    this.loadingService.show();
    this.projectService.saveProject(this.projectToEdit).subscribe((resp) => {
      this.manageRespSave(resp, () => {

        let finishSaving = () => {
          this.loadingService.hide();
          this.messageService.open({
            message: this.tr('SAVE_COMPLETED'),
            title: " ",
            autoClose: false,
            onClose: () => {
              if (this.project.pk) {
                this.ready = false;
                this.loadProject(this.project.pk);
              }
              else {
                this.router.navigate(['project-management', 'detail', resp.pk])
              }
            }
          });
          this.savingProject = false;
        };

        if (isCreate) {
          let members = this.projectToEdit.members;
          members.forEach((pm) => {
            pm.project = resp;
          });
          this.loadingService.show();
          this.saveProjectMembers(members, () => {
            finishSaving();
          })
        }
        else {
          finishSaving();
        }
      }, true)
    })
  }

  addMember() {
    let memberToEdit = new ProjectMember();
    memberToEdit.type = new TypeLink();
    memberToEdit.custom_status = "toSave";
    memberToEdit.user = new HRUser();
    memberToEdit.project = new Project();
    memberToEdit.project.pk = this.projectToEditMembers.pk;
    memberToEdit.is_active = true;
    this.memberToEdit = memberToEdit;
  }

  editMember(m: ProjectMember) {
    this.memberToEdit = cloneObject(m);
  }

  deleteMember(m: ProjectMember) {
    if (m.pk) {
      let assignees = this.projectToEditMembers.tasks.map((t) => t.assignee.pk);
      if (assignees.indexOf(m.pk) != -1) {
        this.messageService.open({
          message: this.tr('MEMBER_HAS_TASKS_NO_DELETE'),
          title: " ",
          autoClose: true
        });
      }
      else {
        m.custom_status = "toDelete";
        m.is_active = false;
      }
    }
    else {
      m.custom_status = "toDelete";
      m.is_active = false;
    }
  }

  confirmMember() {
    this.memberToEdit.custom_status = "toSave";
    if (this.memberToEdit.pk) {
      this.projectToEditMembers.members.forEach((pm, index) => {
        if (pm.pk == this.memberToEdit.pk)
          this.projectToEditMembers.members[index] = cloneObject(this.memberToEdit);
      });
    }
    else {
      this.projectToEditMembers.members.push(cloneObject(this.memberToEdit))
    }
    this.memberToEdit = null;
  }

  saveMembers() {
    this.loadingService.show();
    this.saveProjectMembers(this.projectToEditMembers.members, () => {
      this.loadProjectMembers(() => {
        this.loadingService.hide();
        this.membersEditDialog.close();
      })
    })
  }

  saveProjectMembers(members: ProjectMember[], callback) {
    let count = members.length;
    let cb = () => {
      count--;
      if (count == 0) {
        callback.call(this);
      }
    };

    members.forEach((pm) => {
      if (pm.custom_status) {
        if (pm.custom_status == "toSave") {
          this.projectMembersService.saveProjectMember(pm).subscribe((resp) => {
            this.manageRespSave(resp, () => {
              cb();
            }, true)
          })
        }
        else if (pm.custom_status == "toDelete") {
          if (pm.pk) {
            this.projectMembersService.deleteProjectMember(pm).subscribe((resp) => {
              this.manageRespDelete(resp, () => {
                cb();
              }, true)
            })
          }
          else {
            cb();
          }
        }
      }
      else {
        cb();
      }
    })
  }

  backToList() {
    this.router.navigate(['project-management']);
  }

  fixMemberType() {
    fixValue(this.memberTypes, this.memberToEdit, 'type');
    if (this.memberToEdit.type.delta_rel == 0) {
      let currentLeader = this.projectToEditMembers.members.filter((pm) => pm.type.delta_rel == 0);
      let teamMemberType = cloneObject(this.memberTypes.filter((t) => t.delta_rel == 1)[0]);
      if (currentLeader.length > 0) {
        if (currentLeader[0].user.pk != this.memberToEdit.user.pk) {
          this.messageService.open({
            message: this.tr('SWITCH_LEADER_CONFIRM'),
            title: " ",
            autoClose: false,
            confirm: () => {
              currentLeader[0].type = teamMemberType;
              currentLeader[0].custom_status = "toSave";
            },
            deny: () => {
              this.memberToEdit.type = teamMemberType
            }
          });
        }

      }
    }
  }

  editProject() {
    this.projectEdit = true;
    this.projectToEdit = cloneObject(this.project);
  }

  getProjectStatusStyle() {
    let status_id = {
      color: 'white',
      text_color: 'black'
    };
    if (this.project.status.status_id)
      status_id = cloneObject(this.project.status.status_id);
    return this.sanitizer.bypassSecurityTrustStyle('width:calc(100% - 55px); margin-right:10px; text-align:center; font-weight:bold; height:30px; line-height:30px; background-color: ' + status_id.color + '; color: ' + status_id.text_color)
  }

  getTaskStatusStyle() {
    let status_id = {
      color: 'white',
      text_color: 'black'
    };
    if (cloneObject(this.getTask(this.levels[this.levels.length - 1]).status.status_id))
      status_id = cloneObject(this.getTask(this.levels[this.levels.length - 1]).status.status_id);
    return this.sanitizer.bypassSecurityTrustStyle('width:calc(100% - 50px);  margin-right:10px; text-align:center; font-weight:bold; height:30px; line-height:30px; background-color: ' + status_id.color + '; color: ' + status_id.text_color)
  }

  getTaskStatusColorByDeltaRel(b: StatusButton) {
    if (!b.disabled) {
      let status_id = this.taskStatusLinks.filter((sl) => sl.delta_rel == b.dest_delta_rel)[0].status_id;
      return this.sanitizer.bypassSecurityTrustStyle('background-color: ' + status_id.color + '; color: ' + status_id.text_color);
    }
    else {
      return this.sanitizer.bypassSecurityTrustStyle('');
    }

  }

  getProjectStatusColorByDeltaRel(b: StatusButton) {
    if (!b.disabled) {
      let status_id = this.projectStatusLinks.filter((sl) => sl.delta_rel == b.dest_delta_rel)[0].status_id;
      return this.sanitizer.bypassSecurityTrustStyle('background-color: ' + status_id.color + '; color: ' + status_id.text_color);
    }
    else {
      return this.sanitizer.bypassSecurityTrustStyle('');
    }
  }

  taskDeleted(t: ProjectTask) {
    let taskPk = this.project.tasks.map((t) => t.pk);
    this.project.tasks.splice(taskPk.indexOf(t.pk), 1);
  }

  selectTaskById(taskPk: number) {

    const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);

    // Do sth about the params
    queryParams['task'] = taskPk;

    this.workorderToView = null;
    this.router.navigate([], {queryParams: queryParams, relativeTo: this.activatedRoute});

  }

  taskSelected(pt: ProjectTask) {
    let pks = [];
    pks.push(pt.pk);
    if (pt.parent && pt.parent.pk) {
      let pt_ = cloneObject(pt);
      while (pt_.parent && pt_.parent.pk) {
        pks.push(pt_.parent.pk);
        pt_ = cloneObject(this.project.tasksAll.filter((t) => t.pk == pt_.parent.pk)[0]);
      }
      if (!pt_.parent)
        pks.push(pt_.pk);
    }
    this.levels = pks.reverse();
    this.wbs = new WBSElement();
    this.setReadonlyTask(pt.pk);
    this.selectedTabIndex = 0;
    this.selectTaskById(pt.pk);
  }

  backToProject() {
    this.levels = [];
    this.selectTaskById(null);
  }

  levelSelected(pt: number) {
    let index = this.levels.indexOf(pt);
    if (index != (this.levels.length - 1)) {
      this.levels = this.levels.slice(0, index + 1);
      this.wbs = new WBSElement();
      this.setReadonlyTask(this.levels[this.levels.length - 1]);
      this.selectTaskById(this.levels[this.levels.length - 1]);
    }
  }

  backLevel() {
    if (this.levels.length > 1) {
      this.levelSelected(this.levels[this.levels.length - 2]);
    }
    else {
      this.backToProject();
    }
  }

  getTask(taskPK: number): ProjectTask {
    if (this.project.tasksAll)
      return this.project.tasksAll.filter((t) => t.pk == taskPK)[0];
    else
      return new ProjectTask();
  }

  siteSelected(s: Site) {
    if (s && s.pk) {
      this.projectToEdit.site = s;
    }
    else {
      this.projectToEdit.site = null;
    }
  }

  siteChangeTask(s: Site) {
    if (s && s.pk) {
      this.taskToEdit.site = s;
    }
    else {
      this.taskToEdit.site = null;
    }
  }

  canSaveProject() {
    return this.projectToEdit.start_date && this.projectToEdit.name && this.projectToEdit.description &&
      this.projectToEdit.planned_end_date && this.projectToEdit.site && this.projectToEdit.site.pk &&
      this.projectToEdit.project_types.filter((pt) => pt.is_active).length > 0 && this.areDateAligned(this.projectToEdit.start_date, this.projectToEdit.planned_end_date);
  }

  areDateAligned(start_date: string, end_date: string) {
    let d1 = new Date(start_date);
    let d2 = new Date(end_date);
    return d2.getTime() - d1.getTime() >= 0;
  }

  taskInRequest = false;

  reloadCosts(taskPk?: number, callback?: any) {
    this.aggregateCostRevenuesProject(() => {
      if (taskPk) {
        this.getWBS(taskPk, () => {
          this.aggregateCostRevenues();
          if (callback)
            callback();
        })
      }
    });
  }

  getWBS(taskPK: number, callback?: any) {
    if (!this.taskInRequest) {
      this.taskInRequest = true;
      this.project.tasksAll.forEach((t) => {
        if (t.pk == taskPK) {
          this.loadingService.show();
          this.loadWBS(t).subscribe((resp) => {
            this.loadingService.hide();
            this.taskInRequest = false;
            if (resp.status == 200) {
              this.wbs = resp.response;
              this.aggregateCostRevenues();
              this.wbs.hasAskedData = true;
              if (callback)
                callback();
            }
            else {
              this.wbs = new WBSElement();
              resp.context = "SAP";
              this.messageService.open({
                title: 'Error',
                error: true,
                message: buildErrorMessage(resp),
                autoClose: false
              });
            }
          })
        }
      });
    }
  }

  showWBSCostsOnly = false;
  disableSwitch = false;

  filterCosts() {
    let taskSapNames = [];
    let currentTask = this.getTask(this.levels[this.levels.length - 1]);

    if (!currentTask.children) {
      this.showWBSCostsOnly = true;
      this.disableSwitch = true;
    }
    else {
      this.disableSwitch = false;
    }

    if (this.showWBSCostsOnly) {
      taskSapNames.push(currentTask.sap_name);
    }
    else {
      this.extractNames(taskSapNames, currentTask);
    }
    let costs = this.costsProject.filter((c) => taskSapNames.indexOf(c.WBS) != -1);

    let currencies = arrayDistinct(costs.map((c) => c.Currency));
    this.taskShouldUseEURMode = currencies.length > 1;

    this.costStructureTask = this.buildCostStructure(costs, this.taskShouldUseEURMode);

    this.costStructureCompaniesTask = [];
    let comp = arrayDistinct(costs.map((c: WBSCostRevenue) => c.CompanyCode));
    comp.forEach((c) => {
      let s = new CostStructureCompany();
      s.company = c;
      s.costStructure = this.buildCostStructure(costs.filter((c_) => c_.CompanyCode == c));
      this.costStructureCompaniesTask.push(s);
    });

  }

  loadWBS(pt: ProjectTask) {
    return this.projectTasksService.getWBS(pt, {urlParameters: {$expand: "POLink,WOLink,SOLink"}});
  }

  getEventRows(task: ProjectTask) {
    let originalPeriod = new PeriodRow();
    originalPeriod.name = "Original";
    if (task.original_start_date && task.original_end_date) {
      let end = new Date(task.original_end_date);
      end.setHours(23);
      originalPeriod.event = {
        start: new Date(task.original_start_date),
        end: end,
        color: '#FF8A65',
        title: 'Original',
      };
    }
    let forecastPeriod = new PeriodRow();
    forecastPeriod.name = "Forecast";
    if (task.forecast_start_date && task.forecast_end_date) {
      let end = new Date(task.forecast_end_date);
      end.setHours(23);
      forecastPeriod.event = {
        start: new Date(task.forecast_start_date),
        end: end,
        color: '#4FC3F7',
        title: 'Forecast',
      };
    }
    let actualPeriod = new PeriodRow();
    actualPeriod.name = "Actual";
    if (task.actual_start_date && task.actual_end_date) {
      let end = new Date(task.actual_end_date);
      end.setHours(23);
      actualPeriod.event = {
        start: new Date(task.actual_start_date),
        end: end,
        color: '#81C784',
        title: 'Forecast',
      };
    }
    return [originalPeriod, forecastPeriod, actualPeriod];
  }

  viewFullTask(pt: ProjectTask) {
    this.taskToView = cloneObject(pt);
    if (this.taskToView.parent && this.taskToView.parent.pk)
      this.taskToView.parent = this.project.tasksAll.filter((pt) => pt.pk == this.taskToView.parent.pk)[0];
    this.taskViewDialog.open();
  }

  editTask(pt: ProjectTask) {
    this.taskToEdit = cloneObject(pt);
    if (this.taskToEdit.parent && this.taskToEdit.parent.pk)
      this.taskToEdit.parent = this.project.tasksAll.filter((pt) => pt.pk == this.taskToEdit.parent.pk)[0];
    this.taskEditDialog.open();
  }

  getSubTaskStyle(s: ProjectTask) {
    return this.sanitizer.bypassSecurityTrustStyle('background-color: ' + s.status.status_id.color + ';')
  }

  changeProjectStatus(pr: Project) {
    this.projectStatusChangeRequestMessage = "";
    this.availableStatusesService.getAvailableProjectStatuses(pr.pk).subscribe((resp) => {
      let projectAvailableStatuses = parseSerionetRespToData(resp);
      this.availableStatusButtonsProject = [];
      let currentStatusDeltarel = pr.status.delta_rel;

      let buttonAbort = new StatusButton();
      buttonAbort.icon = "clear";
      buttonAbort.index = 0;

      let buttonPrev = new StatusButton();
      buttonPrev.icon = "skip_previous";
      buttonPrev.index = 1;

      let buttonPause = new StatusButton();
      buttonPause.icon = "pause";
      buttonPause.index = 2;

      let buttonPlay = new StatusButton();
      buttonPlay.icon = "play_arrow";
      buttonPlay.index = 3;

      let buttonNext = new StatusButton();
      buttonNext.icon = "skip_next";
      buttonNext.index = 4;

      projectAvailableStatuses.forEach((sl: StatusLink) => {
        let button = null;
        switch (sl.delta_rel) {
          case 1:
            //Released
            if (currentStatusDeltarel == 0) {
              button = buttonNext;
            }
            else if (currentStatusDeltarel == 2) {
              button = buttonPlay;
            }
            else if (currentStatusDeltarel == 3) {
              button = buttonPrev;
            }
            break;
          case 2:
            //On Hold
            button = buttonPause;
            break;
          case 3:
            //Under Review
            button = buttonNext;
            break;
          case 4:
            //Close Completed
            button = buttonNext;
            break;
          case 5:
            //Close Aborted
            button = buttonAbort;
            break;
          default:
            return null;
        }
        if (button) {
          button.disabled = false;
          button.dest_delta_rel = sl.delta_rel;
          button.tooltip = sl.__str__;
          button.action = () => {
            this.makeChangeProjectStatusRequest(pr, sl.delta_rel);
          };
        }
      });

      this.availableStatusButtonsProject.push(buttonAbort, buttonPrev);
      if (!buttonPause.disabled)
        this.availableStatusButtonsProject.push(buttonPause);
      if (!buttonPlay.disabled)
        this.availableStatusButtonsProject.push(buttonPlay);
      if (buttonPlay.disabled && buttonPause.disabled)
        this.availableStatusButtonsProject.push(buttonPause);
      this.availableStatusButtonsProject.push(buttonNext);

      this.projectStatusDialog.open();
    });
  }

  changeTaskStatus(pt: ProjectTask) {
    this.projectTaskStatusChangeRequestMessage = "";
    this.availableStatusesService.getAvailableProjectTaskStatuses(pt.pk).subscribe((resp) => {
      let projectTaskAvailableStatuses = parseSerionetRespToData(resp);
      this.availableStatusButtonsTask = [];
      let currentStatusDeltarel = pt.status.delta_rel;
      let buttonAbort = new StatusButton();
      buttonAbort.icon = "clear";
      buttonAbort.index = 0;

      let buttonPrev = new StatusButton();
      buttonPrev.icon = "skip_previous";
      buttonPrev.index = 1;

      let buttonPause = new StatusButton();
      buttonPause.icon = "pause";
      buttonPause.index = 2;

      let buttonPlay = new StatusButton();
      buttonPlay.icon = "play_arrow";
      buttonPlay.index = 3;

      let buttonNext = new StatusButton();
      buttonNext.icon = "skip_next";
      buttonNext.index = 4;

      projectTaskAvailableStatuses.forEach((sl: StatusLink) => {
        let button = null;
        switch (sl.delta_rel) {
          case 1:
            //Released
            if (currentStatusDeltarel == 0) {
              button = buttonNext;
            }
            else if (currentStatusDeltarel == 2) {
              button = buttonPlay;
            }
            else if (currentStatusDeltarel == 3) {
              button = buttonPrev;
            }
            break;
          case 2:
            //On Hold
            button = buttonPause;
            break;
          case 3:
            //Under Review
            button = buttonNext;
            break;
          case 4:
            //Close Completed
            button = buttonNext;
            break;
          case 5:
            //Close Aborted
            button = buttonAbort;
            break;
          default:
            return null;
        }
        if (button) {
          button.disabled = false;
          button.dest_delta_rel = sl.delta_rel;
          button.tooltip = sl.__str__;
          button.action = () => {
            this.makeChangeProjectTaskStatusRequest(pt, sl.delta_rel);
          };
        }
      });

      this.availableStatusButtonsTask.push(buttonAbort, buttonPrev);
      if (!buttonPause.disabled)
        this.availableStatusButtonsTask.push(buttonPause);
      if (!buttonPlay.disabled)
        this.availableStatusButtonsTask.push(buttonPlay);
      if (buttonPlay.disabled && buttonPause.disabled)
        this.availableStatusButtonsTask.push(buttonPause);
      this.availableStatusButtonsTask.push(buttonNext);

      this.taskToChangeStatus = pt;
      this.taskStatusDialog.open();
    });
  }

  makeChangeProjectStatusRequest(pr: Project, delta_rel_request: number) {

    this.messageService.open({
      message: this.tr('CONFIRM_STATUS_CHANGE', [{status: this.getStatusNameByDeltaRel(delta_rel_request)}]),
      title: " ",
      autoClose: false,
      confirm: () => {
        this.loadingService.show();
        this.projectService.makeStatusChangeRequest(pr, delta_rel_request).subscribe((resp) => {
          this.projectStatusDialog.close();
          this.manageRespSave(resp, () => {
            this.loadingService.hide();
            pr.next_status = resp.next_status;
            pr.status = resp.status;
            pr.process_id = resp.process_id;
            fixValue(this.projectStatusLinks, pr, 'status');
            this.setReadonlyByProject();
            this.postComment(this.projectStatusChangeRequestMessage, pr.process_id, () => {
              if (delta_rel_request == 5) {
                this.ready = false;
                this.loadProject(this.project.pk);
              }
            });
          }, true);
        });
      },
      deny: () => {
      }
    });

  }

  makeChangeProjectTaskStatusRequest(pt: ProjectTask, delta_rel_request: number) {
    this.messageService.open({
      message: this.tr('CONFIRM_STATUS_CHANGE', [{status: this.getStatusNameByDeltaRel(delta_rel_request)}]),
      title: " ",
      autoClose: false,
      confirm: () => {
        this.loadingService.show();
        this.projectTasksService.makeStatusChangeRequest(pt, delta_rel_request).subscribe((resp) => {
          this.taskStatusDialog.close();
          this.manageRespSave(resp, () => {
            this.loadingService.hide();
            pt.next_status = resp.next_status;
            pt.status = resp.status;
            pt.actual_start_date = resp.actual_start_date;
            pt.actual_end_date = resp.actual_end_date;
            pt.process_id = resp.process_id;
            fixValue(this.taskStatusLinks, pt, 'status');
            this.setReadonlyTask(pt.pk);
            this.postComment(this.projectTaskStatusChangeRequestMessage, pt.process_id, () => {
              this.showComments = false;
              setTimeout(() => {
                this.showComments = true;
              }, 500);
              if (delta_rel_request == 5) {
                this.ready = false;
                this.loadProject(this.project.pk);
              }
            }, pt.pk);
          }, true);
        });
      },
      deny: () => {
      }
    });
  }

  getStatusNameByDeltaRel(delta_rel: number) {
    return this.taskStatusLinks.filter((sl) => sl.delta_rel == delta_rel)[0].__str__;
  }

  aggregateCostRevenues() {
    this.showWBSCostsOnly = false;
    this.filterCosts();
  }

  extractNames(list: string[], task: ProjectTask) {
    list.push(task.sap_name);
    if (task.children) {
      task.children.forEach((t) => {
        this.extractNames(list, t);
      })
    }
  }

  buildCostStructure(data: WBSCostRevenue[], useEurValues = false) {
    data.forEach((c) => {
      c.ActualQty = parseFloat(c.ActualQty + "");
      c.ActualValue = parseFloat(c.ActualValue + "");
      c.PlannedQty = parseFloat(c.PlannedQty + "");
      c.PlannedValue = parseFloat(c.PlannedValue + "");
      c.FinancialCommit = parseFloat(c.FinancialCommit + "");
      if (c.ActualValueEUR)
        c.ActualValueEUR = parseFloat(c.ActualValueEUR + "");
      if (c.PlannedValueEUR)
        c.PlannedValueEUR = parseFloat(c.PlannedValueEUR + "");
      if (c.FinancialCommitEUR)
        c.FinancialCommitEUR = parseFloat(c.FinancialCommitEUR + "");
    });
    let structure = {};
    let costStructure = [];
    structure = aggregate(structure, data, ['Year', 'CostingRevenueVoice'], 0);
    for (let year in structure) {
      if (structure.hasOwnProperty(year)) {
        let cs = new CostStructure();
        cs.year = parseInt(year, 10);
        cs.rows = [];
        for (let voice in structure[year]) {
          if (structure[year].hasOwnProperty(voice)) {
            let rows = structure[year][voice];
            let csRow = new WBSCostRevenue();
            csRow.Year = rows[0].Year;
            csRow.CostingRevenueDescription = rows[0].CostingRevenueDescription;
            csRow.CostingRevenueVoice = rows[0].CostingRevenueVoice;
            csRow.WBS = rows[0].WBS;
            csRow.ProjectDefinition = rows[0].ProjectDefinition;
            csRow.UoM = rows[0].UoM;
            csRow.ActualQty = sumByKey(rows, 'ActualQty');
            csRow.ActualValue = sumByKey(rows, 'ActualValue' + (useEurValues ? 'EUR' : ''));
            csRow.PlannedQty = sumByKey(rows, 'PlannedQty');
            csRow.PlannedValue = sumByKey(rows, 'PlannedValue' + (useEurValues ? 'EUR' : ''));
            csRow.FinancialCommit = sumByKey(rows, 'FinancialCommit' + (useEurValues ? 'EUR' : ''));
            if (useEurValues)
              csRow.Currency = 'EUR';
            else
              csRow.Currency = rows[0].Currency;
            cs.rows.push(csRow);
          }
        }
        costStructure.push(cs);
      }
    }
    costStructure.sort((cs1, cs2) => {
      return cs1.year - cs2.year;
    });
    return costStructure;
  }

  aggregateCostRevenuesProject(callback?: any) {

    this.loadingService.show();
    this.wbsCostrevenueSapService.getCostRevenues({ProjectDefinition: this.project.pk}).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        let data = resp.response.results;
        this.costsProject = data;

        let currencies = arrayDistinct(this.costsProject.map((c) => c.Currency));
        this.projectShouldUseEURMode = currencies.length > 1;

        this.costStructureProject = this.buildCostStructure(data, this.projectShouldUseEURMode);

        this.costStructureCompaniesProject = [];
        let comp = arrayDistinct(data.map((c: WBSCostRevenue) => c.CompanyCode));
        comp.forEach((c) => {
          let s = new CostStructureCompany();
          s.company = c;
          s.costStructure = this.buildCostStructure(data.filter((c_) => c_.CompanyCode == c));
          this.costStructureCompaniesProject.push(s);
        });
        if (callback)
          callback();
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  noteInsertMessage() {
    this.messageService.open({
      title: " ",
      message: "Insert note to proceed",
      autoclose: true
    })
  }

  getProjectStatusFromDeltaRel(delta_rel: number) {
    return this.projectStatusLinks.filter((sl) => sl.delta_rel == delta_rel)[0];
  }

  getProjectTaskStatusFromDeltaRel(delta_rel: number) {
    return this.taskStatusLinks.filter((sl) => sl.delta_rel == delta_rel)[0];
  }

  postComment(comment: string, object_id: number, callback: any, task_id?: number) {
    let c = new SerionetComment();
    c.message = "**Approval request**: " + comment;
    c.object_id = object_id;
    c.content_type = this.projectApprovalProcessCTPK;
    this.commentsService.saveComment(c).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        if (!task_id) {
          this.loadingService.hide();
          if (callback)
            callback.call();
        }
        else {
          c.content_type = this.taskCT;
          c.object_id = task_id;
          this.commentsService.saveComment(c).subscribe((resp) => {
            this.manageRespSave(resp, () => {
              this.loadingService.hide();
              if (callback)
                callback.call();
            })
          })
        }

      }, task_id != null)
    })
  }

  enableTaskAdvance(task: ProjectTask) {
    return !task.readOnly || this.isTaskOnHold(task.pk) || this.isTaskUnderReview(task.pk);
  }

  setReadonlyTask(taskPk: number) {
    let task = this.getTask(taskPk);
    task.readOnly = //Il task è in readonly se
      this.project.readOnly ||  //Il progetto è in readonly
      ((task.parent && task.parent.pk) ? this.getTask(task.parent.pk).readOnly : false) || //Mio padre è in readonly
      task.status.delta_rel == 3 || task.status.delta_rel == 4 || task.status.delta_rel == 5 || //Sono in under review, closed completed o close aborted
      task.next_status != null; // sono in fase di approvazione
    if (task.children) {
      task.children.forEach((pt) => {
        this.setReadonlyTask(pt.pk);
      })
    }
  }

  isTaskOnHold(taskPk: number) {
    let task = this.getTask(taskPk);
    return !task.next_status && task.status.delta_rel == 2;
  }

  isTaskUnderReview(taskPk: number) {
    let task = this.getTask(taskPk);
    return !task.next_status && task.status.delta_rel == 3;
  }

  readOnlyByAudience = false;

  setReadonlyByProject() {
    this.project.readOnly =  //Il progetto è in readonly se
      this.project.status.delta_rel == 2 || this.project.status.delta_rel == 3 || this.project.status.delta_rel == 4 || this.project.status.delta_rel == 5 || //Sono in on hold, under review, closed completed o close aborted
      this.project.next_status != null; // sono in fase di approvazione

    if (!this.project.readOnly && this.amIAudience()) {
      this.project.readOnly = true;
      this.readOnlyByAudience = true;
    }
    else {
      this.readOnlyByAudience = false;
    }
    this.project.tasksAll.forEach((pt) => {
      this.setReadonlyTask(pt.pk);
    })
  }

  isProjectOnHold() {
    return !this.project.next_status && this.project.status.delta_rel == 2;
  }

  isProjectUnderReview() {
    return !this.project.next_status && this.project.status.delta_rel == 3;
  }

  canSaveTask(task: ProjectTask) {
    return task.name && task.assignee && task.assignee.pk && task.department && task.department.pk && task.original_start_date && task.original_end_date &&
    task.pk ? this.areDateAligned(task.forecast_start_date, task.forecast_end_date) : this.areDateAligned(task.original_start_date, task.original_end_date);
  }

  parseFloat(n) {
    return parseFloat(n);
  }

  canEditTaskSite() {
    return Observable.create((observer) => {
      let sitesVisible = this.userSites.map((s) => s.pk);
      if (sitesVisible.indexOf(this.taskToEdit.site.pk) != -1) {
        this.wbsHasRelatedData().subscribe((resp) => {
          observer.next(!resp);
          observer.complete();
        })
      }
      else {
        observer.next(false);
        observer.complete();
      }
    })
  }

  wbsHasRelatedData() {
    return Observable.create((observer) => {
      if (this.wbs && this.wbs.hasAskedData) {
        observer.next(this.costStructureTask.length > 0 || this.wbs.POLink.results.length > 0 || this.wbs.SOLink.results.length > 0 || this.wbs.WOLink.results.length > 0);
        observer.complete();
      }
      else {
        this.reloadCosts(this.levels[this.levels.length - 1], () => {
          observer.next(this.costStructureTask.length > 0 || this.wbs.POLink.results.length > 0 || this.wbs.SOLink.results.length > 0 || this.wbs.WOLink.results.length > 0);
          observer.complete();
        });
      }
    })
  }

  amIAudience() {
    let isAudience = false;
    this.project.members.forEach((m) => {
      if (m.user.pk == this.loginService.user.pk) {
        if (m.type.delta_rel == 2) {
          isAudience = true;
        }
      }
    });
    return isAudience;
  }

  hasFeasibility() {
    return this.project.tasksAll && this.project.tasksAll.filter((pt) => pt.feasibility).length > 0;
  }

  hasFeasibilityCompleted() {
    return this.project.tasksAll && this.project.tasksAll.filter((pt) => pt.feasibility && pt.status.delta_rel == 4 && !pt.parent.pk).length > 0;
  }

  tabChange(tab: MatTabGroup) {
    if (!this.wbs.hasAskedData) {
      if (tab.selectedIndex == 2 || tab.selectedIndex == 3) {
        if (this.costsProject.length == 0) {
          this.reloadCosts(this.levels[this.levels.length - 1]);
        }
        else {
          this.aggregateCostRevenues();
        }
      }

    }
  }

  fixProjectDates() {
    if (!this.projectToEdit.planned_end_date) {
      this.projectToEdit.planned_end_date = getUrlDate(new Date(this.projectToEdit.start_date))
    }
  }

  fixTaskDates() {
    if (!this.taskToEdit.original_end_date) {
      this.taskToEdit.original_end_date = getUrlDate(new Date(this.taskToEdit.original_start_date))
    }
  }

  headerConfWO: HeaderConf[] = [
    {
      label: this.tr('WORKORDER'),
      valueKey: 'work_order_nr'
    },
    {
      label: this.tr('TYPE'),
      valueKey: 'type',
      useStr: true
    },
    {
      label: this.tr('MATERIAL'),
      valueKey: 'material',
      useStr: true
    },
    {
      label: this.tr('CONFIRMED'),
      valueFunction: (item: WorkorderSerionet) => {
        if (item.work_order_nr && item.status.delta_rel > 2) {
          return item.confirmed_quantity + "/" + item.quantity;
        }
        else {
          return "-";
        }
      }
    },
    {
      label: this.tr('START'),
      valueFunction: (item: WorkorderSerionet) => {
        if (item.sap_actual_start && item.sap_actual_start != '00000000') {
          return sapDateToUrlDate(item.sap_actual_start);
        }
        else {
          return item.planned_start;
        }
      }
    },
    {
      label: this.tr('END'),
      valueFunction: (item: WorkorderSerionet) => {
        if (item.sap_actual_start && item.sap_actual_start != '00000000') {
          if (item.sap_actual_end && item.sap_actual_end != '00000000') {
            return sapDateToUrlDate(item.sap_actual_end);
          }
          else {
            return this.calcActualEndDate(item);
          }
        }
        else {
          return item.planned_end;
        }
      }
    },
    {
      label: this.tr('STATUS'),
      valueFunction: (item: WorkorderSerionet) => {
        return '<span class="wo-status" style="background-color:' + item.status.status_id.color + '">' + item.status.__str__ + '</span>';
      }
    }
  ];
  noWorkordersMessage = this.tr('NO_SAMPLINGS_RELATED_TO_THIS_WBS');
  workorderToView: WorkorderSerionet;

  calcActualEndDate(wo: WorkorderSerionet) {
    let d = new Date(sapDateToUrlDate(wo.sap_actual_start));
    d.setDate(d.getDate() + dateDiffInDays(new Date(wo.planned_start), new Date(wo.planned_end)));
    return d;
  }

  selectedWO(wo: WorkorderSerionet) {
    this.loadingService.show();
    this.workordersSerionetService.getWorkorderSerionet(wo.pk).subscribe((resp) => {
      this.loadingService.hide();
      this.workorderToView = resp;
    });
  }

  resetWO() {
    this.workorderToView = null;
  }

  getSamplings(page: number, results: number, table: TableWithPagesComponent) {
    //Nota: la paginazione non esiste in questo servizio, ma si suppone che il filtro per WBS limiti i risultati ad un numero gestibile
    let query = {
      wbs: this.getTask(this.levels[this.levels.length - 1]).pk,
      is_active: true,
      explode_all: true
    };
    return this.workordersSerionetService.getWorkordersSerionet(query);
  }

  extendDataWorkorders(workorders: WorkorderSerionet[]) {
    return Observable.create((observer) => {
      let getEndDate = (wo: WorkorderSerionet) => {
        if (wo.sap_actual_start && wo.sap_actual_start != '00000000') {
          if (wo.sap_actual_end && wo.sap_actual_end != '00000000') {
            return sapDateToUrlDate(wo.sap_actual_end);
          }
          else {
            return getUrlDate(this.calcActualEndDate(wo));
          }
        }
        else {
          return wo.planned_end;
        }
      };
      workorders = workorders.sort((wo1: WorkorderSerionet, wo2: WorkorderSerionet) => {
        return new Date(getEndDate(wo1)).getTime() - new Date(getEndDate(wo2)).getTime();
      });

      observer.next(workorders);
      observer.complete();
    })
  }

  editMembers() {
    this.projectToEditMembers = cloneObject(this.project);
    this.membersEditDialog.open();
  }

  restoreMember(m: ProjectMember) {
    m.custom_status = "toSave";
    m.is_active = true;
  }

  hasInactiveMembers() {
    return this.projectToEditMembers.members.filter((m) => !m.is_active).length > 0
  }

  canEditProject() {
    if (this.project && this.project.members && this.project.members.length > 0) {
      let m = this.project.members.find((m_) => m_.user.pk == this.loginService.user.pk);
      if (m) {
        return m.type.delta_rel == 0;
      }
      else {
        //Se non trova il membro ma ha accesso al progetto, vuol dire che è un developer o un director, abilito
        return true;
      }
    }
    else {
      return false;
    }
  }

  canEditTask(task: ProjectTask) {
    let sitesVisible = this.userSites.map((s) => s.pk);
    return (sitesVisible.indexOf(task.site.pk) != -1)
  }

  getUrlDate(d) {
    return getUrlDate(d);
  }

  editAssignee() {
    this.assigneeEditDialog.open();
  }

  hideUser = false;

  selectNewAssignee(m: ProjectMember) {
    let t = this.getTask(this.levels[this.levels.length - 1]);
    t.assignee = m;
    this.loadingService.show();
    this.hideUser = true;
    this.projectTasksService.saveProjectTask(t).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.assigneeEditDialog.close();
        this.hideUser = false;
        this.loadingService.hide();
      });
    })
  }

  tabChangeProject(projectTab) {
    if (projectTab.selectedIndex == 3) {
      if (this.costsProject.length == 0)
        this.aggregateCostRevenuesProject();
    }
  }
}
