import {Component, EventEmitter, Input, OnInit, Output, SimpleChange} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {ContentTypeService} from "../../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {StatusService} from "../../../../_common/services/status.service";
import {DomSanitizer} from "@angular/platform-browser";
import {ProjectTask} from "../../../../_common/services/project-task.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

export class Phase {
  text_color:string;
  background_color:string;
  statuses:StatusLink[];
}

@Component({
  selector: 'app-project-management-kanban',
  templateUrl: 'kanban.component.html',
  styleUrls: ['kanban.component.css']
})
export class KanbanComponent extends TranslatableComponent implements OnInit {

  @Input('tasks') tasks:ProjectTask[] = [];
  @Input('enableTaskClick') enableTaskClick = true;
  @Input('allowCreate') allowCreate = true;
  @Input('isFullViewMode') isFullViewMode = false;
  taskCT:number;
  taskStatusLinks:StatusLink[] = [];
  phases:Phase[] = [];
  @Output() createTask = new EventEmitter();
  @Output() deleteTask = new EventEmitter();
  @Output() selectTask = new EventEmitter();

  constructor(protected translationService:TranslationService,
              private contentTypeService:ContentTypeService, private statusLinkService:StatusLinkService,
              private statusService:StatusService, private sanitizer:DomSanitizer) {
    super(translationService)
  }

  ngOnInit() {

    this.contentTypeService.getContentTypes({model:'projecttask', is_active:true}).subscribe((resp)=>{
      if(resp && resp['count'] && resp['count']>0){
        this.taskCT = resp['results'][0].pk;
        this.statusLinkService.getStatusLinks({is_active:true, app: resp['results'][0].pk}).subscribe((resp)=>{
          this.taskStatusLinks = resp['results'].sort((a:StatusLink,b:StatusLink) => {
            return a.delta_rel-b.delta_rel;
          });
          let count = this.taskStatusLinks.length;
          let callback2 = ()=> {
            count--;
            if(count==0){
              if(this.tasks)
                this.tasks.sort((t1,t2) => {
                  return t1.status.delta_rel-t2.status.delta_rel;
                });
              this.buildPhases();
            }
          };

          this.taskStatusLinks.forEach((sl)=>{
            this.statusService.getStatus(sl.status_id.pk).subscribe((s)=>{
              sl.status_id = s;
              callback2();
            })
          })
        })
      }
    });
  }

  buildPhases(){
    /**
     * 1) Requested (delta_rel=0)
     * 2) Released (delta_rel=1)
     * 3) On Hold (delta_rel=2) <- va sotto Released
     * 4) Under Review (delta_rel=3)
     * 5) Closed (completed) (delta_rel=4)
     * 6) Closed (aborted) (delta_rel=5) <- va sotto Closed Completed
     */

    this.taskStatusLinks.forEach((sl)=>{
      if(sl.delta_rel==0 || sl.delta_rel==1 || sl.delta_rel==3 || sl.delta_rel==4 ){
        let p = new Phase();
        p.background_color = sl.status_id.color;
        p.text_color = sl.status_id.text_color;
        p.statuses = [sl];
        this.phases.push(p);
      }
      else if(sl.delta_rel==2){
        this.phases[1].statuses.push(sl);
      }
      else if(sl.delta_rel==5){
        this.phases[3].statuses.push(sl);
      }
    });
  }

  taskInPhase(t:ProjectTask,p:Phase){
    let statusesPK = p.statuses.map((sl)=>sl.pk);
    return statusesPK.indexOf(t.status.pk)!=-1;
  }

  getStyle(p:Phase){
    return this.sanitizer.bypassSecurityTrustStyle('background-color: '+p.background_color+'; color: '+p.text_color)
  }

  doCreateTask(){
    this.createTask.emit();
  }

  taskDeleted(t:ProjectTask){
    this.deleteTask.emit(t);
  }

  taskSelected(t:ProjectTask){
    if(this.enableTaskClick)
      this.selectTask.emit(t);
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if(changes['tasks'])
      this.tasks = changes['tasks'].currentValue;
    if(changes['tasks'])
      this.tasks = changes['tasks'].currentValue;
  }
}
