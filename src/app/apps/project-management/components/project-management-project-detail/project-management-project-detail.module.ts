import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {ProjectManagementProjectDetailComponent} from "./project-management-project-detail.component";
import {TaskComponent} from "./task/task.component";
import {KanbanComponent} from "./kanban/kanban.component";
import {CostStructureComponent} from "./cost-structure/cost-structure.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [ProjectManagementProjectDetailComponent, TaskComponent,KanbanComponent, CostStructureComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class ProjectManagementProjectDetailModule { }


