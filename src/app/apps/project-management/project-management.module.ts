import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {ProjectManagementRoutes} from "./project-management.routes";
import {ProjectManagementIndexModule} from "./components/project-management-index/project-management-index.module";
import {ProjectManagementProjectListModule} from "./components/project-management-project-list/project-management-project-list.module";
import {ProjectManagementProjectDetailModule} from "./components/project-management-project-detail/project-management-project-detail.module";
import {ProjectService} from "./services/project.service";
import {ProjectMembersService} from "./services/project-member.service";
import {AvailableStatusesService} from "./services/available-statuses.service";
import {ProjectTypesService} from "./services/project-types.service";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(ProjectManagementRoutes), ProjectManagementIndexModule, ProjectManagementProjectListModule, ProjectManagementProjectDetailModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [ProjectService,ProjectMembersService,AvailableStatusesService,ProjectTypesService]                    // services
})
export class ProjectManagementModule { }

