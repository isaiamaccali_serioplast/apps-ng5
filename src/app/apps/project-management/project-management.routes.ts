import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {ProjectManagementIndexComponent} from "./components/project-management-index/project-management-index.component";
import {ProjectManagementProjectListComponent} from "./components/project-management-project-list/project-management-project-list.component";
import {ProjectManagementProjectDetailComponent} from "./components/project-management-project-detail/project-management-project-detail.component";

export const ProjectManagementRoutesRoot: Routes = [
  {
    path: 'project-management',
    loadChildren: './project-management.module.ts#ProjectManagementModule',
    canActivate: [AuthGuard]
  }
];

// Route Configuration
export const ProjectManagementRoutes: Routes = [
  {
    path: '',
    component: ProjectManagementIndexComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch:'list',
        component: ProjectManagementProjectListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'detail/:p',
        component: ProjectManagementProjectDetailComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];
