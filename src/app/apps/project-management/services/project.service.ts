import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {ProjectMember} from "./project-member.service";
import {ProjectTask} from "../../_common/services/project-task.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";
import {TypeLink} from "../../_common/services/type-link.service";
import {StatusLink} from "../../_common/services/status-link.service";
import {Site} from "../../_common/services/sites.service";
import {ProjectType} from "./project-types.service";
import {cloneObject} from "../../../shared/utils";

export class Project extends GenericObj{
  name?:string;
  leader?:ProjectMember;
  type?:TypeLink;
  status?:StatusLink;
  description?:string;
  site?:Site;
  start_date:string;
  planned_end_date:string;
  next_status?:number;
  process_id?:number;
  project_types:ProjectType[];

  members?:ProjectMember[];
  tasks?:ProjectTask[];
  tasksAll?:ProjectTask[];

  readOnly = false;
}

@Injectable()
export class ProjectService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Project";
    this.serviceURL = "projects/projects/";
  }

  getProjects(query: any, format?:any, timeout?:any) {
    return this.getList(query, format, timeout);
  }

  saveProject(i:Project, params?:any){
    return this.saveItem(i, null, params, 90000);
  }

  deleteProject(i:Project){
    return this.deleteItem(i);
  }

  restoreProject(i:Project){
    return this.restoreItem(i);
  }

  getProject(id: number, params?:any) {
    return this.getItem(id, false, null, params);
  }

  makeStatusChangeRequest(pr:Project,statusDestinationDeltaRel:number){
    return this.saveItem(pr,null, {run_approval_flow:statusDestinationDeltaRel});
  }

  clearItem(p:Project){
    let p_:Project = cloneObject(p);
    p_.project_types =  p_.project_types.filter((pt)=>pt.is_active);
    return p_
  }
}

