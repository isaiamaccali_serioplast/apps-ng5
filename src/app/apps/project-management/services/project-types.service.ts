import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";

export class ProjectType extends GenericObj{
  name:string;
}

@Injectable()
export class ProjectTypesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Project Types";
    this.serviceURL = "projects/project-types/";
  }

  getProjectTypes(query: any) {
    return this.getList(query);
  }
}

