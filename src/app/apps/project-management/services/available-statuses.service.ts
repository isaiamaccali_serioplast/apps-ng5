import {Injectable} from "@angular/core";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";

@Injectable()
export class AvailableStatusesService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {}

  getAvailableProjectStatuses(projectPK: number) {
    return Observable.create((observer) => {

      let url = "projects/projects_next_status/" + projectPK;

      let opts = new SerionetRequestOptions();
      opts.options = {};

      this.serionetService.doGet(url, opts).subscribe((data) => {
        observer.next(data);
        observer.complete();
      },(response) => {
        this.loggingService.error("Error in retreving project available statuses, status : " + response.status);
        observer.next(response);
        observer.complete();
      });
    })
  }

  getAvailableProjectTaskStatuses(projectTaskPK: number) {
    return Observable.create((observer) => {

      let url = "projects/projects_task_next_status/" + projectTaskPK;

      let opts = new SerionetRequestOptions();
      opts.options = {};

      this.serionetService.doGet(url, opts).subscribe((data) => {
        observer.next(data);
        observer.complete();
      },(response) => {
        this.loggingService.error("Error in retreving project task available statuses, status : " + response.status);
        observer.next(response);
        observer.complete();
      });
    })
  }

}


