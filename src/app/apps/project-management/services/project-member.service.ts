import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";
import {Project} from "./project.service";
import {HRUser} from "../../_common/services/hr-users.service";
import {Department} from "../../_common/services/department.service";
import {TypeLink} from "../../_common/services/type-link.service";

export class ProjectMember extends GenericObj{
  user?:HRUser;
  type?:TypeLink;
  project?:Project;

  departments?:Department[];
  custom_status?:string;
}


@Injectable()
export class ProjectMembersService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Project Member";
    this.serviceURL = "projects/project-members/";
  }

  getProjectMembers(query: any) {
    return this.getList(query);
  }

  saveProjectMember(i:ProjectMember){
    return this.saveItem(i);
  }

  deleteProjectMember(i:ProjectMember){
    return this.deleteItem(i);
  }

  restoreProjectMember(i:ProjectMember){
   return this.restoreItem(i);

  }

  getProjectMember(id: number) {
    return this.getItem(id);
  }

}

