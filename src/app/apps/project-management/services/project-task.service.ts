import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {ProjectMember} from "./project-member.service";
import {Project} from "./project.service";
import {GenericObj, GenericService} from "../../_common/services/_generic.service";
import {Department} from "../../_common/services/department.service";
import {StatusLink} from "../../_common/services/status-link.service";
import {Site} from "../../_common/services/sites.service";
import {Observable} from "rxjs";

export class ProjectTask extends GenericObj {
  name?: string;
  sap_name?:string;
  description?: string;
  assignee?: ProjectMember;
  forecast_start_date:string;
  forecast_end_date?: string;
  original_start_date?:string;
  original_end_date?: string;
  actual_start_date?:string;
  actual_end_date?: string;
  status?: StatusLink;
  parent?: ProjectTask;
  project?: Project;
  department?: Department;
  text_color: string;
  site?:Site;
  next_status?:number; //delta rel dello stato richiesto
  process_id?:number;
  children?: ProjectTask[];
  feasibility:boolean;

  readOnly = false;

}

export class WBSPurchaseOrder{
  CompletelyDelivered:boolean;
  PurchaseOrderSupplierName:string;
  PurchaseOrderSupplier:string;
  PurchaseOrderDelQty:number;
  PurchaseOrderQty:number;
  PurchaseOrderDate:string;
  PurchaseOrder:string;
  WBS:string;
  Material:string;
  PurchaseOrderItem:string;
  ProjectDefinition:string;
  Description:string;
  Uom:string;
}

export class WBSWorkorder{
  WBS:string;
  WorkOrderPackIns:string;
  WorkOrderConfQty:number;
  WorkOrderTotQty:number;
  WorkOrderStatus:string;
  WorkOrderDate:string;
  Plant:string;
  Material:string;
  Workorder:string;
  ProjectDefinition:string;
  Description:string;
  Uom:string;
}

export class WBSSalesOrder{
  SalesOrderItem:string;
  SalesOrder:string;
  SalesOrderStatus:string;
  SalesOrderCustomerName:string;
  SalesOrderCustomer:string;
  SalesOrderDelQty:number;
  SalesOrderQty:number;
  SalesOrderDate:string;
  Plant:string;
  Material:string;
  WBS:string;
  ProjectDefinition:string;
  Description:string;
  Uom:string;
}

export class WBSCostRevenue {
  WBS:string;
  ProjectDefinition:string;
  Year:string;
  Month:string;
  CostingRevenueVoice:string;
  CostingRevenueDescription:string;
  Currency:string;
  UoM:string;
  ActualValue:number;
  ActualQty:number;
  PlannedValue:number;
  PlannedQty:number;
}

export class WBSElement {
  Status: string;
  WBSFather:string;
  ProjectDefinition: string;
  WBS: string;
  ActualStartDate: string;
  ActualEndDate: string;
  Description: string;
  Plant: string;
  ForecastStartDate: string;
  ForecastEndDate: string;
  OriginalStartDate:string;
  OriginalEndDate:string;
  POLink?:{
    results:WBSPurchaseOrder[]
  };
  WOLink?:{
    results:WBSWorkorder[]
  };
  SOLink?:{
    results:WBSSalesOrder[]
  };
  CostRevLink?:{
    results:WBSCostRevenue[]
  };

  getId(){
    return "ProjectDefinition='"+this.ProjectDefinition+"',WBS='"+this.WBS+"'";
  }
}

@Injectable()
export class ProjectTasksService extends GenericService {

  serviceName_SAP = "ZPROJECT_MANAGEMENT_SRV";
  entitySetName_SAP = "WBSElementSet";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Project Task";
    this.serviceURL = "projects/project-tasks/";
  }

  getProjectTasks(query: any) {
    return this.getList(query);
  }

  saveProjectTask(i:ProjectMember, params?:any){
    return this.saveItem(i, null, params);
  }

  deleteProjectTask(i:ProjectTask){
    return this.deleteItem(i);
  }

  restoreProjectTask(i:ProjectTask){
    return this.restoreItem(i);
  }

  getProjectTask(id: number, params?:any) {
    return this.getItem(id, false,null,params);
  }

  getWBS(pt:ProjectTask, params:any){
    let wbs = new WBSElement();
    wbs.WBS = pt.sap_name;
    wbs.ProjectDefinition = pt.project.pk+"";
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null, wbs.getId(), params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      })
    })
  }

  makeStatusChangeRequest(pt:ProjectTask,statusDestinationDeltaRel:number){
    return this.saveItem(pt,null, {run_approval_flow:statusDestinationDeltaRel});
  }
}

