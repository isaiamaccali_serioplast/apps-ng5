/**
 * Service used to share available apps
 */
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetRequestOptions, SerionetService} from "../shared/serionet/service/serionet.service";
import {POSTIT_APP_ID} from "./post-it/post-it.config";
import {MATERIALS_APP_ID} from "./materials/materials.config";
import {LoggingService} from "../shared/logging/logging.service";
import {RDDASHBOARD_APP_ID} from "./rd-dashboard/rd-dashboard.config";
import {ASSETS_WORKCENTERS_APP_ID} from "./assets-workcenters/assets-workcenters.config";
import {HR_DASHBOARD_APP_ID} from "./hr-dashboard/hr-dashboard.config";
import {HR_MANAGEMENT_APP_ID} from "./hr-management/hr-management.config";
import {ORGANIZATION_APP_ID} from "./organizations/organizations.config";
import {SITES_APP_ID} from "./sites/sites.config";
import {MES_APP_ID} from "./mes/mes.config";
import {SERIOLAB_APP_ID} from "./seriolab/seriolab.config";
import {HR_RECRUITMENT_APP_ID} from "./hr-recruitment/hr-recruitment.config";
import {QUALITYSTOCK_APP_ID} from "./quality-stock/quality-stock.config";
import {DECK_APP_ID} from "./deck/deck.config";
import {SALES_DASHBOARD_APP_ID} from "./sales-dashboard/sales-dashboard.config";
import {SPC_APP_ID} from "./spc/spc.config";
import {PROJECTMANAGEMENT_APP_ID} from "./project-management/project-management.config";
import {HR_TRAININGS_APP_ID} from "./hr-trainings/hr-trainings.config";
import {GUEST_APP_ID} from "./guest/guest.config";
import {PRICE_LIST_APP_ID} from "./procurement-pricelist/procurement-pricelist.config";
import {SALES_PRICELIST_APP_ID} from "./sales-pricelist/sales-pricelist.config";

import {GenericObj} from "./_common/services/_generic.service";
import {RFID_TRAYS_APP_ID} from "./rfid-trays/rfid-trays.config";
import {ROUTINGS_APP_ID} from "./routings/routings.config";
import {HttpParams} from "@angular/common/http";
import {PRODUCTION_PLANNING_APP_ID} from "./production-planning/production-planning.config";
import {LoadingService} from "../shared/loading/loading.service";

let AVAILABLE_APPS = {};
AVAILABLE_APPS[POSTIT_APP_ID] = true;
AVAILABLE_APPS[MATERIALS_APP_ID] = true;
AVAILABLE_APPS[RDDASHBOARD_APP_ID] = true;
AVAILABLE_APPS[ASSETS_WORKCENTERS_APP_ID] = true;
AVAILABLE_APPS[HR_DASHBOARD_APP_ID] = true;
AVAILABLE_APPS[HR_MANAGEMENT_APP_ID] = true;
AVAILABLE_APPS[ORGANIZATION_APP_ID] = true;
AVAILABLE_APPS[SITES_APP_ID] = true;
AVAILABLE_APPS[SERIOLAB_APP_ID] = true;
AVAILABLE_APPS[MES_APP_ID] = true;
AVAILABLE_APPS[HR_RECRUITMENT_APP_ID] = true;
AVAILABLE_APPS[QUALITYSTOCK_APP_ID] = true;
AVAILABLE_APPS[DECK_APP_ID] = true;
AVAILABLE_APPS[SALES_DASHBOARD_APP_ID] = true;
AVAILABLE_APPS[SPC_APP_ID] = true;
AVAILABLE_APPS[PROJECTMANAGEMENT_APP_ID] = true;
AVAILABLE_APPS[HR_TRAININGS_APP_ID] = true;
AVAILABLE_APPS[GUEST_APP_ID] = true;
AVAILABLE_APPS[PRICE_LIST_APP_ID] = true;
AVAILABLE_APPS[SALES_PRICELIST_APP_ID] = true;
AVAILABLE_APPS[RFID_TRAYS_APP_ID] = true;
AVAILABLE_APPS[ROUTINGS_APP_ID] = true;
AVAILABLE_APPS[PRODUCTION_PLANNING_APP_ID] = true;

let APPS_IN_DEVELOPMENT =[];

export class App extends GenericObj{
  title: string;
  app_root: string;
  icon: string;
  icon_type:string;
  icon_name:string;
}


@Injectable()
export class AppsService {

  authorizedApps = {};
  apps = [];
  constructor(private serionetService: SerionetService, private loggingService:LoggingService, private loadingService:LoadingService) {
  }

  getAvailableApps() {
    return Observable.create((observer) => {
      let urlParams = new HttpParams();
      urlParams.set("is_active", "true");

      let options = {
        search: urlParams
      };

      let opts = new SerionetRequestOptions();
      opts.options = options;

      this.loadingService.show();
      this.serionetService.doGet("core/apps", opts).subscribe((data) => {
        this.loadingService.hide();
        if(data && data['count'] && data['count']>0){
          data['results_full'] = data['results'];
          data['results'] = data['results'].filter((app) => {
            return AVAILABLE_APPS[app.app_root];
          });
          this.apps = data['results'].sort((a:App,b:App)=>{
            let textA = a.title;
            let textB = b.title;
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
        }
        observer.next(data);
        observer.complete();
      }, (response)=> {
        this.loadingService.hide();
        this.loggingService.error("Error in retreving apps, status: " + response.status);
        observer.next({status: response.status});
        observer.complete();
      });
    });
  }

  ready() {
    return Observable.create((observer) => {

      if (this.apps.length > 0) {
        observer.next(true);
        observer.complete();
      }
      else {
        this.getAvailableApps().subscribe(() => {
            observer.next(true);
            observer.complete();
        });
      }
    });
  }

  handleError(){

  }

  isAppAvailableForUser(appName: string) {
    return Observable.create((observer) => {

      if (this.authorizedApps[appName] == true || APPS_IN_DEVELOPMENT[appName]) {
        observer.next(true);
        observer.complete();
      }
      else {
        let urlParams = new HttpParams();
        urlParams = urlParams.set('app_name', appName);

        let options = {
          params: urlParams
        };

        let opts = new SerionetRequestOptions();
        opts.options = options;
        this.loadingService.show();
        this.serionetService.doGet("core/apps_check", opts).subscribe((response) => {
          this.loadingService.hide();
          this.authorizedApps[appName] = (response == true);
          observer.next(response == true);
          observer.complete();
        }, (response)=> {
          this.loadingService.hide();
          observer.next(response);
          observer.complete();
        });
      }
    });
  }

  getAppData(appName:string){
    let appData = {};
    this.apps.forEach((app) =>{
      if(app.app_root==appName){
        appData = app;
      }
    });
    return appData;
  }
}


