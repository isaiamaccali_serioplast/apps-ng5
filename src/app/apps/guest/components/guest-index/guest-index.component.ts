import {Component, OnInit, ViewChild} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {cloneObject, fixValue, getUrlDate, parseSerionetRespToData} from "../../../../shared/utils";
import {Badge, BadgeService} from "../../../_common/services/badges.service";
import {DjangoLanguage, DjangoLanguagesService} from "../../../_common/services/django-languages.service";
import {HRUser, HRUserService} from "../../../_common/services/hr-users.service";
import {PersonalInfo, PersonalInfoService} from "../../../_common/services/personalinfo.service";
import {BadgeUserRef, BadgeUserRefService} from "../../../_common/services/badge-user-refs.service";
import {StepperComponent} from "../../../../shared/material/stepper/stepper.component";
import {Site} from "../../../_common/services/sites.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-guest-index',
  templateUrl: 'guest-index.component.html',
  styleUrls: ['guest-index.component.css']
})
export class GuestIndexComponent extends BaseComponent implements OnInit {

  @ViewChild('stepper') stepper: StepperComponent;
  badges: Badge[] = [];
  // appWtt:WorkToolType;
  djangolanguages: DjangoLanguage[] = [];

  user: HRUser = new HRUser();
  personalInfo: PersonalInfo = new PersonalInfo();

  badgeUserRef: BadgeUserRef = new BadgeUserRef();
  // userWorkTools:UserWorktool[] = [];

  similarities: PersonalInfo[] = [];
  similaritiesActive = false;

  constructor(private sidenavService: SideNavService, private titleService: Title,
              protected messageService: MessageService, protected translationService: TranslationService,
              protected loadingService: LoadingService, private badgeService: BadgeService,
              // private contentTypeService:ContentTypeService, private worktoolTypeService:WorkToolTypeService,
              private languagesService: DjangoLanguagesService, private hrUserService: HRUserService,
              private personalInfoService: PersonalInfoService, private badgeUserRefService: BadgeUserRefService//,
              // private userWorktoolService:UserWorktoolService
  ) {
    super(translationService, messageService, loadingService);
    this.sidenavService.leftMenuEnabled = false;
    this.sidenavService.leftMenu = false;
    this.sidenavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Guest");
  }

  initData() {
    this.step2Previous();
    this.step3Previous();
  }

  ngOnInit() {

    // this.contentTypeService.getContentTypes({model:'externalapp'}).subscribe((resp)=>{
    //   if(resp['count'] && resp['count']>0){
    //     let ct:ContentType = resp['results'][0];
    //     this.worktoolTypeService.getWorkToolTypes({model_reference:ct.pk}).subscribe((resp)=>{
    //       if(resp['count'] && resp['count']>0){
    //         this.appWtt = resp['results'][0];
    //       }
    //     })
    //   }
    // });

    this.languagesService.getDjangoLanguages({}).subscribe((data) => {
      for (let i = 0; i < data.length; i++) {
        let djangoLanguage = new DjangoLanguage();
        djangoLanguage.value = data[i][0];
        djangoLanguage.label = data[i][1];
        this.djangolanguages[i] = djangoLanguage;
      }
      // Remove empty label
      this.djangolanguages.shift();
    });

    this.initData();
  }

  step1ProceedCondition() {
    return this.personalInfo.first_name && this.personalInfo.last_name;
  }

  step1ProceedConfirm() {
    if (this.user.pk) {
      //User has already be set, information will be loaded from previous data
      this.loadingService.show();
      this.badgeUserRefService.getBadgeUserRefs({user: this.user.pk}).subscribe((resp) => {
        this.loadingService.hide();
        let refs: BadgeUserRef[] = parseSerionetRespToData(resp);
        if (refs.length > 0) {
          refs.sort((a, b) => {
            return b.pk - a.pk
          });
          let lastRef = refs[0];
          this.badgeUserRef.language = lastRef.language;
          this.badgeUserRef.external_doc_id = lastRef.external_doc_id;
        }
      })
    }
  }

  siteChange(s: Site) {
    if (s && s.pk) {
      let query = {
        location: s.pk,
        is_active: true,
        is_free: 2,
        guest: true
      };
      this.loadingService.show();
      this.badgeService.getBadges(query).subscribe((data) => {
        this.loadingService.hide();
        this.badges = parseSerionetRespToData(data);
      });
    }
    else {
      this.badges = [];
    }
  }

  step2ProceedCondition() {
    return this.badgeUserRef.badge.pk && this.badgeUserRef.start_date && this.badgeUserRef.end_date && this.badgeUserRef.language;
  }

  step2ProceedConfirm() {
    //this.userWorkTools = [];
  }

  step2Previous() {
    this.personalInfo = new PersonalInfo();
    this.user = new HRUser();
    this.user.is_guest = true;
    this.personalInfo.user_id = this.user;
  }

  // step3ProceedCondition(){
  //   return true;
  // }
  //
  // step3ProceedConfirm(){
  //   return true;
  // }

  step3Previous() {
    this.badgeUserRef.user = this.user;
    this.badgeUserRef.badge = new Badge();
    this.badgeUserRef.start_date = getUrlDate(new Date());
    this.badgeUserRef.end_date = getUrlDate(new Date());
    this.badgeUserRef.notes = "";
  }

  saveData() {
    if (!this.personalInfo.user_id.username)
      this.personalInfo.user_id.username = "xxxxxxx";
    this.hrUserService.saveHrUser(this.personalInfo.user_id).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.user = resp;
        this.personalInfo.user_id = this.user;
        this.personalInfo.pk = this.user.pk;
        this.personalInfoService.updatePersonalInfo(this.personalInfo).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.badgeUserRef.user = this.user;
            this.badgeUserRefService.saveBadgeUserRef(this.badgeUserRef).subscribe((resp) => {
              this.manageRespSave(resp, () => {
                this.messageService.open({
                  message: this.tr('SAVE_COMPLETED'),
                  title: " ",
                  autoClose: false,
                  onClose: () => {
                    this.printInfo();
                  }
                });
              }, true)
            });
          }, true)
        })
      }, true)
    })
  }

  printInfo() {
    this.messageService.open({
      message: this.buildInfo(),
      title: " ",
      autoClose: false,
      onClose: () => {
        this.reset();
      }
    });
  }

  buildInfo() {
    let content = [];
    content.push("Username: " + this.personalInfo.user_id.username);
    return content.join("");
  }

  fixBadge() {
    fixValue(this.badges, this.badgeUserRef, 'badge');
  }

  searchSimilarities() {
    this.loadingService.show();
    this.personalInfoService.getPersonalInfos({levenshtein: this.personalInfo.first_name + " " + this.personalInfo.last_name}).subscribe((resp) => {
      this.loadingService.hide();
      this.handleSimilarities(resp);
    })
  }

  handleSimilarities(resp) {
    this.similaritiesActive = true;
    let similarities: PersonalInfo[] = parseSerionetRespToData(resp);

    this.similarities = similarities.filter((pi) => pi.user_id.is_guest);

  }

  selectResult(pi: PersonalInfo) {
    this.personalInfo = cloneObject(pi);
    this.user = cloneObject(this.personalInfo.user_id);
    this.similaritiesActive = false;
    this.similarities = [];
    this.stepper.setIndex(1);
    this.step1ProceedConfirm();
  }

  // removeUserWorktool(uwt:UserWorktool){
  //   this.userWorkTools.splice(this.userWorkTools.indexOf(uwt),1);
  // }
  //
  // addWorktooltoUser(wtt:WorkToolType,wt:GenericObj){
  //   let ctPk = wtt.model_reference.pk;
  //   let objectId = wt.pk;
  //   let match = this.userWorkTools.filter((uwt)=>{
  //     return uwt.content_type==ctPk && uwt.object_id==objectId;
  //   });
  //   if(match && match.length>0){
  //     this.messageService.open({
  //       title: this.tr('ERROR'),
  //       error: true,
  //       message: "Item already set",
  //       autoClose: false
  //     });
  //   }
  //   else {
  //     let uwt = new UserWorktool();
  //     uwt.object_id = objectId;
  //     uwt.content_type = ctPk;
  //     uwt.type = cloneObject(wtt);
  //     uwt.__str__ = wt.__str__;
  //     this.userWorkTools.push(uwt);
  //   }
  // }

  backLastStep() {
    this.stepper.setIndex(1);
  }

  reset() {
    this.stepper.reset();
    setTimeout(() => {
      this.initData();
      this.similarities = [];
      this.similaritiesActive = false;
      this.stepper.reCheck();
    }, 500)
  }
}
