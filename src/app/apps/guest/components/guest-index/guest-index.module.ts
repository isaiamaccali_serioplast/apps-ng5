import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {GuestIndexComponent} from "./guest-index.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [GuestIndexComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class GuestIndexModule { }


