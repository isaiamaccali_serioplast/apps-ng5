import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {GuestIndexModule} from "./components/guest-index/guest-index.module";
import {GuestRoutes, GuestRoutesRoot} from "./guest.routes";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(GuestRoutes), GuestIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class GuestModule { }

