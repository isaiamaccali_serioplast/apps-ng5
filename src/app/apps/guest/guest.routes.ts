import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {GuestIndexComponent} from "./components/guest-index/guest-index.component";

export const GuestRoutesRoot: Routes = [
  {
    path: 'guest',
    loadChildren: './guest.module.ts#GuestModule',
    canActivate: [AuthGuard]
  }
];

// Route Configuration
export const GuestRoutes: Routes = [
  {
    path: '',
    component: GuestIndexComponent//,
    // canActivate: [AuthGuard]
  }
];
