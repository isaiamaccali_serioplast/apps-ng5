import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Material} from "../../_common/services/materials.service";
import {HasCompanyPosition} from "../../_common/services/has-company-position.service";

export const DEVELOPERS = [1, 10,7,8]; //list of CompanyPositionName pk: [Serioplast Software Developer, Software Development Manager, ERP Manager, ERP Support]
export const SERIOPLAST_RD_LAB = [50,51]; // list of CompanyPositionName pk: [Serioplast RD Lab Manager]
export const SERIOPLAST_RD_TECH_OFF = [46, 47]; // list of CompanyPositionName pk [Serioplast RD Technical Manager,Serioplast RD Designer]
export const SERIOMOULD_RD = [80, 83]; // list of CompanyPositionName pk {Seriomould General Manager, Seriomould CAM Engineer]

@Injectable()
export class MaterialsAuthorizationsService {

  private materialEditAnnouncedSource = new Subject<Material>();
  materialEditAnnounced$ = this.materialEditAnnouncedSource.asObservable();

  constructor() {
  }

/*  isAuthorized(hasCompanyPositions: HasCompanyPosition[], allowedCompanyPositionNames: number[]) {

    for (let i = 0; i < hasCompanyPositions.length; i++) {
      if (DEVELOPERS.indexOf(hasCompanyPositions[i].get_company_position_obj.name.pk) !== -1 || allowedCompanyPositionNames.indexOf(hasCompanyPositions[i].get_company_position_obj.name.pk) !== -1) {
        return true;
      }
    }
    return false
  }*/

  isAuthorized(hasCompanyPositions: HasCompanyPosition[], allowedCompanyPositionNames: number[]) {

    let userCompanyPositionNames = hasCompanyPositions.map((item: HasCompanyPosition)=> {
      return item.get_company_position_obj.name.pk;
    });

    return (DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 || userCompanyPositionNames.filter((item) => allowedCompanyPositionNames.indexOf(item) !== -1).length > 0) ? true : false;

  }

  // Service message commands
  announceMaterialEditInitComplete(material: Material) {
    this.materialEditAnnouncedSource.next(material);
  }

}
