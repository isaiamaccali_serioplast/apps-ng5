import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {MaterialsIndexComponent} from "./materials-index.component";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {ShapeListModule} from "../shapes/shapes-list/shape-list.module";
import {MaterialsListModule} from "../materials/materials-list/materials-list.module";
import {ShapeViewModule} from "../shapes/shape-view/shape-view.module";
import {ShapeEditModule} from "../shapes/shape-edit/shape-edit.module";
import {MaterialsViewModule} from "../materials/material-view/material-view.module";
import {MaterialEditModule} from "../materials/material-edit/material-edit.module";

@NgModule({
  imports: [SharedModule,ShapeListModule, ShapeViewModule, ShapeEditModule, MaterialsListModule, MaterialsViewModule, MaterialEditModule],       // module dependencies
  declarations: [MaterialsIndexComponent],   // components and directives
  exports:[MaterialsIndexComponent],
  providers: [AppPlantUsersService]                    // services
})
export class MaterialsIndexModule { }


