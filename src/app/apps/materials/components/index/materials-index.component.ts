import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {DictionariesService} from "../../../../shared/i18n/dictionaries.service";
import {IdleService} from "../../../../shared/idle/idle.service";
import {ConfigurationService} from "../../../../shared/serionet/service/configuration.service";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {MatSnackBar} from "@angular/material";
import {MaterialsListQueryService} from "../../services/materials-list-query-service";
import {Site} from "../../../_common/services/sites.service";

@Component({
  selector: 'app-materials-index',
  templateUrl: 'materials-index.component.html',
  styleUrls: ['materials-index.component.css']
})
export class MaterialsIndexComponent implements OnInit {

  // ready = false;
  siteId: number;
  site: Site;
  sites: Site[];
  constructor(private sideNavService: SideNavService,
              private loginService: LoginService,
              private router: Router,
              private titleService: Title,
              private dictService:DictionariesService,
              private idleService:IdleService,
              private appPlantUsersService:AppPlantUsersService,
              private confService:ConfigurationService,
              private snackBar: MatSnackBar,
              private materialsListQueryService:MaterialsListQueryService
  ) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Materials");
    // this.dictService.getDictionary("materials").subscribe((dict)=>{
    //   this.ready = true;
    // });

    let query = {
      app__app_root: 'materials',
      is_active:true
    };

    this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
      if (response['results'] && response['results'].length == 1) {
        this.confService.set("SAP_USER",response['results'][0].user,true);
        this.confService.set("SAP_PASSWORD",response['results'][0].password,true);
        this.router.navigate(["materials","materials"]);
      }else{
        this.router.navigate(["/index"]);
        this.snackBar.open("NO_SAP_LICENCE_FOR_APP_MATERIALS", null, {duration: 5000, panelClass: ['error']});
      }
    });
  }

  ngOnInit() {
    this.materialsListQueryService.saveQuery({})
    this.idleService.setTimeoutSeconds(300);
  }

  logout() {
    this.loginService.doLogout().first().subscribe((logoutResponse) => {
      this.router.navigate(["/login"]);
    });
  }

  reload() {
    this.router.navigateByUrl(this.router.url + "");
  }

}
