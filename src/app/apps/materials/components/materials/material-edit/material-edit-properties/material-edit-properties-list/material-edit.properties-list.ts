import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {PropertyStructure, PropertyStructureMaterial} from "../../../../../models/property-structure";
import {MessageService} from "../../../../../../../shared/material/message/message.service";
import {PropertySet, PropertySetService} from "../../../../../../_common/services/property-set.service";
import {TranslationService} from "../../../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../../../shared/loading/loading.service";
import {arrayDistinct, buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../../../../../../shared/utils";
import {PropertiesService, Property} from "../../../../../../_common/services/properties.service";
import {Material} from "../../../../../../_common/services/materials.service";
import {ContentType} from "../../../../../../_common/services/content-type.service";
import {PropertyType} from "../../../../../../_common/services/property-type.service";
import {MeasureUnit} from "../../../../../../_common/services/measure-unit.service";
import {Formula} from "../../../../../../_common/services/formulas.service";
import {TranslatableComponent} from "../../../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-material-edit-properties-list',
  templateUrl: 'material-edit.properties-list.html',
  styleUrls: ['material-edit.properties-list.css']
})
export class MaterialEditPropertiesListComponent extends TranslatableComponent implements OnInit {

  @Input() material: Material;
  @Output() editProperty = new EventEmitter();

  propertySets: PropertySet[] = [];
  propertyStructures: PropertyStructure[] = [];
  siblings: Material[] = [];

  constructor(private messageService: MessageService, private propertySetService: PropertySetService,
              protected translationService: TranslationService, private loadingService: LoadingService,
              private propertyService: PropertiesService) {
    super(translationService);
  }

  initPropertyToAdd() {
    let property = new Property();
    property.type = new PropertyType();
    property.attribute = [];
    property.content_type = new ContentType();
    property.uom = new MeasureUnit();
    property.formula = null;
    property.parameter_list = [];
    return property;
  }

  ngOnInit() {
    if (this.material && this.material.sub_model && this.material.sub_model.shape && this.material.sub_model.shape.pk) {
      this.siblings = this.material.sub_model.shape.material_list || [];
      if (this.siblings.length == 0) {
        this.messageService.open({
          title: " ",
          message: this.tr("ERROR_IN_RETREIVING_SIBLINGS"),
          autoClose: true
        });
      }
    }
    else {
      this.siblings = [this.material];
    }
    if (this.siblings.length > 0) {
      this.loadingService.show();
      this.loadPropertySets();
    }

  }

  loadPropertySets() {
    let siblingsId = this.siblings.map((s) => {
      return s.pk
    });
    this.propertySetService.getPropertySets({material__pk: siblingsId.join("|"), results: 500, is_active: true}).subscribe((data) => {
      this.propertySets = parseSerionetRespToData(data);
      this.createPropertyStructures();
    });
  }

  createPropertyStructures() {
    this.propertyStructures = [];
    if (this.propertySets.length > 0) {
      let materials = this.siblings;

      let properties_id = arrayDistinct(this.propertySets.map((ps) => ps.property.pk));

      this.propertyService.getProperties({pk: properties_id.join("|"), results: 500, is_active: true}).subscribe((data) => {

        let properties: Property[] = parseSerionetRespToData(data);

        for (let p of properties) {
          //For each property a new property structure is built
          let pstruct = new PropertyStructure();
          pstruct.property = p;
          pstruct.materials = [];
          for (let m of materials) {
            let pstructMaterial = new PropertyStructureMaterial();
            let propertySetMatch = this.propertySets.filter((ps) => {
              return (ps.material.pk == m.pk) && (ps.property.pk == p.pk);
            });

            if (propertySetMatch.length > 0) {
              pstructMaterial.propertySet = propertySetMatch[0];
              pstructMaterial.enabled = propertySetMatch[0].is_active;
            }
            else {
              let ps_ = new PropertySet();
              ps_.property = p;
              ps_.material = m;
              pstructMaterial.propertySet = ps_;
              pstructMaterial.enabled = false;
            }
            pstruct.materials.push(pstructMaterial);
          }
          this.propertyStructures.push(pstruct);
        }

        this.propertyStructures.forEach((ps) => {
          let isActive = false;
          ps.materials.forEach((psm) => {
            if (psm.enabled)
              isActive = true;
          });
          ps.is_active = isActive; //La struttura è attiva se almeno uno dei materiali a cui è collegata è attivo
        });
        this.loadingService.hide();
      });
    }
    else {
      this.loadingService.hide();
    }
  }

  isMaterialInStructure(ps: PropertyStructure) {

    for (let m of ps.materials) {
      if ((m.propertySet.material.pk == this.material.pk) && m.enabled) {
        return true;
      }
    }
    return false;
  }

  editProp(p: Property) {
    this.editProperty.emit(p);
  }

  addAllPropertiesToMaterial() {
    let psToSave: PropertySet[] = this.propertyStructures
      .filter((ps) => !this.isMaterialInStructure(ps) && ps.is_active)
      .map((ps) =>
        ps.materials.filter((m) => {
            return m.enabled == false && (m.propertySet.material.pk == this.material.pk)
          }
        )[0].propertySet //Recupero il propertySet corretto
      );

    this.loadingService.show();
    this.processPsToSave(psToSave, 0, (resp) => {
      this.loadingService.hide();
      if (resp.pk) {
        this.messageService.open({
          title: " ",
          message: this.tr("SAVE_COMPLETED"),
          autoClose: true
        });
        this.loadPropertySets();
      }
      else {
        this.handleError(resp);
      }
    })
  }

  processPsToSave(psToSave: PropertySet[], index: number, callback: any, lastResponse?: any) {
    if (psToSave[index]) {
      let ps = psToSave[index];
      this.propertySetService.savePropertySet(ps).subscribe((resp) => {
        if (resp.pk) {
          this.processPsToSave(psToSave, index + 1, callback, resp);
        }
        else {
          callback.call(null, resp);
        }
      })
    }
    else {
      if (!lastResponse)
        lastResponse = {pk: 1};
      callback.call(null, lastResponse);
    }
  }

  createProp() {
    this.editProp(this.initPropertyToAdd());
  }

  handleError(resp) {
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  deleteProp(ps: PropertyStructure) {
    let psToDelete = ps.materials.map((psm) => {
      return psm.propertySet;
    }).filter((ps) => {
      return ps.pk;
    });

    this.loadingService.show();
    this.processPsToRemove(psToDelete, 0, (resp) => {
      this.loadingService.hide();
      if (resp.status == 204) {
        this.loadPropertySets();
      }
      else {
        this.handleError(resp);
      }
    })
  }

  processPsToRemove(psToRemove: PropertySet[], index: number, callback: any, lastResponse?: any) {
    if (psToRemove[index]) {
      let ps = psToRemove[index];
      this.propertySetService.deletePropertySet(ps).subscribe((resp) => {
        if (resp.status == 204) {
          this.processPsToRemove(psToRemove, index + 1, callback, resp);
        }
        else {
          callback.call(null, resp);
        }
      })
    }
    else {
      if (!lastResponse)
        lastResponse = {status: 204};
      callback.call(null, lastResponse);
    }
  }

  restoreProp(ps: PropertyStructure) {
    this.editProp(ps.property);
  }

  cloneProp(p: Property) {
    let property: Property = cloneObject(p);
    delete property['pk'];
    property.attribute = [];
    property.calculated = false;
    property.formula = new Formula();
    property.parameter_list = [];
    this.editProp(property);
  }

  getActivePS() {
    return this.propertyStructures.filter((ps) => ps.is_active);
  }

  getInactivePS() {
    return this.propertyStructures.filter((ps) => !ps.is_active);
  }
}
