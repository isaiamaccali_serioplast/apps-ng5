import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {DialogComponent} from "../../../../../../../shared/material/dialog/dialog.component";
import {MessageService} from "../../../../../../../shared/material/message/message.service";
import {PropertyName, PropertyNamesService} from "../../../../../../_common/services/property-names.service";
import {TestMethod, TestMethodService} from "../../../../../../_common/services/test-method.service";
import {MeasureUnit, MeasureUnitService} from "../../../../../../_common/services/measure-unit.service";
import {PropertyAttribute, PropertyAttributeService} from "../../../../../../_common/services/property-attribute.service";
import {PropertySet, PropertySetService} from "../../../../../../_common/services/property-set.service";
import {Formula, FormulasService} from "../../../../../../_common/services/formulas.service";
import {_getPropertyType, _PropertyTypes, PropertyType, PropertyTypeService} from "../../../../../../_common/services/property-type.service";
import {FormulaParameter, PropertiesService, Property} from "../../../../../../_common/services/properties.service";
import {TestMethodTypeService} from "../../../../../../_common/services/test-method-type.service";
import {TranslationService} from "../../../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../../../shared/loading/loading.service";
import {buildErrorMessage, cloneObject, fixValue, isElementInList, moveDownElementInList, moveUpElementInList, parseSerionetRespToData, removeElementFromList} from "../../../../../../../shared/utils";
import {Organization} from "../../../../../../_common/services/organization.service";
import {ContentType, ContentTypeService} from "../../../../../../_common/services/content-type.service";
import {TypeLink, TypeLinkService} from "../../../../../../_common/services/type-link.service";
import {Material} from "../../../../../../_common/services/materials.service";
import {TranslatableComponent} from "../../../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-material-edit-properties-edit',
  templateUrl: 'material-edit.properties-edit.html',
  styleUrls: ['material-edit.properties-edit.css']
})
export class MaterialEditPropertiesEditComponent extends TranslatableComponent implements OnInit {

  @Input() material: Material;
  @Input() propertyToEdit:Property;
  @Output() propertySaved = new EventEmitter();

  @ViewChild('parametersDialog') parametersDialog: DialogComponent;

  propertyNames: PropertyName[] = [];
  testMethods: TestMethod[] = [];
  testMethodTypes:ContentType[] = [];
  measureUnits: MeasureUnit[] = [];
  attributes: PropertyAttribute[] = [];
  formulas: Formula[] = [];
  types: PropertyType[] = [];
  siblings: Material[] = [];
  organizationFilter:Organization = new Organization();
  testMethodTypeLinkPK:number;

  PropertyTypes = _PropertyTypes;

  constructor(private messageService: MessageService, private propertyNameService: PropertyNamesService,
              private testMethodService: TestMethodService, private measureUnitService: MeasureUnitService,
              private attributeService: PropertyAttributeService, private propertySetService: PropertySetService,
              private formulasService: FormulasService, private propertyTypeLinkService: PropertyTypeService,
              private propertyService: PropertiesService, private testMethodTypeService: TestMethodTypeService,
              protected translationService: TranslationService, private loadingService: LoadingService,
              private contentTypeService:ContentTypeService, private typeLinkService:TypeLinkService
  ) {
    super(translationService);
  }

  ngOnInit() {
    this.testMethodTypeService.getTestMethodTypes({is_active: true}).subscribe((data) => {
      this.testMethodTypes = data;
    });
    this.measureUnitService.getMeasureUnits({is_active: true}).subscribe((data) => {
      this.measureUnits = parseSerionetRespToData(data);
    });
    this.formulasService.getFormulas({is_active: true}).subscribe((data) => {
      this.formulas = parseSerionetRespToData(data);
    });
    this.propertyTypeLinkService.getPropertyTypes({is_active: true}).subscribe((data) => {
     this.types = parseSerionetRespToData(data);
     if(this.propertyToEdit.type)
       fixValue(this.types,this.propertyToEdit,'type');
    });

    if (this.material && this.material.sub_model && this.material.sub_model.shape && this.material.sub_model.shape.pk) {
      this.siblings= this.material.sub_model.shape.material_list || [];
    }
    else {
      this.siblings = [this.material];
    }

    this.propertyToEdit.sets = this.siblings.map((m)=>{
      let ps = new PropertySet();
      ps.property = this.propertyToEdit;
      ps.material = m;
      ps.is_active = (m.pk==this.material.pk);
      if(ps.is_active)
        ps.status = "toSave";
      return ps;
    });
    if(this.propertyToEdit.pk){
      this.propertySetService.getPropertySets({property:this.propertyToEdit.pk}).subscribe((resp)=>{
        let ps:PropertySet[] = parseSerionetRespToData(resp);
        this.propertyToEdit.sets.forEach((set,index)=>{
          let match = ps.filter((ps_)=>ps_.material.pk==set.material.pk);
          if(match && match[0]){
            this.propertyToEdit.sets[index] = match[0];
          }
        });
        this.handleTestMethodTypeChange();
      })
    }

    this.contentTypeService.getContentTypes({model:'groupstandarddraft', is_active:true}).subscribe((resp)=>{
      if(resp && resp['count'] && resp['count']>0){
        let ctPK = resp['results'][0].pk;
        this.typeLinkService.getTypeLinks({app:ctPK, is_active:true}).subscribe((data)=>{
          let tl:TypeLink[] = parseSerionetRespToData(data);
          this.testMethodTypeLinkPK = tl.filter((t)=>t.delta_rel==0)[0].pk;
        })
      }
    });
    if(this.propertyToEdit.content_type && this.propertyToEdit.object_id){
      this.testMethodService.getTestMethods({stype:this.propertyToEdit.content_type.pk, id:this.propertyToEdit.object_id, standard_type:this.testMethodTypeLinkPK}).subscribe((resp)=>{
        let tms:TestMethod[] = parseSerionetRespToData(resp);
        if(tms.length>0){
          if(tms[0].organizations){
            this.organizationFilter = tms[0].organizations;
            this.handleOrganizationChange();
          }
        }
      })
    }
  }

  saveProperty() {
    let nCallbacks = 0;

    let callback = () =>{
      nCallbacks--;
      if (nCallbacks == 0) {
        this.propertySaved.emit();
      }
    };

    let hasMaterial = false;
    for (let mat of this.propertyToEdit.sets) {
      if (mat.is_active) {
        hasMaterial = true;
      }
    }

    if (!hasMaterial) {
      this.messageService.open({
        title: this.tr('ATTENTION'),
        message: 'You can\'t save a property without a material'
      })
    }
    else {
      let psToAdd = this.propertyToEdit.sets.filter((ps)=>ps.status=='toSave');
      let psToDelete = this.propertyToEdit.sets.filter((ps)=>ps.status=='toDelete'&&ps.pk);

      if (this.checkPropertyType()) {
        nCallbacks = 3;

        this.loadingService.show();
        this.propertyService.saveProperty(this.propertyToEdit).subscribe((resp) => {
          this.loadingService.hide();
          if (resp.pk) {
            callback();
            psToAdd.forEach((ps) => {
              ps.property = resp;
            });
            this.processPsToSave(psToAdd, 0, (resp) =>{
              if (resp.pk) {
                callback();
                this.processPsToRemove(psToDelete, 0, (resp_) =>{
                  if (resp_.status == 204) {
                    callback();
                  }
                  else {
                    this.handleError(resp_);
                  }
                })
              }
              else {
                this.handleError(resp);
              }
            });
          }
          else {
            this.handleError(resp);
          }
        });
      }
    }
  }

  checkPropertyType() {
    let type = this.propertyToEdit.type;
    if (!type) {
      this.messageService.open({
        title: this.tr('ATTENTION'),
        message: this.tr('PROPERTY_TYPE_MANDATORY')
      });
      return false;
    }
    else {
      let propType = _getPropertyType(this.propertyToEdit);
      switch (propType) {
        case this.PropertyTypes.BOOLEAN:
          return true;
        case this.PropertyTypes.MIN:
          if (this.propertyToEdit.lsl == null) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              message: this.tr('LSL_MANDATORY')
            });
            return false;
          }
          else {
            if (!this.propertyToEdit.uom.pk) {
              this.messageService.open({
                title: this.tr('ATTENTION'),
                message: this.tr('UOM_MANDATORY')
              });
              return false;
            }
            else {
              return true;
            }
          }
        case this.PropertyTypes.MAX:
          if (this.propertyToEdit.usl == null) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              message: this.tr('USL_MANDATORY')
            });
            return false;
          }
          else {
            if (!this.propertyToEdit.uom.pk) {
              this.messageService.open({
                title: this.tr('ATTENTION'),
                message: this.tr('UOM_MANDATORY')
              });
              return false;
            }
            else {
              return true;
            }
          }
        case this.PropertyTypes.RANGE:
          if (this.propertyToEdit.lsl == null) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              message: this.tr('LSL_MANDATORY')
            });
            return false;
          }
          else if (this.propertyToEdit.usl == null) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              message: this.tr('USL_MANDATORY')
            });
            return false;
          }
          else if (this.propertyToEdit.target == null) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              message: this.tr('TARGET_MANDATORY')
            });
            return false;
          }
          else if (!this.propertyToEdit.uom.pk) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              message: this.tr('UOM_MANDATORY')
            });
            return false;
          }
          else {
            return true;
          }
        default:
          this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('PROPERTY_TYPE_MANDATORY')
          });
          return false;
      }
    }
  }

  getAvailableProperties() {
    return this.propertyToEdit.available_props;
  }

  undoEditProp() {
    this.propertySaved.emit();
  }

  handleTypeChange() {
    for (let t of this.types) {
      if (t.pk == this.propertyToEdit.type.pk) {
        this.propertyToEdit.type = cloneObject(t);
        if (this.propertyToEdit.type.__str__ == 'Boolean') {
          this.propertyToEdit.lsl = null;
          this.propertyToEdit.usl = null;
          this.propertyToEdit.uom = null;
          this.propertyToEdit.target = 1;
        }
        else {
          if (!this.propertyToEdit.uom) {
            this.propertyToEdit.uom = new MeasureUnit();
          }
        }
      }
    }
  }

  handleMeasureChange() {
    for (let m of this.measureUnits) {
      if (m.pk == this.propertyToEdit.uom.pk) {
        this.propertyToEdit.uom.uom = cloneObject(m.uom);
        break;
      }
    }
  }

  handleCalculatedChange() {
    this.propertyToEdit.formula = new Formula();
  }

  handleFormulaChange() {
    fixValue(this.formulas, this.propertyToEdit, 'formula');
    this.propertyToEdit.parameter_list = [];
  }

  openParametersDialog() {
    this.propertySetService.getPropertySets({is_active:true, material:this.material.pk}).subscribe((resp)=>{
      let av_props:PropertySet[] = parseSerionetRespToData(resp);
      this.propertyToEdit.available_props = av_props.map((ps)=>ps.property).filter((p)=>p.pk!=this.propertyToEdit.pk);
    });
    this.parametersDialog.open();
  }

  isPropertySetAsParameter(p: Property) {
    return isElementInList(p.pk, this.propertyToEdit.parameter_list.map((p)=>p.parameter));
  }

  togglePropertyAsParameter(p: Property) {

    let pIds = this.propertyToEdit.parameter_list.map((p) => {
      return p.parameter;
    });

    if (pIds.indexOf(p.pk) != -1) {
      this.propertyToEdit.parameter_list.splice(pIds.indexOf(p.pk), 1);
    }
    else {
      let fp = new FormulaParameter();
      fp.parameter = p.pk;
      this.propertyToEdit.parameter_list.push(fp);
    }

    this.reIndexParameters();
  }

  moveParameterUp(p: FormulaParameter) {
    moveUpElementInList(p, this.propertyToEdit.parameter_list);
    this.reIndexParameters();
  }

  moveParameterDown(p: FormulaParameter) {
    moveDownElementInList(p, this.propertyToEdit.parameter_list);
    this.reIndexParameters();
  }

  removeParameter(p: FormulaParameter) {
    removeElementFromList(p, this.propertyToEdit.parameter_list);
    this.reIndexParameters();
  }

  reIndexParameters() {
    this.propertyToEdit.parameter_list.forEach((p, index) => {
      p.delta = index;
    })
  }

  handleTestMethodTypeChange() {
    this.testMethods = [];
    if(this.propertyToEdit.content_type.pk==55){
      this.organizationFilter = new Organization();
      this.testMethodService.getTestMethods({
        stype: this.propertyToEdit.content_type.pk,
        standard_type:this.testMethodTypeLinkPK
      }).subscribe((data) => {
        this.testMethods = parseSerionetRespToData(data);
        if(this.propertyToEdit.object_id){
          this.handleTestMethodChange();
        }
      });
    }
    else {
      this.organizationFilter = new Organization();
      this.handleOrganizationChange();
    }
  }

  handleOrganizationChange() {
    this.testMethods = [];
    if(this.propertyToEdit.content_type.pk==66){
      this.testMethodService.getTestMethods(
        {
          stype: this.propertyToEdit.content_type.pk,
          organizations:this.organizationFilter?this.organizationFilter.pk:null,
          organizations__null:this.organizationFilter?1:2,
          standard_type:this.testMethodTypeLinkPK
        }).subscribe((data) => {
        this.testMethods = parseSerionetRespToData(data);
        if(this.propertyToEdit.object_id){
          this.handleTestMethodChange();
        }
      });
    }
  }

  getPropertyNames(keyword: string) {
    return this.propertyNameService.getPropertyNames({
      is_active: true,
      name_icontains: keyword,
      results: 5
    })
  }

  getAttributes(keyword: string) {
    return this.attributeService.getAttributes({is_active: true, label_icontains: keyword, results: 5});
  }

  selectedName(pn: PropertyName) {
    this.propertyToEdit.name = pn;
  }

  handleError(resp) {
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  getPropertyType(p: Property) {
    return _getPropertyType(p);
  }

  processPsToSave(psToSave: PropertySet[], index: number, callback: any, lastResponse?: any) {
    if (psToSave[index]) {
      let ps = psToSave[index];
      this.propertySetService.savePropertySet(ps).subscribe((resp) => {
        if (resp.pk) {
          this.processPsToSave(psToSave, index + 1, callback, resp);
        }
        else {
          callback.call(null, resp);
        }
      })
    }
    else {
      if (!lastResponse)
        lastResponse = {pk: 1};
      callback.call(null, lastResponse);
    }
  }

  processPsToRemove(psToRemove: PropertySet[], index: number, callback: any, lastResponse?: any) {
    if (psToRemove[index]) {
      let ps = psToRemove[index];
      this.propertySetService.deletePropertySet(ps).subscribe((resp) => {
        if (resp.status == 204) {
          this.processPsToRemove(psToRemove, index + 1, callback, resp);
        }
        else {
          callback.call(null, resp);
        }
      })
    }
    else {
      if (!lastResponse)
        lastResponse = {status: 204};
      callback.call(null, lastResponse);
    }
  }

  handleTestMethodChange(){
    let tm = this.testMethods.filter((t)=>t.pk==this.propertyToEdit.object_id)[0];
    if(tm.organizations)
      this.organizationFilter = tm.organizations;
  }

  changePSStatus(ps:PropertySet){
    if(ps.is_active)
      ps.status = "toSave";
    else
      ps.status = "toDelete";
  }

  organizationSelected(o:Organization){
    if(o && o.pk)
      this.organizationFilter = o;
    else
      this.organizationFilter = new Organization();

    this.handleOrganizationChange();
  }
}
