import {Component, OnInit, Input} from "@angular/core";
import {Material} from "../../../../../_common/services/materials.service";
import {PropertyStructure} from "../../../../models/property-structure";

@Component({
  selector: 'app-material-edit-properties',
  templateUrl: 'material-edit.properties.html',
  styleUrls: ['material-edit.properties.css']
})
export class MaterialEditPropertiesComponent implements OnInit {

  @Input() material: Material;
  @Input() editTab: any;
  @Input() saveTab: any;

  editOn = false;
  propertyToEdit:PropertyStructure;

  constructor() {

  }

  ngOnInit() {

  }

  editProperty(ps:PropertyStructure){
    this.propertyToEdit = ps;
    this.editOn = true;
  }

  propertySaved(){
    this.editOn = false;
    this.propertyToEdit = null;
  }
}
