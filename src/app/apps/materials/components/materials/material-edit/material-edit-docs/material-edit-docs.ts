import {Component, Input, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {Organization, OrganizationService} from "../../../../../_common/services/organization.service";
import {ContentTypeService} from "../../../../../_common/services/content-type.service";
import {FileService, SPFile} from "../../../../../_common/services/file.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {
  arrayGroupBy, buildErrorMessage, cloneObject,
  parseSerionetRespToData
} from "../../../../../../shared/utils";
import {Material} from "../../../../../_common/services/materials.service";
import {
  MaterialOrganization,
  MaterialOrganizationService
} from "../../../../../_common/services/material-organization.service";
import {TypeLink, TypeLinkService} from "../../../../../_common/services/type-link.service";
import {FileManager} from "../../../../../_common/services/file-manager.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

export class MaterialOrganizationFile {
  materialOrganization: MaterialOrganization;
  active: boolean;
}

export class OrganizationDocumentFile {
  document: SPFile;
  organization: Organization;
  is_enabled_for_material: boolean;
  material_organizations: MaterialOrganizationFile[];
  updateFile = false;
}

export class OrganizationDocument {
  organization: Organization;
  documents: OrganizationDocumentFile[];
}

@Component({
  selector: 'app-material-edit-docs',
  templateUrl: 'material-edit-docs.html',
  styleUrls: ['material-edit-docs.css']
})
export class MaterialEditDocsComponent extends BaseComponent implements OnInit {

  documentInput: any;
  @Input() material: Material;
  @Input() editTab: any;
  @Input() saveTab: any;
  @Input() setHasUnsavedData: any;

  matOrgContentType: number;
  orgDocs: OrganizationDocument[] = [];
  siblings: Material[] = [];
  editOn = false;
  disableSection = false;
  odfToEdit: OrganizationDocumentFile;
  matOrgDocTypes: TypeLink[] = [];
  organizations: Organization[];
  createMode = false;
  matOrgs: MaterialOrganization[] = [];

  constructor(protected translationService: TranslationService, private organizationsService: OrganizationService,
              private contentTypeService: ContentTypeService, private fileService: FileService,
              protected messageService: MessageService, protected loadingService: LoadingService,
              private materialOrganizationService: MaterialOrganizationService, private typeLinkService: TypeLinkService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    let count = 3;
    this.loadingService.show();
    let callback = () => {
      count--;
      if (count == 0) {
        this.loadingService.hide();
        this.getSiblings();
        if (this.siblings.length > 0) {
          this.loadMatDoc();
        }
        else {
          this.disableSection = true;
          this.messageService.open({
            title: 'Error',
            error: true,
            message: "Invalid shape data",
            autoClose: false
          });
        }
      }
    };

    this.contentTypeService.getContentTypes({model: 'materialorganization', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.matOrgContentType = data['results'][0]['pk'];
        callback();
      }
    });

    this.contentTypeService.getContentTypes({model: 'file', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let fileCT = data['results'][0]['pk'];
        this.typeLinkService.getTypeLinks({app: fileCT, context: 'materialorganization', is_active: true}).subscribe((resp) => {
          this.matOrgDocTypes = parseSerionetRespToData(resp);
          callback();
        })
      }
    });

    this.organizationsService.getOrganizations({is_active: true, results: 500, ordering: 'name'}).subscribe((resp) => {
      this.organizations = parseSerionetRespToData(resp);
      callback();
    });
  }

  getSiblings() {
    if (this.material && this.material.sub_model && this.material.sub_model.shape && this.material.sub_model.shape.pk) {
      this.siblings = this.material.sub_model.shape.material_list || [];
    }
    else {
      this.siblings = [this.material];
    }
  }

  initMatDoc(o: Organization) {
    this.odfToEdit = new OrganizationDocumentFile();
    this.odfToEdit.organization = new Organization();

    this.odfToEdit.document = new SPFile();
    this.odfToEdit.document.type = new TypeLink();
    this.odfToEdit.material_organizations = [];

    if (o) {
      this.odfToEdit.organization = o;
      this.organizationSelected(o);
    }
    // this.handleOrganizationEditChange();
  }

  organizationSelected(o: Organization) {
    if (o && o.pk) {
      this.odfToEdit.organization = o;
      this.odfToEdit.material_organizations = [];
      this.siblings.forEach((m) => {
        if (this.odfToEdit.organization.pk) {
          let match = this.matOrgs.filter((mo) => (mo.material.pk == m.pk) && mo.organization.pk == this.odfToEdit.organization.pk);
          if (match && match.length > 0) {
            let mof = new MaterialOrganizationFile();
            mof.materialOrganization = match[0];
            mof.active = false;
            this.odfToEdit.material_organizations.push(mof);
          }
          else {
            let mo = new MaterialOrganization();
            mo.is_active = true;
            mo.organization = this.odfToEdit.organization;
            mo.material = m;
            let mof = new MaterialOrganizationFile();
            mof.materialOrganization = mo;
            mof.active = false;
            this.odfToEdit.material_organizations.push(mof);
          }
        }
      });
    }
    else {
      this.odfToEdit.organization = new Organization();
      this.odfToEdit.material_organizations = [];
    }

  }

  canSave() {
    return this.odfToEdit.organization.pk &&
      this.odfToEdit.document.doc_id &&
      this.odfToEdit.document.version &&
      this.odfToEdit.material_organizations.filter((d) => {
        return d.active
      }).length > 0;
  }

  loadMatDoc() {
    this.materialOrganizationService.getMaterialOrganizations({
      material_id_list: this.siblings.map((m) => {
        return m.pk
      }).join("|"),
      is_active: true
    }).subscribe((data) => {
      this.matOrgs = parseSerionetRespToData(data);
      this.getOrganizationDocs(this.matOrgs);
    });
  }

  getOrganizationDocs(matOrgs: MaterialOrganization[]) {
    let grouped = arrayGroupBy(matOrgs, 'organization', true);

    let organizations = Object.keys(grouped).map((oPk) => {
      return matOrgs.filter((mo) => mo.organization.pk == parseInt(oPk, 10))[0].organization;
    });

    this.orgDocs = organizations.map((o) => {
      let orgDoc = new OrganizationDocument();
      orgDoc.organization = o;
      orgDoc.documents = [];
      let mo = grouped[o.pk].map((mo: MaterialOrganization) => mo.pk);
      this.fileService.getFiles({
        objects_content_type: this.matOrgContentType,
        objects_obj_id_list: mo.join("|"),
        is_active: true
      }).subscribe((resp) => {
        let files: SPFile[] = parseSerionetRespToData(resp);
        orgDoc.documents = files.map((f) => {
          let odf = new OrganizationDocumentFile();
          odf.document = f;
          odf.organization = o;
          let fObjIds = f.related_objects.map((ro) => ro.object_id);
          let match = matOrgs.filter((mo) => (mo.material.pk == this.material.pk) && mo.organization.pk == o.pk);
          if (match && match.length > 0) {
            odf.is_enabled_for_material = fObjIds.indexOf(match[0].pk) != -1;
          }
          else {
            odf.is_enabled_for_material = false;
          }
          odf.material_organizations = [];
          this.siblings.forEach((m) => {
            let match = matOrgs.filter((mo) => (mo.material.pk == m.pk) && mo.organization.pk == o.pk);
            if (match && match.length > 0) {
              let mof = new MaterialOrganizationFile();
              mof.materialOrganization = match[0];
              mof.active = f.related_objects.filter((o) => o.object_id == match[0].pk).length > 0;
              odf.material_organizations.push(mof);
            }
            else {
              let mo = new MaterialOrganization();
              mo.is_active = false;
              mo.organization = o;
              mo.material = m;
              let mof = new MaterialOrganizationFile();
              mof.materialOrganization = mo;
              mof.active = false;
              odf.material_organizations.push(mof);
            }
          });
          return odf;
        });
      });
      return orgDoc;
    });
  }

  extractFileName(filePath) {
    let pieces = filePath.split("/");
    if (pieces.length <= 1)
      pieces = filePath.split("\\");
    return pieces[pieces.length - 1];
  }

  editMaterialOrganizationDocument(odf: OrganizationDocumentFile) {
    this.odfToEdit = odf;
    this.editOn = true;
  }

  downloadMaterialOrganizationDocument(f: SPFile) {
    let a = document.createElement("a");
    a.setAttribute("download", "");
    a.setAttribute("href", f.attachment_file);
    a.click();
  }

  createMaterialOrganizationDocument(o?: Organization) {
    this.createMode = true;
    this.initMatDoc(o);
    this.editOn = true;
  }

  undoMaterialOrganizationDocument() {
    this.editOn = false;
  }

  deleteMaterialOrganizationDocument(f: SPFile) {
    this.messageService.open({
      title: this.tr('CONFIRM'),
      message: this.tr('CONFIRM_DELETE'),
      confirm: () => {
        this.loadingService.show();
        this.fileService.deleteFile(f).subscribe((response) => {
          this.loadingService.hide();
          this.manageRespDelete(response, () => {
            this.loadMatDoc();
          });
        })
      },
      deny: () => {
      }
    })
  }

  saveMaterialOrganizationDocument() {
    let f: SPFile = cloneObject(this.odfToEdit.document);
    let mo: MaterialOrganization[] = this.odfToEdit.material_organizations.filter((mof) => mof.active).map((mof) => mof.materialOrganization);

    this.loadingService.show();
    let saveFile = () => {
      f.related_objects = mo.map((mo_) => {
        let fm = new FileManager();
        fm.object_id = mo_.pk;
        fm.content_type = this.matOrgContentType;
        return fm;
      });
      this.fileService.saveFile(f, this.odfToEdit.updateFile ? this.documentInput : null).subscribe((resp) => {
        this.manageRespSave(resp, () => {
          this.loadingService.hide();
          this.editOn = false;
          this.loadMatDoc();
        })
      })
    };

    let moToSave = mo.filter((mo) => !mo.pk || !mo.is_active);

    if (moToSave.length > 0) {
      let count = moToSave.length;
      let callback = () => {
        count--;
        if (count == 0) {
          saveFile();
        }
      };
      moToSave.forEach((mo) => {
        mo.is_active = true;
        this.materialOrganizationService.saveMaterialOrganization(mo).subscribe((resp) => {
          if (resp.pk) {
            mo.pk = resp.pk;
            callback();
          }
        })
      })
    } else {
      saveFile();
    }
  }

  uploadFile(input: HTMLInputElement) {
    this.documentInput = input;
    this.odfToEdit.updateFile = true;
  }
}
