import {NgModule} from "@angular/core";
import {MaterialEditComponent} from "./material-edit.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {MaterialEditPropertiesComponent} from "./material-edit-properties/material-edit.properties";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {MaterialEditMainInfoComponent} from "./material-edit-main-info/material-edit-main-info";
import {MaterialCodeGeneratorService} from "./material-code-generator/material-code-generator-service";
import {PkTransformerModule} from "../../../../../shared/components/pk-transformer/pk-transformer.module";
import {MaterialEditBomComponent} from "./material-edit-bom/material-edit-bom";
import {MaterialEditPackageInstructionsComponent} from "./material-edit-packing-instructions/material-edit-packing-instructions";
import {MaterialEditDocsComponent} from "./material-edit-docs/material-edit-docs";
import {MaterialEditPropertiesEditComponent} from "./material-edit-properties/material-edit-properties-edit/material-edit.properties-edit";
import {MaterialEditPropertiesListComponent} from "./material-edit-properties/material-edit-properties-list/material-edit.properties-list";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule, PkTransformerModule],       // module dependencies
  declarations: [MaterialEditComponent, MaterialEditPropertiesComponent, MaterialEditMainInfoComponent,
    MaterialEditBomComponent, MaterialEditPackageInstructionsComponent,
    MaterialEditDocsComponent, MaterialEditPropertiesEditComponent, MaterialEditPropertiesListComponent],   // components and directives
  exports: [],
  providers: [MaterialCodeGeneratorService]                    // services
})
export class MaterialEditModule {
}


