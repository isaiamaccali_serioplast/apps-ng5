import {Component, Input, OnInit} from "@angular/core";
import {BOM, BOMItem, BOMService, BOMSite} from "../../../../../_common/services/bom.service";
import {Site} from "../../../../../_common/services/sites.service";
import {MaterialType, MaterialTypesService} from "../../../../../_common/services/material-type.service";
import {Material, MaterialsService} from "../../../../../_common/services/materials.service";
import {MeasureUnitService} from "../../../../../_common/services/measure-unit.service";
import {allowsBOMCreation, MaterialPlantsService} from "../../../../../_common/services/material-plant.service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {ProductionType, ProductionTypesService} from "../../../../../_common/services/production-types.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {
  arrayDiff, buildErrorMessage, cloneObject, fixValue, formatNumber,
  parseSerionetRespToData
} from "../../../../../../shared/utils";
import {
  RoutingProductionVersion,
  RoutingProductionVersionService
} from "../../../../../_common/services/routing-production-version-service";
import {Routing, RoutingsService} from "../../../../../_common/services/routings.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: "app-material-edit-bom",
  templateUrl: "material-edit-bom.html",
  styleUrls: ["material-edit-bom.css"]
})
export class MaterialEditBomComponent extends BaseComponent implements OnInit {

  @Input() material: Material;
  @Input() editTab: any;
  @Input() saveTab: any;
  @Input() setHasUnsavedData: any;
  @Input() bomCreatable: boolean;
  // @Output() routingCreatableEvent = new EventEmitter();

  boms: BOM[];
  bom: BOM;
  allBoMs: BOM[] = [];

  bomItem: BOMItem;
  site: Site;
  sites: Site[];

  materialType: number;
  materialTypes: MaterialType[];

  materials: Material[];
  activeSites: Site[] = [];
  productionTypes: ProductionType[] = [];

  constructor(private bomService: BOMService, private materialTypesService: MaterialTypesService, private materialsService: MaterialsService,
              private measureUnitService: MeasureUnitService, private materialPlantService: MaterialPlantsService,
              protected messageService: MessageService, protected translationService: TranslationService,
              private productionTypesService: ProductionTypesService, protected loadingService: LoadingService,
              private routingProductionVersionService: RoutingProductionVersionService, private routingsService: RoutingsService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.boms = [];
    this.sites = [];
    this.materialTypes = [];
    this.materials = [];

    this.materialTypesService.getMaterialTypes({is_active: true}).subscribe((data) => {
      this.materialTypes = parseSerionetRespToData(data);
    });
    this.materialPlantService.getMaterialPlants({material_id: this.material.pk, is_active: true}).subscribe((data) => {
      const mps = parseSerionetRespToData(data);
      this.productionTypesService.getProductionTypes({is_active: true}).subscribe((data) => {
        this.productionTypes = parseSerionetRespToData(data);
        mps.forEach((mp) => {
          fixValue(this.productionTypes, mp, "production_type");
        });
        this.activeSites = mps
          .filter((mp) => {
            return allowsBOMCreation(mp);
          })
          .map((mp) => {
            return mp.site;
          });
        this.loadBOMs();
        //self.fixSites(self.activeSites);

      });
    })
  }

  loadBOMs() {
    this.bomService.getBOMs({material_id: this.material.pk, is_active: true}).subscribe((data) => {
      this.boms = parseSerionetRespToData(data);
      this.boms.sort((b1, b2) => {
        return parseInt(b1.alt_bom_id, 10) - parseInt(b2.alt_bom_id, 10);
      });
      this.fixBOMData();
      // this.routingCreatableEvent.emit(this.boms.length>0)
    });

    this.bomService.getBOMs({material_id: this.material.pk}).subscribe((data) => {
      this.allBoMs = parseSerionetRespToData(data)
    })
  }

  fixBOMData() {
    for (let b of this.boms) {
      b.material_id = this.material;
      this.initBOMSites(b);
      this.fixBOMItems(b.bomitem_set);
    }
  }

  initBOMSites(b: BOM) {
    let activeSitesPK = this.activeSites.map((s: Site) => s.pk);
    let sites_saved = b.sites.map((s: Site) => {
      return s.pk
    });
    b.bom_sites = [];
    for (let s of this.activeSites) {
      let bs = new BOMSite();
      bs.site = s;
      bs.status = "";
      bs.active = (sites_saved.indexOf(s.pk) != -1);
      bs.disabled = false;
      b.bom_sites.push(bs);
    }
    let orphans = arrayDiff(sites_saved, activeSitesPK);
    for (let o of orphans) {
      let bs = new BOMSite();
      bs.site = b.sites.filter((s: Site) => s.pk == o)[0];
      bs.status = "";
      bs.active = true;
      bs.disabled = true;
      b.bom_sites.push(bs);
    }
  }

  // fixSites(sites:Site[]){
  //   for(let i=0; i<sites.length; i++){
  //     this.fixSite(sites,i);
  //   }
  // }
  //
  // fixSite(sites,index){
  //   this.sitesService.getSite(sites[index].pk).subscribe((site)=>{
  //     sites[index] = site;
  //   })
  // }

  fixBOMItems(bSets: BOMItem[]) {
    for (let i = 0; i < bSets.length; i++) {
      this.fixUOM(bSets, i);
      this.fixComponent(bSets, i);
    }
  }

  fixUOM(bSets: BOMItem[], index) {
    this.measureUnitService.getMeasureUnit(parseInt(bSets[index].uom + "", 10)).subscribe((uom) => {
      bSets[index].uom = uom;
    })
  }

  fixComponent(bSets: BOMItem[], index) {
    this.materialsService.getMaterial(parseInt(bSets[index].material + "", 10)).subscribe((component) => {
      bSets[index].material = component;
    })
  }

  disableAllBOMs() {
    this.boms.forEach((b) => {
      b.enable_add_item = false;
    })
  }

  enableAddMaterialToBOM(b: BOM) {
    this.disableAllBOMs();
    b.enable_add_item = true;
    this.initNewBOMItem();
    this.bom = b;
  }

  initNewBOMItem() {
    this.bomItem = new BOMItem();
    this.bomItem.group = 1;
  }

  disableAddMaterialToBOM(b: BOM) {
    b.enable_add_item = false;
    this.initNewBOMItem();
    this.bom = null;
  }

  addMaterialToBOM(b: BOM) {
    if (!this.bomItem.pk) {
      b.bomitem_set.push(cloneObject(this.bomItem));
    }
    else {
      b.bomitem_set.forEach(((element, index) => {
        if (this.bomItem.pk == element.pk) {
          b.bomitem_set[index] = this.bomItem;
        }
      }))
    }
    this.disableAddMaterialToBOM(b);
  }

  selectedMaterial(event: any) {
    if (event != null) {
      this.bomItem.material = event;
      this.bomItem.uom = event.uom;
    }
  }

  handleTypeChange() {
    this.initNewBOMItem();
  }

  editBOMItem(b: BOM, bi: BOMItem) {
    this.enableAddMaterialToBOM(b);
    if (!bi.pk) {
      //Fake, just to have the same form of original bom items also for new added items
      bi.pk = (new Date()).getTime();
    }
    this.bomItem = cloneObject(bi);
  }

  deleteBOMItem(b: BOM, bi: BOMItem) {
    b.bomitem_set.forEach(((bi_, index) =>{
      if (bi.material == bi_.material) {
        b.bomitem_set.splice(index, 1);
      }
    }));
    //this.saveBOM(b);
  }

  saveBOM(b: BOM) {
    this.loadingService.show();
    this.bomService.saveBOM(b).subscribe((response) => {
      this.loadingService.hide();
      this.manageRespSave(response, () => {
        this.loadBOMs();
      });
    })
  }

  // isSiteActive(s:Site,b:BOM){
  //   return b.sites.map(function(s){return s.pk}).indexOf(s.pk)!=-1;
  // }

  createBOM() {
    let bom = new BOM();
    bom.sites = [];
    bom.bomitem_set = [];
    bom.material_id = this.material;
    bom.alt_bom_id = this.getNewAlternativeID();
    this.initBOMSites(bom);
    this.boms.push(bom);
  }

  getNewAlternativeID() {
    let alternatives = this.allBoMs.map((b)=> {
      return parseInt(b.alt_bom_id, 10);
    });
    let lastAlternative = 0;
    if (alternatives.length > 0) {
      lastAlternative = Math.max.apply(null, alternatives);
    }
    return formatNumber(2, lastAlternative + 1);
  }

  hasBOMInEdit() {
    let bomInEdit = false;
    this.boms.forEach((b) => {
      if (b.enable_add_item) {
        bomInEdit = true;
      }
    });
    return bomInEdit;
  }

  hasUnsavedBOM() {
    let hasUnSavedBOM = false;
    this.boms.forEach((b) => {
      if (!b.pk) {
        hasUnSavedBOM = true;
      }
    });
    return hasUnSavedBOM;
  }

  handleBSChange(bs: BOMSite, b: BOM) {
    if (bs.active) {
      this.verifySiteCompliance(bs.site, b, (isOK) => {
        if (!isOK)
          bs.active = false;
        else
          bs.status = "toSave";
      })
    }
    else {
      if (b.pk) {
        this.routingProductionVersionService.getRoutingProductionVersions({
          bom_header: b.pk,
          is_active: true
        }).subscribe((resp) => {
          let pvs: RoutingProductionVersion[] = parseSerionetRespToData(resp);
          if (pvs.length != 0) {

            let isOK = true;
            let count = pvs.length;
            let callback = () => {
              count--;
              if (count == 0) {
                if (isOK) {
                  bs.status = "toDelete";
                }
                else {
                  bs.active = true;
                  this.messageService.open({
                    title: this.tr('ERROR'),
                    error: true,
                    message: "Alternative used in a routing for this site. Impossible to delete",
                    autoClose: false
                  });
                }
              }
            };

            pvs.forEach((pv) => {
              this.routingsService.getRouting(pv.routing_header.pk).subscribe((r: Routing) => {
                let siteRouting = r.site.pk;
                if (siteRouting == bs.site.pk) {
                  isOK = false;
                }
                callback();
              });
            })

          }
          else {
            bs.status = "toDelete";
          }
        })
      }
      else {
        bs.status = "toDelete";
      }
    }
  }

  verifySiteCompliance(s: Site, b: BOM, callback) {
    if (b.bomitem_set.length > 0) {
      //Chiamo un servizio che verifica che il dato plant produca tutti i materiali contenuti nella BOM.
      //<sitePK>|<material1pk>|<material2pk>....
      let query = [s.pk];
      let bomItemMaterialPKs = b.bomitem_set.map((bi) => bi.material.pk);
      query = query.concat(bomItemMaterialPKs);
      this.materialsService.getMaterials({
        siteon_matlist: query.join("|")
      }).subscribe((data) => {
        let materials = parseSerionetRespToData(data);
        if (materials.length > 0) {
          let bomSitesPK = b.sites.map((s) => s.pk);
          let index = bomSitesPK.indexOf(s.pk);
          if (index != -1) {
            b.sites.splice(index, 1);
          }
          let as_b = this.activeSites.slice(0);
          this.activeSites = [];
          setTimeout(() => {
            this.activeSites = as_b;
          }, 100);
          //ci sono materiali non prodotti nei plant selezionati
          this.loadingService.hide();
          this.messageService.open({
            title: this.tr('BOM_ITEM_MISMATCH'),
            message: this.buildMismatchMessage(data['results']),
            confirm: ()=>{}
          });
          callback.call(null, false);
        }
        else {
          callback.call(null, true);
        }
      })
    }
    else {
      callback(true);
    }
  }

  buildMismatchMessage(ms: Material[]) {
    let content = [];
    content.push("<div>" + this.tr("MATERIALS_NOT_COMPLIANT") + "</div>");
    content.push("<ul>");
    ms.forEach((m) => {
      content.push("<li>");
      content.push(m.code + " - " + m.description);
      content.push("</li>");
    });
    content.push("</ul>");
    return content.join("");
  }

  // shouldSiteBeDisabled(s:Site,b:BOM){
  //   //return false;
  //   return (b.sites.length<2) && (this.isSiteActive(s,b))&& (this.activeSites.length==1);
  // }

  deleteBOM(b: BOM) {
    if (b.pk) {
      this.messageService.open({
        title: this.tr('CONFIRM_DELETE'),
        message: this.tr('DELETE_BOM_MESSAGE'),
        confirm: () => {
          this.loadingService.show();
          this.routingProductionVersionService.getRoutingProductionVersions({
            bom_header: b.pk,
            is_active: true
          }).subscribe((resp) => {
            let activePV = parseSerionetRespToData(resp);
            if (activePV.length == 0) {
              this.bomService.deleteBOM(b).subscribe((response) => {
                this.loadingService.hide();
                this.manageRespDelete(response, () => {
                  this.loadBOMs();
                });
              })
            }
            else {
              this.loadingService.hide();
              this.messageService.open({
                title: this.tr('ERROR'),
                error: true,
                message: "BoM currently in use as a production version. Impossible to delete",
                autoClose: false
              });
            }
          });
        },
        deny: ()=>{}
      })
    }
    else {
      let indexToDelete = null;
      this.boms.forEach((element, index) =>{
        if (element.alt_bom_id == b.alt_bom_id) {
          indexToDelete = index;
        }
      });
      if (indexToDelete != null) {
        this.boms.splice(indexToDelete, 1);
      }
    }
  }

  reload() {
    this.ngOnInit();
  }

  handleError(resp) {
    this.loadingService.hide();
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  canBOMBeSaved(b: BOM) {
    return !b.enable_add_item && !(b.bomitem_set.length == 0) && !(b.bom_sites.filter((bs) => bs.active).length == 0)
  }

  showBOMDelete(b: BOM) {
    return b.bom_sites.filter((bs) => bs.active).length == 1 && b.bom_sites.filter((bs) => bs.status != "").length == 0
  }
}
