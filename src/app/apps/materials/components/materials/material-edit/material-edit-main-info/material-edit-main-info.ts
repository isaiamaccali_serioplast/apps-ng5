import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {Shape, ShapesService} from "../../../../../_common/services/shapes.service";
import {Material, MaterialsService, Submaterial} from "../../../../../_common/services/materials.service";
import {MeasureUnit, MeasureUnitService} from "../../../../../_common/services/measure-unit.service";
import {_getMaterialType, _MATERIAL_TYPES, isBottleClosurePreform, MaterialType, MaterialTypesService} from "../../../../../_common/services/material-type.service";
import {TechnologyLink, TechnologyLinkService} from "../../../../../_common/services/technology-link.service";
import {SegmentsService} from "../../../../_common/services/segment.service";
import {Site, SitesService} from "../../../../../_common/services/sites.service";
import {buildErrorMessage, cloneObject, fixValue, parseSerionetRespToData} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MaterialCodeGeneratorService} from "../material-code-generator/material-code-generator-service";
import {DialogComponent} from "../../../../../../shared/material/dialog/dialog.component";
import {LoggingService} from "../../../../../../shared/logging/logging.service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {CavitiesService, Cavity} from "../../../../../_common/services/cavities.service";
import {CommodityCodesService} from "../../../../../_common/services/commodity-code.service";
import {allowsBOMCreation, MaterialPlant, MaterialPlantsService} from "../../../../../_common/services/material-plant.service";
import {ProductionType, ProductionTypesService} from "../../../../../_common/services/production-types.service";
import {LegalEntityService} from "../../../../../_common/services/legal-entity.service";
import {MatSnackBar} from "@angular/material";
import {MarketSegmentLink, MarketSegmentLinksService} from "../../../../../_common/services/market-segment-link.service";
import {MaterialSubTypeLink, MaterialSubtypesLinkService} from "../../../../../_common/services/material-subtype-link.service";
import {MainMaterialLinksService} from "../../../../../_common/services/main-material-links.service";
import {MaterialsAuthorizationsService, SERIOPLAST_RD_LAB} from "../../../../services/materials-authorizations.service";
import {Organization, OrganizationService} from "../../../../../_common/services/organization.service";
import {MaterialSubType} from "../../../../../_common/services/material-subtype.service";
import {HasCompanyPosition} from "../../../../../_common/services/has-company-position.service";
import {MainMaterial} from "../../../../../_common/services/main-materials.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

export class CavityCheckbox {
  cavity: Cavity;
  checked: boolean;
}

@Component({
  selector: 'app-material-edit-main-info',
  templateUrl: 'material-edit-main-info.html',
  styleUrls: ['material-edit-main-info.css']
})
export class MaterialEditMainInfoComponent extends BaseComponent implements OnInit {

  @Input() material: Material;
  @Input() editTab: any;
  @Input() saveTab: any;
  @Input() setHasUnsavedData: any;
  @Input() setBOMAvailableForMaterial: any;
  @Input() hasCompanyPositions: HasCompanyPosition[];

  @ViewChild('unvaluatedDialog') unvaluatedDialog: DialogComponent;
  @ViewChild('palletizedDialog') palletizedDialog: DialogComponent;
  @ViewChild('cloneDialog') cloneDialog: DialogComponent;
  @ViewChild('commodityDialog') commodityDialog: DialogComponent;

  materialTypes: MaterialType[] = [];
  uoms: MeasureUnit[] = [];
  weightUoms: MeasureUnit[] = [];
  materialSubTypeLinks: MaterialSubTypeLink[] = [];
  technologies: TechnologyLink[] = [];
  segments: MarketSegmentLink[] = [];
  mainMaterials: MainMaterial[] = [];
  // sites: Site[];
  locations: Site[] = [];
  sitesToAdd: Site[] = [];
  loading = true;
  isClonedMaterial = false;
  codeClass: string;
  materialCavities: string;
  cavities: Cavity[] = [];
  cavitiesCheckboxes: CavityCheckbox[] = [];
  siblingMaterials: Material[] = [];
  pallettizedMaterials: Material[] = [];
  clonableMaterials: Material[] = [];
  disableCodeGeneration = false;

  codeDescFilter: string;

  MATERIAL_TYPES = _MATERIAL_TYPES;

  commodityCodes: string[] = [];
  commodityCodesFiltered: string[] = [];
  ccCodeFilter: string = "";
  ccDescFilter: string = "";

  activeMP: MaterialPlant;

  materialPlants: MaterialPlant[] = [];
  productionTypes: ProductionType[] = [];
  siteToAdd: Site = null;

  ready = false;

  disableShowAllButton = false;

  constructor(private measureUnitService: MeasureUnitService, private materialsService: MaterialsService,
              private materialTypesService: MaterialTypesService, private materialSubtypeLinkService: MaterialSubtypesLinkService,
              private technologiesService: TechnologyLinkService, private segmentsService: MarketSegmentLinksService,
              private mainMaterialLinksService: MainMaterialLinksService, private sitesService: SitesService,
              protected messageService: MessageService, private shapesService: ShapesService, protected loadingService: LoadingService,
              private router: Router, private activatedRoute: ActivatedRoute, private matGen: MaterialCodeGeneratorService,
              private loggingService: LoggingService, protected translationService: TranslationService, private cavitiesService: CavitiesService,
              private commodityCodesService: CommodityCodesService, private materialPlantsService: MaterialPlantsService,
              private productionTypesService: ProductionTypesService, private legalEntityService: LegalEntityService,
              private materialsAuthorizationsService: MaterialsAuthorizationsService, private snackBar: MatSnackBar, private organizationService: OrganizationService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe((params) => {
      let materialModel = params['materialModel'];
      let nCalls = 2;

      let callback = () => {
        nCalls--;
        if (nCalls == 0) {
          if (materialModel) {
            this.materialsService.getMaterial(materialModel).subscribe((data) => {
              this.isClonedMaterial = true;
              this.cloneMaterial(data);
              this.setBOMAvailableForMaterial(false);
            });
          }
          else {
            if (this.checkEditMaterial()) {
              this.fixMaterial();

              let matTypeConst = _getMaterialType(this.material);

              // for M / C / B
              if (matTypeConst == _MATERIAL_TYPES.BILLET || matTypeConst == _MATERIAL_TYPES.MOULDSET || matTypeConst == _MATERIAL_TYPES.COMPONENT) {
                // check tabs to active/deactive
                this.materialsAuthorizationsService.announceMaterialEditInitComplete(this.material);
              } else {
                // for all material types
                if (!this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_LAB)) {
                  this.router.navigate(["materials"]);
                }
              }

            }
            else {
              this.setBOMAvailableForMaterial(false);
              this.messageService.open({
                error: true,
                title: this.tr('ERROR'),
                message: this.tr('MISSING_MAIN_INFORMATIONS'),
                confirm: () => {
                  this.router.navigate(["materials", "materials", "detail", this.material.pk, "view"]);
                }
              })
            }
          }
        }
      }

      this.materialTypesService.getMaterialTypes({is_active: true, ordering: 'description'}).subscribe((data) => {
        this.materialTypes = parseSerionetRespToData(data);
        callback();
      });
      this.measureUnitService.getMeasureUnits({is_active: true}).subscribe((data) => {
        this.uoms = parseSerionetRespToData(data);
        this.weightUoms = this.uoms.filter((u) => u.type == "W");
        callback();
      });
    })
  }

  loadMaterialPlants() {
    this.materialPlants = [];
    this.productionTypesService.getProductionTypes({is_active: true}).subscribe((data) => {
      this.productionTypes = parseSerionetRespToData(data);
      if (this.material.pk) {
        this.materialPlantsService.getMaterialPlants({
          material_id: this.material.pk
        }).subscribe((data) => {
          this.materialPlants = parseSerionetRespToData(data);
          this.fixMaterialPlants(this.materialPlants, 0, () => {
            this.material.old_version = cloneObject(this.material);
            this.loading = false;
            if (!this.material.is_active) {
              this.materialPlants.forEach((mp: MaterialPlant) => {
                mp.status = "toSave";
              });
              this.snackBar.open(this.tr("SAVING_MANDATORY_TO_RESTORE"), null, {duration: 5000});
            }
            else {
              this.materialPlants.forEach((mp: MaterialPlant) => {
                mp.status = mp.is_active ? "active" : "inactive";
              });
            }
            this.checkForBOM();
          });
        })
      }
      else {
        this.loading = false;
      }
    });
  }

  checkForBOM() {
    let BOMisCreatable = false;
    this.materialPlants.forEach((mp) => {
      if (allowsBOMCreation(mp)) {
        BOMisCreatable = true;
      }
    });
    this.setBOMAvailableForMaterial(BOMisCreatable)
  }

  checkEditMaterial() {
    if (this.material.pk) {
      if (!this.material.material_type || !this.material.material_type.pk) {
        return false;
      }
      if (!this.material.code) {
        return false;
      }
      fixValue(this.materialTypes, this.material, 'material_type');
      let matTypeConst = _getMaterialType(this.material);
      switch (matTypeConst) {
        case _MATERIAL_TYPES.MOULDSET:
        case _MATERIAL_TYPES.COMPONENT:
        case _MATERIAL_TYPES.BILLET:
        case _MATERIAL_TYPES.DECORATION:
          return true;
        case _MATERIAL_TYPES.BOTTLE:
        case _MATERIAL_TYPES.PREFORM:
        case _MATERIAL_TYPES.CLOSURE:
          if (!this.material.sub_model.shape) {
            return false;
          }
          break;
        default:
          if (!this.material.sub_model.subtype) {
            return false;
          }
          break;
      }
      return true;
    }
    else {
      return true;
    }
  }

  cloneMaterial(m: Material) {
    let mat = cloneObject(m);
    delete mat['pk'];
    delete mat['code'];
    delete mat['description'];
    this.material = mat;
    this.fixMaterial();
  }

  fixMaterial() {

    if (!this.material.material_type)
      this.material.material_type = new MaterialType();
    else
      this.handleTypeChange(true);
    if (!this.material.uom)
      this.material.uom = new MeasureUnit();
    if (!this.material.weight_uom)
      this.material.weight_uom = new MeasureUnit();
    if (!this.material.sub_model)
      this.material.sub_model = new Submaterial();
    this.fixSubMaterial();
    if (!this.material.pk) {
      this.material.valuated = true;
    }
    if (this.material.pk || this.isClonedMaterial) {
      this.loadMaterialPlants();
      fixValue(this.materialTypes, this.material, 'material_type');
      fixValue(this.uoms, this.material, 'uom');
      fixValue(this.weightUoms, this.material, 'weight_uom');
    }
    else {
      this.productionTypesService.getProductionTypes({is_active: true}).subscribe((data) => {
        this.productionTypes = parseSerionetRespToData(data);
        this.loading = false;
      });
    }
  }

  fixSubMaterial() {

    this.materialCavities = "";
    if (!this.material.sub_model.subtype) {
      if (!this.material.pk)
        this.material.sub_model.subtype = new MaterialSubType();
      else {
        this.material.sub_model.subtype = new MaterialSubType();
      }
    }
    else {
      if (!isNaN(parseInt(this.material.sub_model.subtype + "", 10))) {
        this.materialSubtypeLinkService.getMaterialSubtypeLink(parseInt(this.material.sub_model.subtype + "", 10)).subscribe((subType) => {
          this.material.sub_model.subtype = subType;
        });
      }
    }

    if (_getMaterialType(this.material) == _MATERIAL_TYPES.MOULDSET) {
      if (!isNaN(parseInt(this.material.sub_model.shape + "", 10))) {
        this.shapesService.getShape(parseInt(this.material.sub_model.shape + "", 10)).subscribe((shape) => {
          this.material.sub_model.shape = shape;
        })
      }

      // initialize active cavities
      this.cavitiesService.getCavities({
        mouldset: this.material.pk,
        plm_revision: this.material.sub_model.plm_revision
      }).subscribe((data) => {
        // Handle active cavities
        this.cavities = parseSerionetRespToData(data);
        this.cavitiesCheckboxes = [];
        for (let i = 0; i < this.cavities.length; i++) {
          let cavityCheckbox = new CavityCheckbox();
          cavityCheckbox.cavity = this.cavities[i];
          cavityCheckbox.checked = this.cavities[i].is_active;
          this.cavitiesCheckboxes.push(cavityCheckbox);
        }

        this.materialCavities = this.cavities.map((c) => {
          return c.__str__
        }).join(", ");
        if (this.materialCavities == "")
          this.materialCavities = "-"
      });

      // initialize owner
      if (!isNaN(parseInt(this.material.sub_model.owner + "", 10))) {
        this.organizationService.getOrganization(parseInt(this.material.sub_model.owner + "", 10)).subscribe((organization) => {
          this.material.sub_model.owner = organization;
        })
      } else {
        this.material.sub_model.owner = new Organization();
      }

      // initialize location
      if (!isNaN(parseInt(this.material.sub_model.location + "", 10))) {
        this.sitesService.getSite(parseInt(this.material.sub_model.location + "", 10)).subscribe((site) => {
          this.material.sub_model.location = site;
        })
      } else {
        this.material.sub_model.location = new Site();
      }

    }

    if (this.isBottlePrefClosure(this.material)) {
      if (!this.material.sub_model.market_segment) {
        this.material.sub_model.market_segment = new MarketSegmentLink();
      }
      else {
        if (!isNaN(parseInt(this.material.sub_model.market_segment + "", 10))) {
          this.segmentsService.getMarketSegmentLink(parseInt(this.material.sub_model.market_segment + "", 10)).subscribe((segment) => {
            this.material.sub_model.market_segment = segment;
            this.handleSegmentChange(true);
          })
        }
      }
      if (!this.material.sub_model.main_material)
        this.material.sub_model.main_material = new MainMaterial();
      else {
        if (!isNaN(parseInt(this.material.sub_model.main_material + "", 10))) {
          this.mainMaterialLinksService.getMainMaterialLink(parseInt(this.material.sub_model.main_material + "", 10)).subscribe((main_material) => {
            this.material.sub_model.main_material = main_material;
          })
        }
      }
      if (!this.material.sub_model.palletized)
        this.material.sub_model.palletized = false;
      if (!this.material.sub_model.shape) {
        this.material.sub_model.shape = new Shape();
      }
      else {
        if (!isNaN(parseInt(this.material.sub_model.shape + "", 10))) {
          this.shapesService.getShape(parseInt(this.material.sub_model.shape + "", 10)).subscribe((shape) => {
            this.material.sub_model.shape = shape;
          })
        }
      }
    }
  }

  fixMaterialPlants(materialPlants: MaterialPlant[], index: number, callback) {
    if (materialPlants[index]) {
      this.sitesService.getSite(materialPlants[index]['site'].pk).subscribe((site_full) => {
        if (site_full) {
          materialPlants[index]['site'] = site_full;
          materialPlants[index]['material_id'] = this.material;
          fixValue(this.productionTypes, materialPlants[index], 'production_type');
          this.legalEntityService.getLegalEntity(materialPlants[index]['site'].legalentity_ref.pk).subscribe((le) => {
            materialPlants[index]['site'].legalentity_ref = le;
            this.fixMaterialPlants(materialPlants, index + 1, callback);
          });
        }
      });
    }
    else {
      callback.call(this);
    }
  }

  handleTypeChange(noCheckCode?: boolean) {

    if (!noCheckCode) {
      this.material.code = "";
    }

    if (!this.material.pk && !this.isClonedMaterial) {
      this.material.sub_model = new Submaterial();
      this.material.sub_model.subtype = new MaterialSubType();
      this.material.sub_model.technology = new TechnologyLink();
      this.material.sub_model.market_segment = new MarketSegmentLink();
      this.material.sub_model.main_material = new MainMaterial();
      this.material.sub_model.palletized = false;
    }
    else {
      if (this.isClonedMaterial) {
        fixValue(this.materialTypes, this.material, 'material_type');
      }
    }

    this.materialSubtypeLinkService.getMaterialSubtypeLinks({
      app: this.material.material_type.content_type,
      is_active: true
    }).subscribe((data) => {
      this.materialSubTypeLinks = parseSerionetRespToData(data);
    });

    this.technologiesService.getTechnologyLinks({
      app: this.material.material_type.content_type,
      is_active: true
    }).subscribe((data) => {
      this.technologies = parseSerionetRespToData(data);
    });

    this.segmentsService.getMarketSegmentLinks({
      app: this.material.material_type.content_type,
      is_active: true
    }).subscribe((data) => {
      this.segments = parseSerionetRespToData(data);
    });

    this.mainMaterialLinksService.getMainMaterialLinks({
      app: this.material.material_type.content_type,
      is_active: true
    }).subscribe((data) => {
      this.mainMaterials = parseSerionetRespToData(data);
    });

    if (!this.material.pk && this.material.material_type.default_uom && this.material.material_type.default_uom.pk) {
      this.material.uom = cloneObject(this.material.material_type.default_uom);
      this.handleUomChange();
    }

    if (!this.material.pk) {
      this.setHasUnsavedData(true, 0);
    }
  }

  handleSubTypeChange(firstLoad?: boolean) {
    fixValue(this.materialSubTypeLinks, this.material.sub_model, 'subtype');
    if (!this.hasEditableSubtype(this.material))
      this.material.code = "";
    if (!firstLoad) {
      this.materialPlants.forEach((mp) => {
        mp.status = "toSave";
      });
      this.setHasUnsavedData(true, 0);
    }
  }

  handleUomChange(firstLoad?: boolean) {
    fixValue(this.uoms, this.material, 'uom');

    let wu = this.weightUoms.splice(0);
    this.weightUoms = [];
    setTimeout(() => {
      this.weightUoms = wu;
      if (this.material.uom.type == 'W') {
        this.material.weight_uom = null;
        this.material.weight_uom = cloneObject(this.material.uom);
        if (!firstLoad) {
          this.setHasUnsavedData(true, 0);
        }
      }
    }, 50)
  }

  handleWeightUomChange(firstLoad?: boolean) {
    fixValue(this.weightUoms, this.material, 'weight_uom');
    if (!firstLoad) {
      this.setHasUnsavedData(true, 0);
    }
  }

  handleSegmentChange(firstLoad?: boolean) {
    fixValue(this.segments, this.material.sub_model, 'market_segment');
    if (!firstLoad) {
      this.setHasUnsavedData(true, 0);
    }
  }

  handleMainMaterialChange(firstLoad?: boolean) {
    fixValue(this.mainMaterials, this.material.sub_model, 'main_material');
    if (!firstLoad) {
      this.materialPlants.forEach((mp) => {
        mp.status = "toSave";
      });
      this.setHasUnsavedData(true, 0);
    }
  }

  handleValuatedChange() {
    if (this.material.material_type.pk == 11) {
      this.checkRegrindCode();
    }
  }

  removeMaterialPlant(mp: MaterialPlant) {
    this.setHasUnsavedData(true, 0);
    mp.status = "toDelete";
  }

  checkRegrindCode() {
    //settimeout added to correct event sync
    setTimeout(() => {
      let m = this.material;
      this.codeClass = "";
      let startCode = this.matGen.getQueryCode(m);
      if (!m.valuated) {
        startCode = "N" + startCode;
      }
      if (m.code.indexOf(startCode) != 0) {
        m.code = startCode;
      }
      else {
        let nChars = 7;
        if (!m.valuated)
          nChars = 8;
        if (m.code.length > nChars) {
          m.code = m.code.substring(0, nChars);
        }
        if (m.code.length == nChars) {
          this.assignCode(m.code);
        }

      }
    }, 100)
  }

  saveMaterial() {
    if (this.getActiveMaterialPlants().length == 0) {
      this.messageService.open({
        title: this.tr("ATTENTION"),
        message: this.tr("NO_SITES_WANT_DELETE"),
        deny: () => {

        },
        confirm: () => {
          this.loadingService.show();
          this.materialsService.deleteMaterial(this.material).subscribe((response) => {
            this.loadingService.hide();
            if (response.status == 204) {
              this.router.navigate(["materials"]);
              this.setHasUnsavedData(false, 0);
            }
            else {
              this.handleError(response);
            }
          })
        }
      })
    }
    else {
      this.doSaveMaterial();
    }
  }

  updateCavities() {
    this.processCavity(this.cavitiesCheckboxes, 0, [], (failedSaves: CavityCheckbox[]) => {
      if (failedSaves.length == 0) {
        this.messageService.open({
          message: this.tr('SAVE_COMPLETED'),
          title: " ",
          autoClose: true
        });
      }
      else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.buildFailedSavesMessage(failedSaves),
          autoClose: false
        });
      }
    })
  }

  buildFailedSavesMessage(failedSaves: CavityCheckbox[]) {
    let content = [];
    content.push("<div>The following packages were not saved correctly:</div>");
    content.push("<ul>");
    failedSaves.forEach((cavityCheckbox) => {
      content.push("<li><b>" + cavityCheckbox.cavity.__str__ + "</b><br/></li>");
    });
    content.push("</ul>");
    return content.join("");
  }

  processCavity(cavitiesToSave, index, failedSaves, callback) {
    if (cavitiesToSave[index]) {
      let cavityCheckbox = cavitiesToSave[index];
      let cavity = cavityCheckbox.cavity;
      cavity.is_active = cavityCheckbox.checked;
      if (cavity.is_active) {
        this.cavitiesService.saveCavity(cavity).subscribe((resp) => {
          if (!resp.pk) {
            cavity.errorResponse = resp;
            failedSaves.push(cavity);
          }
          this.processCavity(cavitiesToSave, index + 1, failedSaves, callback);
        });
      } else {
        this.cavitiesService.deleteCavity(cavity).subscribe((resp) => {
          if (!resp.pk) {
            cavity.errorResponse = resp;
            failedSaves.push(cavity);
          }
          this.processCavity(cavitiesToSave, index + 1, failedSaves, callback);
        });
      }
    }
    else {
      callback.call(this, failedSaves);
    }
  }

  saving = false;

  doSaveMaterial() {
    this.loadingService.show();
    let material_id = cloneObject(this.material);
    this.materialPlants.forEach((mp) => {
      mp.material_id = material_id;
    });
    this.material.material_plants = this.materialPlants;

    this.saving = true;
    let matType = _getMaterialType(this.material);
    this.materialsService.saveMaterial(this.material).subscribe((response) => {
      this.saving = false;
      this.manageRespSave(response, () => {
        if (matType == _MATERIAL_TYPES.MOULDSET) {
          this.updateCavities();
        }
        this.setHasUnsavedData(false, 0);
        this.router.navigate(["materials", "materials", "detail", response.pk]);
      })
    })
  }

  isMaterialReadyToBeSaved() {
    if (this.material.pk) {
      return this.checkMaterialPlants();
    }
    else {
      if (this.codeClass != 'valid') {
        return false;
      }

      let isReady = true;

      let fieldsToCheck = [];
      let parent = this.material;
      fieldsToCheck.push({parent: parent, key: 'code'});
      fieldsToCheck.push({parent: parent, key: 'description'});
      fieldsToCheck.push({parent: parent, key: 'material_type'});
      fieldsToCheck.push({parent: parent.material_type, key: 'pk'});
      fieldsToCheck.push({parent: parent, key: 'weight'});
      fieldsToCheck.push({parent: parent, key: 'uom'});
      fieldsToCheck.push({parent: parent.uom, key: 'pk'});
      fieldsToCheck.push({parent: parent, key: 'weight_uom'});
      fieldsToCheck.push({parent: parent.weight_uom, key: 'pk'});
      if (!this.hasEditableSubtype(this.material))
        fieldsToCheck.push({parent: parent.sub_model.subtype, key: 'pk'});

      let fieldsToNormalize = [];
      fieldsToNormalize.push({parent: this.material, key: 'valuated'});
      let matTypeConstant = _getMaterialType(this.material);
      if (matTypeConstant) {
        switch (matTypeConstant) {
          case _MATERIAL_TYPES.RESIN:
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'pcr'});
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'food_approved'});
            break;
          case _MATERIAL_TYPES.ADDITIVE:
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'food_approved'});
            break;
          case _MATERIAL_TYPES.DECORATION:
            break;
          case _MATERIAL_TYPES.PACKAGING:
            break;
          case _MATERIAL_TYPES.RETURNABLE_PACKAGING:
            break;
          case _MATERIAL_TYPES.PREFORM:
          case _MATERIAL_TYPES.BOTTLE:
          case _MATERIAL_TYPES.CLOSURE:
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'pallettized'});
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'pcr'});
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'subcontracting'});
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'food_approved'});
            fieldsToCheck.push({parent: this.material.sub_model.shape, key: 'pk'});
            fieldsToCheck.push({parent: this.material.sub_model.market_segment, key: 'pk'});
            break;
          case _MATERIAL_TYPES.REGRIND:
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'food_approved'});
            break;
          case _MATERIAL_TYPES.COMPONENT:
          case _MATERIAL_TYPES.MOULDSET:
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'location'});
            fieldsToNormalize.push({parent: this.material.sub_model, key: 'owner'});
          case _MATERIAL_TYPES.BILLET:
          default:
            break;
        }
      }
      else {
        return false;
      }

      fieldsToNormalize.forEach((f) => {
        if (f['parent'][f['key']] == null)
          f['parent'][f['key']] = false;
      });

      fieldsToCheck.forEach((f) => {
        if (!f['parent'] || f['parent'][f['key']] == null || f['parent'][f['key']] == "") {
          isReady = false;
          return false;
        }
      });

      if (isReady) {
        if (this.materialPlants.length == 0) {
          isReady = false;
        }
        else {
          isReady = this.checkMaterialPlants();
        }
      }
      return isReady;
    }
  }

  checkMaterialPlants() {
    let matPlantsOk = true;
    this.materialPlants.forEach((mp) => {
      if (!mp.production_type.pk)
        matPlantsOk = false;
    });
    return matPlantsOk;
  }

  getShapes(input: string) {
    return this.shapesService.getShapes({search: input, results: 5, is_active: true})
  }

  selectedShape(event: any) {
    if (event)
      this.material.sub_model.shape = event;
    else
      this.material.sub_model.shape = new Shape();
    // this.generateCode();
  }

  generateCode(forceGeneration?: boolean) {
    if (this.material.pk || this.disableCodeGeneration)
      return;

    if (this.matGen.hasAllInfos(this.material)) {
      if (_getMaterialType(this.material) == _MATERIAL_TYPES.ASSEMBLED_GOODS) {
        forceGeneration = true;
      }
      if (this.material.valuated == false && !forceGeneration) {
        // this.loadingService.show();
        this.openDialogSiblings();
      }
      else {
        this.matGen.generate(this.material).subscribe((response) => {

          if (response.status == 0) {
            this.assignCode(response.code);
          }
          else {
            switch (response.status) {
              case 1:
                //Missing info (should never enter here)
                break;
              case 2:
                //Bottle, preform, closures: the code is been generated,
                // but can be overwritten by user choice of sibling
                if (!forceGeneration)
                  this.openPallettizedDialog(this.matGen.getQueryCode(this.material), response.code);
                else
                  this.assignCode(response.code);
                break;
              case 3:
                //4 digits have to be inserted manually
                this.enableCodeInsertForRegrind(response.code);
                break;
              default:
                break;
            }
          }
        });
      }
    }
    else {
      this.loggingService.info("Material doesn't have enough info to generate code")
    }
  }

  enableCodeInsertForRegrind(partialCode: string) {
    this.material.code = partialCode;
  }

  selectMaterialForCloning(m: Material) {
    this.unvaluatedDialog.close();

    this.disableCodeGeneration = true; //In order to prevent conflicts
    // this.cloneMaterial(m);
    this.material.valuated = false;
    this.material.description = m.description;
    this.assignCode("N" + m.code);
  }

  selectPallettizedMaterialForCloning(m: Material) {
    this.palletizedDialog.close();
    let codePartToSearch = m.code.substring((m.valuated ? 0 : 1), (m.valuated ? 8 : 9));

    let clonedMs = this.pallettizedMaterials.map((m) => {
      return cloneObject(m)
    });
    clonedMs.forEach((m) => {
      m.codeForSorting = m.code;
      if (!m.valuated) {
        m.codeForSorting = m.code.substring(1);
      }
    });
    clonedMs = clonedMs.filter((m) => {
      return m.codeForSorting.indexOf(codePartToSearch) == 0;
    });

    //Reverse ordering
    clonedMs.sort((a, b) => {
      if (b.codeForSorting > a.codeForSorting) {
        return 1;
      }
      else {
        if (b.codeForSorting == a.codeForSorting) {
          return 0;
        }
        else {
          return -1;
        }
      }
    });

    let lastMaterial = clonedMs[0];

    let codePartToIncrement = lastMaterial.codeForSorting.substring(codePartToSearch.length);
    let newNumber = parseInt(codePartToIncrement, 10) + 1;
    let newNumberString = newNumber + "";
    if (newNumber < 10) {
      newNumberString = "0" + newNumber;
    }
    let newCode = (this.material.valuated ? "" : "N") + codePartToSearch + newNumberString;
    this.assignCode(newCode);
  }

  openDialogSiblings() {
    this.codeFilter = "";
    this.descFilter = "";
    this.filterSiblings(true);
  }

  openPallettizedDialog(queryCode: string, proposedCode: string) {
    let query = {
      is_active: true,
      code_icontains: queryCode
    };
    this.materialsService.getMaterials(query).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.pallettizedMaterials = data['results'];
        let unpallettized = this.pallettizedMaterials.filter((m) => {
          return !m.sub_model.palletized;
        });
        if (unpallettized.length > 0) {
          this.palletizedDialog.open();
        }
        else {
          if (this.pallettizedMaterials.length == 0) {
            //First material for this shape
            this.material.sub_model.palletized = false;
            this.handlePallettizedChange();
            this.messageService.open({
              title: this.tr("ATTENTION"),
              message: this.tr("NO_UNPALLETIZED_MATERIALS")
            })
          }
          else {
            //Error: there are materials for this shape but all of them are palletized
            this.material.sub_model.palletized = false;
            this.handlePallettizedChange();
            this.messageService.open({
              error: true,
              title: this.tr("ATTENTION"),
              message: this.tr("ONLY_PALLETIZED_MATERIALS")
            })
          }

        }

      }
      else {
        //There are no siblings

        //If the material is not regrind I ask for a new code

        this.assignCode(proposedCode);
      }
    })

  }

  assignCode(code) {
    this.disableCodeGeneration = true;
    this.material.code = code;
    this.codeClass = "checking";
    this.materialsService.isCodeAvailable(this.material.code).subscribe((isAvailable) => {
      this.disableCodeGeneration = false;
      if (isAvailable) {
        this.codeClass = "valid";
      }
      else {
        this.codeClass = "error";
      }
    });
  }

  codeFilter: string;
  descFilter: string;
  filterTimeout: any;

  filterSiblings(firstQuery?: boolean) {

    if (this.filterTimeout)
      clearTimeout(this.filterTimeout);

    this.filterTimeout = setTimeout(() => {
      let queryCode = this.matGen.getQueryCode(this.material);
      let query = {
        material_type: this.material.material_type.pk,
        is_active: true,
        code_startswith: queryCode,
        code_icontains: this.codeFilter,
        description_icontainer: this.descFilter,
        valuated: true,
        results: 25
      };
      this.loadingService.show();
      this.materialsService.getMaterials(query).subscribe((data) => {
        this.loadingService.hide();
        if (data && data['count'] && data['count'] > 0) {
          this.siblingMaterials = data['results'];
          if (firstQuery)
            this.unvaluatedDialog.open();
        }
        else {
          this.siblingMaterials = [];
          //If that's the first query and no results are given, there's no
          //valuated material to clone, the code needs to be generated
          if (firstQuery) {
            this.generateCode(true);
          }
        }
      })
    }, 200)
  }

  handlePallettizedChange() {
    // this.generateCode();
  }

  isBottlePrefClosure(m: Material) {
    return isBottleClosurePreform(m);
  }

  isPlmField(m: Material) {
    let matType = this.getMaterialType(m);

    switch (matType) {
      case _MATERIAL_TYPES.MOULDSET:
        return true;
      case _MATERIAL_TYPES.COMPONENT:
        return true;
      case _MATERIAL_TYPES.BILLET:
        return true;
      default:
        return false;
    }
  }

  getMaterialType(m: Material) {
    return _getMaterialType(m);
  }

  hasEditableSubtype(material: Material) {
    // return isBottleClosurePreform(material)||getMaterialType(material)==MATERIAL_TYPES.MOULDSET||getMaterialType(material)==MATERIAL_TYPES.COMPONENT||getMaterialType(material)==MATERIAL_TYPES.BILLET
    return isBottleClosurePreform(material) || _getMaterialType(material) == _MATERIAL_TYPES.DECORATION || _getMaterialType(material) == _MATERIAL_TYPES.ASSEMBLED_GOODS || !this.isPlmField(material);
  }

  handlePCRChange() {
    this.setHasUnsavedData(true, 0);
  }

  handleFoodApprovedChange() {
    this.setHasUnsavedData(true, 0);
  }

  handleConstructionYearChange() {
    this.setHasUnsavedData(true, 0);
  }

  handleWeightChange() {
    this.setHasUnsavedData(true, 0);
  }

  getSites(input: string) {
    return this.sitesService.getSites({name_icontains: input, results: 5, is_active: true});
  }

  selectedLocation(event: any) {
    if (event)
      this.material.sub_model.location = event;
    else
      this.material.sub_model.location = new Site();
  }

  selectedOwner(event: any) {
    if (event)
      this.material.sub_model.owner = event;
    else
      this.material.sub_model.owner = new Organization();
  }

  getSitesToAdd(keyword: string) {
    return this.sitesService.getSites({name_icontains: keyword, results: 5, is_active: true});
  }

  openCloneDialog() {
    this.cloneDialog.open();
    this.filterClones();
  }

  cloneTimeout: any;

  filterClones() {
    if (this.cloneTimeout)
      clearTimeout(this.cloneTimeout);

    this.cloneTimeout = setTimeout(() => {
      this.materialsService.getMaterials({
        search: this.codeDescFilter,
        results: 10,
        is_active: true
      }).subscribe((data) => {
        this.clonableMaterials = parseSerionetRespToData(data);
      });
    }, 500)
  }

  selectMaterialToClone(m: Material) {
    this.cloneDialog.close();
    this.isClonedMaterial = true;
    this.cloneMaterial(m);
  }

  openCommodityDialog(mp: MaterialPlant) {
    this.activeMP = mp;
    this.activeMP.disableCommodityCode = false;
    this.ccDescFilter = "";
    this.ccCodeFilter = "";
    this.disableShowAllButton = false;
    this.loadingService.show();
    this.commodityCodesService.getCommodityCodes({
      used: true,
      country: this.activeMP.site.legalentity_ref.country
    }).subscribe((response) => {
      this.loadingService.hide();
      if (response.status == 200) {
        this.commodityCodes = response['response']['results'];
        if (this.commodityCodes.length == 0) {
          this.showAllCommodityCodes();
        }
        else {
          this.commodityCodesFiltered = cloneObject(this.commodityCodes);
          this.commodityDialog.open();
        }
      }
      else {
        this.commodityCodes = [];
        this.commodityCodesFiltered = cloneObject(this.commodityCodes);
        //If no results are available, automatically the full list is requested
        this.showAllCommodityCodes();
      }
    })
  }

  showAllCommodityCodes() {
    this.ccDescFilter = "";
    this.ccCodeFilter = "";
    this.loadingService.show();
    this.commodityCodesService.getCommodityCodes({country: this.activeMP.site.legalentity_ref.country}).subscribe((response) => {
      this.loadingService.hide();
      if (response.status == 200) {
        this.commodityCodes = response['response']['results'];
        if (this.commodityCodes.length == 0) {
          this.commodityDialog.close();
          this.activeMP.disableCommodityCode = true;
          this.snackBar.open(this.tr("NO_COMMODITY_CODES_AVAILABLE"), null, {duration: 5000});
        }
        else {
          this.commodityCodesFiltered = cloneObject(this.commodityCodes);
          this.disableShowAllButton = true;
          this.commodityDialog.open();
        }
      }
      else {
        this.commodityCodes = [];
        this.commodityCodesFiltered = cloneObject(this.commodityCodes);
        //If the full list is empty. No commodity codes are available for this site, the field is disabled
        this.commodityDialog.close();
        this.activeMP.disableCommodityCode = true;
        this.snackBar.open(this.tr("NO_COMMODITY_CODES_AVAILABLE"), null, {duration: 5000});
      }
    });
  }

  selectCommodityCode(c: any, mp: MaterialPlant) {
    this.commodityDialog.close();
    mp.commodity_code = c.Code;
    this.handleChangeMP(mp);
  }

  filterCommodityCodes() {
    this.commodityCodesFiltered = this.commodityCodes.filter((cc) => {
      let isValid = true;
      if (this.ccCodeFilter) {
        isValid = cc['Code'].indexOf(this.ccCodeFilter) != -1;
      }
      if (isValid && this.ccDescFilter) {
        isValid = cc['Description'].toLowerCase().indexOf(this.ccDescFilter.toLowerCase()) != -1;
      }
      return isValid;
    })
  }

  handleProductionTypeChange(mp: MaterialPlant) {
    fixValue(this.productionTypes, mp, 'production_type');
    this.handleChangeMP(mp);
  }

  handleChangeMP(mp: MaterialPlant) {
    this.setHasUnsavedData(true, 0);
    mp.status = "toSave";
  }

  clearCommodityCode(mp: MaterialPlant) {
    mp.commodity_code = "";
    mp.disableCommodityCode = false;
  }

  createMaterialPlant() {
    let mp = new MaterialPlant();
    mp.site = cloneObject(this.siteToAdd);

    this.siteToAdd = null;

    mp.material_id = this.material;
    mp.production_type = new ProductionType();
    mp.mes = false;
    mp.inspection_stock = false;

    this.legalEntityService.getLegalEntity(mp.site.legalentity_ref.pk).subscribe((le) => {
      mp.site.legalentity_ref = le;
      mp.status = "toSave";
      this.materialPlants.push(mp);
      this.setHasUnsavedData(true, 0);
      if (!mp.site.legalentity_ref.country) {
        this.snackBar.open(this.tr("NO_COUNTRY_WARNING"), null, {duration: 5000});
      }
    });
  }

  selectSite(s: Site) {
    if (s && s.pk) {
      if (this.siteIsNotUsed(s)) {
        this.siteToAdd = s;
      }
      else {
        this.snackBar.open(this.tr("PLANT_ALREADY_SELECTED"), null, {duration: 5000});
      }
    }
    else {
      this.siteToAdd = null;
    }

  }

  siteIsNotUsed(s: Site) {
    let sitePK = s.pk;
    let sitesPK = this.materialPlants.map((mp) => {
      return mp.site.pk;
    });
    return (sitesPK.indexOf(sitePK) == -1);
  }

  handleFlagChange(mp: MaterialPlant) {
    this.handleChangeMP(mp);
  }

  handleError(resp) {
    this.loadingService.hide();
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  restoreMaterialPlant(mp: MaterialPlant) {
    mp.status = "toSave";
    mp.is_active = true;
  }

  getActiveMaterialPlants() {
    return this.materialPlants.filter((mp) => {
      return mp.status == "toSave" || mp.status == "active";
    })
  }

  getInactiveMaterialPlants() {
    return this.materialPlants.filter((mp) => {
      return mp.status == "toDelete" || mp.status == "inactive";
    })
  }
}
