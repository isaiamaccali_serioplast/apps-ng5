"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var utils_1 = require("../../../../../shared/utils");
var package_instruction_1 = require("../../../../_common/models/package-instruction");
var package_instruction_item_1 = require("../../../../_common/models/package-instruction-item");
var package_instruction_location_1 = require("../../../../_common/models/package-instruction-location");
var MaterialEditPackageInstructionsComponent = (function () {
    function MaterialEditPackageInstructionsComponent(materialTypesService, materialsService, measureUnitService, messageService, translationService, loadingService, packageInstructionsService, snackBar, materialPlantsService, packageInstrPlantsService, sitesService) {
        this.materialTypesService = materialTypesService;
        this.materialsService = materialsService;
        this.measureUnitService = measureUnitService;
        this.messageService = messageService;
        this.translationService = translationService;
        this.loadingService = loadingService;
        this.packageInstructionsService = packageInstructionsService;
        this.snackBar = snackBar;
        this.materialPlantsService = materialPlantsService;
        this.packageInstrPlantsService = packageInstrPlantsService;
        this.sitesService = sitesService;
        this.pi = [];
        this.p = new package_instruction_1.PackageInstruction();
        this.packageItem = new package_instruction_item_1.PackageInstructionItem();
        this.piActive = true;
        this.loading = false;
        this.matPlants = [];
    }
    MaterialEditPackageInstructionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.materialTypes = [];
        this.materials = [];
        this.materialTypesService.getMaterialTypes({ first_char: 'K' }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.materialTypes = data['results'];
            }
        });
        var self = this;
        this.packageItem.main_package = false;
        this.materialPlantsService.getMaterialPlants({ is_active: true, material_id: this.material.pk }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.matPlants = data['results'];
                _this.fixMaterialPlants(_this.matPlants, function () {
                    self.loadPackageInstructions();
                });
            }
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.loadPackageInstructions = function () {
        var _this = this;
        var query = {};
        query['is_active'] = this.piActive;
        query['material_id'] = this.material.pk;
        this.pi = [];
        this.loading = true;
        this.packageInstructionsService.getPackageInstructions(query).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                var pi_1 = data['results'];
                var self_1 = _this;
                _this.fixPackageInstructions(pi_1, function () {
                    self_1.pi = pi_1;
                    self_1.loading = false;
                });
            }
            else {
                _this.loading = false;
            }
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.fixPackageInstructions = function (pi, callback) {
        this.fixPackageInstruction(pi, 0, callback);
    };
    MaterialEditPackageInstructionsComponent.prototype.fixPackageInstruction = function (pi, index, callback) {
        if (pi[index]) {
            var self_2 = this;
            this.fixPackageItems(pi[index].packinstructionitem_set, 0, function () {
                var _this = this;
                var main_index = 0;
                pi[index].packinstructionitem_set.forEach(function (pii, i) {
                    if (pii.main_package)
                        main_index = i;
                });
                pi[index].mainPackage = main_index;
                pi[index].packinstructionitem_set.sort(function (a, b) {
                    if ((a.material.pk == self_2.material.pk) && (b.material.pk != self_2.material.pk)) {
                        return 1;
                    }
                    else {
                        if ((b.material.pk == self_2.material.pk) && (a.material.pk != self_2.material.pk)) {
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    }
                });
                self_2.packageInstrPlantsService.getPackageInstructionLocations({ packing_instruction: pi[index].pk }).subscribe(function (data) {
                    if (data && data['count'] && data['count'] > 0) {
                        pi[index].plants = _this.processLocations(pi[index], data['results']);
                        self_2.fixPackageInstruction(pi, index + 1, callback);
                    }
                    else {
                    }
                });
            });
        }
        else {
            callback();
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.fixPackageItems = function (pis, index, callback) {
        var _this = this;
        if (pis[index]) {
            var self_3 = this;
            var pis_1 = pis[index];
            this.materialsService.getMaterial(pis_1.material).subscribe(function (material) {
                pis[index].material = material;
                _this.measureUnitService.getMeasureUnit(parseInt(pis_1.uom + "", 10)).subscribe(function (uom) {
                    pis[index].uom = uom;
                    self_3.fixPackageItems(pis, index + 1, callback);
                });
            });
        }
        else {
            callback();
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.fixMaterialPlants = function (mp, callback) {
        this.fixMaterialPlant(mp, 0, callback);
    };
    MaterialEditPackageInstructionsComponent.prototype.fixMaterialPlant = function (mp, index, callback) {
        var _this = this;
        if (mp[index]) {
            var self_4 = this;
            mp[index].material_id = utils_1.cloneObject(this.material);
            this.sitesService.getSite(mp[index].site.pk).subscribe(function (resp) {
                mp[index].site = resp;
                _this.fixMaterialPlant(mp, index + 1, callback);
            });
        }
        else {
            callback();
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.searchMaterials = function (keyword) {
        var _this = this;
        this.materialsService.getMaterials({
            search: keyword,
            results: 5,
            material_type: this.materialType
        }).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.materials = data['results'];
            else
                _this.materials = [];
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.selectedMaterial = function (event) {
        if (event != null) {
            this.packageItem.material = event;
            this.packageItem.uom = event.uom;
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.handleTypeChange = function () {
        //this.initNewBOMItem();
    };
    MaterialEditPackageInstructionsComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    MaterialEditPackageInstructionsComponent.prototype.reload = function () {
        this.ngOnInit();
    };
    MaterialEditPackageInstructionsComponent.prototype.handleSaveOrUpdate = function (resp, callback, disableMessage) {
        var self = this;
        if (resp.pk) {
            if (!disableMessage) {
                self.messageService.open({
                    message: self.tr('SAVE_COMPLETED'),
                    title: " ",
                    autoClose: false,
                    onClose: function () {
                        if (callback)
                            callback();
                    }
                });
            }
            else {
                if (callback)
                    callback();
            }
        }
        else {
            if (!resp.context)
                resp.context = "SERIONET";
            this.handleError(resp);
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.handleDelete = function (resp, callback) {
        var self = this;
        if (resp.status == 204) {
            self.messageService.open({
                title: " ",
                message: self.tr('DELETE_COMPLETED'),
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.handleError = function (resp) {
        this.loadingService.hide();
        this.messageService.open({
            title: this.tr('ERROR'),
            error: true,
            message: utils_1.buildErrorMessage(resp),
            autoClose: false
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.deletePI = function (p) {
        if (p.pk) {
            var self_5 = this;
            this.messageService.open({
                title: self_5.tr('CONFIRM'),
                message: self_5.tr('CONFIRM_DELETE'),
                confirm: function () {
                    self_5.loadingService.show();
                    self_5.packageInstructionsService.deletePackageInstruction(p).subscribe(function (response) {
                        self_5.loadingService.hide();
                        self_5.handleDelete(response, function () {
                            var indexToDelete = null;
                            self_5.pi.forEach(function (element, index) {
                                if (element.pk == p.pk) {
                                    indexToDelete = index;
                                }
                            });
                            if (indexToDelete != null) {
                                self_5.pi.splice(indexToDelete, 1);
                            }
                        });
                    });
                },
                deny: function () {
                }
            });
        }
        else {
            var indexToDelete_1 = null;
            this.pi.forEach(function (element, index) {
                if (element.pk == p.pk) {
                    indexToDelete_1 = index;
                }
            });
            if (indexToDelete_1 != null) {
                this.pi.splice(indexToDelete_1, 1);
            }
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.restorePI = function (p) {
        var self = this;
        this.messageService.open({
            title: self.tr('CONFIRM'),
            message: self.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.loadingService.show();
                self.packageInstructionsService.restorePackageInstruction(p).subscribe(function (response) {
                    self.loadingService.hide();
                    self.handleSaveOrUpdate(response, function () {
                        self.piActive = true;
                    });
                });
            },
            deny: function () {
            }
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.enableAddPackageItem = function (p) {
        this.disableAllPIs();
        p.enable_add_item = true;
    };
    MaterialEditPackageInstructionsComponent.prototype.disableAddPackageItem = function (p) {
        p.enable_add_item = false;
    };
    MaterialEditPackageInstructionsComponent.prototype.addPackageItem = function (p) {
        p.enable_add_item = false;
        var pToAdd = utils_1.cloneObject(this.packageItem);
        if (!pToAdd.pk) {
            p.packinstructionitem_set.push(pToAdd);
        }
        else {
            p.packinstructionitem_set.forEach((function (element, index) {
                if (pToAdd.pk == element.pk) {
                    p.packinstructionitem_set[index] = pToAdd;
                }
            }));
        }
        this.packageItem = new package_instruction_item_1.PackageInstructionItem();
        this.packageItem.main_package = false;
        var self = this;
        p.packinstructionitem_set.sort(function (a, b) {
            if ((a.material.pk == self.material.pk) && (b.material.pk != self.material.pk)) {
                return 1;
            }
            else {
                if ((b.material.pk == self.material.pk) && (a.material.pk != self.material.pk)) {
                    return -1;
                }
                else {
                    return 0;
                }
            }
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.savePackageInstruction = function (p) {
        var _this = this;
        var self = this;
        this.loadingService.show();
        p.packinstructionitem_set.forEach(function (pii, index) {
            pii.item_nr = index;
        });
        this.packageInstructionsService.savePackageInstruction(p).subscribe(function (data) {
            _this.loadingService.hide();
            _this.handleSaveOrUpdate(data, function () {
                self.loadPackageInstructions();
            });
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.createPackageInstruction = function () {
        this.p = new package_instruction_1.PackageInstruction();
        this.p.packinstructionitem_set = [];
        var defaultPackageItem = new package_instruction_item_1.PackageInstructionItem();
        defaultPackageItem.material = this.material;
        defaultPackageItem.uom = this.material.uom;
        defaultPackageItem.pk = (new Date()).getTime();
        defaultPackageItem.main_package = false;
        defaultPackageItem.main_material = true;
        this.p.material_id = this.material;
        this.p.code = "";
        this.p.is_active = true;
        this.pi.push(this.p);
        this.enableAddPackageItem(this.p);
        this.packageItem = defaultPackageItem;
        this.p.packinstructionitem_set.push(defaultPackageItem);
    };
    MaterialEditPackageInstructionsComponent.prototype.editPackageItem = function (p, pi) {
        this.enableAddPackageItem(p);
        if (!pi.pk) {
            //Fake, just to have the same form of original bom items also for new added items
            pi.pk = (new Date()).getTime();
        }
        this.packageItem = utils_1.cloneObject(pi);
    };
    MaterialEditPackageInstructionsComponent.prototype.deletePackageItem = function (p, pi) {
        p.packinstructionitem_set.forEach((function (pi_, index) {
            if (pi.material.pk == pi_.material.pk) {
                p.packinstructionitem_set.splice(index, 1);
            }
        }));
        if (p.packinstructionitem_set.length > 1) {
            p.needsSaving = true;
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.disableAllPIs = function () {
        this.pi.forEach(function (p) {
            p.enable_add_item = false;
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.isPackageInstructionReadyToBeSaved = function (p) {
        return p.packinstructionitem_set.length > 1 && this.checkMainPackage(p) == true && p.code && p.description;
    };
    MaterialEditPackageInstructionsComponent.prototype.checkMainPackage = function (p) {
        var mainPackages = p.packinstructionitem_set.filter(function (pii) {
            return pii.main_package;
        });
        if (mainPackages.length == 1) {
            return true;
        }
        else {
            if (mainPackages.length == 0) {
                return this.tr("SELECT_MAIN_PACKAGE");
            }
            else {
                return this.tr("MORE_THAN_ONE_MAIN_PACKAGE");
            }
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.isPackageItemReady = function (pii) {
        return pii.material && pii.material.pk && pii.quantity;
    };
    MaterialEditPackageInstructionsComponent.prototype.isMainMaterial = function (pii) {
        if (pii.main_material) {
            return true;
        }
        else {
            pii.main_material = (pii.material.pk == this.material.pk);
            return pii.main_material;
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.handleMainPackageChange = function (p, packageItem) {
        if (packageItem.main_package) {
            p.packinstructionitem_set.forEach(function (pi) {
                pi.main_package = false;
            });
            packageItem.main_package = true;
            packageItem.quantity = 1;
        }
    };
    MaterialEditPackageInstructionsComponent.prototype.handleCodeBlur = function (p) {
        var _this = this;
        var code = p.code;
        this.packageInstructionsService.getPackageInstructions({ code: code.toUpperCase(), material_id: this.material.pk }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                p.code = "";
                _this.snackBar.open(_this.tr("CODE_ALREADY_IN USE"), null, { duration: 5000 });
            }
            else {
                p.code = p.code.toUpperCase();
            }
        });
    };
    MaterialEditPackageInstructionsComponent.prototype.processLocations = function (pi, pils) {
        var matPlantsPKS = this.matPlants.map(function (mp) {
            return mp.pk;
        });
        var plantsToAssign = [];
        this.matPlants.forEach(function (mp) {
            var pil = new package_instruction_location_1.PackageInstructionLocation();
            var filteredPIL = pils.filter(function (pil) {
                return pil.material_plant.pk == mp.pk;
            });
            if (filteredPIL.length > 0) {
                pil = filteredPIL[0];
            }
            else {
                pil.material_plant = mp;
                pil.pack_instruct = pi;
                pil.is_active = false;
            }
            plantsToAssign.push(pil);
        });
        var orphansPIL = pils.filter(function (pil) {
            return matPlantsPKS.indexOf(pil.material_plant.pk) == -1;
        });
        if (orphansPIL.length > 0) {
            orphansPIL.forEach(function (pil) {
                pil.is_orphan = true;
            });
            plantsToAssign = plantsToAssign.concat(orphansPIL);
        }
        return plantsToAssign;
    };
    return MaterialEditPackageInstructionsComponent;
}());
__decorate([
    core_1.Input()
], MaterialEditPackageInstructionsComponent.prototype, "material", void 0);
__decorate([
    core_1.Input()
], MaterialEditPackageInstructionsComponent.prototype, "editTab", void 0);
__decorate([
    core_1.Input()
], MaterialEditPackageInstructionsComponent.prototype, "saveTab", void 0);
__decorate([
    core_1.Input()
], MaterialEditPackageInstructionsComponent.prototype, "setHasUnsavedData", void 0);
MaterialEditPackageInstructionsComponent = __decorate([
    core_1.Component({
        selector: 'app-material-edit-package-instructions',
        templateUrl: 'material-edit-packing-instructions.html',
        styleUrls: ['material-edit-packing-instructions.css']
    })
], MaterialEditPackageInstructionsComponent);
exports.MaterialEditPackageInstructionsComponent = MaterialEditPackageInstructionsComponent;
