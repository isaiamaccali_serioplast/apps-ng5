import {Component, Input, OnInit} from "@angular/core";
import {buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {Material, MaterialsService} from "../../../../../_common/services/materials.service";
import {MeasureUnitService} from "../../../../../_common/services/measure-unit.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {PackingInstruction, PackingInstructionItem, PackingInstructionsService} from "../../../../../_common/services/packing-instructions.service";
import {MatSnackBar} from "@angular/material";
import {MaterialPlant, MaterialPlantsService} from "../../../../../_common/services/material-plant.service";
import {PackingInstructionLocation, PackingInstructionsLocationService} from "../../../../../_common/services/packing-instructions-plant.service";
import {SitesService} from "../../../../../_common/services/sites.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-material-edit-packing-instructions',
  templateUrl: 'material-edit-packing-instructions.html',
  styleUrls: ['material-edit-packing-instructions.css']
})
export class MaterialEditPackageInstructionsComponent extends BaseComponent implements OnInit {

  @Input() material: Material;
  @Input() editTab: any;
  @Input() saveTab: any;
  @Input() setHasUnsavedData: any;

  // materialType: number;
  // materialTypes: MaterialType[];

  // materials: Material[];
  pi: PackingInstruction[] = [];
  p: PackingInstruction = new PackingInstruction();
  packageItem: PackingInstructionItem = new PackingInstructionItem();

  piActive = true;
  loading = false;
  matPlants: MaterialPlant[] = [];

  additionalQueryTypes = {first_char: 'K'};

  constructor(private materialsService: MaterialsService,
              private measureUnitService: MeasureUnitService, protected messageService: MessageService,
              protected translationService: TranslationService, protected loadingService: LoadingService,
              private packingInstructionsService: PackingInstructionsService, private snackBar: MatSnackBar,
              private materialPlantsService: MaterialPlantsService, private packingInstrPlantsService: PackingInstructionsLocationService,
              private sitesService: SitesService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    // this.materialTypes = [];
    // this.materials = [];
    // this.materialTypesService.getMaterialTypes({first_char: 'K', is_active: true}).subscribe((data) => {
    //   this.materialTypes = parseSerionetRespToData(data);
    //   if (this.materialTypes.length > 0)
    //     this.materialType = this.materialTypes[0].pk;
    // });

    this.packageItem.main_package = false;
    this.materialPlantsService.getMaterialPlants({is_active: true, material_id: this.material.pk}).subscribe((data) => {
      this.matPlants = parseSerionetRespToData(data);
      this.fixMaterialPlants(this.matPlants, () => {
        this.loadPackingInstructions();
      })
    })
  }

  loadPackingInstructions() {
    let query = {};
    query['is_active'] = this.piActive;
    query['material_id'] = this.material.pk;
    this.pi = [];
    this.loading = true;
    this.packingInstructionsService.getPackingInstructions(query).subscribe((data) => {
      let pi_ = parseSerionetRespToData(data);
      this.fixPackingInstructions(pi_, () => {
        this.pi = pi_;
        this.loading = false;
      })
    })
  }

  fixPackingInstructions(pi: PackingInstruction[], callback?: any) {
    this.fixPackingInstruction(pi, 0, callback);
  }

  fixPackingInstruction(pi: PackingInstruction[], index: number, callback?: any) {
    if (pi[index]) {
      this.fixPackingItems(pi[index].packinstructionitem_set, 0, () => {
        let main_index = 0;
        pi[index].packinstructionitem_set.forEach((pii, i) => {
          if (pii.main_package)
            main_index = i;
        });
        pi[index].mainPackage = main_index;
        pi[index].packinstructionitem_set.sort((a, b) => {
          if ((a.material.pk == this.material.pk) && (b.material.pk != this.material.pk)) {
            return 1;
          }
          else {
            if ((b.material.pk == this.material.pk) && (a.material.pk != this.material.pk)) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
        this.packingInstrPlantsService.getPackingInstructionLocations({
          pack_instruct__id: pi[index].pk,
          is_active: true
        }).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            pi[index].plants = this.processLocations(pi[index], data['results']);
            this.fixPackingInstruction(pi, index + 1, callback);
          }
          else {
            //casistica impossibile
            pi[index].plants = this.processLocations(pi[index], []);
            this.fixPackingInstruction(pi, index + 1, callback);
          }
        });

      })
    }
    else {
      callback();
    }
  }

  fixPackingItems(pis: PackingInstructionItem[], index: number, callback?: any) {
    if (pis[index]) {
      let pis_ = pis[index];
      this.materialsService.getMaterial(pis_.material).subscribe((material) => {
        pis[index].material = material;
        this.measureUnitService.getMeasureUnit(parseInt(pis_.uom + "", 10)).subscribe((uom) => {
          pis[index].uom = uom;
          this.fixPackingItems(pis, index + 1, callback);
        })
      })
    }
    else {
      callback();
    }
  }

  fixMaterialPlants(mp: MaterialPlant[], callback?: any) {
    this.fixMaterialPlant(mp, 0, callback);
  }

  fixMaterialPlant(mp: MaterialPlant[], index: number, callback?: any) {
    if (mp[index]) {
      mp[index].material_id = cloneObject(this.material);
      this.sitesService.getSite(mp[index].site.pk).subscribe((resp) => {
        mp[index].site = resp;
        this.fixMaterialPlant(mp, index + 1, callback);
      })
    }
    else {
      callback();
    }
  }

  getSiteList() {
    let siteList = this.p.plants.filter((pil) => {
      return pil.is_active
    }).map((pil) => {
      return pil.material_plant.site.pk
    }).join("|");
    return siteList;
  }

  selectedMaterial(event: any) {
    if (event != null) {
      this.packageItem.material = event;
      this.packageItem.uom = event.uom;
    }
  }

  handleTypeChange() {
    //this.initNewBOMItem();
  }

  reload() {
    this.ngOnInit();
  }

  handleError(resp) {
    this.loadingService.hide();
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  deletePI(p: PackingInstruction) {
    if (p.pk) {
      this.messageService.open({
        title: this.tr('CONFIRM'),
        message: this.tr('CONFIRM_DELETE'),
        confirm: () => {
          this.loadingService.show();
          this.packingInstructionsService.deletePackageInstruction(p).subscribe((response) => {
            this.loadingService.hide();
            this.manageRespDelete(response, () => {
              let indexToDelete = null;
              this.pi.forEach((element, index) => {
                if (element.pk == p.pk) {
                  indexToDelete = index;
                }
              });
              if (indexToDelete != null) {
                this.pi.splice(indexToDelete, 1);
              }
            });
          })
        },
        deny: () => {

        }
      })
    }
    else {
      let indexToDelete = null;
      this.pi.forEach((element, index) => {
        if (element.pk == p.pk) {
          indexToDelete = index;
        }
      });
      if (indexToDelete != null) {
        this.pi.splice(indexToDelete, 1);
      }
    }
  }

  restorePI(p: PackingInstruction) {
    this.messageService.open({
      title: this.tr('CONFIRM'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.loadingService.show();
        this.packingInstructionsService.restorePackageInstruction(p).subscribe((response) => {
          this.loadingService.hide();
          this.manageRespSave(response, () => {
            this.piActive = true;
            this.messageService.open({
              title: " ",
              message: "IMPORTANT: select plants in restored packing instruction",
            });
          });
        })
      },
      deny: () => {
      }
    })
  }

  enableAddPackageItem(p: PackingInstruction) {
    this.disableAllPIs();
    this.p = p;
    this.packageItem = new PackingInstructionItem();
    this.packageItem.main_package = false;
    p.enable_add_item = true;
  }

  disableAddPackageItem(p: PackingInstruction) {
    p.enable_add_item = false;
    this.packageItem = new PackingInstructionItem();
  }

  addPackageItem(p: PackingInstruction) {
    p.enable_add_item = false;
    let pToAdd = cloneObject(this.packageItem);

    if (!pToAdd.pk) {
      p.packinstructionitem_set.push(pToAdd);
    }
    else {
      p.packinstructionitem_set.forEach(((element, index) => {
        if (pToAdd.pk == element.pk) {
          p.packinstructionitem_set[index] = pToAdd;
        }
      }))
    }

    this.packageItem = new PackingInstructionItem();
    this.packageItem.main_package = false;
    p.packinstructionitem_set.sort((a, b) => {
      if ((a.material.pk == this.material.pk) && (b.material.pk != this.material.pk)) {
        return 1;
      }
      else {
        if ((b.material.pk == this.material.pk) && (a.material.pk != this.material.pk)) {
          return -1;
        }
        else {
          return 0;
        }
      }
    });
  }

  savePackageInstruction(p: PackingInstruction) {
    this.loadingService.show();
    p.packinstructionitem_set.forEach((pii, index) => {
      pii.item_nr = index;
    });

    this.packingInstructionsService.savePackageInstruction(p).subscribe((data) => {
      this.loadingService.hide();
      this.manageRespSave(data, () => {
        this.loadPackingInstructions();
      })
    })
  }

  createPackageInstruction() {
    this.p = new PackingInstruction();
    this.p.packinstructionitem_set = [];
    let defaultPackageItem = new PackingInstructionItem();
    defaultPackageItem.material = this.material;
    defaultPackageItem.uom = this.material.uom;
    defaultPackageItem.pk = (new Date()).getTime();
    defaultPackageItem.main_package = false;
    defaultPackageItem.main_material = true;
    this.p.material_id = this.material;
    this.p.code = "";
    this.p.is_active = true;
    this.pi.push(this.p);
    this.enableAddPackageItem(this.p);
    this.packageItem = defaultPackageItem;
    this.p.packinstructionitem_set.push(defaultPackageItem);
    this.p.plants = this.processLocations(this.p, []);
  }

  editPackageItem(p: PackingInstruction, pi: PackingInstructionItem) {
    this.enableAddPackageItem(p);
    this.p = p;
    if (!pi.pk) {
      //Fake, just to have the same form of original bom items also for new added items
      pi.pk = (new Date()).getTime();
    }
    this.packageItem = cloneObject(pi);

  }

  deletePackageItem(p: PackingInstruction, pi: PackingInstructionItem) {
    p.packinstructionitem_set.forEach((pi_, index) => {
      if (pi.material.pk == pi_.material.pk) {
        p.packinstructionitem_set.splice(index, 1);
      }
    });
    if (p.packinstructionitem_set.length > 1) {
      p.needsSaving = true;
    }
  }

  disableAllPIs() {
    this.pi.forEach((p) => {
      p.enable_add_item = false;
    })
  }

  isPackageInstructionReadyToBeSaved(p: PackingInstruction) {
    return p.packinstructionitem_set.length > 1 && this.checkMainPackage(p) == true && p.code && p.description && this.checkSelectedPlant(p) == true;
  }

  checkSelectedPlant(p: PackingInstruction) {
    return p.plants.filter((pil) => {
      return pil.is_active
    }).length > 0
  }

  checkMainPackage(p: PackingInstruction) {
    let mainPackages = p.packinstructionitem_set.filter((pii) => {
      return pii.main_package;
    });
    if (mainPackages.length == 1) {
      return true;
    }
    else {
      if (mainPackages.length == 0) {
        return this.tr("SELECT_MAIN_PACKAGE");
      } else {
        return this.tr("MORE_THAN_ONE_MAIN_PACKAGE");
      }
    }
  }

  isPackageItemReady(pii: PackingInstructionItem) {
    return pii.material && pii.material.pk && pii.quantity;
  }

  isMainMaterial(pii: PackingInstructionItem) {
    if (pii.main_material) {
      return true;
    }
    else {
      pii.main_material = (pii.material.pk == this.material.pk);
      return pii.main_material;
    }
  }

  handleMainPackageChange(p: PackingInstruction, packageItem: PackingInstructionItem) {
    if (packageItem.main_package) {
      p.packinstructionitem_set.forEach((pi: PackingInstructionItem) => {
        pi.main_package = false;
      });
      packageItem.main_package = true;
      packageItem.quantity = 1;
    }
  }

  handleCodeBlur(p: PackingInstruction) {
    let code = p.code;
    this.packingInstructionsService.getPackingInstructions({
      code: code.toUpperCase(),
      material_id: this.material.pk
    }).subscribe((data) => {
      if (data && data['count'] && data ['count'] > 0) {
        p.code = "";
        this.snackBar.open(this.tr("CODE_ALREADY_IN USE"), null, {duration: 5000});
      }
      else {
        p.code = p.code.toUpperCase();
      }
    });
  }

  processLocations(pi: PackingInstruction, pils: PackingInstructionLocation[]) {
    let matPlantsPKS = this.matPlants.map((mp) => {
      return mp.pk;
    });

    let plantsToAssign = [];

    this.matPlants.forEach((mp) => {
      let pil = new PackingInstructionLocation();
      let filteredPIL = pils.filter((pil) => {
        return pil.material_plant.pk == mp.pk;
      });
      if (filteredPIL.length > 0) {
        pil = filteredPIL[0];
        pil.material_plant = mp;
      }
      else {
        pil.material_plant = mp;
        pil.pack_instruct = pi;
        pil.is_active = false;
      }
      plantsToAssign.push(pil);
    });

    let orphansPIL = pils.filter((pil) => {
      return matPlantsPKS.indexOf(pil.material_plant.pk) == -1;
    });

    if (orphansPIL.length > 0) {
      orphansPIL.forEach((pil) => {
        pil.is_orphan = true;
        this.fixOrphanMP(pil);
      });
      plantsToAssign = plantsToAssign.concat(orphansPIL);
    }
    return plantsToAssign;
  }

  fixOrphanMP(pil: PackingInstructionLocation) {
    this.materialPlantsService.getMaterialPlant(pil.material_plant.pk).subscribe((data) => {
      pil.material_plant = data;
      this.sitesService.getSite(pil.material_plant.site.pk).subscribe((data_) => {
        pil.material_plant.site = data_;
      })
    })
  }

  handlePlantChange(p: PackingInstruction, s: PackingInstructionLocation) {
    if (!s.is_active) {
      if (s.pk) {
        s.status = "toDelete"
      }
      else {
        s.status = "toIgnore"; //non pregresso e disattivato, non faccio nulla
      }
    }
    else {
      if (s.is_active) {
        this.verifySiteCompliance(s, p, (isOK) => {
          if (isOK) {
            s.status = "toSave"; //va aggiunto
          }
        });
      }
    }
  }

  verifySiteCompliance(s: PackingInstructionLocation, p: PackingInstruction, callback) {
    if (p.packinstructionitem_set.length > 0) {
      //Chiamo un servizio che verifica che il dato plant produca tutti i materiali contenuti nella BOM.
      let query = [s.material_plant.site.pk];
      p.packinstructionitem_set.forEach((pii) => {
        query.push(pii.material.pk);
      });
      this.materialsService.getMaterials({
        siteon_matlist: query.join("|")
      }).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          s.is_active = false;
          //ci sono materiali non prodotti nei plant selezionati
          this.loadingService.hide();
          this.messageService.open({
            title: this.tr('BOM_ITEM_MISMATCH'),
            message: this.buildMismatchMessage(data['results']),
            confirm: () => {
            }
          })
        }
        else {
          if (data && data['count'] != null && data['count'] == 0) {
            callback.call(null, true);
          }
        }
      })
    }
    else {
      callback(true);
    }
  }

  buildMismatchMessage(ms: Material[]) {
    let content = [];
    content.push("<div>" + this.tr("MATERIALS_NOT_COMPLIANT") + "</div>");
    content.push("<ul>");
    ms.forEach((m) => {
      content.push("<li>");
      content.push(m.code + " - " + m.description);
      content.push("</li>");
    });
    content.push("</ul>");
    return content.join("");
  }

  getMaterialDesc(m: Material) {
    return m.code + " - " + m.description;
  }
}
