import {MaterialCodeGenerator} from "./material-code-generator";
import {Observable, Observer} from "rxjs";
import {Material} from "../../../../../_common/services/materials.service";

export class MaterialCodeGeneratorPrefBottClos extends MaterialCodeGenerator {

  generate(m:Material){
    this.loggingService.info("Code generation for \"bottle, closure, preform\" materials");
    return Observable.create((observer) => {
      let sapCharType = m.material_type.first_char;
      if(!sapCharType) {
        this.returnMissingInfo(observer);
      }
      else {
        let sapCodeShape = m.sub_model.shape.code;
        if (!sapCodeShape) {
          this.returnMissingInfo(observer);
        }
        else {
          //I get the last material created for this shape (pallettized or not)
          let query = {};
          query['ordering'] = "-code";
          query['code_icontains']=this.getQueryCode(m);
          query['results']=1;
          query['valuated'] = true;
          this.loggingService.info("Query: "+JSON.stringify(query));
          let lastMaterialValuated:Material = null;
          let lastMaterialUnvaluated:Material = null;
          this.materialsService.getMaterials(query).subscribe((data)=>{
            if(data && data['count'] && data['count']>0) {
              lastMaterialValuated = data['results'][0];
            }
            query['results']=1;
            query['valuated'] = false;
            this.loggingService.info("Query: "+JSON.stringify(query));
            this.materialsService.getMaterials(query).subscribe((data)=> {
              if (data && data['count'] && data['count'] > 0) {
                lastMaterialUnvaluated = data['results'][0];
              }
              if (lastMaterialValuated && lastMaterialUnvaluated) {
                if(lastMaterialUnvaluated.pk>lastMaterialValuated.pk){
                  this.processLastMaterial(lastMaterialUnvaluated, m, observer);
                }
                else {
                  this.processLastMaterial(lastMaterialValuated,m, observer);
                }
              }
              else {
                if (lastMaterialValuated)
                  this.processLastMaterial(lastMaterialValuated,m, observer);
                else {
                  if (lastMaterialUnvaluated) {
                    this.processLastMaterial(lastMaterialUnvaluated, m, observer);
                  }
                  else {
                    //Nessun materiale presente
                    //No results, new code generation
                    let code_comp = [];
                    if (!m.valuated) {
                      code_comp.push("N");
                    }
                    code_comp.push(sapCharType, sapCodeShape);
                    code_comp.push("01");
                    if (!m.sub_model.palletized) {
                      code_comp.push("00")
                    }
                    else {
                      code_comp.push("01")
                    }
                    let resp = {};
                    resp['status'] = 0;
                    resp['code'] = code_comp.join("");
                    this.loggingService.info("Generated code: " + resp['code']);
                    observer.next(resp);
                    observer.complete();
                  }
                }
              }
            })
          })
        }
      }
    });
  }

  processLastMaterial(lastMaterial:Material,m:Material, observer:Observer<any>){
    let charsToCut = 8;
    let code_cut = lastMaterial.code.substring(lastMaterial.code.indexOf(m.material_type.first_char),lastMaterial.valuated?charsToCut:charsToCut+1);
    let variant = parseInt(code_cut.substring(6),10);
    let code_comp = [];
    if(!m.valuated){
      code_comp.push("N");
    }
    code_comp.push(m.material_type.first_char,m.sub_model.shape.code);
    variant++;
    let variantString = variant+"";
    if(variant<10)
      variantString = "0"+variant;
    code_comp.push(variantString);
    if(!m.sub_model.palletized){
      code_comp.push("00")
    }
    else {
      code_comp.push("01")
    }
    let resp = {};
    if(!m.sub_model.palletized)
      resp['status'] = 0;
    else
      resp['status'] = 2;
    resp['code'] = code_comp.join("");
    this.loggingService.info("Generated code: "+resp['code']);
    observer.next(resp);
    observer.complete();
  }

  hasAllInfos(m:Material){
    return m.material_type.first_char!=null && m.sub_model.shape && m.sub_model.shape.code!=null && m.sub_model.palletized!=null;
  }

  getQueryCode(m:Material){
    return m.material_type.first_char+m.sub_model.shape.code;
  }
}
