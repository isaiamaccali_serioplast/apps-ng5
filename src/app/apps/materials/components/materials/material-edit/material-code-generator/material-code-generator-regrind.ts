import {MaterialCodeGenerator} from "./material-code-generator";
import {Observable} from "rxjs";
import {Material} from "../../../../../_common/services/materials.service";

export class MaterialCodeGeneratorRegrind extends MaterialCodeGenerator {

  generate(m:Material){
    this.loggingService.info("Code generation for \"regrind\" materials");
    return Observable.create((observer) => {
      let sapCharType = m.material_type.first_char;
      if (!sapCharType) {
        this.returnMissingInfo(observer);
      }
      else {
        let sapCodeSubType = m.sub_model.subtype.sap_code;
        if (!sapCodeSubType) {
          this.returnMissingInfo(observer);
        }
        else {
          let code_components = [];
          if(!m.valuated)
            code_components.push("N");
          code_components.push(sapCharType);
          code_components.push(sapCodeSubType);
          //4 digits have to be inserted manually
          let resp = {};
          resp['status'] = 3;
          resp['code'] = code_components.join("");
          this.loggingService.info("Generated code: "+resp['code']);
          observer.next(resp);
          observer.complete();
        }
      }
    });
  }

  hasAllInfos(m:Material){
    return m.material_type.first_char!=null && m.sub_model.subtype.sap_code!=null;
  }

  getQueryCode(m:Material){
    return m.material_type.first_char+ m.sub_model.subtype.sap_code;
  }
}
