import {MaterialCodeGenerator} from "./material-code-generator";
import {Observable} from "rxjs";
import {formatNumber} from "../../../../../../shared/utils";
import {Material} from "../../../../../_common/services/materials.service";

export class MaterialCodeGeneratorAssembledGoods extends MaterialCodeGenerator {

  generate(m:Material){
    this.loggingService.info("Code generation for \"common\" materials");
    return Observable.create((observer) => {
      let sapCharType = m.material_type.first_char;
      if(!sapCharType) {
        this.returnMissingInfo(observer);
      }
      else {
        let nCalls = 2;
        let lastValuatedMaterial:Material;
        let lastUnvaluatedMaterial:Material;
        let callback = () => {
          nCalls--;
          if(nCalls==0){
            this.loggingService.info("Queries finished, processing");
            let createCode = (nextNumber) => {
              this.loggingService.info("Next progressive: "+nextNumber);
              let nextNumberString = formatNumber(9,nextNumber);
              let code_components = [];
              if(!m.valuated)
                code_components.push("N");
              code_components.push(sapCharType);
              code_components.push(nextNumberString);
              let resp = {};
              resp['status'] = 0;
              resp['code'] = code_components.join("");
              this.loggingService.info("Next progressive: "+nextNumber);
              observer.next(resp);
              observer.complete();
            };

            if(lastValuatedMaterial && lastUnvaluatedMaterial){

              if(lastValuatedMaterial.pk == lastUnvaluatedMaterial.pk){
                //They are the same
                let lastCode = lastUnvaluatedMaterial.code;
                let lastNumber = lastCode.substring(2);
                let nextNumber = parseInt(lastNumber,10)+1;
                createCode(nextNumber);
              }
              else {
                //Different
                let nextNumber = Math.max(parseInt(lastValuatedMaterial.code.substring(1),10), parseInt(lastUnvaluatedMaterial.code.substring(2),10))+1;
                createCode(nextNumber)
              }
            }
            else {
              if(!lastValuatedMaterial && !lastUnvaluatedMaterial){
                //No materials exists for this type
                createCode(1);
              }
              else {
                let index = lastValuatedMaterial?1:2;
                let lastCode = lastValuatedMaterial?lastValuatedMaterial.code:lastUnvaluatedMaterial.code;
                let lastNumber = lastCode.substring(index);
                let nextNumber = parseInt(lastNumber,10)+1;
                createCode(nextNumber);
              }
            }
          }
        };
        this.loggingService.info("Queries for last code");
        let query = {};
        query['ordering'] = "-code";
        query['code_icontains']=sapCharType;
        query['valuated'] = true;
        query['results']=1;
        this.loggingService.info("Query 1: "+JSON.stringify(query));


        this.materialsService.getMaterials(query).subscribe((data)=>{
          if(data && data['count'] && data['count']>0){
            lastValuatedMaterial = data['results'][0];
          }
          callback();
        });

        let query2 = {};
        query2['ordering'] = "-code";
        query2['code_icontains']="N"+sapCharType;
        query['valuated'] = false;
        query2['results']=1;
        this.loggingService.info("Query 2: "+JSON.stringify(query2));


        this.materialsService.getMaterials(query2).subscribe((data)=>{
          if(data && data['count'] && data['count']>0) {
            lastUnvaluatedMaterial = data['results'][0];
          }
          callback();
        });
      }
    });
  }

  hasAllInfos(m:Material){
    return m.material_type.first_char!=null;
  }

  getQueryCode(m:Material){
    return m.material_type.first_char;
  }
}
