import {Material, MaterialsService} from "../../../../../_common/services/materials.service";
import {Observer, Observable} from "rxjs";
import {LoggingService} from "../../../../../../shared/logging/logging.service";
export class MaterialCodeGenerator {

  constructor(protected materialsService: MaterialsService, protected loggingService: LoggingService){}

  generate(m:Material):Observable<any>{
    return null;
  }

  hasAllInfos(m:Material):boolean{
    return null;
  }

  getQueryCode(m:Material):string{
    return null;
  }

  returnMissingInfo(observer:Observer<any>){
    this.loggingService.info("Missing info for code generation");
    let resp = {};
    resp['status'] = 1;
    observer.next(resp);
    observer.complete();
  }

}
