import {Material, MaterialsService} from "../../../../../_common/services/materials.service";
import {Injectable} from "@angular/core";
import {MaterialCodeGenerator} from "./material-code-generator";
import {MaterialCodeGeneratorCommon} from "./material-code-generator-common";
import {LoggingService} from "../../../../../../shared/logging/logging.service";
import {MaterialCodeGeneratorDecoration} from "./material-code-generator-decoration";
import {MaterialCodeGeneratorPrefBottClos} from "./material-code-generator-prefbottclos";
import {MaterialCodeGeneratorRegrind} from "./material-code-generator-regrind";
import {_getMaterialType, _MATERIAL_TYPES} from "../../../../../_common/services/material-type.service";
import {MaterialCodeGeneratorAssembledGoods} from "./material-code-generator-assembled-goods";

@Injectable()
export class MaterialCodeGeneratorService {

  generator: MaterialCodeGenerator;

  constructor(private materialService: MaterialsService, private loggingService: LoggingService) {
  }

  initGenerator(m: Material) {
    let matTypeConst = _getMaterialType(m);
    switch (matTypeConst) {
      case _MATERIAL_TYPES.RESIN:
      case _MATERIAL_TYPES.ADDITIVE:
      case _MATERIAL_TYPES.PACKAGING:
      case _MATERIAL_TYPES.RETURNABLE_PACKAGING:
        this.generator = new MaterialCodeGeneratorCommon(this.materialService, this.loggingService);
        break;
      case _MATERIAL_TYPES.DECORATION:
        this.generator = new MaterialCodeGeneratorDecoration(this.materialService, this.loggingService);
        break;
      case _MATERIAL_TYPES.PREFORM:
      case _MATERIAL_TYPES.BOTTLE:
      case _MATERIAL_TYPES.CLOSURE:
        this.generator = new MaterialCodeGeneratorPrefBottClos(this.materialService, this.loggingService);
        break;
      case _MATERIAL_TYPES.REGRIND:
        this.generator = new MaterialCodeGeneratorRegrind(this.materialService, this.loggingService);
        break;
      case _MATERIAL_TYPES.ASSEMBLED_GOODS:
        this.generator = new MaterialCodeGeneratorAssembledGoods(this.materialService, this.loggingService);
        break;
      case _MATERIAL_TYPES.COMPONENT:
      case _MATERIAL_TYPES.MOULDSET:
      case _MATERIAL_TYPES.BILLET:
      default:
        break;
    }
  }

  generate(m: Material) {
    this.initGenerator(m);
    if (this.generator)
      return this.generator.generate(m);
    else {
      this.loggingService.error("Impossible to instantiate code generator for material")
    }
  }

  hasAllInfos(m: Material) {
    this.initGenerator(m);
    if (this.generator) {
      return this.generator.hasAllInfos(m);
    }
    else {
      this.loggingService.error("Impossible to instantiate code generator for material")
    }
  }

  getQueryCode(m: Material) {
    this.initGenerator(m);
    if (this.generator) {
      return this.generator.getQueryCode(m)
    }
    else {
      this.loggingService.error("Impossible to instantiate code generator for material");
      return null;
    }
  }
}
