import {Component, OnInit, ViewChild} from "@angular/core";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {BOMService} from "../../../../_common/services/bom.service";
import {MatTabGroup} from "@angular/material";
import {MaterialEditBomComponent} from "./material-edit-bom/material-edit-bom";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {
  MaterialsAuthorizationsService,
  SERIOPLAST_RD_TECH_OFF,
  SERIOMOULD_RD
} from "../../../services/materials-authorizations.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {_getMaterialType, _MATERIAL_TYPES} from "../../../../_common/services/material-type.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-material-edit',
  templateUrl: 'material-edit.component.html',
  styleUrls: ['material-edit.component.css']
})
export class MaterialEditComponent extends TranslatableComponent implements OnInit {

  disabledStatus = {
    0: false,
    1: false,
    2: false,
    3: false,
    4: false,
    5: false
  };
  loadStatus = {
    0: true,
    1: false,
    2: false,
    3: false,
    4: false,
    5: false
  };
  material: Material;
  hasUnsavedData = false;
  bomCreatable = false;
  bomEditable = false;
  // routingCreatable = false;
  @ViewChild('tab') tab: MatTabGroup;
  @ViewChild('BOMComp') BOMComp: MaterialEditBomComponent;
  hasCompanyPositions: HasCompanyPosition[] = [];
  selectedIndex: number = 0; // index of "Main info" as default

  constructor(private materialsService: MaterialsService, private activatedRoute: ActivatedRoute,
              private messageService: MessageService, private router: Router, protected translationService: TranslationService,
              private bomService: BOMService, private loadingService: LoadingService,
              private materialsAuthorizationsService: MaterialsAuthorizationsService, private hasCompanyPositionService: HasCompanyPositionService,
              private loginService: LoginService,) {
    super(translationService);
    this.materialsAuthorizationsService.materialEditAnnounced$.subscribe(
      (material) => {
        // parent receive material to check and set related active tab
        this.setDisabledTabs(material);
        this.setDefaultActiveTab();
      });

  }

  ngOnInit() {

    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk, is_active:true}).subscribe((response) => {
      if (response['results'] && response['results'].length > 0) {
        this.hasCompanyPositions = response['results'];
        this.activatedRoute.params.subscribe((params) => {
          if (params['id']) {
            this.materialsService.getMaterial(params['id']).subscribe((material) => {
              this.material = material;
            })
          }
          else {
            this.material = new Material();
          }
        })
      }
    });
  }

  editTab(index: number) {
    for (let i in this.disabledStatus) {
      if (this.disabledStatus.hasOwnProperty(i)) {
        if (parseInt(i, 10) != index) {
          this.disabledStatus[i] = true;
        }
      }

    }
  }

  saveTab(index: number) {
    for (let i in this.disabledStatus) {
      if (this.disabledStatus.hasOwnProperty(i)) {
        if (parseInt(i, 10) != index) {
          this.disabledStatus[i] = false;
        }
      }
    }
  }

  deleteMaterial(m: Material) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      error: true,
      message: this.tr('ACTION_WITH_CONSEQUENCES'),
      confirm: () => {
        this.loadingService.show();
        this.materialsService.deleteMaterial(m).subscribe((response) => {
          this.loadingService.hide();
          if (response.status == 204) {
            this.router.navigate(["materials"]);
          }
          else {
            this.messageService.open({
              title: 'Error',
              error: true,
              message: response.message
            });
          }
        });
      },
      deny: () => {

      }
    })
  }

  setHasUnsavedData(hasUnsavedData: boolean, tabIndex: number) {
    this.hasUnsavedData = hasUnsavedData;
    if (this.hasUnsavedData) {
      this.editTab(tabIndex);
    }
    else {
      this.saveTab(tabIndex);
    }
  }

  back() {
    if (this.hasUnsavedData) {
      this.messageService.open({
        title: this.tr('ATTENTION'),
        error: true,
        message: this.tr('ATTENTION_DATA_LOSS'),
        confirm: () => {
          this.hasUnsavedData = false;
          this.router.navigate(['/materials','materials']);
        },
        deny: () => {
        }
      })
    }
    else {
      this.router.navigate(['/materials','materials']);
    }
  }

  setBOMAvailableForMaterial(bomCreatable: boolean) {

    this.bomCreatable = bomCreatable;
    this.bomEditable = bomCreatable;
    if (this.material.pk) {
      this.bomService.getBOMs({material_id: this.material.pk}).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          if (!this.bomCreatable) {
            this.bomEditable = true;
          }
          // this.routingCreatable = true;
        }
      });
    }
  }

  tabChange() {
    this.loadStatus[this.tab.selectedIndex] = true;
    if (this.tab.selectedIndex == 1) {
      if (this.BOMComp)
        this.BOMComp.reload();
    }
  }

  setDisabledTabs(material: Material) {
    this.canMainInfo(material);
    this.canBom(material);
    this.canProperties(material);
    this.canPackingInstructions(material);
    // this.canRoutings(material);
    this.canDocuments(material);
    this.canDeactivate(material);
  }

  canMainInfo(m: Material) {
    // Set disabled for Main info tab
    this.disabledStatus[0] = !(this.canEditMaterialInfo(m));
  }

  canBom(m: Material) {
    // Set disabled for Bom tab
    let matTypeConst = _getMaterialType(m);
    this.disabledStatus[1] = !(matTypeConst != _MATERIAL_TYPES.COMPONENT && matTypeConst != _MATERIAL_TYPES.MOULDSET && matTypeConst != _MATERIAL_TYPES.BILLET)
  }

  canProperties(m: Material) {
    // Set disabled for Properties tab
    this.disabledStatus[2] = !(this.canEditRoutings(m));
  }

  canPackingInstructions(m: Material) {
    // Set disabled for Packing Instructions tab
    this.disabledStatus[3] = !(this.canEditRoutings(m));
  }

  // canRoutings(m: Material) {
  //   // Set disabled for Routings tab
  //   this.disabledStatus[4] = !(this.canEditRoutings(m));
  // }

  canDocuments(m: Material) {
    // Set disabled for Documents tab
    this.disabledStatus[4] = !(this.canEditMaterialInfo(m));
  }

  canDeactivate(m: Material) {
    let matTypeConst = _getMaterialType(m);
    // Set disabled for Deactivate tab
    this.disabledStatus[5] = !(matTypeConst != _MATERIAL_TYPES.COMPONENT && matTypeConst != _MATERIAL_TYPES.MOULDSET && matTypeConst != _MATERIAL_TYPES.BILLET);
  }

  canEditMaterialInfo(m: Material) {
    let matTypeConst = _getMaterialType(m);
    if (matTypeConst == _MATERIAL_TYPES.BILLET || matTypeConst == _MATERIAL_TYPES.MOULDSET || matTypeConst == _MATERIAL_TYPES.COMPONENT) {
      return this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF);
    }
  }

  canEditRoutings(m: Material) {
    let matTypeConst = _getMaterialType(m);
    if (matTypeConst == _MATERIAL_TYPES.MOULDSET || matTypeConst == _MATERIAL_TYPES.COMPONENT) {
      return this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOMOULD_RD);
    }
  }

  setDefaultActiveTab() {

    // if "Main info" disabled then active first available tab
    if (this.disabledStatus[0]) {

      // convert disabledStatus (tabs) to an array
      let tabs = [];
      for (let key in this.disabledStatus) {
        if (this.disabledStatus.hasOwnProperty(key)) {
          let tabIndex = Number(key);
          let tab = {tabIndex: tabIndex, disabled: this.disabledStatus[key]};
          tabs.push(tab);
        }

      }

      let selectedIndexFound = false;
      for (let t of tabs) {
        // active first available tab
        if (!t["disabled"]) {
          this.selectedIndex = t["tabIndex"];
          selectedIndexFound = !selectedIndexFound;
          break;
        }
      }

      // If all tabs are disactive route to material list
      if (!selectedIndexFound) {
        this.router.navigate(["materials"]);
      }
    }

  }

}
