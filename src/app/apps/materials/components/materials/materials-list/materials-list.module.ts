import {NgModule} from "@angular/core";
import {MaterialsListComponent} from "./materials-list.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {MaterialsListQueryService} from "../../../services/materials-list-query-service";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [MaterialsListComponent],   // components and directives
  exports:[MaterialsListComponent],
  providers: []                    // services
})
export class MaterialsListModule { }


