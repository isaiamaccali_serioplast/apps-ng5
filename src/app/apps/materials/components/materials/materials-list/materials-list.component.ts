import {Component, OnInit, ViewChild} from "@angular/core";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {Router} from "@angular/router";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {MaterialType, MaterialTypesService} from "../../../../_common/services/material-type.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MaterialsListQueryService} from "../../../services/materials-list-query-service";
import {
  MaterialsAuthorizationsService, SERIOPLAST_RD_LAB, SERIOPLAST_RD_TECH_OFF, SERIOMOULD_RD
} from "../../../services/materials-authorizations.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {cloneObject, fixValue, parseSerionetRespToData} from "../../../../../shared/utils";
import {
  HeaderConf, TableRowAction,
  TableRowActions,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSubtypesLinkService} from "../../../../_common/services/material-subtype-link.service";
import {Observable} from "rxjs";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-materials-list',
  templateUrl: 'materials-list.component.html',
  styleUrls: ['materials-list.component.css']
})
export class MaterialsListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('materialsTable') materialsTable: TableWithPagesComponent;
  material: Material;
  site: Site;
  materialType: MaterialType;
  materialTypes: MaterialType[] = [];
  sites: Site[] = [];
  code_or_description: string;
  valuated = true;
  isActive = true;
  loading = true;
  resultsPerPage = 10;
  hasCompanyPositions: HasCompanyPosition[] = [];

  noItemsMessage = this.tr("NO_MATERIALS_FOUND");
  headerConf: HeaderConf[] = [
    {
      columnId: 'code',
      label: this.tr("CODE"),
      valueKey: 'code'
    },
    {
      label: this.tr("DESCRIPTION"),
      valueKey: 'description'
    },
    {
      columnId: 'type',
      label: this.tr("TYPE"),
      valueKey: 'material_type',
      useStr: true
    },
    {
      columnId: 'subtype',
      label: this.tr("SUBTYPE"),
      valueFunction: (m: Material) =>{
        if (!m.sub_model.subtype) {
          return "-";
        }
        else {
          return m.sub_model.subtype.__str__;
        }
      }
    }
  ];
  availableActions = [];
  ready = false;

  constructor(private materialsService: MaterialsService, private router: Router,
              private sitesService: SitesService, private materialTypesService: MaterialTypesService,
              protected translationService: TranslationService, private materialsListQueryService: MaterialsListQueryService,
              private materialsAuthorizationsService: MaterialsAuthorizationsService, private hasCompanyPositionService: HasCompanyPositionService,
              private loginService: LoginService, private materialSubtypeLinksService: MaterialSubtypesLinkService) {
    super(translationService);
    this.materialTypes = [];
    this.materialType = null;
    this.site = new Site();
    this.sites = [];

    let edit = cloneObject(TableRowActions.EDIT);
    edit.click = this.editMaterial.bind(this);
    edit.visibilityCondition = (m: Material) =>{
      return this.isCreatable(m.material_type.pk) && m.is_active;
    };
    let view = cloneObject(TableRowActions.VIEW);
    view.click = this.viewMaterial.bind(this);
    let restore = cloneObject(TableRowActions.RESTORE);
    restore.click = this.editMaterial.bind(this);
    restore.visibilityCondition = (m: Material) => {
      return this.isCreatable(m.material_type.pk) && !m.is_active;
    };
    let clone = new TableRowAction();
    clone.icon = 'content_copy';
    clone.tooltip = "Clone";
    clone.click = this.cloneMaterial.bind(this);
    clone.visibilityCondition = (m: Material) => {
      return this.isCreatable(m.material_type.pk) && m.is_active && this.isClonable(m.material_type.pk);
    };
    this.availableActions = [edit, view, restore, clone];
  }

  ngOnInit() {

    this.materialTypesService.getMaterialTypes({ordering: 'description', is_active: true}).subscribe((data) => {
      this.materialTypes = parseSerionetRespToData(data);
    });

    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk, is_active: true}).subscribe((response) => {
      this.hasCompanyPositions = parseSerionetRespToData(response);
      this.sitesService.getSites({is_active: true}).subscribe((data) => {
        this.sites = parseSerionetRespToData(data);
        this.ready = true;
        let query = this.materialsListQueryService.getQuery();
        if (query) {
          this.restoreQueryData(query);
        }
      })
    });
  }

  restoreQueryData(query: any) {
    if (query['site'])
      this.site = query['site'];
    if (query['type'])
      this.materialType = query['type'];
    if (query['search'])
      this.code_or_description = query['search'];
    if (query['valuated'] != null)
      this.valuated = query['valuated'];
    if (query['is_active'] != null)
      this.isActive = query['is_active'];
    if (query['currentPage'] != null)
      this.materialsTable.setPage(query['currentPage']);
    if (query['resultsPerPage'] != null)
      this.resultsPerPage = query['resultsPerPage'];
  }

  saveQueryData() {
    let query = {};
    query['site'] = this.site;
    query['type'] = this.materialType;
    query['search'] = this.code_or_description;
    query['valuated'] = this.valuated;
    query['is_active'] = this.isActive;
    query['currentPage'] = this.materialsTable.getPage();
    query['resultsPerPage'] = this.resultsPerPage;
    this.materialsListQueryService.saveQuery(cloneObject(query));
  }

  loadMaterials(page?: number, results?: number) {

    let query = {};
    if (this.site.pk)
      query['site_set'] = this.site.pk;
    if (this.materialType)
      query['material_type'] = this.materialType;
    if (this.code_or_description)
      query['search'] = this.code_or_description;
    if (this.valuated != null)
      query['valuated'] = this.valuated;
    if (this.isActive != null)
      query['is_active'] = this.isActive;

    query['ordering'] = 'code';

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    this.saveQueryData();

    return this.materialsService.getMaterials(query);
  }

  editMaterial(m: Material) {
    this.router.navigate(["materials", "materials", "detail", m.pk, 'edit']);
  }

  viewMaterial(m: Material) {
    this.router.navigate(["materials", "materials", "detail", m.pk]);
  }

  createMaterial() {
    this.router.navigate(["materials", "materials", "create"]);
  }

  cloneMaterial(m: Material) {
    let extras = {
      queryParams: {
        materialModel: m.pk
      }
    };
    this.router.navigate(["materials", "materials", "create"], extras);
  }

  selectedSite() {
    if (this.site.pk) {
      fixValue(this.sites, this, 'site');
    }
    else {
      this.site = new Site();
    }
  }

  canCreateMaterial() {
    return this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_LAB);
  }

  isCreatable(matTypePk: number) {
    let create = false;
    this.materialTypes.forEach((mt) => {
      if (mt.pk == matTypePk) {
        if (mt.sap_code == "ZBIL") {
          // Billet editable only for this permission
          create = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF);
        } else {
          // Mouldset and Component
          if (mt.sap_code == "ZFHM" || mt.sap_code == "ZIBE") {
            create = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF) || this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOMOULD_RD);
          } else {
            create = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_LAB);
          }
        }
      }
    });
    return create;
  }

  isClonable(matTypePk: number) {
    let clonable = false;
    this.materialTypes.forEach((mt) => {
      if (mt.pk == matTypePk) {
        // if Biller or Mouldset or Component check authorization
        if (mt.sap_code == "ZBIL" || mt.sap_code == "ZFHM" || mt.sap_code == "ZIBE") {
          clonable = false;
        } else {
          clonable = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_LAB);
        }
      }
    });
    return clonable;
  }

  expandDataSubTypes(materials: Material[]) {
    return Observable.create((observer) => {

      let expandST = (ms, index, callback) => {
        if (ms[index]) {
          let m = ms[index];
          if (m.sub_model.subtype) {
            this.materialSubtypeLinksService.getMaterialSubtypeLink(m.sub_model.subtype).subscribe((s) => {
              if (s.pk)
                m.sub_model.subtype = s;
            })
          }
          expandST(ms, index + 1, callback);
        }
        else {
          callback();
        }
      };
      expandST(materials, 0, () => {
        observer.next(materials);
        observer.complete();
      })
    });
  }

  reloadList() {
    this.materialsTable.setPage(1);
    this.materialsTable.reload();
  }

}
