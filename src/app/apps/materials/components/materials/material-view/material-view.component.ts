import {Component, OnInit} from "@angular/core";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {ActivatedRoute} from "@angular/router";
import {_getMaterialType, _MATERIAL_TYPES, isBottleClosurePreform, MaterialType, MaterialTypesService} from "../../../../_common/services/material-type.service";
import {MaterialPlant, MaterialPlantsService} from "../../../../_common/services/material-plant.service";
import {BOM, BOMItem, BOMService} from "../../../../_common/services/bom.service";
import {MeasureUnitService} from "../../../../_common/services/measure-unit.service";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {PackingInstruction, PackingInstructionItem, PackingInstructionsService} from "../../../../_common/services/packing-instructions.service";
import {PackingInstructionLocation, PackingInstructionsLocationService} from "../../../../_common/services/packing-instructions-plant.service";
import {MaterialsAuthorizationsService, SERIOMOULD_RD, SERIOPLAST_RD_LAB, SERIOPLAST_RD_TECH_OFF} from "../../../services/materials-authorizations.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {CavitiesService, Cavity} from "../../../../_common/services/cavities.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {Routing, RoutingsService} from "../../../../_common/services/routings.service";
import {parseSerionetRespToData} from "../../../../../shared/utils";

@Component({
  selector: 'app-material-edit',
  templateUrl: 'material-view.component.html',
  styleUrls: ['material-view.component.css']
})
export class MaterialViewComponent implements OnInit {

  material: Material;
  MATERIAL_TYPES = _MATERIAL_TYPES;
  materialPlants: MaterialPlant[];
  boms: BOM[] = [];
  materialTypes: MaterialType[] = [];
  packingInstructions: PackingInstruction[] = [];
  hasCompanyPositions: HasCompanyPosition[] = [];
  cavities: Cavity[] = [];
  routings: Routing[] = [];

  constructor(private materialsService: MaterialsService, private activatedRoute: ActivatedRoute,
              private materialTypesService: MaterialTypesService, private materialPlantsService: MaterialPlantsService,
              private bomService: BOMService, private measureUnitService: MeasureUnitService, private sitesService: SitesService,
              private packingInstructionsService: PackingInstructionsService, private packingInstrPlantsService: PackingInstructionsLocationService,
              private materialsAuthorizationsService: MaterialsAuthorizationsService, private hasCompanyPositionService: HasCompanyPositionService,
              private cavityService: CavitiesService, private loginService: LoginService, private routingsService: RoutingsService) {
    this.materialPlants = [];
  }

  ngOnInit() {
    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk}).subscribe((response) => {
      if (response['results'] && response['results'].length > 0) {
        this.hasCompanyPositions = response['results'];
        this.activatedRoute.params.subscribe((params) => {
          if (params['id']) {
            this.materialsService.getMaterial(params['id']).subscribe((material) => {
              this.material = material;
              this.materialTypesService.getMaterialType(this.material.material_type.pk).subscribe((matType) => {
                this.material.material_type = matType;
                this.cavities = [];
                if (_getMaterialType(this.material) == _MATERIAL_TYPES.MOULDSET) {
                  this.cavityService.getCavities({mouldset: this.material.pk, plm_revision: this.material.sub_model.plm_revision}).subscribe((data) => {
                    this.cavities = parseSerionetRespToData(data);
                  });
                }

              });
              this.materialPlantsService.getMaterialPlants({
                material_id: this.material.pk,
                is_active: true
              }).subscribe((data) => {
                this.materialPlants = parseSerionetRespToData(data);
              });
              this.loadBOMs();
              this.loadPackingInstructions();
              this.loadRoutings();
            })
          }
          else {
            this.material = new Material();
          }
        });
        this.materialTypesService.getMaterialTypes({is_active: true}).subscribe((data) => {
          this.materialTypes = parseSerionetRespToData(data);
        })
      }
    });
  }

  isBottlePrefClosure(m: Material) {
    return isBottleClosurePreform(m);
  }

  getMaterialType(m: Material) {
    return _getMaterialType(m);
  }

  loadBOMs() {
    this.bomService.getBOMs({material_id: this.material.pk, is_active: true}).subscribe((data) => {
      this.boms = parseSerionetRespToData(data);
      this.boms.sort((b1, b2) => {
        return parseInt(b1.alt_bom_id, 10) - parseInt(b2.alt_bom_id, 10);
      });
      this.fixBOMData()
    });
  }

  fixBOMData() {
    for (let b of this.boms) {
      b.material_id = this.material;
      this.fixSites(b.sites);
      this.fixBOMItems(b.bomitem_set);
    }
  }

  fixSites(sites: Site[]) {
    for (let i = 0; i < sites.length; i++) {
      this.fixSite(sites, i);
    }
  }

  fixSite(sites, index) {
    this.sitesService.getSite(sites[index].pk).subscribe((site) => {
      sites[index] = site;
    })
  }

  fixBOMItems(bSets: BOMItem[]) {
    for (let i = 0; i < bSets.length; i++) {
      this.fixUOM(bSets, i);
      this.fixComponent(bSets, i);
    }
  }

  fixUOM(bSets: BOMItem[], index) {
    this.measureUnitService.getMeasureUnit(parseInt(bSets[index].uom + "", 10)).subscribe((uom) => {
      bSets[index].uom = uom;
    })
  }

  fixComponent(bSets: BOMItem[], index) {
    this.materialsService.getMaterial(parseInt(bSets[index].material + "", 10)).subscribe((component) => {
      bSets[index].material = component;
    })
  }

  isCreatable(matTypePk: number) {
    let create = false;
    this.materialTypes.forEach((mt) => {
      if (mt.pk == matTypePk) {
        if (mt.sap_code == "ZBIL") {
          // Billet editable only for this permission
          create = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF);
        } else {
          // Mouldset and Component
          if (mt.sap_code == "ZFHM" || mt.sap_code == "ZIBE") {
            create = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF) || this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOMOULD_RD);
          } else {
            create = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_LAB);
          }
        }
      }
    });
    return create;
  }

  loadPackingInstructions() {
    let query = {};
    query['is_active'] = true;
    query['material_id'] = this.material.pk;
    this.packingInstructions = [];
    this.packingInstructionsService.getPackingInstructions(query).subscribe((data) => {
      let pi_: PackingInstruction[] = parseSerionetRespToData(data);
      this.fixPackingInstructions(pi_, () => {
        this.packingInstructions = pi_;
      })
    })
  }

  fixPackingInstructions(pi: PackingInstruction[], callback?: any) {
    this.fixPackingInstruction(pi, 0, callback);
  }

  fixPackingInstruction(pi: PackingInstruction[], index: number, callback?: any) {
    if (pi[index]) {
      this.fixPackingItems(pi[index].packinstructionitem_set, 0, () => {
        let main_index = 0;
        pi[index].packinstructionitem_set.forEach((pii, i) => {
          if (pii.main_package)
            main_index = i;
        });
        pi[index].mainPackage = main_index;
        pi[index].packinstructionitem_set.sort((a, b) => {
          if ((a.material.pk == this.material.pk) && (b.material.pk != this.material.pk)) {
            return 1;
          }
          else {
            if ((b.material.pk == this.material.pk) && (a.material.pk != this.material.pk)) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
        pi[index].plants = [];
        this.packingInstrPlantsService.getPackingInstructionLocations({pack_instruct__id: pi[index].pk, is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            pi[index].plants = this.processLocations(pi[index], data['results']);
            this.fixPackingInstruction(pi, index + 1, callback);
          }
          else {
            //casistica impossibile
            pi[index].plants = this.processLocations(pi[index], []);
            this.fixPackingInstruction(pi, index + 1, callback);
          }
        });
      })
    }
    else {
      callback();
    }
  }

  fixPackingItems(pis: PackingInstructionItem[], index: number, callback?: any) {
    if (pis[index]) {
      let pis_ = pis[index];
      this.materialsService.getMaterial(pis_.material).subscribe((material) => {
        pis[index].material = material;
        this.measureUnitService.getMeasureUnit(parseInt(pis_.uom + "", 10)).subscribe((uom) => {
          pis[index].uom = uom;
          this.fixPackingItems(pis, index + 1, callback);
        })
      })
    }
    else {
      callback();
    }
  }

  processLocations(pi: PackingInstruction, pils: PackingInstructionLocation[]) {
    let matPlantsPKS = this.materialPlants.map((mp) => {
      return mp.pk;
    });

    let plantsToAssign = [];

    this.materialPlants.forEach((mp) => {
      let pil = new PackingInstructionLocation();
      let filteredPIL = pils.filter((pil) => {
        return pil.material_plant.pk == mp.pk;
      });
      if (filteredPIL.length > 0) {
        pil = filteredPIL[0];
        pil.material_plant = mp;
        plantsToAssign.push(pil);
      }
    });

    let orphansPIL = pils.filter((pil) => {
      return matPlantsPKS.indexOf(pil.material_plant.pk) == -1;
    });

    if (orphansPIL.length > 0) {
      orphansPIL.forEach((pil) => {
        pil.is_orphan = true;
        this.fixOrphanMP(pil);
      });
      plantsToAssign = plantsToAssign.concat(orphansPIL);
    }
    return plantsToAssign;
  }

  fixOrphanMP(pil: PackingInstructionLocation) {
    this.materialPlantsService.getMaterialPlant(pil.material_plant.pk).subscribe((data) => {
      pil.material_plant = data;
      this.sitesService.getSite(pil.material_plant.site.pk).subscribe((data_) => {
        pil.material_plant.site = data_;
      })
    })
  }

  loadRoutings() {
    let query = {};
    query['is_active'] = true;
    query['material'] = this.material.pk;
    query['explode_all'] = true;
    this.routings = [];
    this.routingsService.getRoutings(query).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.routings = data['results'];
      }
    })
  }
}
