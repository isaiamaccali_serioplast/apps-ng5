import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {MaterialViewComponent} from "./material-view.component";
import {I18NModule} from "../../../../../shared/i18n/i18n.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule, I18NModule],       // module dependencies
  declarations: [MaterialViewComponent],   // components and directives
  exports:[],
  providers: []                    // services
})
export class MaterialsViewModule { }


