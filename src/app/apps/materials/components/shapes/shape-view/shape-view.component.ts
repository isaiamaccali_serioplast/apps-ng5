import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {buildErrorMessage} from "../../../../../shared/utils";
import {Shape, ShapesService} from "../../../../_common/services/shapes.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {
  MaterialsAuthorizationsService,
  SERIOPLAST_RD_TECH_OFF
} from "../../../services/materials-authorizations.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'shape-view',
  templateUrl: 'shape-view.component.html',
  styleUrls: ['shape-view.component.css']
})
export class ShapeViewComponent extends TranslatableComponent implements OnInit {

  shape: Shape = new Shape();
  canViewActionEdit: boolean;
  hasCompanyPositions:HasCompanyPosition[] = [];

  constructor(private shapesService: ShapesService,
              protected translationService: TranslationService,
              private messageService:MessageService,
              private activatedRoute:ActivatedRoute,
              private router:Router, private hasCompanyPositionService:HasCompanyPositionService,
              private materialsAuthorizationsService:MaterialsAuthorizationsService,
              private loginService:LoginService
  ) {
    super(translationService);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params)=> {
      if (params['id']) {
        this.loadShape(parseInt(params['id'],10));
      }
    });

    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk}).subscribe((response) => {
      if (response['results'] && response['results'].length > 0) {
        this.hasCompanyPositions = response['results'];
        this.canViewActionEdit = this.canEditShapes();
      }
    });

  }

  canEditShapes() {
    return this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF);
  }


  loadShape(shapePK) {
    this.shapesService.getShape(shapePK).subscribe((resp) => {
      if(resp.pk){
        this.shape = resp;
        let alternativeNames = this.shape.alternative_name.map((alternativeName) => {
          return alternativeName.__str__;
        });

        this.shape.alternative_name_plain = alternativeNames.join();
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  goToList(){
    this.router.navigate(["/materials","shapes"]);
  }

  editShape(s: Shape) {
    this.router.navigate(["/materials","shapes","detail",s.pk,"edit"]);
  }
}
