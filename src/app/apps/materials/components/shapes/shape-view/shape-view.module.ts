import {NgModule} from "@angular/core";
import {ShapeListComponent} from "./shape-list.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {ShapeViewComponent} from "./shape-view.component";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [ShapeViewComponent],   // components and directives
  exports:[ShapeViewComponent],
  providers: []                    // services
})
export class ShapeViewModule { }


