import {NgModule} from "@angular/core";
import {ShapeListComponent} from "./shape-list.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [ShapeListComponent],   // components and directives
  exports:[ShapeListComponent],
  providers: []                    // services
})
export class ShapeListModule { }


