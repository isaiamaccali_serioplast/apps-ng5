import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {
  MaterialsAuthorizationsService, SERIOPLAST_RD_TECH_OFF
} from "../../../services/materials-authorizations.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {Shape, ShapesService} from "../../../../_common/services/shapes.service";
import {Router} from "@angular/router";
import {
  HeaderConf, TableRowAction, TableRowActions,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, parseSerionetRespToData} from "../../../../../shared/utils";
import {TechnologyLink, TechnologyLinkService} from "../../../../_common/services/technology-link.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

export let SHAPE_CT = 104; // DjangoContentType model Shape PK

declare let sap:any;

@Component({
  selector: 'shape-list',
  templateUrl: 'shape-list.component.html',
  styleUrls: ['shape-list.component.css']
})
export class ShapeListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('shapesTable') shapesTable:TableWithPagesComponent;
  code_or_description: string;
  isActive = true;
  hasCompanyPositions: HasCompanyPosition[] = [];
  canViewActionEdit: boolean = false;
  ready = false;
  technologyFilter:number;

  headerConf:HeaderConf[] = [
    {
      label:this.tr("CODE"),
      valueKey:'code'
    },
    {
      label:this.tr("DESCRIPTION"),
      valueKey:'description'
    }
  ];
  availableActions = [];
  technologies:TechnologyLink[] = [];
  resultsPerPage = 10;

  constructor(private shapesService: ShapesService,
              protected translationService: TranslationService,
              private materialsAuthorizationsService: MaterialsAuthorizationsService,
              private hasCompanyPositionService: HasCompanyPositionService,
              private loginService: LoginService,
              private router:Router,
              private technologiesService:TechnologyLinkService
  ) {
    super(translationService);
  }

  ngOnInit() {

    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk}).subscribe((response) => {
      if (response['results'] && response['results'].length > 0) {
        this.hasCompanyPositions = response['results'];
        this.canViewActionEdit = this.canEditShapes();
      }
    });

    this.technologiesService.getTechnologyLinks({
      app: SHAPE_CT,
      is_active: true
    }).subscribe((data) => {
      this.technologies = parseSerionetRespToData(data);
      this.ready = true;
      this.shapesTable.reload();
    });

    let actionEdit:TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEdit.click = this.editShape.bind(this);
    actionEdit.visibilityCondition = (s:Shape) => {
      return s.is_active && this.canViewActionEdit
    };
    let actionView:TableRowAction = cloneObject(TableRowActions.VIEW);
    actionView.click = this.viewShape.bind(this);
    this.availableActions = [actionView,actionEdit];

  }

  loadShapes(page?: number, results:number=10) {
    let query = {};

    if (this.code_or_description)
      query['search'] = this.code_or_description;
    if (this.isActive != null)
      query['is_active'] = this.isActive;

    if (this.technologyFilter != null)
      query['technology'] = this.technologyFilter;

    query['ordering'] = 'code';

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    return this.shapesService.getShapes(query);
  }

  editShape(s: Shape) {
    this.router.navigate(["/materials","shapes","detail",s.pk,"edit"]);
  }

  viewShape(s: Shape) {
   this.router.navigate(["/materials","shapes","detail",s.pk,"view"]);
  }

  canEditShapes() {
    return this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_TECH_OFF);
  }

  reload(){
    this.shapesTable.reload();
  }

}
