import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {buildErrorMessage, parseSerionetRespToData} from "../../../../../shared/utils";
import {Shape, ShapeAlternativeName, ShapesService} from "../../../../_common/services/shapes.service";
import {Organization} from "../../../../_common/services/organization.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {MatSnackBar} from "@angular/material";
import {ActivatedRoute, Router} from "@angular/router";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {MaterialSubTypeLink, MaterialSubtypesLinkService} from "../../../../_common/services/material-subtype-link.service";
import {MaterialType, MaterialTypesService} from "../../../../_common/services/material-type.service";
import {TechnologyLink, TechnologyLinkService} from "../../../../_common/services/technology-link.service";
import {HasCompanyPosition} from "../../../../_common/services/has-company-position.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

export let SHAPE_CT = 104; // DjangoContentType model Shape PK

@Component({
  selector: 'shape-edit',
  templateUrl: 'shape-edit.component.html',
  styleUrls: ['shape-edit.component.css']
})
export class ShapeEditComponent extends BaseComponent implements OnInit {

  shapes: Shape[] = [];
  shape: Shape = new Shape();
  materialType: MaterialType = new MaterialType();
  materialTypes: MaterialType[] = [];
  loading = true;
  resultsPerPage = 10;
  hasCompanyPositions: HasCompanyPosition[] = [];
  canViewActionEdit: boolean;
  mode_list: boolean;
  mode_edit: boolean;
  mode_view: boolean;
  materialSubTypeLinks: MaterialSubTypeLink[] = [];
  technologies: TechnologyLink[] = [];

  constructor(private shapesService: ShapesService, protected translationService: TranslationService,
              protected messageService: MessageService,
              private snackBar: MatSnackBar, private activatedRoute: ActivatedRoute,
              private router: Router, protected loadingService: LoadingService,
              private materialTypesService: MaterialTypesService,
              private materialSubtypeLinkService: MaterialSubtypesLinkService, private technologiesService: TechnologyLinkService) {
    super(translationService, messageService, loadingService);
    this.materialTypes = [];
    this.materialType = null;
    this.canViewActionEdit = false;
    this.mode_list = true;
    this.mode_view = false;
    this.mode_edit = false;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.loadShape(parseInt(params['id'], 10));
      }
    });
  }

  loadShape(shapePK) {
    this.shapesService.getShape(shapePK).subscribe((resp) => {
      if (resp.pk) {
        this.shape = resp;
        this.shape.alternative_name = this.shape.alternative_name.map((alternativeName) => {
          alternativeName.label = alternativeName.__str__;
          return alternativeName;
        });
        this.materialTypesService.getMaterialTypes(({is_active: true})).subscribe((resp) => {
          this.materialTypes = parseSerionetRespToData(resp);
          let matType = this.materialTypes.filter((x) => x.pk == this.shape.material_type.pk);
          this.materialSubtypeLinkService.getMaterialSubtypeLinks({
            app: matType[0].content_type,
            is_active: true
          }).subscribe((data) => {
            this.materialSubTypeLinks = parseSerionetRespToData(data);
          });
          this.technologiesService.getTechnologyLinks({
            app: SHAPE_CT,
            is_active: true
          }).subscribe((data) => {
            this.technologies = parseSerionetRespToData(data);
          });
        })
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  goToList() {
    this.router.navigate(["/materials", "shapes"]);
  }

  selectedOwner(event: any) {
    if (event)
      this.shape.owner = event;
    else
      this.shape.owner = new Organization();
  }

  saveShape() {
    let displayMsg = false;
    if (this.shape.is_active == false) {
      displayMsg = true;
    }

    if (this.shape.alternative_name.length > 0) {
      let lastElement = this.shape.alternative_name[this.shape.alternative_name.length - 1];
      let lastElementIndex = this.shape.alternative_name.indexOf(lastElement, 0);
      if (!this.shape.alternative_name[lastElementIndex].label) {
        this.shape.alternative_name.splice(lastElementIndex, 1);
      }
    }

    this.loadingService.show();
    this.shapesService.saveShape(this.shape).subscribe((resp) => {
      this.loadingService.hide();
      this.manageRespSave(resp, () => {
        this.goToList();
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  hasEmptyNames() {
    let hasEmpty = false;
    this.shape.alternative_name.forEach((an) => {
      if (!an.label)
        hasEmpty = true;
    });
    return hasEmpty;
  }

  addAlternativeName() {
    this.shape.alternative_name.push(new ShapeAlternativeName());
  }

  deleteAlternativeName(shapeAlternative: ShapeAlternativeName) {
    this.shape.alternative_name = this.shape.alternative_name.filter((x) => x.label !== shapeAlternative.label);
  }

}
