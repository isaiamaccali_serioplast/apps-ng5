import {NgModule} from "@angular/core";
import {ShapeListComponent} from "./shape-list.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {ShapeEditComponent} from "./shape-edit.component";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [ShapeEditComponent],   // components and directives
  exports:[ShapeEditComponent],
  providers: []                    // services
})
export class ShapeEditModule { }


