import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {MaterialsListComponent} from "./components/materials/materials-list/materials-list.component";
import {MaterialEditComponent} from "./components/materials/material-edit/material-edit.component";
import {MaterialViewComponent} from "./components/materials/material-view/material-view.component";
import {ShapeListComponent} from "./components/shapes/shapes-list/shape-list.component";
import {ShapeViewComponent} from "./components/shapes/shape-view/shape-view.component";
import {ShapeEditComponent} from "./components/shapes/shape-edit/shape-edit.component";
import {MaterialsIndexComponent} from "./components/index/materials-index.component";

// Route Configuration
export const materialsRoutes: Routes = [
  {
    path: '',
    component: MaterialsIndexComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'materials',
        pathMatch:'full',
        component: MaterialsListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'materials/detail/:id',
        redirectTo: 'materials/detail/:id/view',
        canActivate: [AuthGuard]
      },
      {
        path: 'materials/detail/:id/view',
        component: MaterialViewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'materials/detail/:id/edit',
        component: MaterialEditComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'materials/create',
        component: MaterialEditComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'shapes',
        pathMatch:'full',
        component: ShapeListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'shapes/detail/:id',
        redirectTo: 'shapes/detail/:id/view',
        canActivate: [AuthGuard]
      },
      {
        path: 'shapes/detail/:id/view',
        component: ShapeViewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'shapes/detail/:id/edit',
        component: ShapeEditComponent,
        canActivate: [AuthGuard]
      }

    ]
  }
];
