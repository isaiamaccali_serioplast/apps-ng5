import {Property} from "../../_common/services/properties.service";
import {PropertySet} from "../../_common/services/property-set.service";
export class PropertyStructure {
  property:Property;
  materials:PropertyStructureMaterial[];
  is_active:boolean;
}

export class PropertyStructureMaterial {
  enabled:boolean;
  propertySet:PropertySet;
}
