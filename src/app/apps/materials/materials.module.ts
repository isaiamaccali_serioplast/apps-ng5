import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {MaterialsIndexModule} from "./components/index/materials-index.module";
import {MaterialEditModule} from "./components/materials/material-edit/material-edit.module";
import {MaterialsListModule} from "./components/materials/materials-list/materials-list.module";
import {MaterialsViewModule} from "./components/materials/material-view/material-view.module";
import {materialsRoutes} from "./materials.routes";
import {RouterModule} from "@angular/router";
import {MaterialsListQueryService} from "./services/materials-list-query-service";
import {MaterialsAuthorizationsService} from "./services/materials-authorizations.service";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(materialsRoutes), MaterialsIndexModule, MaterialEditModule, MaterialsListModule, MaterialsViewModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [MaterialsListQueryService,MaterialsAuthorizationsService]                    // services
})
export class MaterialsModule { }

