import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Material} from "../../_common/services/materials.service";
import {HasCompanyPosition} from "../../_common/services/has-company-position.service";

export const DEVELOPERS = [
  1, //Serioplast Software Developer
  10, //Software Development Manager
  7, //ERP Manager
  8 //ERP Support
];

export const SERIOPLAST_RD_LAB = [
  50, //R&D Lab Manager (RD00) - Deprecated
  51, //R&D Lab Technician (RD00) - Deprecated
  148, //R&D Lab Manager (HQ)
  149, //R&D Lab Technician (HQ)
];

export const SERIOPLAST_RD_PROCESS = [
  123, // R&D Process Engineer
  150 // R&D Process Manager
];

export const SERIOPLAST_RD_TECH_OFF = [
  46, // R&D Technical Manager
  47 // R&D Designer
];
export const SERIOMOULD_RD = [
  80, //Seriomould General Manager
  83 // CAM Engineer
];

@Injectable()
export class MaterialsAuthorizationsService {

  private materialEditAnnouncedSource = new Subject<Material>();
  materialEditAnnounced$ = this.materialEditAnnouncedSource.asObservable();

  constructor() {
  }

/*  isAuthorized(hasCompanyPositions: HasCompanyPosition[], allowedCompanyPositionNames: number[]) {

    for (let i = 0; i < hasCompanyPositions.length; i++) {
      if (DEVELOPERS.indexOf(hasCompanyPositions[i].get_company_position_obj.name.pk) !== -1 || allowedCompanyPositionNames.indexOf(hasCompanyPositions[i].get_company_position_obj.name.pk) !== -1) {
        return true;
      }
    }
    return false
  }*/

  isAuthorized(hasCompanyPositions: HasCompanyPosition[], allowedCompanyPositionNames: number[]) {

    let userCompanyPositionNames = hasCompanyPositions.map((item: HasCompanyPosition) => {
      return item.get_company_position_obj.name.pk;
    });

    return (DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 || userCompanyPositionNames.filter((item) => allowedCompanyPositionNames.indexOf(item) !== -1).length > 0);

  }

  // Service message commands
  announceMaterialEditInitComplete(material: Material) {
    this.materialEditAnnouncedSource.next(material);
  }

}
