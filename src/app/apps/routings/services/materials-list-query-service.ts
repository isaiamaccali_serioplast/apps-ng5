import {Injectable} from "@angular/core";

@Injectable()
export class MaterialsListQueryService {

  query:any = {};

  constructor() {}

  saveQuery(query:any){
    this.query = query;
  }

  getQuery(){
    return this.query;
  }

}
