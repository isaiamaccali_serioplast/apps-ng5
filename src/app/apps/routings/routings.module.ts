import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {MaterialEditModule} from "./components/materials/material-edit/material-edit.module";
import {MaterialsListModule} from "./components/materials/materials-list/materials-list.module";
import {materialsRoutes} from "./materials.routes";
import {RouterModule} from "@angular/router";
import {MaterialsListQueryService} from "./services/materials-list-query-service";
import {MaterialsAuthorizationsService} from "./services/materials-authorizations.service";
import {routingsRoutes} from "./routings.routes";
import {RoutingsIndexModule} from "./components/index/routings-index.module";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routingsRoutes), RoutingsIndexModule, MaterialEditModule, MaterialsListModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [MaterialsListQueryService,MaterialsAuthorizationsService]                    // services
})
export class RoutingsModule { }

