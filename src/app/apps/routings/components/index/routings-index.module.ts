import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {MaterialsIndexComponent} from "./materials-index.component";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {MaterialsListModule} from "../materials/materials-list/materials-list.module";
import {MaterialEditModule} from "../materials/material-edit/material-edit.module";
import {RoutingsIndexComponent} from "./routings-index.component";

@NgModule({
  imports: [SharedModule, MaterialsListModule, MaterialEditModule],       // module dependencies
  declarations: [RoutingsIndexComponent],   // components and directives
  exports:[RoutingsIndexComponent],
  providers: [AppPlantUsersService]                    // services
})
export class RoutingsIndexModule { }


