import {Component, OnInit} from "@angular/core";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {BOMService} from "../../../../_common/services/bom.service";
import {MaterialEditBomComponent} from "./material-edit-bom/material-edit-bom";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-material-edit',
  templateUrl: 'material-edit.component.html',
  styleUrls: ['material-edit.component.css']
})
export class MaterialEditComponent extends TranslatableComponent implements OnInit {

  material: Material;
  hasUnsavedData = false;
  bomCreatable = false;
  bomEditable = false;
  routingCreatable = false;
  hasCompanyPositions: HasCompanyPosition[] = [];
  selectedIndex: number = 0; // index of "Main info" as default

  constructor(private materialsService: MaterialsService, private activatedRoute: ActivatedRoute,
              private messageService: MessageService, private router: Router, protected translationService: TranslationService,
              private bomService: BOMService, private hasCompanyPositionService: HasCompanyPositionService,
              private loginService: LoginService) {
    super(translationService);
  }

  ngOnInit() {

    this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk, is_active: true}).subscribe((response) => {
      if (response['results'] && response['results'].length > 0) {
        this.hasCompanyPositions = response['results'];
        this.activatedRoute.params.subscribe((params) => {
          if (params['id']) {
            this.materialsService.getMaterial(params['id']).subscribe((material) => {
              this.material = material;
            })
          }
          else {
            this.material = new Material();
          }
        })
      }
    });
  }

  back() {
    if (this.hasUnsavedData) {
      this.messageService.open({
        title: this.tr('ATTENTION'),
        error: true,
        message: this.tr('ATTENTION_DATA_LOSS'),
        confirm: () => {
          this.hasUnsavedData = false;
          this.router.navigate(['/routings', 'materials']);
        },
        deny: () => {
        }
      })
    }
    else {
      this.router.navigate(['/routings', 'materials']);
    }
  }

  setBOMAvailableForMaterial(bomCreatable: boolean) {

    this.bomCreatable = bomCreatable;
    this.bomEditable = bomCreatable;
    if (this.material.pk) {
      this.bomService.getBOMs({material_id: this.material.pk}).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          if (!this.bomCreatable) {
            this.bomEditable = true;
          }
          this.routingCreatable = true;
        }
      });
    }
  }
}
