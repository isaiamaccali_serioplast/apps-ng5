import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {DialogComponent} from "../../../../../../shared/material/dialog/dialog.component";
import {_getMaterialType, _MATERIAL_TYPES, MaterialTypesService} from "../../../../../_common/services/material-type.service";
import {Routing, RoutingsService} from "../../../../../_common/services/routings.service";
import {Site, SitesService} from "../../../../../_common/services/sites.service";
import {TechnologyLink, TechnologyLinkService} from "../../../../../_common/services/technology-link.service";
import {Material, MaterialsService} from "../../../../../_common/services/materials.service";
import {PlantSAP, PlantSAPService} from "../../../../../_common/services/plant-flow-service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {ContentTypeService} from "../../../../../_common/services/content-type.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {RoutingStatusLink, RoutingStatusLinkService} from "../../../../../_common/services/routing-status-link.service";
import {RoutingProductionVersion, RoutingProductionVersionService} from "../../../../../_common/services/routing-production-version-service";
import {RoutingOperation, RoutingOperationActivity_SAP, RoutingsOperationsService} from "../../../../../_common/services/routings-operations.service";
import {RoutingOperationActivityService} from "../../../../../_common/services/routing-operation-activity.service";
import {BOM, BOMItem, BOMService} from "../../../../../_common/services/bom.service";
import {Workcenter, WorkcentersService} from "../../../../../_common/services/workcenters.service";
import {WorkcenterActivity_SAP, WorkcenterActivityService} from "../../../../../_common/services/workcenter-activity-service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {arrayDiff, arrayDistinct, buildErrorMessage, cloneObject, fixValue, parseSerionetRespToData} from "../../../../../../shared/utils";
import {HeaderConf, TableRowAction, TableRowActions} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {StepperComponent} from "../../../../../../shared/material/stepper/stepper.component";
import {MaterialPlantsService} from "../../../../../_common/services/material-plant.service";
import {Supplier, SupplierService} from "../../../../../_common/services/suppliers.service";
import {PurchaseOrganization, PurchaseOrganizationsService, PurchaseOrganizationSupplier} from "../../../../../_common/services/purchase-organization.service";
import {MaterialGroup, MaterialGroupsService} from "../../../../../_common/services/material-group.service";
import {MeasureUnit, MeasureUnitService} from "../../../../../_common/services/measure-unit.service";
import {MatSnackBar} from "@angular/material";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

export class RoutingPlant {
  site: Site;
  routings: Routing[];
}

@Component({
  selector: 'app-material-edit-routings',
  templateUrl: 'material-edit-routings.html',
  styleUrls: ['material-edit-routings.css']
})
export class MaterialEditRoutingsComponent extends BaseComponent implements OnInit {

  @ViewChild('bomDialog') bomDialog: DialogComponent;
  @ViewChild('operationDialog') operationDialog: DialogComponent;
  @ViewChild('cloneRoutingDialog') cloneRoutingDialog: DialogComponent;
  @ViewChild('cloneRoutingViewDialog') cloneRoutingViewDialog: DialogComponent;
  @ViewChild('routingRestorePV') routingRestorePV: DialogComponent;
  @ViewChild('organizationDialog') organizationDialog: DialogComponent;
  @Input() material: Material;
  matTypeMouldSetPK: number;
  routings: Routing[] = [];
  sites: Site[] = [];
  routingOperation: RoutingOperation;
  items = [];
  bomToShow: BOM;
  routingsActive: boolean = true;
  statuses: RoutingStatusLink[] = [];

  operationToShow: RoutingOperation;
  disableAddButton = false;

  routingPlants: RoutingPlant[] = [];

  availableActionsViewRouting: TableRowAction[] = [];

  materialToCloneSearch: string;

  materialGroups: MaterialGroup[] = [];
  supplierCodeFilter = "";
  supplierNameFilter = "";

  uoms: MeasureUnit[] = [];

  pvsToSelect: RoutingProductionVersion[] = [];
  pvToSelectMandatory = false;
  routingToRestore = new Routing();

  plants_sap: PlantSAP[] = [];
  allActivities: RoutingOperationActivity_SAP[] = [];

  wcCT: number;

  ready = false;

  constructor(private materialTypeService: MaterialTypesService, private routingsService: RoutingsService,
              private sitesService: SitesService, private technologyLinkService: TechnologyLinkService,
              private materialsService: MaterialsService, protected translationService: TranslationService,
              private bomService: BOMService, private snackBar: MatSnackBar, private workcentersService: WorkcentersService,
              private workcenterOperationsService: WorkcenterActivityService, private routingOperationActivityService: RoutingOperationActivityService,
              private productionVersionService: RoutingProductionVersionService, private routingOperationService: RoutingsOperationsService,
              private routingStatusLinkService: RoutingStatusLinkService, protected messageService: MessageService,
              protected loadingService: LoadingService, private contentTypeService: ContentTypeService,
              private plantFlowService: PlantSAPService, private materialPlantService: MaterialPlantsService,
              private purchaseOrganizationsService: PurchaseOrganizationsService, private suppliersService: SupplierService,
              private materialGroupsService: MaterialGroupsService, private measureUnitService: MeasureUnitService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    let view: TableRowAction = cloneObject(TableRowActions.VIEW);
    view.click = (r, e) => {
      this.viewRoutingToClone(r, e)
    };

    this.availableActionsViewRouting.push(view);

    let count = 0;
    this.loadingService.show();
    let callback = () => {
      count--;
      if (count == 0) {
        this.loadingService.hide();
        this.loadRoutings();
      }
    };

    this.measureUnitService.getMeasureUnits({is_active: true}).subscribe((data) => {
      let uoms = parseSerionetRespToData(data);
      this.uoms = this.convertUomLabels(uoms);
      callback();
    });
    count++;

    this.materialGroupsService.getMaterialGroups({is_active: true}).subscribe((data) => {
      this.materialGroups = parseSerionetRespToData(data);
      callback();
    });
    count++;

    this.materialTypeService.getMaterialType(this.material.material_type.pk).subscribe((data) => {
      this.material.material_type = data;
      callback();
    });
    count++;

    this.materialPlantService.getMaterialPlants({material_id: this.material.pk, is_active: true}).subscribe((resp) => {
      this.material.material_plants = parseSerionetRespToData(resp);
      callback();
    });
    count++;

    this.materialTypeService.getMaterialTypes({is_active: true, first_char: 'M'}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.matTypeMouldSetPK = data['results'][0]['pk'];
        callback();
      }
    });
    count++;

    this.routingStatusLinkService.getRoutingStatusLinks({is_active: true}).subscribe((data) => {
      this.statuses = parseSerionetRespToData(data);
      callback();
    });
    count++;

    this.plantFlowService.getPlants({}).subscribe((resp) => {
      if (resp.status == 200) {
        this.plants_sap = resp['response']['results'];
        callback();

      }
      else {
        this.handleError(resp);
      }
    });
    count++;

    this.bomService.getBOMs({is_active: true, material_id: this.material.pk}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let sites = [];
        data['results'].forEach((b: BOM) => {
          sites = sites.concat(b.sites);
        });
        sites = arrayDistinct(sites);
        let sitesPKS = arrayDistinct(sites.map((s: Site) => s.pk));
        sites = sitesPKS.map((i) => sites.filter((s) => s.pk == i)[0]);
        this.sites = sites;
        let count_ = this.sites.length;
        if (count_ > 0) {
          let callback_ = () => {
            count_--;
            if (count_ == 0) {
              callback();
            }
          };
          this.sites.forEach((s, index) => {
            this.sitesService.getSite(s.pk).subscribe((s_) => {
              this.sites[index] = s_;
              callback_();
            })
          })

        }
        else {
          callback();
        }
      }
      else {
        this.disableAddButton = true;
        this.ready = true;
        // this.snackBar.open(this.tr("NO_BOM_FOR_MATERIAL"), null, {duration: 2000});
      }
    });
    count++;

    this.contentTypeService.getContentTypes({model: 'workcenter', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.wcCT = data['results'][0].pk;
        callback();
      }
    });
    count++;
  }

  convertUomLabels(uoms: MeasureUnit[]) {
    let mapConvert = {
      Hour: 'H',
      Min: 'MIN',
      Day: 'DAY',
      Month: 'MON',
      Year: 'YR'
    };

    uoms.forEach((u) => {
      if (mapConvert[u.__str__]) {
        u.uom = mapConvert[u.__str__];
        u.__str__ = mapConvert[u.__str__];
      }
    });
    return uoms;
  }

  searchInProgress = false;

  loadRoutings() {
    if (this.searchInProgress) {
      return;
    }
    else {
      this.searchInProgress = true;
      this.routings = [];
      this.loadingService.show();

      this.routingOperationActivityService.getRoutingOperationActivities({
        Code: this.material.code,
      }).subscribe((resp) => {
        if (resp.status == 200) {
          this.allActivities = resp['response']['results'];
        }
        this.routingsService.getRoutings({
          is_active: this.routingsActive,
          material: this.material.pk,
          explode_all: true,
          ordering: 'group'
        }).subscribe((data) => {
          this.searchInProgress = false;
          if (data && data['count'] && data['count'] > 0) {
            let routings: Routing[] = data['results'];

            let count = routings.length;
            let callback = () => {
              count--;
              if (count == 0) {
                this.routings = routings;
                this.buildRoutingPlants();
                this.loadingService.hide();
                this.ready = true;
              }
            };
            routings.forEach((r: Routing) => {
              this.loadRoutingData(r, () => {
                callback();
              });
            });
          }
          else {
            this.loadingService.hide();
            this.routings = [];
            this.buildRoutingPlants();
            this.ready = true;
          }
        });
      });
    }
  }

  buildRoutingPlants() {
    this.routingPlants = [];
    this.sites.forEach((s) => {
      let rp = new RoutingPlant();
      rp.site = s;
      rp.routings = this.routings.filter((r) => r.site.pk == s.pk);
      this.routingPlants.push(rp);
    })
  }

  loadRoutingData(r: Routing, callback: any) {
    r.material = this.material;
    r.mandatory_pv = r.site.active_on_sap ? !!this.plants_sap.find((p) => p.Code == r.site.code).NewFlow : false;
    if (r.production_version && r.production_version.pk) {
      r.productionVersionBOMpk = r.production_version.bom_header.pk;
    }
    else {
      r.production_version = new RoutingProductionVersion();
      r.production_version.bom_header = new BOM();
    }
    let available_pv = r.available_pv;
    available_pv.forEach((pv: RoutingProductionVersion) => pv.bom_header = r.boms.filter((b: BOM) => b.pk == pv.bom_header.pk)[0]);
    let bom_used = available_pv.map((pv) => pv.bom_header.pk);
    let bom_available = r.boms.map((b: BOM) => b.pk);
    let diff = arrayDiff(bom_available, bom_used);
    diff = diff.map((pk) => r.boms.filter((b: BOM) => b.pk == pk)[0]);
    diff.forEach((b: BOM) => {
      let pv = new RoutingProductionVersion();
      pv.bom_header = b;
      pv.routing_header = r;
      available_pv.push(pv);
    });
    available_pv.sort((pv1, pv2) => {
      return parseInt(pv1.bom_header.alt_bom_id, 10) - parseInt(pv2.bom_header.alt_bom_id, 10);
    });
    r.available_pv = available_pv;
    this.loadOperationsData(r, callback);
  }

  loadOperationsData(r: Routing, callback: any) {

    let count = r.routing_operations.length;

    r.routing_operations.sort((ro1, ro2) => {
      return parseInt(ro1.operation, 10) - parseInt(ro2.operation, 10);
    });

    let c = () => {
      count--;
      if (count == 0) {
        callback();
      }
    };

    r.routing_operations.forEach((ro: RoutingOperation) => {
      ro.subcontracting = !!(ro.vendor && ro.vendor.pk && ro.material_group && ro.material_group.pk);
      if (!ro.subcontracting) {
        ro.activities = this.allActivities.filter((a) => (a.Plant == r.site.code) && (parseInt(a.Group, 10) == parseInt(r.group, 10)) && (parseInt(a.Operation, 10) == parseInt(ro.operation, 10)));
        ro.technologyLink = cloneObject(ro.work_center.technology);
        c();
      }
      else {
        fixValue(this.materialGroups, ro, 'material_group');
        this.suppliersService.getSupplier(ro.vendor.pk).subscribe((resp) => {
          ro.vendor = resp;
          c();
        });
      }
    });
  }

  handleSiteChangeRouting(r: Routing, callback?: any) {
    fixValue(this.sites, r, 'site');

    r.mandatory_pv = r.site.active_on_sap ? !!this.plants_sap.find((p) => p.Code == r.site.code).NewFlow : false;

    this.bomService.getBOMs({
      is_active: true,
      material_id: this.material.pk,
      site_list: r.site.pk
    }).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        r.boms = parseSerionetRespToData(data);
        if (r.pk) {
          this.productionVersionService.getRoutingProductionVersions({routing_header: r.pk}).subscribe((data) => {
            if (data && data['count'] && data['count'] > 0) {
              if (r.production_version && r.production_version.pk) {
                r.productionVersionBOMpk = r.production_version.bom_header.pk;
              }
              let available_pv = r.available_pv;
              available_pv.forEach((pv: RoutingProductionVersion) => pv.bom_header = r.boms.filter((b: BOM) => b.pk == pv.bom_header.pk)[0]);
              let bom_used = available_pv.map((pv) => pv.bom_header.pk);
              let bom_available = r.boms.map((b: BOM) => b.pk);
              let diff = arrayDiff(bom_available, bom_used);
              diff = diff.map((pk) => r.boms.filter((b: BOM) => b.pk == pk)[0]);
              diff.forEach((b: BOM) => {
                let pv = new RoutingProductionVersion();
                pv.bom_header = b;
                pv.routing_header = r;
                available_pv.push(pv);
              });
              r.available_pv = available_pv;
            }
            else {
              let available_pv = [];
              r.boms.forEach((b: BOM) => {
                let pv = new RoutingProductionVersion();
                pv.bom_header = b;
                pv.routing_header = r;
                available_pv.push(pv);
              });
              r.available_pv = available_pv;
              if (callback)
                callback();
              // if(r.available_pv.length>0)
              //   r.productionVersionBOMpk = r.available_pv[0].bom_header.pk;
            }
          });
        }
        else {
          let available_pv = [];
          r.boms.forEach((b: BOM) => {
            let pv = new RoutingProductionVersion();
            pv.bom_header = b;
            pv.routing_header = r;
            available_pv.push(pv);
          });
          r.available_pv = available_pv;
          if (callback)
            callback();
          // if(r.available_pv.length>0)
          //   r.productionVersionBOMpk = r.available_pv[0].bom_header.pk;
        }
      }
      else {
        this.snackBar.open(this.tr("NO_BOM_FOR_COMBINATION"), null, {duration: 2000});
      }
    });
  }

  editOperation(r: Routing, o: RoutingOperation) {
    this.routingOperation = cloneObject(o);
    this.routings.forEach((r_: Routing) => {
      r_.enable_add_item = false;
    });

    this.routingOperation.subcontracting = !!(this.routingOperation.vendor && this.routingOperation.vendor.pk && this.routingOperation.material_group && this.routingOperation.material_group.pk);

    if (!this.routingOperation.subcontracting) {
      // if (!o.activities || o.activities.length == 0) {
      //   //per evitare di sovrascrivere modifiche fatte
      //   let query = {
      //     Code: r.material.code,
      //     Plant: r.site.code,
      //     Group: r.group,
      //     Operation: o.operation
      //   };
      //   this.routingOperationActivityService.getRoutingOperationActivities(query).subscribe((resp) => {
      //     if (resp.status == 200) {
      //       o.activities = resp['response']['results'];
      //     }
      //   })
      // }

      if (!this.routingOperation.prt || !this.routingOperation.prt[0])
        this.routingOperation.prt = [new Material()];
    }

    this.routingOperation.status = "toSave";

    if (!this.routingOperation.uom)
      this.routingOperation.uom = new MeasureUnit();

    r.enable_add_item = true;
  }

  getMouldsets(r: Routing, ro: RoutingOperation) {
    this.materialsService.getMaterials({
      site_set: r.site.pk,
      material_type: this.matTypeMouldSetPK,
      is_active: true,
      technology: this.routingOperation.technologyLink.technology_id.pk,
      shape: (this.material.sub_model.shape ? this.material.sub_model.shape : null)
    }).subscribe((resp) => {
      ro.mouldsets = parseSerionetRespToData(resp);
    })
  }

  deleteOperation(o: RoutingOperation) {
    o.status = "toDelete";
  }

  getActiveOperations(r: Routing) {
    return r.routing_operations.filter((ro: RoutingOperation) => {
      return ro.status != "toDelete";
    })
  }

  enableAddOperationToRouting(r: Routing) {
    this.disableAllRoutings();
    this.initNewRoutingOperation(r);
    r.enable_add_item = true;
  }

  disableAddOperationToRouting(r: Routing) {
    r.enable_add_item = false;
  }

  disableAllRoutings() {
    this.routings.forEach((r) => {
      r.enable_add_item = false;
    })
  }

  initNewRoutingOperation(r: Routing) {
    this.routingOperation = new RoutingOperation();
    this.routingOperation.subcontracting = false;
    this.resetOpUom();
    this.routingOperation.work_center = new Workcenter();
    this.routingOperation.operation = this.calcNewOperationIndex(r);
    this.routingOperation.status = "toSave";
    this.routingOperation.technologyLink = new TechnologyLink();
    this.routingOperation.prt = [new Material()];
    this.routingOperation.employees = 0;
    this.routingOperation.base_quantity = this.material.material_type.bom_quantity;
    this.technologyLinkService.getTechnologyLinks({
      is_active: true,
      app: this.wcCT,
      site_wc: r.site.pk
    }).subscribe((data) => {
      this.routingOperation.technologies = parseSerionetRespToData(data);
    });
  }

  getWorkcenters(r: Routing, o: RoutingOperation, callback: any) {
    o.active_work_centers = [];
    this.workcentersService.getWorkcenters({
      is_active: true,
      site: r.site.pk,
      technology__technology_id: o.technologyLink.technology_id.pk
    }).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        o.active_work_centers = data['results'];
        callback();
      }
    })
  }

  calcNewOperationIndex(r: Routing) {
    if (r.routing_operations.length == 0) {
      return "10";
    }
    else {
      let indexes = r.routing_operations.map((ro: RoutingOperation) => parseInt(ro.operation, 10));
      let newIndex = Math.max.apply(Math, indexes) + 10;
      return newIndex + "";
    }
  }

  handleOperationTechnologyChange(r: Routing, ro: RoutingOperation) {
    fixValue(ro.technologies, ro, 'technologyLink');
    //Se è già stato salvato, queste informazioni vengono dal serializzatore
    this.getWorkcenters(r, ro, () => {
      this.getMouldsets(r, ro);
    });
  }

  handleOperationWorkcenterChange(r: Routing, ro: RoutingOperation) {
    fixValue(ro.active_work_centers, ro, 'work_center');
    this.workcenterOperationsService.getActivities({
      WorkCenter: ro.work_center.sap_code,
      Plant: r.site.code
    }).subscribe((resp) => {
      if (resp.status == 200) {
        let newActivities = resp.response.results.map((wc: WorkcenterActivity_SAP) => {
          let ac = new RoutingOperationActivity_SAP();
          ac.Plant = r.site.code;
          ac.Activity = wc.ActivityCode;
          ac.Description = wc.Description;
          ac.Code = r.material.code;
          ac.Group = r.group;
          ac.Operation = this.routingOperation.operation;
          ac.Unit = wc.Unit;
          ac.Value = "0";
          return ac;
        }).filter((ac: RoutingOperationActivity_SAP) => {
          return !(!ac.Activity && !ac.Description && !ac.Unit);
        });
        if (!ro.activities || ro.activities.length == 0) {
          ro.activities = newActivities;
        }
        else {
          let current_acts = ro.activities.map((roa) => roa.Activity);
          let new_acts = newActivities.map((roa) => roa.Activity);
          if (current_acts.length != new_acts.length) {
            ro.activities = newActivities;
          }
          else {
            let diff1 = arrayDiff(current_acts, new_acts);
            let diff2 = arrayDiff(new_acts, current_acts);
            if (diff1.length != 0 || diff2.length != 0) {
              ro.activities = newActivities;
            }
          }
        }
      }
    })
  }

  addOperationToRouting(r: Routing) {

    let process = () => {
      if (!this.routingOperation.pk) {
        let opID = this.routingOperation.operation;
        let match = r.routing_operations.filter((ro) => ro.operation == opID);
        if (match.length > 0) {
          r.routing_operations.forEach((element, index) => {
            if (this.routingOperation.operation == element.operation) {
              r.routing_operations[index] = cloneObject(this.routingOperation);
            }
          });
        }
        else {
          r.routing_operations.push(cloneObject(this.routingOperation));
        }
        //this.calcDescription(r);
      }
      else {
        r.routing_operations.forEach((element, index) => {
          if (this.routingOperation.pk == element.pk) {
            r.routing_operations[index] = cloneObject(this.routingOperation);
          }
        });
      }
      this.disableAddOperationToRouting(r);
    };

    if (!this.routingOperation.subcontracting) {
      this.workcentersService.getWorkcenter(this.routingOperation.work_center.pk).subscribe((wc) => {
        this.routingOperation.work_center = wc;
        process();
      })
    }
    else {
      process();
    }
  }

  saving = false;

  saveRouting(r: Routing) {
    if (this.saving) {
      return;
    }
    else {
      this.saving = true;
      this.loadingService.show();
      this.routingsService.saveRouting(r).subscribe((resp) => {
        this.saving = false;
        this.manageRespSave(resp, () => {
          this.loadRoutings();
        });
      })
    }
  }

  hasRoutingInEdit() {
    let routingsInEdit = false;
    this.routings.forEach((b) => {
      if (b.enable_add_item) {
        routingsInEdit = true;
      }
    });
    return routingsInEdit;
  }

  hasUnsavedRouting() {
    let hasUnSavedRouting = false;
    this.routings.forEach((b) => {
      if (!b.pk) {
        hasUnSavedRouting = true;
      }
    });
    return hasUnSavedRouting;
  }

  createRouting(s: Site) {
    let routing = new Routing();
    routing.routing_operations = [];
    routing.description = "";
    routing.site = s;
    this.handleSiteChangeRouting(routing, () => {
      routing.production_version = new RoutingProductionVersion();
      routing.production_version.bom_header = new BOM();
      routing.material = this.material;
      if (s.code == 'DITS')
        routing.status = cloneObject(this.statuses.filter((rs) => rs.sap_code == '4')[0]);
      else
        routing.status = cloneObject(this.statuses.filter((rs) => rs.sap_code == 'ZUT')[0]);
      routing.is_active = true;
      this.routings.push(routing);
      this.buildRoutingPlants();
    });
  }

  deleteRouting(r: Routing) {
    this.messageService.open({
      title: this.tr('CONFIRM'),
      message: this.tr('CONFIRM_DELETE'),
      confirm: () => {
        this.loadingService.show();
        this.routingsService.deleteRouting(r).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.loadRoutings();
          })
        })
      },
      deny: () => {
      }
    })
  }

  restoreRouting(r: Routing) {
    let r_: Routing = cloneObject(r);
    this.messageService.open({
      title: this.tr('CONFIRM'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {

        let case1 = () => {
          this.openPVSelection(r_, true);
        };

        let case2 = () => {
          this.openPVSelection(r_, false);
        };

        /*        Se il plant ha la pv obbligatoria, obbligare a selezionare una bom tra le disponibili prima di riattivare.
                  Se esiste già una pv non attiva e viene selezionata, riattivarla.
                  Se il plant non ha la pv obbligatoria o non è attivo su sap, se ne ha disattivate fare lo stesso prompt di prima, altrimenti proseguire.*/
        if (r.site.active_on_sap) {
          this.loadingService.show();
          this.plantFlowService.getPlant("'" + r_.site.code + "'").subscribe((resp) => {
            this.loadingService.hide();
            if (resp.status = 200) {
              let plant: PlantSAP = resp.response;
              if (plant.NewFlow) {
                case1();
              }
              else {
                case2();
              }
            }
            else {
              resp.context = "SAP";
              this.messageService.open({
                title: 'Error',
                error: true,
                message: buildErrorMessage(resp),
                autoClose: false
              });
            }
          })
        }
        else {
          case2();
        }
      },
      deny: () => {
      }
    })
  }

  openPVSelection(r: Routing, mandatory: boolean) {
    this.pvsToSelect = cloneObject(r.available_pv);
    this.routingToRestore = r;
    this.pvToSelectMandatory = mandatory;

    let u = this.routingRestorePV.onClose.subscribe(() => {
      u.unsubscribe();
      this.loadingService.show();
      this.routingsService.restoreRouting(r).subscribe((resp) => {
        this.manageRespSave(resp, () => {
          this.loadRoutings();
        })
      })
    });

    this.routingRestorePV.open();
  }

  selectPV(pv: RoutingProductionVersion) {
    this.routingToRestore.productionVersionBOMpk = pv.bom_header.pk;
    this.bomSelection(this.routingToRestore);
    this.routingRestorePV.close();
  }

  bomSelection(r: Routing) {
    r.available_pv.forEach((pv: RoutingProductionVersion) => {
      if (pv.bom_header.pk == r.productionVersionBOMpk) {
        if (r.production_version.pk) {
          r.productionVersionToDelete = cloneObject(r.production_version);
        }
        r.production_version = pv;
      }
    })
  }

  calcDescription(r: Routing) {
    let desc = [];
    if (r.site.code) {
      desc.push(r.site.code);
      if (this.material.code) {
        desc.push(this.material.code);
        if (r.production_version && r.production_version.bom_header && r.production_version.bom_header.alt_bom_id)
          desc.push(r.production_version.bom_header.alt_bom_id);
        if (r.routing_operations.length > 0) {
          desc.push.apply(desc, r.routing_operations.map((ro: RoutingOperation) => {
            if (!ro.subcontracting) {
              if (ro.prt && ro.prt[0] && ro.prt[0].code)
                return ro.work_center.sap_code + "-" + ro.prt[0].code;
              else
                return ro.work_center.sap_code
            }
            else {
              return ro.material_group.sap_code + "-" + ro.vendor.sap_code
            }
          }));
        }
      }
    }
    r.description = desc.join("-").substring(0, 40);
  }

  openBOMDetail(r: Routing) {
    let bom = r.production_version.bom_header;
    let mats = bom.bomitem_set.length;
    this.loadingService.show();
    bom.bomitem_set.forEach((bi: BOMItem, index: number) => {
      this.materialsService.getMaterial(bi.material).subscribe((material) => {
        bom.bomitem_set[index].material = material;
        mats--;
        if (mats == 0) {
          this.loadingService.hide();
          this.bomToShow = bom;
          this.bomDialog.open();
        }
      });
    });
  }

  handleStatusChangeRouting(r: Routing) {
    fixValue(this.statuses, r, 'status');
  }

  handleError(resp) {
    this.loadingService.hide();
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  isOperationComplete(r: Routing, ro: RoutingOperation) {
    if (!ro.subcontracting) {
      return ro.work_center && ro.work_center.pk && ro.base_quantity != null && ro.employees != null &&
        ro.description && (!this.showPRTMandatoryForRouting(r, ro)) && this.allActivitiesHaveUom(ro);
    }
    else {
      return ro.vendor && ro.vendor.pk && ro.material_group && ro.material_group.pk && ro.base_quantity && ro.uom && ro.uom.pk && ro.description;
    }
  }

  allActivitiesHaveUom(ro: RoutingOperation) {
    let isOk = true;
    ro.activities.forEach((a) => {
      if (a.Activity) {
        if (!a.Unit || this.getActUom(a).length == 0)
          isOk = false
      }
    });
    return isOk;
  }

  canSaveRouting(r: Routing) {
    return r.is_active && !r.enable_add_item && !(this.getActiveOperations(r).length == 0) && r.description &&
      (r.mandatory_pv ? (r.production_version && r.production_version.bom_header.pk ) : true);
  }

  viewOperation(r: Routing, o: RoutingOperation) {
    let routingOperation = cloneObject(o);

    if (!routingOperation.activities || routingOperation.activities.length == 0) {
      //per evitare di sovrascrivere modifiche fatte
      let query = {
        Code: r.material.code,
        Plant: r.site.code,
        Group: r.group,
        Operation: routingOperation.operation
      };
      this.routingOperationActivityService.getRoutingOperationActivities(query).subscribe((resp) => {
        if (resp.status == 200) {
          routingOperation.activities = resp['response']['results'];
          this.operationToShow = routingOperation;
          this.operationDialog.open();
        }
      })
    }
    else {
      this.operationToShow = routingOperation;
      this.operationDialog.open();
    }
  }

  fixPRT(ro: RoutingOperation) {
    ro.mouldsets.forEach((m) => {
      if (m.pk == this.routingOperation.prt[0].pk) {
        this.routingOperation.prt[0] = m;
      }
    })
  }

  shouldPRTBeSetForOperation(r: Routing, ro: RoutingOperation) {
    return r.mandatory_pv && ro.technologyLink.has_mouldset;
  }

  showPRTMandatoryForRouting(r: Routing, ro: RoutingOperation) {
    return this.shouldPRTBeSetForOperation(r, ro) && (!ro.prt || ro.prt.length == 0 || !ro.prt[0].pk);
  }

  isFromPlm(m: Material) {
    let matTypeConst = _getMaterialType(m);
    return matTypeConst == _MATERIAL_TYPES.COMPONENT || matTypeConst == _MATERIAL_TYPES.MOULDSET || matTypeConst == _MATERIAL_TYPES.BILLET;
  }

  openCloneDialog() {
    // this.cloneStepper.reset();
    this.materialToCloneSearch = "";
    this.materialToClone = null;
    this.routingsToClone = [];
    this.routingToClone = null;
    this.cloneRoutingDialog.open();
  }

  headerConfMaterialsToClone = [
    {
      label: this.tr("CODE"),
      valueKey: 'code'
    },
    {
      label: this.tr("DESCRIPTION"),
      valueKey: 'description'
    }
  ];

  headerConfRoutingsToClone = [
    {
      label: this.tr("DESCRIPTION"),
      valueKey: 'description'
    },
    {
      label: this.tr("SITE"),
      valueKey: 'site',
      useStr: true
    },
    {
      label: this.tr("GROUP"),
      valueKey: 'group'
    }
  ];

  getMaterialsToClone(page: number, results: number) {
    let query = {};
    query['page'] = page;
    query['results'] = results;
    query['is_active'] = true;
    query['valuated'] = true;
    query['material_type'] = this.material.material_type.pk;
    query['only_bom'] = 2;
    query['only_routing'] = 2;
    query['exclude'] = this.material.pk;
    if (this.materialToCloneSearch)
      query['search'] = this.materialToCloneSearch;

    return this.materialsService.getMaterials(query);
  }

  materialToClone: Material;
  routingsToClone: Routing[] = [];

  selectMaterialToClone(m: Material, stepper: StepperComponent) {
    this.materialToClone = m;
    this.loadingService.show();
    this.routingsService.getRoutings({
      is_active: true,
      material: this.materialToClone.pk,
      explode_all: true
    }).subscribe((resp) => {
      this.loadingService.hide();
      this.routingsToClone = parseSerionetRespToData(resp);
      let sites = this.material.material_plants.filter((mp) => mp.is_active).map((mp) => mp.site.pk);
      this.routingsToClone = this.routingsToClone.filter((r) => sites.indexOf(r.site.pk) != -1);
      if (this.routingsToClone.length > 0) {
        stepper.setIndex(1);
      }
      else {
        this.messageService.open({
          title: this.tr('ATTENTION'),
          message: "No available routing to clone from this material",
          confirm: () => {
          }
        })
      }
    })
  }

  routingToClone: Routing;

  routingToCloneView: Routing;

  viewRoutingToClone(r: Routing, e: MouseEvent) {
    e.stopPropagation();
    this.routingToCloneView = r;
    if (this.routingToCloneView.routing_operations && this.routingToCloneView.routing_operations.length > 0) {
      this.cloneRoutingViewDialog.open();
    }
    else {
      this.loadingService.show();
      this.routingOperationService.getRoutingOperations({routing_header: this.routingToCloneView.pk}).subscribe((resp) => {
        this.loadingService.hide();
        this.routingToCloneView.routing_operations = parseSerionetRespToData(resp);
        let count = this.routingToCloneView.routing_operations.length;
        let callback = () => {
          count--;
          if (count == 0) {
            this.cloneRoutingViewDialog.open();
          }
        };

        this.routingToCloneView.routing_operations.sort((ro1, ro2) => {
          return parseInt(ro1.operation, 10) - parseInt(ro2.operation, 10);
        });

        this.routingToCloneView.routing_operations.forEach((ro) => {
          ro.subcontracting = !!(ro.vendor && ro.vendor.pk && ro.material_group && ro.material_group.pk);

          if (!ro.subcontracting) {
            this.workcentersService.getWorkcenter(ro.work_center.pk).subscribe((resp) => {
              ro.work_center = resp;

              fixValue(ro.technologies, ro.work_center, 'technology');
              this.technologyLinkService.getTechnologyLink(ro.work_center.technology.pk).subscribe((tl) => {
                ro.work_center.technology = tl;
                ro.technologyLink = ro.work_center.technology;
                callback();
              });
            })
          }
          else {
            callback();
          }
        });
      })
    }
  }

  selectRoutingToClone(r: Routing, stepper: StepperComponent) {
    this.routingToClone = r;
    stepper.setIndex(2);
  }

  cloneRouting() {

    let count = 1;

    this.loadingService.show();

    let operationActivitiesMismatch = [];

    let callback = () => {
      count--;
      if (count == 0) {
        this.loadingService.hide();
        this.cloneRoutingDialog.close();

        let routing = new Routing();
        routing.routing_operations = this.routingToClone.routing_operations.map((o) => {
          let o_: RoutingOperation = cloneObject(o);
          delete o_.pk;
          o_.status = "toSave";
          return o_;
        });
        routing.routing_operations.sort((ro1, ro2) => {
          return parseInt(ro1.operation, 10) - parseInt(ro2.operation, 10);
        });
        routing.description = "";
        routing.site = cloneObject(this.routingToClone.site);
        routing.production_version = new RoutingProductionVersion();
        routing.production_version.bom_header = new BOM();
        routing.material = this.material;
        routing.is_active = true;
        this.handleSiteChangeRouting(routing, () => {
          if (routing.site.code == 'DITS')
            routing.status = cloneObject(this.statuses.filter((rs) => rs.sap_code == '4')[0]);
          else
            routing.status = cloneObject(this.statuses.filter((rs) => rs.sap_code == 'ZUT')[0]);
          if (routing.available_pv.length == 1) {
            routing.productionVersionBOMpk = routing.available_pv[0].bom_header.pk;
            this.bomSelection(routing);
          }
          this.routings.push(routing);
          this.buildRoutingPlants();
          if (operationActivitiesMismatch.length > 0) {
            this.messageService.open({
              title: this.tr('ATTENTION'),
              error: true,
              message: this.buildMismatchMessage(operationActivitiesMismatch),
              autoClose: false
            });
          }
        });

      }
    };

    count = this.routingToClone.routing_operations.length;
    this.materialsService.getMaterial(this.routingToClone.material.pk).subscribe((resp) => {
      this.routingToClone.material = resp;
      this.routingToClone.routing_operations.forEach((ro) => {

        ro.subcontracting = !!(ro.vendor && ro.vendor.pk && ro.material_group && ro.material_group.pk);

        if (!ro.subcontracting) {
          ro.technologyLink = ro.work_center.technology;
          let query = {
            Code: this.routingToClone.material.code,
            Plant: this.routingToClone.site.code,
            Group: this.routingToClone.group,
            Operation: ro.operation
          };
          this.routingOperationActivityService.getRoutingOperationActivities(query).subscribe((resp) => {
            if (resp.status == 200) {
              ro.activities = resp['response']['results'];

              this.workcenterOperationsService.getActivities({
                WorkCenter: ro.work_center.sap_code,
                Plant: this.routingToClone.site.code
              }).subscribe((resp) => {
                if (resp.status == 200) {
                  let newActivities = resp.response.results.map((wc: WorkcenterActivity_SAP) => {
                    let ac = new RoutingOperationActivity_SAP();
                    ac.Plant = this.routingToClone.site.code;
                    ac.Activity = wc.ActivityCode;
                    ac.Description = wc.Description;
                    ac.Code = this.routingToClone.material.code;
                    ac.Group = this.routingToClone.group;
                    ac.Operation = ro.operation;
                    ac.Unit = wc.Unit;
                    ac.Value = "0";
                    return ac;
                  }).filter((ac: RoutingOperationActivity_SAP) => {
                    return !(!ac.Activity && !ac.Description && !ac.Unit);
                  });

                  // if(parseInt(ro.operation,10)==10)
                  //   newActivities.pop();

                  if (!ro.activities || ro.activities.length == 0) {
                    ro.activities = newActivities;
                  }
                  else {
                    let current_acts = ro.activities.map((roa) => roa.Activity);
                    let new_acts = newActivities.map((roa) => roa.Activity);
                    if (current_acts.length != new_acts.length) {
                      ro.activities = newActivities;
                      operationActivitiesMismatch.push(ro.operation);
                    }
                    else {
                      let diff1 = arrayDiff(current_acts, new_acts);
                      let diff2 = arrayDiff(new_acts, current_acts);
                      if (diff1.length != 0 || diff2.length != 0) {
                        ro.activities = newActivities;
                        operationActivitiesMismatch.push(ro.operation);
                      }
                    }
                  }
                }
                callback();
              })

            }
          });

        }
        else {
          fixValue(this.materialGroups, ro, 'material_group');
          callback();
        }
      });
    })
  }

  headerConfSuppliers: HeaderConf[] = [
    {
      valueKey: 'Supplier',
      label: 'Supplier'
    },
    {
      valueKey: 'Name',
      label: 'Name'
    }
  ];

  clearSupplier() {
    this.routingOperation.vendor = new Supplier();
  }

  purchaseOrganization: PurchaseOrganization;

  openOrganizationDialog(r: Routing) {
    this.loadingService.show();
    this.plantFlowService.getPlant("'" + r.site.code + "'").subscribe((resp) => {
      if (resp.status == 200) {
        let p: PlantSAP = resp.response;
        if (p.PurchaseOrganization) {
          this.purchaseOrganizationsService.getPurchaseOrganization("'" + p.PurchaseOrganization + "'", {urlParameters: {$expand: 'SupplierLink'}}).subscribe((resp) => {
            this.loadingService.hide();
            if (resp.status == 200) {
              this.purchaseOrganization = resp.response;
              this.supplierCodeFilter = "";
              this.supplierNameFilter = "";

              this.organizationDialog.open();
            }
            else {
              resp.context = 'SAP';
              this.messageService.open({
                title: this.tr('ERROR'),
                error: true,
                message: buildErrorMessage(resp),
                autoClose: false
              });
            }
          })
        }
        else {
          resp.context = 'SAP';
          this.messageService.open({
            title: this.tr('ERROR'),
            error: true,
            message: "No available purchase organization for selected plant",
            autoClose: false
          });
        }
      }
      else {
        resp.context = 'SAP';
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  filterSuppliers(ss: PurchaseOrganizationSupplier[]) {
    return ss.filter((s) => {
      if (this.supplierCodeFilter) {
        if (this.supplierNameFilter) {
          return s.Supplier.indexOf(this.supplierCodeFilter) != -1 && s.Name.indexOf(this.supplierNameFilter) != -1
        }
        else {
          return s.Supplier.indexOf(this.supplierCodeFilter) != -1
        }
      }
      else {
        if (this.supplierNameFilter) {
          return s.Name.toLowerCase().indexOf(this.supplierNameFilter.toLowerCase()) != -1
        }
        else {
          return true;
        }
      }
    })
  }

  selectedSupplier(s: PurchaseOrganizationSupplier) {
    this.suppliersService.getSuppliers({sap_code: s.Supplier}).subscribe((resp) => {
      let suppliers: Supplier[] = parseSerionetRespToData(resp);
      if (suppliers.length == 1) {
        this.routingOperation.vendor = suppliers[0];
        this.organizationDialog.close();
      }
      else {
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: "Invalid supplier selection",
          autoClose: false
        });
      }
    });
  }

  resetOpUom() {
    if (!this.routingOperation.subcontracting) {
      this.routingOperation.uom = cloneObject(this.material.uom);
      this.routingOperation.material_group = new MaterialGroup();
      this.routingOperation.vendor = new Supplier();
    }
    else {
      this.routingOperation.uom = new MeasureUnit();
      this.routingOperation.material_group = new MaterialGroup();
      this.routingOperation.vendor = new Supplier();
    }
  }

  fixMatGroup() {
    fixValue(this.materialGroups, this.routingOperation, 'material_group')
  }

  fixUom() {
    fixValue(this.uoms, this.routingOperation, 'uom')
  }

  roHasMainData(routingOperation: RoutingOperation) {
    if (routingOperation.subcontracting) {
      return routingOperation.material_group && routingOperation.material_group.pk && routingOperation.vendor && routingOperation.vendor.pk;
    }
    else {
      return routingOperation.technologyLink && routingOperation.technologyLink.pk && routingOperation.work_center && routingOperation.work_center.pk;
    }
  }

  getActUom(a: RoutingOperationActivity_SAP) {
    let uom = this.uoms.filter((u) => u.__str__ == a.Unit)[0];
    if (uom) {
      return this.uoms.filter((u) => u.type == uom.type);
    }
    else {
      return [];
    }
  }

  buildMismatchMessage(operationActivitiesMismatch) {
    let content = [];
    content.push("Some operations have mismatching activities from workcenter. Check following operations: ");
    content.push("<ul>");
    operationActivitiesMismatch.forEach((o) => {
      content.push("<li><b>" + o + "</b></li>");
    });
    content.push("</ul>");
    return content.join("");
  }

  reload() {
    this.loadRoutings();
  }
}


