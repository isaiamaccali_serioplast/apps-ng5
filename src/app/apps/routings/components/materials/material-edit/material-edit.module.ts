import {NgModule} from "@angular/core";
import {MaterialEditComponent} from "./material-edit.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {MaterialEditPropertiesComponent} from "./material-edit-properties/material-edit.properties";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {MaterialEditMainInfoComponent} from "./material-edit-main-info/material-edit-main-info";
import {PkTransformerModule} from "../../../../../shared/components/pk-transformer/pk-transformer.module";
import {MaterialEditRoutingsComponent} from "./material-edit-routings/material-edit-routings";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule, PkTransformerModule],       // module dependencies
  declarations: [MaterialEditComponent, MaterialEditRoutingsComponent],   // components and directives
  exports: [],
  providers: []                    // services
})
export class MaterialEditModule {
}


