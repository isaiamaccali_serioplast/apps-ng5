import {Component, OnInit, ViewChild} from "@angular/core";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {Router} from "@angular/router";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {MaterialType, MaterialTypesService} from "../../../../_common/services/material-type.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MaterialsListQueryService} from "../../../services/materials-list-query-service";
import {
  MaterialsAuthorizationsService, SERIOMOULD_RD, SERIOPLAST_RD_PROCESS
} from "../../../services/materials-authorizations.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {cloneObject, parseSerionetRespToData, sortArrayAlphabetically} from "../../../../../shared/utils";
import {
  HeaderConf,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSubtypesLinkService} from "../../../../_common/services/material-subtype-link.service";
import {Observable} from "rxjs";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-materials-list',
  templateUrl: 'materials-list.component.html',
  styleUrls: ['materials-list.component.css']
})
export class MaterialsListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('materialsTable') materialsTable: TableWithPagesComponent;
  material: Material;
  siteFilter: number;
  materialType: MaterialType;
  materialTypes: MaterialType[] = [];
  sites: Site[] = [];
  code_or_description: string;
  valuated = true;
  isActive = true;
  loading = true;
  resultsPerPage = 10;
  hasCompanyPositions: HasCompanyPosition[] = [];

  noItemsMessage = this.tr("NO_MATERIALS_FOUND");
  headerConf: HeaderConf[] = [
    {
      label: this.tr("CODE"),
      valueKey: 'code'
    },
    {
      label: this.tr("DESCRIPTION"),
      valueKey: 'description'
    },
    {
      label: this.tr("TYPE"),
      valueKey: 'material_type',
      useStr: true
    },
    {
      label: this.tr("SUBTYPE"),
      valueFunction: (m: Material) => {
        if (!m.sub_model.subtype) {
          return "-";
        }
        else {
          return m.sub_model.subtype.__str__;
        }
      }
    }
  ];

  workcenter_search: string;

  ready = false;

  constructor(private materialsService: MaterialsService, private router: Router,
              private sitesService: SitesService, private materialTypesService: MaterialTypesService,
              protected translationService: TranslationService, private materialsListQueryService: MaterialsListQueryService,
              private materialsAuthorizationsService: MaterialsAuthorizationsService, private hasCompanyPositionService: HasCompanyPositionService,
              private loginService: LoginService, private materialSubtypeLinksService: MaterialSubtypesLinkService) {
    super(translationService);
  }

  ngOnInit() {

    this.materialTypesService.getMaterialTypes({ordering: 'description', is_active: true}).subscribe((data) => {
      if (data['count'] && data['count'] > 0) {
        this.materialTypes = data['results'];


        this.hasCompanyPositionService.getHasCompanyPositions({
          currents_for_user: this.loginService.user.pk,
          is_active: true
        }).subscribe((response) => {
          if (response['results'] && response['results'].length > 0) {
            this.hasCompanyPositions = response['results'];

            let isRD = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOPLAST_RD_PROCESS);
            let sm = this.materialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, SERIOMOULD_RD);

            let plmMatTypes = this.materialTypes.filter((mt) => {
              return mt.sap_code == "ZBIL" || mt.sap_code == "ZFHM" || mt.sap_code == "ZIBE";
            });

            let otherMatTypes = this.materialTypes.filter((mt) => {
              return mt.sap_code != "ZBIL" && mt.sap_code != "ZFHM" && mt.sap_code != "ZIBE";
            });

            let types: MaterialType[] = [];

            if (isRD)
              types = types.concat(otherMatTypes);

            if (sm) {
              types = types.concat(plmMatTypes);
            }

            sortArrayAlphabetically(types, 'description');
            this.materialTypes = types;

            this.sitesService.getSites({is_active: true}).subscribe((data) => {
              this.sites = parseSerionetRespToData(data);
              this.ready = true;
              let query = this.materialsListQueryService.getQuery();
              if (query) {
                this.restoreQueryData(query);
              }
            })
          }
        });

      }
    });


  }

  restoreQueryData(query: any) {
    if (query['site'])
      this.siteFilter = query['site'];
    if (query['type'])
      this.materialType = query['type'];
    if (query['search'])
      this.code_or_description = query['search'];
    if (query['routing_wc'])
      this.workcenter_search = query['routing_wc'];
    if (query['valuated'] != null)
      this.valuated = query['valuated'];
    if (query['is_active'] != null)
      this.isActive = query['is_active'];
    if (query['currentPage'] != null)
      this.materialsTable.setPage(query['currentPage']);
    if (query['resultsPerPage'] != null)
      this.resultsPerPage = query['resultsPerPage'];
  }

  saveQueryData() {
    let query = {};
    query['site'] = this.siteFilter;
    query['type'] = this.materialType;
    query['search'] = this.code_or_description;
    query['routing_wc'] = this.workcenter_search;
    query['valuated'] = this.valuated;
    query['is_active'] = this.isActive;
    query['currentPage'] = this.materialsTable.getPage();
    query['resultsPerPage'] = this.resultsPerPage;
    this.materialsListQueryService.saveQuery(cloneObject(query));
  }

  loadMaterials(page?: number, results?: number) {

    let query = {};
    if (this.siteFilter)
      query['site_set'] = this.siteFilter;
    if (this.materialType)
      query['material_type'] = this.materialType;
    else
      query['material_type_list'] = this.materialTypes.map((mt) => mt.pk).join("|");
    if (this.code_or_description)
      query['search'] = this.code_or_description;
    if (this.workcenter_search)
      query['routing_wc'] = this.workcenter_search;
    // if (this.valuated != null)
    //   query['valuated'] = this.valuated;
    query['valuated'] = true;
    if (this.isActive != null)
      query['is_active'] = this.isActive;

    query['ordering'] = 'code';
    query['only_bom'] = 2;

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    this.saveQueryData();

    return this.materialsService.getMaterials(query);
  }

  editMaterial(m: Material) {
    this.router.navigate(["routings", "materials", "detail", m.pk, 'edit']);
  }

  expandDataSubTypes(materials: Material[]) {
    return Observable.create((observer) => {

      let expandST = (ms, index, callback) => {
        if (ms[index]) {
          let m = ms[index];
          if (m.sub_model.subtype) {
            this.materialSubtypeLinksService.getMaterialSubtypeLink(m.sub_model.subtype).subscribe((s) => {
              if (s.pk)
                m.sub_model.subtype = s;
            })
          }
          expandST(ms, index + 1, callback);
        }
        else {
          callback();
        }
      };
      expandST(materials, 0, () => {
        observer.next(materials);
        observer.complete();
      })
    });
  }

  reloadList() {
    this.materialsTable.setPage(1);
    this.materialsTable.reload();
  }

}
