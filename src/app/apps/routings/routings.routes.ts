import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {MaterialsListComponent} from "./components/materials/materials-list/materials-list.component";
import {MaterialEditComponent} from "./components/materials/material-edit/material-edit.component";
import {MaterialViewComponent} from "./components/materials/material-view/material-view.component";
import {ShapeListComponent} from "./components/shapes/shapes-list/shape-list.component";
import {ShapeViewComponent} from "./components/shapes/shape-view/shape-view.component";
import {ShapeEditComponent} from "./components/shapes/shape-edit/shape-edit.component";
import {RoutingsIndexComponent} from "./components/index/routings-index.component";

/**
 * Root route, uses lazy loading to improve performance
 * @type {{path: string; loadChildren: string; canActivate: AuthGuard[]}[]}
 */
export const routingsRoutesRoot: Routes = [
  {
    path: 'routings',
    loadChildren: './routings.module.ts#RoutingsModule',
    canActivate: [AuthGuard]
  }
]

// Route Configuration
export const routingsRoutes: Routes = [
  {
    path: '',
    component: RoutingsIndexComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'materials',
        pathMatch:'full',
        component: MaterialsListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'materials/detail/:id/edit',
        component: MaterialEditComponent,
        canActivate: [AuthGuard]
      }

    ]
  }
];
