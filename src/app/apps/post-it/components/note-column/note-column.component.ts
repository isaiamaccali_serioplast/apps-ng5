import {Component, OnInit, Input, SimpleChange} from "@angular/core";
import {Router, ActivatedRoute, NavigationExtras} from "@angular/router";
import {Note} from "../../models/note";
import {NoteService} from "../../services/note.service";
import {getUrlDate, parseSerionetRespToData} from "../../../../shared/utils";

@Component({
  selector: 'sp-note-column',
  templateUrl: 'note-column.component.html',
  styleUrls: ['note-column.component.css']
})
export class NoteColumnComponent implements OnInit {

  @Input() sitePK: number;
  @Input() date: Date;
  @Input() dayDiff: number;
  notes: Note[];
  today: Date;
  isToday: boolean;
  loading = true;


  constructor(private notesService: NoteService, private router: Router, private activatedRoute:ActivatedRoute) {
    this.notes = [];
    this.dayDiff = 0;
    this.today = new Date();
  }

  //I listen to the changes in the components props and reload the data
  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if (changes['date']) {
      if (changes['date'].currentValue) {
        let dateInObj = new Date(changes['date'].currentValue.toString());
        dateInObj.setDate(dateInObj.getDate() - this.dayDiff);
        this.date = dateInObj;
      }
    }
    if (changes['sitePK']) {
      if (changes['sitePK'].currentValue) {
        this.sitePK = changes['sitePK'].currentValue;
      }
    }
    if (changes['dayDiff']) {
      if (!isNaN(changes['dayDiff'].currentValue) && !isNaN(changes['dayDiff'].previousValue)) {
        let tempDate = new Date(this.date.getTime());
        tempDate.setDate(tempDate.getDate() + parseInt(changes['dayDiff'].previousValue,10));
        tempDate.setDate(tempDate.getDate() - parseInt(changes['dayDiff'].currentValue,10));
        this.date = tempDate;
        this.reload();
      }
    }
  }

  ngOnInit() {
    if (this.date && this.sitePK)
      this.reload();
  }

  reload() {
    this.loading = true;
    this.isToday = (this.date.toDateString() == this.today.toDateString());
    this.notesService.getNotes(this.date, this.sitePK).subscribe((data)=> {
      this.notes = parseSerionetRespToData(data);
      this.loading = false;
    });
  }

  getUrlDate(date: Date) {
    return getUrlDate(date);
  }

  selectNote(note: Note) {
    let date_ = new Date(this.date.getTime());
    date_.setDate(date_.getDate() + parseInt(this.dayDiff + "",10));
    this.activatedRoute.queryParams.subscribe((params)=>{
      let params_ = {};
      for(let key in params){
        if(params.hasOwnProperty(key))
          params_[key] = params[key];
      }
      params_['noteid'] = note.pk;
      let navigationExtras: NavigationExtras = {
        queryParams: params_
      };
      this.router.navigate(["post-it"], navigationExtras)
    })
  }

  createNote(){
    this.activatedRoute.queryParams.subscribe((params)=>{
      let params_ = {};
      for(let key in params){
        if(params.hasOwnProperty(key))
          params_[key] = params[key];
      }
      let navigationExtras: NavigationExtras = {
        queryParams: params_
      };
      this.router.navigate(["post-it/create"], navigationExtras)
    })
  }
}
