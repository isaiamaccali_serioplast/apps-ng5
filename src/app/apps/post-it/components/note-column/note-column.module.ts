import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {NoteModule} from "../note-detail/note/note.module";
import {NoteColumnComponent} from "./note-column.component";
import {SerioplastCommonModule} from "../../../_common/common.module";

@NgModule({
  imports: [SharedModule,NoteModule, SerioplastCommonModule],       // module dependencies
  declarations: [NoteColumnComponent],   // components and directives
  exports:[NoteColumnComponent],
  providers: []                    // services
})
export class NoteColumnModule { }

