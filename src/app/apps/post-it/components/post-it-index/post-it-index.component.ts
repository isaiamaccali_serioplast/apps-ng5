import {Component, OnInit, SimpleChange, OnChanges} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {Router, ActivatedRoute, NavigationExtras} from "@angular/router";
import {Note} from "../../models/note";
import {Column} from "../note-column/column";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {NoteService} from "../../services/note.service";
import {IdleService} from "../../../../shared/idle/idle.service";
import {getUrlDate} from "../../../../shared/utils";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {Site} from "../../../_common/services/sites.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sp-post-it-index',
  templateUrl: 'post-it-index.component.html',
  styleUrls: ['post-it-index.component.css']
})
export class PostItIndexComponent extends TranslatableComponent implements OnInit, OnChanges {

  date: Date;
  today: Date;
  sites: Site[];
  sitePK: number;
  selectedNote: Note;
  columns: Column[];
  movingRight = false;
  movingLeft = false;
  minIndex = 2;
  // ready = false;

  SWIPE_ACTION = {LEFT: 'swipeleft', RIGHT: 'swiperight'};

  constructor(private idleService: IdleService, private router: Router, private sidenavService: SideNavService,
              private activatedRoute: ActivatedRoute, private titleService: Title, private notesService: NoteService,
              protected translationService: TranslationService) {
    super(translationService);
    this.columns = [];
    this.titleService.setTitle('Post-It - Serioplast');
    this.today = new Date();
    this.sidenavService.leftMenuEnabled = true;
    this.sidenavService.appMenuEnabled = true;
    this.selectedNote = new Note();
    this.date = new Date();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.sitePK = params['siteid'];
      let date = params['date'];
      let createCols = true;
      if (date) {
        this.date = new Date(date);
        if (createCols) {
          this.recreateColumns();
        }
      }
      else {
        this.changeRoute();
      }

      let noteId = params['noteid'];
      if (noteId && !isNaN(parseInt(noteId,10))) {
        this.notesService.getNote(noteId).subscribe((note) => {
          if (note.pk) {
            this.selectedNote = note;
            this.sidenavService.rightMenu = true;
          }
        })
      }
    });
    this.idleService.setTimeoutSeconds(300);
  }

  recreateColumns() {

    this.columns = [];
    if (this.date && this.sitePK) {
      this.minIndex = 2;
      for (let i = 0; i < 4; i++) {
        this.columns.push({date: this.date, site: this.sitePK, dayDiff: this.minIndex - i})
      }
    }
  }

  previous() {
    if (!this.movingLeft) {
      this.movingLeft = true;
      this.movingRight = false;

      setTimeout(()=> {
        let lastCol = this.columns.pop();
        lastCol.dayDiff = lastCol.dayDiff + 4;
        this.columns.unshift(lastCol);
        this.movingLeft = false;
        this.movingRight = false;
        this.date.setDate(this.date.getDate() - 1);
        this.minIndex++;
        this.changeRoute();
      }, 1000)
    }
  }

  next() {
    if (!this.movingRight) {
      this.movingLeft = false;
      this.movingRight = true;

      setTimeout(()=> {
        let firstCol = this.columns.shift();
        firstCol.dayDiff = firstCol.dayDiff - 4;
        this.columns.push(firstCol);
        this.movingLeft = false;
        this.movingRight = false;
        this.date.setDate(this.date.getDate() + 1);
        this.minIndex--;
        this.changeRoute();
      }, 1000)
    }
  }

  hideNoteDetail() {
    this.selectedNote = new Note();
    let col_ = this.columns;
    this.columns = [];
    setTimeout(()=>{
      this.columns = col_;
    }, 100);
    this.changeRoute();
  }

  //I listen to the changes in the components props and reload the data
  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['dateString']) {
      if (changes['dateString'].currentValue) {
        this.date = new Date(changes['dateString'].currentValue);
        this.changeRoute();
      }
    }
    if (changes['plant']) {
      if (changes['plant'].currentValue) {
        this.recreateColumns();
      }
    }
  }

  queryChange() {
    // this.date = new Date(this.dateString);
    this.changeRoute();
  }

  dateChange() {
    // this.date = new Date(this.dateString);
    this.changeRoute();
    this.recreateColumns();
  }

  // toggleLeftMenu() {
  //   this.sidenavService.leftMenu = !this.sidenavService.leftMenu;
  // }
  //
  // toggleRightMenu() {
  //   this.sidenavService.rightMenu = !this.sidenavService.rightMenu;
  // }

  closeLeftMenuCallback() {
    this.sidenavService.leftMenu = false;
  }

  openLeftMenuCallback() {
    this.sidenavService.leftMenu = true;
  }

  closeRightMenuCallback() {
    this.sidenavService.rightMenu = false;
    this.hideNoteDetail();
  }

  openRightMenuCallback() {
    this.sidenavService.rightMenu = true;
  }

  // action triggered when user swipes
  swipe(action = this.SWIPE_ACTION.RIGHT) {
    if (action == this.SWIPE_ACTION.LEFT) {
      this.next();
    }
    else {
      if (action == this.SWIPE_ACTION.RIGHT) {
        this.previous();
      }
    }
  }

  changeRoute() {
    let urlParams = {};
    if (this.sitePK) {
      urlParams["siteid"] = this.sitePK;
    }
    if (this.date) {
      urlParams["date"] = getUrlDate(this.date);
    }
    urlParams["noteid"] = "a";
    let navigationExtras: NavigationExtras = {
      queryParams: urlParams,
      queryParamsHandling:"merge",
      replaceUrl:true
    };
    urlParams["ts"] = new Date().getTime();
    this.router.navigate(["post-it"],navigationExtras);
  }

  siteChange(s: Site) {
    this.sitePK = s.pk;
    this.queryChange();
  }
}
