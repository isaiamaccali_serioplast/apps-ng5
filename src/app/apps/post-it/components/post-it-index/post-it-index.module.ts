import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {NoteFullModule} from "../note-detail/note-full/note-full.module";
import {NoteAddModule} from "../note-add/note-add.module";
import {NoteColumnModule} from "../note-column/note-column.module";
import {PostItIndexComponent} from "./post-it-index.component";
import {NoteService} from "../../services/note.service";

@NgModule({
  imports: [SharedModule,NoteFullModule, NoteColumnModule, NoteAddModule],       // module dependencies
  declarations: [PostItIndexComponent],   // components and directives
  exports:[PostItIndexComponent],
  providers: [NoteService]                    // services
})
export class PostItIndexModule { }

