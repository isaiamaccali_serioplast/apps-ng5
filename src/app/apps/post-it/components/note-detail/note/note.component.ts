import {Component, OnInit, Input} from "@angular/core";
import {Note} from "../../../models/note";
import {CommentsService} from "../../../../_common/services/comments.service";
import {parseSerionetRespToData} from "../../../../../shared/utils";

@Component({
  selector: 'note',
  templateUrl: 'note.component.html',
  styleUrls: ['note.component.css']
})
export class NoteComponent implements OnInit {

  @Input() note: Note;
  @Input() selectNote: any;
  comments: Comment[];

  constructor(private commentService: CommentsService) {
    this.comments = [];
  }

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.commentService.getComments({object_id:this.note.pk,content_type:this.note.content_type_id,is_active:true}).subscribe((data)=> {
      this.comments = parseSerionetRespToData(data)
    });
  }

  select() {
    this.selectNote(this.note);
  }

}
