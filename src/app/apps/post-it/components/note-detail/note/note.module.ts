import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {CommentListModule} from "../../comment-list/comment-list.module";
import {NoteComponent} from "./note.component";

@NgModule({
  imports: [SharedModule, CommentListModule],       // module dependencies
  declarations: [NoteComponent],   // components and directives
  exports:[NoteComponent],
  providers: []                    // services
})
export class NoteModule { }
