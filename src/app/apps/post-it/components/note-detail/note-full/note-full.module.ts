import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {NoteFullComponent} from "./note-full.component";
import {CommentListModule} from "../../comment-list/comment-list.module";
import {NoteService} from "../../../services/note.service";


@NgModule({
  imports: [SharedModule, CommentListModule],       // module dependencies
  declarations: [NoteFullComponent],   // components and directives
  exports:[NoteFullComponent],
  providers: []                    // services
})
export class NoteFullModule { }


