import {Component, OnInit, Input, SimpleChange} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Note} from "../../../models/note";
import {NoteService} from "../../../services/note.service";


@Component({
  selector: 'sp-note-full',
  templateUrl: 'note-full.component.html',
  styleUrls: ['note-full.component.css']
})
export class NoteFullComponent implements OnInit {

  @Input() note: Note;

  constructor(private route: ActivatedRoute, private noteService: NoteService) {
    this.note = new Note();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if (changes['note'] && changes['note'].currentValue) {
      this.note = changes['note'].currentValue;
    }
  }

}
