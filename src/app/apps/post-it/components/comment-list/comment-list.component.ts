import {Component, OnInit, Input} from "@angular/core";
import {Note} from "../../models/note";
import {CommentsService, SerionetComment} from "../../../_common/services/comments.service";
import {parseSerionetRespToData} from "../../../../shared/utils";


@Component({
  selector: 'sp-comment-list',
  templateUrl: 'comment-list.component.html',
  styleUrls: ['comment-list.component.css']
})
export class CommentListComponent implements OnInit {

  @Input() note: Note;
  comments:SerionetComment[];
  showAddComment = false;

  constructor(private commentsService: CommentsService) {
    this.comments = [];
  }

  ngOnInit() {
    this.reload();
    this.showAddComment = this.isCommentable(this.note);
  }

  reload() {
    this.commentsService.getComments({object_id:this.note.pk,content_type:this.note.content_type_id,is_active:true}).subscribe((data) => {
      this.comments = parseSerionetRespToData(data);
    });
  }

  isCommentable(note: Note) {
    let _MS_PER_DAY = 1000 * 60 * 60 * 24;

    // a and b are javascript Date objects
    let dateDiffInDays = (a, b) => {
      // Discard the time and time-zone information.
      let utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
      let utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

      return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    };

    let today = new Date();
    let noteTime = new Date(note.created_on+"");
    let diff = dateDiffInDays(noteTime, today);
    return diff < 2;
  }


}
