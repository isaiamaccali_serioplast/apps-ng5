import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {CommentDetailModule} from "../comment-detail/comment-detail.module";
import {CommentAddModule} from "../comment-add/comment-add.module";
import {CommentListComponent} from "./comment-list.component";


@NgModule({
  imports: [SharedModule,CommentDetailModule, CommentAddModule],       // module dependencies
  declarations: [CommentListComponent],   // components and directives
  exports:[CommentListComponent],
  providers: []                    // services
})
export class CommentListModule { }

