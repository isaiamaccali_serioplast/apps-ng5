import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, NavigationExtras} from "@angular/router";
import {NoteService} from "../../services/note.service";
import {Note} from "../../models/note";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";


@Component({
  selector: 'sp-note-add',
  templateUrl: 'note-add.component.html',
  styleUrls: ['note-add.component.css'],
  providers: [NoteService]
})
export class NoteAddComponent extends TranslatableComponent implements OnInit {

  note: Note;
  site: Site;
  currentDate: Date;

  constructor(private noteService: NoteService, private router: Router, private activatedRoute: ActivatedRoute,
              protected translationService:TranslationService,
              private sitesService: SitesService, private messageService:MessageService, private loadingService:LoadingService) {
    super(translationService);
    this.note = new Note();
    this.site = new Site();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['siteid'];
      this.sitesService.getSite(id).subscribe((site) => {
        if (site.pk) {
          this.site = site;
        }
        else {
          //If the plant id is not valid, the application goes back to the notes list (security check)
          this.router.navigate(["/post-it"]);
        }

      });
      let date = params['date'];
      if (date) {
        this.currentDate = new Date(date);
      }
      else {
        this.currentDate = new Date();
      }
    });
  }

  addNote() {
    this.note.site = this.site;
    this.loadingService.show();
    this.noteService.addNote(this.note).subscribe((note) => {
      this.loadingService.hide();
      if (note.pk) {
        this.activatedRoute.queryParams.subscribe((params)=>{
          let params_ = {};
          for(let key in params){
            if(params.hasOwnProperty(key))
              params_[key] = params[key];
          }
          params_['noteid'] = note.pk;
          let navigationExtras: NavigationExtras = {
            queryParams: params_
          };
          this.router.navigate(["post-it"], navigationExtras)
        })
      }
    })
  }

  back(){
    this.messageService.open({
      confirm:() => {

        this.activatedRoute.queryParams.subscribe((params)=>{
          let params_ = {};
          for(let key in params){
            if(params.hasOwnProperty(key))
              params_[key] = params[key];
          }
          let navigationExtras: NavigationExtras = {
            queryParams: params_
          };
          this.router.navigate(["post-it"],navigationExtras)
        })
      },
      deny:()=> {},
        title:this.tr("ATTENTION"),
        message:this.tr("ATTENTION_DATA_LOSS")
      }
    )
  }

}
