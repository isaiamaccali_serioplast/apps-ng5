import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {NoteAddComponent} from "./note-add.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [NoteAddComponent],   // components and directives
  providers: []                    // services
})
export class NoteAddModule { }

