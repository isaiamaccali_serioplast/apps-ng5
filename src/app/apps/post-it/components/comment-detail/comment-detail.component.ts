import {Component, OnInit, Input} from "@angular/core";
import {SerionetComment} from "../../../_common/services/comments.service";

@Component({
  selector: 'sp-comment-detail',
  templateUrl: 'comment-detail.component.html',
  styleUrls: ['comment-detail.component.css']
})
export class CommentDetailComponent implements OnInit {

  @Input() comment: SerionetComment;

  constructor() {}

  ngOnInit() {}

}
