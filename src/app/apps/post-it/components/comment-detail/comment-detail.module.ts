import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {CommentDetailComponent} from "./comment-detail.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [CommentDetailComponent],   // components and directives
  exports: [CommentDetailComponent],
  providers: []                    // services
})
export class CommentDetailModule {
}


