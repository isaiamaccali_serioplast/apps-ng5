import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {CommentAddComponent} from "./comment-add.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [CommentAddComponent],   // components and directives
  exports:[CommentAddComponent],
  providers: []                    // services
})
export class CommentAddModule { }


