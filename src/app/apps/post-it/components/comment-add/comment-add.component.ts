import {Component, OnInit, Input} from "@angular/core";
import {CommentsService, SerionetComment} from "../../../_common/services/comments.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sp-comment-add',
  templateUrl: 'comment-add.component.html',
  styleUrls: ['comment-add.component.css'],
  providers: []
})
export class CommentAddComponent extends TranslatableComponent implements OnInit {

  comment: SerionetComment;
  @Input() note;
  @Input() reload;

  constructor(private commentService: CommentsService, protected translationService:TranslationService) {
    super(translationService);
    this.comment = new SerionetComment();
  }

  ngOnInit() {
    this.comment.object_id = this.note.pk;
    this.comment.content_type = this.note.content_type_id;
  }

  addComment() {
    this.commentService.saveComment(this.comment).subscribe(() => {
      this.comment = new SerionetComment();
      this.comment.object_id = this.note.pk;
      this.comment.content_type = this.note.content_type_id;
      this.reload();
    })
  }
}
