import {Injectable} from "@angular/core";
import {Note} from "../models/note";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class NoteService {

  constructor(private serionetService: SerionetService) {
  }

  getNotes(date: any, sitePK: number) {
    let dateToSend = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join("-");

    let urlParams = new HttpParams();
    urlParams = urlParams.set('created_on', dateToSend);
    urlParams = urlParams.set('site', sitePK + "");

    let serionetOptions = new SerionetRequestOptions();
    serionetOptions.options = {
      params: urlParams
    };

    return this.serionetService.doGet("postit/notes", serionetOptions);
  }

  getNote(id: number) {
    let serionetOptions = new SerionetRequestOptions();
    serionetOptions.options = {};
    return this.serionetService.doGet("postit/notes/" + id + "/", serionetOptions);
  }

  addNote(note: Note) {
    let data = {
      title: note.title,
      content: note.content,
      site: note.site.pk
    };
    let serionetOptions = new SerionetRequestOptions();
    serionetOptions.options = {};

    return this.serionetService.doPost("postit/notes", data, serionetOptions);
  }
}

