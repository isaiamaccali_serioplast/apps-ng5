import {NgModule} from "@angular/core";
import {NoteService} from "./services/note.service";
import {postItRoutes} from "./post-it.routes";
import {SharedModule} from "../../shared/shared.module";
import {PostItIndexModule} from "./components/post-it-index/post-it-index.module";
import {RouterModule} from "@angular/router";


@NgModule({
  imports: [SharedModule, RouterModule.forChild(postItRoutes),PostItIndexModule],       // module dependencies
  declarations: [],   // components and directives
  exports:[RouterModule],
  providers: [NoteService]                    // services
})
export class PostItModule { }

