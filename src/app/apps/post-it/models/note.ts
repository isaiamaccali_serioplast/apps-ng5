import {User} from "../../_common/services/users.service";
import {Site} from "../../_common/services/sites.service";

export class Note {
  pk: number;
  __str__:string;
  content_type_id?: number;
  title?: string;
  created_on?: Date;
  created_by?: User;
  content?: string;
  site?:Site;
}
