import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {NoteAddComponent} from "./components/note-add/note-add.component";
import {PostItIndexComponent} from "./components/post-it-index/post-it-index.component";

// Route Configuration
export const postItRoutes: Routes = [

  {
    path: '',
    component: PostItIndexComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: NoteAddComponent,
    canActivate: [AuthGuard]
  }
];
