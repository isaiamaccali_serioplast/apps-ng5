import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {ProcurementPLMaterialsListComponent} from "./components/materials-list/materials-list.component";
import {PurchaseInfoRecordEditComponent} from "./components/purchase-info-record/purchase-info-record-edit/purchase-info-record-edit.component";
import {PurchaseInfoRecordListComponent} from "./components/purchase-info-record/purchase-info-record-list/purchase-info-record-list.component";
import {ProcurementPriceListIndexComponent} from "./components/procurement-pricelist-index.component";
import {PlannedPurchasePriceEditComponent} from "./components/planned-purchase-price/planned-purchase-price-edit/planned-purchase-price-edit.component";

/**
 * Root route, uses lazy loading to improve performance
 * @type {{path: string; loadChildren: string; canActivate: AuthGuard[]}[]}
 */
export const priceListRoutesRoot: Routes = [
  {
    path: 'procurement-pricelist',
    loadChildren: './procurement-pricelist.module.ts#ProcurementPriceListModule',
    canActivate: [AuthGuard]
  }
];

// Route Configuration
export const priceListRoutes: Routes = [
  {
    path: '',
    component: ProcurementPriceListIndexComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path:'',
        pathMatch:'full',
        component:ProcurementPLMaterialsListComponent
      },
      {
        path:':id',
        redirectTo:':id/pricelist',
      },
      {
        path:':id/planned-purchase-price',
        component:PlannedPurchasePriceEditComponent,
      },
      {
        path:':id/purchase-info-record',
        component:PurchaseInfoRecordListComponent
      },
      {
        path:':id/purchase-info-record/:mode',
        component:PurchaseInfoRecordEditComponent
      },
      {
        path:':id/purchase-info-record/:mode/:pirid',
        component:PurchaseInfoRecordEditComponent
      }
    ]
  }
];
