import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {SerioplastCommonModule} from "../../_common/common.module";
import {MaterialsListQueryServiceCustomerInfo} from "./materials-list/materials-list-query-service";
import {MaterialsListModule} from "./materials-list/materials-list.module";
import {ProcurementPriceListIndexComponent} from "./procurement-pricelist-index.component";
import {PlannedPurchasePriceEditModule} from "./planned-purchase-price/planned-purchase-price-edit/planned-purchase-price-edit.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule,MaterialsListModule,PlannedPurchasePriceEditModule],       // module dependencies
  declarations: [ProcurementPriceListIndexComponent],   // components and directives
  exports: [],
  providers: [MaterialsListQueryServiceCustomerInfo]                    // services
})
export class PriceListIndexModule {
}


