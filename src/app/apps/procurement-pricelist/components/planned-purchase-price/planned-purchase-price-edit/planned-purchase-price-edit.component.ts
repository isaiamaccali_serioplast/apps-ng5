import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {buildErrorMessage, cloneObject} from "../../../../../shared/utils";
import {MaterialPrice, MaterialPriceService} from "../../../services/material-prices.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'ppp-edit',
  templateUrl: 'planned-purchase-price-edit.component.html',
  styleUrls: ['planned-purchase-price-edit.component.css']
})

export class PlannedPurchasePriceEditComponent extends TranslatableComponent implements OnInit {

  materialId: string;
  ready = false;
  matPrices: MaterialPrice[] = [];

  constructor(protected translationService: TranslationService, private router: Router,
              private activatedRoute: ActivatedRoute, private messageService: MessageService,
              private loadingService: LoadingService, private materialPriceService: MaterialPriceService) {
    super(translationService);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.materialId = params['id'];
        this.reload();

      }
      else {
        this.back();
      }
    });
  }

  back() {
    this.router.navigate(['procurement-pricelist']);
  }

  parseFloat(n: number) {
    return parseFloat(n + "");
  }

  savePrice(mp) {
    let mp_ = new MaterialPrice();
    for (let key in mp) {
      if (mp.hasOwnProperty(key)) {
        mp_[key] = cloneObject(mp[key]);
      }
    }
    this.loadingService.show();
    this.materialPriceService.saveMaterialPrice(mp_).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status > 200 && resp.status < 300) {
        this.messageService.open({
          title: " ",
          message: this.tr('SAVE_COMPLETED'),
          autoClose: true,
          onClose: () => {
            this.reload();
          }
        });
      }
      else {
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  reload() {
    this.loadingService.show();
    this.materialPriceService.getMaterialPrices({Material: this.materialId}).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        this.matPrices = resp.response.results;
        this.matPrices.forEach((mp) => {
          mp.Price = parseFloat(mp.Price);
          mp.PriceUnit = parseFloat(mp.PriceUnit);
          if (mp.PriceUnit > 0) {
            mp.noEditUnit = true;
          }
          // if(mp.PriceDate)
          //   mp.PriceDate = new Date(sapDateToUrlDate(mp.PriceDate));
        });

      }
      else {
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  canSaveMP(mp: MaterialPrice) {
    return mp.Price != 0 && ((mp.PriceControl == 'S') ? mp.PriceDate : true);
  }
}
