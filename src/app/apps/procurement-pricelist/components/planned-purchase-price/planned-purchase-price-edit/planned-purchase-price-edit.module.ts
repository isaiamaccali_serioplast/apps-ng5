import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {PlannedPurchasePriceEditComponent} from "./planned-purchase-price-edit.component";
import {DateAdapter} from "@angular/material";
import {SAPDateAdapter, SPDateAdapter} from "../../../../../shared/material/date/date-adapter";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule],       // module dependencies
  declarations: [PlannedPurchasePriceEditComponent],   // components and directives
  exports: [PlannedPurchasePriceEditComponent],
  providers: [{provide: DateAdapter, useClass: SAPDateAdapter}]                    // services
})
export class PlannedPurchasePriceEditModule {}


