import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {ActivatedRoute, Router} from "@angular/router";
import {
  InfoRecordCondition, InfoRecordPeriod,
  InfoRecordScale, PurchaseInfoRecord,
  PurchaseInfoRecordService
} from "../../../services/purchase-info-record.service";
import {Incoterm, IncotermsService} from "../../../services/incoterms.service";
import {arrayIntersect, buildErrorMessage, cloneObject, formatNumber, getUrlDate} from "../../../../../shared/utils";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {MaterialPlantSAP, MaterialPlantsSAPService} from "../../../../_common/services/material-plant.service";
import {PlantSAP, PlantSAPService} from "../../../../_common/services/plant-flow-service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {
  HeaderConf, TableRowAction,
  TableRowActions, TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSAP, MaterialsSAPService} from "../../../../_common/services/materials.service";
import {SapDatePipe} from "../../../../../shared/pipes/sap-date.pipe";
import {PurchasingGroup, PurchasingGroupService} from "../../../services/purchase-group.service";
import {PurchaseCondition, PurchaseConditionsService} from "../../../services/purchase-conditions.service";
import {MatTabGroup} from "@angular/material";
import {
  PurchaseInfoRecordType,
  PurchaseInfoRecordTypeService
} from "../../../services/purchase-info-record-type.service";
import {
  PurchaseOrganization, PurchaseOrganizationsService,
  PurchaseOrganizationSupplier
} from "../../../../_common/services/purchase-organization.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'pir-edit',
  templateUrl: 'purchase-info-record-edit.component.html',
  styleUrls: ['purchase-info-record-edit.component.css']
})

export class PurchaseInfoRecordEditComponent extends TranslatableComponent implements OnInit {

  @ViewChild('organizationDialog') organizationDialog: DialogComponent;
  @ViewChild('periodViewDialog') periodViewDialog: DialogComponent;
  @ViewChild('periodsTable') periodsTable: TableWithPagesComponent;
  materialId: string;
  ready = false;
  purchaseInfoRecord: PurchaseInfoRecord;
  incoterms: Incoterm[];
  scaleEditOn = false;
  scaleToEdit = new InfoRecordScale();
  headerEdit = false;
  organization: PurchaseOrganization = new PurchaseOrganization();
  supplier: PurchaseOrganizationSupplier = new PurchaseOrganizationSupplier();

  createMode = false;
  availablePlants: string[] = [];
  organizations: PurchaseOrganization[] = [];
  suppliers: PurchaseOrganizationSupplier[] = [];
  plants: PlantSAP[] = [];

  organizationsToShow: PurchaseOrganization[] = [];
  periodToEdit: InfoRecordPeriod;
  periodToView: InfoRecordPeriod;

  headerConfPeriods: HeaderConf[] = [
    {
      valueFunction: (item: InfoRecordPeriod) => {
        let date = item.ValidFrom || item.NewValidFrom;
        let sapDatePipe = new SapDatePipe();
        return sapDatePipe.transform(date);
      },
      label: 'Valid from'
    },
    {
      valueFunction: (item: InfoRecordPeriod) => {
        let date = item.ValidTo || item.NewValidTo;
        let sapDatePipe = new SapDatePipe();
        return sapDatePipe.transform(date);
      },
      label: 'Valid to'
    }
  ];

  availableActionsPeriods = [];

  today = getUrlDate(new Date());

  materialSAP: MaterialSAP;

  purchaseGroups: PurchasingGroup[] = [];
  conditions: PurchaseCondition[] = [];

  conditionAccordionIndex = 0;

  conditionToEdit: InfoRecordCondition;

  availableActionsConditions = [];

  headerConfConditions: HeaderConf[] = [
    {
      valueFunction: (item: InfoRecordCondition) =>{
        return item.ConditionCode + " - " + item.Description;
      },
      label: 'Condition'
    },
    {
      valueFunction: (item: InfoRecordCondition) =>{
        if (item.PriceUnit) {
          return item.PriceUnit + " " + item.UoM;
        }
        else {
          return "-";
        }

      },
      label: 'Price Unit'
    },
    {
      valueFunction: (item: InfoRecordCondition) => {
        return item.Value + " " + item.Currency;
      },
      label: 'Value'
    },
    {
      valueFunction: (item: InfoRecordCondition) => {
        return item.ScaleLink.length > 0
      },
      label: 'Has Scales',
      isBoolean: true
    }
  ];

  conditionsReady = true;

  types: PurchaseInfoRecordType[] = [];

  constructor(protected translationService: TranslationService, private router: Router,
              private activatedRoute: ActivatedRoute, private purchaseInfoRecordService: PurchaseInfoRecordService,
              private incotermsService: IncotermsService, private messageService: MessageService,
              private loadingService: LoadingService, private organizationsService: PurchaseOrganizationsService,
              private materialPlantsSAPService: MaterialPlantsSAPService, private plantSAPService: PlantSAPService,
              private materialsSAPService: MaterialsSAPService, private purchaseGroupService: PurchasingGroupService,
              private purchaseConditionsService: PurchaseConditionsService, private purchaseInfoRecordTypeService: PurchaseInfoRecordTypeService) {
    super(translationService);
  }

  ngOnInit() {

    this.loadingService.show();
    this.purchaseInfoRecordTypeService.getPurchaseInfoRecordTypes({}).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        this.types = resp.response.results;
        this.ready = true;
      }
    });

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.materialId = params['id'];
        let mode = params['mode'];
        if (mode == 'edit') {
          if (params['pirid']) {
            let idArray = params['pirid'].split("|");
            let idToRequest = "Material='" + idArray[0] + "',PurchaseOrganization='" + idArray[1] + "',Supplier='" + idArray[2] + "',Plant='" + (idArray[3] ? idArray[3] : " ") + "',Type='" + idArray[4] + "'";
            this.loadInfoRecord(idToRequest);
          }
        }
        else {
          if (mode == 'create') {
            this.createMode = true;
            this.headerEdit = true;
            if (this.conditions.length == 0) {
              this.loadingService.show();
              this.purchaseConditionsService.getPurchaseConditions({}).subscribe((resp) => {
                this.loadingService.hide();
                if (resp.status == 200) {
                  this.conditions = resp.response.results;
                  this.initPIR();
                }
              });
            }

          }
        }
      }
      else {
        this.back();
      }
    });

    let editPeriod: TableRowAction = cloneObject(TableRowActions.EDIT);
    editPeriod.visibilityCondition = (p: InfoRecordPeriod) => {
      return this.isPeriodActualOrFuture(p);
    };
    editPeriod.click = (p: InfoRecordPeriod) => {
      this.editPeriod(p);
    };
    let viewPeriod: TableRowAction = cloneObject(TableRowActions.VIEW);
    viewPeriod.click = (p: InfoRecordPeriod) => {
      this.viewPeriod(p);
    };
    this.availableActionsPeriods.push(editPeriod, viewPeriod);

    let editCondition: TableRowAction = cloneObject(TableRowActions.EDIT);
    editCondition.visibilityCondition = (c: InfoRecordCondition) => {
      return !this.conditionToEdit;
    };
    editCondition.click = (c: InfoRecordCondition) => {
      this.editCondition(c);
    };
    let deleteCondition: TableRowAction = cloneObject(TableRowActions.DELETE);
    deleteCondition.visibilityCondition = (c: InfoRecordCondition) => {
      return !this.conditionToEdit && this.canDeleteCondition(c);
    };
    deleteCondition.click = (c: InfoRecordCondition) => {
      this.deleteCondition(c, this.periodToEdit);
    };
    this.availableActionsConditions.push(editCondition, deleteCondition);

  }

  loadInfoRecord(idToRequest) {
    let queryParams = {
      urlParameters: {
        $expand: "PeriodLink/ConditionLink/ScaleLink"
      }
    };
    this.loadingService.show();
    this.purchaseInfoRecordService.getPurchaseInfoRecord(idToRequest, queryParams).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        let purchaseInfoRecord = resp.response;

        purchaseInfoRecord.DeliveryTime = parseFloat(purchaseInfoRecord.DeliveryTime + "");
        purchaseInfoRecord.MinimumQuantity = parseFloat(purchaseInfoRecord.MinimumQuantity + "");
        purchaseInfoRecord.StandardQuantity = parseFloat(purchaseInfoRecord.StandardQuantity + "");

        purchaseInfoRecord.PeriodLink.results.forEach((p) => {
          p.ConditionLink.results.forEach((c) => {
            c.PriceUnit = parseFloat(c.PriceUnit + "");
            c.Value = parseFloat(c.Value + "");
            c.ScaleLink.results.forEach((sl: InfoRecordScale) => {
              sl.ScaleValue = parseFloat(sl.ScaleValue + "");
              sl.ScaleQuantity = parseFloat(sl.ScaleQuantity + "");
            });
            c.ScaleLink = c.ScaleLink.results;
          });
          p.ConditionLink = p.ConditionLink.results;
        });
        purchaseInfoRecord.PeriodLink = purchaseInfoRecord.PeriodLink.results;
        this.purchaseInfoRecord = purchaseInfoRecord;

        this.purchaseInfoRecord.PeriodLink.sort((pl1, pl2) => {
          return parseInt(pl2.ValidTo, 10) - parseInt(pl1.ValidTo, 10);
        });

        this.incotermsService.getIncoterms({}).subscribe((resp) => {
          if (resp.status == 200) {
            this.incoterms = resp.response.results;
            this.materialsSAPService.getMaterial("'" + this.materialId + "'").subscribe((resp) => {
              if (resp.status == 200) {
                this.materialSAP = resp.response;
                this.purchaseGroupService.getPurchaseGroups({}).subscribe((resp) => {
                  if (resp.status == 200) {
                    this.purchaseGroups = resp.response.results;
                    if (this.conditions.length == 0) {
                      this.purchaseConditionsService.getPurchaseConditions({}).subscribe((resp) => {
                        if (resp.status == 200) {
                          this.conditions = resp.response.results;
                          this.ready = true;
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        });

        this.organizationsService.getPurchaseOrganization("'" + this.purchaseInfoRecord.PurchaseOrganization + "'", {urlParameters: {$expand: 'SupplierLink'}}).subscribe((resp) => {
          if (resp.status == 200) {
            this.organization = resp.response;
            this.organization.SupplierLink.results.forEach((s) => {
              if (s.Supplier == this.purchaseInfoRecord.Supplier) {
                this.supplier = s;
              }
            })
          }
        })

        this.materialsSAPService.getMaterial("'" + this.purchaseInfoRecord.Material + "'").subscribe((resp) => {
          if (resp.status == 200) {
            this.materialSAP = resp.response;
          }
        })
      }
      else {
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  initPIR() {
    this.createMode = true;
    let pir = new PurchaseInfoRecord();
    pir.PeriodLink = [];
    pir.Material = this.materialId;
    pir.DeliveryTime = 5;
    pir.PurchGroup = "SP3";
    pir.Type = "0";
    this.loadingService.show();
    this.materialsSAPService.getMaterial("'" + pir.Material + "'").subscribe((resp) => {

      if (resp.status == 200) {
        this.materialSAP = resp.response;
        this.materialPlantsSAPService.getMaterialPlants({MatCode: pir.Material}).subscribe((resp) => {
          if (resp.status == 200) {
            let plants: MaterialPlantSAP[] = resp.response.results;
            this.availablePlants = plants.map((mp: MaterialPlantSAP) => mp.Plant);
            this.organizationsService.getPurchaseOrganizations({}, {urlParameters: {$expand: 'SupplierLink'}}).subscribe((resp) => {
              this.loadingService.hide();
              if (resp.status == 200) {
                this.organizations = resp.response.results;
                this.purchaseInfoRecord = pir;
                this.loadingService.show();
                let organizationsToShow: PurchaseOrganization[] = [];
                this.checkOrganizations(0, organizationsToShow, () => {
                  this.organizationsToShow = organizationsToShow;
                  this.incotermsService.getIncoterms({}).subscribe((resp) => {
                    if (resp.status == 200) {
                      this.incoterms = resp.response.results;
                      this.materialsSAPService.getMaterial("'" + this.materialId + "'").subscribe((resp) => {
                        if (resp.status == 200) {
                          this.materialSAP = resp.response;
                          this.purchaseGroupService.getPurchaseGroups({}).subscribe((resp) => {
                            if (resp.status == 200) {
                              this.purchaseGroups = resp.response.results;
                              this.loadingService.hide();
                              this.headerEdit = true;
                              this.ready = true;
                            }
                          });
                        }
                      });

                    }
                  });
                })
              }
              else {
                this.organizations = [];
                this.messageService.open({
                  title: this.tr('ERROR'),
                  error: true,
                  message: buildErrorMessage(resp),
                  autoClose: false
                });
              }
            });
          }
          else {
            this.messageService.open({
              title: this.tr('ERROR'),
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        });
      }

    });
  }

  back() {
    this.router.navigate(['procurement-pricelist', this.materialId, 'purchase-info-record']);
  }

  parseFloat(n: number) {
    return parseFloat(n + "");
  }

  addScale() {
    this.scaleToEdit = new InfoRecordScale();
    this.scaleToEdit['private_id'] = "scale_" + (new Date()).getTime();
    this.scaleEditOn = true;
  }

  editScale(s: InfoRecordScale) {
    this.scaleToEdit = cloneObject(s);
    this.scaleEditOn = true;
  }

  saveScale(c: InfoRecordCondition) {
    let s = this.scaleToEdit;
    if (s['private_id']) {
      let scaleNumber = s['private_id'];
      let matchFound = false;
      c.ScaleLink.forEach((sl, index) => {
        if (sl['private_id'] == scaleNumber) {
          matchFound = true;
          c.ScaleLink[index] = s;
        }
      });
      if (!matchFound)
        c.ScaleLink.push(s);
    }
    else {
      let scaleNumber = s.ScaleNumber;
      c.ScaleLink.forEach((sl, index) => {
        if (sl.ScaleNumber == scaleNumber) {
          c.ScaleLink[index] = s;
        }
      });
    }
    this.undoScale();
  }

  deleteScale(c: InfoRecordCondition, s: InfoRecordScale) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        if (s['private_id']) {
          let scaleNumber = s['private_id'];
          c.ScaleLink.forEach((sl, index) => {
            if (sl['private_id'] == scaleNumber) {
              c.ScaleLink.splice(index, 1);
            }
          });
        }
        else {
          let scaleNumber = s.ScaleNumber;
          c.ScaleLink.forEach((sl, index) => {
            if (sl.ScaleNumber == scaleNumber) {
              c.ScaleLink.splice(index, 1);
            }
          });
        }
      },
      deny: () => {
      }
    });
  }

  sortScaleLink(sls: InfoRecordScale[]) {
    sls.sort((s1, s2) => {
      return parseFloat(s1.ScaleQuantity + "") - parseFloat(s2.ScaleQuantity + "");
    });
    return sls;
  }

  canSavePIR() {
    return !this.periodToEdit && this.checkHeader() && this.checkPeriods();
  }

  checkHeader() {
    return this.purchaseInfoRecord && this.purchaseInfoRecord.Material && this.purchaseInfoRecord.PurchaseOrganization && this.purchaseInfoRecord.Supplier &&
      this.purchaseInfoRecord.StandardQuantity && this.purchaseInfoRecord.Incoterms1 && this.purchaseInfoRecord.Incoterms2;
  }

  checkPeriods() {
    return this.purchaseInfoRecord.PeriodLink && this.purchaseInfoRecord.PeriodLink && this.purchaseInfoRecord.PeriodLink.length > 0
  }

  openOrganizationDialog() {
    this.supplierCodeFilter = "";
    this.supplierNameFilter = "";
    this.organizationDialog.open();
  }

  checkOrganizations(index: number, organizationsToShow: PurchaseOrganization[], callback: any) {
    if (this.organizations[index]) {
      this.plantSAPService.getPlants({PurchaseOrganization: this.organizations[index].PurchaseOrg}).subscribe((resp) => {
        if (resp.status == 200) {
          this.plants = [];
          let plants: PlantSAP[] = resp.response.results;
          let plantsCodes = plants.map((p) => p.Code);
          if (arrayIntersect(plantsCodes, this.availablePlants).length > 0) {
            this.plants = plants;
            if (this.organizations[index].SupplierLink && this.organizations[index].SupplierLink.results.length > 0)
              organizationsToShow.push(this.organizations[index]);
          }
          this.checkOrganizations(index + 1, organizationsToShow, callback);
        }
        else {
          //Non ci sono plant collegati a questa organizzazione, skippo
          this.checkOrganizations(index + 1, organizationsToShow, callback);
        }
      })
    }
    else {
      callback.call(this);
    }
  }

  headerConfSuppliers: HeaderConf[] = [
    {
      valueKey: 'Supplier',
      label: 'Supplier'
    },
    {
      valueKey: 'Name',
      label: 'Name'
    }
  ];

  selectedSupplier(o: PurchaseOrganization, s: PurchaseOrganizationSupplier) {
    this.organization = o;
    this.supplier = s;
    this.purchaseInfoRecord.PurchaseOrganization = o.PurchaseOrg;
    this.purchaseInfoRecord.Supplier = s.Supplier;
    this.purchaseInfoRecord.Currency = s.Currency;
    this.organizationDialog.close();
    this.plantSAPService.getPlants({PurchaseOrganization: this.purchaseInfoRecord.PurchaseOrganization}).subscribe((resp) => {
      if (resp.status == 200) {
        let plants = resp.response.results;
        let plantCodes = plants.map((p: PlantSAP) => p.Code);

        let matches = arrayIntersect(plantCodes, this.availablePlants);
        matches = matches.map((code: string) => plants.filter((p) => p.Code == code)[0]);
        this.plants = matches;
      }
    })
  }

  supplierCodeFilter: string;
  supplierNameFilter: string;

  filterSuppliers(ss: PurchaseOrganizationSupplier[]) {
    return ss.filter((s) => {
      if (this.supplierCodeFilter) {
        if (this.supplierNameFilter) {
          return s.Supplier.indexOf(this.supplierCodeFilter) != -1 && s.Name.indexOf(this.supplierNameFilter) != -1
        }
        else {
          return s.Supplier.indexOf(this.supplierCodeFilter) != -1
        }
      }
      else {
        if (this.supplierNameFilter) {
          return s.Name.indexOf(this.supplierNameFilter) != -1
        }
        else {
          return true;
        }
      }
    })
  }

  clearOrgSelection() {
    this.organization = new PurchaseOrganization();
    this.supplier = new PurchaseOrganizationSupplier();
    this.purchaseInfoRecord.PurchaseOrganization = null;
    this.purchaseInfoRecord.Supplier = null;
    this.purchaseInfoRecord.Currency = null;
    this.plants = [];
    this.purchaseInfoRecord.Plant = null;
  }

  highlightPeriod(p: InfoRecordPeriod) {
    return this.isPeriodActive(p) ? "active" : "";
  }

  isPeriodActive(p: InfoRecordPeriod) {
    let ValidFrom = p.ValidFrom || p.NewValidFrom;
    let ValidTo = p.ValidTo || p.NewValidTo;
    let date = new Date();
    let sapDate = parseInt([date.getFullYear(), formatNumber(2, (date.getMonth() + 1)), formatNumber(2, date.getDate())].join(""), 10);
    let validFrom = parseInt(ValidFrom, 10);
    let validTo = parseInt(ValidTo, 10);
    return ((sapDate >= validFrom) && (sapDate <= validTo));
  }

  isPeriodActualOrFuture(p: InfoRecordPeriod) {
    let ValidFrom = p.ValidFrom || p.NewValidFrom;
    let ValidTo = p.ValidTo || p.NewValidTo;
    let date = new Date();
    let sapDate = parseInt([date.getFullYear(), formatNumber(2, (date.getMonth() + 1)), formatNumber(2, date.getDate())].join(""), 10);
    let validFrom = parseInt(ValidFrom, 10);
    let validTo = parseInt(ValidTo, 10);
    return ((sapDate >= validFrom) && (sapDate <= validTo)) || (sapDate < validFrom);
  }

  editPeriod(p: InfoRecordPeriod) {
    this.periodToEdit = cloneObject(p);
  }

  viewPeriod(p: InfoRecordPeriod) {
    this.periodToView = cloneObject(p);
    this.periodViewDialog.open();
  }

  periodAddActive = false;

  addPeriod() {
    this.periodAddActive = true;
    let period = new InfoRecordPeriod();
    period.ConditionLink = [];
    let cond = new InfoRecordCondition();
    let pb00 = this.conditions.filter((c) => c.ConditionCode == 'PB00')[0];
    cond.ConditionCode = pb00.ConditionCode;
    cond.Description = pb00.ConditionDescription;
    cond.UoM = this.materialSAP.Uom;
    cond.Currency = this.purchaseInfoRecord.Currency || this.supplier.Currency;
    cond.ScaleLink = [];
    this.periodToEdit = period;
    this.conditionToEdit = cond;
  }

  canSavePeriod() {
    return (this.periodAddActive ? (this.periodToEdit.NewValidFrom && this.periodToEdit.NewValidTo) : true) && this.checkConditions(this.periodToEdit);
  }

  checkConditions(p: InfoRecordPeriod) {
    let isOK = true;
    p.ConditionLink.forEach((c) => {
      if (!this.canSaveCondition(c))
        isOK = false;
    });
    return isOK;
  }

  savePeriod() {
    if (this.periodAddActive) {
      this.messageService.open({
        title: this.tr('ATTENTION'),
        message: this.buildConfirmSavePeriod(this.periodToEdit),
        autoClose: false,
        confirm: () => {
          this.periodToEdit.NewValidFrom = getUrlDate(new Date(this.periodToEdit.NewValidFrom),"");
          this.periodToEdit.NewValidTo = getUrlDate(new Date(this.periodToEdit.NewValidTo),"");
          let p: InfoRecordPeriod = cloneObject(this.periodToEdit);
          p.toSave = true;
          this.purchaseInfoRecord.PeriodLink.push(p);
          this.purchaseInfoRecord.PeriodLink.sort((pl1, pl2) => {
            return parseInt(pl2.ValidTo, 10) - parseInt(pl1.ValidTo, 10);
          });
          this.periodAddActive = false;
          this.periodToEdit = null;
          this.periodsTable.reload();
        },
        deny: () => {
        }
      });
    }
    else {
      this.purchaseInfoRecord.PeriodLink.forEach((pl, index) => {
        if ((pl.ValidTo == this.periodToEdit.ValidTo) && (pl.ValidFrom == this.periodToEdit.ValidFrom)) {
          let p: InfoRecordPeriod = cloneObject(this.periodToEdit);
          p.toSave = true;
          this.purchaseInfoRecord.PeriodLink[index] = p;
        }
      });
      this.periodToEdit = null;
      this.periodsTable.reload();
    }
  }

  buildConfirmSavePeriod(p: InfoRecordPeriod) {
    let content = [];
    content.push("Please double check and confirm the inserted period:<br/>")
    content.push("<b>" + p.NewValidFrom + "->" + p.NewValidTo + "</b>");
    return content.join("");
  }

  undoPeriod() {
    this.periodAddActive = false;
    this.periodToEdit = null;
    this.undoEditCondition();
    this.undoScale();
  }

  savePIR() {
    let pir: PurchaseInfoRecord = cloneObject(this.purchaseInfoRecord);
    pir.PeriodLink = pir.PeriodLink.filter((p) => {
      return p.toSave;
    });

    this.loadingService.show();
    this.purchaseInfoRecordService.savePurchaseInfoRecord(pir).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status >= 201 && resp.status < 300) {
        this.messageService.open({
          message: this.tr('SAVE_COMPLETED'),
          title: " ",
          autoClose: false,
          onClose: () => {
            let idToRequest = "Material='" + this.purchaseInfoRecord.Material + "',PurchaseOrganization='" +
              this.purchaseInfoRecord.PurchaseOrganization + "',Supplier='" + this.purchaseInfoRecord.Supplier +
              "',Plant='" + (this.purchaseInfoRecord.Plant ? this.purchaseInfoRecord.Plant : " ") + "',Type='" + this.purchaseInfoRecord.Type + "'";
            this.loadInfoRecord(idToRequest)
          }
        });
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  undoScale() {
    this.scaleEditOn = false;
    this.scaleToEdit = new InfoRecordScale();
  }

  addCondition(p: InfoRecordPeriod) {
    let c = new InfoRecordCondition();
    c.ScaleLink = [];
    c.UoM = this.materialSAP.Uom;
    c.isNew = true;

    this.conditionToEdit = c;
  }

  handleConditionChange(c: InfoRecordCondition) {
    let cond = this.conditions.filter((c_) => c_.ConditionCode == c.ConditionCode)[0];
    c.Description = cond.ConditionDescription;
    c.Currency = (cond.Currency) ? cond.Currency : this.purchaseInfoRecord.Currency;
  }

  canDeleteCondition(c: InfoRecordCondition) {
    return c.ConditionCode != "PB00";
  }

  deleteCondition(c: InfoRecordCondition, p: InfoRecordPeriod) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        p.ConditionLink.splice(p.ConditionLink.indexOf(c), 1);
        this.conditionsReady = false;
        this.loadingService.show();
        setTimeout(() => {
          this.loadingService.hide();
          this.conditionsReady = true;
        }, 1000)
      },
      deny: () => {
      }
    });
  }

  getAvailableConditions(p: InfoRecordPeriod, c: InfoRecordCondition) {
    let cond = p.ConditionLink.map((pc) => pc.ConditionCode);
    return this.conditions.filter((c_) => {
      return cond.indexOf(c_.ConditionCode) == -1 || c.ConditionCode == c_.ConditionCode;
    })
  }

  editCondition(c: InfoRecordCondition) {
    this.conditionToEdit = cloneObject(c);
  }

  undoEditCondition() {
    this.conditionToEdit = null;
  }

  canSaveCondition(c: InfoRecordCondition) {
    let isOK = true;
    let scalesOK = true;
    c.ScaleLink.forEach((s) => {
      if (!(s.ScaleQuantity && s.ScaleValue))
        scalesOK = false;
    });
    if (!((c.Currency == "%" ? true : c.PriceUnit) && c.Value && scalesOK && !this.scaleEditOn)) {
      isOK = false;
    }
    return isOK;
  }

  saveCondition(c: InfoRecordCondition) {
    let match = this.periodToEdit.ConditionLink.filter((c_) => c_.ConditionCode == c.ConditionCode);
    if (match && match[0]) {
      this.periodToEdit.ConditionLink.splice(this.periodToEdit.ConditionLink.indexOf(match[0]), 1, c);
    }
    else {
      this.periodToEdit.ConditionLink.push(c);
    }
    this.conditionToEdit = null;
    this.conditionsReady = false;
    this.loadingService.show();
    setTimeout(() => {
      this.loadingService.hide();
      this.conditionsReady = true;
    }, 1000)
  }

  selectedIndex = 0;

  tabChange(tab: MatTabGroup) {
    this.selectedIndex = tab.selectedIndex;
  }

  getTypeDescription(p: PurchaseInfoRecord) {
    if (this.types.length > 0 && p.Type)
      return this.types.filter((t) => t.Type == p.Type)[0].Description;
    else
      return "-";
  }
}
