import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {PurchaseInfoRecordEditComponent} from "./purchase-info-record-edit.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";
import {PurchaseGroupPipe} from "../../../services/purchase-group.service";
import {PurchaseInfoRecordEditAttachmentsComponent} from "./purchase-info-record-edit-attachments/purchase-info-record-edit-attachments";
import {PurchaseOrganizationPipe} from "../../../../_common/services/purchase-organization.service";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule],       // module dependencies
  declarations: [PurchaseInfoRecordEditComponent,PurchaseOrganizationPipe,PurchaseGroupPipe,PurchaseInfoRecordEditAttachmentsComponent],   // components and directives
  exports: [PurchaseInfoRecordEditComponent],
  providers: []                    // services
})
export class PurchaseInfoRecordEditModule {}


