import {Component, Input, OnInit} from "@angular/core";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {ContentTypeService} from "../../../../../_common/services/content-type.service";
import {FileService, SPFile} from "../../../../../_common/services/file.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {Material, MaterialsService} from "../../../../../_common/services/materials.service";
import {PurchaseInfoRecord} from "../../../../services/purchase-info-record.service";
import {Supplier, SupplierService} from "../../../../../_common/services/suppliers.service";
import {MaterialSupplier, MaterialSupplierService} from "../../../../../_common/services/material-supplier.service";
import {TypeLink, TypeLinkService} from "../../../../../_common/services/type-link.service";
import {FileManager} from "../../../../../_common/services/file-manager.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'purchase-info-record-edit-attachments',
  templateUrl: 'purchase-info-record-edit-attachments.html',
  styleUrls: ['purchase-info-record-edit-attachments.css']
})
export class PurchaseInfoRecordEditAttachmentsComponent extends BaseComponent implements OnInit {

  @Input() pir: PurchaseInfoRecord;
  material: Material;
  supplier: Supplier;

  documentInput: any;
  matSupCT: number;
  editOn = false;
  types: TypeLink[] = [];

  disableButton = false;
  materialSupplier: MaterialSupplier;
  files: SPFile[] = [];
  file: SPFile = new SPFile();

  constructor(protected translationService: TranslationService, private contentTypeService: ContentTypeService,
              private fileService: FileService, private materialSupplierService: MaterialSupplierService,
              protected messageService: MessageService, protected loadingService: LoadingService,
              private materialsService: MaterialsService, private suppliersService: SupplierService,
              private typeLinkService: TypeLinkService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    let count = 2;
    let callback = () => {
      count--;
      if (count == 0) {
        this.loadMatDoc();
      }
    };

    this.materialsService.getMaterials({code: this.pir.Material, is_active: true}).subscribe((resp) => {
      let ms = parseSerionetRespToData(resp);
      if (ms.length > 0) {
        this.material = ms[0];
        callback();
      }
      else {
        this.disableButton = true;
      }
    });

    this.suppliersService.getSuppliers({
      sap_code: this.pir.Supplier,
      is_supplier: true,
      is_active: true
    }).subscribe((resp) => {
      let orgs = parseSerionetRespToData(resp);
      if (orgs.length > 0) {
        this.supplier = orgs[0];
        callback();
      }
      else {
        this.disableButton = true;
      }
    });

    this.contentTypeService.getContentTypes({model: 'materialsupplier', is_active:true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.matSupCT = data['results'][0]['pk'];
        this.typeLinkService.getTypeLinks({app: this.matSupCT, is_active:true}).subscribe((resp) => {
          this.types = parseSerionetRespToData(resp)
        })
      }
    });

    this.contentTypeService.getContentTypes({model: 'file', is_active:true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let fileCT = data['results'][0]['pk'];
        this.typeLinkService.getTypeLinks({app: fileCT, context:'procurement-pricelist', is_active:true}).subscribe((resp) => {
          this.types = parseSerionetRespToData(resp)
        })
      }
    });

  }

  initMatDoc() {
    this.file = new SPFile();
    this.file.type = new TypeLink();
    this.file.related_objects = [];
    let fm = new FileManager();
    fm.object_id = this.materialSupplier.pk;
    fm.content_type = this.matSupCT;
    this.file.related_objects.push(fm);
  }

  canSave() {
    return this.file && this.file.type.pk;
  }

  loadMatDoc() {

    let callback = () => {
      this.fileService.getFiles({
        objects_content_type : this.matSupCT,
        objects_obj_id_list: this.materialSupplier.pk,
        is_active:true
      }).subscribe((resp) => {
        this.loadingService.hide();
        this.files = parseSerionetRespToData(resp);
      })
    };

    this.loadingService.show();

    this.materialSupplierService.getMaterialSuppliers({
      material: this.material.pk,
      supplier: this.supplier.pk,
      is_active:true
    }).subscribe((resp) => {
      let materialSuppliers = parseSerionetRespToData(resp);
      if (materialSuppliers.length > 0) {
        this.materialSupplier = materialSuppliers[0];
        callback();
      }
      else {
        let ms = new MaterialSupplier();
        ms.material = this.material;
        ms.supplier = this.supplier;
        this.materialSupplierService.saveMaterialSupplier(ms).subscribe((data) => {
          if (data.pk) {
            this.materialSupplier = data;
            callback();
          }
          else {
            this.loadingService.hide();
            resp.context = "SERIONET";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      }
    });
  }

  extractFileName(filePath) {
    let pieces = filePath.split("/");
    return pieces[pieces.length - 1];
  }

  editMaterialSupplierDocument(f: SPFile) {
    this.file = cloneObject(f);
    if(!this.file.type)
      this.file.type = new TypeLink();
    this.documentInput = null;
    this.editOn = true;
  }

  downloadMaterialSupplierDocument(f: SPFile) {
    let a = document.createElement("a");
    a.setAttribute("download","");
    a.setAttribute("href",f.attachment_file);
    a.click();
  }

  createMaterialSupplierDocument() {
    this.initMatDoc();
    this.documentInput = null;
    this.editOn = true;
  }

  undoMaterialOrganizationDocument() {
    this.editOn = false;
  }

  deleteMaterialSupplierDocument(f: SPFile) {
    this.messageService.open({
      title: this.tr('CONFIRM'),
      message: this.tr('CONFIRM_DELETE'),
      confirm: () => {
        this.loadingService.show();
        this.fileService.deleteFile(f).subscribe((resp) => {
          this.manageRespDelete(resp,()=> {
            this.loadingService.hide();
            this.loadMatDoc();
          });
        })
      },
      deny: ()=>{}
    })
  }

  saveMaterialSupplierDocument(){
    this.loadingService.show();
    this.fileService.saveFile(this.file,this.documentInput?this.documentInput:null).subscribe((resp)=>{
      this.loadingService.hide();
      this.manageRespSave(resp, ()=>{
        this.editOn = false;
        this.loadMatDoc();
      })
    })
  }

  undoMaterialSupplierDocument(){
    this.editOn = false;
  }

  deleteFile(){
    delete this.file.attachment_file;
  }

  updateFile(input){
    this.documentInput = input;
  }

  getFileName(input) {
    if(input.value && input.value.length>0){
      return input.value.split("\\").pop();
    }
    else {
      return "";
    }
  }
}
