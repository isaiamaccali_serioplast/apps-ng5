import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {PurchaseInfoRecordListComponent} from "./purchase-info-record-list.component";
import {SerioplastCommonModule} from "../../../../_common/common.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [PurchaseInfoRecordListComponent],   // components and directives
  exports: [PurchaseInfoRecordListComponent],
  providers: []                    // services
})
export class PurchaseInfoRecordListModule {}


