import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {PurchaseInfoRecord, PurchaseInfoRecordService} from "../../../services/purchase-info-record.service";
import {ActivatedRoute, Router} from "@angular/router";
import {
  HeaderConf,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {
  PurchaseInfoRecordType,
  PurchaseInfoRecordTypeService
} from "../../../services/purchase-info-record-type.service";
import {
  PurchaseOrganization,
  PurchaseOrganizationsService, PurchaseOrganizationSupplier
} from "../../../../_common/services/purchase-organization.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'pir-list',
  templateUrl: 'purchase-info-record-list.component.html',
  styleUrls: ['purchase-info-record-list.component.css']
})

export class PurchaseInfoRecordListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('table') table: TableWithPagesComponent;
  headerConf: HeaderConf[] = [];
  availableActions = [];
  noItemsMessage = this.tr('NO_PURCHASE_INFO_RECORDS_FOR_THIS_MATERIAL');
  ready = false;
  materialId: string;
  organization: string;
  vendor: string;
  organizations: PurchaseOrganization[] = [];
  types: PurchaseInfoRecordType[] = [];
  type = "0";

  constructor(protected translationService: TranslationService, private purchaseInfoRecordService: PurchaseInfoRecordService,
              private router: Router, private activatedRoute: ActivatedRoute, private purchaseOrganizationsService: PurchaseOrganizationsService,
              private loadingService: LoadingService, private purchaseInfoRecordTypeService: PurchaseInfoRecordTypeService) {
    super(translationService);
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.materialId = params['id'];
        this.loadingService.show();
        this.purchaseOrganizationsService.getPurchaseOrganizations({}, {urlParameters: {$expand: "SupplierLink"}}).subscribe((resp) => {
          this.loadingService.hide();
          if (resp.status == 200) {
            this.organizations = resp.response.results;
            this.purchaseInfoRecordTypeService.getPurchaseInfoRecordTypes({}).subscribe((resp) => {
              if (resp.status == 200) {
                this.types = resp.response.results;
                this.ready = true;
              }
            });
          }
          else {
            this.organizations = [];
          }
        })
      }
      else {
        this.back();
      }
    });

    this.headerConf = [
      {
        label: 'Purchase Organization',
        valueFunction: (item: PurchaseInfoRecord) => {
          let org: PurchaseOrganization = null;
          this.organizations.forEach((o) => {
            if (o.PurchaseOrg == item.PurchaseOrganization) {
              org = o;
            }
          });
          return org.PurchaseOrg + " - " + org.Description;
        }
      },
      {
        label: 'Vendor',
        valueFunction: (item: PurchaseInfoRecord) => {
          let orgS: PurchaseOrganizationSupplier = null;
          this.organizations.forEach((o) => {
            o.SupplierLink.results.forEach((s) => {
              if (s.Supplier == item.Supplier) {
                orgS = s;
              }
            })

          });
          return orgS.Supplier + " - " + orgS.Name;
        }
      },
      {
        label: 'Plant',
        valueKey: 'Plant'
      },
      {
        label: 'Valid',
        valueKey:'Active',
        isBoolean: true
      },
      {
        label: 'Type',
        valueFunction: (item: PurchaseInfoRecord) => {
          return this.types.filter((t) => t.Type == item.Type)[0].Description;
        }
      }
    ];
  }

  getPurchaseInfoRecords(page: number, results: number) {

    let query = {};

    query['Material'] = this.materialId;

    if (this.organization) {
      query['PurchaseOrganization'] = this.organization;
    }

    if (this.vendor) {
      query['Supplier'] = this.vendor;
    }

    query['Type'] = this.type;

    return this.purchaseInfoRecordService.getPurchaseInfoRecords(query);
  }

  editPIR(pir: PurchaseInfoRecord) {
    this.router.navigate(['edit', [pir.Material, pir.PurchaseOrganization, pir.Supplier, pir.Plant, pir.Type].join("|")], {relativeTo: this.activatedRoute});
  }

  createPIR() {
    this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  back() {
    this.router.navigate(['procurement-pricelist']);
  }

  reload() {
    this.table.reload();
  }
}
