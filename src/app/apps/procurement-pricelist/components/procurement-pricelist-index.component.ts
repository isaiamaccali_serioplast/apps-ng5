import {Component, OnInit} from "@angular/core";
import {SideNavService} from "../../../shared/sidenav/service/sidenav.service";
import {Title} from "@angular/platform-browser";
import {TranslationService} from "../../../shared/i18n/translation.service";
import {TranslatableComponent} from "../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-procurement-pricelist-index',
  templateUrl: 'procurement-pricelist-index.component.html',
  styleUrls: ['procurement-pricelist-index.component.css']
})

export class ProcurementPriceListIndexComponent extends TranslatableComponent implements OnInit {

  constructor(private sideNavService: SideNavService, private titleService: Title, protected translationService:TranslationService) {
    super(translationService);
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Procurement Pricelist");
  }

  ngOnInit() {}
}
