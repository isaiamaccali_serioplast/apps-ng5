import {Component, EventEmitter, OnInit, Output, ViewChild} from "@angular/core";
import {Material, MaterialsService} from "../../../_common/services/materials.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {MaterialType, MaterialTypesService} from "../../../_common/services/material-type.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {MaterialsListQueryServiceCustomerInfo} from "./materials-list-query-service";
import {
  HeaderConf, TableRowAction,
  TableWithPagesComponent
} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {Observable} from "rxjs";
import {MaterialSubtypesLinkService} from "../../../_common/services/material-subtype-link.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MaterialPriceService} from "../../services/material-prices.service";
import {parseSerionetRespToData} from "../../../../shared/utils";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";
@Component({
  selector: 'app-materials-list-customer-info',
  templateUrl: 'materials-list.component.html',
  styleUrls: ['materials-list.component.css']
})
export class ProcurementPLMaterialsListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('materialsTable') materialsTable:TableWithPagesComponent;
  materials: Material[];
  currentPage: number;
  material: Material;
  site: Site = new Site();
  materialType: MaterialType;
  materialTypes: MaterialType[] = [];
  sites: Site[] = [];
  code_or_description: string;
  valuated = true;
  isActive = true;
  loading = true;
  resultsPerPage = 10;
  total: number;
  @Output('materialSelected') materialSelected = new EventEmitter();

  noItemsMessage = this.tr("NO_MATERIALS_FOUND");
  headerConf:HeaderConf[] = [
    {
      label:this.tr("CODE"),
      valueKey:'code'
    },
    {
      label:this.tr("DESCRIPTION"),
      valueKey:'description'
    },
    {
      label:this.tr("TYPE"),
      valueKey:'material_type',
      useStr:true
    },
    {
      label:this.tr("SUBTYPE"),
      valueFunction:(m:Material) => {
        if(!m.sub_model.subtype){
          return "-";
        }
        else {
          return m.sub_model.subtype.__str__;
        }
      }
    },
    {
      label:this.tr("HAS_PRICES"),
      isBoolean:true,
      valueKey:'has_prices'
    }
  ];
  availableActions = [];


  constructor(private materialsService: MaterialsService, private router:Router, private activatedRoute:ActivatedRoute,
              private sitesService: SitesService, private materialTypesService: MaterialTypesService,
              protected translationService: TranslationService,private materialsListQueryService:MaterialsListQueryServiceCustomerInfo,
              private materialSubtypeLinksService:MaterialSubtypesLinkService, private materialPriceService:MaterialPriceService
  ) {
    super(translationService);
  }

  ngOnInit() {
    this.materials = [];
    this.materialTypesService.getMaterialTypes({is_active:true,ordering:'description'}).subscribe((data) => {
      this.materialTypes = parseSerionetRespToData(data);
    });

    let query = this.materialsListQueryService.getQuery();
    if(query){
      this.restoreQueryData(query);
    }

    let goToPriceList = new TableRowAction();
    goToPriceList.icon = 'euro_symbol';
    goToPriceList.click = (m:Material)=>{
      this.router.navigate([m.code,'planned-purchase-price'],{relativeTo:this.activatedRoute});
    };
    let goToCustomerInfo = new TableRowAction();
    goToCustomerInfo.icon = 'list';
    goToCustomerInfo.click = (m:Material)=>{
      this.router.navigate([m.code,'purchase-info-record'],{relativeTo:this.activatedRoute});
    };
    this.availableActions = [goToPriceList,goToCustomerInfo];

    this.sitesService.getSites({is_active:true}).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
    })
  }

  restoreQueryData(query:any){
    if(query['site'])
      this.site = query['site'];
    if(query['type'])
      this.materialType = query['type'];
    if(query['search'])
     this.code_or_description = query['search'];
    if(query['valuated']!=null)
      this.valuated = query['valuated'];
    if(query['is_active']!=null)
      this.isActive = query['is_active'];
  }

  saveQueryData(){
    let query = {};
    query['site'] = this.site;
    query['type'] = this.materialType;
    query['search'] = this.code_or_description;
    query['valuated'] = this.valuated;
    query['is_active'] = this.isActive;

    this.materialsListQueryService.saveQuery(query);
  }

  loadMaterials(page?: number, results?:number) {

    let query = {};
    if (this.site.pk)
      query['site_set'] = this.site.pk;
    if (this.materialType)
      query['material_type'] = this.materialType;
    if (this.code_or_description)
      query['search'] = this.code_or_description;
    if (this.valuated != null)
      query['valuated'] = this.valuated;
    if (this.isActive != null)
      query['is_active'] = this.isActive;

    query['ordering'] = 'code';

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    this.saveQueryData();

    return this.materialsService.getMaterials(query);
  }

  selectedShape(event: any) {
    if (event != null)
      this.site = event;
    else
      this.site = new Site();
  }

  expandDataSubTypes(materials:Material[]){
    return Observable.create((observer)=>{

      let expandST = (ms,index,callback) => {
        if(ms[index]){
          let m = ms[index];
          if(m.sub_model.subtype){
            this.materialSubtypeLinksService.getMaterialSubtypeLink(m.sub_model.subtype).subscribe((s)=>{
              if(s.pk)
                m.sub_model.subtype = s;
            })
          }
          this.materialPriceService.getMaterialPrices({Material:m.code}).subscribe((resp)=>{
            if(resp.status==200){
              let matPrices = resp.response.results;
              if(matPrices.length>0){
                m['has_prices'] = true;
                matPrices.forEach((mp)=>{
                  let price = parseFloat(mp.Price);
                  if(price==0){
                    m['has_prices'] = false;
                  }
                });
              }
              else {
                m['has_prices'] = false;
              }
            }
            else {
              m['has_prices'] = false;
            }
            expandST(ms,index+1, callback);
          });

        }
        else {
          callback();
        }
      };
      expandST(materials,0, () => {
        observer.next(materials);
        observer.complete();
      })
    });
  }

  reset(){
    setTimeout(()=>{
      this.reload();
    },500)
  }

  reload(){
    this.materialsTable.reload();
  }
}
