import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {MaterialsListQueryService} from "../../services/materials-list-query-service";
import {ProcurementPLMaterialsListComponent} from "./materials-list.component";
import {PurchaseInfoRecordListModule} from "../purchase-info-record/purchase-info-record-list/purchase-info-record-list.module";
import {PurchaseInfoRecordEditModule} from "../purchase-info-record/purchase-info-record-edit/purchase-info-record-edit.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule, PurchaseInfoRecordListModule, PurchaseInfoRecordEditModule],       // module dependencies
  declarations: [ProcurementPLMaterialsListComponent],   // components and directives
  exports:[ProcurementPLMaterialsListComponent],
  providers: []                    // services
})
export class MaterialsListModule { }


