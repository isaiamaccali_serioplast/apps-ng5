import {Injectable} from "@angular/core";
import {LoggingService} from "../../../shared/logging/logging.service";

@Injectable()
export class MaterialsListQueryServiceCustomerInfo {

  query:any = {};

  constructor() {}

  saveQuery(query:any){
    this.query = query;
  }

  getQuery(){
    return this.query;
  }

}
