import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {PeriodService} from "./services/period.service";
import {PriceListService} from "./services/pricelist-service";
import {priceListRoutes} from "./procurement-pricelist.routes";
import {PriceListIndexModule} from "./components/procurement-pricelist-index.module";
import {PurchaseInfoRecordService} from "./services/purchase-info-record.service";
import {IncotermsService} from "./services/incoterms.service";
import {MaterialPriceService} from "./services/material-prices.service";
import {PurchasingGroupService} from "./services/purchase-group.service";
import {PurchaseConditionsService} from "./services/purchase-conditions.service";
import {PurchaseInfoRecordTypeService} from "./services/purchase-info-record-type.service";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(priceListRoutes),PriceListIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [
    PurchaseInfoRecordService,
    IncotermsService,
    MaterialPriceService,
    PurchasingGroupService,
    PurchaseConditionsService,
    PurchaseInfoRecordTypeService
  ]   // services
})
export class ProcurementPriceListModule { }

