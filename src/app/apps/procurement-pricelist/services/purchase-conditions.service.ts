import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {Observable} from "rxjs";

export class PurchaseCondition {
  ConditionDescription: string;
  ConditionCode: string;
  Currency:string;
}

declare let sap: any;

@Injectable()
export class PurchaseConditionsService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "PurchConditionSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getPurchaseConditions(query: any) {
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let filters = [];

        // if(query['PurchaseOrganization']){
        //   let orgFilter = new sap.ui.model.Filter({
        //     path: "PurchaseOrganization",
        //     operator: sap.ui.model.FilterOperator.EQ,
        //     value1: query['PurchaseOrganization']
        //   });
        //   filters.push(orgFilter);
        // }
        //
        // if(query['Supplier']){
        //   let vendorFilter = new sap.ui.model.Filter({
        //     path: "Supplier",
        //     operator: sap.ui.model.FilterOperator.EQ,
        //     value1: query['Supplier']
        //   });
        //   filters.push(vendorFilter);
        // }

        let params = {};
        params['filters'] = filters;

        this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      });
    });
  }
}


