import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {cloneObject} from "../../../shared/utils";

declare let sap: any;

export class PurchaseInfoRecord{
  Plant: string;
  PurchaseOrganization: string;
  Supplier: string;
  Material: string;
  MinimumQuantity?: any;
  StandardQuantity?: any;
  DeliveryTime?: any;
  Incoterms1?: string;
  Incoterms2?: string;
  PurchGroup?: string;
  SupplierMaterialCode?: string;
  Currency?: string;
  Active?:boolean;
  UoM?:string;
  PeriodLink?: InfoRecordPeriod[];
  Type:string;

  getId(){
    return "Plant='"+(this.Plant?this.Plant:" ")+"',PurchaseOrganization='"+this.PurchaseOrganization+"',Supplier='"+this.Supplier+"',Material='"+this.Material+"',Type='"+this.Type+"'";
  };
}

export class InfoRecordPeriod {
  ValidFrom: string;
  ValidTo: string;
  NewValidFrom:string;
  NewValidTo:string;
  ConditionLink?: InfoRecordCondition[];

  toSave?:boolean = false;
}

export class InfoRecordCondition {
  ConditionCode: string;
  Description:string;
  Value?: any;
  PriceUnit?: any;
  UoM?: string;
  Currency:string;
  ScaleLink?: InfoRecordScale[];

  isNew = false;
}

export class InfoRecordScale {
  ScaleNumber: any;
  ScaleQuantity?: any;
  ScaleValue?: any;
}

@Injectable()
export class PurchaseInfoRecordService extends GenericSAPService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "InfoRecordSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getPurchaseInfoRecords(query: any) {
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let filters = [];

        if(query['Plant']){
          let plantFilter = new sap.ui.model.Filter({
            path: "Plant",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(plantFilter);
        }

        if(query['Material']){
          let materialFilter = new sap.ui.model.Filter({
            path: "Material",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Material']
          });
          filters.push(materialFilter);
        }

        if(query['PurchaseOrganization']){
          let orgFilter = new sap.ui.model.Filter({
            path: "PurchaseOrganization",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['PurchaseOrganization']
          });
          filters.push(orgFilter);
        }

        if(query['Supplier']){
          let vendorFilter = new sap.ui.model.Filter({
            path: "Supplier",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Supplier']
          });
          filters.push(vendorFilter);
        }

        if(query['Type']){
          let vendorFilter = new sap.ui.model.Filter({
            path: "Type",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Type']
          });
          filters.push(vendorFilter);
        }

        let params = {};
        params['filters'] = filters;

        params['sorters'] = [
          new sap.ui.model.Sorter("PurchaseOrganization",false),
          new sap.ui.model.Sorter("Supplier",false),
          new sap.ui.model.Sorter("Plant",false)
        ];

        params = this.checkPagination(params, query);
        this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      });
    });
  }

  savePurchaseInfoRecord(i:PurchaseInfoRecord){
    return this.saveItem(i, true);
  }

  getPurchaseInfoRecord(id: any, params:any) {
    return this.getItem(id, params);
  }

  protected clearItem(item:any){
    let i = new PurchaseInfoRecord();
    for(let key in item){
      if(item.hasOwnProperty(key)){
        i[key] = cloneObject(item[key]);
      }
    }
    if(i.StandardQuantity!=null)
      i.StandardQuantity = i.StandardQuantity.toExponential();
    if(i.DeliveryTime!=null)
      i.DeliveryTime = i.DeliveryTime.toExponential();
    if(i.MinimumQuantity!=null)
      i.MinimumQuantity = i.MinimumQuantity.toExponential();

    i.PeriodLink.forEach((p)=>{
      delete p.toSave;
      for(let key in p){
        if(p.hasOwnProperty(key)){
          if(p[key] == "" && (typeof p[key]=='string'))
            p[key] = " ";
        }
      }

      if(!p.NewValidFrom|| p.NewValidFrom==" ")
        p.NewValidFrom = p.ValidFrom;
      if(!p.NewValidTo|| p.NewValidTo==" ")
        p.NewValidTo = p.ValidTo;
      p.ConditionLink.forEach((c)=>{
        for(let key in c){
          if(c.hasOwnProperty(key)){
            if(c[key] == "" && (typeof c[key]=='string'))
              c[key] = " ";
          }
        }
        delete c.isNew;
        if(c.PriceUnit)
          c.PriceUnit = c.PriceUnit.toExponential();
        else
          c.PriceUnit = " ";
        c.Value = c.Value.toExponential();
        if(c.ScaleLink)
          c.ScaleLink.forEach((s)=>{
            for(let key in s){
              if(s.hasOwnProperty(key)){
                if(s[key] == "" && (typeof s[key]=='string'))
                  s[key] = " ";
              }
            }
            if(s['private_id'])
              delete s['private_id'];
            s.ScaleQuantity = s.ScaleQuantity.toExponential();
            s.ScaleValue = s.ScaleValue.toExponential();
          })
      })
    });

    for(let key in i){
      if(i.hasOwnProperty(key)){
        if(i[key] == "" && (typeof i[key]=='string'))
          i[key] = " ";
      }
    }
    return i;
  }
}


