import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";

export class Incoterm {
  Incoterms: string;
  Description: string;
}

@Injectable()
export class IncotermsService extends GenericSAPService {

  constructor(protected sapService: SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "IncotermSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getIncoterms(query: any) {
    return this.getList(query);
  }

}


