import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {Observable} from "rxjs";

declare let sap: any;

export class PurchasingGroup {
  PurchGroupCode: string;
  Description:string;
}

@Injectable()
export class PurchasingGroupService extends GenericSAPService {

  constructor(protected sapService: SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "PurchasingGroupSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getPurchaseGroups(query: any) {

    let filters = [];

    let params = {};
    params['filters'] = filters;
    if(query['PurchGroupCode']){
      let vendorFilter = new sap.ui.model.Filter({
        path: "PurchGroupCode",
        operator: sap.ui.model.FilterOperator.EQ,
        value1: query['PurchGroupCode']
      });
      filters.push(vendorFilter);
    }


    return this.getList(params);
  }

}

@Pipe({
  name: 'purchaseGroup'
})
export class PurchaseGroupPipe implements PipeTransform{

  pg:PurchasingGroup = new PurchasingGroup();
  querying = false;

  constructor(private pgService:PurchasingGroupService){}

  transform(id: number) {

    return Observable.create((observer)=>{

      if(id){
        if(!this.querying){
          this.pgService.getPurchaseGroups({PurchGroupCode:id}).subscribe((resp) => {
            if (resp.status == 200 && resp.response.results.length>0) {
              this.pg = resp.response.results[0];
              observer.next(this.pg.PurchGroupCode + " - " + this.pg.Description);
              observer.complete();
            }
          });
        }
        else {
          observer.next(id);
        }
      }
      else {
        observer.next(id);
      }
    });
  }
}
