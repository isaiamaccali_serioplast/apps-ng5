import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";

declare let sap: any;

export class PurchaseInfoRecordType{
  Type:string;
  Description:string;
}

@Injectable()
export class PurchaseInfoRecordTypeService extends GenericSAPService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "InfoRecordTypeSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getPurchaseInfoRecordTypes(query: any) {
    return this.getList(query)
  }
}


