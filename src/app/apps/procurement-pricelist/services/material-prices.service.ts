import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {cloneObject, getUrlDate} from "../../../shared/utils";

export class MaterialPrice {
  Material: string;
  Plant: string;
  Currency: string;
  Uom: string;
  PriceUnit: any;
  Price: any;
  PriceControlLabel: string;
  PriceControl: string;
  PriceDate:any;

  getId(){
    return "Plant='"+this.Plant+"',Material='"+this.Material+"'";
  };

  noEditUnit = false;
}

declare let sap: any;

@Injectable()
export class MaterialPriceService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "MaterialPriceSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getMaterialPrices(query: any) {
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let filters = [];

        if(query['Plant']){
          let plantFilter = new sap.ui.model.Filter({
            path: "Plant",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(plantFilter);
        }

        if(query['Material']){
          let materialFilter = new sap.ui.model.Filter({
            path: "Material",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Material']
          });
          filters.push(materialFilter);
        }

        let params = {};
        params['filters'] = filters;

        params['sorters'] = [
          new sap.ui.model.Sorter("Plant",true)
        ];

        this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      });
    });
  }

  saveMaterialPrice(i:MaterialPrice){
    return this.saveItem(i);
  }

  clearItem(i:MaterialPrice){
    let i_ = new MaterialPrice();
    for(let key in i){
      if(i.hasOwnProperty(key)){
        i_[key] = cloneObject(i[key]);
      }
    }
    delete i_.noEditUnit;
    i_.PriceUnit = i_.PriceUnit.toExponential();
    i_.Price = i_.Price.toExponential();
    if(typeof i_.PriceDate == "object"){
      i_.PriceDate = getUrlDate(i_.PriceDate,"");
    }
    else {
      if(i_.PriceDate.indexOf("-")!=-1)
        i_.PriceDate = i_.PriceDate.split("-").join("");
    }

    return i_;
  }
}


