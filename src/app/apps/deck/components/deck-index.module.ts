import {DeckIndexComponent} from "./deck-index.component";
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {DeckTransfersModule} from "./deck-transfers/deck-transfers.module";
import {DeckDeliveriesModule} from "./deck-deliveries/deck-deliveries.module";
import {DeckReturnsModule} from "./deck-returns/deck-returns.module";
import {DeckRePackModule} from "./deck-re-pack/deck-re-pack.module";
import {DeckGoodsReceiptModule} from "./deck-goods-receipt/deck-goods-receipt.module";
import {DeliveriesService} from "../services/deliveries.service";

@NgModule({
  imports: [SharedModule,DeckTransfersModule,DeckDeliveriesModule,DeckReturnsModule,DeckRePackModule,DeckGoodsReceiptModule],       // module dependencies
  declarations: [DeckIndexComponent],   // components and directives
  exports: [],
  providers: [DeliveriesService]                    // services
})
export class DeckIndexModule {
}


