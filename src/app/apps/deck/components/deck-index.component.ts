import {Component, OnInit} from "@angular/core";
import {SideNavService} from "../../../shared/sidenav/service/sidenav.service";
import {Title} from "@angular/platform-browser";
import {TranslationService} from "../../../shared/i18n/translation.service";
import {TranslatableComponent} from "../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-deck-index',
  templateUrl: 'deck-index.component.html',
  styleUrls: ['deck-index.component.css']
})

export class DeckIndexComponent extends TranslatableComponent implements OnInit {

  selectedIndex = 0;

  constructor(private sideNavService: SideNavService, private titleService: Title, protected translationService:TranslationService) {
    super(translationService)
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Deck");
  }

  ngOnInit() {}

  handleTabChange(event){
      this.selectedIndex = event.index;
  }
}
