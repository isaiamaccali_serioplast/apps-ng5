import {DeckTransfersComponent} from "./deck-transfers.component";
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {TransfersService} from "../../services/transfers.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [DeckTransfersComponent],   // components and directives
  exports: [DeckTransfersComponent],
  providers: [TransfersService]                    // services
})
export class DeckTransfersModule {}


