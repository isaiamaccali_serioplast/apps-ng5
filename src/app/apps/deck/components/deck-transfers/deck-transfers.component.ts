import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage, cloneObject, fixValue,
  parseSerionetRespToData
} from "../../../../shared/utils";
import {Warehouse_SAP, WarehouseSAPService} from "../../../_common/services/warehouses.service";
import {TransferHeader, TransferRow, TransfersService} from "../../services/transfers.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {PackagingSapService} from "../../../_common/services/packaging-sap.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {Material} from "../../../_common/services/materials.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'deck-transfers',
  templateUrl: 'deck-transfers.component.html',
  styleUrls: ['deck-transfers.component.css']
})

export class DeckTransfersComponent extends TranslatableComponent implements OnInit {

  sendingSites: Site[] = [];
  sendingPlant: Site = new Site();
  receivingPlant: Site = new Site();

  receivingWarehouses: Warehouse_SAP[] = [];
  sendingWarehouses: Warehouse_SAP[] = [];
  transfer = new TransferHeader();

  additionalQueryTypes = {};
  selectionOn = true;

  constructor(protected translationService: TranslationService, private warehouseSAPService: WarehouseSAPService,
              private transfersService: TransfersService, private packagingSapService: PackagingSapService,
              private messageService:MessageService, private loadingService:LoadingService,
              private sitesService:SitesService
  ) {
    super(translationService);
  }

  ngOnInit() {
    this.transfer.RowLink = [];
  }

  canConfirmSelection(){
    return this.transfer.ReceivingPlant && this.transfer.ReceivingWarehouse &&
        this.transfer.SendingPlant && this.transfer.SendingWarehouse &&
      !((this.transfer.ReceivingPlant == this.transfer.SendingPlant)&&(this.transfer.ReceivingWarehouse == this.transfer.SendingWarehouse));
  }

  confirmSelection(){
    let w = this.sendingWarehouses.filter((w)=>w.WarehouseCode==this.transfer.SendingWarehouse)[0];
    let sap_mat_types = w.WHMatTypeLink.results.map((wt)=>wt.MaterialTypeCode);
    this.additionalQueryTypes = {sap_code__in:sap_mat_types.join("|")};
    this.selectionOn = false;
  }

  undoSelection(){
    if(this.transfer.RowLink.length>0){
      this.messageService.open({
        title:this.tr("ATTENTION"),
        message:this.tr("TRANSFER_DATA_ERASE"),
        confirm:() =>{
          this.selectionOn = true;
          this.reset();
        },
        deny:() =>{}
      })
    }
    else {
      this.selectionOn = true;
      this.reset();
    }
  }

  loadReceivingWarehouses() {
    this.warehouseSAPService.getWarehouseSAPs({Plant: this.receivingPlant.code}).subscribe((data) => {
      this.receivingWarehouses = data['response']['results'];
    })
  }

  loadSendingWarehouses() {
    if(this.transfer.ReceivingWarehouse && this.sendingPlant.pk){
      this.warehouseSAPService.getWarehouseSAPs({Plant: this.sendingPlant.code,WarehouseCode_start:this.transfer.ReceivingWarehouse.substr(0,3)}).subscribe((data) => {
        this.sendingWarehouses = data['response']['results'];
      })
    }
  }

  handleReceivingPlantChange(s: Site) {
    this.receivingPlant = s;
    this.transfer.ReceivingPlant = this.receivingPlant.code;
    this.loadReceivingWarehouses();
    this.sendingPlant = new Site();
    this.sitesService.getSites({
      is_active: true,
      legalentity_ref: this.receivingPlant.legalentity_ref.pk,
      active_on_sap: true
    }).subscribe((data) => {
      this.sendingSites = parseSerionetRespToData(data);
    });
    this.sendingWarehouses = [];
  }

  handleRecevingWarehouseChange() {
    this.loadSendingWarehouses();
  }

  handleSendingPlantChange() {
    fixValue(this.sendingSites, this, 'sendingPlant');
    this.transfer.SendingPlant = this.sendingPlant.code;
    this.loadSendingWarehouses();
  }

  handleSendingWarehouseChange(){

  }

  canSave() {
    return this.transfer.RowLink.filter((tr) => {
        return tr.Material && tr.Quantity
      }).length > 0
  }

  saveTransfer() {
    this.messageService.open({
      title:" ",
      message:this.tr("SAVE_CONFIRM"),
      confirm:() =>{
        let tr_ = cloneObject(this.transfer);
        tr_.RowLink.forEach((tr) => {
          tr.Quantity = tr.Quantity.toExponential();
        });
        this.loadingService.show();
        this.transfersService.saveTransfer(tr_).subscribe((resp) => {
          this.loadingService.hide();
          if(resp.status==201){
            this.messageService.open({
              message: DeckTransfersComponent.buildTransferMessage(resp),
              title:" ",
              autoClose: false,
              closeByOverlayClick:false,
              confirm:() =>{

              },
              onClose:() =>{
                this.reset();
              }
            });
          }
          else {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        });
      },
      deny:() =>{

      }
    });
  }

  static buildTransferMessage(resp){
    let tr:TransferHeader = resp['response'];
    let data = [];
    if(tr.Order){
      data.push("Transfer Order: "+tr.Order)
    }
    if(tr.InbDelivery){
      data.push("Inbound Delivery: "+tr.InbDelivery)
    }
    if(tr.OutDelivery){
      data.push("Outbound Delivery: "+tr.OutDelivery)
    }
    return data.join("<br/>");
  }

  reset() {
    this.selectionOn = true;
    this.transfer = new TransferHeader();
    this.transfer.RowLink = [];
    this.handleReceivingPlantChange(this.receivingPlant);
    this.sendingPlant = new Site();
    this.sendingWarehouses = [];

  }

  setMaterialToTransferRow(m: Material, t: TransferRow) {
    if (m) {
      t.Material = m.code;

      this.packagingSapService.getPackagings({Material: m.code, Plant:this.transfer.SendingPlant}).subscribe((resp) => {
        if(resp.status==200){
          t.packingInstructions = resp.response.results;
        }
      })
    }
    else {
      t = new TransferRow();
      t.packingInstructions = [];
    }
  }

  addTransferRow() {
    let tr = new TransferRow();
    tr.packingInstructions = [];
    tr.Palletization = " ";
    this.transfer.RowLink.push(tr);
  }

  canAdd() {
    let canAdd = true;
    this.transfer.RowLink.forEach((tr) => {
      if (!tr.Material || !tr.Quantity) {
        canAdd = false;
      }
    });
    return canAdd;
  }

  isDuplicate(tr:TransferRow){
    let isDupl = false;
    this.transfer.RowLink.forEach((tr_) =>{
      if(tr_!=tr){
        if((tr_.Material==tr.Material) && (tr_.Palletization==tr.Palletization)){
          isDupl = true;
        }
      }
    });
    return isDupl;
  }

  removeTransferRow(tr:TransferRow){
    this.transfer.RowLink.splice(this.transfer.RowLink.indexOf(tr),1);
  }

  getWarehouseDesc(warehouses:Warehouse_SAP[],warehouseCode:string){
    let w = warehouses.filter((w)=>w.WarehouseCode==warehouseCode)[0];
    if(w)
      return w.Description+" ("+w.WarehouseCode+")";
  }
}
