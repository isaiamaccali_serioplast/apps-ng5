import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {PurchaseOrder, PurchaseOrderItem, PurchaseOrderService} from "../../services/purchase-order.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {buildErrorMessage, cloneObject} from "../../../../shared/utils";
import {DeliveriesService, DeliveryHeader, DeliveryItem} from "../../services/deliveries.service";
import {HUDeck, HUDeckService} from "../../services/hu-deck.service";
import {SupplierBatch, SupplierBatchService} from "../../../_common/services/supplier-batch.service";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {HULabel, LabelHandlingUnitsQualitySapService} from "../../../_common/services/label-hu-quality-sap.service";
import {PrinterSAP, PrinterSapService} from "../../../_common/services/printers-sap.service";
import {UserSitesWithSAPComponent} from "../../../../shared/components/user-sites-with-sap/user-sites-with-sap.component";
import {HandlingUnitsQualitySapService, HUQuality} from "../../../_common/services/handling-units-quality-sap.service";
import {PackagingSap, PackagingSapService} from "../../../_common/services/packaging-sap.service";
import {isNullOrUndefined} from "util";
import {Site} from "../../../_common/services/sites.service";
import {HeaderConf, TableRowAction} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";
import {UserSitesComponent} from "../../../../shared/components/user-sites/user-sites.component";

@Component({
  selector: 'deck-goods-receipt',
  templateUrl: 'deck-goods-receipt.component.html',
  styleUrls: ['deck-goods-receipt.component.css']
})

export class DeckGoodsReceiptComponent extends TranslatableComponent implements OnInit {

  @ViewChild('printSettingDialog') printSettingDialog: DialogComponent;
  @ViewChild('printSettingDialogEdit') printSettingDialogEdit: DialogComponent;
  @ViewChild('printSettingDialogPO') printSettingDialogPO: DialogComponent;
  @ViewChild('printSettingDialogEditPO') printSettingDialogEditPO: DialogComponent;
  @ViewChild('sitesUser') sitesUser: UserSitesComponent;

  purchaseOrder: PurchaseOrder = new PurchaseOrder();
  deliveryHeader: DeliveryHeader = new DeliveryHeader();
  printers: PrinterSAP[] = [];
  itemToPrint: PurchaseOrderItem = new PurchaseOrderItem();
  labels: any = [];

  poPlant: Site = new Site();

  disableEdit = false;

  availableActionsSupplierBatch: TableRowAction[] = [];
  headerConfSupplierBatch: HeaderConf[] = [
    {
      columnId: 'supplier_batch',
      valueKey: 'SuppBatch',
      label: this.tr('SUPPLIER_BATCH')
    },
    {
      columnId: 'batch',
      valueKey: 'Batch',
      label: this.tr('BATCH')
    },
    {
      columnId: 'amount',
      valueFunction: (h) => {
        return parseFloat(h.Quantity);
      },
      label: this.tr('AMOUNT')
    }
  ];

  availableActionsHU: TableRowAction[] = [];
  headerConfHU: HeaderConf[] = [
    {
      columnId: 'sscc',
      valueKey: 'SSCC',
      label: this.tr('SSCC')
    },
    {
      columnId: 'batch',
      valueKey: 'Batch',
      label: this.tr('BATCH')
    },
    {
      columnId: 'supplier_batch',
      valueKey: 'SuppBatch',
      label: this.tr('SUPPLIER_BATCH')
    },
    {
      columnId: 'amount',
      valueFunction: (h: HUDeck) => {
        return parseFloat(h.PackedQty) + " " + h.Uom;
      },
      label: this.tr('AMOUNT')
    }
  ];

  constructor(protected translationService: TranslationService, private purchaseOrderService: PurchaseOrderService,
              private loadingService: LoadingService, private messageService: MessageService, private deliveryService: DeliveriesService,
              private huDeckService: HUDeckService, private supplierBatchService: SupplierBatchService,
              private labelService: LabelHandlingUnitsQualitySapService, private printerService: PrinterSapService,
              private huQualityService: HandlingUnitsQualitySapService, private packingSapService: PackagingSapService) {
    super(translationService)
  }

  ngOnInit() {
    let printAction = new TableRowAction();
    printAction.icon = "print";
    printAction.tooltip = "Print";
    printAction.click = (h) => this.printBatch(h);
    this.availableActionsSupplierBatch.push(printAction);

    let printActionHU = new TableRowAction();
    printActionHU.icon = "print";
    printActionHU.tooltip = "Print";
    printActionHU.click = (h) => this.rePrintHU(h);
    this.availableActionsHU.push(printAction);
  }

  loadPurchaseOrder() {
    this.loadingService.show();
    this.purchaseOrderService.getPurchaseOrders({
      Order: this.purchaseOrder.Order,
      Plant: this.poPlant.code
    }).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        let po: PurchaseOrder = resp['response']['results'][0];

        if (this.validatePlant(po.Plant)) {
          if (po.InbDel) {
            this.deliveryHeader.Delivery = po.InbDel;
            this.loadDelivery();
          }
          else {
            this.purchaseOrder = new PurchaseOrder();
            for (let key in po) {
              if (po.hasOwnProperty(key))
                this.purchaseOrder[key] = po[key];
            }
            this.purchaseOrder.POItemLink = this.purchaseOrder.POItemLink['results'].map((poi: PurchaseOrderItem) => {
              poi.Quantity = parseFloat(poi.Quantity + "");
              poi.originalQuantity = poi.Quantity + 0;
              poi.SuppBatchLink = [];
              return poi;
            });
          }
        }
        else {
          this.messageService.open({
            title: 'Error',
            error: true,
            message: this.tr("INVALID_PLANT"),
            autoClose: false
          });
        }
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  validatePlant(plant: string) {
    let plants = this.sitesUser.sites.map((s: Site) => s.code);
    let isValid = plants.indexOf(plant) != -1;
    if (isValid) {
      this.loadingService.show();
      this.printerService.getPrinters({Plant: plant, RFID: false}).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          this.printers = resp['response']['results'];
        }
        else {
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: this.tr("NO_PRINTERS_AVAILABLE"),
            autoClose: false
          });
        }
      });
      this.loadingService.show();
      this.labelService.getLabelsHandlingUnitsQuality({Plant: plant}).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          this.labels = resp['response']['results'];
        }
        else {
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      });
      return true;
    }
    else {
      return false;
    }
  }

  loadDelivery() {
    this.loadingService.show();
    this.deliveryService.getDelivery(this.deliveryHeader.getId()).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        let d: DeliveryHeader = resp['response'];

        if (this.validatePlant(d.Plant)) {
          this.deliveryHeader = new DeliveryHeader();
          for (let key in d) {
            if (d.hasOwnProperty(key))
              this.deliveryHeader[key] = d[key];
          }

          if (this.deliveryHeader.Type == 'INB' && !this.deliveryHeader.isClosed()) {
            this.deliveryHeader.DelItemLink = this.deliveryHeader.DelItemLink['results'];
            this.deliveryHeader.DelHuLink = this.deliveryHeader.DelHuLink['results'];
            this.deliveryHeader.DelItemLink.forEach((di: DeliveryItem) => {
              this.packingSapService.getPackaging("Palletization='" + di.Palletization + "',Plant='" + di.Plant + "'").subscribe((resp) => {
                if (resp.status == 200) {
                  let p: PackagingSap = resp['response'];
                  di.palletizationDefQty = p.DefaultQty;
                }
                else {
                  resp.context = "SAP";
                  this.messageService.open({
                    title: 'Error',
                    error: true,
                    message: buildErrorMessage(resp),
                    autoClose: false
                  });
                }
              })
            })
          }
          else {
            this.messageService.open({
              title: 'Error',
              error: true,
              message: this.tr("INVALID_DELIVERY"),
              autoClose: false
            });
          }
        }
        else {
          this.messageService.open({
            title: 'Error',
            error: true,
            message: this.tr("INVALID_PLANT"),
            autoClose: false
          });
        }

      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  printItem(poi: PurchaseOrderItem) {
    this.itemToPrint = poi;
  }

  savePO() {
    let po = cloneObject(this.purchaseOrder);
    po.POItemLink = po.POItemLink.filter((poi) => poi.selected);
    this.loadingService.show();
    this.purchaseOrderService.savePurchaseOrder(po).subscribe((response) => {
      this.loadingService.hide();
      if (response.status == 201) {
        if (this.printers.length > 0) {
          this.batchPrintPO(po);
        }
        else {
          this.messageService.open({
            title: ' ',
            error: false,
            message: this.tr("SAVE_COMPLETED"),
            autoClose: true,
            onClose: () => {
              this.purchaseOrder = new PurchaseOrder();
              this.deliveryHeader = new DeliveryHeader();
              this.poPlant = new Site();
              this.disableEdit = false;
            }
          });
        }
      }
      else {
        response.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(response),
          autoClose: false
        });
      }
    })
  }

  addHuToItem(di: DeliveryItem) {
    let h = new HUDeck();
    h.DeliveryItem = di.Item;
    h.DeliveryNumber = di.Delivery;
    h.Palletization = di.Palletization;
    h.Plant = di.Plant;
    h.Uom = di.Uom;
    h.Material = di.Material;
    if (di.batch)
      h.Batch = di.batch;
    di.huToAdd = h;

    let target = parseFloat(di.Quantity);
    let currentAmount = 0;
    if (di.hus)
      di.hus.forEach((hu: HUDeck) => {
        currentAmount += parseFloat(hu.PackedQty);
      });
    let missing = target - currentAmount;
    if (missing > di.palletizationDefQty) {
      h.PackedQty = parseFloat(di.palletizationDefQty);
    }
    else {
      h.PackedQty = missing;
    }
    di.enable_add = true;
  }

  confirmAddHuToItem(di: DeliveryItem) {
    let hu: HUDeck = cloneObject(di.huToAdd);
    if (hu.SuppBatch) {
      if (di.hus) {
        let match = di.hus.filter((hu_: HUDeck) => {
          return hu_.SuppBatch == hu.SuppBatch
        });
        if (match && match.length > 0) {
          hu.Batch = match[0].Batch;
        }
        else {
          hu.Batch = "";
        }
      }
      else {
        hu.Batch = "";
      }

    }

    let target = parseFloat(di.Quantity);
    let currentAmount = 0;
    if (di.hus) {
      di.hus.forEach((hu: HUDeck) => {
        currentAmount += parseFloat(hu.PackedQty);
      });
    }
    let missing = target - currentAmount;
    if (hu.PackedQty * hu.repeats > missing) {
      this.messageService.open({
        title: 'Error',
        error: true,
        message: this.tr("QUANTITY_EXCEEDS_LIMITS"),
        autoClose: false
      });
    }
    else {
      hu.PackedQty = hu.PackedQty.toExponential();
      let rep = hu.repeats;
      this.loadingService.show();
      this.huDeckService.saveHU(hu).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 201) {
          let hu = resp['response'];
          di.batch = hu.Batch;
          if (!di.hus) di.hus = [];
          di.hus.push(hu);
          if (!this.deliveryHeader.DelHuLink)
            this.deliveryHeader.DelHuLink = [];
          this.deliveryHeader.DelHuLink.push(hu);
          //self.printHU(hu,function(){
          di.enable_add = false;
          if (rep > 1) {
            hu.repeats = rep;
            // this.loadingService.show();
            this.processHURepeats(hu, di, 0);
          }
          //},rep!=1);
        }
        else {
          this.loadingService.hide();
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      });
    }
  }

  addSupplierBatchToItem(poi: PurchaseOrderItem) {
    let b = new SupplierBatch();
    b.Item = poi.Item;
    b.Order = poi.Order;
    b.Plant = poi.Plant;
    b.Material = poi.Material;
    b.Supplier = this.purchaseOrder.Supplier;
    poi.bToAdd = b;

    b.Quantity = this.calculateMissingPOI(poi);
    poi.enable_add = true;
  }

  calculateMissingPOI(poi) {
    let target = parseFloat(poi.Quantity + "");
    let currentAmount = 0;
    if (poi.SuppBatchLink) {
      poi.SuppBatchLink.forEach((sb) => {
        currentAmount += parseFloat(sb.Quantity);
      })
    }

    return target - currentAmount;
  }

  printHU(hu: HUDeck, callback?: any) {
    let doPrint = (hu: HUDeck) => {
      let huToPrint = new HUQuality();
      huToPrint.Plant = hu.Plant;
      huToPrint.Material = hu.Material;
      huToPrint.Quantity = hu.PackedQty;
      huToPrint.Batch = hu.Batch;
      huToPrint.Uom = hu.Uom;
      huToPrint.Palletization = hu.Palletization;
      huToPrint.Delivery = hu.DeliveryNumber;
      huToPrint.Printer = this.deliveryHeader.printer;
      huToPrint.HULabelLink = [];
      let lab = new HULabel();
      lab.LabelName = this.deliveryHeader.label;
      lab.Copies = this.deliveryHeader.copies;
      huToPrint.HULabelLink.push(lab);
      huToPrint.SSCC = hu.SSCC;
      this.loadingService.show();
      this.huQualityService.createHandlingUnitsQuality(huToPrint).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 201) {
          if (!callback) {
            this.messageService.open({
              title: ' ',
              error: false,
              message: this.tr("REQUEST_COMPLETED"),
              autoClose: false
            });
          }
          hu.has_printed = true;
          if (callback)
            callback.call(this, hu);
        }
        else {
          hu.has_printed = false;
          if (callback) {
            callback.call(this, hu);
          }
          else {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        }
      });
    };

    if (this.deliveryHeader.printer && this.deliveryHeader.label && !isNullOrUndefined(this.deliveryHeader.copies)) {
      doPrint(hu);
    }
    else {
      this.setPrintInfo(() => {
        doPrint(hu);
      })
    }
  }

  setPrintInfo(callback?: any) {
    if (callback) {
      let o = this.printSettingDialog.onClose.subscribe(() => {
        if (callback) {
          callback.call(this);
          o.unsubscribe();
        }
      });
      this.printSettingDialog.open();
    }
    else {
      this.printSettingDialogEdit.open();
    }
  }

  confirmAddBatchToItem(poi: PurchaseOrderItem) {

    let b: SupplierBatch = cloneObject(poi.bToAdd);

    if (this.calculateMissingPOI(poi) < b.Quantity) {
      this.messageService.open({
        title: 'Error',
        error: true,
        message: this.tr("QUANTITY_EXCEEDS_LIMITS"),
        autoClose: false
      });
    }
    else {
      b.Quantity = b.Quantity.toExponential();
      this.loadingService.show();
      this.supplierBatchService.saveSupplierBatch(b).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 201) {
          poi.selected = true;
          poi.SuppBatchLink.push(resp['response']);
          //this.printBatch(resp['response']);
          poi.enable_add = false;
        }
        else {
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      });
    }
  }

  saveDelivery() {
    this.loadingService.show();
    this.deliveryService.saveDelivery(this.deliveryHeader).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 201) {
        if (this.printers.length > 0) {
          this.batchPrintDelivery(this.deliveryHeader);
        }
        else {
          this.messageService.open({
            title: ' ',
            error: false,
            message: this.tr("SAVE_COMPLETED"),
            autoClose: true,
            onClose: () => {
              this.purchaseOrder = new PurchaseOrder();
              this.deliveryHeader = new DeliveryHeader();
              this.poPlant = new Site();
              this.disableEdit = false;
            }
          });
        }
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  reset() {
    let count = 1;
    let callback = () => {
      count--;
      if (count == 0) {
        this.purchaseOrder = new PurchaseOrder();
        this.deliveryHeader = new DeliveryHeader();
        this.poPlant = new Site();
      }
    };

    if (this.purchaseOrder.POItemLink) {

      if (this.purchaseOrder.POItemLink.length > 0) {
        count = 0;

        this.purchaseOrder.POItemLink.forEach((poi: PurchaseOrderItem) => {
          count += poi.SuppBatchLink.length;
        });

        if (count > 0) {
          this.purchaseOrder.POItemLink.forEach((poi: PurchaseOrderItem) => {

            if (poi.SuppBatchLink.length > 0) {
              poi.SuppBatchLink.forEach((sb: SupplierBatch) => {
                let sb_ = new SupplierBatch();
                for (let key in sb) {
                  if (sb.hasOwnProperty(key)) {
                    sb_[key] = sb[key];
                  }
                }
                this.loadingService.show();
                this.supplierBatchService.deleteSupplierBatch(sb_).subscribe((resp) => {
                  this.loadingService.hide();
                  if (resp.status == 204) {
                    callback();
                  }
                  else {
                    resp.context = "SAP";
                    this.messageService.open({
                      title: 'Error',
                      error: true,
                      message: buildErrorMessage(resp),
                      autoClose: false
                    });
                  }
                })
              });
            }

          })
        }
        else {
          count = 1;
          callback();
        }
      }
      else {
        count = 1;
        callback();
      }
    }

    if (this.deliveryHeader.DelItemLink) {
      count = this.deliveryHeader.DelHuLink.length;
      if (count > 0) {
        this.deliveryHeader.DelHuLink.forEach((hu: HUDeck) => {
          let hu_ = new HUDeck();
          for (let key in hu) {
            if (hu.hasOwnProperty(key)) {
              hu_[key] = hu[key];
            }
          }
          this.loadingService.show();
          this.huDeckService.deleteHU(hu_).subscribe((resp) => {
            this.loadingService.hide();
            if (resp.status == 204) {
              callback();
            }
            else {
              resp.context = "SAP";
              this.messageService.open({
                title: 'Error',
                error: true,
                message: buildErrorMessage(resp),
                autoClose: false
              });
            }
          })
        })
      }
      else {
        count = 1;
        callback();
      }
    }
  }

  parseFloat(n) {
    return parseFloat(n);
  }

  canSaveDelivery() {
    let canSave = true;
    this.deliveryHeader.DelItemLink.forEach((di: DeliveryItem) => {
      if (!di.hus || di.hus.length == 0) {
        canSave = false;
      }
    });
    return canSave;

  }

  targetReached(di: DeliveryItem) {
    let target = parseFloat(di.Quantity);
    let currentAmount = 0;
    if (di.hus) {
      di.hus.forEach((hu: HUDeck) => {
        currentAmount += parseFloat(hu.PackedQty);
      });
    }
    return target == currentAmount;
  }

  checkQuantity(poi: PurchaseOrderItem) {
    if (poi.Quantity > poi.originalQuantity) {
      this.messageService.open({
        title: 'Error',
        error: true,
        message: this.tr("QUANTITY_EXCEEDS_ORDERED_QUANTITY"),
        autoClose: false,
        onClose: () => {
          poi.Quantity = poi.originalQuantity;
        }
      });
    }
  }

  rePrintHU(h: HUDeck) {
    this.printHU(h);
  }

  printBatch(b: SupplierBatch, callbackAfterPrint?: any) {
    let doPrint = (b: SupplierBatch) => {
      let b_ = new SupplierBatch();
      for (let key in b) {
        if (b.hasOwnProperty(key)) {
          b_[key] = b[key];
        }
      }
      b_.Printer = this.purchaseOrder.printer;
      b_.Copies = this.purchaseOrder.copies;
      this.loadingService.show();
      this.supplierBatchService.printSupplierBatch(b_).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 204 || resp.status == 201) {
          b.has_printed = true;
          if (!callbackAfterPrint) {
            this.messageService.open({
              title: ' ',
              error: false,
              message: this.tr("REQUEST_COMPLETED"),
              autoClose: false
            });
          }
          else {
            callbackAfterPrint.call(this, b);
          }

        }
        else {
          b.has_printed = false;
          if (!callbackAfterPrint) {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
          else {
            callbackAfterPrint.call(this, b);
          }
        }
      });
    };

    if (this.purchaseOrder.printer && this.purchaseOrder.copies) {
      doPrint(b);
    }
    else {
      this.setPrintInfoBatch(() => {
        doPrint(b);
      })
    }
  }

  setPrintInfoBatch(callback?: any) {
    if (callback) {
      let o = this.printSettingDialogPO.onClose.subscribe(() => {
        if (callback) {
          callback.call(this);
          o.unsubscribe();
        }
      });
      this.printSettingDialogPO.open();
    }
    else {
      this.printSettingDialogEditPO.open();
    }
  }

  siteSelected(s: Site) {
    this.poPlant = s;
  }

  processHURepeats(hu: HUDeck, di: DeliveryItem, index = 0) {
    if (index < (hu.repeats - 1)) {
      this.loadingService.show();
      this.huDeckService.saveHU(hu).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 201) {
          let hu_ = resp['response'];
          di.hus.push(hu_);
          this.deliveryHeader.DelHuLink.push(hu_);
          di.enable_add = false;
          this.processHURepeats(hu, di, index + 1);
        }
        else {
          resp.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(resp),
            autoClose: false
          });
        }
      });
    }
    else {
      di.enable_add = false;
      this.messageService.open({
        title: ' ',
        error: false,
        message: this.tr("REQUEST_COMPLETED"),
        autoClose: false
      });
    }
  }

  batchPrintPO(po: PurchaseOrder) {
    let batches = po.POItemLink.map((poi) => poi.SuppBatchLink);
    let batchesToPrint = [];
    for (let gb of batches) {
      batchesToPrint = batchesToPrint.concat(gb);
    }

    let batchesToPrintErrors = [];

    this.disableEdit = true;

    this.processBatchPrintSB(batchesToPrint, 0, batchesToPrintErrors, (errors) => {

      if (errors.length == 0) {
        this.messageService.open({
          title: ' ',
          error: false,
          message: this.tr("SAVE_COMPLETED"),
          autoClose: true,
          onClose: () => {
            this.purchaseOrder = new PurchaseOrder();
            this.deliveryHeader = new DeliveryHeader();
            this.poPlant = new Site();
            this.disableEdit = false;
          }
        });
      }
    })
  }

  processBatchPrintSB(batchesToPrint, index, batchesToPrintErrors, callback) {
    if (batchesToPrint[index]) {
      this.printBatch(batchesToPrint[index], (b: SupplierBatch) => {
        if (!b.has_printed) {
          batchesToPrintErrors.push(b);
        }
        this.processBatchPrintSB(batchesToPrint, index + 1, batchesToPrintErrors, callback);
      })
    }
    else {
      callback.call(this, batchesToPrintErrors);
    }
  }

  batchPrintDelivery(d: DeliveryHeader) {
    let husToPrint: HUDeck[] = d.DelHuLink;
    let husToPrintErrors = [];

    this.disableEdit = true;
    this.loadingService.show();
    this.processBatchPrintHU(husToPrint, 0, husToPrintErrors, (errors) => {
      this.loadingService.hide();
      if (errors.length == 0) {
        this.messageService.open({
          title: ' ',
          error: false,
          message: this.tr("SAVE_COMPLETED"),
          autoClose: true,
          onClose: () => {
            this.purchaseOrder = new PurchaseOrder();
            this.deliveryHeader = new DeliveryHeader();
            this.poPlant = new Site();
            this.disableEdit = false;
          }
        });
      }
    })
  }

  processBatchPrintHU(husToPrint, index, husToPrintErrors, callback) {
    if (husToPrint[index]) {
      this.printHU(husToPrint[index], (b: SupplierBatch) => {
        if (!b.has_printed) {
          husToPrintErrors.push(b);
        }
        this.processBatchPrintHU(husToPrint, index + 1, husToPrintErrors, callback);
      })
    }
    else {
      callback.call(this, husToPrintErrors);
    }
  }

  restoreInterface() {
    this.purchaseOrder = new PurchaseOrder();
    this.deliveryHeader = new DeliveryHeader();
    this.poPlant = new Site();
    this.disableEdit = false;
  }
}
