import {DeckGoodsReceiptComponent} from "./deck-goods-receipt.component";
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {PurchaseOrderService} from "../../services/purchase-order.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [DeckGoodsReceiptComponent],   // components and directives
  exports: [DeckGoodsReceiptComponent],
  providers: [PurchaseOrderService]                    // services
})
export class DeckGoodsReceiptModule {}


