import {DeckReturnsComponent} from "./deck-returns.component";
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {HUDeckService} from "../../services/hu-deck.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [DeckReturnsComponent],   // components and directives
  exports: [DeckReturnsComponent],
  providers: [HUDeckService]                    // services
})
export class DeckReturnsModule {}


