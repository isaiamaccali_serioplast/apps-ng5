import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {buildErrorMessage, fixValue, parseSerionetRespToData} from "../../../../shared/utils";
import {TransferHeader, TransfersService} from "../../services/transfers.service";
import {HUDeck, HUDeckService} from "../../services/hu-deck.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

class HUReturnsStructure {
  selected: boolean;
  hus: HUDeck[];
  DeliveryNumber: string;
}

@Component({
  selector: 'deck-returns',
  templateUrl: 'deck-returns.component.html',
  styleUrls: ['deck-returns.component.css']
})
export class DeckReturnsComponent extends TranslatableComponent implements OnInit {

  sendingPlant: Site = new Site();
  receivingPlant: Site = new Site();
  receivingSites: Site[] = [];

  transfer = new TransferHeader();

  huReturns: HUDeck[] = [];

  loading = false;

  structures: HUReturnsStructure[] = [];

  constructor(protected translationService: TranslationService, private sitesService: SitesService,
              private transfersService: TransfersService, private huReturnService: HUDeckService,
              private messageService: MessageService, private loadingService: LoadingService) {
    super(translationService);
  }

  ngOnInit() {
    this.transfer.HULink = [];
  }

  handleSendingPlantChange(s: Site) {
    this.sendingPlant = s;
    this.transfer.SendingPlant = this.sendingPlant.code;
    this.sitesService.getSites({
      is_active: true,
      legalentity_ref: this.sendingPlant.legalentity_ref.pk,
      active_on_sap: true
    }).subscribe((data) => {
      this.receivingSites = parseSerionetRespToData(data);
    });
    this.loadHUs();
  }

  handleReceivingPlantChange() {
    fixValue(this.receivingSites, this, 'receivingPlant');
    this.transfer.ReceivingPlant = this.receivingPlant.code;
  }

  loadHUs() {
    this.huReturns = [];
    this.structures = [];
    this.loading = true;
    this.huReturnService.getHUReturns({Plant: this.sendingPlant.code, Return: 'TRUCK'}).subscribe((resp) => {
      this.loading = false;
      if (resp.status == 200) {
        this.huReturns = resp['response']['results'];
        this.buildDataStructure();
      }
      else {
        this.huReturns = [];
        this.structures = [];
      }
    })
  }

  buildDataStructure() {
    let struct = {};

    this.huReturns.forEach((hur: HUDeck) => {
      if (!struct[hur.DeliveryNumber])
        struct[hur.DeliveryNumber] = [];
      struct[hur.DeliveryNumber].push(hur);
    });
    for (let key in struct) {
      if (struct.hasOwnProperty(key)) {
        let s = new HUReturnsStructure();
        s.selected = false;
        s.DeliveryNumber = key;
        s.hus = struct[key];
        this.structures.push(s);
      }
    }
    this.structures.sort((a, b) => {
      return parseInt(a.DeliveryNumber, 10) - parseInt(b.DeliveryNumber, 10);
    })
  }

  canSave() {
    return this.transfer.ReceivingPlant && this.transfer.SendingPlant && this.transfer.HULink.length > 0;
  }

  saveHUReturn() {
    this.loadingService.show();
    this.transfersService.saveTransfer(this.transfer).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 201) {
        this.messageService.open({
          message: this.buildTransferMessage(resp),
          title: " ",
          autoClose: false,
          confirm: () => {

          },
          onClose: () => {
            this.reset();
          }
        });
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  processHULinks() {
    let selectedStructs = this.structures.filter((s) => s.selected);
    let hus = [];
    selectedStructs.forEach((s) => {
      hus = hus.concat.apply(hus, s.hus);
    });
    this.transfer.HULink = hus;
  }

  parseFloat(n) {
    return parseFloat(n);
  }

  buildTransferMessage(resp) {
    let tr: TransferHeader = resp['response'];
    return "Outbound Delivery: " + tr.OutDelivery;
  }

  reset() {
    this.transfer = new TransferHeader();
    this.transfer.HULink = [];
    this.handleSendingPlantChange(this.sendingPlant);
    this.sendingPlant = new Site();
  }
}
