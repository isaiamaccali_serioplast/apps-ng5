import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {DeliveriesService, DeliveryHeader, DeliveryItem} from "../../services/deliveries.service";
import {PrinterSAP, PrinterSapService} from "../../../_common/services/printers-sap.service";
import {DialogConfirmComponent} from "../../../../shared/components/dialog-confirm/dialog-confirm.component";
import {OutputMessage, OutputMessageService} from "../../../_common/services/output-message.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {buildErrorMessage, cloneObject, getUrlDate, sapDateToUrlDate} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {Site} from "../../../_common/services/sites.service";
import {HeaderConf} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {HUDeck} from "../../services/hu-deck.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'deck-deliveries',
  templateUrl: 'deck-deliveries.component.html',
  styleUrls: ['deck-deliveries.component.css']
})

export class DeckDeliveriesComponent extends TranslatableComponent implements OnInit {

  @ViewChild('printModalConfirm') printModalConfirm: DialogConfirmComponent;
  @ViewChild('editPlateDelivery') editPlateDelivery: DialogComponent;
  openDeliveries: DeliveryHeader[] = [];
  printers: PrinterSAP[] = [];
  loading = false;
  siteCode: string;

  odToPrint: DeliveryHeader;

  outputMessages: OutputMessage[] = [];
  outputMessage: OutputMessage;

  deliveryFilter: string;
  outbound: boolean = true;
  inbound: boolean = true;

  fromDate: Date;
  toDate: Date;

  deliveryToEdit: DeliveryHeader = new DeliveryHeader();

  public deliveriesCodeFilter: any; //Temp fix per warning

  headerConfItems: HeaderConf[] = [
    {
      columnId: 'item',
      label: 'Item',
      valueFunction: (i: DeliveryItem) => {
        if (i['sub']) {
          return '<b style="font-size:24px">↳</b>' + i.Item;
        }
        else {
          return i.Item;
        }
      }
    },
    {
      columnId: 'material',
      label: 'Material',
      valueFunction: (i: DeliveryItem) => {
        return i.Material + " - " + i.Description;
      }
    },
    {
      columnId: 'amount',
      label: 'Amount',
      valueFunction: (i: DeliveryItem) => {
        return parseFloat(i.Quantity) + " " + i.Uom;
      }
    },
    {
      columnId: 'palletization',
      valueKey: 'Palletization',
      label: 'Palletization'
    },
    {
      columnId: 'batch',
      label: 'Batch',
      valueFunction: (i: DeliveryItem) => {
        if (i['sub']) {
          return i['Batch'];
        }
        else {
          return "";
        }
      }
    },
  ];

  headerConfHU: HeaderConf[] = [
    {
      columnId: 'SSCC',
      label: 'SSCC',
      valueKey: 'SSCC'
    },
    {
      columnId: 'material',
      label: 'Material',
      valueFunction: (i: HUDeck) => {
        return i.Material;
      }
    },
    {
      columnId: 'batch',
      label: 'Batch',
      valueKey: 'Batch'
    },
    {
      columnId: 'amount',
      label: 'Amount',
      valueFunction: (i: HUDeck) => {
        return parseFloat(i.PackedQty) + " " + i.Uom;
      }
    },
    {
      columnId: 'warehouse',
      label: 'Warehouse',
      valueKey: 'Warehouse'
    },
    {
      columnId: 'palletization',
      valueKey: 'Palletization',
      label: 'Palletization'
    },
    {
      columnId: 'packaging_material',
      valueKey: 'PackagingMaterial',
      label: 'Packaging Material'
    },

  ];

  constructor(protected translationService: TranslationService, private openDeliveriesService: DeliveriesService,
              private printerSapService: PrinterSapService, private outputMessageService: OutputMessageService,
              private messageService: MessageService, private loadingService: LoadingService) {
    super(translationService);
  }

  ngOnInit() {
    this.toDate = new Date();
    let d = new Date();
    d.setDate(d.getDate() - 1);
    this.fromDate = d;
  }

  handlePlantChange(s: Site) {
    this.siteCode = s.code;
  }

  loadOpenDeliveries() {
    this.loading = true;
    this.openDeliveries = [];
    let from = this.fromDate ? getUrlDate(this.fromDate, "") : null;
    let to = this.fromDate ? getUrlDate(this.toDate, "") : null;
    this.loadingService.show();
    this.openDeliveriesService.getOpenDeliveries({Plant: this.siteCode, from: from, to: to}).subscribe((resp) => {
      this.loadingService.hide();
      this.loading = false;
      if (resp.status == 200) {
        let openDeliveries = resp['response']['results'];
        openDeliveries.forEach((d: DeliveryHeader) => {
          d.DelItemLink = d.DelItemLink['results'];
          d.DelItemLink.forEach((di) => {
            di.DelSubItmLink = di.DelSubItmLink['results'];
          })
        });
        this.openDeliveries = openDeliveries;
      }
      else {
        this.openDeliveries = [];
      }
    })
  }

  printOpenDelivery(od: DeliveryHeader) {
    this.odToPrint = od;

    let count = 2;

    let callback = () => {
      count--;
      if (count == 0) {
        this.printModalConfirm.open();
      }
    };

    this.outputMessage = new OutputMessage();

    this.printerSapService.getPrinters({Plant: od.Plant, Thermal: false, RFID: false}).subscribe((resp) => {
      if (resp.status == 200) {
        this.printers = resp['response']['results'];
        if (this.printers.length == 1)
          this.outputMessage.Printer = this.printers[0].Name;
        callback();
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.tr('NO_PRINTERS_AVAILABLE'),
          autoClose: false
        });
      }

    });

    this.outputMessageService.getOutputMessages({Object: od.Delivery, ObjectType: 'V2', Medium: '1'}).subscribe((resp) => {
      if (resp.status == 200) {
        if (resp['response']['results'].length > 0) {
          this.outputMessages = resp['response']['results'];
          if (this.outputMessages.length == 1)
            this.outputMessage = this.outputMessages[0];
          callback();
        }
        else {
          this.messageService.open({
            title: ' ',
            message: this.tr("NO_MESSAGE_AVAILABLE"),
            autoClose: false
          });
        }
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }

    })
  }

  doPrintOpenDelivery() {
    this.outputMessageService.saveOutputMessage(this.outputMessage).subscribe((resp) => {
      if (resp.status == 204) {
        this.messageService.open({
          title: ' ',
          error: false,
          message: this.tr("REQUEST_COMPLETED"),
          autoClose: true
        });
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  buildAddress(d: DeliveryHeader) {
    let add = [];
    if (d.Street)
      add.push(d.Street);
    if (d.PostalCode)
      add.push(d.PostalCode);
    if (d.City)
      add.push(d.City);
    if (d.Region)
      add.push(d.Region);
    if (d.Country)
      add.push(d.Country);
    return add.join(", ");
  }

  parseFloat(n) {
    return parseFloat(n);
  }

  fixOutputMessage() {
    this.outputMessages.forEach((o) => {
      if (o.MessageCode == this.outputMessage.MessageCode) {
        this.outputMessage.MessageCode = o.MessageCode;
        this.outputMessage.Description = o.Description;
        this.outputMessage.Medium = o.Medium;
        this.outputMessage.Object = o.Object;
        this.outputMessage.ObjectType = o.ObjectType;
      }
    })
  }

  isClosed(od: DeliveryHeader) {
    let od_ = new DeliveryHeader();
    od_.Status = od.Status;
    return od_.isClosed();
  }

  sapDateToUrlDate(value: string) {
    return sapDateToUrlDate(value);
  }

  sapHour(value: string) {
    if (value) {
      return [value.substring(0, 2), value.substring(2, 4), value.substring(4, 6)].join(":");
    }
    else {
      return "-";
    }
  }

  editPlate(od: DeliveryHeader) {
    this.deliveryToEdit = new DeliveryHeader();

    for (let key in od) {
      if (od.hasOwnProperty(key))
        this.deliveryToEdit[key] = od[key];
    }

    if (this.deliveryToEdit.DelItemLink)
      delete this.deliveryToEdit.DelItemLink;
    if (this.deliveryToEdit.DelHuLink)
      delete this.deliveryToEdit.DelHuLink;
    this.editPlateDelivery.open();
  }

  saveEditDelivery() {
    this.loadingService.show();
    this.editPlateDelivery.close();
    this.openDeliveriesService.saveDelivery(cloneObject(this.deliveryToEdit), false).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 204) {
        this.messageService.open({
          title: ' ',
          error: false,
          message: this.tr("SAVE_COMPLETED"),
          autoClose: true
        });
        this.openDeliveries.forEach((d: DeliveryHeader) => {
          if (d.Delivery == this.deliveryToEdit.Delivery) {
            d.Plate = this.deliveryToEdit.Plate;
          }
        });
        this.deliveryToEdit = new DeliveryHeader();
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  getODItems(od: DeliveryHeader) {
    let items = [];
    od.DelItemLink.forEach((di) => {
      items.push(di);
      let subItems = di.DelSubItmLink;
      subItems.forEach((si) => {
        si['sub'] = true;
        items.push(si);
      })
    });
    return items;
  }
}

