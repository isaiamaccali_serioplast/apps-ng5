import {FilterDeliveriesPipe, InBoundDeliveriesPipe,OutBoundDeliveriesPipe} from "./deliveries-pipes";
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {DeckDeliveriesComponent} from "./deck-deliveries.component";
import {MatNativeDateModule} from "@angular/material";

@NgModule({
  imports: [SharedModule, MatNativeDateModule],       // module dependencies
  declarations: [DeckDeliveriesComponent,OutBoundDeliveriesPipe,InBoundDeliveriesPipe,FilterDeliveriesPipe],   // components and directives
  exports: [DeckDeliveriesComponent],
  providers: []                    // services
})
export class DeckDeliveriesModule {}


