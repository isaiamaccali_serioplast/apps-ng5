import { Pipe, PipeTransform } from '@angular/core';
import {DeliveryHeader} from "../../services/deliveries.service";

@Pipe({
  name: 'outboundDeliveries'
})
export class OutBoundDeliveriesPipe implements PipeTransform {

  transform(items: DeliveryHeader[]): DeliveryHeader[] {
    return items.filter((item)=>item.Type=='OUTB');
  }
}

@Pipe({
  name: 'inboundDeliveries'
})
export class InBoundDeliveriesPipe implements PipeTransform {

  transform(items: DeliveryHeader[]): DeliveryHeader[] {
    return items.filter((item)=>item.Type=='INB');
  }
}

@Pipe({
  name: 'deliveriesCodeFilter'
})
export class FilterDeliveriesPipe implements PipeTransform {

  transform(items: DeliveryHeader[], filter:string, outbound:boolean): DeliveryHeader[] {
    if(filter){
      return items.filter((item)=>
        {
          return item.Delivery.indexOf(filter)!=-1;
        }
      );
    }
    else {
      return items;
    }
  }
}
