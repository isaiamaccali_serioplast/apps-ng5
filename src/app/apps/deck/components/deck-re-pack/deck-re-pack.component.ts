import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage, cloneObject} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {HandlingUnitSap, HandlingUnitSapService} from "../../../_common/services/handling-units-sap.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {PackagingSap, PackagingSapService} from "../../../_common/services/packaging-sap.service";
import {PrinterSAP, PrinterSapService} from "../../../_common/services/printers-sap.service";
import {HULabel, LabelHandlingUnitsQualitySapService} from "../../../_common/services/label-hu-quality-sap.service";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {HandlingUnitsQualitySapService, HUQuality} from "../../../_common/services/handling-units-quality-sap.service";
import {Material} from "../../../_common/services/materials.service";
import {Site} from "../../../_common/services/sites.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'deck-re-pack',
  templateUrl: 'deck-re-pack.component.html',
  styleUrls: ['deck-re-pack.component.css']
})

export class DeckRePackComponent extends TranslatableComponent implements OnInit {

  @ViewChild('printDialog') printDialog: DialogComponent;
  @ViewChild('amountDialog') amountDialog: DialogComponent;
  site: Site = new Site();
  material: Material = null;
  packInstr: PackagingSap[] = [];

  hu: HandlingUnitSap = new HandlingUnitSap();

  printers: PrinterSAP[] = [];
  huLabels: HULabel[] = [];

  huToPrint: HUQuality = new HUQuality();

  constructor(protected translationService: TranslationService, private loadingService: LoadingService,
              private loginService: LoginService, private handlingUnitSapService: HandlingUnitSapService,
              private messageService: MessageService, private packagingSapService: PackagingSapService,
              private sapPrintersService: PrinterSapService, private labelService: LabelHandlingUnitsQualitySapService,
              private huQualityService: HandlingUnitsQualitySapService) {
    super(translationService);
  }

  ngOnInit() {
  }

  siteChange(s: Site) {
    if (s && s.pk) {
      this.site = s;
      this.hu.Plant = this.site.code;
    }
    else {
      this.site = null;
    }
  }

  selectedMaterial(m: Material) {
    if (m && m.pk) {
      this.material = m;
      this.hu.Material = this.material.code;
      this.packagingSapService.getPackagings({Material: this.hu.Material, Plant: this.hu.Plant}).subscribe((resp) => {
        if (resp.status == 200) {
          this.packInstr = resp.response.results;
        }
      })
    }
    else {
      this.material = null;
      this.packInstr = [];
    }
  }

  saveRepack() {
    this.amountDialog.close();
    let hu: HandlingUnitSap = cloneObject(this.hu);
    hu.CID = this.loginService.user.badge_code ? this.loginService.user.badge_code : '99999';
    hu.PackedQty = hu.PackedQty.toExponential();

    this.loadingService.show();

    this.handlingUnitSapService.createHandlingUnit(hu).subscribe((response) => {

      if (response["status"] == 201) {

        let huReturned: HandlingUnitSap = response['response'];
        this.huQualityService.getHandlingUnitQuality("'" + huReturned.SSCC + "'").subscribe((response) => {
          this.huToPrint = response['response'];

          this.messageService.open({
            title: ' ',
            error: false,
            message: "SSCC: " + huReturned.SSCC,
            autoClose: false,
            confirm: () => {
            },
            onClose: () => {
              this.sapPrintersService.getPrinters({Plant: this.huToPrint.Plant, Thermal: true, RFID: false}).subscribe((response) => {
                if (response['status'] == 200) {
                  this.printers = response['response']['results'];
                  this.labelService.getLabelsHandlingUnitsQuality({
                    Plant: this.hu.Plant,
                    SSCC: this.huToPrint.SSCC
                  }).subscribe((response) => {
                    this.huLabels = response['response']['results'];
                    this.huToPrint.HULabelLink = [new HULabel()];
                    this.loadingService.hide();
                    this.openDialogPrint();
                  })
                }
                else {
                  this.messageService.open({
                    title: 'Error',
                    error: true,
                    message: this.tr('NO_PRINTERS_AVAILABLE'),
                    autoClose: false,
                    confirm: () => {

                    },
                    onClose: () => {
                      this.reset();
                    }
                  });
                }
              });
            }
          });
        });
      }
      else {
        this.loadingService.hide();
        response.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(response),
          autoClose: false
        });
      }
    });
  }

  openDialogPrint() {
    this.printDialog.open();
  }

  reset() {
   //?
  }

  canSave() {
    return this.hu.Material && this.hu.Palletization && this.hu.Plant;
  }

  doPrintHU() {
    this.loadingService.show();
    this.huQualityService.createHandlingUnitsQuality(this.huToPrint).subscribe((resp) => {
      this.loadingService.hide();
      this.printDialog.close();
      if (resp.status == 201) {
        this.messageService.open({
          title: ' ',
          error: false,
          message: this.tr("REQUEST_COMPLETED"),
          autoClose: false,
          onClose: () => {
            this.reset();
          }
        });
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  };

  handleLabelChange() {
    for (let l of this.huLabels) {
      if (this.huToPrint.HULabelLink[0].LabelName == l.LabelName) {
        //lo clono in modo da poter modificare le copie senza problemi
        this.huToPrint.HULabelLink[0] = cloneObject(l);
        this.huToPrint.HULabelLink[0].SSCC = this.huToPrint.SSCC;
      }
    }
  }

  openAmountDialog() {
    let defQty = 0;
    this.packInstr.forEach((p) => {
      if (p.Palletization == this.hu.Palletization) {
        defQty = p.DefaultQty;
      }
    });
    this.hu.PackedQty = parseFloat(defQty + "");
    this.amountDialog.open();
  }
}
