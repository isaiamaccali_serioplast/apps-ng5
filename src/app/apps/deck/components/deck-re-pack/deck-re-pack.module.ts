import {DeckRePackComponent} from "./deck-re-pack.component";
import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [DeckRePackComponent],   // components and directives
  exports: [DeckRePackComponent],
  providers: []                    // services
})
export class DeckRePackModule {}


