import {Injectable} from "@angular/core";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {cloneObject} from "../../../shared/utils";
import {PackagingSap} from "../../_common/services/packaging-sap.service";
import {HUDeck} from "./hu-deck.service";

declare let sap: any;

export class TransferHeader{
  Order:string;
  SendingPlant:string;
  SendingWarehouse:string;
  ReceivingPlant:string;
  ReceivingWarehouse:string;
  DeliveryDate:string;
  InbDelivery:string;
  OutDelivery:string;
  RowLink:TransferRow[];
  HULink?:HUDeck[];
}

export class TransferRow {
  Order:string;
  Item:string;
  Material:string;
  Quantity:any;
  Palletization:string;

  packingInstructions:PackagingSap[];
}


@Injectable()
export class TransfersService extends GenericSAPService{

  constructor(protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "zdeck_srv";
    this.entitySetName_SAP = "TransferHeaderSet";
  }

  saveTransfer(t:TransferHeader){
    let t_ = cloneObject(t);

    if(t_.HULink)
      t_.HULink.forEach((hu:HUDeck)=>{
        for(let key in hu){
          if(hu.hasOwnProperty(key)){
            if(hu[key] == "")
              hu[key] = " ";
          }
        }
        return hu;
      });
    if(t_.RowLink)
      t_.RowLink.forEach((tr:TransferRow)=>{
        delete tr['packingInstructions'];
      });

    return this.saveItem(t_,true);
  }

}
