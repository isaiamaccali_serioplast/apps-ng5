import {Injectable} from "@angular/core";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {cloneObject} from "../../../shared/utils";
import {SupplierBatch} from "../../_common/services/supplier-batch.service";


declare let sap: any;

export class PurchaseOrder {
  Order:string;
  SupplierName:string;
  Supplier:string;
  Plant:string;
  InbDel:string;
  POItemLink: PurchaseOrderItem[];

  getId(){
    return "'"+this.Order+"'";
  }

  printer?:string;
  copies?:number;
}

export class PurchaseOrderItem {
  Item:string;
  Order:string;
  Quantity:any;
  Uom:string;
  Material:string;
  Description:string; //material description
  Palletization?:string;
  DefQty?:number;
  Warehouse:string;
  SuppBatchLink?:SupplierBatch[];
  Plant:string;

  getId?(){
    return "Item='"+this.Item+"',Order='"+this.Order+"'";
  }

  enable_add?:boolean;
  bToAdd?:SupplierBatch;
  selected:boolean = false;
  originalQuantity:number;
}





@Injectable()
export class PurchaseOrderService extends GenericSAPService {

  constructor(protected loggingService: LoggingService, protected sapService: SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "zdeck_srv";
    this.entitySetName_SAP = "PurchaseOrderSet";
  }

  getPurchaseOrders(query) {
    let params = {};
    if (query) {
      let filters = [];
      if (query.Plant != null) {
        let f = new sap.ui.model.Filter({
          path: 'Plant',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Plant
        });
        filters.push(f);
      }
      if (query.Order != null) {
        let f = new sap.ui.model.Filter({
          path: 'Order',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Order
        });
        filters.push(f);
      }
      params['filters'] = filters;
    }
    params['urlParameters'] = {$expand: "POItemLink"};
    return this.getList(params);
  }


  getPurchaseOrder(id) {
    let params = {};
    params['urlParameters'] = {$expand: "POItemLink"};
    return this.getItem(id, params);
  }

  savePurchaseOrder(po: PurchaseOrder) {
    let p:PurchaseOrder = cloneObject(po);

    if(p.printer)
      delete p.printer;
    if(p.copies)
      delete p.copies;

    for(let key in p){
      if(p.hasOwnProperty(key)){
        if(p[key] == "")
          p[key] = " ";
      }
    }
    p.POItemLink.forEach((poi)=>{
      for(let key in poi){
        if(poi.hasOwnProperty(key)){
          if(poi[key] === "")
            poi[key] = " ";
        }
      }
      if (poi.enable_add != null)
        delete poi.enable_add;
      if (poi.selected != null)
        delete poi.selected;
      if (poi.bToAdd != null)
        delete poi.bToAdd;
      if(poi.originalQuantity)
        delete poi.originalQuantity;

      poi.Quantity = poi.Quantity.toExponential();

      if(poi.SuppBatchLink && poi.SuppBatchLink.length>0){
        poi.SuppBatchLink.forEach((sb)=>{
          for(let key in sb){
            if(sb.hasOwnProperty(key)){
              if(sb[key] == ""){
                if(key!='Copies'){
                  sb[key] = " ";
                }
                else
                  sb[key] = 0;
              }

            }
          }
          delete sb.has_printed;
        })
      }
      else {
        if(poi.SuppBatchLink)
          delete poi.SuppBatchLink;
      }

    });

    return this.saveItem(p, true);
  }
}
