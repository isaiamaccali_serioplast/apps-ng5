import {Injectable} from "@angular/core";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {cloneObject} from "../../../shared/utils";
import {isNullOrUndefined} from "util";
import {HUDeck} from "./hu-deck.service";

declare let sap: any;

export class DeliveryHeader {
  Type:string;
  Delivery:string;
  Partner?:string;
  PartnerName?:string;
  City?:string;
  PostalCode?:string;
  Region?:string;
  Street?:string;
  Country?:string;
  Plant?:string;
  CreationDate:string;
  CreationHour:string;
  Status:string;
  Plate:string;
  DelHuLink?:HUDeck[];
  DelItemLink?:DeliveryItem[];

  isClosed() {
    return this.Status != ' ' && this.Status != 'A';
  }

  getId(){
    return "'"+this.Delivery+"'";
  }

  label:string;
  printer:string;
  copies:number;
}

export class DeliveryItem {
  Plant?:string;
  Palletization?:string;
  Uom?:string;
  Quantity?:string;
  Description?:string;
  Material?:string;
  Item?:string;
  Delivery?:string;
  DelSubItmLink:DeliverySubItem[];


  batch:string;
  hus:HUDeck[];
  enable_add:boolean;
  huToAdd:HUDeck;
  palletizationDefQty:any;

}

export class DeliverySubItem {
  Delivery?:string;
  Item?:string;
  PosSup:string;
  Material?:string;
  Description?:string;
  Quantity?:string;
  Uom?:string;
  Palletization?:string;
  Plant?:string;
  Batch:string;
}


@Injectable()
export class DeliveriesService extends GenericSAPService{

  constructor(protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "zdeck_srv";
    this.entitySetName_SAP = "DeliveryHeaderSet";
  }

  getOpenDeliveries(query){
    let params = {};
    if (query) {
      let filters = [];
      if (query.Plant != null) {
        let f = new sap.ui.model.Filter({
          path: 'Plant',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Plant
        });
        filters.push(f);
      }
      if (query['from'] && query ['to']) {
        let dateFilter = new sap.ui.model.Filter({
          path: "CreationDate",
          operator: sap.ui.model.FilterOperator.BT,
          value1: query['from'],
          value2: query ['to']
        });
        filters.push(dateFilter);
      }
      params['filters'] = filters;
    }

    params['sorters'] = [
      new sap.ui.model.Sorter("CreationDate",true),
      new sap.ui.model.Sorter("CreationHour",true)
    ];

    params['urlParameters'] = {$expand: "DelItemLink,DelHuLink,DelItemLink/DelSubItmLink"};
    return this.getList(params);
  }

  getDelivery(id:string){
    let params = {};
    params['urlParameters'] = {$expand: "DelItemLink,DelHuLink,DelItemLink/DelSubItmLink"};
    return this.getItem(id+"",params);
  }

  saveDelivery(del:DeliveryHeader, forceCreate=true){
    let d_:DeliveryHeader = cloneObject(del);
    if(d_.printer)
      delete d_.printer;
    if(d_.label)
      delete d_.label;
    if(!isNullOrUndefined(d_.copies))
      delete d_.copies;
    for(let key in d_){
      if(d_.hasOwnProperty(key)){
        if(d_[key] == "")
          d_[key] = " ";
      }
    }

    if(d_.DelItemLink){
      d_.DelItemLink = d_.DelItemLink.map((di:DeliveryItem)=>{
        if(di.batch)
          delete di.batch;
        if(di.enable_add!=null)
          delete di.enable_add;
        if(di.huToAdd)
          delete di.huToAdd;
        if(di.hus)
          delete di.hus;
        if(di.palletizationDefQty)
          delete di.palletizationDefQty;
        if(di.DelSubItmLink && di.DelSubItmLink['results']){
          if(di.DelSubItmLink['results'].length>0){
            di.DelSubItmLink = di.DelSubItmLink['results'];
          }
          else {
            delete di.DelSubItmLink;
          }
        }
        if(di.DelSubItmLink===[]){
          delete di.DelSubItmLink;
        }
        return di;
      });
    }

    if(d_.DelHuLink){
      d_.DelHuLink = d_.DelHuLink.map((h:HUDeck)=>{
        for(let key in h){
          if(h.hasOwnProperty(key)){
            if(h[key] == "")
              h[key] = " ";
          }
        }
        if(h.repeats)
          delete h.repeats;
        return h;
      });
    }

    return this.saveItem(d_, forceCreate);
  }

}
