import {Injectable} from "@angular/core";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {cloneObject} from "../../../shared/utils";

declare let sap: any;

export class HUDeck {
  SSCC?:string;
  TransferOrder?:string;
  TransferItem?:string;
  PackagingMaterial?:string;
  Material?:string;
  PackedQty?:any;
  Uom?:string;
  Batch?:string;
  SuppBatch:string;
  Palletization?:string;
  Warehouse?:string;
  Plant?:string;
  DeliveryNumber?:string;
  DeliveryItem?:string;
  Return:string;

  getId(){
    return "'"+this.SSCC+"'";
  }

  repeats:number = 1;
  has_printed:boolean;
}


@Injectable()
export class HUDeckService extends GenericSAPService{

  constructor(protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "zdeck_srv";
    this.entitySetName_SAP = "HUSet";
  }

  getHUReturns(query){
    let params = {};
    if (query) {
      let filters = [];
      if (query.Plant != null) {
        let f = new sap.ui.model.Filter({
          path: 'Plant',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Plant
        });
        filters.push(f);
      }
      if (query.Return != null) {
        let f = new sap.ui.model.Filter({
          path: 'Return',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Return
        });
        filters.push(f);
      }
      params['filters'] = filters;
    }
    return this.getList(params);
  }

  saveHU(hu:HUDeck){
    return this.saveItem(hu, true);
  }

  deleteHU(hu:HUDeck){
    return this.deleteItem(hu);
  }

  clearItem(hu:HUDeck){
    let hu_:HUDeck = cloneObject(hu);
    delete hu_.repeats;
    delete hu_.has_printed;
    return hu_;
  }
}
