import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {PackagesIndexComponent} from "./components/packages-index/packages-index.component";
import {DeckIndexComponent} from "./components/deck-index.component";

// Route Configuration
export const deckRoutes: Routes = [
  {
    path: '',
    component: DeckIndexComponent,
    canActivate: [AuthGuard]
  }
];

