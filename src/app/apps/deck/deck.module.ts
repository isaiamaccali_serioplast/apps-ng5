import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {deckRoutes} from "./deck.routes";
import {DeckIndexModule} from "./components/deck-index.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(deckRoutes),DeckIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []  // services
})
export class DeckModule {}
