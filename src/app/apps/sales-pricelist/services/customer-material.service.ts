import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {Observable} from "rxjs";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {cloneObject} from "../../../shared/utils";

export class CustMat {
  SalesOrganization:string;
  Customer:string;
  Material:string;
  MaterialDescription:string;
  CustomerMaterial:string;
  CustomerMatDescript:string;

  getId(){
    return "Material='"+this.Material+"',SalesOrganization='"+this.SalesOrganization+"',Customer='"+this.Customer+"'";
  }
}

declare let sap: any;

@Injectable()
export class CustomerMaterialService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "CustMatSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getCustomerMaterials(query: any) {

    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let filters = [];

        if(query['SalesOrganization']){
          let filter = new sap.ui.model.Filter({
            path: "SalesOrganization",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['SalesOrganization']
          });
          filters.push(filter);
        }

        if(query['Customer']){
          let filter = new sap.ui.model.Filter({
            path: "Customer",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Customer']
          });
          filters.push(filter);
        }
        else {
          if(query['Customers']){
            let customerFilters = query['Customers'].map((cCode)=>{
              let filter = new sap.ui.model.Filter({
                path: "Customer",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: cCode
              });
              return filter;
            });
            let filter = new sap.ui.model.Filter({
              filters:customerFilters,
              and:false
            });
            filters.push(filter);
          }
        }

        if(query['MaterialFilter']){
          let filter1 =  new sap.ui.model.Filter({
            path: "Material",
            operator: sap.ui.model.FilterOperator.Contains,
            value1: query['MaterialFilter']
          });
          let filter2 = new sap.ui.model.Filter({
            path: "MaterialDescription",
            operator: sap.ui.model.FilterOperator.Contains,
            value1: query['MaterialFilter']
          });
          let filter = new sap.ui.model.Filter({
            filters: [filter1, filter2],
            and: false
          });
          filters.push(filter);
        }

        if(query['Material']){
          let filter =  new sap.ui.model.Filter({
            path: "Material",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Material']
          });
          filters.push(filter);
        }

        let params = {};
        params['filters'] = filters;

        params = this.checkPagination(params, query);
        this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      })
    })
  }

  saveCustomerMaterial(i:CustMat){
    return this.saveItem(this.clearItem(i));
  }

  deleteCustomerMaterial(i:CustMat){
    return this.deleteItem(i);
  }

  restoreCustomerMaterial(i:CustMat){
    return this.restoreItem(i);
  }

  getCustomerMaterial(id: any) {
    return this.getItem(id);
  }

  clearItem(i:CustMat){
    let i_:CustMat = cloneObject(i);
    delete i_['selected'];
    return i_;
  }


}


