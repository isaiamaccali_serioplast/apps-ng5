import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {cloneObject} from "../../../shared/utils";
import {Observable} from "rxjs";
import {MaterialSAP} from "../../_common/services/materials.service";
import {ConditionName} from "./condition-name.service";

export class WIPPeriod {
  SalesOrganization: string;
  Customer: string;
  Material: string;
  ShipToParty: string;
  PackInstruction: string;
  Plant: string;
  ValidTo: string;
  ValidFrom: string;
  WIPConditionLink:WIPCondition[];

  basePriceUnit:number;
  materialFull:MaterialSAP;
  selected = false;

  getId(){
    return "Material='"+this.Material+"',SalesOrganization='"+this.SalesOrganization+"',Customer='"+this.Customer+"',ShipToParty='"+this.ShipToParty+"',Plant='"+this.Plant+"',PackInstruction='"+this.PackInstruction+"'";
  }
}

export class WIPCondition {
  Condition: string;
  Description:string;
  PriceUnit: any;
  Uom:string;
  Value: any;
  Currency: string;
  OriginalValue:any;
  OldValue:any;

  oldValueSet = false;
  valueBackup:number;
  calculatedValue:number;
  calculatedOldValue:number;
  conditionName:ConditionName;
  isNew = false;
}

declare let sap: any;

@Injectable()
export class WIPPeriodService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "WIPPeriodSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getWIPPeriods(query: any) {

    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let filters = [];

        if(query['SalesOrganization']){
          let filter = new sap.ui.model.Filter({
            path: "SalesOrganization",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['SalesOrganization']
          });
          filters.push(filter);
        }

        if(query['Customer']){
          let filter = new sap.ui.model.Filter({
            path: "Customer",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Customer']
          });
          filters.push(filter);
        }
        else {
          if(query['Customers']){
            let customerFilters = query['Customers'].map((cCode)=>{
              return new sap.ui.model.Filter({
                path: "Customer",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: cCode
              });
            });
            let filter = new sap.ui.model.Filter({
              filters:customerFilters,
              and:false
            });
            filters.push(filter);
          }
        }

        if(query['ShipToParty']){
          let filter = new sap.ui.model.Filter({
            path: "ShipToParty",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['ShipToParty']
          });
          filters.push(filter);
        }
        else {
          if(query['ShipToParties']){
            let customerFilters = query['ShipToParties'].map((cCode)=>{
              return new sap.ui.model.Filter({
                path: "ShipToParty",
                operator: sap.ui.model.FilterOperator.EQ,
                value1: cCode
              });
            });
            let filter = new sap.ui.model.Filter({
              filters:customerFilters,
              and:false
            });
            filters.push(filter);
          }
        }

        if(query['Plant']){
          let filter = new sap.ui.model.Filter({
            path: "Plant",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(filter);
        }

        if(query['PackInstruction']){
          let filter = new sap.ui.model.Filter({
            path: "PackInstruction",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['PackInstruction']
          });
          filters.push(filter);
        }

        if(query['Material']){
          let filter =  new sap.ui.model.Filter({
            path: "Material",
            operator: sap.ui.model.FilterOperator.Contains,
            value1: query['Material']
          });
          filters.push(filter);
        }

        let params = {};
        params['filters'] = filters;
        params['urlParameters'] = {$expand: 'WIPConditionLink'};
        //params = this.checkPagination(params, query);
        this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      })
    });
  }

  saveWIPPeriod(i:WIPPeriod){
    let i_ = new WIPPeriod();
    Object.assign(i_, i);
    return this.saveItem(this.clearItem(i_),true);
  }

  deleteWIPPeriod(i:WIPPeriod){
    let i_ = new WIPPeriod();
    Object.assign(i_, i);
    return this.deleteItem(i_);
  }

  getWIPPeriod(id: string) {
    return this.getItem(id);
  }

  clearItem(i:WIPPeriod){
    let i_:WIPPeriod = cloneObject(i);
    i_.ValidFrom = i_.ValidFrom.split("-").join("");
    i_.ValidTo = i_.ValidTo.split("-").join("");

    for(let key in i_){
      if(i_.hasOwnProperty(key)&& (typeof i_[key]=='string')){
        if(i_[key] == "")
          i_[key] = " ";
      }
    }

    i_.WIPConditionLink.forEach((wipC)=>{
      for(let key in wipC){
        if(wipC.hasOwnProperty(key)&& (typeof wipC[key]=='string')){
          if(wipC[key] == "")
            wipC[key] = " ";
        }
      }
      if(wipC.PriceUnit)
        wipC.PriceUnit = Number(wipC.PriceUnit).toExponential();
      else
        wipC.PriceUnit = " ";
      wipC.Value = Number(wipC.Value).toExponential();
      wipC.OriginalValue = Number(wipC.OriginalValue).toExponential();
      wipC.OldValue = Number(wipC.OldValue).toExponential();
      delete wipC.isNew;
      delete wipC.conditionName;
      delete wipC.calculatedValue;
      delete wipC.calculatedOldValue;
      delete wipC.oldValueSet;
      delete wipC.valueBackup;
    });

    delete i_.basePriceUnit;
    delete i_.materialFull;
    delete i_.selected;

    return i_;
  }
}



