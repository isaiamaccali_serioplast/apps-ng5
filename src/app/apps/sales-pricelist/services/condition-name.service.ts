import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {Observable} from "rxjs";

declare let sap: any;

export class ConditionName {
  Description:string;
  Condition:string;
  Currency:string;
  SalesOrganization:string;
  Helper:string;
  Formula:string;
  Sort:string;
  Rule:string;
  NoEdit:boolean;
}

@Injectable()
export class ConditionNameService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "ConditionNameSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getConditionNames(query: any) {
    return Observable.create((observer)=> {
      this.sapService.ready().subscribe(() => {
        let filters = [];

        if (query['SalesOrganization']) {
          let filter = new sap.ui.model.Filter({
            path: "SalesOrganization",
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['SalesOrganization']
          });
          filters.push(filter);
        }

        let params = {};
        params['filters'] = filters;

        this.getList(params).subscribe((resp) => {
          observer.next(resp);
          observer.complete();
        });
      });
    });
  }
}


