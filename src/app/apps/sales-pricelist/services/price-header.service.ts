import {Injectable} from "@angular/core";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {cloneObject} from "../../../shared/utils";

declare let sap: any;

export class PriceHeader {
  SalesOrganization?: string;
  Customer?: string;
  Material?: string;
  ShipToParty?: string;
  PackInstruction?: string;
  Plant?: string;
  CustCurrency?: string;
  ShipCurrency?: string;
  PeriodLink?:PricePeriod[];

  savedInProgress?:boolean;

  getId(){
    return "Material='"+this.Material+"',SalesOrganization='"+this.SalesOrganization+"',Customer='"+this.Customer+"',ShipToParty='"+this.ShipToParty+"',Plant='"+this.Plant+"',PackInstruction='"+(this.PackInstruction?this.PackInstruction:" ")+"'";
  }
}

export class PricePeriod {
  ValidTo: string;
  ValidFrom: string;
  ConditionLink:PriceCondition[];
}

export class PriceCondition {
  Condition: string;
  Plant: string;
  PriceUnit: string;
  Value: any;
  Currency: string;
  Uom:string;

  ConvertedValue:any;
  ConversionCurrency:string;
}

@Injectable()
export class PriceHeaderService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "PriceHeaderSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getPriceHeaders(query: any) {

      return Observable.create((observer)=>{
        this.sapService.ready().subscribe(()=>{
          let filters = [];

          if(query['SalesOrganization']){
            let filter = new sap.ui.model.Filter({
              path: "SalesOrganization",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['SalesOrganization']
            });
            filters.push(filter);
          }

          if(query['Customer']){
            let filter = new sap.ui.model.Filter({
              path: "Customer",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Customer']
            });
            filters.push(filter);
          }
          else {
            if(query['Customers']){
              let customerFilters = query['Customers'].map((cCode)=>{
                return new sap.ui.model.Filter({
                  path: "Customer",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: cCode
                });
              });
              let filter = new sap.ui.model.Filter({
                filters:customerFilters,
                and:false
              });
              filters.push(filter);
            }
          }

          if(query['Material']){
            let filter =  new sap.ui.model.Filter({
              path: "Material",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Material']
            });
            filters.push(filter);
          }

          if(query['PackInstruction']){
            let filter =  new sap.ui.model.Filter({
              path: "PackInstruction",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['PackInstruction']
            });
            filters.push(filter);
          }

          if(query['ShipToParty']){
            let filter =  new sap.ui.model.Filter({
              path: "ShipToParty",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['ShipToParty']
            });
            filters.push(filter);
          }
          else {
            if(query['ShipToParties']){
              let customerFilters = query['ShipToParties'].map((cCode)=>{
                return new sap.ui.model.Filter({
                  path: "ShipToParty",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: cCode
                });
              });
              let filter = new sap.ui.model.Filter({
                filters:customerFilters,
                and:false
              });
              filters.push(filter);
            }
          }

          if(query['Plant']){
            let filter =  new sap.ui.model.Filter({
              path: "Plant",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Plant']
            });
            filters.push(filter);
          }

          let params = {};
          params['filters'] = filters;
          params['urlParameters'] = {$expand: 'PeriodLink,PeriodLink/ConditionLink'};

          params = this.checkPagination(params, query);
          this.getList(params).subscribe((resp)=>{
            observer.next(resp);
            observer.complete();
          });
        })
      })
  }

  savePriceHeader(i:PriceHeader){
    return Observable.create((observer)=>{
      let i_:PriceHeader = cloneObject(i);

      for(let key in i_){
        if(i_.hasOwnProperty(key)&& (typeof i_[key]=='string')){
          if(i_[key] == "")
            i_[key] = " ";
        }
      }

      i_.PeriodLink.forEach((pl)=>{
        pl.ConditionLink.forEach((pc)=>{
          for(let key in pc){
            if(pc.hasOwnProperty(key)&& (typeof pc[key]=='string')){
              if(pc[key] == "")
                pc[key] = " ";
            }
          }
          if(pc.PriceUnit)
            pc.PriceUnit = Number(pc.PriceUnit).toExponential();
          else
            pc.PriceUnit = " ";
          pc.Value = Number(pc.Value).toExponential();
        });
      });

      this.saveItem(i_, true).subscribe((resp)=>{
        observer.next(resp);
        observer.complete();
      })
    })
  }
}
