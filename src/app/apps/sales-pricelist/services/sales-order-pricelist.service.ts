import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";

export class SaleOrderPricelist {
  SalesOrganization: string;
  Customer: string;
  Material: string;
  ShipToParty: string;
  PackInstruction: string;
  Plant: string;
  ReferenceDate:string;

  Quantity: any;
  Value:any;

  getId = () => {
    return "SalesOrganization='"+this.SalesOrganization+"',Customer='"+this.Customer+"',Material='"+this.Material+"',ShipToParty='"+this.ShipToParty+"',PackInstruction='"+(this.PackInstruction?this.PackInstruction:' ')+"',Plant='"+(this.Plant?this.Plant:" ")+"',Quantity='"+this.Quantity+"',ReferenceDate='"+this.ReferenceDate+"'";
  }
}

@Injectable()
export class SaleOrderPricelistService extends GenericSAPService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "SaleOrderSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getSaleOrder(id: any) {
    return this.getItem(id);
  }

}



