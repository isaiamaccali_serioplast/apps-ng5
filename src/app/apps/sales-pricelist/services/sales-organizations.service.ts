import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "../../_common/services/_generic_sap_service";

export class SalesOrganizationSAP {
  SalesOrganizationCode:string;
  Description:string;
  Currency:string;
  CustomerLink: {
    results:CustomerSalesOrg[]
  };

  getId(){
    return "'"+this.SalesOrganizationCode+"'";
  }
}

export class CustomerSalesOrg {
  Customer:string;
  SalesOrganization:string;
  Name:string;
  Type:string;
  Currency:string;
}

@Injectable()
export class SalesOrganizationSAPService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "SalesOrganizationSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getSalesOrganizations(query: any) {
    let params = {};
    params['urlParameters'] = {$expand: 'CustomerLink'};
    return this.getList(params);
  }

  saveSalesOrganization(i:SalesOrganizationSAP){
    return this.saveItem(i);
  }

  deleteSalesOrganization(i:SalesOrganizationSAP){
    return this.deleteItem(i);
  }

  getSalesOrganization(id: any) {
    return this.getItem(id,{urlParameters:{$expand: 'CustomerLink'}});
  }
}


