import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {SalesPricelistRoutingModule} from "./sales-pricelist.routes";
import {SalesPricelistIndexModule} from "./components/sales-pricelist-index/sales-pricelist-index.module";
import {SalesOrganizationSAPService} from "./services/sales-organizations.service";
import {CustomerMaterialService} from "./services/customer-material.service";
import {PriceHeaderService} from "./services/price-header.service";
import {WIPPeriodService} from "./services/wip-period.service";
import {QuerySharerService} from "./services/query-sharer.service";
import {ConditionNameService} from "./services/condition-name.service";
import {SaleOrderPricelistService} from "./services/sales-order-pricelist.service";
import {Angular2MaterialModule} from "../../shared/material/angular2-material.module";

@NgModule({
  imports: [SharedModule,SalesPricelistRoutingModule,SalesPricelistIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [SalesOrganizationSAPService,CustomerMaterialService,PriceHeaderService,WIPPeriodService,QuerySharerService,ConditionNameService,SaleOrderPricelistService]                    // services
})
export class SalesPricelistModule {}

