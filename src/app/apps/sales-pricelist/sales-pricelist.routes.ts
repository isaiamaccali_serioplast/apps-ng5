import {Routes, RouterModule} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {NgModule} from "@angular/core";
import {SalesPricelistIndexComponent} from "./components/sales-pricelist-index/sales-pricelist-index.component";
import {SalesPricelistModule} from "./sales-pricelist.module";

// Route Configuration
export const SalesPricelistRoutes: Routes = [
  {
    path: '',
    component: SalesPricelistIndexComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':section',
    component: SalesPricelistIndexComponent,
    canActivate: [AuthGuard]
  }
];

/**
 * Root route, uses lazy loading to improve performance
 */
export const SalesPricelistRoutesRoot: Routes = [
  {
    path: 'sales-pricelist',
    loadChildren: ()=>SalesPricelistModule,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(SalesPricelistRoutes)],
  exports: [RouterModule],
})
export class SalesPricelistRoutingModule {
}
