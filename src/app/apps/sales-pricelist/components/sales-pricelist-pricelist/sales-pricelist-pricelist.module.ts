import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SalesPricelistPricelistComponent} from "./sales-pricelist-pricelist.component";
import {SalesPricelistPricelistListComponent} from "./sales-pricelist-pricelist-list/sales-pricelist-pricelist-list.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {SalesPricelistPriceListViewComponent} from "./sales-pricelist-pricelist-view/sales-pricelist-pricelist-view.component";
import {SalesPricelistFiltersModule} from "../sales-pricelist-filters/sales-pricelist-filters.module";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule,SalesPricelistFiltersModule],       // module dependencies
  declarations: [SalesPricelistPricelistComponent,SalesPricelistPricelistListComponent,SalesPricelistPriceListViewComponent],   // components and directives
  exports:[SalesPricelistPricelistComponent,SalesPricelistPricelistListComponent,SalesPricelistPriceListViewComponent],
  providers: []                    // services
})
export class SalesPricelistPricelistModule { }


