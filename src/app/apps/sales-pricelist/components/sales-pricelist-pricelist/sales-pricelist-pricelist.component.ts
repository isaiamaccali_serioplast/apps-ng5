import {Component, EventEmitter, Output} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {PriceHeader} from "../../services/price-header.service";
import {WIPPeriod} from "../../services/wip-period.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-pricelist',
  templateUrl: 'sales-pricelist-pricelist.component.html',
  styleUrls: ['sales-pricelist-pricelist.component.css']
})

export class SalesPricelistPricelistComponent extends TranslatableComponent{

  @Output('editWIPEvent') editWIPEvent = new EventEmitter();
  editMode = false;
  createMode = false;
  phToView:PriceHeader;
  loading = false;

  constructor(protected translationService:TranslationService) {
    super(translationService);
  }

  createPH(ph:PriceHeader){
    this.phToView = ph;
    this.createMode = true;
    this.editMode = true;
  }

  viewPH(ph:PriceHeader){
    this.phToView = ph;
    this.createMode = false;
    this.editMode = true;
  }

  back(){
    this.editMode = false;
  }

  editWIP(wip:WIPPeriod){
    this.editWIPEvent.emit(wip);
  }
}
