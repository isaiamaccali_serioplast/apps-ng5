import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {
  arrayIntersect, buildErrorMessage, cloneObject, formatNumber, getUrlDate,
  sapDateToUrlDate
} from "../../../../../shared/utils";
import {
  PurchaseOrganization,
  PurchaseOrganizationsService,
  PurchaseOrganizationSupplier
} from "../../../services/purchase-organization.service";
import {PlantSAP, PlantSAPService} from "../../../../_common/services/plant-flow-service";
import {
  HeaderConf,
  TableRowAction,
  TableRowActions,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSAP, MaterialsSAPService} from "../../../../_common/services/materials.service";
import {SapDatePipe} from "../../../../../shared/pipes/sap-date.pipe";
import {PurchasingGroup, PurchasingGroupService} from "../../../services/purchase-group.service";
import {PurchaseCondition, PurchaseConditionsService} from "../../../services/purchase-conditions.service";
import {PriceCondition, PriceHeader, PricePeriod} from "../../../services/price-header.service";
import {
  CustomerSalesOrg,
  SalesOrganizationSAP,
  SalesOrganizationSAPService
} from "../../../services/sales-organizations.service";
import {ConditionName, ConditionNameService} from "../../../services/condition-name.service";
import {CurrencyService} from "../../../../_common/services/currency.service";
import {ValueConversion, ValueConversionService} from "../../../../_common/services/value-conversion.service";
import {SaleOrderPricelist, SaleOrderPricelistService} from "../../../services/sales-order-pricelist.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {WIPPeriodService} from "../../../services/wip-period.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-pricelist-view',
  templateUrl: 'sales-pricelist-pricelist-view.component.html',
  styleUrls:['sales-pricelist-pricelist-view.component.css']
})

export class SalesPricelistPriceListViewComponent extends TranslatableComponent implements OnInit {

  @Output('backEvent') backEvent = new EventEmitter();
  @Output('editWip') editWip = new EventEmitter();
  @ViewChild('periodsTable') periodsTable:TableWithPagesComponent;
  @ViewChild('saleOrderSimulationDialog') saleOrderSimulationDialog:DialogComponent;

  materialId:string;
  ready = false;
  @Input('phToView') phToView:PriceHeader;
  organization:SalesOrganizationSAP = new SalesOrganizationSAP();
  customer:CustomerSalesOrg = new CustomerSalesOrg();
  shipToParty:CustomerSalesOrg = new CustomerSalesOrg();

  @Input('createMode') createMode = false;
  availablePlants:string[] = [];
  organizations:SalesOrganizationSAP[] = [];
  customers:CustomerSalesOrg[] = [];
  plants:PlantSAP[] = [];

  organizationsToShow:SalesOrganizationSAP[] = [];
  periodToEdit:PricePeriod;
  periodToView:PricePeriod;

  headerConfPeriods:HeaderConf[] = [
    {
      valueFunction:(item:PricePeriod) => {
        let date = item.ValidFrom;
        let sapDatePipe = new SapDatePipe();
        return sapDatePipe.transform(date);
      },
      label:'Valid from'
    },
    {
      valueFunction:(item:PricePeriod) =>{
        let date = item.ValidTo;
        let sapDatePipe = new SapDatePipe();
        return sapDatePipe.transform(date);
      },
      label:'Valid to'
    }
  ];

  availableActionsPeriods = [];

  today = getUrlDate(new Date());

  materialSAP:MaterialSAP = new MaterialSAP();

  conditions:ConditionName[] = [];

  headerConfConditions:HeaderConf[] = [];
  currencies:any[] = [];
  currencyToConvertTo:string;

  convertedColumn:HeaderConf = new HeaderConf();

  constructor(protected translationService: TranslationService, private organizationsService:SalesOrganizationSAPService,
              private plantSAPService:PlantSAPService, private materialsSAPService:MaterialsSAPService,
              private conditionNameService:ConditionNameService, private currencyService:CurrencyService,
              private valueConversionService:ValueConversionService, private saleOrderPricelistService:SaleOrderPricelistService,
              private loadingService:LoadingService, private messageService:MessageService,
              private wipPeriodService:WIPPeriodService
  ) {
    super(translationService);
  }

  ngOnInit() {

    this.conditionNameService.getConditionNames({}).subscribe((resp)=>{
      if(resp.status==200){
        this.conditions = resp.response.results;
      }
    });

    this.currencyService.getCurrencies({}).subscribe((resp)=>{
      this.currencies = resp;
    });

    let viewPeriod:TableRowAction = cloneObject(TableRowActions.VIEW);
    viewPeriod.click = (p:PricePeriod)=>{
      this.viewPeriod(p);
    };
    this.availableActionsPeriods.push(viewPeriod);

    this.headerConfConditions = [
      {
        valueFunction: (item: PriceCondition) => {
          let match = this.conditions.filter((c)=>c.Condition==item.Condition);
          if(match.length>0)
            return item.Condition + " - " + match[0].Description;
          else
            return item.Condition;
        },
        label: 'Condition'
      },
      {
        valueFunction: (item: PriceCondition) => {
          if(item.Currency!="%"){
            return parseFloat(item.PriceUnit) + " " + this.materialSAP.Uom;
          }
          else {
            return "-";
          }

        },
        label: 'Price Unit'
      },
      {
        valueFunction: (item: PriceCondition) => {
          return parseFloat(item.Value) + " " + item.Currency;
        },
        label: 'Value'
      }
    ];

    this.convertedColumn = {
      valueFunction: (item: PriceCondition) => {
        if(item.Currency!="%"){
          if(item.ConvertedValue!=null)
            return '<span class="grey-text">'+parseFloat(item.ConvertedValue) + " " + item.ConversionCurrency+'</span>';
          else
            return "-";
        }
        else {
          return "-";
        }

      },
      label: 'Converted Value'
    };

    this.organizationsService.getSalesOrganization("'"+this.phToView.SalesOrganization+"'").subscribe((resp)=>{
      if(resp.status==200){
        this.organization = resp.response;
        this.customer = this.organization.CustomerLink.results.filter((c)=>c.Customer==this.phToView.Customer)[0];
        this.shipToParty = this.organization.CustomerLink.results.filter((c)=>c.Customer==this.phToView.ShipToParty)[0];
      }
      else {

      }
    });

    this.materialsSAPService.getMaterial("'"+this.phToView.Material+"'").subscribe((resp)=> {
      if (resp.status == 200) {
        this.materialSAP = resp.response;
      }
    });

    this.phToView.PeriodLink.forEach((pl)=>{
      if(this.isPeriodActive(pl)){
        this.viewPeriod(pl);
      }
    });

    let query = {
      Organization: this.phToView.SalesOrganization,
      Plant: this.phToView.Plant,
      Customer: this.phToView.Customer,
      Material: this.phToView.Material,
      ShipToParty: this.phToView.ShipToParty,
      PackInstruction: this.phToView.PackInstruction
    };

    this.wipPeriodService.getWIPPeriods(query).subscribe((resp) => {
      if(resp.status == 200 && resp.response.results.length>0){
        let wipP = resp.response.results[0];
        wipP.WIPConditionLink = wipP.WIPConditionLink['results'];

        wipP.basePriceUnit = parseFloat(wipP.WIPConditionLink[0].PriceUnit);

        wipP.WIPConditionLink.forEach((c) => {
          c.PriceUnit = parseFloat(c.PriceUnit);
          c.Value = parseFloat(c.Value);
          c.conditionName =  this.conditions.filter((c_)=>c_.Condition==c.Condition)[0];
          if (c.Currency != "%")
            c.PriceUnit = wipP.basePriceUnit;
        });

        wipP.materialFull = new MaterialSAP();
        this.materialsSAPService.getMaterial("'"+wipP.Material+"'").subscribe((resp)=>{
          if(resp.status==200){
            wipP.materialFull = resp.response;
            this.phToView['wip'] = wipP;
          }
        });
      }
    });
  }

  back(){
    this.backEvent.emit();
  }

  parseFloat(n:number){
    return parseFloat(n+"");
  }

  checkOrganizations(index:number,organizationsToShow:SalesOrganizationSAP[],callback:any){
    if(this.organizations[index]){
      this.plantSAPService.getPlants({SalesOrganization:this.organizations[index].SalesOrganizationCode}).subscribe((resp)=>{
        if(resp.status==200){
          this.plants = [];
          let plants:PlantSAP[] = resp.response.results;
          let plantsCodes = plants.map((p)=>p.Code);
          if(arrayIntersect(plantsCodes,this.availablePlants).length>0){
            this.plants = plants;
            if(this.organizations[index].CustomerLink && this.organizations[index].CustomerLink.results.length>0)
              organizationsToShow.push(this.organizations[index]);
          }
          this.checkOrganizations(index+1,organizationsToShow,callback);
        }
        else {
          //Non ci sono plant collegati a questa organizzazione, skippo
          this.checkOrganizations(index+1,organizationsToShow,callback);
        }
      })
    }
    else {
      callback.call(this);
    }
  }

  highlightPeriod(p:PricePeriod){
    return this.isPeriodActive(p)?"active":"";
  }

  isPeriodActive(p:PricePeriod){
    let ValidFrom = p.ValidFrom;
    let ValidTo = p.ValidTo;
    let date = new Date();
    let sapDate = parseInt([date.getFullYear(),formatNumber(2,(date.getMonth()+1)),formatNumber(2,date.getDate())].join(""),10);
    let validFrom = parseInt(ValidFrom,10);
    let validTo = parseInt(ValidTo,10);
    return ((sapDate>=validFrom) && (sapDate<=validTo));
  }

  viewPeriod(p:PricePeriod){
    this.currencyToConvertTo = "";
    this.convertConditions();
    this.periodToView = cloneObject(p);
  }

  convertConditions(){
    if(this.currencyToConvertTo){
      if(this.headerConfConditions.indexOf(this.convertedColumn)==-1) {
        this.headerConfConditions.push(this.convertedColumn);
      }

      let count = this.periodToView.ConditionLink.filter((c)=>c.Currency!="%").length;
      let callback = ()=>{
        count--;
        if(count==0){
          this.loadingService.hide();
        }
      };
      this.loadingService.show();
      this.periodToView.ConditionLink.forEach((c)=>{
        if(c.Currency!="%"){
          let vc = new ValueConversion();
          vc.ActualValue = parseFloat(c.Value);
          vc.ActualCurrency = c.Currency;
          vc.ConversionCurrency = this.currencyToConvertTo;
          this.valueConversionService.getValueConversion(vc.getId()).subscribe((resp)=>{
            if(resp.status==200){
              let vc_:ValueConversion = resp.response;
              c.ConversionCurrency = vc_.ConversionCurrency;
              c.ConvertedValue = vc_.ConvertedValue;
            }
            callback();
          })
        }
      })
    } else {
      if(this.headerConfConditions.indexOf(this.convertedColumn)!=-1)
        this.headerConfConditions.splice(this.headerConfConditions.indexOf(this.convertedColumn),1);
    }
  }

  simulatedOrder = new SaleOrderPricelist();

  openSimulateOrder(){
    this.simulatedOrder = new SaleOrderPricelist();
    this.simulatedOrder.Customer = this.phToView.Customer;
    this.simulatedOrder.Material = this.phToView.Material;
    this.simulatedOrder.SalesOrganization = this.phToView.SalesOrganization;
    this.simulatedOrder.ShipToParty = this.phToView.ShipToParty;
    this.simulatedOrder.PackInstruction = this.phToView.PackInstruction;
    this.simulatedOrder.Plant = this.phToView.Plant;
    this.simulatedOrder.ReferenceDate = getUrlDate(new Date(sapDateToUrlDate(this.periodToView.ValidFrom)));
    this.saleOrderSimulationDialog.open();
  }

  simulateOrder(){
    this.loadingService.show();
    let so:SaleOrderPricelist = cloneObject(this.simulatedOrder);
    so.Value = 0;
    so.ReferenceDate = getUrlDate(new Date(so.ReferenceDate),"");
    this.saleOrderPricelistService.getSaleOrder(so.getId()).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        let so:SaleOrderPricelist = resp.response;
        this.simulatedOrder.Value = so.Value;
      }
      else {
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  goToWip(){
    this.editWip.emit(this.phToView['wip']);
  }
}
