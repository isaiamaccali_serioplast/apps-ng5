import {Component, EventEmitter, OnInit, Output, ViewChild} from "@angular/core";
import {HeaderConf, TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Material, MaterialsService} from "../../../../_common/services/materials.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {WIPCondition, WIPPeriod, WIPPeriodService} from "../../../services/wip-period.service";
import {PriceHeader, PriceHeaderService, PricePeriod} from "../../../services/price-header.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {buildErrorMessage, cloneObject, dateDiffInDays, getUrlDate, parseSerionetRespToData, sapDateToUrlDate} from "../../../../../shared/utils";
import {Observable} from "rxjs";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {EventsRow, TimeLineEvent} from "../../../../../shared/components/event-timeline/event-timeline.component";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {ConditionName, ConditionNameService} from "../../../services/condition-name.service";
import {SalesPriceListFiltersComponent} from "../../sales-pricelist-filters/sales-pricelist-filters.component";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-pricelist-list',
  templateUrl: 'sales-pricelist-pricelist-list.component.html',
  styleUrls: ['sales-pricelist-pricelist-list.component.css']
})

export class SalesPricelistPricelistListComponent extends TranslatableComponent implements OnInit {

  @Output('createEvent') createEvent = new EventEmitter();
  @Output('viewEvent') viewEvent = new EventEmitter();
  @ViewChild('table') table:TableWithPagesComponent;
  @ViewChild('clonePriceDialog') clonePriceDialog:DialogComponent;
  @ViewChild('periodsDialog') periodsDialog:DialogComponent;
  @ViewChild('filters') filters:SalesPriceListFiltersComponent;
  headerConf:HeaderConf[] = [];
  availableActions = [];
  ready = false;

  constructor(protected translationService: TranslationService, private wipPeriodService:WIPPeriodService,
              private priceHeadersService:PriceHeaderService, private loadingService:LoadingService,
              private messageService:MessageService, private conditionNamesService:ConditionNameService,
              private materialsService:MaterialsService, private sitesService:SitesService
  ) {
    super(translationService);
  }

  ngOnInit() {

    this.headerConf = [
      {
        label:'Sales Organization',
        columnId:'salesOrganization',
        valueFunction:(i:PriceHeader)=>{
          return i.SalesOrganization;
        }
      },
      {
        label:'Customer',
        columnId:'customer',
        valueFunction:(i:PriceHeader)=>{
          let cust = this.filters.salesOrganizations.filter((o)=>o.SalesOrganizationCode==i.SalesOrganization)[0].CustomerLink.results.filter((c)=>c.Customer==i.Customer)[0];
          if(cust)
            return cust.Customer+ " - " + cust.Name;
          else
            return i.Customer
        }
      },
      {
        label:'Material',
        columnId:'material',
        valueFunction:(i:PriceHeader)=>{
         return i.Material +  " - "+ i['mat_desc'];
        }
      },
      {
        label:'ShipToParty',
        columnId:'shipToParty',
        valueFunction:(i:PriceHeader)=>{
          let cust = this.filters.salesOrganizations.filter((o)=>o.SalesOrganizationCode==i.SalesOrganization)[0].CustomerLink.results.filter((c)=>c.Customer==i.ShipToParty)[0];
          if(cust)
            return cust.Customer+ " - " + cust.Name;
          else
            return i.Customer
        }
      },
      {
        label:'PackInstruction',
        columnId:'packInstr',
        valueKey:'PackInstruction'
      },
      {
        label:'Plant',
        columnId:'plant',
        valueFunction:(i:PriceHeader)=>{
          return i.Plant +  " - "+ i['site_desc'];
        }
      },
      {
        label:'WIP',
        columnId:'wip',
        valueFunction:(i:PriceHeader)=>{
          if(i['has_wip']){
            return '<img style="width:25px" src="/assets/img/constructions.png"/>';
          }
        }
      },
      {
        label:'Period status',
        columnId:'periodStatus',
        valueFunction:(i:PriceHeader)=>{
          return this.processPeriods(i);
        }
      }
    ];

    let actionView: TableRowAction = cloneObject(TableRowActions.VIEW);
    actionView.click = this.viewPriceHeader.bind(this);
    this.availableActions = [actionView];

  }

  processPeriods(ph:PriceHeader){
    let today = parseInt(getUrlDate(new Date(),""),10);
    let periods = ph.PeriodLink.sort((a:PricePeriod,b:PricePeriod)=>{
      return parseInt(b.ValidTo,10)-parseInt(a.ValidTo,10);
    });

    let currentPeriod:PricePeriod = null;
    let currentPeriodIndex = null;
    periods.forEach((p, index)=>{
      let start = parseInt(p.ValidFrom,10);
      let end = parseInt(p.ValidTo,10);
      if(start<=today && end>=today){
        currentPeriod = p;
        currentPeriodIndex = index;
      }
    });

    if(currentPeriod){
      //esiste un periodo attivo
      let end = parseInt(currentPeriod.ValidTo,10);
      let todayDate = new Date(sapDateToUrlDate(today+""));
      let endDate = new Date(sapDateToUrlDate(end+""));
      if(dateDiffInDays(todayDate,endDate)>30){
        //Il periodo attivo scade tra più di trenta giorni, tutto ok
        return '<span class="circle green">&nbsp;</span>';
      }
      else {
        if(currentPeriodIndex==0){
          //Il periodo attivo è il più recente e scade fra meno di trenta giorni, attenzione
          return '<span class="circle yellow">&nbsp;</span>';
        }
        else {
          //fra venti giorni
          let d = new Date(sapDateToUrlDate(today+""));
          d.setDate(d.getDate()+20);
          let futureDate = parseInt(getUrlDate(d,""),10);

          let matchingPeriod = null;
          periods.forEach((p)=>{
            let start = parseInt(p.ValidFrom,10);
            let end = parseInt(p.ValidTo,10);
            if(start<=futureDate && end>=futureDate){
              matchingPeriod = p;
            }
          });

          if(matchingPeriod){
            //Tra venti giorni c'è un periodo, tutto ok
            return '<span class="circle green">&nbsp;</span>';
          }
          else {
            //Tra venti giorni non c'è un periodo, attenzione
            return '<span class="circle yellow">&nbsp;</span>';
          }
        }
      }
    }
    else {
      //Non esiste un periodo attivo oggi
      //fra venti giorni
      let d = new Date(sapDateToUrlDate(today+""));
      d.setDate(d.getDate()+20);
      let futureDate = parseInt(getUrlDate(d,""),10);

      let matchingPeriod = null;
      periods.forEach((p)=>{
        let start = parseInt(p.ValidFrom,10);
        let end = parseInt(p.ValidTo,10);
        if(start<=futureDate && end>=futureDate){
          matchingPeriod = p;
        }
      });

      if(matchingPeriod){
        //Tra venti giorni c'è un periodo, new
        return '<span class="circle new">N</span>';
      }
      else {
        //Tra venti giorni non c'è un periodo, occhio
        return '<span class="circle red">&nbsp;</span>';
      }
    }
  }

  expandData(prices:PriceHeader[]) {
    return Observable.create((observer) => {

      let expandPH = (ps, index, callback) => {
        if (ps[index]) {
          let p = ps[index];

          p.PeriodLink = p.PeriodLink.results;
          p.PeriodLink.forEach((pl)=>{
            pl.ConditionLink = pl.ConditionLink.results;
          });

          let query = {
            Organization: p.Organization,
            Plant: p.Plant,
            Customer: p.Customer,
            Material: p.Material,
            ShipToParty: p.ShipToParty,
            PackInstruction: p.PackInstruction
          };

          let count = 3;
          let callback_ = () => {
            count--;
            if(count==0){
              expandPH(ps, index + 1, callback);
            }
          };

          this.wipPeriodService.getWIPPeriods(query).subscribe((resp) => {
            p['has_wip'] = (resp.status == 200)&& resp.response.results.length>0;
            callback_();
          });

          this.materialsService.getMaterials({code:p.Material}).subscribe((resp)=>{
            let mats:Material[] = parseSerionetRespToData(resp);
            if(mats[0])
              p['mat_desc'] = mats[0].description;
            callback_();
          });

          this.sitesService.getSites({code:p.Plant, is_active:true}).subscribe((resp)=>{
            let sites:Site[] = parseSerionetRespToData(resp);
            if(sites[0])
              p['site_desc'] = sites[0].name;
            callback_();
          })
        }
        else {
          callback();
        }
      };
      expandPH(prices, 0, () => {
        observer.next(prices);
        observer.complete();
      })
    });
  }

  getPriceHeaders(page:number, results:number){
    let query = {
      page:page,
      results:results
    };

    let filterQuery = this.filters.getQuery();

    query = Object.assign(query,filterQuery);

    return this.priceHeadersService.getPriceHeaders(query);
  }

  viewPriceHeader(ph:PriceHeader){
    this.viewEvent.emit(ph)
  }

  openClonePriceDialog(){
    this.newPeriodRange = "";
    this.periodRange = [];
    this.clonePriceDialog.open();
  }

  getSelectedPricelists():any[]{
    return this.table.getSelectedItems();
  }

  newPeriodRange:string;
  periodRange:string[] = [];

  calcNewPeriodRange(){
    let d = new Date();
    this.periodRange = [];
    switch(this.newPeriodRange){
      case 'MONTH':
        let d1_month = new Date();
        d1_month.setDate(1);
        d1_month.setMonth(d.getMonth()+1);
        let d2_month = new Date();
        d2_month.setDate(1);
        d2_month.setMonth(d.getMonth()+2);
        d2_month.setDate(d2_month.getDate()-1);
        this.periodRange = [getUrlDate(d1_month),getUrlDate(d2_month)];
        break;
      case 'QUARTER':
        let d1_trimester = new Date();
        d1_trimester.setDate(1);

        let currentMonth = d.getMonth()+1;

        let startMonth = currentMonth + (3 - currentMonth%3);

        d1_trimester.setMonth(startMonth);
        let d2_trimester = new Date();
        d2_trimester.setDate(1);
        d2_trimester.setMonth(startMonth+3);
        d2_trimester.setDate(d2_trimester.getDate()-1);
        this.periodRange = [getUrlDate(d1_trimester),getUrlDate(d2_trimester)];
        break;
      default:
        break;
    }
  }

  calcStatus(ph:PriceHeader){
    let periods = ph.PeriodLink.sort((a:PricePeriod,b:PricePeriod)=>{
      return parseInt(b.ValidTo,10)-parseInt(a.ValidTo,10);
    });

    let lastPeriod = periods[0];
    let periodRangeStart = parseInt(this.periodRange[0].split("-").join(""),10);

    let d1 = new Date(sapDateToUrlDate(lastPeriod.ValidTo));
    let d2 = new Date(sapDateToUrlDate(periodRangeStart+""));

    let diff = dateDiffInDays(d1,d2);

    if(diff==1){
      return "OK";
    }
    else {
      if(diff<0){
        return "KO";
      }
      else {
        return "WARN";
      }
    }
  }

  canCopy(){
    if(this.periodRange.length>0){
      let phs:any[] = this.table.getSelectedItems();
      let statuses = phs.map((ph)=>this.calcStatus(ph));
      return statuses.indexOf("KO")==-1;
    }
    else {
      return false;
    }

  }

  eventRows:EventsRow[] = [];

  showPeriods(ph:PriceHeader){
    this.eventRows = this.getEventRows(ph);
    this.periodsDialog.open();
  }

  getEventRows(ph:PriceHeader) {
    let currentPeriod = new EventsRow();
    currentPeriod.name = "Current";
    currentPeriod.events = ph.PeriodLink.map((p:PricePeriod)=>{
      let te = new TimeLineEvent();
      te.start = new Date(sapDateToUrlDate(p.ValidFrom));
      te.end = new Date(sapDateToUrlDate(p.ValidTo));
      te.title = "";
      te.color = "green";
      return te;
    });

    let newPeriod = new EventsRow();
    newPeriod.name = "New";

    let te = new TimeLineEvent();
    te.start = new Date(this.periodRange[0]);
    te.end = new Date(this.periodRange[1]);
    te.title = "";
    te.color = "blue";

    return [currentPeriod, newPeriod];
  }

  copyPriceHeaders(){
    let phs:any[] = this.table.getSelectedItems();

    let org = phs[0].SalesOrganization;

    this.conditionNamesService.getConditionNames({SalesOrganization:org}).subscribe((resp)=>{
      if(resp.status==200){
        let conditions:ConditionName[] = resp.response.results;
        let condCodes = conditions.map((cn)=>cn.Condition);

        let wip:WIPPeriod[] = phs.map((ph)=>{
          let wp = new WIPPeriod();
          wp.ValidFrom = this.periodRange[0].split("-").join("");
          wp.ValidTo = this.periodRange[1].split("-").join("");
          wp.PackInstruction = ph.PackInstruction;
          wp.SalesOrganization = ph.SalesOrganization;
          wp.Customer = ph.Customer;
          wp.Material = ph.Material;
          wp.ShipToParty = ph.ShipToParty;
          wp.Plant = ph.Plant;

          let periods = ph.PeriodLink.sort((a:PricePeriod,b:PricePeriod)=>{
            return parseInt(b.ValidTo,10)-parseInt(a.ValidTo,10);
          });

          let lastPeriod = periods[0];

          lastPeriod.ConditionLink = lastPeriod.ConditionLink.filter((cl)=>{
            return condCodes.indexOf(cl.Condition)!=-1;
          });

          wp.WIPConditionLink = lastPeriod.ConditionLink.map((c)=>{
            let wipCond = new WIPCondition();
            wipCond.Condition = c.Condition;
            wipCond.PriceUnit = c.PriceUnit;
            wipCond.Value = c.Value;
            wipCond.Currency = c.Currency;
            wipCond.OriginalValue = c.Value;
            wipCond.OldValue = c.Value;
            return wipCond;
          });

          return wp;
        });

        let count = wip.length;
        this.loadingService.show();
        let callback = ()=>{
          count--;
          if(count==0){
            this.loadingService.hide();
            this.clonePriceDialog.close();
            this.reload();
          }
        };

        wip.forEach((wp)=>{
          this.wipPeriodService.saveWIPPeriod(wp).subscribe((resp)=>{
            if(resp.status>200 && resp.status<300){
              let match = phs.filter((p) => {
                return p.Material == wp.Material &&
                  p.Customer == wp.Customer &&
                  p.ShipToParty == wp.ShipToParty &&
                  p.PackInstruction == wp.PackInstruction &&
                  p.SalesOrganization == wp.SalesOrganization &&
                  p.Plant == wp.Plant
              });
              if(match.length>0){
                match[0].savedInProgress = true;
              }
            }
            else {
              this.messageService.open({
                title: this.tr('ERROR'),
                error: true,
                message: buildErrorMessage(resp),
                autoClose: false
              });
            }
            callback();
          })
        })

      }
    });
  }

  reload(){
    if(!this.ready){
      this.ready = true;
    }
    else {
      this.table.reload();
    }
  }

  preventAccess(){
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: "No customer managed by the user",
      autoClose: false
    });
  }
}
