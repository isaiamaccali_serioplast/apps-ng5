import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SalesPricelistInProgressComponent} from "./sales-pricelist-in-progress.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {SalesPricelistInProgressListComponent} from "./sales-pricelist-in-progress-list/sales-pricelist-in-progress-list.component";
import {SalesPricelistInProgressEditComponent} from "./sales-pricelist-in-progress-edit/sales-pricelist-in-progress-edit.component";
import {SalesPriceListFiltersComponent} from "../sales-pricelist-filters/sales-pricelist-filters.component";
import {SalesPricelistFiltersModule} from "../sales-pricelist-filters/sales-pricelist-filters.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule,SalesPricelistFiltersModule],       // module dependencies
  declarations: [SalesPricelistInProgressComponent,SalesPricelistInProgressListComponent,SalesPricelistInProgressEditComponent],   // components and directives
  exports:[SalesPricelistInProgressComponent,SalesPricelistInProgressListComponent,SalesPricelistInProgressEditComponent],
  providers: []                    // services
})
export class SalesPricelistInProgressModule { }


