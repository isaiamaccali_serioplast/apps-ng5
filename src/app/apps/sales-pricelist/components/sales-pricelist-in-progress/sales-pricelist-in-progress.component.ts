import {Component} from "@angular/core";
import {WIPPeriod, WIPPeriodService} from "../../services/wip-period.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-in-progress',
  templateUrl: 'sales-pricelist-in-progress.component.html',
  styleUrls: ['sales-pricelist-in-progress.component.css']
})

export class SalesPricelistInProgressComponent extends TranslatableComponent{

  editMode = false;
  createMode = false;
  wipToEdit:WIPPeriod;
  loading = false;

  constructor(private messageService:MessageService, protected translationService:TranslationService,
              private wipPeriodService:WIPPeriodService, private loadingService:LoadingService) {
    super(translationService);
  }

  createWIP(wip:WIPPeriod){
    this.wipToEdit = wip;
    this.createMode = true;
    this.editMode = true;
  }

  editWIP(wip:WIPPeriod){
    this.wipToEdit = wip;
    this.createMode = false;
    this.editMode = true;
  }

  deleteWIP(wip:WIPPeriod){
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.loadingService.show();
        this.wipPeriodService.deleteWIPPeriod(wip).subscribe((resp)=>{
          this.loadingService.hide();
          if (resp.status == 204) {
            this.loading = true;
            setTimeout(() =>{
              this.loading = false;
            },500)
          }
          else {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      },
      deny:  ()=> {}
    })
  }

  back(){
    this.editMode = false;
  }
}
