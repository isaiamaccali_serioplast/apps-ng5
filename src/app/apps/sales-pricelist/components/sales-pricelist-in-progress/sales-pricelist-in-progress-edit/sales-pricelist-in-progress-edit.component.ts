import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {WIPCondition, WIPPeriod, WIPPeriodService} from "../../../services/wip-period.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {Material, Material_SAP, MaterialsSAPService} from "../../../../_common/services/materials.service";
import {MaterialPlantSAP, MaterialPlantsSAPService} from "../../../../_common/services/material-plant.service";
import {
  CustomerSalesOrg, SalesOrganizationSAP,
  SalesOrganizationSAPService
} from "../../../services/sales-organizations.service";
import {PlantSAP, PlantSAPService} from "../../../../_common/services/plant-flow-service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {buildErrorMessage, cloneObject, parseSerionetRespToData, sapDateToUrlDate} from "../../../../../shared/utils";
import {PackagingSap, PackagingSapService} from "../../../../_common/services/packaging-sap.service";
import {ConditionName, ConditionNameService} from "../../../services/condition-name.service";
import {SalesOrganization, SalesOrganizationService} from "../../../../_common/services/sales-organization.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {LegalEntity, LegalEntityService} from "../../../../_common/services/legal-entity.service";
import {Organization} from "../../../../_common/services/organization.service";
import {Customer, CustomerService} from "../../../../_common/services/customers.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-in-progress-edit',
  templateUrl: 'sales-pricelist-in-progress-edit.component.html',
  styleUrls: ['sales-pricelist-in-progress-edit.component.css']
})

export class SalesPricelistInProgressEditComponent extends TranslatableComponent implements OnInit {

  @Input('wipToEdit') wipToEdit: WIPPeriod;
  @Input('createMode') createMode = false;
  @Output('backEvent') backEvent = new EventEmitter();

  materialSAP: Material_SAP = new Material_SAP();
  salesOrganizationsSerionet: SalesOrganization[] = [];
  organizationSerionet:SalesOrganization = new SalesOrganization();
  organizations: Organization[] = [];
  salesOrganization: SalesOrganizationSAP = new SalesOrganizationSAP();
  salesOrganizations: SalesOrganizationSAP[] = [];
  salesOrganizationsToShow: SalesOrganizationSAP[] = [];
  plants: PlantSAP[] = [];
  plantsToShow: PlantSAP[] = [];
  packaging: PackagingSap = new PackagingSap();
  packagings: PackagingSap[] = [];
  customer: CustomerSalesOrg = new CustomerSalesOrg();
  shipToParty: CustomerSalesOrg = new CustomerSalesOrg();
  customers: CustomerSalesOrg[] = [];
  customersToShow: CustomerSalesOrg[] = [];
  shipToPartyToShow: CustomerSalesOrg[] = [];
  conditions: ConditionName[] = [];
  legalEntities: LegalEntity[] = [];
  organizationCustomers:Customer[] = [];
  materialPlants:MaterialPlantSAP[] = [];

  ready = false;


  constructor(protected translationService: TranslationService, private conditionNamesService: ConditionNameService,
              private materialPlantsSAPService: MaterialPlantsSAPService, private salesOrganizationsSAPService: SalesOrganizationSAPService,
              private loadingService: LoadingService, private messageService: MessageService,
              private plantSAPService: PlantSAPService, private packagingSapService: PackagingSapService,
              private wipPeriodService: WIPPeriodService, private materialsSAPService: MaterialsSAPService,
              private salesOrganizationService: SalesOrganizationService, private loginService: LoginService,
              private legalEntityService: LegalEntityService, private customersService:CustomerService) {
    super(translationService);
  }

  ngOnInit() {
    this.loadingService.show();
    this.legalEntityService.getLegalEntities({is_active: true}).subscribe((resp) => {
      this.legalEntities = parseSerionetRespToData(resp);
      this.salesOrganizationsSAPService.getSalesOrganizations({}).subscribe((resp) => {
        if (resp.status == 200) {
          let organizations = resp.response.results.filter((o: SalesOrganizationSAP) => o.CustomerLink.results.length > 0);
          organizations = organizations.filter(
            (so) => this.legalEntities.map((le) => le.code).indexOf(so.SalesOrganizationCode) != -1);
          this.salesOrganizations = organizations;
          this.salesOrganizationService.getSalesOrganizations({is_active:true, user:this.loginService.user.pk}).subscribe((resp)=> {
            this.salesOrganizationsSerionet = parseSerionetRespToData(resp);
            if(this.salesOrganizationsSerionet.length>0){
              this.organizations = this.salesOrganizationsSerionet.map((so) => so.organization);
              if (!this.createMode) {
                this.wipToEdit.basePriceUnit = this.wipToEdit.WIPConditionLink[0].PriceUnit;
                this.wipToEdit.ValidFrom = sapDateToUrlDate(this.wipToEdit.ValidFrom);
                this.wipToEdit.ValidTo = sapDateToUrlDate(this.wipToEdit.ValidTo);
                this.materialsSAPService.getMaterial("'" + this.wipToEdit.Material + "'").subscribe((resp) => {
                  this.loadingService.hide();
                  if (resp.status == 200) {
                    this.materialSAP = resp.response;
                    if (this.wipToEdit.PackInstruction) {
                      this.packagingSapService.getPackagings({Material: this.materialSAP.Code}).subscribe((resp) => {
                        this.loadingService.hide();
                        if (resp.status == 200) {
                          this.packagings = resp.response.results;
                          this.packaging = this.packagings.filter((p) => p.Palletization == this.wipToEdit.PackInstruction)[0];
                          this.ready = true;
                        }
                      });
                    }
                    else {
                      this.ready = true;
                    }
                  }
                });

                this.salesOrganization = this.salesOrganizations.filter((so) => so.SalesOrganizationCode == this.wipToEdit.SalesOrganization)[0];
                this.customer = this.salesOrganization.CustomerLink.results.filter((c) => c.Customer == this.wipToEdit.Customer)[0];
                this.shipToParty = this.salesOrganization.CustomerLink.results.filter((c) => c.Customer == this.wipToEdit.ShipToParty)[0];
                this.loadConditionNames();
              }
              else {
                this.loadingService.hide();
                this.ready = true;
              }
            }
          });
        }
      });
    });
  }

  loadConditionNames(callback?:any) {
    this.conditionNamesService.getConditionNames({SalesOrganization: this.wipToEdit.SalesOrganization}).subscribe((resp) => {
      if (resp.status == 200) {
        this.conditions = resp.response.results;

        this.wipToEdit.WIPConditionLink = this.conditions.map((cn)=>{
          let match = this.wipToEdit.WIPConditionLink.filter((wipC)=>wipC.Condition==cn.Condition);
          if(match && match[0]){
            match[0].conditionName = cn;
            match[0].Currency = cn.Currency||this.customer.Currency;
            match[0].OldValue = parseFloat(match[0].OldValue);
            match[0].OriginalValue = parseFloat(match[0].OriginalValue);
            match[0].Value = parseFloat(match[0].Value);
            match[0].valueBackup = match[0].Value;
            match[0].oldValueSet = false;
            return match[0];
          }
          else {
            let wipC = new WIPCondition();
            wipC.Condition = cn.Condition;
            wipC.Currency = cn.Currency||this.customer.Currency;
            wipC.Value = 0;
            wipC.OldValue = 0;
            wipC.OriginalValue = 0;
            wipC.oldValueSet = true;
            wipC.Description = cn.Description;
            wipC.conditionName = cn;
            return wipC;
          }
        });

        this.updateCondsPriceUnit();
        if(callback)
          callback.call(this);
      }
    });
  }

  back() {
    this.backEvent.emit();
  }

  handleOrganizationChange(callback?:any) {
    this.wipToEdit.SalesOrganization = "";
    this.wipToEdit.Customer = "";
    this.wipToEdit.ShipToParty = "";
    let so: SalesOrganization = cloneObject(this.salesOrganizationsSerionet.filter((so) => so.organization.pk == this.organizationSerionet.pk)[0]);
    let le = so.legal_entities;
    if (le.length == 0) {
      le = this.legalEntities;
    }
    else {
      le.forEach((l) => {
        l = this.legalEntities.filter((l_) => l_.pk == l.pk)[0];
      })
    }
    let codes = le.map((l) => l.code);


    this.customersService.getCustomers({
      organization_ref: this.organizationSerionet.pk,
      is_active: true
    }).subscribe((resp) => {
      this.organizationCustomers = parseSerionetRespToData(resp);
      let customerCodes = this.organizationCustomers.map((c)=>c.sap_code);

      this.salesOrganizationsToShow = this.salesOrganizations.filter((so) =>{
        return codes.indexOf(so.SalesOrganizationCode) != -1 &&
          so.CustomerLink.results.filter((c)=>customerCodes.indexOf(c.Customer)!=-1 && (c.Type=='CUST'||c.Type=='BOTH')).length>0;
      });

      if(callback)
        callback.call(this);
    });
  }

  handleSalesOrganizationChange(callback?:any){
    if(this.wipToEdit.SalesOrganization){
      this.salesOrganization = this.salesOrganizations.filter((so)=>so.SalesOrganizationCode==this.wipToEdit.SalesOrganization)[0];
      this.customersToShow = [];
      this.wipToEdit.Customer = "";
      this.wipToEdit.ShipToParty = "";
      let codes = this.organizationCustomers.map((c)=>c.sap_code);
      this.customersToShow = this.salesOrganization.CustomerLink.results.filter((c)=>codes.indexOf(c.Customer)!=-1 && (c.Type=='CUST'||c.Type=='BOTH'));
      this.shipToPartyToShow = this.salesOrganization.CustomerLink.results.filter((c)=>codes.indexOf(c.Customer)!=-1 && (c.Type=='BOTH'||c.Type=='SHIP'));
      this.plants = [];
      this.plantSAPService.getPlants({SalesOrganization:this.wipToEdit.SalesOrganization}).subscribe((resp)=>{
        if(resp.status==200){
          this.plants = resp.response.results;
          this.checkPlants();
          if(callback)
            callback.call(this);


        }
      });

    }
  }

  preventMaterialEvent = false;

  selectedMaterial(m: Material, callback?: any) {
    if (this.preventMaterialEvent) return;
    if (m && m.pk) {
      this.wipToEdit.Material = m.code;
      this.materialSAP.Uom = m.uom.__str__;
      this.packagingSapService.getPackagings({Material: this.wipToEdit.Material}).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          this.packagings = resp.response.results;
          this.materialPlantsSAPService.getMaterialPlants({MatCode: this.wipToEdit.Material}).subscribe((resp)=>{
            this.materialPlants = [];
            if(resp.status==200){
              this.materialPlants = resp.response.results;
            }
            this.checkPlants();
            if (callback) {
              callback.call(this);
            }
          })
        }
      });
    }
    else {
      this.wipToEdit.Material = "";
    }
  }

  checkPlants(){
    this.plantsToShow = [];
    if(this.plants.length>0 && this.materialPlants.length>0){
      let mpCodes = this.materialPlants.map((mp)=>mp.Plant);
      this.plantsToShow = this.plants.filter((p)=>mpCodes.indexOf(p.Code)!=-1);
    }
  }

  canSave() {
    return this.wipToEdit && this.wipToEdit.Material && this.wipToEdit.SalesOrganization && this.wipToEdit.Customer && this.wipToEdit.ShipToParty && this.wipToEdit.Plant;
  }

  save() {
    this.loadingService.show();
    this.wipPeriodService.saveWIPPeriod(this.wipToEdit).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status > 200 && resp.status < 300) {
        this.back();
      }
      else {
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  updateCondsPriceUnit() {
    this.wipToEdit.WIPConditionLink.forEach((wic) => {
      if (wic.Currency != "%"){
        wic.PriceUnit = this.wipToEdit.basePriceUnit;
        wic.Uom = this.materialSAP.Uom;
      }

    })
  }

  calculateTotal(wipC:WIPCondition){
    let cond = wipC.conditionName;
    let form = cond.Formula;
    let condNamesToSum = form.split("+").filter((c:string)=>c!="");
    let condsToSum = this.wipToEdit.WIPConditionLink.filter((wipC)=>condNamesToSum.indexOf(wipC.Condition)!=-1);

    let condsValueToSum = condsToSum.map((wipC)=>{
      if(wipC.conditionName.Formula && wipC.conditionName.Currency=="%"){
        return wipC.calculatedValue;
      }
      else{
        return wipC.Value||0;
      }
    });
    let condToUpdate = wipC;
    condToUpdate.Value = Math.floor(condsValueToSum.reduce((total, currentValue)=>{
      return total+currentValue;
    })*10000)/10000;
    return condToUpdate.Value;
  }

  calculateOldTotal(wipC:WIPCondition){
    let cond = wipC.conditionName;
    let form = cond.Formula;
    let condNamesToSum = form.split("+").filter((c:string)=>c!="");
    let condsToSum = this.wipToEdit.WIPConditionLink.filter((wipC)=>condNamesToSum.indexOf(wipC.Condition)!=-1);

    let condsValueToSum = condsToSum.map((wipC)=>wipC.OldValue);
    let condToUpdate = wipC;
    condToUpdate.OldValue = Math.floor(condsValueToSum.reduce((total, currentValue)=>{
          return total+currentValue;
        })*10000)/10000;
    return condToUpdate.OldValue;
  }

  doesConditionHaveAllData(wipC:WIPCondition){
    // let cond = wipC.conditionName;
    // let form = cond.Formula;
    // let condNamesToSum = form.split("+").filter((c:string)=>c!="");
    // let condsToSum = this.wipToEdit.WIPConditionLink.filter((wipC)=>condNamesToSum.indexOf(wipC.Condition)!=-1);
    // return (condsToSum.length==condNamesToSum.length)
    return true;
  }

  handleCustomerChange(){
    this.customer = this.salesOrganization.CustomerLink.results.filter((c) => c.Customer == this.wipToEdit.Customer)[0];

    this.loadConditionNames(()=>{
      this.wipToEdit.WIPConditionLink.forEach((wipC)=>{
        wipC.Currency = wipC.conditionName.Currency||this.customer.Currency;
      })
    });

  }

  updateOldValue(wipC:WIPCondition){
    if(!wipC.oldValueSet){
      wipC.OldValue = wipC.valueBackup;
    }
  }

  calcValue(wipp:WIPPeriod, wipC:WIPCondition){
    if(wipC.conditionName.Rule=="MP"){
      let form = wipC.conditionName.Formula;
      let condNamesToSum = form.substring(1, form.length-2).split("+").filter((c:string)=>c!="");

      let condsToSum = wipp.WIPConditionLink.filter((wipC_)=>condNamesToSum.indexOf(wipC_.Condition)!=-1);
      let condsValueToSum = condsToSum.map((wipC)=>wipC.Value);
      wipC.calculatedValue = condsValueToSum.reduce((total, currentValue)=>{
        return total+currentValue;
      })*wipC.Value/100;
      return Math.floor(wipC.calculatedValue*100)/100;
    }
  }

  calcOldValue(wipp:WIPPeriod, wipC:WIPCondition){
    if(wipC.conditionName.Rule=="MP"){
      let form = wipC.conditionName.Formula;
      let condNamesToSum = form.substring(1, form.length-2).split("+").filter((c:string)=>c!="");

      let condsToSum = wipp.WIPConditionLink.filter((wipC_)=>condNamesToSum.indexOf(wipC_.Condition)!=-1);
      let condsValueToSum = condsToSum.map((wipC)=>wipC.OldValue);
      wipC.calculatedOldValue = condsValueToSum.reduce((total, currentValue)=>{
        return total+currentValue;
      })*wipC.OldValue/100;
      return Math.floor(wipC.calculatedOldValue*100)/100;
    }
  }

  getWIPCurrency(wipp: WIPPeriod, cond: WIPCondition, skipMine = false) {
    if (cond.Currency && !skipMine) {
      return cond.Currency;
    }
    else {
      return this.salesOrganizations.filter((o) => {
        return o.SalesOrganizationCode == wipp.SalesOrganization
      })[0].CustomerLink.results.filter((c) => c.Customer == wipp.Customer)[0].Currency;
    }
  }
}
