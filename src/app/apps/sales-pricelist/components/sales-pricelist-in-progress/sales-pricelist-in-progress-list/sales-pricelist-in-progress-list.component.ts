import {Component, EventEmitter, OnInit, Output, ViewChild} from "@angular/core";
import {MaterialSAP, MaterialsSAPService} from "../../../../_common/services/materials.service";
import {WIPCondition, WIPPeriod, WIPPeriodService} from "../../../services/wip-period.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {arrayDistinct, arrayGroupBy, buildErrorMessage, parseSerionetRespToData} from "../../../../../shared/utils";
import {ConditionName, ConditionNameService} from "../../../services/condition-name.service";
import {BomReference, BomReferenceService} from "../../../../_common/services/bom-reference.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {PriceCondition, PriceHeader, PriceHeaderService, PricePeriod} from "../../../services/price-header.service";
import {MeasureUnit, MeasureUnitService} from "../../../../_common/services/measure-unit.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {SalesPriceListFiltersComponent} from "../../sales-pricelist-filters/sales-pricelist-filters.component";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

export class ConditionColumn {
  conditionName: ConditionName;
  conditions: WIPCondition[];
}

@Component({
  selector: 'sales-pricelist-in-progress-list',
  templateUrl: 'sales-pricelist-in-progress-list.component.html',
  styleUrls: ['sales-pricelist-in-progress-list.component.css']
})
export class SalesPricelistInProgressListComponent extends TranslatableComponent implements OnInit {

  @Output('createEvent') createEvent = new EventEmitter();
  @Output('editEvent') editEvent = new EventEmitter();
  @ViewChild('mbDialog') mbDialog: DialogComponent;
  @ViewChild('mpDialog') mpDialog: DialogComponent;
  @ViewChild('filters') filters: SalesPriceListFiltersComponent;

  wipPeriods: WIPPeriod[] = [];
  condColumns: ConditionColumn[] = [];
  conditions: ConditionName[] = [];
  groupedConditions: any = {};
  uoms: MeasureUnit[] = [];
  mbPrice: number;
  mbUom: string;
  mpPercentage: number;

  constructor(protected translationService: TranslationService, private wipPeriodService: WIPPeriodService,
              private loadingService: LoadingService, private materialsSAPService: MaterialsSAPService,
              private bomReferenceService: BomReferenceService, private messageService: MessageService,
              private priceHeaderService: PriceHeaderService, private measureUnitService: MeasureUnitService,
              private conditionNameService:ConditionNameService
              ) {
    super(translationService);
  }

  ngOnInit() {

    this.measureUnitService.getMeasureUnits({is_active: true, type: 'W'}).subscribe((resp) => {
      this.uoms = parseSerionetRespToData(resp);
    });

    this.conditionNameService.getConditionNames({}).subscribe((resp)=>{
      if(resp.status==200){
        this.conditions = resp.response.results;
        this.groupedConditions = arrayGroupBy(this.conditions, 'SalesOrganization');
      }
    })
  }

  reload() {
    this.loadingService.show();
    this.wipPeriodService.getWIPPeriods(this.filters.getQuery()).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        this.wipPeriods = resp.response.results;
        let cond = [];
        this.wipPeriods.forEach((wipP) => {
          wipP.WIPConditionLink = wipP.WIPConditionLink['results'];
          wipP.WIPConditionLink.forEach((c) => {
            c.PriceUnit = parseFloat(c.PriceUnit);
            c.Value = parseFloat(c.Value);
            c.conditionName = this.conditions.filter((c_) => c_.Condition == c.Condition)[0];
          });
          let perConds = wipP.WIPConditionLink.map((c) => c.Condition);
          wipP.basePriceUnit = wipP.WIPConditionLink[0].PriceUnit;
          this.updateCondsPriceUnit(wipP);

          wipP.materialFull = new MaterialSAP();
          this.materialsSAPService.getMaterial("'" + wipP.Material + "'").subscribe((resp) => {
            if (resp.status == 200) {
              wipP.materialFull = resp.response;
            }
          });
          cond = cond.concat(perConds);
        });
        let condCodes = arrayDistinct(cond);
        this.condColumns = [];
        let condColumns = [];
        condCodes.forEach((c) => {
          if (this.conditions.filter((c_) => c_.Condition == c && c_.SalesOrganization==this.filters.query.SalesOrganization).length > 0) {
            let condCol = new ConditionColumn();
            condCol.conditionName = this.conditions.filter((c_) => c_.Condition == c)[0];
            condCol.conditions = [];
            this.wipPeriods.forEach((wipp) => {
              condCol.conditions.push(this.getConditionByCondName(wipp, c));
            });
            condColumns.push(condCol);
          }
        });
        this.condColumns = condColumns.sort((cc1, cc2) => parseInt(cc1.conditionName.Sort, 10) - parseInt(cc2.conditionName.Sort, 10));
      }
      else {
        this.wipPeriods = [];
      }
    })
  }

  getConditionByCondName(wipPeriod: WIPPeriod, condName: string): WIPCondition {
    let match = wipPeriod.WIPConditionLink.filter((c) => c.Condition == condName);
    if (match.length > 0) {
      return match[0];
    }
    else {
      // let cond = new WIPCondition();
      // cond.Condition = condName;
      // cond.Currency = "EUR";
      // cond.PriceUnit = 0;
      // cond.Value = 0;
      // return cond;
      return null;
    }
  }

  isConditionEditable(wipp: WIPPeriod, condName: string) {
    let cond = this.getCondition(wipp.SalesOrganization, condName);
    if (cond) {
      return !cond.NoEdit && !cond.Rule;
    }
    else {
      return false;
    }
  }

  getCondition(org: string, condName: string): ConditionName {
    let avCond = this.groupedConditions[org];
    let condMatch: ConditionName[] = avCond.filter((cn: ConditionName) => cn.Condition == condName);
    if (condMatch.length > 0) {
      return condMatch[0];
    }
  }

  getCustomerDescription(wipp: WIPPeriod) {
    let cust = this.filters.salesOrganizations.filter((o) => {
      return o.SalesOrganizationCode == wipp.SalesOrganization
    })[0].CustomerLink.results.filter((c) => c.Customer == wipp.Customer)[0];
    return cust.Customer + " - " + cust.Name;
  }

  getShipToPartyDescription(wipp: WIPPeriod) {
    let cust = this.filters.salesOrganizations.filter((o) => {
      return o.SalesOrganizationCode == wipp.SalesOrganization
    })[0].CustomerLink.results.filter((c) => c.Customer == wipp.ShipToParty)[0];
    if (cust)
      return cust.Customer + " - " + cust.Name;
    else
      return wipp.ShipToParty
  }

  createWIP() {
    let wip = new WIPPeriod();
    wip.WIPConditionLink = [];
    this.createEvent.emit(wip)
  }

  editWIP(wip: WIPPeriod) {
    this.editEvent.emit(wip)
  }

  getWIPCurrency(wipp: WIPPeriod, cond: WIPCondition, skipMine = false) {
    if (cond.Currency && !skipMine) {
      return cond.Currency;
    }
    else {
      return this.filters.salesOrganizations.filter((o) => {
        return o.SalesOrganizationCode == wipp.SalesOrganization
      })[0].CustomerLink.results.filter((c) => c.Customer == wipp.Customer)[0].Currency;
    }
  }

  saveAll() {
    this.loadingService.show();
    this.savePeriods(0, () => {
      this.loadingService.hide();
      this.reload();
    })
  }

  savePeriods(index: number, callback: any) {
    if (this.wipPeriods[index]) {
      let wip = this.wipPeriods[index];
      this.wipPeriodService.saveWIPPeriod(wip).subscribe((resp) => {
        if (resp.status > 200 && resp.status < 300) {
          this.savePeriods(index + 1, callback);
        }
      })
    }
    else {
      callback.call(this);
    }
  }

  updateCondsPriceUnit(wipp: WIPPeriod) {
    wipp.WIPConditionLink.forEach((wic) => {
      if (wic.Currency != "%") {
        wic.PriceUnit = wipp.basePriceUnit;
        if(wipp.materialFull)
          wic.Uom = wipp.materialFull.Uom;
      }
    })
  }

  calculateTotal(wipp: WIPPeriod, condName: string) {
    let cond = this.getCondition(wipp.SalesOrganization, condName);
    let form = cond.Formula;
    let condNamesToSum = form.split("+").filter((c: string) => c != "");
    let condsToSum = wipp.WIPConditionLink.filter((wipC) => condNamesToSum.indexOf(wipC.Condition) != -1);
    let condsValueToSum = condsToSum.map((wipC) => wipC.Currency != '%' ? (wipC.Value || 0) : (wipC.calculatedValue || 0));

    let condToUpdate = wipp.WIPConditionLink.filter((wipC) => wipC.Condition == condName)[0];
    condToUpdate.Value = condsValueToSum.reduce((total, currentValue) => {
      return total + currentValue;
    });
    return condToUpdate.Value;
  }

  doesConditionHaveAllData(wipp: WIPPeriod, condName: string) {
    return true;
    // let cond = this.getCondition(wipp.Organization,condName);
    // if(cond){
    //   if(cond.Rule=="T"){
    //     // let form = cond.Formula;
    //     // let condNamesToSum = form.split("+").filter((c:string)=>c!="");
    //     // let condsToSum = wipp.WIPConditionLink.filter((wipC)=>condNamesToSum.indexOf(wipC.Condition)!=-1);
    //     // return condsToSum.length==condNamesToSum.length;
    //     return true;
    //   }
    //   else {
    //     if(cond.Rule=="MP"){
    //       let form = cond.Formula;
    //       let condNamesToSum = form.substring(1, form.length-2).split("+").filter((c:string)=>c!="");
    //       let condsToSum = wipp.WIPConditionLink.filter((wipC)=>condNamesToSum.indexOf(wipC.Condition)!=-1);
    //       return condsToSum.length==condNamesToSum.length && wipp.WIPConditionLink.filter((wipC)=>[condName].indexOf(wipC.Condition)!=-1).length==1;
    //     }
    //     else {
    //       if(cond.Rule=="MB"){
    //         return true;
    //       }
    //       else {
    //         return cond.Rule=="";
    //       }
    //     }
    //   }
    // }
    // else {
    //   return false;
    // }
  }

  mpConfirm = false;
  mbConfirm = false;

  editConditionMassively(cn: ConditionName) {

    if (cn.Rule == "MP") {
      this.mpPercentage = 0;
      this.mpConfirm = false;
      this.mpDialog.onClose.subscribe(() => {
        if(this.mpConfirm){
          let percentage = this.mpPercentage;
          this.getSelectedPeriods().forEach((wipp) => {
            let condToUpdate = wipp.WIPConditionLink.filter((wipC) => wipC.Condition == cn.Condition)[0];
            condToUpdate.Value = percentage;
          })
        }
      });
      this.mpDialog.open();
    }
    else {
      if (cn.Rule == "MB") {
        this.mbPrice = 0;
        this.mbUom = "";
        this.mbConfirm = false;
        this.mbDialog.onClose.subscribe(() => {
          if(this.mbConfirm){
            let periods = this.getSelectedPeriods();
            let count = periods.length;
            this.loadingService.show();
            let callback = () => {
              count--;
              if (count == 0) {
                this.loadingService.hide();
              }
            };

            periods.forEach((wipp) => {
              let bomRef = new BomReference();
              bomRef.Material = wipp.Material;
              bomRef.Plant = wipp.Plant;
              bomRef.RefQuantity = wipp.basePriceUnit;
              bomRef.RefUom = this.mbUom;
              bomRef.Rule = cn.Formula;

              this.bomReferenceService.getBomReference(bomRef.getId()).subscribe((resp) => {
                if (resp.status == 200) {
                  let br: BomReference = resp.response;
                  let condToUpdate = wipp.WIPConditionLink.filter((wipC) => wipC.Condition == cn.Condition)[0];
                  condToUpdate.Value = parseFloat(br.Quantity) * this.mbPrice;
                  callback();
                }
                else {
                  this.loadingService.hide();
                  this.messageService.open({
                    title: this.tr('ERROR'),
                    error: true,
                    message: buildErrorMessage(resp),
                    autoClose: false
                  });
                }
              });
            })
          }
        });
        this.mbDialog.open();
      }
    }
  }

  allPeriodsHaveOkCondition(cn: ConditionName) {
    let isOK = true;
    this.wipPeriods.forEach((wipp) => {
      if (!this.doesConditionHaveAllData(wipp, cn.Condition)) {
        isOK = false;
      }
    });
    return isOK;
  }

  selectAll = false;

  toggleSelectAll() {
    if (this.selectAll) {
      this.wipPeriods.forEach((i) => i.selected = true);
    }
    else {
      this.wipPeriods.forEach((i) => i.selected = false);
    }
  }

  calcValue(wipp: WIPPeriod, wipC: WIPCondition) {
    if (wipC.conditionName.Rule == "MP") {
      let form = wipC.conditionName.Formula;
      let condNamesToSum = form.substring(1, form.length - 2).split("+").filter((c: string) => c != "");

      let condsToSum = wipp.WIPConditionLink.filter((wipC_) => condNamesToSum.indexOf(wipC_.Condition) != -1);
      let condsValueToSum = condsToSum.map((wipC) => (wipC.Value || 0));
      wipC.calculatedValue = condsValueToSum.reduce((total, currentValue) => {
        return total + currentValue;
      }) * wipC.Value / 100;
      return Math.floor(wipC.calculatedValue * 100) / 100;
    }
  }

  hasSelectedPeriods() {
    return this.getSelectedPeriods().length > 0
  }

  getSelectedPeriods() {
    return this.wipPeriods.filter((wipp) => wipp.selected)
  }

  promoteSelectedWIPP() {
    let selectedPeriods = this.wipPeriods.filter((wipp) => wipp.selected);


    let conditions: ConditionName[] = this.groupedConditions[this.wipPeriods[0].SalesOrganization];

    let conditionMissing = false;
    selectedPeriods.forEach((wipp) => {
      conditions.forEach((cn) => {
        let match = wipp.WIPConditionLink.filter((wipC) => wipC.Condition == cn.Condition);
        if (match.length == 0) {
          conditionMissing = true;
        }
        else {
          if (!this.doesConditionHaveAllData(wipp, cn.Condition)) {
            conditionMissing = true;
          }
        }
      })
    });

    if (conditionMissing) {
      this.messageService.open({
        title: this.tr('ERROR'),
        error: true,
        message: "One or more selected periods are incomplete",
        autoClose: false
      });
    }
    else {
      this.loadingService.show();
      this.promoteWips(selectedPeriods, 0, () => {
        this.loadingService.hide();
        this.reload();
      })
    }


  }

  promoteWips(wips: WIPPeriod[], index, callback) {
    if (wips[index]) {
      this.promoteWIP(wips[index], () => {
        this.promoteWips(wips, index + 1, callback);
      })
    }
    else {
      callback.call(this);
    }
  }

  promoteWIP(wip: WIPPeriod, callback: any) {
    let ph = new PriceHeader();
    ph.SalesOrganization = wip.SalesOrganization;
    ph.Customer = wip.Customer;
    ph.Material = wip.Material;
    ph.ShipToParty = wip.ShipToParty;
    ph.PackInstruction = wip.PackInstruction;
    ph.Plant = wip.Plant;

    let period = new PricePeriod();
    period.ValidTo = wip.ValidTo;
    period.ValidFrom = wip.ValidFrom;

    period.ConditionLink = wip.WIPConditionLink.map((wipC) => {
      let cond = new PriceCondition();
      cond.Condition = wipC.Condition;
      cond.PriceUnit = wipC.PriceUnit;
      cond.Value = wipC.Value;
      cond.Currency = wipC.Currency;
      return cond;
    });

    ph.PeriodLink = [period];

    this.priceHeaderService.savePriceHeader(ph).subscribe((resp) => {
      if (resp.status > 200 && resp.status < 300) {
        this.wipPeriodService.deleteWIPPeriod(wip).subscribe((resp) => {
          if (resp.status == 204) {
            callback.call(this);
          }
          else {
            this.loadingService.hide();
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      }
      else {
        this.loadingService.hide();
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  deleteSelectedWIPP() {
    let selectedPeriods = this.wipPeriods.filter((wipp) => wipp.selected);
    this.loadingService.show();
    this.deleteWips(selectedPeriods, 0, () => {
      this.loadingService.hide();
      this.reload();
    })
  }

  deleteWips(wips: WIPPeriod[], index, callback) {
    if (wips[index]) {
      this.deleteWIP(wips[index], () => {
        this.deleteWips(wips, index + 1, callback);
      })
    }
    else {
      callback.call(this);
    }
  }

  deleteWIP(wip: WIPPeriod, callback: any) {
    this.wipPeriodService.deleteWIPPeriod(wip).subscribe((resp) => {
      if (resp.status == 204) {
        callback.call(this);
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  preventAccess(){
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: "No customer managed by the user",
      autoClose: false
    });
  }
}
