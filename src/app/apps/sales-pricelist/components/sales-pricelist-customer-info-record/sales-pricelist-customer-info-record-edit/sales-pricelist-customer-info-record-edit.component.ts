import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {CustMat, CustomerMaterialService} from "../../../services/customer-material.service";
import {
  CustomerSalesOrg, SalesOrganizationSAP,
  SalesOrganizationSAPService
} from "../../../services/sales-organizations.service";
import {HeaderConf} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../../../../shared/utils";
import {Material, Material_SAP, MaterialsSAPService} from "../../../../_common/services/materials.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MaterialSales, MaterialSalesService} from "../../../../_common/services/material-sales.service";
import {LegalEntity, LegalEntityService} from "../../../../_common/services/legal-entity.service";
import {SalesOrganization, SalesOrganizationService} from "../../../../_common/services/sales-organization.service";
import {Organization} from "../../../../_common/services/organization.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {Customer, CustomerService} from "../../../../_common/services/customers.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-customer-info-record-edit',
  templateUrl: 'sales-pricelist-customer-info-record-edit.component.html',
  styleUrls: ['sales-pricelist-customer-info-record-edit.component.css']
})
export class SalesPriceListCustomerInfoRecordEditComponent extends TranslatableComponent implements OnInit {

  @ViewChild('organizationDialog') organizationDialog:DialogComponent;
  @Output('back') backEvent = new EventEmitter();
  @Input('cirToEdit') cirToEdit:CustMat = new CustMat();
  @Input('create') create = false;
  legalEntities:LegalEntity[] = [];
  salesOrganization:SalesOrganizationSAP;
  salesOrganizations:SalesOrganizationSAP[] = [];
  salesOrganizationsToShow:SalesOrganizationSAP[] = [];
  customer:CustomerSalesOrg;
  customerCodeFilter:string;
  customerNameFilter:string;
  materialSAP:Material_SAP;
  salesOrganizationsSerionet:SalesOrganization[] = [];
  organizations:Organization[] = [];

  headerConfCustomers:HeaderConf[] = [
    {
      valueKey:'Customer',
      label:'Customer'
    },
    {
      valueKey:'Name',
      label:'Name'
    }
  ];

  materialSales:MaterialSales[] = [];
  ready = false;
  organization_serionet:number;
  organizationCustomers:Customer[] = [];

  constructor(private materialsSAPService:MaterialsSAPService,
              private salesOrganizationsSAPService:SalesOrganizationSAPService, private loginService:LoginService,
              private loadingService:LoadingService, private messageService:MessageService, protected translationService:TranslationService,
              private customerMaterialService:CustomerMaterialService, private materialSalesService:MaterialSalesService,
              private legalEntityService:LegalEntityService, private salesOrganizationsService: SalesOrganizationService,
              private customersService:CustomerService
  ){
    super(translationService);
  }

  ngOnInit() {
    if(this.cirToEdit.Material){
      this.loadingService.show();
      this.salesOrganizationsSAPService.getSalesOrganization("'"+this.cirToEdit.SalesOrganization+"'").subscribe((resp)=>{
        if(resp.status==200){
          this.salesOrganization = resp.response;
          this.customer = this.salesOrganization.CustomerLink.results.filter((c)=>c.Customer==this.cirToEdit.Customer)[0];
          this.materialsSAPService.getMaterial("'"+this.cirToEdit.Material+"'").subscribe((resp)=> {
            if (resp.status == 200) {
              this.materialSAP = resp.response;
              this.loadingService.hide();
            }
          });
        }
      });
      let cirToEdit = new CustMat();
      for(let key in this.cirToEdit){
        if(this.cirToEdit.hasOwnProperty(key))
         cirToEdit[key] = this.cirToEdit[key];
      }
      this.cirToEdit = cirToEdit;
      this.ready = true;
    }
    else {
      this.legalEntityService.getLegalEntities({is_active: true}).subscribe((resp) => {
        this.legalEntities = parseSerionetRespToData(resp);
        this.salesOrganizationsSAPService.getSalesOrganizations({}).subscribe((resp) => {
          if (resp.status == 200) {
            let organizations = resp.response.results.filter((o: SalesOrganizationSAP) => o.CustomerLink.results.length > 0);
            organizations = organizations.filter(
              (so) => this.legalEntities.map((le) => le.code).indexOf(so.SalesOrganizationCode) != -1);
            this.salesOrganizations = organizations;
            this.salesOrganizationsService.getSalesOrganizations({is_active:true, user:this.loginService.user.pk}).subscribe((resp)=> {
              this.salesOrganizationsSerionet = parseSerionetRespToData(resp);
              if(this.salesOrganizationsSerionet.length>0){
                this.organizations = this.salesOrganizationsSerionet.map((so) => so.organization);
                this.ready = true;
              }
            });
          }
        });
      });
    }
  }

  selectedMaterial(m:Material){
    if(m && m.pk){
      this.cirToEdit.Material = m.code;
      this.loadingService.show();
      this.materialsSAPService.getMaterial("'" + this.cirToEdit.Material + "'").subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          this.materialSAP = resp.response;
        }
      });
    }
    else {
      this.cirToEdit.Material = "";
    }
  }

  handleOrganizationChange(callback?:any) {
    let so: SalesOrganization = cloneObject(this.salesOrganizationsSerionet.filter((so) => so.organization.pk == this.organization_serionet)[0]);
    let le = so.legal_entities;
    if (le.length == 0) {
      le = this.legalEntities;
    }
    else {
      le.forEach((l) => {
        l = this.legalEntities.filter((l_) => l_.pk == l.pk)[0];
      })
    }
    let codes = le.map((l) => l.code);


    this.customersService.getCustomers({
      organization_ref: this.organization_serionet,
      is_active: true
    }).subscribe((resp) => {
      this.organizationCustomers = parseSerionetRespToData(resp);
      let customerCodes = this.organizationCustomers.map((c)=>c.sap_code);

      let salesOrganizationsToShow = this.salesOrganizations.filter((so) =>{
        return codes.indexOf(so.SalesOrganizationCode) != -1 &&
          so.CustomerLink.results.filter((c)=>customerCodes.indexOf(c.Customer)!=-1 && (c.Type=='CUST'||c.Type=='BOTH')).length>0 &&
          this.materialSales.map((ms)=>ms.SalesOrganization).indexOf(so.SalesOrganizationCode)!=-1
      });
      salesOrganizationsToShow.forEach((so)=>{
        so.CustomerLink.results = so.CustomerLink.results.filter((c)=>customerCodes.indexOf(c.Customer)!=-1 && (c.Type=='BOTH'))
      });

      this.salesOrganizationsToShow = salesOrganizationsToShow;
      if(callback)
        callback.call(this);
    });
  }

  back(){
    this.backEvent.emit();
  }

  filterCustomers(cs:CustomerSalesOrg[]){
    return cs.filter((s)=>{
      if(s.Type=="BOTH"){
        if(this.customerCodeFilter){
          if(this.customerNameFilter){
            return s.Customer.indexOf(this.customerCodeFilter)!=-1 && s.Name.indexOf(this.customerNameFilter)!=-1
          }
          else {
            return s.Customer.indexOf(this.customerCodeFilter)!=-1
          }
        }
        else {
          if(this.customerNameFilter){
            return s.Name.indexOf(this.customerNameFilter)!=-1
          }
          else {
            return true;
          }
        }
      }
      else {
        return false;
      }
    })
  }

  selectedCustomer(o:SalesOrganizationSAP, c:CustomerSalesOrg){
    this.salesOrganization = o;
    this.customer = c;
    this.cirToEdit.SalesOrganization = o.SalesOrganizationCode;
    this.cirToEdit.Customer = c.Customer;
    this.organizationDialog.close();
  }

  clearMaterial(){
    this.cirToEdit = new CustMat();
  }

  openOrganizationDialog(){
    this.customerCodeFilter = "";
    this.customerNameFilter = "";
    this.organization_serionet = this.organizations[0].pk;
    this.loadingService.show();
    this.materialSalesService.getMaterialSales({Material: this.cirToEdit.Material}).subscribe((resp) => {
      if (resp.status == 200) {
        this.materialSales = resp.response.results;
        this.loadingService.hide();
        this.handleOrganizationChange(()=>{
          this.organizationDialog.open();
        })
      }
    });
  }

  save(){
    this.loadingService.show();
    this.customerMaterialService.saveCustomerMaterial(this.cirToEdit).subscribe((resp)=>{
      this.loadingService.hide();
      if(resp.status>200 && resp.status<300){
        this.back();
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: this.tr('ERROR'),
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  clearOrgSelection(){
    this.salesOrganization = new SalesOrganizationSAP();
    this.customer = new CustomerSalesOrg();
    this.cirToEdit.SalesOrganization = null;
    this.cirToEdit.Customer = null;
  }
}

