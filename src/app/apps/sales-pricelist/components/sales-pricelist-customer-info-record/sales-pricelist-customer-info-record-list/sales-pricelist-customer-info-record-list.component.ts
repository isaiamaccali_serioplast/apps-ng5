import {Component, EventEmitter, OnInit, Output, ViewChild} from "@angular/core";
import {
  HeaderConf,
  TableRowAction,
  TableRowActions,
  TableWithPagesComponent
} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {cloneObject} from "../../../../../shared/utils";
import {CustMat, CustomerMaterialService} from "../../../services/customer-material.service";
import {CustomerSalesOrg} from "../../../services/sales-organizations.service";
import {SalesPriceListFiltersComponent} from "../../sales-pricelist-filters/sales-pricelist-filters.component";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-customer-info-record-list',
  templateUrl: 'sales-pricelist-customer-info-record-list.component.html',
  styleUrls: ['sales-pricelist-customer-info-record-list.component.css']
})
export class SalesPriceListCustomerInfoRecordListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('table') table:TableWithPagesComponent;
  @ViewChild('filters') filters:SalesPriceListFiltersComponent;

  @Output('create') createEvent = new EventEmitter();
  @Output('edit') editEvent = new EventEmitter();
  @Output('delete') deleteEvent = new EventEmitter();


  headerConf:HeaderConf[] = [];
  availableActions = [];
  noItemsMessage = this.tr("NO_AVAILABLE_CUSTOMER_MATERIALS");
  ready = false;

  constructor(protected translationService: TranslationService, private customerMaterialService:CustomerMaterialService,
              private messageService:MessageService
  ) {
    super(translationService);
  }

  ngOnInit() {

    this.headerConf = [
      {
        label:'Material',
        valueFunction:(item:CustMat)=>{
          return item.Material+ " - " + item.MaterialDescription;
        }
      },
      {
        label:'Customer Material',
        valueFunction:(item:CustMat)=>{
          return item.CustomerMaterial+ " - " + item.CustomerMatDescript;
        }
      },
      {
        label:'Customer',
        valueFunction:(item:CustMat)=>{
          let c:CustomerSalesOrg = null;
          this.filters.salesOrganizations.forEach((so)=>{
            if(so.CustomerLink.results.filter((c)=>c.Customer==item.Customer).length>0){
              c = so.CustomerLink.results.filter((c)=>c.Customer==item.Customer)[0];
            }
          });
          if(c)
            return c.Customer+ " - " + c.Name;
          else
            return "-"
        }
      },
      {
        label:'Sales Organization',
        valueFunction:(item:CustMat)=>{
          let so = this.filters.salesOrganizations.filter((so)=>so.SalesOrganizationCode==item.SalesOrganization)[0];
          return so.SalesOrganizationCode+ " - " + so.Description;
        }
      }
    ];

    let actionEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEdit.click = this.editCustomerMaterial.bind(this);
    let actionDelete: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDelete.click = this.deleteCustomerMaterial.bind(this);
    this.availableActions = [actionEdit, actionDelete];
  }

  getCustomerMaterials(page:number, results:number){
    let query = {
      page:page,
      results:results
    };

    let filterQuery = this.filters.getQuery();

    query = Object.assign(query,filterQuery);

    return this.customerMaterialService.getCustomerMaterials(query);
  }

  editCustomerMaterial(cm:CustMat){
    this.editEvent.emit(cm);
  }

  deleteCustomerMaterial(cm:CustMat){
    let cirToDelete = new CustMat();
    for(let key in cm){
      if(cm.hasOwnProperty(key))
        cirToDelete[key] = cm[key];
    }
    this.deleteEvent.emit(cirToDelete);
  }

  createPIR(){
    this.createEvent.emit();
  }

  reload(){
    if(!this.ready){
      this.ready = true;
    }
    else {
      this.table.reload();
    }
  }

  preventAccess(){
    this.messageService.open({
      title: this.tr('ERROR'),
      error: true,
      message: "No customer managed by the user",
      autoClose: false
    });
  }
}
