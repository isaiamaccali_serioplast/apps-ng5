import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SalesPricelistCustomerInfoRecordComponent} from "./sales-pricelist-customer-info-record.component";
import {
  SalesPriceListCustomerInfoRecordEditComponent
} from "./sales-pricelist-customer-info-record-edit/sales-pricelist-customer-info-record-edit.component";
import {SalesPriceListCustomerInfoRecordListComponent} from "./sales-pricelist-customer-info-record-list/sales-pricelist-customer-info-record-list.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {SalesPriceListFiltersComponent} from "../sales-pricelist-filters/sales-pricelist-filters.component";
import {SalesPricelistFiltersModule} from "../sales-pricelist-filters/sales-pricelist-filters.module";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule,SalesPricelistFiltersModule],       // module dependencies
  declarations: [
    SalesPricelistCustomerInfoRecordComponent,
    SalesPriceListCustomerInfoRecordListComponent,
    SalesPriceListCustomerInfoRecordEditComponent
  ],   // components and directives
  exports:[
    SalesPricelistCustomerInfoRecordComponent,
    SalesPriceListCustomerInfoRecordListComponent,
    SalesPriceListCustomerInfoRecordEditComponent],
  providers: []                    // services
})
export class SalesPricelistCustomerInfoRecordModule { }


