import {Component} from "@angular/core";
import {CustMat, CustomerMaterialService} from "../../services/customer-material.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-customer-info-record',
  templateUrl: 'sales-pricelist-customer-info-record.component.html',
  styleUrls: ['sales-pricelist-customer-info-record.component.css']
})

export class SalesPricelistCustomerInfoRecordComponent extends TranslatableComponent {

  listMode = true;
  cirToEdit: CustMat = null;
  create = false;
  loading = false;

  constructor(private customerMaterialService: CustomerMaterialService, private messageService: MessageService,
              protected translationService: TranslationService, private loadingService: LoadingService) {
    super(translationService);
  }

  editCIR(cir: CustMat) {
    this.cirToEdit = cir;
    this.create = false;
    this.listMode = false;
  }

  createCIR() {
    this.cirToEdit = new CustMat();
    this.create = true;
    this.listMode = false;
  }

  deleteCIR(cir: CustMat) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.loadingService.show();
        this.customerMaterialService.deleteCustomerMaterial(cir).subscribe((resp) => {
          this.loadingService.hide();
          if (resp.status == 204) {
            this.loading = true;
            setTimeout(() => {
              this.loading = false;
            }, 500)
          }
          else {
            resp.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      },
      deny: () => {
      }
    })
  }
}
