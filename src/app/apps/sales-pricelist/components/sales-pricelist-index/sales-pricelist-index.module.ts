import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SalesPricelistIndexComponent} from "./sales-pricelist-index.component";
import {SalesPricelistCustomerInfoRecordModule} from "../sales-pricelist-customer-info-record/sales-pricelist-customer-info-record.module";
import {SalesPricelistInProgressModule} from "../sales-pricelist-in-progress/sales-pricelist-in-progress.module";
import {SalesPricelistPricelistModule} from "../sales-pricelist-pricelist/sales-pricelist-pricelist.module";

@NgModule({
  imports: [SharedModule, SalesPricelistCustomerInfoRecordModule,SalesPricelistInProgressModule,SalesPricelistPricelistModule],       // module dependencies
  declarations: [SalesPricelistIndexComponent],   // components and directives
  exports:[SalesPricelistIndexComponent],
  providers: []                    // services
})
export class SalesPricelistIndexModule { }


