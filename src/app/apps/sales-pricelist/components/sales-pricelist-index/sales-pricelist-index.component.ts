import {Component, OnInit, QueryList, ViewChild, ViewChildren} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {MatTabGroup} from "@angular/material";
import {QuerySharerService} from "../../services/query-sharer.service";
import {WIPPeriod} from "../../services/wip-period.service";
import {SalesPricelistInProgressComponent} from "../sales-pricelist-in-progress/sales-pricelist-in-progress.component";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'sales-pricelist-index',
  templateUrl: 'sales-pricelist-index.component.html',
  styleUrls: ['sales-pricelist-index.component.css']
})

export class SalesPricelistIndexComponent extends TranslatableComponent implements OnInit {

  @ViewChild('tab') tab:MatTabGroup;
  @ViewChildren(SalesPricelistInProgressComponent) viewChildren: QueryList<SalesPricelistInProgressComponent>;
  selectedTab = 1;

  constructor(protected translationService: TranslationService, private titleService: Title,
              private activatedRoute:ActivatedRoute, private router:Router, private querySharerService:QuerySharerService
  ) {
    super(translationService);
    this.titleService.setTitle("Serioplast - Sales Pricelist");
    this.querySharerService.saveQuery(null);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params)=>{
      if(params['section']){
        switch(params['section']){
          case 'customer-info-record':
            this.selectedTab = 0;
            break;
          case 'pricelist':
            this.selectedTab = 1;
            break;
          case 'in-progress':
            this.selectedTab = 2;
            break;
          default:
            this.router.navigate(['customer-info-record'],{relativeTo:this.activatedRoute});
            break;
        }
      }
      else {
        this.router.navigate(['customer-info-record'],{relativeTo:this.activatedRoute});
      }
    })
  }

  tabChange(){
    let index = this.tab.selectedIndex;
    switch(index){
      case 0:
        this.router.navigate(['/sales-pricelist','customer-info-record']);
        break;
      case 1:
        this.router.navigate(['/sales-pricelist','pricelist']);
        break;
      case 2:
        this.router.navigate(['/sales-pricelist','in-progress']);
        break;
      default:
        break;
    }
  }

  editWIP(wip:WIPPeriod){
    this.router.navigate(['/sales-pricelist','in-progress']).then(()=>{
      let s = this.viewChildren.changes.subscribe(()=>{
        this.viewChildren.first.editWIP(wip);
        s.unsubscribe();
      })
    });
  }
}
