import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {LegalEntity, LegalEntityService} from "../../../_common/services/legal-entity.service";
import {SalesOrganization, SalesOrganizationService} from "../../../_common/services/sales-organization.service";
import {Organization} from "../../../_common/services/organization.service";
import {Customer, CustomerService} from "../../../_common/services/customers.service";
import {
  CustomerSalesOrg, SalesOrganizationSAP,
  SalesOrganizationSAPService
} from "../../services/sales-organizations.service";
import {Material, MaterialsService} from "../../../_common/services/materials.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {QuerySharerService} from "../../services/query-sharer.service";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {PackagingSap, PackagingSapService} from "../../../_common/services/packaging-sap.service";
import {PlantSAP, PlantSAPService} from "../../../_common/services/plant-flow-service";
import {MaterialPlantSAP, MaterialPlantsSAPService} from "../../../_common/services/material-plant.service";

@Component({
  selector: 'sales-pricelist-filters',
  templateUrl: 'sales-pricelist-filters.component.html',
  styleUrls: ['sales-pricelist-filters.component.css']
})
export class SalesPriceListFiltersComponent implements OnInit {

  @Input('cir') cir = false;
  @Output('reloadEvent') reloadEvent = new EventEmitter();
  @Output('preventAccess') preventAccess = new EventEmitter();
  legalEntities:LegalEntity[] = [];
  salesOrganizationsSerionet:SalesOrganization[] = [];
  organizations:Organization[] = [];
  organizationCustomers:Customer[] = [];
  salesOrganizations:SalesOrganizationSAP[] = [];
  salesOrganizationsToShow:SalesOrganizationSAP[] = [];
  material_serionet:Material;
  customersToShow:CustomerSalesOrg[] = [];
  shipToPartyToShow:CustomerSalesOrg[] = [];
  packagings:PackagingSap[] = [];
  plants:PlantSAP[] = [];
  materialPlants:MaterialPlantSAP[] = [];
  plantsToShow:PlantSAP[] = [];

  query = {
    SalesOrganization:"",
    Customer:"",
    Customers:[],
    ShipToParty:"",
    ShipToParties:[],
    Material:"",
    Plant:"",
    PackInstruction:"",
    organization_serionet:null
  };


  constructor(private salesOrganizationsSAPService:SalesOrganizationSAPService, private salesOrganizationsService:SalesOrganizationService,
              private loginService:LoginService, private legalEntityService:LegalEntityService, private customersService:CustomerService,
              private loadingService:LoadingService, private querySharerService:QuerySharerService, private materialsService:MaterialsService,
              private plantSAPService:PlantSAPService, private packagingSapService:PackagingSapService, private materialPlantsSAPService:MaterialPlantsSAPService
              ) {}

  ngOnInit() {

    this.loadingService.show();
    this.legalEntityService.getLegalEntities({is_active: true}).subscribe((resp) => {
      this.legalEntities = parseSerionetRespToData(resp);
      this.salesOrganizationsSAPService.getSalesOrganizations({}).subscribe((resp) => {
        if (resp.status == 200) {
          let organizations = resp.response.results.filter((o: SalesOrganizationSAP) => o.CustomerLink.results.length > 0);
          organizations = organizations.filter(
            (so) => this.legalEntities.map((le) => le.code).indexOf(so.SalesOrganizationCode) != -1);
          this.salesOrganizations = organizations;
          this.salesOrganizationsService.getSalesOrganizations({is_active:true, user:this.loginService.user.pk}).subscribe((resp)=> {
            this.salesOrganizationsSerionet = parseSerionetRespToData(resp);
            if(this.salesOrganizationsSerionet.length>0){
              this.organizations = this.salesOrganizationsSerionet.map((so) => so.organization);

              let query = this.querySharerService.getQuery();
              if(query){
                this.restoreQuery(query);
              }
              else {
                this.query.organization_serionet = this.organizations[0].pk;
                this.handleOrganizationChange(()=>{
                  this.loadingService.hide();
                  this.query.SalesOrganization = this.salesOrganizationsToShow[0].SalesOrganizationCode;
                  this.handleSalesOrganizationChange(()=>{
                    this.reload();
                  });

                });
              }
            }
            else {
              this.loadingService.hide();
              this.preventAccess.emit();
            }
          });
        }
      });
    });
  }

  restoreQuery(query) {
    if (query.SalesOrganization) {
      this.query.organization_serionet  = query.organization_serionet;
      this.handleOrganizationChange(()=>{
        this.query.SalesOrganization = query.SalesOrganization;
        this.handleSalesOrganizationChange(()=>{
          if (query.Customer)
            this.query.Customer = query.Customer;
          if (query.ShipToParty)
            this.query.ShipToParty = query.ShipToParty;
          if (query.Plant)
            this.query.Plant = query.Plant;
          if (query.Material) {
            this.handleMaterialQueryRestore(query);
          }
          else {
            this.loadingService.hide();
            this.reload();
          }
        });
      })
    }
  }

  preventMaterialEvent = false;

  handleMaterialQueryRestore(query) {
    this.materialsService.getMaterials({code: query.Material}).subscribe((resp) => {
      let ms = parseSerionetRespToData(resp);
      this.preventMaterialEvent = true;
      this.material_serionet = ms[0];
      setTimeout(() => {
        this.preventMaterialEvent = false;
        this.selectedMaterial(this.material_serionet, () =>{
          this.query.Material = query.Material;
          this.loadingService.hide();
          this.reload();
        });
      }, 500)
    })
  }

  handleOrganizationChange(callback?:any) {
    this.query.SalesOrganization = "";
    this.query.Customer = "";
    this.query.ShipToParty = "";
    let so: SalesOrganization = cloneObject(this.salesOrganizationsSerionet.filter((so) => so.organization.pk == this.query.organization_serionet)[0]);
    let le = so.legal_entities;
    if (le.length == 0) {
      le = this.legalEntities;
    }
    else {
      le.forEach((l) => {
        l = this.legalEntities.filter((l_) => l_.pk == l.pk)[0];
      })
    }
    let codes = le.map((l) => l.code);


    this.customersService.getCustomers({
      organization_ref: this.query.organization_serionet,
      is_active: true
    }).subscribe((resp) => {
      this.organizationCustomers = parseSerionetRespToData(resp);
      let customerCodes = this.organizationCustomers.map((c)=>c.sap_code);

      this.salesOrganizationsToShow = this.salesOrganizations.filter((so) =>{
        return codes.indexOf(so.SalesOrganizationCode) != -1 &&
          so.CustomerLink.results.filter((c)=>customerCodes.indexOf(c.Customer)!=-1 && (c.Type=='CUST'||c.Type=='BOTH')).length>0;
      });

      if(callback)
        callback.call(this);
    });
  }

  handleSalesOrganizationChange(callback?:any){
    if(this.query.SalesOrganization){
      let salesOrganization = this.salesOrganizations.filter((so)=>so.SalesOrganizationCode==this.query.SalesOrganization)[0];
      this.customersToShow = [];
      this.query.Customer = "";
      this.query.ShipToParty = "";
      let codes = this.organizationCustomers.map((c)=>c.sap_code);
      this.customersToShow = salesOrganization.CustomerLink.results.filter((c)=>codes.indexOf(c.Customer)!=-1 && (c.Type=='CUST'||c.Type=='BOTH'));
      this.shipToPartyToShow = salesOrganization.CustomerLink.results.filter((c)=>codes.indexOf(c.Customer)!=-1 && (c.Type=='BOTH'||c.Type=='SHIP'));
      this.plants = [];
      this.plantSAPService.getPlants({SalesOrganization:this.query.SalesOrganization}).subscribe((resp)=>{
        if(resp.status==200){
          this.plants = resp.response.results;
          this.checkPlants();
          if(callback)
            callback.call(this);
        }
      })
    }
  }

  selectedMaterial(m: Material, callback?: any) {
    if (this.preventMaterialEvent) return;
    if (m && m.pk) {
      this.query.Material = m.code;
      this.packagingSapService.getPackagings({Material: this.query.Material}).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.status == 200) {
          this.packagings = resp.response.results;
          this.materialPlantsSAPService.getMaterialPlants({MatCode: this.query.Material}).subscribe((resp)=>{
            this.materialPlants = [];
            if(resp.status==200){
              this.materialPlants = resp.response.results;
            }
            this.checkPlants();
            if (callback) {
              callback.call(this);
            }
          })
        }
      });
    }
    else {
      this.query.Material = "";
    }
  }

  checkPlants(){
    this.plantsToShow = [];
    if(this.plants.length>0 && this.materialPlants.length>0){
      let mpCodes = this.materialPlants.map((mp)=>mp.Plant);
      this.plantsToShow = this.plants.filter((p)=>mpCodes.indexOf(p.Code)!=-1);
    }
  }

  reload(){
    if(!this.query.Customer)
     this.query.Customers = cloneObject(this.customersToShow.map((c)=>c.Customer));

    if(!this.query.ShipToParty)
      this.query.ShipToParties = cloneObject(this.shipToPartyToShow.map((c)=>c.Customer));

    this.querySharerService.saveQuery(cloneObject(this.query));

    this.reloadEvent.emit(this.query);
  }

  getQuery(){
    return this.query;
  }
}
