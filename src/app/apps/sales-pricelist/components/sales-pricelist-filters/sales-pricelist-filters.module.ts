import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {SalesPriceListFiltersComponent} from "./sales-pricelist-filters.component";

@NgModule({
  imports: [SharedModule,SerioplastCommonModule],       // module dependencies
  declarations: [
    SalesPriceListFiltersComponent],   // components and directives
  exports:[
    SalesPriceListFiltersComponent],
  providers: []                    // services
})
export class SalesPricelistFiltersModule { }


