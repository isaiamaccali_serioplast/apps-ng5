import {NgModule} from "@angular/core";
import {AppsService} from "./apps.service";
import {SiteFromSAPCodePipe, SitePipe, SitesService} from "./services/sites.service";
import {MaterialCodePipe, MaterialPipe, MaterialsSAPService, MaterialsService} from "./services/materials.service";
import {CavitiesService} from "./services/cavities.service";
import {BatchesService} from "./services/batches.service";
import {PropertyNamesService} from "./services/property-names.service";
import {PropertiesService, PropertyPipe} from "./services/properties.service";
import {TestMethodPipe, TestMethodService} from "./services/test-method.service";
import {MeasureUnitPipe, MeasureUnitService} from "./services/measure-unit.service";
import {PropertySetService} from "./services/property-set.service";
import {FormulasService} from "./services/formulas.service";
import {PropertyTypeService} from "./services/property-type.service";
import {ShapePipe, ShapesService} from "./services/shapes.service";
import {MaterialTypePipe, MaterialTypesService} from "./services/material-type.service";
import {MaterialSubTypePipe, MaterialSubtypesService} from "./services/material-subtype.service";
import {TechnologyLinkService, TechnologyPipe} from "./services/technology-link.service";
import {MainMaterialPipe, MainMaterialsService} from "./services/main-materials.service";
import {SAPService} from "../../shared/sap/sap.service";
import {MaterialSubTypeLinkPipe, MaterialSubtypesLinkService} from "./services/material-subtype-link.service";
import {MarketSegmentLinkPipe, MarketSegmentLinksService} from "./services/market-segment-link.service";
import {MainMaterialLinkPipe, MainMaterialLinksService} from "./services/main-material-links.service";
import {BOMService} from "./services/bom.service";
import {CommodityCodesService} from "./services/commodity-code.service";
import {MaterialPlantsSAPService, MaterialPlantsService} from "./services/material-plant.service";
import {ProductionTypesService} from "./services/production-types.service";
import {LegalEntityService} from "./services/legal-entity.service";
import {TestMethodTypeService} from "./services/test-method-type.service";
import {MarketSegmentPipe, MarketSegmentsService} from "./services/market-segment.service";
import {WorkcentersService} from "./services/workcenters.service";
import {PropertyAttributeService} from "./services/property-attribute.service";
import {OrganizationPipe, OrganizationService} from "./services/organization.service";
import {AssetsService} from "./services/assets.service";
import {WorkcenterLocationsService} from "./services/workcenter-locations.service";
import {HRProfileService} from "./services/hr-profiles.service";
import {AssetTypesService} from "./services/asset-types.service";
import {StatusLinkService} from "./services/status-link.service";
import {TechnologyService} from "./services/technology.service";
import {AssetSubTypeLinkService} from "./services/asset-subtype-link.service";
import {FilePipe, FileService} from "./services/file.service";
import {PackingInstructionsService} from "./services/packing-instructions.service";
import {RoutingsService} from "./services/routings.service";
import {AssetUsersService, AssetUserTypeService} from "./services/asset-user.service";
import {NetworkInterfacesService, NetworkInterfaceTypeService} from "./services/network-interface.service";
import {ContentTypeService} from "./services/content-type.service";
import {PackingInstructionsLocationService} from "./services/packing-instructions-plant.service";
import {
  MaterialOrganizationDocumentService,
  MaterialOrganizationDocumentTypeService
} from "./services/material-organization-document.service";
import {CustomerService} from "./services/customers.service";
import {CustomerTypeService} from "./services/customers-types.service";
import {MaterialCustomerInfoService} from "./services/material-customer-info.service";
import {WorkOrdersService} from "./services/workorders.service";
import {AppPlantUsersService} from "./services/app-plant-users.service";
import {PackagesItemService} from "./services/packages-item.service";
import {PackagesNameService} from "./services/packages-name.service";
import {WorkToolTypeService} from "./services/worktooltype.service";
import {DepartmentService} from "./services/department.service";
import {CompanyPositionHierarchyService} from "./services/company-position-hierarchy.service";
import {OrganizationLevelService} from "./services/organization-level.service";
import {BadgeService} from "./services/badges.service";
import {CountriesService} from "./services/countries.service";
import {EducationService} from "./services/education.service";
import {EducationStudyFieldService} from "./services/education-study-fields.service";
import {EducationTypesService} from "./services/education-type.service";
import {DjangoLanguagesService} from "./services/django-languages.service";
import {LanguageService} from "./services/language.service";
import {NationalityService} from "./services/nationality.service";
import {SexService} from "./services/sex.service";
import {PersonalInfoService} from "./services/personalinfo.service";
import {ContractTypeService} from "./services/contract-type.service";
import {PEFunctionService} from "./services/pefunction.service";
import {PEIndustryService} from "./services/peindustry.service";
import {ProfessionalExperiencesService} from "./services/professional-experiences.service";
import {DjangoTimezoneService} from "./services/django-timezones.service";
import {WorkingContactsService} from "./services/workingcontacts.service";
import {CompanyPositionService} from "./services/company-position.service";
import {PrinterSapService} from "./services/printers-sap.service";
import {BadgeUserRefService} from "./services/badge-user-refs.service";
import {HRUserService} from "./services/hr-users.service";
import {ContractService} from "./services/contracts.service";
import {HasCompanyPositionService} from "./services/has-company-position.service";
import {CompanyPositionNameService} from "./services/company-position-name.service";
import {PriorityLinkService} from "./services/priority-link.service";
import {HcpAdvertisementService} from "./services/hcp-advertisement.service";
import {TagLinkService} from "./services/tag-link.service";
import {HrProfileTagService} from "./services/hr-profile-tag.service";
import {CompanyPositionNameSkillsService} from "./services/company-position-name-skill.service";
import {SkillsService, SkillTypeIconPipe} from "./services/skill.service";
import {SkillTypeService} from "./services/skill-types.service";
import {CurrencyService} from "./services/currency.service";
import {HREventSubtypesService} from "./services/hr-event-subtype.service";
import {HREventSubtypesLinkService} from "./services/hr-event-subtype-link.service";
import {TypeLinkService} from "./services/type-link.service";
import {HrEventService} from "./services/hr-events.service";
import {WorkcenterActivityService} from "./services/workcenter-activity-service";
import {RoutingOperationActivityService} from "./services/routing-operation-activity.service";
import {RoutingsOperationsService} from "./services/routings-operations.service";
import {RoutingProductionVersionService} from "./services/routing-production-version-service";
import {RoutingStatusLinkService} from "./services/routing-status-link.service";
import {HandlingUnitSapService} from "./services/handling-units-sap.service";
import {PackagingSapService} from "./services/packaging-sap.service";
import {ConfirmationSapService} from "./services/confirmations-sap.service";
import {UserOneToOneService} from "./services/user-one-to-one.service";
import {OutputMessageService} from "./services/output-message.service";
import {HandlingUnitsQualitySapService} from "./services/handling-units-quality-sap.service";
import {LabelHandlingUnitsQualitySapService} from "./services/label-hu-quality-sap.service";
import {SalesOrganizationService} from "./services/sales-organization.service";
import {ControlPlanMeasuresService} from "./services/control-plan-measures.service";
import {ControlPlansService} from "./services/control-plans.service";
import {FrequencyService} from "./services/frequency.service";
import {MeasureSessionService} from "./services/measure-session.service";
import {PlantSAPService} from "./services/plant-flow-service";
import {TrainingAreaService} from "./services/training-area.service";
import {TrainingTypeService} from "./services/training-type.service";
import {TrainingService} from "./services/training.service";
import {LocalPackagesService} from "./services/localpackages.service";
import {LocalCpnPackageItemService} from "./services/localcpnpackageitem.service";
import {TrainingUserService} from "./services/training-user.service";
import {ReleaseCriteriaService} from "./services/release-criteria.service";
import {CriteriaService} from "./services/criteria.service";
import {CommentsService} from "./services/comments.service";
import {WarehouseSAPService} from "./services/warehouses.service";
import {FileManagerService} from "./services/file-manager.service";
import {StatusService} from "./services/status.service";
import {SupplierBatchService} from "./services/supplier-batch.service";
import {UserWorktoolService} from "./services/user-worktool.service";
import {WorkcenterStatusMatrixService} from "./services/workcenter-status-matrix.service";
import {ReportService} from "./services/reports.service";
import {UserWorkCalendarService} from "./services/user-work-calendar.service";
import {ClockProfileService} from "./services/clock-profile.service";
import {UserClockProfileService} from "./services/user-clock-profile.service";
import {DayWeekProfileService} from "./services/day-week-profile-service";
import {DayWeekUserMilestoneService} from "./services/day-week-user-milestone-service";
import {WorkorderBatchSapService} from "./services/workorder-batches-sap.service";
import {FactorLevelsService} from "./services/factor-levels.service";
import {CMMParsersService} from "./services/cmm-parsers.service";
import {CMMFileParserService} from "./services/cmm-file-parser.service";
import {UserPipe, UsersService} from "./services/users.service";
import {SupplierPipe, SupplierService} from "./services/suppliers.service";
import {SerionetGenericObjectService} from "./services/serionet-generic-objects.service";
import {ValueConversionService} from "./services/value-conversion.service";
import {BomReferenceService} from "./services/bom-reference.service";
import {RFIDTagService} from "./services/rfid-tag.service";
import {MaterialSalesService} from "./services/material-sales.service";
import {MaterialSupplierService} from "./services/material-supplier.service";
import {MaterialOrganizationService} from "./services/material-organization.service";
import {FileFieldDraftService} from "./services/file-field.service";
import {PurchaseOrganizationsService} from "./services/purchase-organization.service";
import {MaterialGroupsService} from "./services/material-group.service";
import {MaterialStockService} from "./services/material-stock.service";
import {AreasService} from "./services/areas.service";
import {ProjectTasksService} from "./services/project-task.service";
import {WorkordersSerionetService} from "./services/workorders-serionet.service";
import {MaterialInfoService} from "./services/material-info.service";
import {WorkorderLabelSapService} from "./services/workorder-label-sap.service";
import {WorkorderTypeSapService} from "./services/workorder-type-sap.service";
import {SiteEventsService} from "./services/site-events.service";
import {WorkorderRelatedBatchSapService} from "./services/workorder-related-batches-sap.service";
import {WbsCostrevenueSapService} from "./services/wbs-costrevenue-sap.service";

let modules = [PropertyPipe, MaterialPipe, SitePipe, MaterialSubTypePipe, ShapePipe,
  MarketSegmentPipe, MainMaterialPipe, TechnologyPipe, UserPipe, MeasureUnitPipe,
  MarketSegmentLinkPipe, MaterialSubTypeLinkPipe, MainMaterialLinkPipe, OrganizationPipe, TestMethodPipe, SkillTypeIconPipe, FilePipe, SiteFromSAPCodePipe, MaterialTypePipe, MaterialCodePipe, SupplierPipe];

@NgModule({
  declarations: modules,   // components and directives
  providers: [
    AppPlantUsersService,
    AreasService,
    AssetsService,
    AssetSubTypeLinkService,
    AssetTypesService,
    AssetUsersService,
    AssetUserTypeService,
    BadgeService,
    BadgeUserRefService,
    BatchesService,
    BomReferenceService,
    BOMService,
    CavitiesService,
    ClockProfileService,
    CMMFileParserService,
    CMMParsersService,
    ConfirmationSapService,
    CommentsService,
    CommodityCodesService,
    CompanyPositionHierarchyService,
    CompanyPositionNameService,
    CompanyPositionNameSkillsService,
    CompanyPositionService,
    ContentTypeService,
    ContractService,
    ContractTypeService,
    ControlPlanMeasuresService,
    ControlPlansService,
    CountriesService,
    CriteriaService,
    CurrencyService,
    CustomerService,
    CustomerTypeService,
    DayWeekProfileService,
    DayWeekUserMilestoneService,
    DepartmentService,
    DjangoLanguagesService,
    DjangoTimezoneService,
    EducationService,
    EducationStudyFieldService,
    EducationTypesService,
    FactorLevelsService,
    FileService,
    FileManagerService,
    FormulasService,
    FrequencyService,
    FileManagerService,
    FileFieldDraftService,
    SerionetGenericObjectService,
    HandlingUnitsQualitySapService,
    HandlingUnitSapService,
    HasCompanyPositionService,
    HcpAdvertisementService,
    HrEventService,
    HREventSubtypesService,
    HREventSubtypesLinkService,
    HRProfileService,
    HrProfileTagService,
    HRUserService,
    LabelHandlingUnitsQualitySapService,
    LanguageService,
    LegalEntityService,
    LocalCpnPackageItemService,
    LocalPackagesService,
    MainMaterialLinksService,
    MainMaterialsService,
    MarketSegmentLinksService,
    MarketSegmentsService,
    MaterialCustomerInfoService,
    MaterialGroupsService,
    MaterialInfoService,
    MaterialOrganizationService,
    MaterialPlantsService,
    MaterialPlantsSAPService,
    MaterialSalesService,
    MaterialsService,
    MaterialsSAPService,
    MaterialStockService,
    MaterialSubtypesLinkService,
    MaterialSubtypesService,
    MaterialSupplierService,
    MaterialTypesService,
    MeasureSessionService,
    MeasureUnitService,
    NationalityService,
    NetworkInterfacesService,
    NetworkInterfaceTypeService,
    OrganizationService,
    OrganizationLevelService,
    OutputMessageService,
    PackagesItemService,
    PackagesNameService,
    PackagingSapService,
    PackingInstructionsService,
    PackingInstructionsLocationService,
    PlantSAPService,
    PrinterSapService,
    PEFunctionService,
    PEIndustryService,
    PersonalInfoService,
    PriorityLinkService,
    ProductionTypesService,
    ProfessionalExperiencesService,
    ProjectTasksService,
    PropertiesService,
    PropertyAttributeService,
    PropertyNamesService,
    PropertySetService,
    PropertyTypeService,
    PurchaseOrganizationsService,
    ReleaseCriteriaService,
    ReportService,
    RFIDTagService,
    RoutingOperationActivityService,
    RoutingsOperationsService,
    RoutingProductionVersionService,
    RoutingStatusLinkService,
    RoutingsService,
    SalesOrganizationService,
    SAPService,
    SexService,
    ShapesService,
    SiteEventsService,
    SitesService,
    SkillsService,
    SkillTypeService,
    StatusLinkService,
    StatusService,
    SupplierService,
    SupplierBatchService,
    TagLinkService,
    TechnologyService,
    TechnologyLinkService,
    TestMethodService,
    TestMethodTypeService,
    TrainingAreaService,
    TrainingTypeService,
    TrainingService,
    TrainingUserService,
    TypeLinkService,
    UserClockProfileService,
    UserWorkCalendarService,
    UserOneToOneService,
    UsersService,
    UserWorktoolService,
    ValueConversionService,
    WarehouseSAPService,
    WbsCostrevenueSapService,
    WorkcenterActivityService,
    WorkcenterLocationsService,
    WorkcentersService,
    WorkcenterStatusMatrixService,
    WorkingContactsService,
    WorkorderBatchSapService,
    WorkorderLabelSapService,
    WorkorderRelatedBatchSapService,
    WorkorderTypeSapService,
    WorkordersSerionetService,
    WorkOrdersService,
    WorkToolTypeService
  ],                   // services
  exports: modules
})
export class SerioplastCommonModule {
}
