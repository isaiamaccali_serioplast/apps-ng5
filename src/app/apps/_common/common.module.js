"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var sites_service_1 = require("./services/sites.service");
var materials_service_1 = require("./services/materials.service");
var cavities_service_1 = require("./services/cavities.service");
var batches_service_1 = require("./services/batches.service");
var property_names_service_1 = require("./services/property-names.service");
var properties_service_1 = require("./services/properties.service");
var test_method_service_1 = require("./services/test-method.service");
var measure_unit_service_1 = require("./services/measure-unit.service");
var property_set_service_1 = require("./services/property-set.service");
var formulas_service_1 = require("./services/formulas.service");
var property_type_service_1 = require("./services/property-type.service");
var property_pipe_1 = require("./pipes/property.pipe");
var material_pipe_1 = require("./pipes/material.pipe");
var shapes_service_1 = require("./services/shapes.service");
var site_pipe_1 = require("./pipes/site.pipe");
var material_type_service_1 = require("./services/material-type.service");
var material_subtype_service_1 = require("./services/material-subtype.service");
var technology_link_service_1 = require("./services/technology-link.service");
var main_materials_service_1 = require("./services/main-materials.service");
var sap_service_1 = require("../../shared/sap/sap.service");
var shape_pipe_1 = require("./pipes/shape.pipe");
var technology_link_pipe_1 = require("./pipes/technology-link.pipe");
var user_pipe_1 = require("./pipes/user.pipe");
var material_subtype_link_service_1 = require("./services/material-subtype-link.service");
var market_segment_link_service_1 = require("./services/market-segment-link.service");
var market_segment_link_pipe_1 = require("./pipes/market-segment-link.pipe");
var material_subtype_link_pipe_1 = require("./pipes/material-subtype-link.pipe");
var main_material_link_pipe_1 = require("./pipes/main-material-link.pipe");
var main_material_links_service_1 = require("./services/main-material-links.service");
var bom_service_1 = require("./services/bom.service");
var measure_unit_pipe_1 = require("./pipes/measure-unit.pipe");
var commodity_code_service_1 = require("./services/commodity-code.service");
var material_plant_service_1 = require("./services/material-plant.service");
var production_types_service_1 = require("./services/production-types.service");
var legal_entity_service_1 = require("./services/legal-entity.service");
var test_method_type_service_1 = require("./services/test-method-type.service");
var market_segment_service_1 = require("./services/market-segment.service");
var material_subtype_pipe_1 = require("./pipes/material-subtype.pipe");
var main_material_pipe_1 = require("./pipes/main-material.pipe");
var market_segment_pipe_1 = require("./pipes/market-segment.pipe");
var test_method_pipe_1 = require("./pipes/test_method.pipe");
var workcenters_service_1 = require("./services/workcenters.service");
var property_attribute_service_1 = require("./services/property-attribute.service");
var organization_service_1 = require("./services/organization.service");
var assets_service_1 = require("./services/assets.service");
var workcenter_locations_service_1 = require("./services/workcenter-locations.service");
var hr_profiles_service_1 = require("./services/hr-profiles.service");
var asset_types_service_1 = require("./services/asset-types.service");
var status_link_service_1 = require("./services/status-link.service");
var technology_service_1 = require("./services/technology.service");
var asset_subtype_link_service_1 = require("./services/asset-subtype-link.service");
var file_service_1 = require("./services/file.service");
var packing_instructions_service_1 = require("./services/packing-instructions.service");
var routings_service_1 = require("./services/routings.service");
var asset_user_service_1 = require("./services/asset-user.service");
var network_interface_service_1 = require("./services/network-interface.service");
var contenttype_service_1 = require("./services/contenttype.service");
var packing_instructions_plant_service_1 = require("./services/packing-instructions-plant.service");
var material_organization_document_service_1 = require("./services/material-organization-document.service");
var customers_service_1 = require("./services/customers.service");
var customers_types_service_1 = require("./services/customers-types.service");
var material_customer_info_service_1 = require("./services/material-customer-info.service");
var workorders_service_1 = require("./services/workorders.service");
var app_plant_users_service_1 = require("./services/app-plant-users.service");
var packages_item_service_1 = require("./services/packages-item.service");
var packages_name_service_1 = require("./services/packages-name.service");
var worktooltype_service_1 = require("./services/worktooltype.service");
var department_service_1 = require("./services/department.service");
var company_position_hierarchy_service_1 = require("./services/company-position-hierarchy.service");
var organization_level_service_1 = require("./services/organization-level.service");
var badges_service_1 = require("./services/badges.service");
var countries_service_1 = require("./services/countries.service");
var education_service_1 = require("./services/education.service");
var educationstudyfields_service_1 = require("./services/educationstudyfields.service");
var educationtype_service_1 = require("./services/educationtype.service");
var django_languages_service_1 = require("./services/django-languages.service");
var language_service_1 = require("./services/language.service");
var nationality_service_1 = require("./services/nationality.service");
var sex_service_1 = require("./services/sex.service");
var personalinfo_service_1 = require("./services/personalinfo.service");
var contracttype_service_1 = require("./services/contracttype.service");
var pefunction_service_1 = require("./services/pefunction.service");
var peindustry_service_1 = require("./services/peindustry.service");
var professionalexperiences_service_1 = require("./services/professionalexperiences.service");
var django_timezones_service_1 = require("./services/django-timezones.service");
var workingcontacts_service_1 = require("./services/workingcontacts.service");
var company_position_service_1 = require("./services/company-position.service");
var printers_sap_service_1 = require("./services/printers-sap.service");
var badge_user_refs_service_1 = require("./services/badge-user-refs.service");
var hr_users_service_1 = require("./services/hr-users.service");
var contracts_service_1 = require("./services/contracts.service");
var has_company_position_service_1 = require("./services/has-company-position.service");
var company_position_name_service_1 = require("./services/company-position-name.service");
var priority_link_service_1 = require("./services/priority-link.service");
var hcp_advertisement_service_1 = require("./services/hcp-advertisement.service");
var tag_link_service_1 = require("./services/tag-link.service");
var hr_profile_tag_service_1 = require("./services/hr-profile-tag.service");
var company_position_name_skill_service_1 = require("./services/company-position-name-skill.service");
var skill_service_1 = require("./services/skill.service");
var skill_types_service_1 = require("./services/skill-types.service");
var currency_service_1 = require("./services/currency.service");
var skill_type_icon_pipe_1 = require("./pipes/skill-type-icon.pipe");
var hr_event_subtype_service_1 = require("./services/hr-event-subtype.service");
var hr_event_subtype_link_service_1 = require("./services/hr-event-subtype-link.service");
var type_link_service_1 = require("./services/type-link.service");
var hr_events_service_1 = require("./services/hr-events.service");
var workcenter_activity_service_1 = require("./services/workcenter-activity-service");
var routing_operation_activity_service_1 = require("./services/routing-operation-activity.service");
var routings_operations_service_1 = require("./services/routings-operations.service");
var routing_production_version_service_1 = require("./services/routing-production-version-service");
var routing_status_link_service_1 = require("./services/routing-status-link.service");
var warehouses_service_1 = require("./services/warehouses.service");
var handling_units_sap_service_1 = require("./services/handling-units-sap.service");
var packaging_sap_service_1 = require("./services/packaging-sap.service");
var batches_sap_service_1 = require("./services/batches-sap.service");
var component_sap_service_1 = require("./services/component-sap.service");
var confirmations_sap_service_1 = require("./services/confirmations-sap.service");
var operations_sap_service_1 = require("./services/operations-sap.service");
var user_one_to_one_service_1 = require("./services/user-one-to-one.service");
var output_message_service_1 = require("./services/output-message.service");
var handling_units_quality_sap_service_1 = require("./services/handling-units-quality-sap.service");
var label_hu_quality_sap_service_1 = require("./services/label-hu-quality-sap.service");
var control_plan_measures_service_1 = require("./services/control-plan-measures.service");
var control_plans_service_1 = require("./services/control-plans.service");
var frequency_service_1 = require("./services/frequency.service");
var measure_session_service_1 = require("./services/measure-session.service");
var modules = [property_pipe_1.PropertyPipe, material_pipe_1.MaterialPipe, site_pipe_1.SitePipe, material_subtype_pipe_1.MaterialSubTypePipe, shape_pipe_1.ShapePipe,
    market_segment_pipe_1.MarketSegmentPipe, main_material_pipe_1.MainMaterialPipe, technology_link_pipe_1.TechnologyPipe, user_pipe_1.UserPipe, measure_unit_pipe_1.MeasureUnitPipe,
    market_segment_link_pipe_1.MarketSegmentLinkPipe, material_subtype_link_pipe_1.MaterialSubTypeLinkPipe, main_material_link_pipe_1.MainMaterialLinkPipe, test_method_pipe_1.TestMethodPipe, skill_type_icon_pipe_1.SkillTypeIconPipe];
var SerioplastCommonModule = (function () {
    function SerioplastCommonModule() {
    }
    return SerioplastCommonModule;
}());
SerioplastCommonModule = __decorate([
    core_1.NgModule({
        declarations: modules,
        providers: [
            app_plant_users_service_1.AppPlantUsersService,
            assets_service_1.AssetsService,
            asset_subtype_link_service_1.AssetSubTypeLinkService,
            asset_types_service_1.AssetTypesService,
            asset_user_service_1.AssetUsersService,
            asset_user_service_1.AssetUserTypeService,
            badges_service_1.BadgeService,
            badge_user_refs_service_1.BadgeUserRefService,
            batches_sap_service_1.BatchSapService,
            batches_service_1.BatchesService,
            bom_service_1.BOMService,
            cavities_service_1.CavitiesService,
            confirmations_sap_service_1.ConfirmationSapService,
            commodity_code_service_1.CommodityCodesService,
            company_position_hierarchy_service_1.CompanyPositionHierarchyService,
            company_position_name_service_1.CompanyPositionNameService,
            company_position_name_skill_service_1.CompanyPositionNameSkillsService,
            company_position_service_1.CompanyPositionService,
            component_sap_service_1.ComponentSapService,
            contenttype_service_1.ContentTypeService,
            contracts_service_1.ContractService,
            contracttype_service_1.ContractTypeService,
            control_plan_measures_service_1.ControlPlanMeasuresService,
            control_plans_service_1.ControlPlansService,
            countries_service_1.CountriesService,
            currency_service_1.CurrencyService,
            customers_service_1.CustomerService,
            customers_types_service_1.CustomerTypeService,
            department_service_1.DepartmentService,
            django_languages_service_1.DjangoLanguagesService,
            django_timezones_service_1.DjangoTimezoneService,
            education_service_1.EducationService,
            educationstudyfields_service_1.EducationStudyFieldService,
            educationtype_service_1.EducationTypesService,
            file_service_1.FileService,
            formulas_service_1.FormulasService,
            frequency_service_1.FrequencyService,
            handling_units_quality_sap_service_1.HandlingUnitsQualitySapService,
            handling_units_sap_service_1.HandlingUnitSapService,
            has_company_position_service_1.HasCompanyPositionService,
            hcp_advertisement_service_1.HcpAdvertisementService,
            hr_events_service_1.HrEventService,
            hr_event_subtype_service_1.HREventSubtypesService,
            hr_event_subtype_link_service_1.HREventSubtypesLinkService,
            hr_profiles_service_1.HRProfileService,
            hr_profile_tag_service_1.HrProfileTagService,
            hr_users_service_1.HRUserService,
            label_hu_quality_sap_service_1.LabelHandlingUnitsQualitySapService,
            language_service_1.LanguageService,
            legal_entity_service_1.LegalEntityService,
            main_material_links_service_1.MainMaterialLinksService,
            main_materials_service_1.MainMaterialsService,
            market_segment_link_service_1.MarketSegmentLinksService,
            market_segment_service_1.MarketSegmentsService,
            material_customer_info_service_1.MaterialCustomerInfoService,
            material_organization_document_service_1.MaterialOrganizationDocumentService,
            material_organization_document_service_1.MaterialOrganizationDocumentTypeService,
            material_plant_service_1.MaterialPlantsService,
            materials_service_1.MaterialsService,
            material_subtype_link_service_1.MaterialSubtypesLinkService,
            material_subtype_service_1.MaterialSubtypesService,
            material_type_service_1.MaterialTypesService,
            measure_session_service_1.MeasureSessionService,
            measure_unit_service_1.MeasureUnitService,
            nationality_service_1.NationalityService,
            network_interface_service_1.NetworkInterfacesService,
            network_interface_service_1.NetworkInterfaceTypeService,
            operations_sap_service_1.OperationSapService,
            organization_service_1.OrganizationService,
            organization_level_service_1.OrganizationLevelService,
            output_message_service_1.OutputMessageService,
            packages_item_service_1.PackagesItemService,
            packages_name_service_1.PackagesNameService,
            packaging_sap_service_1.PackagingSapService,
            packing_instructions_service_1.PackingInstructionsService,
            packing_instructions_plant_service_1.PackingInstructionsLocationService,
            printers_sap_service_1.PrinterSapService,
            pefunction_service_1.PEFunctionService,
            peindustry_service_1.PEIndustryService,
            personalinfo_service_1.PersonalInfoService,
            priority_link_service_1.PriorityLinkService,
            production_types_service_1.ProductionTypesService,
            professionalexperiences_service_1.ProfessionalExperiencesService,
            properties_service_1.PropertiesService,
            property_attribute_service_1.PropertyAttributeService,
            property_names_service_1.PropertyNamesService,
            property_set_service_1.PropertySetService,
            property_type_service_1.PropertyTypeService,
            routing_operation_activity_service_1.RoutingOperationActivityService,
            routings_operations_service_1.RoutingsOperationsService,
            routing_production_version_service_1.RoutingProductionVersionService,
            routing_status_link_service_1.RoutingStatusLinkService,
            routings_service_1.RoutingsService,
            sap_service_1.SAPService,
            sex_service_1.SexService,
            shapes_service_1.ShapesService,
            sites_service_1.SitesService,
            skill_service_1.SkillsService,
            skill_types_service_1.SkillTypeService,
            status_link_service_1.StatusLinkService,
            tag_link_service_1.TagLinkService,
            technology_service_1.TechnologyService,
            technology_link_service_1.TechnologyLinkService,
            test_method_service_1.TestMethodService,
            test_method_type_service_1.TestMethodTypeService,
            type_link_service_1.TypeLinkService,
            user_one_to_one_service_1.UserOneToOneService,
            warehouses_service_1.WarehouseSAPService,
            workcenter_activity_service_1.WorkcenterActivityService,
            workcenter_locations_service_1.WorkcenterLocationsService,
            workcenters_service_1.WorkcentersService,
            workingcontacts_service_1.WorkingContactsService,
            workorders_service_1.WorkOrdersService,
            worktooltype_service_1.WorkToolTypeService
        ],
        exports: modules
    })
], SerioplastCommonModule);
exports.SerioplastCommonModule = SerioplastCommonModule;
