import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {HRUser} from "./hr-users.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Badge} from "./badges.service";
import {DjangoLanguage} from "./django-languages.service";

export class BadgeUserRef extends GenericObj{
  user: HRUser;
  external_name: string;
  external_doc_id: string;
  badge: Badge;
  start_date: string;
  end_date: string;
  language: DjangoLanguage;
  notes:string;
}

@Injectable()
export class BadgeUserRefService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "BadgeUserRef";
    this.serviceURL = "salaries/badge-user-refs/";
  }

  getBadgeUserRefs(query: any) {
    return this.getList(query);
  }

  getBadgeUserRef(id: number) {
    return this.getItem(id);
  }

  deleteBadgeUserRef(item:BadgeUserRef){
    return this.deleteItem(item);
  }

  saveBadgeUserRef(item:BadgeUserRef){
    return this.saveItem(item);
  }

}
