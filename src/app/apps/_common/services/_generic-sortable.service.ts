import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {cloneObject} from "../../../shared/utils";
import {GenericObj, GenericService} from "./_generic.service";
import {HttpParams} from "@angular/common/http";

export class GenericSortableObj extends GenericObj{
  delta?:number;
  is_first?:boolean = false;
  is_last?:boolean = false;
}

@Injectable()
export class GenericSortableService extends GenericService {

  protected entityName = "";
  protected serviceURL = "";
  protected preserveObject = false;

  protected constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
  }

  moveUpItem(item: any) {
    return this.moveItem(item, "up");
  }

  moveDownItem(item: any) {
    return this.moveItem(item, "down");
  }

  moveTo(item:any,position:number, timeout?:number){
    return Observable.create((observer) => {
      let urlParams = new HttpParams();

      urlParams = urlParams.set("move_to", position+"");

      let i_;
      if (this.preserveObject) {
        i_ = cloneObject(item);
      }
      else {
        i_ = GenericService.processItemForSave(this.clearItem(item));
      }

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;

      if (i_.pk) {
        this.serionetService.doPut(this.serviceURL + i_.pk, i_, serionetOptions).subscribe((data) => {
          observer.next(data);
          observer.complete();
        }, (response) => {
          this.loggingService.error("Error in moving to position" + position + " " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        });
      }
      else {
        observer.next({status: 10000, message: "Unmovable object", context: "SERIONET"});
        observer.complete();
      }
    });
  }

  moveItem(item: any, direction: string, timeout?:number) {
    return Observable.create((observer) => {
      let urlParams = new HttpParams();

      urlParams = urlParams.set("move", direction);

      let i_;
      if (this.preserveObject) {
        i_ = cloneObject(item);
      }
      else {
        i_ = GenericService.processItemForSave(this.clearItem(item));
      }

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;


      if (i_.pk) {
        this.serionetService.doPut(this.serviceURL + i_.pk, i_, serionetOptions).subscribe((data) => {
          observer.next(data);
          observer.complete();
        }, (response) => {
          this.loggingService.error("Error in moving " + direction + " " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        });
      }
      else {
        observer.next({status: 10000, message: "Unmovable object", context: "SERIONET"});
        observer.complete();
      }
    });
  }

}




