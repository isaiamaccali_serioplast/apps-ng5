import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {PackageName} from "./packages-name.service";
import {ContentType} from "./content-type.service";
import {WorkToolType} from "./worktooltype.service";
import {GenericObj, GenericService} from "./_generic.service";

export class PackageItem extends GenericObj{
  package_name: PackageName;
  content_type: ContentType;
  object_id:number;
  type:WorkToolType;
  mandatory:Boolean;

  //dummy fields
  status:string;
}

@Injectable()
export class PackagesItemService extends GenericService{

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
  }

  init() {
    this.entityName = "Packages Items";
    this.serviceURL = "packages/package/";
  }

  getPackagesItems(query:any){
    return this.getList(query);
  }

  savePackageItem(packageItem: PackageItem) {
   return this.saveItem(packageItem);
  }

  removePackageItem(packageItem: PackageItem) {
    return this.deleteItem(packageItem)
  }
}



