import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Material} from "./materials.service";
import {MeasureUnit} from "./measure-unit.service";


export class MaterialType extends GenericObj{
  first_char?: string;
  create?: boolean;
  sap_code?: string;
  content_type: number;
  bom_quantity: number;
  default_uom?:MeasureUnit;
}

export let _MATERIAL_TYPES = {
  RESIN: 'RESIN',
  ADDITIVE: 'ADDITIVE',
  DECORATION: 'DECORATION',
  PACKAGING: 'PACKAGING',
  PREFORM: 'PREFORM',
  BOTTLE: 'BOTTLE',
  CLOSURE: 'CLOSURE',
  COMPONENT: 'COMPONENT',
  MOULDSET: 'MOULDSET',
  BILLET: 'BILLET',
  RETURNABLE_PACKAGING: 'RETURNABLE_PACKAGING',
  REGRIND: 'REGRIND',
  ASSEMBLED_GOODS: 'ASSEMBLED_GOODS'
};

export const _getMaterialType = (m: Material)=> {
  let sap_code = m.material_type.sap_code;
  switch (sap_code) {
    case 'ROH':
      return _MATERIAL_TYPES.RESIN;
    case 'ZADD':
      return _MATERIAL_TYPES.ADDITIVE;
    case 'ZETI':
      return _MATERIAL_TYPES.DECORATION;
    case 'VERP':
      return _MATERIAL_TYPES.PACKAGING;
    case 'HALB':
      return _MATERIAL_TYPES.PREFORM;
    case 'ZBOT':
      return _MATERIAL_TYPES.BOTTLE;
    case 'FERT':
      return _MATERIAL_TYPES.CLOSURE;
    case 'ZIBE':
      return _MATERIAL_TYPES.COMPONENT;
    case 'ZFHM':
      return _MATERIAL_TYPES.MOULDSET;
    case 'ZBIL':
      return _MATERIAL_TYPES.BILLET;
    case 'ZEIH':
      return _MATERIAL_TYPES.RETURNABLE_PACKAGING;
    case 'ZSC1':
      return _MATERIAL_TYPES.REGRIND;
    case 'ZASS':
      return _MATERIAL_TYPES.ASSEMBLED_GOODS;
    default:
      return null;
  }
}

export const isBottleClosurePreform = (m: Material) =>{
  let matTypeConst = _getMaterialType(m);
  return matTypeConst == _MATERIAL_TYPES.PREFORM || matTypeConst == _MATERIAL_TYPES.BOTTLE || matTypeConst == _MATERIAL_TYPES.CLOSURE;
}

@Injectable()
export class MaterialTypesService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Material Types";
    this.serviceURL = "materials/materialindex/";
  }

  getMaterialTypes(query:any) {
   return this.getList(query);
  }

  getMaterialType(id:number){
    return this.getItem(id, true);
  }
}

@Pipe({
  name: 'material_type'
})
export class MaterialTypePipe implements PipeTransform{

  constructor(private materialTypesService:MaterialTypesService){}

  transform(id:number) {
    return this.materialTypesService.getMaterialType(id);
  }
}



