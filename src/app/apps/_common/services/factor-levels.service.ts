import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {GroupingFactor} from "./measure-session.service";
import {cloneObject} from "../../../shared/utils";

export class FactorLevel extends GenericObj  {
  name:string;
  active:boolean = true;
  grouping_factor?:GroupingFactor;

  inUse?:boolean;
  inEdit?:boolean;
  originalName:string;
}

@Injectable()
export class FactorLevelsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Factor Level";
    this.serviceURL = "qc/factor-levels/";
  }

  saveFactorLevel(i:FactorLevel){
    return this.saveItem(i);
  }

  clearItem(i:FactorLevel){
    let i_:FactorLevel = cloneObject(i);
    delete i_.inUse;
    delete i_.inEdit;
    delete i_.originalName;
    return i_;
  }
}


