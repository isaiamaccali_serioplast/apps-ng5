import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {MaterialType} from "./material-type.service";
import {Material} from "./materials.service";
import {Organization} from "./organization.service";
import {TechnologyLink} from "./technology-link.service";
import {MaterialSubTypeLink} from "./material-subtype-link.service";

export class Shape extends GenericObj{
  url?:string;
  material_list?:Material[];
  code:string;
  material_type?:MaterialType;
  technology?:TechnologyLink;
  plm_revision?:string;
  alternative_name?:ShapeAlternativeName[];
  description?:string;
  owner?:Organization;
  notes?:string;
  subtype?:MaterialSubTypeLink;
  long_description?:string;

  alternative_name_plain?:string;
}

export class ShapeAlternativeName{
  pk?: number;
  __str__?: string;
  is_active?:boolean;
  label:string;
}

@Injectable()
export class ShapesService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Shape";
    this.serviceURL = "materials/shapes/";
  }

  getShapes(query:any) {
    return this.getList(query);
  }

  getShape(id:number) {
    return this.getItem(id);
  }

  saveShape(s:Shape){
    return this.saveItem(s,["alternative_name"]);
  }
}

@Pipe({
  name: 'shape'
})
export class ShapePipe implements PipeTransform{

  constructor(private shapeService:ShapesService){}

  transform(id:number) {
    return this.shapeService.getShape(id);
  }
}



