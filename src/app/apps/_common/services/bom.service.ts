import {Injectable} from "@angular/core";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";
import {cloneObject} from "../../../shared/utils";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Site, SitesService} from "./sites.service";
import {Material} from "./materials.service";
import {MeasureUnit} from "./measure-unit.service";

export class BOM extends GenericObj {
  material_id?: Material;
  bomitem_set?: BOMItem[];
  sites?: Site[];
  alt_bom_id?: string;
  bom_quantity: number;

  //fake fields
  enable_add_item: boolean;
  original_sites?: Site[];
  bom_sites?: BOMSite[];
}

export class BOMItem extends GenericObj {
  material?: Material;
  quantity?: number;
  uom?: MeasureUnit;
  group?: number;
}

export class BOMItemSAP {
  Code: string;
  Plant: string;
  Alternative: string;
  Item: string;
  Component: string;
  Quantity: string;
  Uom: string;
}

//Classe finta di appoggio
export class BOMSite {
  site: Site;
  status: string;
  active: boolean;
  disabled: boolean;
}

export class BOMHeaderSAP {
  Code: string;
  Plant: string;
  Alternative: string;
  Description: string;
  BOMLink: BOMItemSAP[];
  BaseQuantity: string;

  getId() {
    return "Code='" + this.Code + "',Plant='" + this.Plant + "',Alternative='" + this.Alternative + "'";
  }
}

@Injectable()
export class BOMService extends GenericService {

  entityName = "BOM";
  serviceNameSAP = "ZMATERIALS_SRV";
  entityNameSAP = 'BOMHeaderSet';

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService,
              protected sapService: SAPService, private sitesService: SitesService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "BoM";
    this.serviceURL = "materials/boms/";
  }

  getBOMs(query: any) {
    return this.getList(query);
  }

  getBOM(id: number, revisionDate?: string, params?: any) {
    return this.getItem(id, false, revisionDate, params);
  }

  saveBOM(b: BOM) {

    return Observable.create((observer) => {

      let bToSave = cloneObject(b);

      this.processBOMForSAP(bToSave, (response) => {
        if (response.status >= 200 && response.status < 300) {
          bToSave.bomitem_set.map((element, index) => {
            if (element.pk) {
              delete element['pk'];
            }
            element.material = element.material.pk;
            element.uom = element.uom.pk;
            bToSave.bomitem_set[index] = element;
          });

          bToSave.sites = b.bom_sites.filter((bs: BOMSite) => bs.active).map((bs: BOMSite) => bs.site.pk);

          bToSave.material_id = bToSave.material_id.pk;

          let serionetOptions = new SerionetRequestOptions();
          serionetOptions.options = {};

          this.serionetService.doPost("materials/boms/", bToSave, serionetOptions).subscribe((response) => {
            this.loggingService.info("OK: " + JSON.stringify(response));
            observer.next(response);
            observer.complete();
          }, (response) => {
            this.loggingService.error("Error: " + JSON.stringify(response));
            response.context = "SERIONET";
            observer.next(response);
            observer.complete();
          })
        }
        else {
          observer.next({status: response.status, message: response, context: 'SAP'});
          observer.complete();
        }
      });
    });
  }

  processBOMForSAP(b: BOM, callback) {
    let sitesToSave = b.bom_sites.filter((bs: BOMSite) => bs.active).map((bs: BOMSite) => bs.site);
    let sitesToDelete = b.bom_sites.filter((bs: BOMSite) => bs.status == "toDelete").map((bs: BOMSite) => bs.site);

    this.fixSites(sitesToSave, () => {
      let bomsToSave = [];
      for (let site of sitesToSave) {
        if (site.active_on_sap) {
          let bHeader = new BOMHeaderSAP();
          bHeader.Code = b.material_id.code;
          bHeader.Plant = site.code;
          bHeader.Alternative = b.alt_bom_id;
          bHeader.Description = b.material_id.__str__ + "/" + bHeader.Alternative;
          if (bHeader.Description.length > 40)
            bHeader.Description = bHeader.Description.substring(0, 40);
          bHeader.BOMLink = [];
          bHeader.BaseQuantity = b.material_id.material_type.bom_quantity + "";

          let bItems = b.bomitem_set;
          for (let bItem of bItems) {
            let bItemSAP = new BOMItemSAP();
            bItemSAP.Code = bHeader.Code;
            bItemSAP.Plant = bHeader.Plant;
            bItemSAP.Alternative = bHeader.Alternative;
            // bItemSAP.Item = ((l * 10) + 10)+"";
            bItemSAP.Component = bItem.material.code;
            bItemSAP.Quantity = bItem.quantity + "";
            bItemSAP.Uom = bItem.uom.__str__;
            bHeader.BOMLink.push(bItemSAP);
          }
          bomsToSave.push(bHeader);
        }
      }
      this.saveBOMSOnSAP(bomsToSave, 0, (response) => {
        if (sitesToDelete.length == 0) {
          callback.call(null, response)
        }
        else {
          this.fixSites(sitesToDelete, () => {
            let bomsToDelete = [];
            for (let site of sitesToDelete) {
              if (site.active_on_sap) {
                let bHeader = new BOMHeaderSAP();
                bHeader.Code = b.material_id.code;
                bHeader.Plant = site.code;
                bHeader.Alternative = b.alt_bom_id;
                bomsToDelete.push(bHeader);
              }
            }
            this.deleteBOMSFromSAP(bomsToDelete, 0, (response) => {
              callback.call(null, response)
            })
          });
        }
      })
    });
  }

  fixSites(sites: Site[], callback) {
    this.fixSite(sites, 0, callback);
  }

  fixSite(sites, index, callback) {
    if (sites[index]) {
      this.sitesService.getSite(sites[index].pk).subscribe((site) => {
        sites[index] = site;
        this.fixSite(sites, index + 1, callback);
      })
    }
    else {
      callback.call(null);
    }
  }

  saveBOMSOnSAP(boms, index, callback, lastResponse?) {
    if (boms[index]) {
      this.sapService.createItem(this.serviceNameSAP, this.entityNameSAP, null, boms[index]).subscribe((response) => {
        if (response.status >= 200 && response.status < 300) {
          this.saveBOMSOnSAP(boms, index + 1, callback, response);
        }
        else {
          this.loggingService.error("Error in adding BOM on SAP: " + JSON.stringify(boms[index]));
          callback.call(null, response);
        }
      })
    }
    else {
      if (!lastResponse) {
        lastResponse = {status: 204};
      }
      callback.call(null, lastResponse);
    }
  }

  deleteBOMSFromSAP(boms, index, callback, lastResponse?) {
    if (boms[index]) {
      this.sapService.getItem(this.serviceNameSAP, this.entityNameSAP, null, boms[index].getId()).subscribe((response) => {
        if (response.status == 200) {
          this.sapService.deleteItem(this.serviceNameSAP, this.entityNameSAP, null, boms[index].getId()).subscribe((response_) => {
            if (response_.status >= 200 && response_.status < 300) {
              this.deleteBOMSFromSAP(boms, index + 1, callback, response_);
            }
            else {
              this.loggingService.error("Error in deleting BOM on SAP: " + JSON.stringify(boms[index]));
              callback.call(null, response_);
            }
          })
        }
        else {
          this.deleteBOMSFromSAP(boms, index + 1, callback);
        }
      });
    }
    else {
      if (!lastResponse) {
        lastResponse = {status: 204};
      }
      callback.call(null, lastResponse);
    }
  }

  deleteBOM(b: BOM) {
    return Observable.create((observer) => {

      let bToDelete = cloneObject(b);

      this.deleteBOMFromSAP(bToDelete, (response) => {
        if (response.status >= 200 && response.status < 300) {
          this.deleteItem(b).subscribe((resp) => {
            observer.next(resp);
            observer.complete();
          })
        }
        else {
          observer.next({status: response.status, message: response, context: 'SAP'});
          observer.complete();
        }
      });
    });
  }

  deleteBOMFromSAP(b: BOM, callback: any) {
    let sites = b.sites;
    this.fixSites(sites, () => {
      let bomsToDelete = [];
      for (let site of sites) {
        let bHeader = new BOMHeaderSAP();
        bHeader.Code = b.material_id.code;
        bHeader.Plant = site.code;
        bHeader.Alternative = b.alt_bom_id;
        bomsToDelete.push(bHeader);
      }
      this.deleteBOMSFromSAP(bomsToDelete, 0, (response) => {
        callback.call(null, response);
      })
    })
  }
}
