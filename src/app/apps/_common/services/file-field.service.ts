import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

// for get autosave field contents (eg. description of BaseDoc models)
export class FileFieldAutoSave {
  last_draft:string;
  last_saved:string;
}

// for update draft file
export class FileFieldDraft {
  content_type:number;
  object_id:number;
  fieldname:string;
  last_draft:string;
}

@Injectable()
export class FileFieldDraftService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "FileFieldDraft";
    this.serviceURL = "files/file-field-draft/";
  }

  updateFileFieldDraft(f:FileFieldDraft) {
    return this.saveItem(f);
  }

}
