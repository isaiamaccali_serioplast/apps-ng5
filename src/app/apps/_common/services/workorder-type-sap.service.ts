import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

declare let sap: any;

export class WorkorderTypeSAP {
  Plant:string;
  Description:string;
  Type:string;
  Sampling:boolean;
}

@Injectable()
export class WorkorderTypeSapService extends GenericSAPService {
  serviceName_SAP = "zprodplanning_srv";
  entitySetName_SAP = "OrderTypeSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  getTypes(query){
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.Plant != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(filter);
        }
        if (query.Sampling != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Sampling',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Sampling']
          });
          filters.push(filter);
        }
        params['filters'] = filters;
      }

      this.getList(params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}



