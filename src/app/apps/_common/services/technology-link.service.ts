import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {Technology} from "./technology.service";

export class TechnologyLink extends GenericObj {
  technology_id?:Technology;
  sap_hierarchy?:string;
  delta_rel?:number;
  fieldname?:string;
  app?:GenericObj;
  has_mouldset?:boolean;
}

@Injectable()
export class TechnologyLinkService extends GenericService {

  fieldName = "technology";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Technology";
    this.serviceURL = "statuses/technologylink/";
  }

  getTechnologyLinks(query: any) {
    if (!query)
      query = {};
    query['fieldname'] = this.fieldName;
    return this.getList(query);
  }

  getTechnologyLink(id: number) {
    return this.getItem(id);
  }
}

@Pipe({
  name: 'technologyLink'
})
export class TechnologyPipe implements PipeTransform{

  constructor(private technologyLinkService:TechnologyLinkService){}

  transform(id:number) {
    return this.technologyLinkService.getTechnologyLink(id);
  }
}



