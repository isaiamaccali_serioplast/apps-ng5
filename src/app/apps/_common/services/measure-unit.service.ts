import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class MeasureUnit extends GenericObj{
  uom?:string;
  type?:string;
}

@Injectable()
export class MeasureUnitService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "SampleMeasure Units";
    this.serviceURL = "statuses/uom/";
  }

  getMeasureUnits(query:any) {
    return this.getList(query);
  }

  getMeasureUnit(id:number){
    return this.getItem(id, true);
  }
}

@Pipe({
  name: 'measureUnit'
})
export class MeasureUnitPipe implements PipeTransform{

  constructor(private measureUnitService:MeasureUnitService){}

  transform(id:number) {
    return this.measureUnitService.getMeasureUnit(id);
  }
}



