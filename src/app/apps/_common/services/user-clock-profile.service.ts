import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {ClockProfile} from "./clock-profile.service";
import {GenericObj, GenericService} from "./_generic.service";
import {HRUser} from "./hr-users.service";

export class UserClockProfile extends GenericObj{
  user:HRUser;
  clock_profile:ClockProfile
}

@Injectable()
export class UserClockProfileService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "UserClockProfile";
    this.serviceURL = "salaries/user-clock-profiles/";
  }

  getUserClockProfiles(query: any) {
    return this.getList(query);
  }

  saveUserClockProfile(i:UserClockProfile){
    return this.saveItem(i);
  }

  deleteUserClockProfile(i:UserClockProfile){
    return this.deleteItem(i);
  }

  restoreUserClockProfile(i:UserClockProfile){
    return this.restoreItem(i);
  }

  getUserClockProfile(id: number) {
    return this.getItem(id);
  }
}


