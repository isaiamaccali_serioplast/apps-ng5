import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Observable} from "rxjs";

export class Skill extends GenericObj{
  description?:string;
  type?:string;
  name?:string;
}

@Injectable()
export class SkillsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Skill";
    this.serviceURL = "hrpositions/skills/"
  }

  getSkills(query?: any) {
    return this.getList(query);
  }

  getSkill(id:number) {
    return this.getItem(id);
  }

  deleteSkill(item:Skill) {
    return this.deleteItem(item);
  }

  saveSkill(item:Skill) {
    return this.saveItem(item);
  }

}

@Pipe({
  name: 'skillTypeIcon'
})
export class SkillTypeIconPipe implements PipeTransform{

  constructor(private skillsService:SkillsService){}

  transform(skill:number) {
    return Observable.create((observer)=>{
      this.skillsService.getSkill(skill).subscribe((s:Skill)=>{
        let skillTypeIcon = "";
        switch (s.type) {
          case 'Technical':
            skillTypeIcon = "<i class='material-icons'>build</i>";
            break;
          case 'Soft':
            skillTypeIcon = "<i class='material-icons'>person</i>";
            break;
          default:
            break;
        }
        observer.next(skillTypeIcon);
        observer.complete();
      });
    });
  }
}




