import {Injectable} from "@angular/core";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Skill} from "./skill.service";
import {CompanyPositionName} from "./company-position-name.service";
import {PriorityLink} from "./priority-link.service";

export class CompanyPositionNameSkill extends GenericObj {
  skill?: Skill;
  company_position_name?: CompanyPositionName;
  level?:PriorityLink;
}

@Injectable()
export class CompanyPositionNameSkillsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "CompanyPositionNameSkill";
    this.serviceURL = "hrpositions/company-position-name-skills/"
  }

  getSkills(query?: any) {
    return this.getList(query);
  }

  getCompanyPositionNameSkill(id: number) {
    return this.getItem(id);
  }

  saveCompanyPositionNameSkill(cpn:CompanyPositionNameSkill){
    return this.saveItem(cpn);
  }

  deleteCompanyPositionNameSkill(cpn:CompanyPositionNameSkill){
    return this.deleteItem(cpn);
  }

  restoreCompanyPositionNameSkill(cpn:CompanyPositionNameSkill){
    return this.saveItem(cpn);
  }

}



