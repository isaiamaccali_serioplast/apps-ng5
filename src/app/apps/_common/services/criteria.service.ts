import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class Criteria extends GenericObj {
  nr_of_parameters?:number;
}

@Injectable()
export class CriteriaService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Criteria";
    this.serviceURL = "qc/criteria/";
  }

  getCriterias(query: any) {
    return this.getList(query);
  }


  getCriteria(id: number) {
    return this.getItem(id);
  }
}


