import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class  ContentType extends GenericObj {
  app_label:string;
  model:string;
}

@Injectable()
export class ContentTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "ContentType";
    this.serviceURL = "core/contenttypes/";
  }

  getContentTypes(query: any) {
    return this.getList(query);
  }
}
