import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class PEIndustry extends GenericObj{

}


@Injectable()
export class PEIndustryService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "PEIndustry";
    this.serviceURL = "hrprofiles/peindustry/";
  }

  getPEIndustries(query:any){
    return this.getList(query);
  }

}



