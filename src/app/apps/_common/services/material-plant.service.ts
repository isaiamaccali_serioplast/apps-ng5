import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Site, SitesService} from "./sites.service";
import {cloneObject} from "../../../shared/utils";
import {GenericSAPService} from "./_generic_sap_service";
import {Material} from "./materials.service";
import {ProductionType} from "./production-types.service";

export class MaterialPlant extends GenericObj{
  material_id: Material;
  inspection_stock: boolean;
  mes: boolean;
  commodity_code: string;
  site: Site;
  production_type: ProductionType;
  disableCommodityCode?: boolean;

  status:string;
}

export class MaterialPlantSAP {
  MatCode:string;
  Plant:string;
  ProductionType:string;
  CommodityCode:string;
  InspectionStock:boolean;
  MES:boolean;
  MainMaterial:string;
  Subtype:string;
  Type:string;

  getId = () => {
    return "MatCode='"+this.MatCode+"',Plant='"+this.Plant+"'";
  }
}

export const allowsBOMCreation = (mp:MaterialPlant)=>{
  return mp.production_type.delta_rel==2||mp.production_type.delta_rel==4;
};

@Injectable()
export class MaterialPlantsService extends GenericService {

  entityName = "MATERIALPLANT";
  serviceURLSerionet = "materials/materialsplant/";

  serviceNameSAP = "ZMATERIALS_SRV";
  entityNameSAP = "MatPlantsSet";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, private sapService: SAPService, private sitesService: SitesService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Material Plant";
    this.serviceURL = this.serviceURLSerionet;
  }


  getMaterialPlants(query: any) {
    return this.getList(query);
  }

  getMaterialPlant(id: number) {
    return this.getItem(id);
  }

  saveMaterialPlant(mp: MaterialPlant) {
    return Observable.create((observer) => {

      let saveFunc = () => {
        let mpSAP = MaterialPlantsService.convertToSAP(mp);

        this.sapService.getItem(this.serviceNameSAP, this.entityNameSAP, null, mpSAP.getId()).subscribe((response) => {
          if (response.status == 200) {
            this.sapService.updateItem(this.serviceNameSAP, this.entityNameSAP, null, mpSAP.getId(), mpSAP).subscribe((response) => {
              if (response.status == 204) {
                this.saveItem(mp).subscribe((resp) => {
                  observer.next(resp);
                  observer.complete();
                });
              }
              else {
                this.loggingService.error("Error: " + JSON.stringify(response));
                observer.next({status: response.status, message: response, context: 'SAP'});
                observer.complete();
              }
            });
          }
          else {
            this.sapService.createItem(this.serviceNameSAP, this.entityNameSAP, null, mpSAP).subscribe((response) => {
              if (response.status == 201) {
                this.saveItem(mp).subscribe((resp) => {
                  observer.next(resp);
                  observer.complete();
                });
              }
              else {
                this.loggingService.error("Error: " + JSON.stringify(response));
                observer.next({status: response.status, message: response, context: 'SAP'});
                observer.complete();
              }
            });
          }
        });
      };

      if (mp.site.code && mp.site.active_on_sap) {
        saveFunc();
      }
      else {
        this.sitesService.getSite(mp.site.pk).subscribe((site) => {
          mp.site = site;
          if(mp.site.active_on_sap){
            saveFunc();
          }
          else {
            this.saveItem(mp).subscribe((resp) => {
              observer.next(resp);
              observer.complete();
            });
          }
        })
      }
    });
  }

  static convertToSAP(matPlant: MaterialPlant) {
    let mp = cloneObject(matPlant);
    let mpSAP = new MaterialPlantSAP();
    mpSAP.MatCode = mp.material_id.code;
    mpSAP.Plant = mp.site.code;
    mpSAP.ProductionType = mp.production_type.delta_rel + "";
    if (mp.commodity_code)
      mpSAP.CommodityCode = mp.commodity_code;
    // mpSAP.CommodityCode = mp.commodity_code;
    mpSAP.InspectionStock = mp.inspection_stock;
    mpSAP.MES = mp.mes;

    if(mp.material_id.material_type.sap_code){
      mpSAP.Type = mp.material_id.material_type.sap_code;
    }

    if (mp.material_id.sub_model.main_material && mp.material_id.sub_model.main_material.pk) {
      mpSAP.MainMaterial = mp.material_id.sub_model.main_material.__str__;
      mpSAP.Subtype = " ";
    }
    else {
      if (mp.material_id.sub_model.subtype && mp.material_id.sub_model.subtype.pk) {
        mpSAP.MainMaterial = " ";
        mpSAP.Subtype = mp.material_id.sub_model.subtype.__str__;
      }
      else {
        mpSAP.MainMaterial = " ";
        mpSAP.Subtype = " ";
      }
    }

    return mpSAP;
  }

  deleteMaterialPlant(mp: MaterialPlant) {
    return Observable.create((observer) => {
      let deleteFunc = () => {
        let mpSAP = MaterialPlantsService.convertToSAP(mp);
        this.sapService.deleteItem(this.serviceNameSAP, this.entityNameSAP, null, mpSAP.getId()).subscribe((response) => {
          if (response.status == 204) {
            this.deleteItem(mp).subscribe((resp) => {
              observer.next(resp);
              observer.complete();
            });
          }
          else {
            this.loggingService.error("Error: " + JSON.stringify(response));
            observer.next({status: response.status, message: response, context: 'SAP'});
            observer.complete();
          }
        });
      };
      if (mp.site.code && mp.site.active_on_sap) {
        deleteFunc();
      }
      else {
        this.sitesService.getSite(mp.site.pk).subscribe((site) => {
          mp.site = site;
          if(mp.site.active_on_sap){
            deleteFunc();
          }
          else {
            this.deleteItem(mp).subscribe((resp) => {
              observer.next(resp);
              observer.complete();
            });
          }
        })
      }
    });
  }

  public saveItemSerionet(mp: MaterialPlant) {
    return this.saveItem(mp);
  }

  public deleteItemSerionet(mp: MaterialPlant) {
    return this.deleteItem(mp);
  }
}

declare let sap: any;

@Injectable()
export class MaterialPlantsSAPService extends GenericSAPService {

  constructor(protected sapService: SAPService) {
    super(sapService);
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entitySetName_SAP =  "MatPlantsSet";
  }

  getMaterialPlants(query: any) {
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let params = {};
        if (query) {
          let filters = [];
          if (query.MatCode != null) {
            let filter = new sap.ui.model.Filter({
              path: 'MatCode',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['MatCode']
            });
            filters.push(filter);
          }
          if (query.Plant != null) {
            let filter = new sap.ui.model.Filter({
              path: 'Plant',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Plant']
            });
            filters.push(filter);
          }
          params['filters'] = filters;
        }
        return this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      })
    })
  }

  getMaterialPlant(id: any) {
    return this.getItem(id);
  }
}



