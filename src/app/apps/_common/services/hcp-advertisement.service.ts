import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class HcpAdvertisement extends GenericObj{
  label?:string;
}

@Injectable()
export class HcpAdvertisementService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "HcpAdvertisement";
    this.serviceURL = "hrpositions/hcpadvertisements/";
  }

  getHcpAdvertisements(query: any) {
    return this.getList(query);
  }
}
