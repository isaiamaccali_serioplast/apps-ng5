import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Organization} from "./organization.service";

export class LegalEntity extends GenericObj{
  name?: string;
  code?: string;
  country?: string;
  organization_ref?: Organization;
}

@Injectable()
export class LegalEntityService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Legal Entities";
    this.serviceURL = "organizations/legalentity/";
  }

  getLegalEntities(query:any) {
   return this.getList(query);
  }

  saveLegalEntity(le:LegalEntity){
    return this.saveItem(le);
  }
  getLegalEntity(id:number) {
    return this.getItem(id,true);
  }

  deleteLegalEntiy(le:LegalEntity){
    return this.deleteItem(le);
  }

  restoreLegalEntity(le:LegalEntity){
    return this.restoreItem(le);
  }
}



