import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {MaterialSubTypeLink, MaterialSubtypesLinkService} from "./material-subtype-link.service";
import {GenericObj, GenericService} from "./_generic.service";
import {parseSerionetRespToData} from "../../../shared/utils";

export class MaterialSubType extends GenericObj{
  label?:string;
}

@Injectable()
export class MaterialSubtypesService extends GenericService{

  entityName = "MATERIAL-SUBTYPES";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService,
              protected materialSubtypeLinkService:MaterialSubtypesLinkService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Material Subtypes";
    this.serviceURL = "materials/materialsubtype/";
  }

  getMaterialSubtypes(query: any, format?:string) {
    return this.getList(query, format);
  }

  getMaterialSubtype(id: number) {
    return this.getItem(id);
  }

  saveMaterialSubtype(mst: MaterialSubType) {
    return this.saveItem(mst);
  }

  deleteMaterialSubtype(mst: MaterialSubType) {
    return Observable.create((observer) => {

      this.materialSubtypeLinkService.getMaterialSubtypeLinks({subtype_id__label: mst.label}).subscribe((data) => {
        let mstl = parseSerionetRespToData(data);
        if (mstl.length > 0) {
          this.deleteSubtypeLinks(mstl, 0, () => {
            this.deleteItem(mst).subscribe((resp)=>{
              observer.next(resp);
              observer.complete();
            });
          })
        }
        else {
          this.deleteItem(mst).subscribe((resp)=>{
            observer.next(resp);
            observer.complete();
          });
        }
      });
    });
  }

  deleteSubtypeLinks(sls:MaterialSubTypeLink[],index:number,callback:any,lastResponse?:any){
    if(sls[index]){
      let sl = sls[index];
      this.materialSubtypeLinkService.deleteMaterialSubtypeLink(sl).subscribe((resp)=>{
        if(resp.pk){
          lastResponse = resp;
          this.deleteSubtypeLinks(sls,index+1,callback,lastResponse);
        }
        else {
          callback.call(null,resp);
        }
      })
    }
    else {
      if(!lastResponse){
        lastResponse = {
          pk: ((new Date()).getTime())
        }
      }
      else {
        callback.call(null,lastResponse)
      }
    }
  }

  restoreMaterialSubtype(mst: MaterialSubType) {
    return this.restoreItem(mst);
  }
}

@Pipe({
  name: 'material_subtype'
})
export class MaterialSubTypePipe implements PipeTransform{

  constructor(private materialsSubTypeService:MaterialSubtypesService){}

  transform(id:number) {
    return this.materialsSubTypeService.getMaterialSubtype(id);
  }
}
