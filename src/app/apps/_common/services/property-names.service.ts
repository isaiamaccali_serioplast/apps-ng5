import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericSortableObj, GenericSortableService} from "./_generic-sortable.service";

export class PropertyName extends GenericSortableObj {
  name?:string;
}

@Injectable()
export class PropertyNamesService extends GenericSortableService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Property Names";
    this.serviceURL = "qc/propertyname/";
  }

  getPropertyNames(query:any) {
    return this.getList(query);
  }

  savePropertyName(pn:PropertyName){
    return this.saveItem(pn);
  }

  deletePropertyName(pn:PropertyName){
    return this.deleteItem(pn);
  }

  restorePropertyName(pn:PropertyName){
    return this.restoreItem(pn);
  }
}



