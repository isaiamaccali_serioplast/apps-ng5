import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {UserOneToOneService} from "./user-one-to-one.service";
import {GenericObj} from "./_generic.service";
import {HRUser} from "./hr-users.service";
import {SPFile} from "./file.service";
import {Sex} from "./sex.service";

export class PersonalInfo extends GenericObj {
  user_id: HRUser;
  first_name :string;
  middle_name :string;
  last_name:string;
  real_name:string;
  sex :Sex;
  birth_date : string;
  birth_country:string;
  home_address :string;
  home_email :string;
  home_landline_phone:string;
  home_mobile_phone :string;
  skype :string;
  cv :SPFile;
}

@Injectable()
export class PersonalInfoService extends UserOneToOneService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "PersonalInfo";
    this.serviceURL = "hrprofiles/personalinfo/";
  }

  getPersonalInfos(query: any) {
    return this.getList(query);
  }

  getPersonalInfo(id: number) {
    return this.getItem(id,false);
  }

  updatePersonalInfo(pi: PersonalInfo) {
    return this.saveItem(pi);
  }

}



