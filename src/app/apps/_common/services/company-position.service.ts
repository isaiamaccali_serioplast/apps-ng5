import {Injectable} from "@angular/core";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {CompanyPositionName} from "./company-position-name.service";
import {Site} from "./sites.service";
import {StatusLink} from "./status-link.service";
import {HRProfile} from "./hr-profiles.service";
import {ContractType} from "./contract-type.service";
import {Observable} from "rxjs";

export class CompanyPosition extends GenericObj{
  active_tasks?:string;
  name?:CompanyPositionName;
  site_ref?:Site;
  filled_on?:string;
  cost?:number;
  cost_currency?:string;
  status?:StatusLink;
  hr_ref?:HRProfile;
  reason_request?:string;
  requested_entry_date?:string;
  requested_contract_duration?:string;
  requested_training?:string;
  target_entry_date?:string;
  target_gross_salary?:number;
  target_gross_salary_currency?:string;
  target_contract_type?:ContractType;
  note_requested?:string;
  is_public?:boolean;
  employment_offer?:string; //file
  staff_request_owner?:HRProfile;
}

@Injectable()
export class CompanyPositionService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "CompanyPosition";
    this.serviceURL = "hrpositions/companypositions/";
  }

  getCompanyPositions(query: any) {
    return this.getList(query);
  }

  getCompanyPosition(id: number) {
    return this.getItem(id,true);
  }

  saveCompanyPosition(cp:CompanyPosition) {
    return this.saveItem(cp);
  }

  advanceUD(cp:CompanyPosition) {
    return Observable.create((observer) => {

      let form = document.createElement("form");
      let input = document.createElement("input");
      input.setAttribute("name", "cp");
      input.value = cp.pk+"";
      form.appendChild(input);
      const formData = new FormData(form);

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {};
      serionetOptions.noCT = true;

      this.serionetService.doPost("hrpositions/start-develop-vacancy-task/", formData,serionetOptions).subscribe((data) => {
        let d = {
          status:200,
          response:data
        };
        observer.next(d);
        observer.complete();
      },(response) => {
        this.loggingService.error("Error in advancing " + this.entityName + ", status: " + response.status);
        observer.next(response);
        observer.complete();
      });
    });
  }

  setShortListStart(cp:CompanyPosition) {
    return Observable.create((observer) => {
      let form = document.createElement("form");
      let input = document.createElement("input");
      input.setAttribute("name", "cp");
      input.value = cp.pk+"";
      form.appendChild(input);
      const formData = new FormData(form);

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {};
      serionetOptions.noCT = true;

      this.serionetService.doPost("hrpositions/start-short-list-task/", formData,serionetOptions).subscribe((data) => {
        let d = {
          status:200,
          response:data
        };
        observer.next(d);
        observer.complete();
      },(response) => {
        this.loggingService.error("Error in advancing " + this.entityName + ", status: " + response.status);
        observer.next(response);
        observer.complete();
      });
    });
  }
}
