import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

export class Technology extends GenericObj {
  label?:string;
  sap_code?:string;
}

@Injectable()
export class TechnologyService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Technology";
    this.serviceURL = "statuses/technology/";
  }

  getTechnologies(query: any) {
    return this.getList(query);
  }

  getTechnology(id: number) {
    return this.getItem(id);
  }
}



