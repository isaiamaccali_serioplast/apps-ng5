import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {Site, SitesService} from "./sites.service";
import {Workcenter, WorkcenterSAP} from "./workcenters.service";

export class WorkcenterLocation {
  pk: number;
  __str__: string;
  location?: Site;
  is_active?: boolean;
  workcenter_id?: Workcenter;
  top?: number;
  left?: number;

  status: string; //active/inactive -> custom attribute used in the template
}

@Injectable()
export class WorkcenterLocationsService extends GenericService {

  serviceName = "ZWORKCENTER_SRV";
  entitySetName = "WorkCenterSet";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService,
              private sapService: SAPService, private sitesService: SitesService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Workcenter Locations";
    this.serviceURL = "operations/workcenterlocation/";
  }

  getWorkcenterLocations(query: any) {
    return this.getList(query);
  }

  getWorkcenterLocation(id: number) {
    return this.getItem(id);
  }

  deleteWorkcenterLocation(wcl: WorkcenterLocation) {
    return Observable.create((observer) => {
      let wcSap = new WorkcenterSAP();
      let deleteFunc = () => {
        wcSap.Plant = wcl.location.code;
        wcSap.WorkCentCode = wcl.workcenter_id.sap_code;
        this.sapService.getItem(this.serviceName, this.entitySetName, null, wcSap.getId()).subscribe((responseRead) => {
          if (responseRead.status == 200) {
            this.sapService.deleteItem(this.serviceName, this.entitySetName, null, wcSap.getId()).subscribe((response) => {
              if (response.status == 204) {
                this.deleteItem(wcl).subscribe((resp) => {
                  observer.next(resp);
                  observer.complete();
                });
              }
              else {
                this.loggingService.error("Error during deleting workcenter location on SAP: " + response.message);
                observer.next({
                  status: response.status,
                  context: 'SAP',
                  message: response.message
                });
                observer.complete();
              }
            });
          }
          else {
            this.deleteItem(wcl).subscribe((resp) => {
              observer.next(resp);
              observer.complete();
            });
          }

        });

      };
      if (wcl.location.code && wcl.location.active_on_sap) {
        deleteFunc();
      }
      else {
        this.sitesService.getSite(wcl.location.pk).subscribe((site) => {
          wcl.location = site;
          if (wcl.location.active_on_sap) {
            deleteFunc();
          }
          else {
            this.deleteItem(wcl).subscribe((resp) => {
              observer.next(resp);
              observer.complete();
            });
          }
        })
      }

    });
  }

  saveWorkcenterLocation(wcl: WorkcenterLocation) {
    return Observable.create((observer) => {
      let saveFunc = () => {
        let wcSap = new WorkcenterSAP();
        wcSap.Plant = wcl.location.code;
        wcSap.WorkCentCode = wcl.workcenter_id.sap_code;
        try {
          wcSap.Technology = wcl.workcenter_id.technology.technology_id.sap_code;
        }
        catch (e) {
          wcSap.Technology = "";
        }
        wcSap.Description = wcl.workcenter_id.description;
        wcl.is_active = true;
        this.sapService.getItem(this.serviceName, this.entitySetName, null, wcSap.getId()).subscribe((response) => {
          if (response.status == 200) {
            this.sapService.updateItem(this.serviceName, this.entitySetName, null, wcSap.getId(), wcSap).subscribe((response) => {
              if (response.status >= 200 && response.status < 300) {
                this.saveItem(wcl).subscribe((resp) => {
                  observer.next(resp);
                  observer.complete();
                });
              }
              else {
                this.loggingService.error("Error during updating workcenter location on SAP: " + response.message);
                observer.next({
                  status: response.status,
                  context: 'SAP',
                  message: response.message
                });
                observer.complete();
              }
            })
          }
          else {
            this.sapService.createItem(this.serviceName, this.entitySetName, null, wcSap).subscribe((response) => {
              if (response.status >= 200 && response.status < 300) {
                this.saveItem(wcl).subscribe((resp) => {
                  observer.next(resp);
                  observer.complete();
                });
              }
              else {
                this.loggingService.error("Error during creating workcenter location on SAP: " + response.message);
                observer.next({
                  status: response.status,
                  context: 'SAP',
                  message: response.message
                });
                observer.complete();
              }
            })
          }
        });
      };
      if (wcl.location.code && wcl.location.active_on_sap) {
        saveFunc();
      }
      else {
        this.sitesService.getSite(wcl.location.pk).subscribe((site) => {
          wcl.location = site;
          if (wcl.location.active_on_sap) {
            saveFunc();
          }
          else {
            this.saveItem(wcl).subscribe((resp) => {
              observer.next(resp);
              observer.complete();
            });
          }
        })
      }
    });
  }

  saveWorkcenterLocationOnSerionet(wcl: WorkcenterLocation) {
    return this.saveItem(wcl);
  }

  restoreWorkcenterLocation(wc: WorkcenterLocation) {
    return this.saveWorkcenterLocation(wc);
  }
}
