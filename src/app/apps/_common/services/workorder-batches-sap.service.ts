import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

declare let sap: any;

export class WorkorderBatch {
  WorkOrderNumber?:string;
  ComponentCode?:string;
  BatchNumber?:string;
  Plant?:string;
  StorLoc?:string;
  Quantity:any;
  Uom:string
}

@Injectable()
export class WorkorderBatchSapService extends GenericSAPService {
  serviceName_SAP = "zworkorders_srv";
  entitySetName_SAP = "BatchSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  createBatch(item: WorkorderBatch) {
    return this.saveItem(item, true);
  }

  getBatches(query){
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.ComponentCode != null) {
          let filter = new sap.ui.model.Filter({
            path: 'ComponentCode',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['ComponentCode']
          });
          filters.push(filter);
        }
        if (query.WorkOrderNumber != null) {
          let filter = new sap.ui.model.Filter({
            path: 'WorkOrderNumber',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['WorkOrderNumber']
          });
          filters.push(filter);
        }
        if (query.Plant != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(filter);
        }
        params['filters'] = filters;
      }

      this.checkPagination(params, query);

      this.getList(params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}



