import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

declare let sap: any;

export class WorkorderRelatedBatch {
  WorkOrder?:string;
  Batch?:string;
  Plant?:string;
  Quantity:any;
  Uom:string;
  Material:string;
  Description?:string;
}

@Injectable()
export class WorkorderRelatedBatchSapService extends GenericSAPService {
  serviceName_SAP = "zhuquality_srv";
  entitySetName_SAP = "WOBatchSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  getBatches(query){
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.Batch != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Batch',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Batch']
          });
          filters.push(filter);
        }
        if (query.WorkOrder != null) {
          let filter = new sap.ui.model.Filter({
            path: 'WorkOrder',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['WorkOrder']
          });
          filters.push(filter);
        }
        if (query.Plant != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(filter);
        }
        if (query.Material != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Material',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Material']
          });
          filters.push(filter);
        }

        params['filters'] = filters;
      }

      this.checkPagination(params, query);

      params['sorters'] = [
        new sap.ui.model.Sorter("Material",false),
        new sap.ui.model.Sorter("Batch",false)
      ];

      this.getList(params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}



