import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {Observable} from "rxjs";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {MaterialSubType} from "./material-subtype.service";

export class MaterialSubTypeLink extends GenericObj{
  subtype?:string;
  sap_code?:string;
  delta_rel?:number;
  app?:GenericObj;
  fieldname?:string;
  subtype_id?:MaterialSubType
}

@Injectable()
export class MaterialSubtypesLinkService extends GenericService {

  fieldName = "subtype";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Material Subtype Link";
    this.serviceURL = "materials/materialsubtypelink/"
  }


  getMaterialSubtypeLinks(query: any) {
    if (!query)
      query = {};
    query['fieldname'] = this.fieldName;
    return this.getList(query);
  }

  getMaterialSubtypeLink(id: number) {
    return this.getItem(id);
  }

  saveMaterialSubtypeLink(mstl: MaterialSubTypeLink) {
    return Observable.create((observer) => {

      mstl.fieldname = this.fieldName;

      if (mstl.pk) {
        this.saveItem(mstl).subscribe((resp) => {
          observer.next(resp);
          observer.complete();
        });
      }
      else {
        this.getMaterialSubtypeLinks({app: mstl.app}).subscribe((resp) => {
          let max_delta_rel = 0;
          if (resp && resp['count'] > 0) {
            resp.results.forEach((l: MaterialSubTypeLink)=> {
              if (l.delta_rel > max_delta_rel) {
                max_delta_rel = l.delta_rel;
              }
            })
          }
          mstl.delta_rel = max_delta_rel + 1;
          this.saveItem(mstl).subscribe((resp) => {
            observer.next(resp);
            observer.complete();
          });
        });
      }
    });
  }

  deleteMaterialSubtypeLink(mstl: MaterialSubTypeLink) {
    return this.deleteItem(mstl);
  }

  restoreMaterialSubtypeLink(mstl: MaterialSubTypeLink) {
    return this.restoreItem(mstl);
  }
}

@Pipe({
  name: 'material_subtype_link'
})
export class MaterialSubTypeLinkPipe implements PipeTransform{

  constructor(private materialsSubTypeLinkService:MaterialSubtypesLinkService){}

  transform(id:number) {
    return this.materialsSubTypeLinkService.getMaterialSubtypeLink(id);
  }
}




