import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

export class TagLink extends GenericObj{
  fieldname?: string;
  app?: GenericObj;
  delta_rel?: number;
  type_id?: GenericObj;
}

@Injectable()
export class TagLinkService extends GenericService {

  fieldName = "tags";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Technology";
    this.serviceURL = "statuses/taglink/";
  }

  getTagLinks(query: any) {
    if (!query)
      query = {};
    query['fieldname'] = this.fieldName;
    return this.getList(query);
  }

  getTagLink(id: number) {
    return this.getItem(id);
  }
}



