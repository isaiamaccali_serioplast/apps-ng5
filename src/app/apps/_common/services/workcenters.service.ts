import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {Observable} from "rxjs";
import {WorkcenterLocation, WorkcenterLocationsService} from "./workcenter-locations.service";
import {cloneObject} from "../../../shared/utils";
import {TechnologyLink} from "./technology-link.service";
import {StatusLink} from "./status-link.service";
import {Site} from "./sites.service";
import {HttpParams} from "@angular/common/http";

export class Workcenter extends GenericObj {
  sap_code?: string;
  description?: string;
  technology?: TechnologyLink;
  is_active?:boolean;

  locations?:Site[];
  newly_created?:boolean;

  status?:StatusLink;
  sub_status?:StatusLink;
  color?:string;
  text_color?:string;
}

export class WorkcenterSAP {
  WorkCentCode: string; //workcenter.sap_code
  Plant:string; // site.code
  Description:string; //workcenter.description
  Technology:string; //workcenter.technology.technolgy_id.sap_code
  Location:string; // blank for now

  getId(){
    return "WorkCentCode='"+this.WorkCentCode+"',Plant='"+this.Plant+"'";
  }
}

@Injectable()
export class WorkcentersService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, private workcenterLocationsService:WorkcenterLocationsService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Workcenters";
    this.serviceURL = "operations/workcenter/";
  }

  getWorkcenters(query:any) {
    return this.getList(query);
  }

  getWorkcenter(id:number) {
    return this.getItem(id);
  }

  deleteWorkcenter(wc:Workcenter) {

    return Observable.create((observer) => {

      this.workcenterLocationsService.getWorkcenterLocations({is_active:true,workcenter_id__id:wc.pk}).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          let wcls = data['results'];
          wcls.forEach((wcl:WorkcenterLocation) =>{
            wcl.workcenter_id = cloneObject(wc);
          });
          this.deleteWorkcenterLocations(wcls, 0, null, (resp)=> {
            if(resp.status==204){
              this.deleteItem(wc).subscribe((resp_) => {
                observer.next(resp_);
                observer.complete();
              })
            }
            else {
              observer.next(resp);
              observer.complete();
            }
          });
        }
        else {
          //Di sicurezza, ma non dovrebbe mai succedere
          this.deleteItem(wc).subscribe((resp_) => {
            observer.next(resp_);
            observer.complete();
          })
        }
      });
    });
  }

  deleteWorkcenterLocations(wcls:WorkcenterLocation[],index,lastResponse,callback){
    if(wcls[index]){
      let wcl = wcls[index];
      this.workcenterLocationsService.deleteWorkcenterLocation(wcl).subscribe((response)=>{
        if(response.status == 204){
          this.deleteWorkcenterLocations(wcls,index+1,response,callback);
        }
        else {
          callback.call(null,response);
        }
      })
    }
    else {
      callback.call(null,lastResponse)
    }
  }

  saveWorkcenter(wc:Workcenter) {
    return this.saveItem(wc);
  }

  deleteItemSerionet(wc:Workcenter, params?:any){
    return this.deleteItem(wc, params);
  }

  newStatus(item:Workcenter,status:number,sub_status?:number){
    return Observable.create((observer) => {
      let urlParams = new HttpParams();
      if (sub_status){
        urlParams.set("change_status", status.toString() + "|" + sub_status.toString()+"");
      }
      else{
        urlParams.set("change_status", status.toString() + "|");
      }

      let i_;
      i_ = GenericService.processItemForSave(this.clearItem(item));

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };

      if (i_.pk) {
        this.serionetService.doPut(this.serviceURL + i_.pk, i_,serionetOptions).subscribe((data) => {
          observer.next(data);
          observer.complete();
        }, (response) => {
          this.loggingService.error("Error in updating " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        });
      }
    });
  }

}



