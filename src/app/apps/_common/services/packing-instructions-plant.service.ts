import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {cloneObject} from "../../../shared/utils";
import {MaterialPlant, MaterialPlantsService} from "./material-plant.service";
import {MaterialsService} from "./materials.service";
import {Observable} from "rxjs";
import {SitesService} from "./sites.service";
import {SAPObject} from "./_generic_sap_service";
import {GenericObj} from "./_generic.service";
import {PackingInstruction} from "./packing-instructions.service";

export class PackingInstructionLocation extends GenericObj{
  pack_instruct?:PackingInstruction;
  material_plant?:MaterialPlant;

  is_orphan:boolean;
  status:string;
}

export class PackingInstructionLocation_SAP extends SAPObject {
  Code:string;
  Plant:string;
  Material:string;

  public getId(){
    return "Code='"+this.Code+"',Plant='"+this.Plant+"',Material='"+this.Material+"'";
  }
}

@Injectable()
export class PackingInstructionsLocationService extends GenericServiceWithSAP{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService,
              private materialPlantService:MaterialPlantsService, private materialsService:MaterialsService, private sitesService:SitesService
  ) {
    super(serionetService,loggingService,sapService);
    this.init();
  }

  init(){
    this.entityName = "Packing Instructions Location";
    this.serviceURL = "materials/packinginstructionlocation/";
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entitySetName_SAP = "PackInsPlantSet";
  }

  getPackingInstructionLocations(query:any) {
    return this.getList(query);
  }

  getPackingInstructionLocation(id:number) {
    return this.getItem(id);
  }

  deletePackingInstructionLocation(pil:PackingInstructionLocation) {
    return Observable.create((observer)=>{
      let pil_ = cloneObject(pil);
      this.materialPlantService.getMaterialPlant(pil_.material_plant.pk).subscribe((mp)=>{
        pil_.material_plant = mp;
        this.materialsService.getMaterial(pil_.material_plant.material_id.pk).subscribe((m)=>{
          pil_.material_plant.material_id = m;
          this.sitesService.getSite(pil_.material_plant.site.pk).subscribe((s)=>{
            pil_.material_plant.site = s;
            this.deleteItem(pil_).subscribe((resp)=>{
              observer.next(resp);
              observer.complete();
            })
          });
        })
      })
    })
  }

  savePackingInstructionLocation(pil:PackingInstructionLocation) {
    return this.saveItem(pil);
  }

  getSAPObject(pil:PackingInstructionLocation) {
    let pil_SAP = new PackingInstructionLocation_SAP();
    pil_SAP.Code = pil.pack_instruct.code;
    pil_SAP.Material = pil.material_plant.material_id.code;
    pil_SAP.Plant = pil.material_plant.site.code;
    return pil_SAP;
  }
}
