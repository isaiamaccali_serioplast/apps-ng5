import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Material} from "./materials.service";

export class Cavity extends GenericObj{
  name?:string;
  mouldset?: Material;
  plm_revision?:string;
}

@Injectable()
export class CavitiesService extends GenericService{

  entityName = "CAVITIES";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Cavities";
    this.serviceURL = "materials/cavities/";
  }

  getCavities(query) {
    return this.getList(query);
  }

  saveCavity(item:Cavity){
    return this.saveItem(item);
  }

  deleteCavity(item:Cavity){
    return this.deleteItem(item);
  }
}



