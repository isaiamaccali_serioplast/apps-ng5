import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Organization} from "./organization.service";

export class Supplier extends GenericObj{
  organization_ref:Organization;
  type:string;
  sap_code:string;
  sap_description:string;
}

@Injectable()
export class SupplierService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Suppliers";
    this.serviceURL = "organizations/suppliers/";
  }

  getSuppliers(query: any) {
    return this.getList(query);
  }

  getSupplier(id: any) {
    return this.getItem(id);
  }

  updateSupplier(supplier: Supplier){
    return this.saveItem(supplier);
  }
}

@Pipe({
  name: 'supplier'
})
export class SupplierPipe implements PipeTransform{

  supplier:Supplier = new Supplier();
  querying = false;

  constructor(private suppliersService:SupplierService){}

  transform(id:number) {
    if(id){
      if(!this.supplier.pk && !this.querying) {
        this.querying = true;
        this.suppliersService.getSupplier(id).subscribe((data) => {
          this.supplier = data;
          this.querying = false;
        });
      }
      return this.supplier.__str__;
    }
    else {
      return id;
    }
  }
}
