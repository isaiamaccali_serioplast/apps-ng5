/**
 * Service used to share the available sites
 */
import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {LegalEntity} from "./legal-entity.service";
import {OrganizationLevel} from "./organization-level.service";
import {parseSerionetRespToData} from "../../../shared/utils";
import {SPFile} from "./file.service";

export class Site extends GenericObj {
  name?: string;
  code?:string;
  address?:string;
  lng?:string;
  lat?:string;
  legalentity_ref?:LegalEntity;
  org_level?:OrganizationLevel;
  active_on_sap?:boolean;
  country?:string; // dynamic field
  site_map?:SPFile;
  workcenter_scale?:number;
}

@Injectable()
export class SitesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Sites";
    this.serviceURL = "organizations/sites/";
  }

  getSites(query?: any) {
    return this.getList(query);
  }

  getSite(id: number) {
    return this.getItem(id, true);
  }

  deleteSite(s:Site){
   return this.deleteItem(s);
  }

  restoreSite(s:Site){
    return this.restoreItem(s);
  }

  saveSite(s:Site){
    return this.saveItem(s);
  }

}

@Pipe({
  name: 'site'
})
export class SitePipe implements PipeTransform{

  constructor(private sitesService:SitesService){}

  transform(id:number) {
    return this.sitesService.getSite(id);
  }
}

@Pipe({
  name: 'siteSapCode',
  pure: false
})
export class SiteFromSAPCodePipe implements PipeTransform{

  site:Site = new Site();
  querying = false;

  constructor(private sitesService:SitesService){}

  transform(code:string) {
    if(code){
      if(!this.site.pk && !this.querying) {
        this.querying = true;
        this.sitesService.getSites({code: code}).subscribe((data) => {
          let sites = parseSerionetRespToData(data);
          if (sites.length > 0) {
            this.site = sites[0];
            this.querying = false;
          }
        });
      }
      return this.site.__str__;
    }
    else {
      return code;
    }

  }
}



