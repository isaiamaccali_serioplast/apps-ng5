import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

export class BomReference {
  Material:string;
  Plant:string;
  RefQuantity:any;
  RefUom:string;
  Quantity:any;
  Weight:any;
  Rule:string;
  QtyUom:string;
  WeightUom:string;

  getId = ()=>{
    return "Material='"+this.Material+"',Plant='"+this.Plant+"',RefQuantity='"+this.RefQuantity+"',RefUom='"+this.RefUom+"',Rule='"+this.Rule+"'";
  }
}

@Injectable()
export class BomReferenceService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "BOMReferenceSet";
    this.serviceName_SAP = "ZUTILITIES_srv";
  }

  getBomReference(id: any) {
    return this.getItem(id);
  }
}



