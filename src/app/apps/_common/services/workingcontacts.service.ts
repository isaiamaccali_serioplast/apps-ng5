import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {UserOneToOneService} from "./user-one-to-one.service";
import {GenericObj} from "./_generic.service";

export class WorkingContact extends GenericObj {
  user_id : number;
  work_landline_phone:string;
  work_email:string;
  work_mobile_phone:string;
  work_address:string;
}

@Injectable()
export class WorkingContactsService extends UserOneToOneService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "WorkingContact";
    this.serviceURL = "hrprofiles/workingcontacs/";
  }

  getWorkingContacts(query: any) {
    return this.getList(query);
  }

  getWorkingContact(id: number) {
    return this.getItem(id);
  }

  saveWorkingContact(wc:WorkingContact){
    return this.saveItem(wc);
  }

}
