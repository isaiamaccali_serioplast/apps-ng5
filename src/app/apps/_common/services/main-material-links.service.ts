import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";
import {MaterialSubType} from "./material-subtype.service";

export class MainMaterialLink extends GenericObj{
  subtype?:string;
  sap_code?:string;
  delta_rel?:number;
  app?:GenericObj;
  fieldname?:string;
  subtype_id?:MaterialSubType;
}

@Injectable()
export class MainMaterialLinksService extends GenericService{

  fieldName = "main_material";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Main Material Links";
    this.serviceURL = "materials/materialsubtypelink/";
  }

   getMainMaterialLinks(query:any) {
    if(!query) query = {};
    query['fieldname'] = this.fieldName;
    return this.getList(query);
  }

  getMainMaterialLink(id:number){
    return this.getItem(id);
  };

  saveMainMaterialLink(m:MainMaterialLink){

    return Observable.create((observer) => {

      this.getMainMaterialLinks({app: m.app}).subscribe((resp) => {
        let max_delta_rel = 0;
        if (resp && resp['count'] > 0) {
          resp.results.forEach((l: MainMaterialLink) => {
            if (l.delta_rel > max_delta_rel) {
              max_delta_rel = l.delta_rel;
            }
          })
        }
        m.delta_rel = max_delta_rel + 1;
        m.fieldname = this.fieldName;
        this.saveItem(m).subscribe((resp) => {
          observer.next(resp);
          observer.complete();
        });
      });
    });
  };

  deleteMainMaterialLink(m:MainMaterialLink){
    return this.deleteItem(m);
  };

  restoreMainMaterialLink(m:MainMaterialLink){
    return this.restoreItem(m);
  };
}

@Pipe({
  name: 'main_material_link'
})
export class MainMaterialLinkPipe implements PipeTransform{

  constructor(private mainMaterialLinksService:MainMaterialLinksService){}

  transform(id:number) {
    return this.mainMaterialLinksService.getMainMaterialLink(id);
  }
}



