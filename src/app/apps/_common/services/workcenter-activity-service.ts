import {Injectable} from "@angular/core";
import {SessionStorageService} from "../../../shared/session-storage/session-storage.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";

declare let sap: any;

export class WorkcenterActivity_SAP {
  WorkCenter:string;
  Plant:string;
  Description:string;
  ActivityCode:string;
  Value:string;
  Unit:string;
}

@Injectable()
export class WorkcenterActivityService {

  entityName = "WORKCENTER-ACTIVITY";

  serviceNameSAP = "ZWORKCENTER_SRV";
  entityNameSAP = "ActivitySet";

  constructor(private sessionStorage: SessionStorageService, private sapService: SAPService,
              private loggingService: LoggingService) {
  }

  getActivities(query) {
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.WorkCenter != null) {
          let filter = new sap.ui.model.Filter({
            path: 'WorkCenter',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['WorkCenter']
          });
          filters.push(filter);
        }
        if (query.Plant != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(filter);
        }
        params['filters'] = filters;
      }

      this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}

