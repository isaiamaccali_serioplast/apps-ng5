import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {HRUser} from "./hr-users.service";
import {Site} from "./sites.service";

export class Badge extends GenericObj{
  badge_code: string;
  guest: boolean;
  location: Site;
  assigned_to?:HRUser;
}

@Injectable()
export class BadgeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Badge";
    this.serviceURL = "salaries/badges/";
  }

  getBadges(query: any) {
    return this.getList(query);
  }

  getBadge(id: number) {
    return this.getItem(id);
  }

  restoreBadge(item:Badge){
    return this.restoreItem(item);
  }

  deleteBadge(item:Badge){
    return this.deleteItem(item);
  }

  saveBadge(item:Badge){
    return this.saveItem(item);
  }

}
