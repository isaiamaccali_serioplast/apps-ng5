import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {cloneObject} from "../../../shared/utils";
import {getUrlDateTime} from "../../../shared/utils";
import {Material} from "./materials.service";
import {Batch} from "./batches.service";
import {FactorLevel} from "./factor-levels.service";
import {ControlPlanMeasure} from "./control-plan-measures.service";
import {ControlPlan} from "./control-plans.service";
import {Property} from "./properties.service";
import {StatusLink} from "./status-link.service";
import {Site} from "./sites.service";

export class MeasureSession extends GenericObj {
  date?:string = getUrlDateTime(new Date());
  site?:Site;
  material?:Material;
  control_plan?:ControlPlan;
  status?:StatusLink;
  measuresessionsample_set?:MeasureSessionSample[];
  controlplansessionmeasure_set?:ControlPlanMeasure[];
  notes?:string;
  batch?:string;
  batch_date?:string;
  external?:boolean = false;
  sampling_time?:any;
  groupingfactor_set?:GroupingFactor[] = [];

  batch_sap?:Batch;
}

export class GroupingFactor extends GenericObj  {
  name:string;
  factorlevel_set:FactorLevel[] = [];

  level_string?:string;
  message:string;
}

export class MeasureSessionSample extends GenericObj{
  control_plan?:ControlPlan;
  samplemeasure_set?:SampleMeasure[];
  notes?:string;
  index?:number;
  measuresessionsamplefactorlevel_set?:MeasureSessionSampleFactorLevel[] = [];

  identified:boolean;
}

export class MeasureSessionSampleFactorLevel extends GenericObj{
  factor_level: number
}

export class SampleMeasure {
  pk?:number;
  __str__?:string;
  property:Property;
  value:any;
  overridden_value?:any;
  notes?:string;
  delta:number;
}

@Injectable()
export class MeasureSessionService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Measure Session";
    this.serviceURL = "qc/measure-session/";
  }

  getSessions(query: any) {
    return this.getList(query);
  }

  getSession(id: number) {
    return this.getItem(id);
  }

  deleteSession(s: MeasureSession) {
    return this.deleteItem(s);
  }

  saveSession(session: MeasureSession) {
    let s: MeasureSession = cloneObject(session);
    if (s.controlplansessionmeasure_set)
      s.controlplansessionmeasure_set = s.controlplansessionmeasure_set.map((i) => {
        if(i.__str__)
          delete i.__str__;
        return GenericService.processItemForSave(i)
      });
    if (s.measuresessionsample_set)
      s.measuresessionsample_set = s.measuresessionsample_set.map((i) => {
          i.samplemeasure_set = i.samplemeasure_set.map((m) => {
            if(m.__str__)
              delete m.__str__;
            return GenericService.processItemForSave(m)
          });
          if(i.__str__)
            delete i.__str__;
          delete i.identified;
          i.measuresessionsamplefactorlevel_set = i.measuresessionsamplefactorlevel_set.filter((sfl)=>{
            return sfl.factor_level;
          });
          return GenericService.processItemForSave(i, ['samplemeasure_set','measuresessionsamplefactorlevel_set'])
        }
      );
    return this.saveItem(s, ['groupingfactor_set', 'controlplansessionmeasure_set', 'measuresessionsample_set']);
  }

  clearItem(s: MeasureSession){
    let session:MeasureSession = cloneObject(s);
    if(session.batch_sap)
      delete session.batch_sap;
    session.groupingfactor_set.forEach((gf)=>{
      delete gf.level_string;
      delete gf.message;
      gf.factorlevel_set.forEach((fl)=>{
        delete fl.inEdit;
        delete fl.inUse;
        delete fl.originalName;
      })
    });

    return session;
  }
}



