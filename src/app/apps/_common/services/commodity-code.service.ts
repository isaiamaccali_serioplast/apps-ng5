import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class CommodityCode {
  Code:string;
  Description:string;
  Used:boolean;
  Plant:string;
}


@Injectable()
export class CommodityCodesService {

  entityName = "MATERIAL-COMMODITY-CODE";

  serviceNameSAP = "ZMATERIALS_SRV";
  entityNameSAP = "CommodityCodeSet";

  constructor(private sapService: SAPService) {
  }

  getCommodityCodes(query) {
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.used != null) {
          let usedFilter = new sap.ui.model.Filter({
            path: 'Used',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.used
          });
          filters.push(usedFilter);
        }
        if (query.country != null) {
          let siteFilter = new sap.ui.model.Filter({
            path: 'Country',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.country
          });
          filters.push(siteFilter);
        }
        params['filters'] = filters;
      }

      this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}



