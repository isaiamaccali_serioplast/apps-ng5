import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

declare let sap: any;

export class WorkorderLabelSAP {
  Plant:string;
  Name:string;
  Description:string;
  WorkOrder:string;
  Preferred:boolean;
  Copies:number;

  getId = ()=>{
    return "Name='"+this.Name+"',Plant='"+this.Plant+"',WorkOrder='"+this.WorkOrder+"'";
  }
}

@Injectable()
export class WorkorderLabelSapService extends GenericSAPService {
  serviceName_SAP = "zprodplanning_srv";
  entitySetName_SAP = "LabelSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  getLabels(query){
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.Plant != null) {
          let filter = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['Plant']
          });
          filters.push(filter);
        }
        if (query.WorkOrder != null) {
          let filter = new sap.ui.model.Filter({
            path: 'WorkOrder',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['WorkOrder']
          });
          filters.push(filter);
        }
        params['filters'] = filters;
      }

      this.getList(params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }

  saveLabel(item:WorkorderLabelSAP){
    return this.saveItem(this.getInstance(item));
  }

  deleteLabel(item:WorkorderLabelSAP){
    return this.deleteItem(this.getInstance(item));
  }

  getInstance(item: WorkorderLabelSAP){
    if(!item.getId){
      let i_ = new WorkorderLabelSAP();
      for(let k in item){
        if(item.hasOwnProperty(k)){
          i_[k] = item[k];
        }
      }
      return i_;
    }
    else {
      return item;
    }
  }
}



