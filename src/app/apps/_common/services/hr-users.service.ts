import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class HRUser extends GenericObj{
  username?: string;
  email?: string;
  is_staff?: string;
  date_joined?: boolean;
  is_guest?:boolean;
}

@Injectable()
export class HRUserService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "HR User";
    this.serviceURL = "hrprofiles/users/";
  }

  getHrUsers(query:any){
    return this.getList(query);
  }

  getHrUser(id:number) {
    return this.getItem(id,false);
  }

  saveHrUser(user:HRUser) {
    return this.saveItem(user);
  }
}



