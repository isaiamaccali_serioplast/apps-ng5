import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SPFile} from "./file.service";

export class FileManager {
  pk: number;
  __str__:string;
  is_active?:boolean;
  file?: SPFile;
  content_type?: number;
  object_id?: number;

  filepath?:SPFile;
}

@Injectable()
export class FileManagerService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "File Manager";
    this.serviceURL = "files/file-managers/";
  }

  getFileManagers(query?: any) {
    return this.getList(query);
  }

  getFileManager(id: number) {
    return this.getItem(id);
  }

  saveFileManager(fm:FileManager) {
    return this.saveItem(fm);
  }

  deleteFileManager(fm:FileManager) {
    return this.deleteItem(fm);
  }
}
