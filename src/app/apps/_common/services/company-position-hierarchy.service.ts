import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {CompanyPositionName} from "./company-position-name.service";
import {GenericObj, GenericService} from "./_generic.service";

export class CompanyPositionHierarchy extends GenericObj{
  company_position?:CompanyPositionName;
  parent?:number;
}

@Injectable()
export class CompanyPositionHierarchyService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "CompanyPositionHierarchy";
    this.serviceURL = "hrpositions/companypositionnames-hierarchy/";
  }

  getCompanyPositionHierarchies(query: any) {
    return this.getList(query);
  }

  saveCompanyPositionHierarchy(cph: CompanyPositionHierarchy){
    return this.saveItem(cph);
  }

  updateCompanyPositionHierarchy(cph: CompanyPositionHierarchy){
    return this.saveItem(cph);
  }

}
