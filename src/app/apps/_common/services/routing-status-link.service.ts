import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Status} from "./status.service";

export class RoutingStatusLink extends GenericObj{
  status_id?:Status;
  delta_rel?:number;
  fieldname?:string;
  app?:GenericObj;
  sap_code?:string;
}

@Injectable()
export class RoutingStatusLinkService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Routing Status Link";
    this.serviceURL = "materials/routing-status-links/";
  }

  getRoutingStatusLinks(query?: any) {
    return this.getList(query);
  }
}


