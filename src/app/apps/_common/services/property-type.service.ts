import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Property} from "./properties.service";

export class PropertyType extends GenericObj {
  type_code?:string;
  label?:string;
}

export const _PropertyTypes = {
  MIN: 'min',
  MAX: 'max',
  RANGE: 'range',
  BOOLEAN: 'boolean'
};

export const _getPropertyType = (p:Property) => {
  switch(p.type.type_code){
    case 'bool':
      return _PropertyTypes.BOOLEAN;
    case 'min':
      return _PropertyTypes.MIN;
    case 'max':
      return _PropertyTypes.MAX;
    case 'range':
      return _PropertyTypes.RANGE;
    default:
      return null;
  }
}

@Injectable()
export class PropertyTypeService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Property Type";
    this.serviceURL = "qc/propertytype/";
  }

  getPropertyTypes(query:any) {
    return this.getList(query);
  }
}



