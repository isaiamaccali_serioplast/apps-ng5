/**
 * Service used to share the available sites
 */
import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class AppPlantUser extends GenericObj{
  app:number;
  site:number;
  user:string;
  password:string;
}

@Injectable()
export class AppPlantUsersService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "AppPlantUsers";
    this.serviceURL = "appplantusers/";
  }

  getAppPlantUsersList(query?: any) {
    return this.getList(query);
  }

}


