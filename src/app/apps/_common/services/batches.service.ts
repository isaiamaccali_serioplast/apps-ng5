import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {SAPObject} from "./_generic_sap_service";

declare let sap: any;

export class Batch extends SAPObject{
  BatchNumber:string;
  CreationDate:string;
  Plant:string;
  Material:string;
  RoutingGroup:string;
  WorkOrder?:string;
  Quantity?:number;
  UoM?:string;
  Printer?:string;
  Copies?:number;

  getId(){
    return "BatchNumber='"+this.BatchNumber+"',Plant='"+this.Plant+"',Material='"+this.Material+"'";
  }
}

@Injectable()
export class BatchesService {

  serviceNameSAP = "ZBATCHES_SRV";
  entityNameSAP = "BatchSet";

  constructor(private sapService: SAPService) {
  }

  getBatches(query: any) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {
        if (query) {
          let filters = [];
          if (query['from'] && query ['to']) {
            let dateFilter = new sap.ui.model.Filter({
              path: "CreationDate",
              operator: sap.ui.model.FilterOperator.BT,
              value1: query['from'],
              value2: query ['to']
            });
            filters.push(dateFilter);
          }
          if (query['Plant']) {
            let plantFilter = new sap.ui.model.Filter({
              path: "Plant",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Plant']
            });
            filters.push(plantFilter);
          }
          if (query['Material']) {
            let materialFilter = new sap.ui.model.Filter({
              path: "Material",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Material']
            });
            filters.push(materialFilter);
          }
          if (query['RoutingGroup']) {
            let routingFilter = new sap.ui.model.Filter({
              path: "RoutingGroup",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['RoutingGroup']
            });
            filters.push(routingFilter);
          }
          if (query['BatchNumber']) {
            let batchFilter = this.getFilterByLength("BatchNumber", query['BatchNumber'], 10);
            filters.push(batchFilter);
          }
          params['filters'] = filters;
        }
        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }

  getFilterByLength(key: string, value: string, maxLength: number) {
    let filter;
    if (value.length == maxLength || value.length > maxLength) {
      filter = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.EQ,
        value1: value
      });
    }
    else if (value.length == (maxLength - 1)) {
      let filter1 = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.StartsWith,
        value1: value
      });
      let filter2 = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.EndsWith,
        value1: "*" + value
      });
      filter = new sap.ui.model.Filter({
        filters: [filter1, filter2],
        and: false
      });
    }
    else if (value.length < (maxLength - 1)) {
      filter = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.Contains,
        value1: value
      });
    }
    return filter;
  }

}



