import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {cloneObject} from "../../../shared/utils";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {PackingInstructionLocation, PackingInstructionsLocationService} from "./packing-instructions-plant.service";
import {Material} from "./materials.service";
import {SAPObject} from "./_generic_sap_service";
import {MeasureUnit} from "./measure-unit.service";

export class PackingInstruction {
  pk: number;
  __str__: string;
  code?: string;
  description?: string;
  material_id?: Material;
  packinstructionitem_set?: PackingInstructionItem[];
  is_active?: boolean;

  enable_add_item?: boolean;
  mainPackage?: number;
  needsSaving?: boolean;
  plants?: PackingInstructionLocation[]
}

export class PackingInstructionSAP extends SAPObject {
  Code: string;
  Description: string;
  PackInsLink: PackingInstructionItemSAP[];

  public getId() {
    return "Code='" + this.Code + "'";
  }
}

export class PackingInstructionItem extends GenericObj {
  material?: Material;
  quantity?: number;
  uom?: MeasureUnit;
  item_nr: number;
  main_package: boolean;

  main_material: boolean;
}

export class PackingInstructionItemSAP {
  ItemType: string;  //M se è il materiale da imballare, P se è l'imballo
  Code: string;
  Item: string;
  Material: string;
  Quantity: string;
  Uom: string;
  MainPackage: boolean;

  getId() {
    return "Code='" + this.Code + "',Item='" + this.Item + "'";
  }
}

@Injectable()
export class PackingInstructionsService extends GenericServiceWithSAP {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService,
              protected sapService: SAPService, private packageLocationService: PackingInstructionsLocationService) {
    super(serionetService, loggingService, sapService);
    this.init();
  }

  init() {
    this.entityName = "PackageName Instructions";
    this.serviceURL = "materials/packaging-instructions/";
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entitySetName_SAP = "PackInsHeaderSet";
  }

  getPackingInstructions(query: any) {
    return this.getList(query);
  }

  getPackingInstruction(id: number) {
    return this.getItem(id);
  }

  deletePackageInstruction(pi: PackingInstruction) {
    return Observable.create((observer) => {
      this.packageLocationService.getPackingInstructionLocations({pack_instruct__id: pi.pk, is_active: true}).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          let plToDelete = data['results'];
          this.removeLocations(pi, plToDelete, (resp) => {
            if (resp.status == 204) {
              this.deleteItem(pi).subscribe((resp_) => {
                observer.next(resp_);
                observer.complete();
              })
            }
            else {
              observer.next(resp);
              observer.complete();
            }
          })
        }
        else {
          this.deleteItem(pi).subscribe((resp) => {
            observer.next(resp);
            observer.complete();
          })
        }
      })
    });
  }

  removeLocations(pi: PackingInstruction, plToDelete: PackingInstructionLocation[], callback: any) {
    this.removeLocation(pi, plToDelete, 0, callback);
  }

  removeLocation(pi: PackingInstruction, plToDelete: PackingInstructionLocation[], index, callback: any, lastResponse?: any) {
    if (plToDelete[index]) {
      plToDelete[index].pack_instruct = pi;
      this.packageLocationService.deletePackingInstructionLocation(plToDelete[index]).subscribe((resp) => {
        if (resp.status == 204) {
          this.removeLocation(pi, plToDelete, index + 1, callback, resp);
        }
        else {
          callback.call(null, resp);
        }
      })
    }
    else {
      if (!lastResponse) {
        lastResponse = {status: 204};
      }
      callback.call(null, lastResponse);
    }
  }

  savePackageInstruction(pi: PackingInstruction) {
    return Observable.create((observer) => {
      let plants = cloneObject(pi.plants);
      let useSAP = false;
      let pi_ = cloneObject(pi);
      pi_.packinstructionitem_set = pi_.packinstructionitem_set.map((pii) => {
        return GenericService.processItemForSave(pii);
      });
      if (pi_.mainPackage) {
        delete pi_['mainPackage'];
      }
      plants.forEach((pil: PackingInstructionLocation) => {
        if (pil.material_plant.site && pil.material_plant.site.active_on_sap) {
          useSAP = true;
        }
      });

      if (!useSAP) {
        this.saveItemOnSerionet(pi_, ['packinstructionitem_set']).subscribe((resp) => {
          if (resp.pk) {
            plants.forEach((pil: PackingInstructionLocation) => {
              pil.pack_instruct = resp;
            });
            this.processPIPlants(resp, plants, (isOK, errorResp) => {
              if (isOK) {
                observer.next(resp);
                observer.complete();
              }
              else {
                observer.next(errorResp);
                observer.complete();
              }
            })
          }
          else {
            observer.next(resp);
            observer.complete();
          }
        })
      }
      else {
        let item_SAP = this.getSAPObject(pi);
        this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP).subscribe((response_) => {
          if (response_.status > 200 && response_.status < 300) {
            this.saveItemOnSerionet(pi_, ['packinstructionitem_set']).subscribe((response__) => {
              if (response__.pk) {
                plants.forEach((pil: PackingInstructionLocation) => {
                  pil.pack_instruct = response__;
                });
                this.processPIPlants(response__, plants, (isOK, errorResp) => {
                  if (isOK) {
                    observer.next(response__);
                    observer.complete();
                  }
                  else {
                    observer.next(errorResp);
                    observer.complete();
                  }
                })
              }
              else {
                observer.next(response__);
                observer.complete();
              }
            })
          }
          else {
            observer.next(response_);
            observer.complete();
          }
        })
      }
    });
  }

  processPIPlants(pi: PackingInstruction, plants: PackingInstructionLocation[], callback: any) {
    this.processPIPlant(pi, plants, 0, callback);
  }

  processPIPlant(pi: PackingInstruction, plants: PackingInstructionLocation[], index: number, callback: any, lastResponse?: any) {
    if (plants[index]) {
      let plant = plants[index];
      let status = plant.status;
      if (status)
        delete plant['status'];
      switch (status) {
        case "toDelete":
          plant.pack_instruct = pi;
          this.packageLocationService.deletePackingInstructionLocation(plant).subscribe((resp) => {
            if (resp.status == 204) {
              this.processPIPlant(pi, plants, index + 1, callback, resp);
            }
            else {
              callback.call(null, false, resp);
            }
          });
          break;
        case "toSave":
          plant.pack_instruct = pi;
          this.packageLocationService.savePackingInstructionLocation(plant).subscribe((resp) => {
            if (resp.pk) {
              this.processPIPlant(pi, plants, index + 1, callback, resp);
            }
            else {
              callback.call(null, false, resp);
            }
          });
          break;
        case "toIgnore":
        default:
          this.processPIPlant(pi, plants, index + 1, callback, lastResponse)
      }
    }
    else {
      callback.call(null, true);
    }
  }

  restorePackageInstruction(pi: PackingInstruction) {
    return Observable.create((observer) => {

      let pi_ = cloneObject(pi);
      pi_.packinstructionitem_set = pi_.packinstructionitem_set.map((pii) => {
        return GenericService.processItemForSave(pii);
      });
      if (pi_.mainPackage) {
        delete pi_['mainPackage'];
      }

      let piSAP: PackingInstructionSAP = this.getSAPObject(pi);
      if (piSAP.PackInsLink)
        delete piSAP.PackInsLink;

      //Per il restore, create senza items
      this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, piSAP).subscribe((resp_) => {
        if (resp_.status >= 200 && resp_.status < 300) {
          if (pi_.plants)
            delete pi_['plants'];
          this.restoreItemOnSerionet(pi_, ['packinstructionitem_set']).subscribe((resp__) => {
            observer.next(resp__);
            observer.complete();
          })
        }
        else {
          this.loggingService.error("Error during creating or updating package instruction on SAP: " + resp_.message);
          observer.next({
            status: resp_.status,
            context: "SAP",
            message: resp_.message
          });
          observer.complete();
        }
      });
    });
  }

  getSAPObject(pi: PackingInstruction) {
    let piSAP = new PackingInstructionSAP();
    piSAP.Description = pi.description;
    piSAP.Code = pi.code;
    piSAP.PackInsLink = pi.packinstructionitem_set.map((pii, index) => {
      let piiSAP = new PackingInstructionItemSAP();
      piiSAP.Code = pi.code;
      piiSAP.Item = (index * 10 + 10) + "";
      piiSAP.ItemType = pii.main_material ? "M" : "P";
      piiSAP.Material = pii.material.code;
      piiSAP.Quantity = pii.quantity + "";
      piiSAP.Uom = pii.uom.__str__;
      piiSAP.MainPackage = pii.main_package;
      return piiSAP;
    });
    return piSAP;
  };
}
