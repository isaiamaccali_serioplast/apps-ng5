import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {Observable} from "rxjs";
import {cloneObject} from "../../../shared/utils";
import {Routing} from "./routings.service";
import {BOM} from "./bom.service";
import {GenericObj} from "./_generic.service";

export class RoutingProductionVersion extends GenericObj{
  routing_header?:Routing;
  bom_header:BOM;
}

export class RoutingProductionVersion_SAP {
  Code:string;
  Plant:string;
  Alternative:string;
  Group:string;

  getId(){
    return "Alternative='"+this.Alternative+"',Group='"+this.Group+"',Code='"+this.Code+"',Plant='"+this.Plant+"'";
  }
}

@Injectable()
export class RoutingProductionVersionService extends GenericServiceWithSAP {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(serionetService, loggingService, sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entitySetName_SAP = "ProductionVersionSet";
    this.entityName = "RoutingProductionVersion";
    this.serviceURL = "materials/production-versions/";
  }

  getRoutingProductionVersions(query: any) {
    return this.getList(query);
  }

  saveRoutingProductionVersion(i:RoutingProductionVersion, skipSAP = false){
    return this.saveItem(i,[],{},30000,false,skipSAP);
  }

  deleteRoutingProductionVersion(i:RoutingProductionVersion, skipSAP = false){
    return this.deleteItem(i, skipSAP);
  }

  getSAPObject(i:RoutingProductionVersion){
    let pvSAP = new RoutingProductionVersion_SAP();
    pvSAP.Group = i.routing_header.group;
    pvSAP.Code = i.routing_header.material.code;
    pvSAP.Plant = i.routing_header.site.code;
    pvSAP.Alternative = i.bom_header.alt_bom_id;
    return pvSAP;
  }

  protected saveItem(item: any, parsToSkip?: string[], params?:any, timeout?:number,forceCreate?: boolean, skipSAP = false) {
    return Observable.create((observer) => {
      if (skipSAP) {
        this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
          observer.next(response__);
          observer.complete();
        })
      }
      else {
        let item_SAP = this.getSAPObject(item);

        this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP.getId()).subscribe((response) => {
          if (response.status == 200) {
            //salto l'update
            this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
              observer.next(response__);
              observer.complete();
            })
          }
          else {
            this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP).subscribe((response_) => {
              if (response_.status > 200 && response_.status < 300) {
                this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
                  observer.next(response__);
                  observer.complete();
                })
              }
              else {
                response_.context = 'SAP';
                observer.next(response_);
                observer.complete();
              }
            })
          }
        });

      }
    });
  }

  protected clearItem(item:RoutingProductionVersion){
    let i_:RoutingProductionVersion = cloneObject(item);
    i_.routing_header = {pk:i_.routing_header.pk,__str__:i_.routing_header.__str__};
    return i_;
  }
}



