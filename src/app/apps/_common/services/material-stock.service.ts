import {Injectable} from "@angular/core";
import {GenericSAPService, SAPObject} from "./_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class MaterialStock extends SAPObject{
  Material?:string;
  Plant?:string;
  Warehouse:string;
  Type:string;
  Quantity:string;
  Uom:string;

  getId(){
    return "Material='"+this.Material+"',Plant='"+this.Plant+"',Warehouse='"+this.Warehouse+"',Type='"+this.Type+"'";
  }
}

@Injectable()
export class MaterialStockService extends GenericSAPService{

  constructor(protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "ZUTILITIES_srv";
    this.entitySetName_SAP = "MaterialStockSet";
  }

  getMaterialStocks(query){
    let params = {};
    if (query) {
      let filters = [];
      if (query.Material != null) {
        let f = new sap.ui.model.Filter({
          path: 'Material',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Material
        });
        filters.push(f);
      }
      if (query.Plant != null) {
        let f = new sap.ui.model.Filter({
          path: 'Plant',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Plant
        });
        filters.push(f);
      }
      if (query.Warehouse != null) {
        let f = new sap.ui.model.Filter({
          path: 'Warehouse',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Warehouse
        });
        filters.push(f);
      }
      if (query.Type != null) {
        let f = new sap.ui.model.Filter({
          path: 'Type',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Type
        });
        filters.push(f);
      }
      params['filters'] = filters;
    }

    return this.getList(params);
  }

  getMaterialStock(id){
    return this.getItem(id);
  }

}
