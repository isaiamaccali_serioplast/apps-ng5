import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class Organization extends GenericObj {
  name?:string;
  code?:string;
  is_serioplast_group?:boolean;
  abbreviation?:string;
  is_supplier?:boolean;
  is_customer?:boolean;
}

@Injectable()
export class OrganizationService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Organizations";
    this.serviceURL = "organizations/organizations/";
  }

  getOrganizations(query: any) {
    return this.getList(query);
  }

  getOrganization(id: number) {
    return this.getItem(id);
  }

  updateOrganizations(org: Organization) {
    return this.saveItem(org);
  }

  deleteOrganization(org:Organization){
    return this.deleteItem(org);
  }

  restoreOrganization(org:Organization){
    return this.restoreItem(org);
  }
}

@Pipe({
  name: 'organization'
})
export class OrganizationPipe implements PipeTransform{

  constructor(private organizationService:OrganizationService){}

  transform(id:number) {
    return this.organizationService.getOrganization(id);
  }
}

