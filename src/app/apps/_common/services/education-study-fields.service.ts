import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class EducationStudyField extends GenericObj{
  label:string;
}

@Injectable()
export class EducationStudyFieldService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "EducationStudyField";
    this.serviceURL = "hrprofiles/educationstudyfield/";
  }

  getEducationStudyfields(query:any){
    return this.getList(query);
  }

}



