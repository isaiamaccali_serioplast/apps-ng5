import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {cloneObject} from "../../../shared/utils";

@Injectable()
export abstract class GenericServiceWithSAP extends GenericService{

  protected entityName = "";
  protected serviceURL = "";
  protected serviceName_SAP = "";
  protected entitySetName_SAP = "";

  protected constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(serionetService,loggingService);
  }

  protected abstract getSAPObject(item:any);

  protected saveItem(item: any, parsToSkip?: string[], params?:any, timeout?:number, forceCreate?: boolean, skipSAP = false) {
    return Observable.create((observer) => {
      if (skipSAP) {
        this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
          observer.next(response__);
          observer.complete();
        })
      }
      else {
        let item_SAP = this.getSAPObject(item);
        if (forceCreate) {
          this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP).subscribe((response_) => {
            if (response_.status > 200 && response_.status < 300) {
              this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
                observer.next(response__);
                observer.complete();
              })
            }
            else {
              response_.context = 'SAP';
              observer.next(response_);
              observer.complete();
            }
          });
        }
        else {
          this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP.getId()).subscribe((response) => {
            if (response.status == 200) {
              this.sapService.updateItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP.getId(), item_SAP).subscribe((response_) => {
                if (response_.status > 200 && response_.status < 300) {
                  this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
                    observer.next(response__);
                    observer.complete();
                  })
                }
                else {
                  response_.context = 'SAP';
                  observer.next(response_);
                  observer.complete();
                }
              })
            }
            else {
              this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP).subscribe((response_) => {
                if (response_.status > 200 && response_.status < 300) {
                  this.saveItemOnSerionet(item, parsToSkip).subscribe((response__) => {
                    observer.next(response__);
                    observer.complete();
                  })
                }
                else {
                  response_.context = 'SAP';
                  observer.next(response_);
                  observer.complete();
                }
              })
            }
          });
        }
      }
    });
  }

  protected deleteItem(item:any, skipSAP = false){
    return Observable.create((observer) => {
      if(!skipSAP){
        let item_SAP = this.getSAPObject(item);
        this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null,item_SAP.getId()).subscribe((response)=>{
          if(response.status==200){
            this.sapService.deleteItem(this.serviceName_SAP, this.entitySetName_SAP, null,item_SAP.getId()).subscribe((response_)=>{
              if(response_.status== 204){
                this.deleteItemOnSerionet(item).subscribe((response__)=>{
                  observer.next(response__);
                  observer.complete();
                })
              }
              else {
                response_.context = 'SAP';
                observer.next(response_);
                observer.complete();
              }
            })
          }
          else {
            this.deleteItemOnSerionet(item).subscribe((response__)=>{
              observer.next(response__);
              observer.complete();
            })
          }
        })
      }
      else {
        this.deleteItemOnSerionet(item).subscribe((response__)=>{
          observer.next(response__);
          observer.complete();
        })
      }
    });
  }

  protected deleteItemOnSerionet(item:any){
    return super.deleteItem(item);
  }

  protected saveItemOnSerionet(item:any, parsToSkip?:any[], params?:any){
    return super.saveItem(item,parsToSkip, params);
  }

  protected restoreItemOnSerionet(item:any, parsToSkip?:any[]){
    let i_ = cloneObject(item);
    i_.is_active = true;
    return super.saveItem(i_,parsToSkip);
  }
}




