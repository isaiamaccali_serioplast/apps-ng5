import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class EducationType extends GenericObj {
  label:string;
}

@Injectable()
export class EducationTypesService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "EducationType";
    this.serviceURL = "hrprofiles/educationtypes/";
  }

  getEducationTypes(query:any){
    return this.getList(query);
  }

}



