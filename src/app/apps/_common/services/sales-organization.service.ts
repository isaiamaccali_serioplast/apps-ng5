import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {HRProfile} from "./hr-profiles.service";
import {Organization} from "./organization.service";
import {LegalEntity} from "./legal-entity.service";

export class SalesOrganization extends GenericObj {
  user?:HRProfile;
  organization?:Organization;
  legal_entities?:LegalEntity[];
}

@Injectable()
export class SalesOrganizationService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "SalesOrganization";
    this.serviceURL = "organizations/sales-organization/";
  }

  getSalesOrganizations(query: any) {
    return this.getList(query);
  }

  saveSalesOrganization(salesOrg: SalesOrganization) {
    return this.saveItem(salesOrg);
  }

  deleteSalesOrganization(salesOrg:SalesOrganization){
    return this.deleteItem(salesOrg);
  }

}
