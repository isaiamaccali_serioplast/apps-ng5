import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";
import {AssetSubTypeLinkService} from "./asset-subtype-link.service";
import {ContentTypeService} from "./content-type.service";
import {Asset} from "./assets.service";

export class AssetUser {
  pk: number;
  __str__: string;
  type?: AssetUserType;
  user?: string;
  password?: string;
  asset_id?:Asset;

  status?:string;
  newly_created?:boolean;
}

export class AssetUserType {
  pk: number;
  __str__: string;
}

@Injectable()
export class AssetUsersService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Asset Users";
    this.serviceURL = "operations/assetuser/";
  }

  getAssetUsers(query?: any) {
    return this.getList(query);
  }

  deleteAssetUser(au:AssetUser){
    return this.deleteItem(au);
  }

  saveAssetUser(au:AssetUser){
    return this.saveItem(au);
  }
}

@Injectable()
export class AssetUserTypeService {

  constructor(private assetSubtypeLinkService:AssetSubTypeLinkService, private contentTypeService:ContentTypeService) {

  }

  getAssetUserTypes(query?: any) {
    return Observable.create((observer) => {
      this.contentTypeService.getContentTypes({model: "assetuser", is_active:true}).subscribe((data)=>{
        if(data && data['count'] && data['count']>0){
          let app = data['results'][0]['pk'];
          query = query || {};
          query.app = app;
          this.assetSubtypeLinkService.getAssetSubtypeLinks(query).subscribe((data)=>{
            observer.next(data);
            observer.complete();
          });
        }
      })
    });
  }
}


