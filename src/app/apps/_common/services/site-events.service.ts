/**
 * Service used to share the available sites
 */
import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {StatusLink} from "./status-link.service";
import {TypeLink} from "./type-link.service";

export class SiteEvent extends GenericObj {
  description: string;
  end_date: string;
  start_date: string;
  status_event: StatusLink;
  title: string;
  type: TypeLink;
}

@Injectable()
export class SiteEventsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Sites";
    this.serviceURL = "calendars/siteevents/";
  }

  getSiteEvents(query?: any) {
    return this.getList(query);
  }

}
