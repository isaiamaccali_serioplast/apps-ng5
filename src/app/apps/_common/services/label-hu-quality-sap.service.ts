import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class HULabel {
  Plant:string;
  LabelName:string;
  LabelDesc:string;
  SSCC:string;
  Copies:number;

  getId(){
    return "Plant='"+this.Plant+"',LabelName='"+this.LabelName+"',SSCC='"+this.SSCC+"'";
  }
}

@Injectable()
export class LabelHandlingUnitsQualitySapService {
  serviceNameSAP = "ZHUQUALITY_SRV";
  entityNameSAP = "LabelSet";

  constructor(private sapService: SAPService) {
  }

  getLabelsHandlingUnitsQuality(query?:any) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {
        if (query) {
          let filters = [];
          if(query['Plant']){
            let plantFilter = new sap.ui.model.Filter({
              path: "Plant",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Plant']
            });
            filters.push(plantFilter);
          }
          if(query['SSCC']){
            let huFilter = new sap.ui.model.Filter({
              path: "SSCC",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['SSCC']
            });
            filters.push(huFilter);
          }
          params['filters'] = filters;
        }
        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }
}



