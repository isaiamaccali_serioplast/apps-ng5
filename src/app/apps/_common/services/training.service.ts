import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LocalPackageItem} from "./localpackages.service";
import {MeasureUnit} from "./measure-unit.service";
import {HRUser} from "./hr-users.service";
import {Organization} from "./organization.service";

export class Training {
  pk?: number;
  __str__?:string;
  is_active?:boolean;
  type?:LocalPackageItem;
  name?:string;
  description?:string;
  duration?:string;
  duration_uom?:MeasureUnit; // UOM
  trainer?:HRUser;
  trainer_ext?:string;
  trainer_organization?:Organization;
  notes?:string;
  attachments?:any;

  site:GenericObj;
}

@Injectable()
export class TrainingService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Training";
    this.serviceURL = "hr-events/trainings/";
  }

  getTrainings(query: any) {
    return this.getList(query);
  }

  getTraining(id: number) {
    return this.getItem(id);
  }

  deleteTraining(training:Training){
    return this.deleteItem(training);
  }

  saveTraining(training:Training){
    return this.saveItem(training);
  }

  restoreTraining(training:Training){
    return this.restoreItem(training);
  }
}



