import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {TrainingArea} from "./training-area.service";

export class TrainingType {
  pk: number;
  __str__:string;
  label?:string;
  area?:TrainingArea;
  is_active?:boolean;
}

@Injectable()
export class TrainingTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "TrainingType";
    this.serviceURL = "hr-events/training-types/";
  }

  getTrainingTypes(query: any) {
    return this.getList(query);
  }

  getTrainingType(id: number) {
    return this.getItem(id);
  }

  deleteTrainingType(trainingType:TrainingType){
    return this.deleteItem(trainingType);
  }

  saveTrainingType(trainingType:TrainingType){
    return this.saveItem(trainingType);
  }

  restoreTrainingType(trainingType:TrainingType){
    return this.restoreItem(trainingType);
  }
}



