import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";
import {CompanyPosition} from "./company-position.service";
import {HRProfile} from "./hr-profiles.service";
import {StatusLink} from "./status-link.service";
import {PriorityLink} from "./priority-link.service";
import {HcpAdvertisement} from "./hcp-advertisement.service";

export class  HasCompanyPosition {
  pk:number;
  __str__:string;
  company_position?:CompanyPosition;
  user_ref?:HRProfile;
  status?:StatusLink;
  pre_interview_rating?:PriorityLink;
  rating?:PriorityLink;
  current_gross_salary?:number;
  current_gross_salary_currency?:number;
  expected_salary?:number;
  expected_salary_currency?:string; // TODO currencyfield model & service
  expected_benefits?:string;
  expected_level?:string;
  advertisement?:HcpAdvertisement;
  profile_updated_truthfull?:boolean;
  is_active?:boolean;
  get_company_position_obj?:CompanyPosition;
  company_positions?:string; // no model field
}

@Injectable()
export class HasCompanyPositionService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "HasCompanyPosition";
    this.serviceURL = "hrpositions/hascompanypositions/";
  }

  getHasCompanyPositions(query: any,format?:string, timeout?:number) {
    return this.getList(query,format,timeout);
  }

  saveHasCompanyPosition(hcp:HasCompanyPosition, params?:any){
    return this.saveItem(hcp, null, params);
  }

  deleteHasCompanyPosition(hcp:HasCompanyPosition){
    return this.deleteItem(hcp);
  }

  restoreHasCompanyPosition(hcp:HasCompanyPosition){
    return this.restoreItem(hcp);
  }

  getHasCompanyPosition(id: number) {
    return this.getItem(id);
  }
}

