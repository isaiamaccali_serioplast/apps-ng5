import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {Observable, Observer} from "rxjs";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {cloneObject, isEmpty} from "../../../shared/utils";
import {SAPService} from "../../../shared/sap/sap.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {MaterialPlant, MaterialPlantSAP, MaterialPlantsService} from "./material-plant.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Site, SitesService} from "./sites.service";
import {GenericSAPService} from "./_generic_sap_service";
import {_getMaterialType, _MATERIAL_TYPES, isBottleClosurePreform, MaterialType} from "./material-type.service";
import {Shape} from "./shapes.service";
import {Organization} from "./organization.service";
import {MeasureUnit} from "./measure-unit.service";
import {MaterialSubTypeLink} from "./material-subtype-link.service";
import {TechnologyLink} from "./technology-link.service";
import {MarketSegmentLink} from "./market-segment-link.service";
import {MainMaterialLink} from "./main-material-links.service";

export class Material extends GenericObj {
  description?: string;
  code?: string;
  material_type?: MaterialType;
  created_on?: string;
  shape?: Shape;
  uom?: MeasureUnit;
  weight?: number;
  weight_uom?: MeasureUnit;
  sub_model?: Submaterial;
  valuated?: boolean;
  old_version?: Material;
  material_plants?: MaterialPlant[]
}

export class Submaterial {
  pk: number;
  __str__: string;
  subtype?: MaterialSubTypeLink;
  shape?: Shape;
  pcr?: boolean;
  food_approved?: boolean;
  technology?: TechnologyLink;
  market_segment?: MarketSegmentLink;
  main_material?: MainMaterialLink;
  palletized?: boolean;
  cavities: string;
  plm_revision?: string;
  location?: Site;
  construction_year?: string;
  owner?: Organization;
}

export class Material_SAP {
  Weight_Uom: string;
  Weight: string;
  Uom: string;
  Type: string;
  Description: string;
  Code: string;
  ProdHierarchy?: string;
  MatPlantLink?: MaterialPlantSAP[];
}

@Injectable()
export class MaterialsService extends GenericService {

  serviceNameMaterial_SAP = "ZMATERIALS_SRV";
  entitySetNameMaterial_SAP = "MaterialsSet";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService,
              protected sapService: SAPService, private materialPlantsService: MaterialPlantsService,
              private sitesService: SitesService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Materials";
    this.serviceURL = "materials/materials/"
  }

  getMaterials(query: any) {
    return this.getList(query);
  }

  getMaterial(id: any) {
    return this.getItem(id);
  }

  saveMaterial(material: Material) {
    return Observable.create((observer) => {
      this.fixSites(material.material_plants, () => {
        let m = cloneObject(material);
        this.loggingService.info("Converting object to SAP format");
        let m_sap = MaterialsService.getSAPObject(m);
        this.loggingService.info("Output: " + JSON.stringify(m_sap));
        //La funzione controlla che ci sia almeno un plant attivo su sap, se non ce n'è nessuno skippa tutto sap e va dritto su serionet
        if (m_sap.MatPlantLink.length == 0) {
          this.saveMaterialOnSerionet(m, observer, false);
        }
        else {
          let matType = _getMaterialType(material);
          // MaterialType che vengono solamente aggiornati su serionet
          if (matType == _MATERIAL_TYPES.MOULDSET || matType == _MATERIAL_TYPES.COMPONENT || matType == _MATERIAL_TYPES.BILLET) {
            this.saveMaterialOnSerionet(m, observer, true);
          }
          else {
            //Altrimenti verifica se il materiale è presente su SAP
            this.sapService.getItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, "'" + m_sap.Code + "'").subscribe((resp) => {
              if (resp.status == 200) {
                //Save on SAP

                //E'presente, è un aggiornamento, rimuovo la deep entity
                this.loggingService.info("Saving material on SAP - start");
                delete m_sap['MatPlantLink'];
                this.sapService.updateItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, "'" + m_sap.Code + "'", m_sap).subscribe((response) => {
                  this.loggingService.info("Saving material on SAP - end");
                  if (response.status >= 200 && response.status < 300) {
                    //I plant verranno salvati su serionet/sap uno ad uno
                    this.saveMaterialOnSerionet(m, observer, true);
                  }
                  else {
                    this.loggingService.error("Error during updating material on SAP: " + response.message);
                    observer.next({
                      status: response.status,
                      context: "SAP",
                      message: response.message
                    })
                  }
                })
              }
              else {
                //Non è presente, è una creazione o un restore
                this.loggingService.info("Saving material on SAP (restore) - start");
                let m_sap_backup = cloneObject(m_sap);

                if (m.pk) {
                  //Il materiale ha una pk, ci sono due casi: è una restore, oppure è un materiale creato solo su serionet che va migrato a SAP. I due casi a questo livello sono indistinguibili, si tenta il primo in caso di fallimento il secondo.

                  delete m_sap['MatPlantLink'];
                  m.material_plants.forEach((mp) => {
                    //Per ripristinare i plant su SAP
                    if (mp.is_active)
                      mp.status = "toSave";
                  })
                }
                this.sapService.createItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, m_sap).subscribe((response) => {
                  this.loggingService.info("Saving material on SAP - end");
                  if (response.status >= 200 && response.status < 300) {
                    this.saveMaterialOnSerionet(m, observer, true);
                  }
                  else {
                    if (m.pk) {
                      //Faccio un secondo tentativo completo
                      this.sapService.createItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, m_sap_backup).subscribe((response) => {
                        this.loggingService.info("Saving material on SAP - end");
                        if (response.status >= 200 && response.status < 300) {
                          this.saveMaterialOnSerionet(m, observer, false);
                        }
                        else {
                          //E' fallito anche il secondo tentativo, ritorno errore
                          this.loggingService.error("Error during updating material on SAP: " + response.message);
                          observer.next({
                            status: response.status,
                            context: "SAP",
                            message: response.message
                          })
                        }
                      })
                    }
                    else {
                      //E' una creazione fallita, ritorno errore
                      this.loggingService.error("Error during updating material on SAP: " + response.message);
                      observer.next({
                        status: response.status,
                        context: "SAP",
                        message: response.message
                      })
                    }

                  }
                })
              }
            });

          }
        }
      });
    });
  }

  saveMaterialOnSerionet(m: Material, observer: Observer<any>, useSAPForMP: boolean) {
    let matToSave: Material = cloneObject(m);
    matToSave.is_active = true;
    if (!isEmpty(matToSave['sub_model'])) {
      matToSave['sub_model'] = GenericService.processItemForSave(matToSave['sub_model']);
    }
    else {
      delete matToSave['sub_model'];
    }

    let matPlants = m.material_plants || [];
    if (m.material_plants) {
      delete m['material_plants'];
    }

    this.saveItem(matToSave, ['sub_model']).subscribe((resp) => {
      if (resp.pk) {
        //processo i material plants lato serionet

        let m_ = cloneObject(m);
        m_.pk = resp.pk;

        matPlants.forEach((mp: MaterialPlant) => {
          mp.material_id = m_;
        });

        this.processMaterialPlants(matPlants, useSAPForMP, (isOK, lastErrResp) => {
          if (isOK) {
            //è ok ma torno la response del materiale
            observer.next(resp);
            observer.complete();
          }
          else {
            //errore, torno la response del material plant
            observer.next(lastErrResp);
            observer.complete();
          }
        });
      }
      else {
        observer.next(resp);
        observer.complete();
      }
    });
  }

  processMaterialPlants(matPlants: MaterialPlant[], useSAPForMP, callback: any) {
    this.processMaterialPlant(matPlants, 0, useSAPForMP, callback);
  }

  processMaterialPlant(matPlants: MaterialPlant[], index, useSAPForMP, callback) {
    if (matPlants[index]) {
      let mp = matPlants[index];
      if (mp.status == "toSave") {
        if (useSAPForMP) {
          this.materialPlantsService.saveMaterialPlant(mp).subscribe((resp) => {
            if (resp.pk) {
              this.processMaterialPlant(matPlants, index + 1, useSAPForMP, callback);
            }
            else {
              callback.call(null, false, resp);
            }
          });
        }
        else {
          this.materialPlantsService.saveItemSerionet(mp).subscribe((resp) => {
            if (resp.pk) {
              this.processMaterialPlant(matPlants, index + 1, useSAPForMP, callback);
            }
            else {
              callback.call(null, false, resp);
            }
          });
        }
      }
      else if (mp.status == "toDelete") {
        if (mp.pk) {
          if (useSAPForMP) {
            this.materialPlantsService.deleteMaterialPlant(mp).subscribe((resp) => {
              if (resp.status == 204) {
                this.processMaterialPlant(matPlants, index + 1, useSAPForMP, callback);
              }
              else {
                callback.call(null, false, resp);
              }
            });
          }
          else {
            this.materialPlantsService.deleteItemSerionet(mp).subscribe((resp) => {
              if (resp.status == 204) {
                this.processMaterialPlant(matPlants, index + 1, useSAPForMP, callback);
              }
              else {
                callback.call(null, false, resp);
              }
            });
          }
        }
        else {
          this.processMaterialPlant(matPlants, index + 1, useSAPForMP, callback);
        }
      }
      else {
        this.processMaterialPlant(matPlants, index + 1, useSAPForMP, callback);
      }
    }
    else {
      callback.call(null, true);
    }
  }

  deleteMaterialOnSerionet(m: Material, observer: Observer<any>) {
    this.deleteItem(m).subscribe((resp) => {
      observer.next(resp);
      observer.complete();
    });
  }

  static getSAPObject(m: Material) {
    let m_sap = new Material_SAP();

    if (m.weight_uom)
      m_sap.Weight_Uom = m.weight_uom.uom;
    if (m.weight != null)
      m_sap.Weight = m.weight + "";
    if (m.uom)
      m_sap.Uom = m.uom.uom;
    if (m.material_type)
      m_sap.Type = m.material_type.sap_code;
    if (m.description)
      m_sap.Description = m.description;
    if (m.code)
      m_sap.Code = m.code;

    if (isBottleClosurePreform(m) || _getMaterialType(m) == _MATERIAL_TYPES.ASSEMBLED_GOODS) {
      m_sap.ProdHierarchy = MaterialsService.calcProdHierarchy(m);
    }

    m_sap.MatPlantLink = m.material_plants
      .filter((mp) => {
        return mp.site.active_on_sap;
      })
      .map((mp) => MaterialPlantsService.convertToSAP(cloneObject(mp)));
    return m_sap;
  }

  static calcProdHierarchy(m: Material) {

    let format = (n) => {
      if (n > 10) {
        return n + "";
      }
      else {
        return "0" + n;
      }
    };

    let prodHierarchy = "";
    if (isBottleClosurePreform(m)) {
      if (m.sub_model.shape.technology.sap_hierarchy) {
        prodHierarchy += m.sub_model.shape.technology.sap_hierarchy;
        if (m.sub_model.main_material.sap_code) {
          prodHierarchy += m.sub_model.main_material.sap_code;
          if (m.sub_model.market_segment.delta_rel) {
            prodHierarchy += format(m.sub_model.market_segment.delta_rel);
            if (m.sub_model.pcr) {
              prodHierarchy += "1";
            }
            else {
              prodHierarchy += "0";
            }
          }
        }
      }
    }
    else {
      if (_getMaterialType(m) == _MATERIAL_TYPES.ASSEMBLED_GOODS) {
        prodHierarchy += "08";
        if (m.sub_model.main_material.sap_code) {
          prodHierarchy += m.sub_model.main_material.sap_code;
          if (m.sub_model.market_segment.delta_rel) {
            prodHierarchy += format(m.sub_model.market_segment.delta_rel);
            prodHierarchy += "0";
          }
        }
      }
    }
    return prodHierarchy;
  }

  deleteMaterial(m: Material) {
    return Observable.create((observer) => {
      this.sapService.getItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, "'" + m.code + "'").subscribe((resp) => {
        if (resp.status == 200) {
          this.sapService.deleteItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, "'" + m.code + "'").subscribe((response) => {
            if (response.status == 204) {
              this.loggingService.info("Deleting data");
              this.deleteMaterialOnSerionet(m, observer);
            }
            else {
              let resp = {
                status: response.status,
                message: response.message,
                context: 'SAP'
              };
              observer.next(resp);
              observer.complete();
            }
          });
        }
        else {
          this.deleteMaterialOnSerionet(m, observer);
        }
      });
    });
  }

  isCodeAvailable(code: string) {
    return Observable.create((observer) => {
      this.sapService.getItem(this.serviceNameMaterial_SAP, this.entitySetNameMaterial_SAP, null, "'" + code + "'").subscribe((response) => {
        if (response.status == 200) {
          observer.next(false);
          observer.complete();
        }
        else {
          this.getMaterials({code: code}).subscribe((data) => {
            if (data && data['count'] && data['count'] > 0) {
              observer.next(false);
              observer.complete();
            }
            else {
              observer.next(true);
              observer.complete();
            }
          });
        }
      });
    });
  }

  fixSites(materialPlants: MaterialPlant[], callback) {
    this.fixSite(materialPlants, 0, callback);
  }

  fixSite(materialPlants: MaterialPlant[], index, callback) {
    if (materialPlants[index]) {
      this.sitesService.getSite(materialPlants[index].site.pk).subscribe((site) => {
        materialPlants[index].site = site;
        this.fixSite(materialPlants, index + 1, callback);
      })
    }
    else {
      callback.call(null);
    }
  }
}

declare let sap: any;

export class MaterialSAP {
  Code: string;
  Description: string;
  Type: string;
  Uom: string;
  Weight: number;
  Weight_Uom: string;
  Material_Group: string;
  ProdHierarchy: string;
  MatPlantLink?: {
    results: MaterialPlantSAP[];
  }
}

@Injectable()
export class MaterialsSAPService extends GenericSAPService {

  constructor(protected sapService: SAPService) {
    super(sapService);
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entitySetName_SAP = "MaterialsSet";
  }

  getMaterials(query: any) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        let params = {};
        if (query) {
          let filters = [];
          if (query.Code != null) {
            let filter = new sap.ui.model.Filter({
              path: 'Code',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Code']
            });
            filters.push(filter);
          }
          params['filters'] = filters;
        }
        return this.getList(params).subscribe((resp) => {
          observer.next(resp);
          observer.complete();
        });
      })
    })
  }

  getMaterial(id: any) {
    return this.getItem(id);
  }
}

@Pipe({
  name: 'material'
})
export class MaterialPipe implements PipeTransform {

  constructor(private materialsService: MaterialsService) {
  }

  transform(id: number) {
    return this.materialsService.getMaterial(id);
  }
}

@Pipe({
  name: 'materialCode'
})
export class MaterialCodePipe implements PipeTransform {

  constructor(private materialsService: MaterialsService) {
  }

  transform(code: string) {
    if (code) {
      return Observable.create((observer) => {
        this.materialsService.getMaterials({code: code, is_active: true}).subscribe((resp) => {
          if (resp.count && resp.count > 0) {
            observer.next(resp['results'][0]);
            observer.complete();
          }
        });
      });
    }
    else {
      return code;
    }

  }
}
