import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {GenericSAPService} from "./_generic_sap_service";

declare let sap;

@Injectable()
export class RoutingOperationActivityService extends GenericSAPService{

  serviceName_SAP = "ZMATERIALS_SRV";
  entitySetName_SAP = "RoutingOpActivitySet";

  constructor(protected sapService:SAPService) {
    super(sapService)
  }

  getRoutingOperationActivities(query: any) {
    return Observable.create((observer)=>{
      let params = {};
      if (query) {
        let filters = [];

        if (query.Code != null) {
          let f = new sap.ui.model.Filter({
            path: 'Code',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Code
          });
          filters.push(f);
        }
        if (query.Plant != null) {
          let f = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Plant
          });
          filters.push(f);
        }
        if (query.Group != null) {
          let f = new sap.ui.model.Filter({
            path: 'Group',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Group
          });
          filters.push(f);
        }
        if (query.Operation != null) {
          let f = new sap.ui.model.Filter({
            path: 'Operation',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Operation
          });
          filters.push(f);
        }
        params['filters'] = filters;
      }

      this.sapService.getItems(this.serviceName_SAP, this.entitySetName_SAP, null, params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    })
  }

}



