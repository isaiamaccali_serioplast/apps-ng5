import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {ContentType} from "./content-type.service";
import {Department} from "./department.service";
import {GenericObj, GenericService} from "./_generic.service";


export class WorkToolType extends GenericObj{
  name: string;
  model_reference: ContentType;
  filter?: string;
  asset_tool: boolean;
  editable: boolean;
  by_plant: boolean;
  dept_ref: Department;
  objects?:GenericObj[];

  evalCheck(){
    if (this.editable==false){
      this.editable=false;}
  }
}

@Injectable()
export class WorkToolTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "WorktoolType";
    this.serviceURL = "packages/worktooltypes/";
  }


  getWorkToolTypes(query: any) {
    return this.getList(query);
  }


  addWorkToolType(worktooltype: WorkToolType) {

    let data = {
      name: worktooltype.name,
      model_reference: worktooltype.model_reference.pk,
      filter: worktooltype.filter,
      asset_tool: worktooltype.asset_tool,
      editable: worktooltype.editable,
      by_plant: worktooltype.by_plant,
      dept_ref: worktooltype.dept_ref.pk,
    };

    return this.saveItem(data);
  }

  updateWorkToolType(worktooltype: WorkToolType) {

    let data = {
      name: worktooltype.name,
      model_reference: worktooltype.model_reference.pk,
      filter: worktooltype.filter,
      asset_tool: worktooltype.asset_tool,
      editable: worktooltype.editable,
      by_plant: worktooltype.by_plant,
      dept_ref: worktooltype.dept_ref.pk,
    };

    return this.saveItem(data);

  }
}



