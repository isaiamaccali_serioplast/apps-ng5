import {GenericObj, GenericService} from "./_generic.service";
import {Injectable} from "@angular/core";
import {Material} from "./materials.service";
import {Site} from "./sites.service";
import {HRProfile} from "./hr-profiles.service";
import {ControlPlan} from "./control-plans.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {TypeLink} from "./type-link.service";
import {SPFile} from "./file.service";
import {RoutingProductionVersion} from "./routing-production-version-service";
import {PackingInstruction} from "./packing-instructions.service";
import {Routing} from "./routings.service";
import {cloneObject} from "../../../shared/utils";
import {StatusLink} from "./status-link.service";
import {ProjectTask} from "./project-task.service";
import {WorkorderLabelSAP} from "./workorder-label-sap.service";

export class WorkorderSerionet extends GenericObj {
  type: TypeLink;
  work_order_nr: string;
  confirmed_quantity: number;
  quantity: number;
  site: Site;
  material: Material;
  routing: Routing;
  production_version: RoutingProductionVersion;
  packing_instruction: PackingInstruction;
  control_plans: ControlPlan[];
  original_start_date: string;
  original_end_date: string;
  planned_start: string;
  planned_end: string;
  wbs?: ProjectTask;
  team: HRProfile[];
  notes: string;
  report: SPFile;
  sap_actual_end?: string;
  sap_actual_start?: string;
  sap_status?: string;
  status?:StatusLink;
  bulk_packing_instruction:boolean;
  labels:WorkorderLabelSAP[];
  type_sap_code:string;

  _newFlow: boolean;
  labels_old:WorkorderLabelSAP[];
}

@Injectable()
export class WorkordersSerionetService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Workorders";
    this.serviceURL = "operations/workorders/";
  }

  getWorkordersSerionet(query, params?:any) {
    return this.getList(query, params, 120000);
  }

  getWorkorderSerionet(id) {
    return this.getItem(id);
  }

  saveWorkorderSerionet(item: WorkorderSerionet, params?:any) {
    return this.saveItem(item, ['labels'],params, 60000);
  }

  deleteWorkorderSerionet(item: WorkorderSerionet) {
    return this.deleteItem(item);
  }

  clearItem(item: WorkorderSerionet) {
    let i_:WorkorderSerionet = cloneObject(item);
    delete i_._newFlow;
    if(i_.bulk_packing_instruction || (i_.packing_instruction && i_.packing_instruction.pk==-1))
      i_.packing_instruction = null;
    delete i_.labels_old;

    return i_;
  }
}


