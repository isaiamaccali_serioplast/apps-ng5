import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class Status {
  pk: number;
  __str__: string;
  text_color?:string;
  color?:string;
  label?:string;
}

@Injectable()
export class StatusService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Status";
    this.serviceURL = "statuses/status-id/";
  }

  getStatuses(query?: any) {
    return this.getList(query);
  }

  getStatus(id: number) {
    return this.getItem(id);
  }
}


