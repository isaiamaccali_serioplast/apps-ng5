import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {PriorityLink} from "./priority-link.service";
import {DjangoLanguage} from "./django-languages.service";
import {GenericService} from "./_generic.service";

export class Language {
  user_id : number;
  language: DjangoLanguage;
  level: PriorityLink;
  __str__:string;
  is_active:boolean;
}

@Injectable()
export class LanguageService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Language";
    this.serviceURL = "hrprofiles/language/";
  }

  getLanguages(query: any) {
    return this.getList(query);
  }

  deleteLanguage(l:Language){
    return this.deleteItem(l);
  }

  saveLanguage(l:Language){
    return this.saveItem(l);
  }

}
