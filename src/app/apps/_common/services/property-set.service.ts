import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Material} from "./materials.service";
import {Property} from "./properties.service";

export class PropertySet extends GenericObj {
  material?:Material;
  property?:Property;

  status?:string;
}

@Injectable()
export class PropertySetService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Property Set";
    this.serviceURL = "qc/propertyset/";
  }

  getPropertySets(query: any) {
    return this.getList(query);
  }

  getPropertySet(id: number) {
    return this.getItem(id);
  }

  savePropertySet(ps: PropertySet) {
    return this.saveItem(ps);
  }

  deletePropertySet(ps: PropertySet) {
    return this.deleteItem(ps);
  }
}
