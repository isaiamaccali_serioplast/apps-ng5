import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class OrganizationLevel extends GenericObj{
  name?:string;
}

@Injectable()
export class OrganizationLevelService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Organization Levels";
    this.serviceURL = "organizations/organizations-levels/";
  }

  getOrganizationLevels(query:any) {
    return this.getList(query);
  }

  saveOrganizationLevel(ol:OrganizationLevel){
    return this.saveItem(ol);
  }
  getOrganizationLevel(id:number) {
    return this.getItem(id,true);
  }

  deleteOrganizationLevel(ol:OrganizationLevel){
    return this.deleteItem(ol);
  }

  restoreOrganizationLevel(ol:OrganizationLevel){
    return this.restoreItem(ol);
  }
}


