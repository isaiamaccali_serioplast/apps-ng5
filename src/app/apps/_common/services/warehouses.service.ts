import {Injectable} from "@angular/core";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {Site} from "./sites.service";

export class Warehouse{
  pk:number;
  code?:string;
  description?:string;
  sites?:Site[];
  is_active?:boolean;
  __str__:string;
}

export class Warehouse_SAP{
  Plant:string;
  WarehouseCode:string;
  Description:string;
  HandleHU:boolean;

  WHMatTypeLink?:{
    results:Warehouse_SAP_MatType[]
  }

  getId(){
    return "WarehouseCode='"+this.WarehouseCode+"',Plant='"+this.Plant+"'";
  }
}

export class Warehouse_SAP_MatType {
  MaterialTypeCode:string;
  Plant:string;
  WarehouseCode:string;
}

@Injectable()
export class WarehouseService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Warehouses";
    this.serviceURL = "organizations/warehouses/";
  }

  getWarehouses(query:any) {
    return this.getList(query);
  }

  getWarehouse(id:number) {
    return this.getItem(id);
  }

  deleteWarehouse(wh:Warehouse) {
    return this.deleteItem(wh);
  }

  saveWarehouse(wh:Warehouse) {
    return this.saveItem(wh);
  }
}

declare let sap: any;

@Injectable()
export class WarehouseSAPService extends GenericSAPService{

  constructor(protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "zship_srv";
    this.entitySetName_SAP = "WarehouseSet";
  }

  getWarehouseSAPs(query: any) {
    let params = {};
    let filters = [];
    if (query['Plant']) {
      let plantFilter = new sap.ui.model.Filter({
        path: "Plant",
        operator: sap.ui.model.FilterOperator.EQ,
        value1: query['Plant']
      });
      filters.push(plantFilter);
    }
    if (query['WarehouseCode_start']) {
      let filter1 = new sap.ui.model.Filter({
        path: 'WarehouseCode',
        operator: sap.ui.model.FilterOperator.StartsWith,
        value1: query['WarehouseCode_start']
      });
      filters.push(filter1);
    }
    params['filters'] = filters;
    params['urlParameters'] = {$expand: 'WHMatTypeLink'};
    return this.getList(params);
  }
}





