import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {User} from "./users.service";

export class SerionetComment extends GenericObj {
  created_by?: User;
  created_on?: string;
  message?: string;
  content_type?: number;
  object_id?: number;
}

@Injectable()
export class CommentsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.serviceURL = "core/comments/";
    this.entityName = "Comments";
  }

  getComments(query) {
    return this.getList(query);
  }

  saveComment(comment: SerionetComment) {
    return this.saveItem(comment);
  }

}

