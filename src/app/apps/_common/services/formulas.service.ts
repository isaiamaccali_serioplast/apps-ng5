import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class Formula extends GenericObj{
  args:string;
  description:string;
}

@Injectable()
export class FormulasService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Formulas";
    this.serviceURL = "qc/formulalist/";
  }

  getFormulas(query:any) {
    return this.getList(query);
  }
}



