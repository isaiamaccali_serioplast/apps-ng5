import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {Material} from "./materials.service";
import {Supplier} from "./suppliers.service";

export class MaterialSupplier extends GenericObj {
  material: Material;
  supplier: Supplier
}

@Injectable()
export class MaterialSupplierService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Material Supplier";
    this.serviceURL = "materials/material-suppliers/"
  }

  getMaterialSuppliers(query: any) {
    return this.getList(query);
  }

  getMaterialSupplier(id: number) {
    return this.getItem(id);
  }

  saveMaterialSupplier(ms: MaterialSupplier) {
    return this.saveItem(ms);
  }

  deleteMaterialSupplier(ms: MaterialSupplier) {
    return this.deleteItem(ms);
  }
}




