import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";

export class SkillType {
  value:string;
  label:string;
}

@Injectable()
export class SkillTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "SkillType";
    this.serviceURL = "hrpositions/skill-types/";
  }

  getSkillTypes(query: any) {
    return this.getList(query);
  }
}


