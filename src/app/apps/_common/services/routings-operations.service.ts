import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {cloneObject} from "../../../shared/utils";
import {Material} from "./materials.service";
import {Routing} from "./routings.service";
import {GenericObj} from "./_generic.service";
import {SAPObject} from "./_generic_sap_service";
import {Workcenter} from "./workcenters.service";
import {TechnologyLink} from "./technology-link.service";
import {Supplier} from "./suppliers.service";
import {MeasureUnit} from "./measure-unit.service";
import {MaterialGroup} from "./material-group.service";

export class RoutingOperation extends GenericObj{
  operation?:string;
  work_center?:Workcenter;
  description:string;
  employees?:number;
  routing_header?:Routing;
  base_quantity:number;
  material_group?:MaterialGroup;
  vendor:Supplier;
  uom:MeasureUnit;
  prt?:Material[]; //Mouldset aperto nel plant del routing (opzionale)

  subcontracting?:boolean;
  activities?:RoutingOperationActivity_SAP[];
  technologyLink:TechnologyLink;
  status?:string;
  active_work_centers?:Workcenter[];
  mouldsets?:Material[] = [];
  technologies?:TechnologyLink[];
}

export class RoutingOperation_SAP extends SAPObject{
  Code:string;
  Plant:string;
  Group:string;
  Operation:string;
  WorkCenter:string;
  Description:string;
  MaterialGroup:string;
  Vendor:string;
  UoM:string;

  Employees:string;
  BaseQuantity:string;
  ActivityLink:RoutingOperationActivity_SAP[];
  PRTLink:RoutingOperationPRT_SAP[];

  getId(){
    return "Operation='"+this.Operation+"',Group='"+this.Group+"',Code='"+this.Code+"',Plant='"+this.Plant+"'";
  }
}

export class RoutingOperationActivity_SAP extends SAPObject{
  Code:string;
  Plant:string;
  Group:string;
  Operation:string;
  Activity:string;
  Description:string;
  Value:string;
  Unit:string;

  getId(){
    return "Operation='"+this.Operation+"',Group='"+this.Group+"',Code='"+this.Code+"',Plant='"+this.Plant+"',Activity='"+this.Activity+"'";
  }
}

export class RoutingOperationPRT_SAP extends SAPObject{
  Code:string;
  Plant:string;
  Group:string;
  Operation:string;
  PRT:string;

  getId(){
    return "Operation='"+this.Operation+"',Group='"+this.Group+"',Code='"+this.Code+"',Plant='"+this.Plant+"',PRT='"+this.PRT+"'";
  }
}


@Injectable()
export class RoutingsOperationsService extends GenericServiceWithSAP {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(serionetService, loggingService, sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "RoutingOperationSet";
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entityName = "Routing Operation";
    this.serviceURL = "materials/routing-operations/";
  }

  getRoutingOperations(query: any) {
    return this.getList(query);
  }

  getRoutingOperation(id: number) {
    return this.getItem(id);
  }

  saveRoutingOperation(i:RoutingOperation, skipSAP = false){
    let i_ = cloneObject(i);
    if(!i_.prt || !i_.prt[0] || !i_.prt[0].pk) delete i_['prt'];
    return this.saveItem(i_, ['activities'],null, null,true, skipSAP);
  }

  deleteRoutingOperation(i:RoutingOperation, skipSAP = false){
    return this.deleteItem(i, skipSAP);
  }

  getSAPObject(i:RoutingOperation){
    return RoutingsOperationsService.getSAPObject(i);
  }

  static getSAPObject(i:RoutingOperation){
    let routingOPSAP = new RoutingOperation_SAP();
    routingOPSAP.Code = i.routing_header.material.code;
    if(i.routing_header.group){
      routingOPSAP.Group = i.routing_header.group||null;
    }
    else {
      delete routingOPSAP.Group;
    }

    routingOPSAP.Description = i.routing_header.description;
    routingOPSAP.Plant = i.routing_header.site.code;
    routingOPSAP.Description = i.description;
    routingOPSAP.Operation = i.operation;

    routingOPSAP.BaseQuantity = i.base_quantity+"";
    routingOPSAP.UoM = i.uom.__str__;

    if(!i.subcontracting){
      routingOPSAP.Employees = i.employees+"";
      routingOPSAP.WorkCenter = i.work_center.sap_code;

      if(i.prt && i.prt[0] && i.prt[0].pk){
        routingOPSAP.PRTLink = i.prt.map((prt:Material)=>{
          let prtLink = new RoutingOperationPRT_SAP();
          prtLink.Code = i.routing_header.material.code;
          prtLink.Plant = i.routing_header.site.code;
          if(i.routing_header.group) {
            prtLink.Group = i.routing_header.group;
          }
          prtLink.Operation = i.operation;
          prtLink.PRT = prt.code;
          return prtLink;
        });
      }
      else {
        delete routingOPSAP.PRTLink;
      }

      if(i.activities && i.activities.length>0){
        routingOPSAP.ActivityLink =
          i.activities.map((roa:RoutingOperationActivity_SAP)=>{
            roa.Plant = routingOPSAP.Plant;
            roa.Code = routingOPSAP.Code;
            if(!roa.Activity){
              roa.Activity = " ";
            }
            if(!roa.Unit){
              roa.Unit = " ";
            }
            if(routingOPSAP.Group){
              roa.Group = routingOPSAP.Group;
            }
            else {
              delete roa.Group;
            }
            if(roa.Value==null){
              roa.Value = " ";
            }
            else
              roa.Value+="";
            return roa;
          });
      }
    }
    else {
      routingOPSAP.MaterialGroup = i.material_group.sap_code;
      routingOPSAP.Vendor = i.vendor.sap_code;

      //IMPORTANTE: quello che segue è una pezza per sistemare un comportamento strano di SAP, fa validare il JSON ma non salva nulla.
      let roa = new RoutingOperationActivity_SAP();
      roa.Plant = routingOPSAP.Plant;
      roa.Code = routingOPSAP.Code;
      roa.Activity = " ";

      roa.Unit = " ";

      if(routingOPSAP.Group){
        roa.Group = routingOPSAP.Group;
      }
      else {
        delete roa.Group;
      }
      roa.Value = " ";

      routingOPSAP.ActivityLink = [
        roa
      ]
    }

    return routingOPSAP;
  }

  protected clearItem(item:RoutingOperation){
    let i_:RoutingOperation = cloneObject(item);
    if(i_.activities)
      delete i_.activities;
    if(i_.technologyLink)
      delete i_.technologyLink;
    if(i_.status)
      delete i_.status;
    let r = new Routing();
    r.pk = i_.routing_header.pk;
    i_.routing_header = r;
    delete i_.subcontracting;
    delete i_.mouldsets;
    delete i_.active_work_centers;
    return i_;
  }
}


