import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

export class PriorityLink extends GenericObj {
  priority_id?:Priority;
  delta_rel?:number;
  fieldname?:string;
  app?:GenericObj;
}

export class Priority {
  pk:number;
  __str__:string;
  label?:string;
  text_color?:string;
  icon?:string;
  is_active?:boolean;
}

@Injectable()
export class PriorityLinkService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Priority";
    this.serviceURL = "statuses/priority-link/";
  }

  getPriorityLinks(query: any) {
    return this.getList(query);
  }

  getPriorityLink(id: number) {
    return this.getItem(id,true);
  }
}



