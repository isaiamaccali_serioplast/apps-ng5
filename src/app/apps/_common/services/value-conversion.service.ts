import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

export class ValueConversion {
  ActualCurrency: string;
  ActualValue: number;
  ConversionCurrency: string;
  ConvertedValue: number;

  getId = ()=>{
    return "ActualCurrency='"+this.ActualCurrency+"',ActualValue='"+this.ActualValue+"',ConversionCurrency='"+this.ConversionCurrency+"'";
  }
}

@Injectable()
export class ValueConversionService extends GenericSAPService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "ValueConversionSet";
    this.serviceName_SAP = "ZUTILITIES_srv";
  }

  getValueConversion(id: any) {
    return this.getItem(id);
  }

}


