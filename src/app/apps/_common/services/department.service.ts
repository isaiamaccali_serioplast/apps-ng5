import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Organization} from "./organization.service";

export class Department extends GenericObj{
  name?:string;
  code?:string;
  organization_ref?:Organization;
}

@Injectable()
export class DepartmentService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Department";
    this.serviceURL = "organizations/departments/";
  }

  getDeparments(query: any) {
    return this.getList(query);
  }

  getDepartment(id: number) {
    return this.getItem(id);
  }


}
