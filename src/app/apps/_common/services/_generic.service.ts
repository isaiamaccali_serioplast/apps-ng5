import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {cloneObject, getUrlDate} from "../../../shared/utils";
import {isArray, isDate, isNullOrUndefined} from "util";
import {isObject} from "util";
import {HttpParams} from "@angular/common/http";

export class GenericObj {
  pk: number;
  __str__: string;
  is_active?: boolean;
  created_on?: string;
}

@Injectable()
export class GenericService {

  protected entityName = "";
  protected serviceURL = "";
  protected preserveObject = false;

  protected constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    this.init();
  }

  init() {}

  protected getList(query: any, format?: string, timeout?: number) {
    return Observable.create((observer) => {

      let urlParams = new HttpParams();

      for (let key in query) {
        if (query.hasOwnProperty(key) && !isNullOrUndefined(query[key]))
          urlParams = urlParams.set(key, query[key]);
      }

      if (!query.hasOwnProperty("is_active"))
        this.loggingService.warn("Query for " + this.entityName + " has no is_active set: " + JSON.stringify(query));

      if (format)
        urlParams = urlParams.set('custom_format', format);

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;

      if (!urlParams.get('custom_format')) {
        this.serionetService.doGet(this.serviceURL, serionetOptions).subscribe((data) => {
            observer.next(data);
            observer.complete();
          },
          (response) => {
            this.loggingService.error("Error in retreving " + this.entityName + ", status: " + response.status);
            response.context = "SERIONET";
            observer.next(response);
            observer.complete();
          })
      }
      else {
        this.serionetService.doGet(this.serviceURL, serionetOptions).subscribe(
          (data) => {
            observer.next(data);
            observer.complete();
          },
          (response) => {
            this.loggingService.error("Error in retreving " + this.entityName + ", status: " + response.status);
            response.context = "SERIONET";
            observer.next(response);
            observer.complete();
          });
      }
    });
  }

  protected getItem(id: number, useParam?: boolean, revisionDate?: string, params?: any, timeout?: number) {
    return Observable.create((observer) => {
      let urlParams = new HttpParams();

      if (params) {
        for (let key in params) {
          if (params.hasOwnProperty(key))
            urlParams = urlParams.set(key, params[key]);
        }
      }

      let url = this.serviceURL + id;
      if (useParam) {
        urlParams = urlParams.set("id", id + "");
        url = this.serviceURL;
      }
      if (revisionDate) {
        urlParams = urlParams.set("version_in_date", revisionDate);
      }

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;
      this.serionetService.doGet(url, serionetOptions).subscribe(
        (data) => {
          if (useParam) {
            if (data && data['count'] && data['count'] > 0) {
              observer.next(data['results'][0]);
            }
          }
          else {
            observer.next(data);
          }
          observer.complete();
        },
        (response) => {
          this.loggingService.error("Error in retreving " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        }
      );
    });
  }

  protected saveItem(item: any, parsToSkip?: string[], params?: any, timeout?: number) {
    return Observable.create((observer) => {
      let urlParams = new HttpParams();

      if (params) {
        for (let key in params) {
          if (params.hasOwnProperty(key))
            urlParams = urlParams.set(key, params[key]);
        }
      }

      let i_;
      if (this.preserveObject) {
        i_ = cloneObject(item);
      }
      else {
        i_ = GenericService.processItemForSave(this.clearItem(item), parsToSkip);
      }

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;

      if (i_.pk) {
        this.serionetService.doPut(this.serviceURL + i_.pk, i_, serionetOptions).subscribe(
          (data) => {
            observer.next(data);
            observer.complete();
          },
          (response) => {
            this.loggingService.error("Error in updating " + this.entityName + ", status: " + response.status);
            observer.next(response);
            observer.complete();
          });
      }
      else {
        this.serionetService.doPost(this.serviceURL, i_, serionetOptions).subscribe(
          (data) => {
            observer.next(data);
            observer.complete();
          },
          (response) => {
            this.loggingService.error("Error in creating " + this.entityName + ", status: " + response.status);
            observer.next(response);
            observer.complete();
          });
      }
    });
  }

  static processItemForSave(item: any, parsToSkip?: string[]) {
    let i_ = cloneObject(item);
    for (let key in i_) {
      if (parsToSkip && parsToSkip.indexOf(key) != -1) {
        //if some object attributes are tricky to handle, they are managed
        // by the particular service and sent to server as they are
      }
      else {
        if (isArray(i_[key])) {
          i_[key] = i_[key].map((i) =>{
            return i.pk;
          })
        }
        else {
          if(isDate(i_[key])) {
            i_[key] = getUrlDate(i_[key]);
          }
          else if (isObject(i_[key])) {
            i_[key] = i_[key].pk;
          }
        }
      }
    }
    if (typeof i_.is_active == 'undefined')
      i_.is_active = true;
    return i_;
  }

  protected deleteItem(item: any, params?: any, timeout?: number) {
    return Observable.create((observer) => {
      let urlParams = new HttpParams();

      if (params) {
        for (let key in params) {
          if (params.hasOwnProperty(key))
            urlParams = urlParams.set(key, params[key]);
        }
      }

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;

      this.serionetService.doDelete(this.serviceURL + item.pk, serionetOptions).subscribe((data) => {
          observer.next(data);
          observer.complete();
        },
        (response) => {
          this.loggingService.error("Error in deleting " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        }
      );
    });
  }

  protected restoreItem(item: any, parsToSkip?: string[], params?: any, timeout?: number) {
    return Observable.create((observer) => {
      let item_ = cloneObject(item);
      item_.is_active = true;

      this.saveItem(item_, parsToSkip, params, timeout).subscribe((data) => {
        observer.next(data);
        observer.complete();
      })
    });
  }

  //Metodo sovrascrivibile dai vari servizi per togliere le chiavi ridondanti o problematiche prima di spedire a serionet
  protected clearItem(item: any) {
    return item;
  }

  /*
   * Metodi dummy, nel caso vengano chiamati danno errore (ma permettono la compilazione
   */
  moveUpItem(item: any) {
    return this.moveItem(item, "up");
  }

  moveDownItem(item: any) {
    return this.moveItem(item, "down");
  }

  moveItem(item: any, direction: string) {
    return Observable.create((observer) => {
      observer.next({status: 10000, message: "Unmovable object", context: "SERIONET"});
      observer.complete();
    });
  }

  moveTo(item: any, position: number) {
    return Observable.create((observer) => {
      observer.next({status: 10000, message: "Unmovable object", context: "SERIONET"});
      observer.complete();
    });
  }
}
