import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

export class TypeLink extends GenericObj{
  fieldname?: string;
  app?: GenericObj;
  delta_rel?: number;
  type_id?: GenericObj;
}

@Injectable()
export class TypeLinkService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Type Link";
    this.serviceURL = "statuses/typelink/";
  }

  getTypeLinks(query: any) {
    return this.getList(query);
  }

  getTypeLink(id: number) {
    return this.getItem(id);
  }
}



