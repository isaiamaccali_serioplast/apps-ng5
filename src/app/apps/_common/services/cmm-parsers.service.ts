import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class CMMParser extends GenericObj {
  parser: string;
  description: string;
}

@Injectable()
export class CMMParsersService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Parsers";
    this.serviceURL = "qc/parsers/";
  }

  getCMMParsers(query: any) {
    return this.getList(query);
  }
}



