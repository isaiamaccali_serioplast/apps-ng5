import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SessionStorageService} from "../../../shared/session-storage/session-storage.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {LoggingService} from "../../../shared/logging/logging.service";

declare let sap: any;

export class HandlingUnitSap {
  SSCC: string;
  WorkOrder?: string;
  PackagingMaterial?: string;
  Material?: string;
  PackedQty?: any;
  Batch?: string;
  Palletization?: string;
  Plant?: string;
  Warehouse?: string;
  PrinterName?:string;
  CID?:string;
}


@Injectable()
export class HandlingUnitSapService {
  entityName = "HandlingUnit";
  serviceNameSAP = "zworkorders_srv";
  entityNameSAP = "HandlingUnitSet";

  constructor(private sapService: SAPService) {
  }

  getHandlingUnit(id: string) {
    return Observable.create((observer) => {
      id = "'" + id + "'";
      this.sapService.getItem(this.serviceNameSAP, this.entityNameSAP, null, id, null).subscribe((response) => {
        observer.next(response);
        observer.complete();
      });
    });
  }

  deleteHandlingUnit(id: string) {
    return Observable.create((observer) => {
      id = "'" + id + "'";
      this.sapService.deleteItem(this.serviceNameSAP, this.entityNameSAP, null, id, null).subscribe((response) => {
        observer.next(response);
        observer.complete();
      });
    });
  }

  getHandlingUnits(query) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {


        if (query) {
          let filters = [];
          if (query.SSCC != null) {
            let ssccFilter = new sap.ui.model.Filter({
              path: 'SSCC',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.SSCC
            });
            filters.push(ssccFilter);
          }
          params['filters'] = filters;
        }
        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }


  createHandlingUnit(item: HandlingUnitSap) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        this.sapService.createItem(this.serviceNameSAP, this.entityNameSAP, null, item).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  updateHandlingUnit(item:HandlingUnitSap) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {

        let key = "'" + item.SSCC + "'";
        this.sapService.updateItem(this.serviceNameSAP,this.entityNameSAP,null,key,item).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }


}



