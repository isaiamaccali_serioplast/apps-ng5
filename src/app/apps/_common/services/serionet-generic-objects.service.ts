import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class SerionetGenericObject {
  pk: number;
  __str__:string;
  is_active?:boolean;
  content_object?: any;
  content_type?: number;
  object_id?: number;
}


@Injectable()
export class SerionetGenericObjectService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "GenericObj";
    this.serviceURL = "core/generic-objects/";
  }

  getSerionetGenericObjects(query?: any) {
    return this.getList(query);
  }

}
