import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {HRUser} from "./hr-users.service";
import {DayWeekUserMilestone} from "./day-week-user-milestone-service";
import {GenericObj, GenericService} from "./_generic.service";
import {LegalEntity} from "./legal-entity.service";

export class Contract extends GenericObj {
  user: HRUser;
  hierarchical_superior: HRUser;
  //functional_superior: HRUser;
  start_date: string;
  end_date?: string;
  legal_entity?:LegalEntity;

  milestones?:DayWeekUserMilestone[];
}

@Injectable()
export class ContractService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Contract";
    this.serviceURL = "salaries/contracts/";
  }

  getContracts(query: any) {
    return this.getList(query);
  }

  getContract(id: number) {
    return this.getItem(id);
  }

  deleteContract(item:Contract){
    return this.deleteItem(item);
  }

  saveContract(item:Contract){
    return this.saveItem(item);
  }

}
