import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericSortableObj} from "./_generic-sortable.service";

export class Frequency extends GenericSortableObj{
  code?:string;
  interval?: number;
  remaining?: string;
}

@Injectable()
export class FrequencyService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Frequencies";
    this.serviceURL = "core/frequencies/";
  }

  getFrequencies(query: any) {
    return this.getList(query);
  }
}



