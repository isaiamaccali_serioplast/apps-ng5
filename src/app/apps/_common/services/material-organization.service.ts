import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {Material} from "./materials.service";
import {Organization} from "./organization.service";

export class MaterialOrganization extends GenericObj {
  material: Material;
  organization: Organization
}

@Injectable()
export class MaterialOrganizationService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Material Organization";
    this.serviceURL = "materials/material-organizations/"
  }

  getMaterialOrganizations(query: any) {
    return this.getList(query);
  }

  getMaterialOrganization(id: number) {
    return this.getItem(id);
  }

  saveMaterialOrganization(mo:MaterialOrganization) {
    return this.saveItem(mo);
  }

  deleteMaterialOrganization(mo:MaterialOrganization) {
    return this.deleteItem(mo);
  }
}




