import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";
declare let sap: any;

export class RFIDTag {
  Plant?:string;
  Material?:string;
  Printer?:string;
  Copies?:number;
}

@Injectable()
export class RFIDTagService extends GenericSAPService{

  constructor(protected sapService: SAPService) {
    super(sapService);
    this.init();
  }

  init(){
    this.serviceName_SAP = "ZOUTPUT_SRV";
    this.entitySetName_SAP = "RFIDTagSet";
  }

  saveRFIDTag(item:RFIDTag) {
    return this.saveItem(item,true);
  }

}



