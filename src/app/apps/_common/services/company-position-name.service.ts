import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Skill} from "./skill.service";
import {Department} from "./department.service";
import {OrganizationLevel} from "./organization-level.service";
import {PackageName} from "./packages-name.service";
import {LocalCpnPackageItem} from "./localcpnpackageitem.service";
import {CompanyPositionHierarchy} from "./company-position-hierarchy.service";
import {Area} from "./areas.service";

export class CompanyPositionName extends GenericObj{
  name?: string;
  department_ref?:Department;
  org_level?:OrganizationLevel;
  job_description?:string;
  packages?:PackageName[];
  get_sitecpnpackageitems?:LocalCpnPackageItem[];
  get_company_position_hierarchy?:CompanyPositionHierarchy;
  skills?:Skill[];
  direct_hire?:boolean;
  area_ref?:Area

  //dummy
  errorResponse?:any;
}

@Injectable()
export class CompanyPositionNameService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "CompanyPositionName";
    this.serviceURL = "hrpositions/companypositionnames/";
  }

  getCompanyPositionNames(query: any) {
    return this.getList(query);
  }

  getCompanyPositionName(id: number) {
    return this.getItem(id);
  }

  updateCompanyPositionName(cpn: CompanyPositionName) {
    return this.saveItem(cpn);
  }

  saveCompanyPositionName(cpn:CompanyPositionName){
    return this.saveItem(cpn);
  }

  deleteCompanyPositionName(cpn:CompanyPositionName){
    return this.deleteItem(cpn);
  }

  restoreCompanyPositionName(cpn:CompanyPositionName){
    return this.restoreItem(cpn);
  }
}



