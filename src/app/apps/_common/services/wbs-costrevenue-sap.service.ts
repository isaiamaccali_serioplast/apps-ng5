import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";

declare let sap: any;

@Injectable()
export class WbsCostrevenueSapService extends GenericSAPService {
  serviceName_SAP = "ZPROJECT_MANAGEMENT_SRV";
  entitySetName_SAP = "WBSCostRevenueSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  getCostRevenues(query){
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.ProjectDefinition != null) {
          let filter = new sap.ui.model.Filter({
            path: 'ProjectDefinition',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['ProjectDefinition']
          });
          filters.push(filter);
        }
        if (query.WBS != null) {
          let filter = new sap.ui.model.Filter({
            path: 'WBS',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query['WBS']
          });
          filters.push(filter);
        }
        params['filters'] = filters;
      }

      this.getList(params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}



