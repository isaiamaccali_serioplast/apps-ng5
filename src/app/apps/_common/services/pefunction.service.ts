import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class PEFunction extends GenericObj {

}

@Injectable()
export class PEFunctionService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "PEFunction";
    this.serviceURL = "hrprofiles/pefunction/";
  }

  getPEFunctions(query:any){
    return this.getList(query);
  }

}



