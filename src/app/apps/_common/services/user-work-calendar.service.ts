import {GenericObj, GenericService} from "./_generic.service";
import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {ClockProfile} from "./clock-profile.service";
import {cloneObject} from "../../../shared/utils";
import {HRUser} from "./hr-users.service";

export class UserWorkCalendar extends GenericObj{
  user?:HRUser;
  workday?:boolean;
  clock_profile?:ClockProfile;
  date?:string;

  toSave:boolean = false;
}

@Injectable()
export class UserWorkCalendarService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "UserWorkCalendar";
    this.serviceURL = "salaries/user-work-calendar/";
  }

  getUserWorkCalendars(query: any) {
    return this.getList(query);
  }

  saveUserWorkCalendar(i:UserWorkCalendar){
    return this.saveItem(i);
  }

  clearItem(i:UserWorkCalendar){
    let i_:UserWorkCalendar = cloneObject(i);
    delete i_.toSave;
    return i;
  }
}


