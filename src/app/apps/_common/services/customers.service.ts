import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Organization} from "./organization.service";

export class Customer {
  pk:number;
  organization_ref:Organization;
  type:string;
  sap_code:string;
  sap_description:string;
  is_active:boolean;
  __str__:string;
}

@Injectable()
export class CustomerService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Customers";
    this.serviceURL = "organizations/customers/";
  }

  getCustomers(query: any) {
    return this.getList(query);
  }

  getCustomer(id: number) {
    return this.getItem(id);
  }

  saveCustomer(customer: Customer){
    return this.saveItem(customer);
  }
}
