import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Frequency} from "./frequency.service";
import {Material} from "./materials.service";
import {ControlPlanMeasure} from "./control-plan-measures.service";
import {TypeLink} from "./type-link.service";
import {Site} from "./sites.service";
import {cloneObject} from "../../../shared/utils";

export class ControlPlan extends GenericObj{
  description?:string;
  frequency?:Frequency;
  site?:Site;
  measures?: ControlPlanMeasure[];
  type?:TypeLink;
  time_from_last?:number;
  controlplanmaterial_set?:ControlPlanMaterial[]
}

export class ControlPlanMaterial extends GenericObj{
  material?:Material;
  control_plan?:ControlPlan;

  disabled:boolean;
}

@Injectable()
export class ControlPlansService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "ControlPlan";
    this.serviceURL = "qc/control-plans/";
  }

  getControlPlans(query: any) {
    return this.getList(query);
  }

  saveControlPlan(i:ControlPlan){
    return this.saveItem(i,['controlplanmaterial_set']);
  }

  deleteControlPlan(i:ControlPlan){
    return this.deleteItem(i);
  }

  restoreControlPlan(i:ControlPlan){
    return this.restoreItem(i);
  }

  getControlPlan(id: number) {
    return this.getItem(id);
  }

  clearItem(i:ControlPlan){
    let i_:ControlPlan = cloneObject(i);
    i_.controlplanmaterial_set = i_.controlplanmaterial_set.map((rc)=>{
      delete rc.disabled;
      return GenericService.processItemForSave(rc)
    });
    return i_;
  }

}
