import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SPFile} from "./file.service";
import {SerionetGenericObject} from "./serionet-generic-objects.service";
import {FileFieldAutoSave} from "./file-field.service";

export class InputFilter {
  name: string;
  label: string;
  type: string;
  value?: any;
  value_field_display?:string;
  is_disabled: boolean;
  serviceURL?: string; // for select
  query?: any; // for select
  results?: any[]; // for select
  required:boolean;
  visible:boolean;
  value1?:string;
  label1?:string;
  value2?:string;
  label2?:string;
}

export class Dataset {
  name: string;
  serviceURL?: string;
  fields?: string[];
  filters?: InputFilter[];
  results?: any[];
  notes?: string;
  count?: number;
  file_url?:string;
}

export class ReportConfig {
  name: string;
  params?:InputFilter[];
  filters?:InputFilter[];
}

export class Report {
  pk: number;
  __str__: string;
  is_active?: boolean;
  title?: string;
  python_func?: string;
  description?: FileFieldAutoSave;
  tags?: SerionetGenericObject[];

  datasets?: Dataset[];
}


@Injectable()
export class ReportService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Report";
  }


  getReports(query?: any) {
    this.serviceURL = "reports/reports/";
    return this.getList(query,null,150000);
  }

  getDynamicList(serviceURL: string, query?: any, custom_format?: string) {
    this.serviceURL = serviceURL;
    if (custom_format) {
      return this.getList(query, custom_format, 150000);
    }
    return this.getList(query);
  }
}

