import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {WorkToolType} from "./worktooltype.service";

export class UserWorktool extends GenericObj{
  content_type?:number;
  object_id?:number;
  type?:WorkToolType
}

@Injectable()
export class UserWorktoolService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "User Worktool";
    this.serviceURL = "packages/user-worktool/";
  }

  getUserWorktools(query: any) {
    return this.getList(query);
  }

  saveUserWorktool(i:UserWorktool){
    return this.saveItem(i);
  }

  deleteUserWorktool(i:UserWorktool){
    return this.deleteItem(i);
  }

  restoreUserWorktool(i:UserWorktool){
    return this.restoreItem(i);
  }

  getUserWorktool(id: number) {
    return this.getItem(id);
  }
}


