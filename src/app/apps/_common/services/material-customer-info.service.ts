import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {Material} from "./materials.service";
import {LegalEntity} from "./legal-entity.service";
import {Customer} from "./customers.service";

export class MaterialCustomerInfo {
  pk: number;
  __str__: string;
  material?:Material;
  customer?: Customer;
  legal_entity:LegalEntity;
  code?: string;
  description?: string;
  is_active?:boolean;
  notes:string;
}

export class MaterialCustomerInfo_SAP {
  Organization: string;
  Material: string;
  Customer: string;
  CustomerMatDescript: string;
  CustomerMaterial: string;

  getId(){
    return "Organization='"+this.Organization+"',Material='"+this.Material+"',Customer='"+this.Customer+"'";
  }
}

@Injectable()
export class MaterialCustomerInfoService extends GenericServiceWithSAP {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService, protected sapService:SAPService) {
    super(serionetService,loggingService,sapService);
    this.init();
  }

  init() {
    this.entityName = "Material Customer Info";
    this.serviceURL = "materials/material-purchase-info-record/";
    this.entitySetName_SAP = "CustMatSet";
    this.serviceName_SAP = "ZSDPRICELIST_SRV";
  }

  getMaterialCustomerInfos(query: any) {
    return this.getList(query);
  }

  getMaterialCustomerInfo(id:number) {
    return this.getItem(id);
  }

  saveMaterialCustomerInfo(mci:MaterialCustomerInfo) {
    return this.saveItem(mci);
  }

  deleteMaterialCustomerInfo(mci:MaterialCustomerInfo) {
    return this.deleteItem(mci);
  }

  getSAPObject(mci: MaterialCustomerInfo){
    let mci_sap = new MaterialCustomerInfo_SAP();
    mci_sap.Material = mci.material.code;
    mci_sap.Customer = mci.customer.sap_code;
    mci_sap.Organization = mci.legal_entity.code;
    mci_sap.CustomerMaterial = mci.code;
    mci_sap.CustomerMatDescript = mci.description;
    return mci_sap;
  }
}
