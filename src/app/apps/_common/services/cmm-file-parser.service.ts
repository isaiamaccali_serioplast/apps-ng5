import {Injectable} from "@angular/core";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";
import {GenericService} from "./_generic.service";

export class CMMParserOutput {
  row:string[];
  col:string[];
  data:number[][];
}

export class CMMParserOutputMatch {
  samples: CMMParserOutputMatchSample[];
  properties: CMMParserOutputMatchProperty[];
}

export class CMMParserOutputMatchSample {
  sampleIndex:number;
  sampleName:string;
  index:number;
}

export class CMMParserOutputMatchProperty {
  propertyPK:number;
  propName:string;
  index:number;
}


@Injectable()
export class CMMFileParserService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "File Parse";
    this.serviceURL = "qc/cq-file-parser/";
  }

  parseFile(input: HTMLInputElement, parserPK:number) {
    return Observable.create((observer) => {
      let form = document.createElement("form");
      let clonedInput = input['nativeElement'].cloneNode();
      clonedInput.setAttribute("name", "file");
      form.appendChild(clonedInput);

      let ptTypeInput = document.createElement("input");
      ptTypeInput.setAttribute("name", "parsing_type");
      ptTypeInput.value = parserPK+"";
      form.appendChild(ptTypeInput);

      // let urlParams = new URLSearchParams();
      //
      // let options = {
      //   search: urlParams
      // };

      const formData = new FormData(form);

      let opts = new SerionetRequestOptions();
      opts.options = {};

      this.serionetService.doPostFile(this.serviceURL, formData, opts).subscribe((data) => {
        observer.next(data);
        observer.complete();
      },(response) => {
        this.loggingService.error("Error in posting " + this.entityName + ", status: " + response.status);
        observer.next(response);
        observer.complete();
      });
    });
  }

}



