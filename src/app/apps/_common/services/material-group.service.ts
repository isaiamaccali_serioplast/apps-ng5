import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class MaterialGroup extends GenericObj{
  label?:string;
  sap_code?:string;
}

@Injectable()
export class MaterialGroupsService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Material Groups";
    this.serviceURL = "materials/material-groups/";
  }

  getMaterialGroups(query:any){
    return this.getList(query);
  }
}



