import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {DayWeekProfile} from "./day-week-profile-service";
import {HRUser} from "./hr-users.service";

export class DayWeekUserMilestone extends GenericObj{
  user:HRUser;
  day_week_profile:DayWeekProfile;
  start_time:string;
}

@Injectable()
export class DayWeekUserMilestoneService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "DayWeekUserMilestone";
    this.serviceURL = "salaries/day-user-milestone/";
  }

  getDayWeekUserMilestones(query: any) {
    return this.getList(query);
  }

  saveDayWeekUserMilestone(i:DayWeekUserMilestone){
    return this.saveItem(i);
  }

  deleteDayWeekUserMilestone(i:DayWeekUserMilestone){
    return this.deleteItem(i);
  }

  restoreDayWeekUserMilestone(i:DayWeekUserMilestone){
    return this.restoreItem(i);
  }

  getDayWeekUserMilestone(id: number) {
    return this.getItem(id);
  }
}


