import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericSortableObj, GenericSortableService} from "./_generic-sortable.service";

export class PropertyAttribute extends GenericSortableObj {
  label?: string;
}

@Injectable()
export class PropertyAttributeService extends GenericSortableService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Property Attribute";
    this.serviceURL = "qc/propertyattribute/";
  }

  saveAttribute(a:PropertyAttribute){
    return this.saveItem(a);
  }

  getAttributes(query?:any){
    return this.getList(query);
  }

  deleteAttribute(a:PropertyAttribute){
    return this.deleteItem(a);
  }

  restoreAttribute(a:PropertyAttribute){
    return this.restoreItem(a);
  }
}



