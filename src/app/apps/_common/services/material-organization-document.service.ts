import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Organization} from "./organization.service";
import {SPFile} from "./file.service";
import {Material} from "./materials.service";
import {GenericObj, GenericService} from "./_generic.service";

export class MaterialOrganizationDocument extends GenericObj {
  organization_ref?:Organization;
  file?:SPFile;
  material?:Material;
  doc_id?:string;
  version?:string;
  type:MaterialOrganizationDocumentType;
  notes?:string;

  documents?:MaterialOrganizationDocument[];
  is_enabled_for_material:boolean;
}

export class MaterialOrganizationDocumentType extends GenericObj{
  fieldname:string;
  label:string;
}

@Injectable()
export class MaterialOrganizationDocumentService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Material Organization Documents";
    this.serviceURL = "materials/material-organization-docs/";
  }

  getMaterialOrganizationDocuments(query: any) {
    return this.getList(query);
  }

  getMaterialOrganizationDocument(id:number) {
    return this.getItem(id);
  }

  saveMaterialOrganizationDocument(mod:MaterialOrganizationDocument) {
    return this.saveItem(mod);
  }

  deleteMaterialOrganizationDocument(mod:MaterialOrganizationDocument) {
    return this.deleteItem(mod);
  }
}

@Injectable()
export class MaterialOrganizationDocumentTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Material Organization Document Types";
    this.serviceURL = "materials/material-organization-docs-types/";
  }

  getMaterialOrganizationDocumentTypes(query: any) {
    return this.getList(query);
  }
}
