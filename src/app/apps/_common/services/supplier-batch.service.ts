import {Injectable} from "@angular/core";
import {GenericSAPService} from "./_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {cloneObject} from "../../../shared/utils";

declare let sap: any;

export class SupplierBatch {
  Batch:string;
  Item:string;
  SuppBatch:string;
  Order:string;
  Quantity:any;
  Plant:string;
  Material:string;
  Supplier:string;
  Printer:string;
  Copies:number;

  getId(){
    return "Batch='"+this.Batch+"',Item='"+this.Item+"',SuppBatch='"+this.SuppBatch+"',Order='"+this.Order+"'";
  }

  has_printed?:boolean;
}

@Injectable()
export class SupplierBatchService extends GenericSAPService {

  constructor(protected loggingService: LoggingService, protected sapService: SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "zdeck_srv";
    this.entitySetName_SAP = "SupplierBatchSet";
  }

  saveSupplierBatch(sb: SupplierBatch) {
    let sb_ = cloneObject(sb);
    delete sb_.has_printed;
    return this.saveItem(sb_, true);
  }

  printSupplierBatch(sb:SupplierBatch){
    let sb_ = cloneObject(sb);
    delete sb_.has_printed;
    return this.saveItem(sb_);
  }

  deleteSupplierBatch(sb: SupplierBatch) {
    return this.deleteItem(sb);
  }
}
