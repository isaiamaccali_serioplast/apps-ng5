import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class Country {
  value:string;
  label:string;
}

@Injectable()
export class CountriesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Countries";
    this.serviceURL = "organizations/countries/";
  }

  getCountries(query: any) {
    return this.getList(query);
  }
}
