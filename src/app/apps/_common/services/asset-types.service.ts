import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Department} from "./department.service";
import {ContentType} from "./content-type.service";
import {Asset} from "./assets.service";
import {HRProfile} from "./hr-profiles.service";

export class AssetType {
  pk:number;
  __str__:string;
  department?: Department;
  name?: string;
  referent?: HRProfile;
  is_active?: boolean;
  maintainable?: boolean;
  model_reference?: ContentType;
  form_name?: string;
}

export const ASSET_TYPES = {
  ASSEMBLYING_MACHINE:'ASSEMBLYING_MACHINE',
  BUILDING:'BUILDING',
  CIM_MACHINE:'CIM_MACHINE',
  DECORATION_MACHINE:'DECORATION_MACHINE',
  EBM_MACHINE:'EBM_MACHINE',
  HANDLING_MACHINE:'HANDLING_MACHINE',
  HARDWARE:'HARDWARE',
  HOT_RUNNER:'HOT_RUNNER',
  ISBM_MACHINE:'ISBM_MACHINE',
  LABORATORY_EQUIPMENT:'LABORATORY_EQUIPMENT',
  PALLETIZATION_MACHINE:'PALLETIZATION_MACHINE',
  PIM_MACHINE:'PIM_MACHINE',
  PRODUCTION_ANCILLARIES:'PRODUCTION_ANCILLARIES',
  SBM_MACHINE:'SBM_MACHINE',
  SOFTWARE:'SOFTWARE',
  TESTING_MACHINE:'TESTING_MACHINE',
  TOOLING_MACHINE:'TOOLING_MACHINE',
  TRIMMING_MACHINE:'TRIMMING_MACHINE',
  UTILITIES:'UTILITIES',
  VEHICLES:'VEHICLES'
};

export const _getAssetType = (a:Asset) => {
  let contentTypeDesc = a.type.model_reference.__str__;
  switch(contentTypeDesc) {
    case 'assemblying machine':
      return ASSET_TYPES.ASSEMBLYING_MACHINE;
    case 'building':
      return ASSET_TYPES.BUILDING;
    case 'cim machine':
      return ASSET_TYPES.CIM_MACHINE;
    case 'decoration machine':
      return ASSET_TYPES.DECORATION_MACHINE;
    case 'ebm machine':
      return ASSET_TYPES.EBM_MACHINE;
    case 'handling machine':
      return ASSET_TYPES.HANDLING_MACHINE;
    case 'hardware':
      return ASSET_TYPES.HARDWARE;
    case 'hot runner':
      return ASSET_TYPES.HOT_RUNNER;
    case 'isbm machine':
      return ASSET_TYPES.ISBM_MACHINE;
    case 'laboratory equipment':
      return ASSET_TYPES.LABORATORY_EQUIPMENT;
    case 'palletization machine':
      return ASSET_TYPES.PALLETIZATION_MACHINE;
    case 'pim machine':
      return ASSET_TYPES.PIM_MACHINE;
    case 'production ancillaries':
      return ASSET_TYPES.PRODUCTION_ANCILLARIES;
    case 'sbm machine':
      return ASSET_TYPES.SBM_MACHINE;
    case 'software':
      return ASSET_TYPES.SOFTWARE;
    case 'testing machine':
      return ASSET_TYPES.TESTING_MACHINE;
    case 'tooling machine':
      return ASSET_TYPES.TOOLING_MACHINE;
    case 'trimming machine':
      return ASSET_TYPES.TRIMMING_MACHINE;
    case 'utilities':
      return ASSET_TYPES.UTILITIES;
    case 'vehicles':
      return ASSET_TYPES.VEHICLES;
    default:
      return null;
  }
}

@Injectable()
export class AssetTypesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Asset typess";
    this.serviceURL = "operations/asset-types/";
  }

  getAssetTypes(query?: any) {
    return this.getList(query);
  }
}


