import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class MaterialInfo {
  CCSTPrice: any;
  Currency: string;
  GeneralStatus: string;
  GeneralStatusDescription: string;
  Material: string;
  Plant: string;
  PlantStatus: string;
  PlantStatusDescription: string;
}


@Injectable()
export class MaterialInfoService {

  serviceNameSAP = "ZUTILITIES_srv";
  entityNameSAP = "MaterialInfoSet";

  constructor(private sapService: SAPService) {
  }

  getMaterialInfos(query) {
    return Observable.create((observer) => {

      let params = {};
      if (query) {
        let filters = [];
        if (query.Material != null) {
          let materialFilter = new sap.ui.model.Filter({
            path: 'Material',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Material
          });
          filters.push(materialFilter);
        }
        if (query.Plant != null) {
          let siteFilter = new sap.ui.model.Filter({
            path: 'Plant',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Plant
          });
          filters.push(siteFilter);
        }
        params['filters'] = filters;
      }

      this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
        observer.next(response);
        observer.complete();
      })
    });
  }
}



