import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericSortableObj, GenericSortableService} from "./_generic-sortable.service";
import {cloneObject} from "../../../shared/utils";
import {GenericService} from "./_generic.service";
import {ControlPlan} from "./control-plans.service";
import {ReleaseCriteria} from "./release-criteria.service";
import {Property} from "./properties.service";

export class ControlPlanMeasure extends GenericSortableObj{
  property?:Property;
  repeats?:number;
  control_plan?:ControlPlan;
  release_criteria_list?:ReleaseCriteria[];
  or_operator?:boolean = false;

  progress:string;
  overflow:string;
  is_complete:boolean;
  is_overflow:boolean;
}

@Injectable()
export class ControlPlanMeasuresService extends GenericSortableService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "ControlPlanMeasure";
    this.serviceURL = "qc/control-plan-measure/";
  }

  getControlPlanMeasures(query: any) {
    return this.getList(query);
  }

  saveControlPlanMeasure(i:ControlPlanMeasure){
    return this.saveItem(i,['release_criteria_list']);
  }

  deleteControlPlanMeasure(i:ControlPlanMeasure){
    return this.deleteItem(i);
  }

  restoreControlPlanMeasure(i:ControlPlanMeasure){
    return this.restoreItem(i);
  }

  getControlPlanMeasure(id: number) {
    return this.getItem(id);
  }

  clearItem(i:ControlPlanMeasure){
    let i_:ControlPlanMeasure = cloneObject(i);
    delete i_.progress;
    delete i_.overflow;
    delete i_.is_complete;
    delete i_.is_overflow;
    i_.release_criteria_list = i_.release_criteria_list.map((rc)=>{
      rc.parameters = rc.parameters_list.map((v)=>v.value).join(",");
      delete rc.parameters_list;
      delete rc.criteria_full;
      return GenericService.processItemForSave(rc);
    });
    return i_;
  }
}





