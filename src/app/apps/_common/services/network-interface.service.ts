import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";
import {AssetSubTypeLinkService} from "./asset-subtype-link.service";
import {ContentTypeService} from "./content-type.service";
import {Asset} from "./assets.service";

export class NetworkInterface {
  pk: number;
  __str__: string;
  type?: NetworkInterfaceType;
  ip_address?: string;
  mac_address?: string;
  asset_id?:Asset;
  socketservice_set?:SocketService[];

  status?:string;
  newly_created?:boolean;
}

export class NetworkInterfaceType {
  pk: number;
  __str__: string;
}

export class SocketService {
  pk: number;
  __str__: string;
  service_name?: string;
  port?: number;
}


@Injectable()
export class NetworkInterfacesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Network Interfaces";
    this.serviceURL = "operations/networkinterface/";
  }

  getNetworkInterfaces(query?: any) {
    return this.getList(query);
  }

  deleteNetworkInterface(ni:NetworkInterface){
    return this.deleteItem(ni);
  }

  saveNetworkInterface(ni:NetworkInterface){
    return this.saveItem(ni,["socketservice_set"]);
  }
}

@Injectable()
export class NetworkInterfaceTypeService {

  constructor(private assetSubTypeLinkService:AssetSubTypeLinkService, private contentTypeService:ContentTypeService) {}

  getNetworkInterfaceType(query?: any) {
    return Observable.create((observer) => {
      this.contentTypeService.getContentTypes({model: "networkinterface", is_active:true}).subscribe((data)=>{
        if(data && data['count'] && data['count']>0){
          let app = data['results'][0]['pk'];
          query = query || {};
          query.app = app;
          this.assetSubTypeLinkService.getAssetSubtypeLinks(query).subscribe((data)=>{
            observer.next(data);
            observer.complete();
          });
        }
      })
    });
  }
}


