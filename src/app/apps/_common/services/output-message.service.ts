import {Injectable} from "@angular/core";
import {GenericSAPService} from "./_generic_sap_service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class OutputMessage {
  Printer: string;
  Description: string;
  Medium: string;
  MessageCode: string;
  ObjectType: string;
  Object: string;

  getId(){
    return "Medium='"+this.Medium+"',MessageCode='"+this.MessageCode+"',ObjectType='"+this.ObjectType+"',Object='"+this.Object+"'";
  }
}

@Injectable()
export class OutputMessageService extends GenericSAPService{

  constructor(protected loggingService: LoggingService, protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "ZOUTPUT_SRV";
    this.entitySetName_SAP = "MessageSet";
  }

  getOutputMessages(query){
    let params = {};
    if (query) {
      let filters = [];
      if (query.ObjectType != null) {
        let f = new sap.ui.model.Filter({
          path: 'ObjectType',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.ObjectType
        });
        filters.push(f);
      }
      if (query.Object != null) {
        let f = new sap.ui.model.Filter({
          path: 'Object',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Object
        });
        filters.push(f);
      }
      if (query.Medium != null) {
        let f = new sap.ui.model.Filter({
          path: 'Medium',
          operator: sap.ui.model.FilterOperator.EQ,
          value1: query.Medium
        });
        filters.push(f);
      }
      params['filters'] = filters;
    }
    return this.getList(params);
  }

  saveOutputMessage(o:OutputMessage){
    return this.saveItem(o);
  }

}
