import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";

export class DjangoLanguage {
  value:string;
  label:string;
}


@Injectable()
export class DjangoLanguagesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Django Language";
    this.serviceURL = "hrprofiles/django-settings-languages/";
  }

  getDjangoLanguages(query: any) {
    return this.getList(query);
  }
}
