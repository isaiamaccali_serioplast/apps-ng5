import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class Sex extends GenericObj{
  label: string;
}

@Injectable()
export class SexService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Sex";
    this.serviceURL = "statuses/sex/";
  }

  getSex(query:any){
    return this.getList(query);
  }

}



