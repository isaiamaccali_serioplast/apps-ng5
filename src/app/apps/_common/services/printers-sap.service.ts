import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";

declare let sap: any;

export class PrinterSAP {
  Plant?:string;
  Name:string;
  Thermal:boolean;
}

@Injectable()
export class PrinterSapService {

  entityName = "Printer";

  serviceNameSAP = "ZOUTPUT_SRV";
  entityNameSAP = "PrinterSet";

  constructor(private sapService: SAPService) {
  }

  getPrinters(query) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {
        if (query) {
          let filters = [];
          if (query.Plant != null) {
            let plantFilter = new sap.ui.model.Filter({
              path: 'Plant',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Plant
            });
            filters.push(plantFilter);
          }
          if (query.Thermal != null) {
            let plantFilter = new sap.ui.model.Filter({
              path: 'Thermal',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Thermal
            });
            filters.push(plantFilter);
          }

          if (query.RFID != null) {
            let plantFilter = new sap.ui.model.Filter({
              path: 'RFID',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.RFID
            });
            filters.push(plantFilter);
          }

          params['filters'] = filters;
        }

        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }
}
