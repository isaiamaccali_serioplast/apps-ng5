import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";
import {Country} from "./countries.service";
import {PEFunction} from "./pefunction.service";
import {PEIndustry} from "./peindustry.service";
import {ContractType} from "./contract-type.service";

export class ProfessionalExperience {
  pk:number;
  user_id: number;
  company_name: string;
  date_from: string;
  date_to:string;
  job_title: string;
  country: Country;
  city: string;
  function: PEFunction;
  industry: PEIndustry;
  contract_type: ContractType;
  __str__: string;
  is_active:boolean;
}

@Injectable()
export class ProfessionalExperiencesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "ProfessionalExperiences";
    this.serviceURL = "hrprofiles/professionalexperiences/";
  }

  getProfessionalExperiences(query: any) {
    return this.getList(query);
  }

  deleteProfessionalExperience(p: ProfessionalExperience) {
    return this.deleteItem(p);
  }

  saveProfessionalExperience(p: ProfessionalExperience) {
    return this.saveItem(p);
  }


}

