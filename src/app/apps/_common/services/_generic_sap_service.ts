import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {Observable} from "rxjs";
import {cloneObject} from "../../../shared/utils";

declare let sap: any;

export abstract class SAPObject {

  public abstract getId();
}


@Injectable()
export abstract class GenericSAPService{

  protected serviceName_SAP = "";
  protected entitySetName_SAP = "";

  protected constructor(protected sapService:SAPService) {}

  protected saveItem(_item: any, forceCreate?: boolean) {
    return Observable.create((observer) => {
      let item = this.clearItem(_item);
      if (forceCreate) {
        this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item).subscribe((response_) => {
          observer.next(response_);
          observer.complete();
        });
      }
      else {
        this.getItem(item.getId()).subscribe((response) => {
          if (response.status == 200) {
            this.sapService.updateItem(this.serviceName_SAP, this.entitySetName_SAP, null, item.getId(), item).subscribe((response_) => {
              observer.next(response_);
              observer.complete();
            })
          }
          else {
            this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item).subscribe((response_) => {
              observer.next(response_);
              observer.complete();
            })
          }
        });
      }
    });
  }

  protected deleteItem(item:any){
    return Observable.create((observer) => {
      this.getItem(item.getId()).subscribe((response)=>{
        if(response.status==200){
          this.sapService.deleteItem(this.serviceName_SAP, this.entitySetName_SAP, null,item.getId()).subscribe((response_)=>{
            observer.next(response_);
            observer.complete();
          })
        }
        else {
          observer.next(response);
          observer.complete();
        }
      })
    });
  }

  protected getItem(id: string, params?:any) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null, id+"", params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        })
      });
    });
  }

  protected getList(params: any) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        this.sapService.getItems(this.serviceName_SAP, this.entitySetName_SAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  protected restoreItem(item:any){
    return Observable.create((observer) => {
      //TODO
    });
  }

  getFilterByLength(key:string,value:string,maxLength:number){
    let filter;
    if(value.length==maxLength || value.length>maxLength){
      filter = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.EQ,
        value1: value
      });
    }
    else {
      filter = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.Contains,
        value1: value
      });
    }
    return filter;
  }

  checkPagination(params, query){
    if(query['page']&& query['results']){
      if(!params['urlParameters'])
        params['urlParameters'] = {};
      params['urlParameters']['$top']=query['results'];
      params['urlParameters']['$skip']=(query['page']-1)*query['results'];
      params['urlParameters']['$inlinecount'] = 'allpages';
    }
    return params;
  }

  protected clearItem(item:any){
    return cloneObject(item);
  }
}





