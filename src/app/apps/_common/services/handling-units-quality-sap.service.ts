import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";
import {cloneObject, formatNumber} from "../../../shared/utils";
import {GenericSAPService} from "./_generic_sap_service";
import {HULabel} from "./label-hu-quality-sap.service";

declare let sap: any;

export const _HUQualityStatus: any = {
  FREE: 'FREE_STOCK',
  RETURN_STOCK: 'RETURN_STOCK',
  LOCKED: 'LOCKED_STOCK',
  QUALITY: 'QUALITY_STOCK'
};

export class HUQuality {
  Method?:string;
  Warehouse?:string;
  StockType?:string;
  Uom?:string;
  Quantity?:number;
  Batch?:string;
  InventoryMonth?:string;
  Group4?:string;
  CIDGoodIssue?:string;
  CIDProduction?:string;
  PalletNr?:string;
  HUDescription?:string;
  CreationTime?:string;
  CreationDate?:string;
  Plant?:string;
  MaterialDescription?:string;
  Material?:string;
  WorkOrder?:string;
  SSCC?:string;
  Printer?:string;
  Palletization?:string;
  Delivery?:string;
  WarehouseDescription?:string;

  //dummy
  selected?:boolean;
  message?:string;
  messageClass?:string;

  HULabelLink?:HULabel[];

  public getStatus(){
    switch(this.StockType){
      case "":
        return _HUQualityStatus.FREE;
      case null:
        return _HUQualityStatus.FREE;
      case "R":
        return _HUQualityStatus.RETURN_STOCK;
      case "X":
        return _HUQualityStatus.LOCKED;
      case "S":
        return _HUQualityStatus.LOCKED;
      case "Q":
        return _HUQualityStatus.QUALITY;
      default:
        return _HUQualityStatus.FREE;
    }
  }

  getId(){
    return "'"+this.SSCC+"'";
  }
}

@Injectable()
export class HandlingUnitsQualitySapService extends GenericSAPService{
  entityName = "HandlingUnit";
  serviceNameSAP = "ZHUQUALITY_SRV";
  entityNameSAP = "QualityHUSet";

  constructor(protected sapService: SAPService) {
    super(sapService);
  }

  getHandlingUnitsQuality(query?: any) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {
        if (query) {
          let filters = [];
          if (query['from'] && query ['to']) {
            let dateFilter = new sap.ui.model.Filter({
              path: "CreationDate",
              operator: sap.ui.model.FilterOperator.BT,
              value1: query['from'],
              value2: query ['to']
            });
            filters.push(dateFilter);
          }
          else {
            if (query['from']){
              let fromDateFilter = new sap.ui.model.Filter({
                path: "CreationDate",
                operator: sap.ui.model.FilterOperator.GE,
                value1: query['from']
              });
              filters.push(fromDateFilter);
            }
            if (query['to']){
              let toDateFilter = new sap.ui.model.Filter({
                path: "CreationDate",
                operator: sap.ui.model.FilterOperator.LE,
                value1: query['to']
              });
              filters.push(toDateFilter);
            }
          }
          if (query['fromTime'] && query ['toTime']) {
            let timeFilter = new sap.ui.model.Filter({
              path: "CreationTime",
              operator: sap.ui.model.FilterOperator.BT,
              value1: query['fromTime'],
              value2: query ['toTime']
            });
            filters.push(timeFilter);
          }
          else {
            if (query['fromTime']){
              let fromTimeFilter = new sap.ui.model.Filter({
                path: "CreationTime",
                operator: sap.ui.model.FilterOperator.BT,
                value1: query['fromTime'],
                value2: '235959'
              });
              filters.push(fromTimeFilter);
            }
            if (query['toTime']){
              let toTimeFilter = new sap.ui.model.Filter({
                path: "CreationTime",
                operator: sap.ui.model.FilterOperator.BT,
                value1: '000000',
                value2: query['toTime']
              });
              filters.push(toTimeFilter);
            }
          }
          if (query['plant'] || query['Plant']) {
            let plantFilter = new sap.ui.model.Filter({
              path: "Plant",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['plant'] || query['Plant']
            });
            filters.push(plantFilter);
          }
          if (query['material'] || query['Material']) {
            let materialFilter = new sap.ui.model.Filter({
              path: "Material",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['material'] || query['Material']
            });
            filters.push(materialFilter);
          }
          if (query['batch'] || query['Batch']) {
            let batchFilter = this.getFilterByLength("Batch", query['batch'] || query['Batch'], 10);
            filters.push(batchFilter);
          }
          if (query['hu'] || query['SSCC']) {
            let huFilter = this.getFilterByLength("SSCC", query['hu'] || query['SSCC'], 20);
            filters.push(huFilter);
          }
          if (query['Warehouse']) {
            let warehouseFilter = new sap.ui.model.Filter({
              path: "Warehouse",
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Warehouse']
            });
            filters.push(warehouseFilter);
          }
          if (query['Status']) {
            let statusFilter;
            switch (query['Status']) {
              case _HUQualityStatus.FREE:
                statusFilter = new sap.ui.model.Filter({
                  path: "StockType",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: " "
                });
                break;
              case _HUQualityStatus.QUALITY:
                statusFilter = new sap.ui.model.Filter({
                  path: "StockType",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: "Q"
                });
                break;
              case _HUQualityStatus.RETURN_STOCK:
                statusFilter = new sap.ui.model.Filter({
                  path: "StockType",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: "R"
                });
                break;
              case _HUQualityStatus.LOCKED:
                let filter1 = new sap.ui.model.Filter({
                  path: "StockType",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: "X"
                });
                let filter2 = new sap.ui.model.Filter({
                  path: "StockType",
                  operator: sap.ui.model.FilterOperator.EQ,
                  value1: "S"
                });
                statusFilter = new sap.ui.model.Filter({
                  filters: [filter1, filter2],
                  and: false
                });
                break;
              default:
                break;
            }

            if (statusFilter)
              filters.push(statusFilter);
          }

          if (query['fromPallet'] && query['toPallet']) {
            let palletFilter = new sap.ui.model.Filter({
              path: "PalletNr",
              operator: sap.ui.model.FilterOperator.BT,
              value1: formatNumber(4, query['fromPallet']) + " ",
              value2: formatNumber(4, query['toPallet']) + " "
            });

            filters.push(palletFilter);
          }
          else {
            if (query['fromPallet']) {
              let fromPalletFilter = new sap.ui.model.Filter({
                path: "PalletNr",
                operator: sap.ui.model.FilterOperator.GE,
                value1: formatNumber(4, query['fromPallet']) + " "
              });
              filters.push(fromPalletFilter);
            }
            if (query['toPallet']) {
              let toPalletFilter = new sap.ui.model.Filter({
                path: "PalletNr",
                operator: sap.ui.model.FilterOperator.LE,
                value1: formatNumber(4, query['toPallet']) + " "
              });
              filters.push(toPalletFilter);
            }
          }



          params['filters'] = filters;

          params = this.checkPagination(params, query);
        }

        params['sorters'] = [
          new sap.ui.model.Sorter("CreationDate",false),
          new sap.ui.model.Sorter("CreationTime",false),
          new sap.ui.model.Sorter("PalletNr",false)
        ];



        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          response['response']['results'] = response['response']['results'].map((i) => {
            let huQ = new HUQuality();
            for (let key in i) {
              if (i.hasOwnProperty(key))
                huQ[key] = i[key];
            }
            return huQ;
          });
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  saveHandlingUnitsQuality(hu: HUQuality) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        let hu_ = cloneObject(hu);
        if (hu_['selected'] != null)
          delete hu_['selected'];
        if (hu_['message'] != null)
          delete hu_['message'];
        if (hu_['messageClass'] != null)
          delete hu_['messageClass'];
        this.sapService.updateItem(this.serviceNameSAP, this.entityNameSAP, null, hu.getId(), hu_).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  createHandlingUnitsQuality(hu: HUQuality) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        let hu_ = cloneObject(hu);
        if (hu_['selected'] != null)
          delete hu_['selected'];
        if (hu_['message'] != null)
          delete hu_['message'];
        if (hu_['messageClass'] != null)
          delete hu_['messageClass'];
        this.sapService.createItem(this.serviceNameSAP, this.entityNameSAP, null, hu_).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  deleteHandlingUnitsQuality(hu: HUQuality) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        this.sapService.deleteItem(this.serviceNameSAP, this.entityNameSAP, null, hu.getId()).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  cleanStartingZeroes(value: string) {
    let chars = value.split("");
    while (chars[0] == "0") {
      chars.splice(0, 1);
    }
    return chars.join("");
  }

  getHandlingUnitQuality(id) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        this.sapService.getItem(this.serviceNameSAP, this.entityNameSAP, null, id).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });
    });
  }

  getFilterByLength(key: string, value: string, maxLength: number) {
    let filter;
    if (value.length == maxLength || value.length > maxLength) {
      filter = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.EQ,
        value1: value
      });
    }
    else if (value.length == (maxLength - 1)) {
      let filter1 = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.StartsWith,
        value1: value
      });
      let filter2 = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.EndsWith,
        value1: "*" + value
      });
      filter = new sap.ui.model.Filter({
        filters: [filter1, filter2],
        and: false
      });
    }
    else if (value.length < (maxLength - 1)) {
      filter = new sap.ui.model.Filter({
        path: key,
        operator: sap.ui.model.FilterOperator.Contains,
        value1: value
      });
    }
    return filter;
  }
}



