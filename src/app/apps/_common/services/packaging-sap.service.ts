import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class PackagingSap {
  Palletization: string;
  Plant: string;
  Description: string;
  Material: string;
  DefaultQty: number;
}

@Injectable()
export class PackagingSapService {
  entityName = "Packaging";
  serviceNameSAP = "zworkorders_srv";
  entityNameSAP = "PackagingSet";

  constructor(private sapService: SAPService) {
  }

  getPackagings(query) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {

        if (query) {
          let filters = [];
          if (query.Material != null) {
            let MaterialFilter = new sap.ui.model.Filter({
              path: 'Material',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Material
            });
            filters.push(MaterialFilter);
          }

          if (query.Plant != null) {
            let PlantFilter = new sap.ui.model.Filter({
              path: 'Plant',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Plant
            });
            filters.push(PlantFilter);
          }

          if (query.Palletization != null) {
            let PalletizationFilter = new sap.ui.model.Filter({
              path: 'Palletization',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Palletization
            });
            filters.push(PalletizationFilter);
          }

          params['filters'] = filters;
        }

        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }

  getPackaging(id) {
    return this.sapService.getItem(this.serviceNameSAP, this.entityNameSAP, null, id);
  }

}
