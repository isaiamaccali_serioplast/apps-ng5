import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {LocalPackageItem} from "./localpackages.service";
import {CompanyPositionName} from "./company-position-name.service";

export class LocalCpnPackageItem extends GenericObj{
  site_package?: LocalPackageItem;
  company_position_name?: CompanyPositionName;
  mandatory?: boolean;
}

@Injectable()
export class LocalCpnPackageItemService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Site Cpn PackageName Item";
    this.serviceURL = "packages/sitecpnpackageitem/";
  }

  addLocalCpnPackageItems(localcpnpackage: LocalCpnPackageItem) {
    return this.saveItem(localcpnpackage);
  }

  removeCpnPackageItem(localcpnpackage: LocalCpnPackageItem) {
    return this.deleteItem(localcpnpackage);
  }
}



