import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {PackageItem} from "./packages-item.service";
import {GenericObj, GenericService} from "./_generic.service";

export class PackageName extends GenericObj{
  name: string;
  objects?:PackageItem[];

  //dummy fields
  selected:boolean = false;
}

@Injectable()
export class PackagesNameService extends GenericService{
  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
  }

  init() {
    this.entityName = "Packages Names";
    this.serviceURL = "packages/packagenames/";
  }

  getPackagesNames(query:any){
    return this.getList(query);
  }

  savePackageName(packageName: PackageName) {
    return this.saveItem(packageName);
  }

  removePackageName(packageName: PackageName) {
    return this.deleteItem(packageName);
  }

  restorePackageName(packageName: PackageName) {
    return this.saveItem(packageName);
  }

}



