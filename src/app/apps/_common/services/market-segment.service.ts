import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class MarketSegment extends GenericObj {

}

@Injectable()
export class MarketSegmentsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Market Segments";
    this.serviceURL = "materials/marketsegment/"
  }


  // getMarketSegments(query:any) {
  //   //TODO
  // }

  // getMarketSegment(id:number) {
  //   //TODO
  // }
}

@Pipe({
  name: 'market_segment'
})
export class MarketSegmentPipe implements PipeTransform{

  constructor(private marketSegmentsService:MarketSegmentsService){}

  transform(id:number) {
    return id; //TODO
  }
}



