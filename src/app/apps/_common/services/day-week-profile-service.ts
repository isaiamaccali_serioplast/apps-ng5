import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class DayWeekInterval extends GenericObj{
  interval:number;
  holiday:boolean;
}

export class DayWeekProfile extends GenericObj{
  name:string;
  intervals:DayWeekInterval[];
}

@Injectable()
export class DayWeekProfileService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "DayWeekProfile";
    this.serviceURL = "salaries/day-week-profile/";
  }

  getDayWeekProfiles(query: any) {
    return this.getList(query);
  }

  saveDayWeekProfile(i:DayWeekProfile){
    return this.saveItem(i,['intervals']);
  }

  deleteDayWeekProfile(i:DayWeekProfile){
    return this.deleteItem(i);
  }

  restoreDayWeekProfile(i:DayWeekProfile){
    return this.restoreItem(i,['intervals']);
  }

  getDayWeekProfile(id: number) {
    return this.getItem(id);
  }
}


