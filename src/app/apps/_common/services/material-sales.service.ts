import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {GenericSAPService} from "./_generic_sap_service";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class MaterialSales {
  Material:string;
  SalesOrganization:string;
}

@Injectable()
export class MaterialSalesService extends GenericSAPService {

  constructor(protected sapService: SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entitySetName_SAP = "MaterialSalesSet"
  }

  getMaterialSales(query: any) {
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let params = {};
        if (query) {
          let filters = [];
          if (query.SalesOrganization != null) {
            let filter = new sap.ui.model.Filter({
              path: 'SalesOrganization',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['SalesOrganization']
            });
            filters.push(filter);
          }
          if (query.Material != null) {
            let filter = new sap.ui.model.Filter({
              path: 'Material',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query['Material']
            });
            filters.push(filter);
          }
          params['filters'] = filters;
        }
        return this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      })
    })
  }
}



