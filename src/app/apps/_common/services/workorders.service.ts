import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SessionStorageService} from "../../../shared/session-storage/session-storage.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {ConfirmationSAP} from "./confirmations-sap.service";

declare let sap: any;

export class WorkOrderSAP {
  MatDescription: string;
  Plant: string;
  PackIns?: string;
  DefaultQuantity: number;
  Uom: string;
  Status: string;
  ScrapQuantity: number;
  ConfirmedQuantity: number;
  WorkOrderNumber: string;
  Material: string;
  RequiredQuantity: number;
  Warehouse: string;
  Priority:string;
  Group:string;
  WConfLink?: {results:ConfirmationSAP[]};
  WPackLink?: {results:PallettizationSAP[]};
  WComponentLink?: {results:ComponentSAP[]};
  WOperationLink?: {results:OperationSAP[]};
  WBatchLink?: {results:ProdBatch[]};
}

export class ComponentSAP {
  Plant:string;
  StorLoc:string;
  WorkOrderNumber:string;
  ComponentCode:string;
  ReqQuantity:number;
  ConsQuantity:number;
  Uom:string;
  StockQuantity:number;
  CompDescription:string;
  ItemNumber:string;
  Action?:string;
}

export class OperationSAP {
  WorkOrderNumber: string;
  OpNumber: string;
  WorkCenter: string;
  Milestone: boolean;
  Description: string;
  Plant: string;
}

export class PallettizationSAP {
  WorkOrderNumber: string;
  PackCode: string;
  ReqQuantity: number;
  ConsQuantity: number;
  Uom: string;
  StockQuantity: number;
  PackDescription:string;
}

export class ProdBatch {
  WorkOrderNumber?:string;
  BatchNumber?:string;
  CreationDate?:string;
  CreationTime?:string;
}

@Injectable()
export class WorkOrdersService {

  entityName = "WorkOrder";

  serviceNameSAP = "zworkorders_srv";
  entityNameSAP = "WorkOrderSet";

  constructor(private sapService: SAPService) {
  }

  getWorkOrder(id: string) {
    return Observable.create((observer) => {
      id = "'" + id + "'";
      let requestParameters = {urlParameters: {$expand: "WComponentLink,WOperationLink,WPackLink,WBatchLink"}};
      this.sapService.getItem(this.serviceNameSAP, this.entityNameSAP, null, id, requestParameters).subscribe((response) => {
        observer.next(response);
        observer.complete();
      });
    });

  }


  getWorkOrders(query) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {
        if (query) {
          let filters = [];
          if (query.Plant != null) {
            let plantFilter = new sap.ui.model.Filter({
              path: 'Plant',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Plant
            });
            filters.push(plantFilter);
          }

          if (query.Material != null) {
            let MaterialFilter = new sap.ui.model.Filter({
              path: 'Material',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Material
            });
            filters.push(MaterialFilter);
          }

          if (query.WorkCenter != null) {

              let workCenterFilter = new sap.ui.model.Filter({
                path: 'WorkCenter',
                operator: sap.ui.model.FilterOperator.EQ,
                value1: query.WorkCenter
              });

              filters.push(workCenterFilter);


          }

          if (query.WorkOrderNumber != null) {

            let workOrderNumberFilter = new sap.ui.model.Filter({
              path: 'WorkOrderNumber',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.WorkOrderNumber
            });

            filters.push(workOrderNumberFilter);


          }

          // "WORKING", "RELEASED"
          if (query.Status != null) {
            let StatusFilter = new sap.ui.model.Filter({
              path: 'Status',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.Status
            });
            filters.push(StatusFilter);
          }

          params['filters'] = filters;
          params['sorters'] = [
            new sap.ui.model.Sorter("Priority",false)
          ];
          params['urlParameters'] = {$expand: 'WOperationLink,WConfLink'};

        }

        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }


}






