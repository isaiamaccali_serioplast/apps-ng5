import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {HREventSubtype} from "./hr-event-subtype.service";
import {TypeLink} from "./type-link.service";

export class HREventSubtypeLink extends GenericObj{
  subtype_id?:HREventSubtype;
  country?:string;
  short_description?:string;
  has_docs?:boolean;
  type?:TypeLink;
  delta_rel?:number;
}

@Injectable()
export class HREventSubtypesLinkService extends GenericService {

  fieldName = "subtype";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Event Subtype Link";
    this.serviceURL = "calendars/event-subtype-links/"
  }


  getEventSubtypeLinks(query: any) {
    return this.getList(query);
  }

  getEventSubtypeLink(id: number) {
    return this.getItem(id);
  }

  saveEventSubtypeLink(hrestl: HREventSubtypeLink) {
    return this.saveItem(hrestl);
  }

  deleteEventSubtypeLink(mstl: HREventSubtypeLink) {
    return this.deleteItem(mstl);
  }

  restoreEventSubtypeLink(mstl: HREventSubtypeLink) {
    return this.restoreItem(mstl);
  }
}



