import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Status} from "./status.service";

export class StatusLink extends GenericObj{
  status_id?:Status;
  delta_rel?:number;
  fieldname?:string;
  app?:GenericObj;
}


@Injectable()
export class StatusLinkService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Status link";
    this.serviceURL = "statuses/status/";
  }

  getStatusLinks(query?: any) {
    return this.getList(query);
  }
}


