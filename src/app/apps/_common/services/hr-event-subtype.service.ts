import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {HREventSubtypeLink, HREventSubtypesLinkService} from "./hr-event-subtype-link.service";

export class HREventSubtype extends GenericObj{
  label?:string;
}

@Injectable()
export class HREventSubtypesService extends GenericService{

  entityName = "EVENT-SUBTYPES";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService,
              protected eventSubtypeLinkService:HREventSubtypesLinkService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Event Subtypes";
    this.serviceURL = "calendars/event-subtypes/";
  }

  getEventSubtypes(query: any) {
    return this.getList(query);
  }

  getEventSubtype(id: number) {
    return this.getItem(id);
  }

  saveEventSubtype(mst: HREventSubtype) {
    return this.saveItem(mst);
  }

  deleteEventSubtype(mst: HREventSubtype) {
    return Observable.create((observer) => {

      this.eventSubtypeLinkService.getEventSubtypeLinks({subtype_id__label: mst.label}).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          this.deleteEventSubtypeLinks(data['results'], 0, ()=> {
            this.deleteItem(mst).subscribe((resp)=>{
              observer.next(resp);
              observer.complete();
            });
          })
        }
        else {
          this.deleteItem(mst).subscribe((resp)=>{
            observer.next(resp);
            observer.complete();
          });
        }
      });
    });
  }

  deleteEventSubtypeLinks(sls:HREventSubtypeLink[],index:number,callback:any,lastResponse?:any){
    if(sls[index]){
      let sl = sls[index];
      this.eventSubtypeLinkService.deleteEventSubtypeLink(sl).subscribe((resp)=>{
        if(resp.status==204){
          lastResponse = resp;
          this.deleteEventSubtypeLinks(sls,index+1,callback,lastResponse);
        }
        else {
          callback.call(null,resp);
        }
      })
    }
    else {
      if(!lastResponse){
        lastResponse = {
          status: 204
        }
      }
      else {
        callback.call(null,lastResponse)
      }
    }
  }

  restoreEventSubtype(mst: HREventSubtype) {
    return this.restoreItem(mst);
  }
}



