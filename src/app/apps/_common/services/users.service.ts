/**
 * Service used to retreive users data. Uses a cache to optimize performance
 */
import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {Observable} from "rxjs";
import {ConfigurationService} from "../../../shared/serionet/service/configuration.service";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {AssetUser} from "./asset-user.service";

export class User extends GenericObj{
  first_name?: string;
  last_name?: string;
  username?: string;
  password?: string;
  badge_code?: string;
  avatar?:string;
  badge_language:string;
  calendar?:any;
  sap_license?:AssetUser
}

@Injectable()
export class UsersService extends GenericService{

  usersCache = {};

  constructor(protected serionetService:SerionetService, protected loggingService:LoggingService,private confService:ConfigurationService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "Users";
    this.serviceURL = "user/";
  }

  getUserData(id:number){

    return Observable.create((observer) => {

      let u;
      if(u = this.getCachedUser(id)){
        observer.next(u);
        observer.complete();
      }
      else {
        let serionetOptions = new SerionetRequestOptions();
        serionetOptions.options = {};

        return this.serionetService.doGet(this.serviceURL+id+"/",serionetOptions).subscribe((user) => {
          user['avatar'] = this.confService.get("SERVER")+user['avatar'];
          this.storeUser(user);
          observer.next(user);
          observer.complete();
        });
      }
    });
  }

  storeUser(user:User){
    this.usersCache[user.pk] = user;
  }

  getCachedUser(id:number){
    return this.usersCache[id];
  }
}

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform{

  constructor(private userService:UsersService){}

  transform(id:number) {
    return this.userService.getUserData(id);
  }
}


