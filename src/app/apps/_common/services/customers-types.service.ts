import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class CustomerType {
  value:string;
  label:string;
}

@Injectable()
export class CustomerTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Customers Types";
    this.serviceURL = "organizations/customers-types/";
  }

  getCustomersTypes(query: any) {
    return this.getList(query);
  }
}
