import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

export class TrainingArea extends GenericObj {
  label?:string;
}

@Injectable()
export class TrainingAreaService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "TrainingArea";
    this.serviceURL = "hr-events/training-areas/";
  }

  getTrainingAreas(query: any) {
    return this.getList(query);
  }

  getTrainingArea(id: number) {
    return this.getItem(id);
  }
}



