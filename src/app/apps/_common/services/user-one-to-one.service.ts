import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {Observable} from "rxjs";
import {URLSearchParams} from "@angular/http";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class UserOneToOneService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  saveItem(item: any, parsToSkip?: string[], timeout?:number) {
    return Observable.create((observer) => {
      let urlParams = new HttpParams();

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };
      if (timeout)
        serionetOptions.timeout = timeout;

      let i_ = GenericService.processItemForSave(item, parsToSkip);

      if (i_.user_id) {
        this.serionetService.doPut(this.serviceURL + i_.user_id, i_, serionetOptions).subscribe((data) => {
          observer.next(data);
          observer.complete();
        }, (response) => {
          this.loggingService.error("Error in updating " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        });
      }
      else {
        this.serionetService.doPost(this.serviceURL, i_, serionetOptions).subscribe((data) => {
          observer.next(data);
          observer.complete();
        }, (response) => {
          this.loggingService.error("Error in creating " + this.entityName + ", status: " + response.status);
          observer.next(response);
          observer.complete();
        });
      }
    });
  }
}



