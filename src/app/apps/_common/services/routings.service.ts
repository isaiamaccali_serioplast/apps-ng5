import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {Observable} from "rxjs";
import {RoutingOperation, RoutingOperation_SAP, RoutingsOperationsService} from "./routings-operations.service";
import {Site, SitesService} from "./sites.service";
import {cloneObject, parseSerionetRespToData} from "../../../shared/utils";
import {Material} from "./materials.service";
import {RoutingStatusLink} from "./routing-status-link.service";
import {BOM, BOMService} from "./bom.service";
import {SAPObject} from "./_generic_sap_service";
import {GenericObj} from "./_generic.service";
import {RoutingProductionVersion, RoutingProductionVersionService} from "./routing-production-version-service";

export class Routing extends GenericObj{
  material?:Material;
  site?:Site;
  group?:string;
  description?:string;
  routing_operations?:RoutingOperation[];
  status?:RoutingStatusLink;
  created_on?:string;

  enable_add_item?:boolean;
  available_pv?:RoutingProductionVersion[];
  productionVersionBOMpk?: number;
  production_version?:RoutingProductionVersion;
  productionVersionToDelete?:RoutingProductionVersion;
  mandatory_pv?:boolean;
  boms?:BOM[];
}

export class Routing_SAP extends SAPObject{
  Code:string;
  Plant:string;
  Group:string;
  Description:string;
  Status:string;
  RoutingLink:RoutingOperation_SAP[];

  enable_add_item:boolean;
  available_boms:BOM[];

  getId(){
    return "Code='"+this.Code+"',Plant='"+this.Plant+"',Group='"+this.Group+"'";
  }
}

@Injectable()
export class RoutingsService extends GenericServiceWithSAP {

  constructor(protected serionetService: SerionetService,
              protected loggingService: LoggingService,
              protected sapService: SAPService,
              private routingsOperationsService: RoutingsOperationsService,
              private sitesService: SitesService,
              private bomService:BOMService,
              private productionVersionService: RoutingProductionVersionService
  ) {
    super(serionetService, loggingService, sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "RoutingHeaderSet";
    this.serviceName_SAP = "ZMATERIALS_SRV";
    this.entityName = "Routing";
    this.serviceURL = "materials/routing-headers/";
  }

  getRouting(id: any, revisionDate?: string,params?:any) {
    return this.getItem(id, false, revisionDate,params);
  }

  getRoutings(query: any) {
    return this.getList(query);
  }

  saveRouting(i: Routing) {
    return this.saveItem(i, ['routing_operations', 'production_version', 'enable_add_item', 'available_boms']);
  }

  deleteRouting(i: Routing) {
    return Observable.create((observer) => {
      this.productionVersionService.getRoutingProductionVersions({routing_header: i.pk}).subscribe((resp)=>{
        let pvs:RoutingProductionVersion[] = parseSerionetRespToData(resp);
        let count = pvs.length;

        if (count > 0){
          let callback = ()=>{
            count--;
            if(count==0){
              this.batchDeletePV(pvs,0, observer,()=>{
                this.deleteItem(i).subscribe((resp)=>{
                  observer.next(resp);
                  observer.complete();
                })
              })
            }
          };
          pvs.forEach((pv)=>{
            this.bomService.getBOM(pv.bom_header.pk).subscribe((b)=>{
              pv.bom_header = b;
              pv.routing_header = cloneObject(i);
              callback();
            })
          });
        }else {
          this.deleteItem(i).subscribe((resp)=>{
            observer.next(resp);
            observer.complete();
          })
        }
      });
    });
  }

  batchDeletePV(pvs:RoutingProductionVersion[],index, observer,callback){
    if(pvs[index]){
      let pv = pvs[0];
      this.productionVersionService.deleteRoutingProductionVersion(pv).subscribe((resp)=>{
        if(resp.status == 204){
          this.batchDeletePV(pvs,index+1, observer,callback);
        }
        else {
          observer.next(resp);
          observer.complete();
        }
      });
    }
    else {
      callback.call(this);
    }
  }

  restoreRouting(item: Routing) {
    return Observable.create((observer) => {
      let item_SAP = this.getSAPObject(item);
      this.sitesService.getSite(item.site.pk).subscribe((site) => {
        item.site = site;
        if (item.site.active_on_sap) {
          this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP.getId()).subscribe((response) => {
            if (response.status == 200) {
              item.is_active = true;
              //Faccio il restore dell'header solo su serionet (casistica strana ma la prevedo)
              this.saveItemOnSerionet(this.clearItem(item), ['routing_operations', 'production_version', 'enable_add_item', 'available_boms']).subscribe((response__) => {
                if(response__.pk){
                  this.processProductionVersions(item, !item.site.active_on_sap).subscribe((resp__) => {
                    observer.next(resp__);
                    observer.complete();
                  })
                }
                else {
                  observer.next(response__);
                  observer.complete();
                }

              })
            }
            else {
              if (item_SAP['RoutingLink'])
                delete item_SAP['RoutingLink'];
              this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP).subscribe((response_) => {
                if (response_.status > 200 && response_.status < 300) {
                  item.is_active = true;
                  this.saveItemOnSerionet(this.clearItem(item), ['routing_operations', 'production_version', 'enable_add_item', 'available_boms']).subscribe((response__) => {
                    if(response__.pk){
                      this.processProductionVersions(item, !item.site.active_on_sap).subscribe((resp__) => {
                        observer.next(resp__);
                        observer.complete();
                      })
                    }
                    else {
                      observer.next(response__);
                      observer.complete();
                    }
                  })
                }
                else {
                  response_['context']='SAP';
                  observer.next(response_);
                  observer.complete();
                }
              })
            }
          });
        }
        else {
          this.saveItemOnSerionet(this.clearItem(item), ['routing_operations', 'production_version', 'enable_add_item', 'available_boms']).subscribe((response) => {
            observer.next(response);
            observer.complete();
          })
        }
      });
    });
  }

  getSAPObject(i: Routing) {
    let routingSAP = new Routing_SAP();
    routingSAP.Code = i.material.code;
    if (i.group)
      routingSAP.Group = i.group || null;
    routingSAP.Description = i.description;
    routingSAP.Plant = i.site.code;
    routingSAP.Status = i.status.sap_code;
    routingSAP.RoutingLink = i.routing_operations.filter((roa: RoutingOperation) => {
      return roa.status != "toDelete";
    }).map((ro: RoutingOperation) => {
      let ro_ = cloneObject(ro);
      ro_.routing_header = new Routing();
      ro_.routing_header.pk = i.pk;
      ro_.routing_header.material = cloneObject(i.material);
      ro_.routing_header.group = i.group;
      ro_.routing_header.site = cloneObject(i.site);
      return RoutingsOperationsService.getSAPObject(ro_);
    });
    return routingSAP;
  }

  protected saveItem(item: Routing, parsToSkip?: string[], forceCreate?: boolean) {
    return Observable.create((observer) => {
      let item_SAP = this.getSAPObject(item);

      this.sitesService.getSite(item.site.pk).subscribe((site) => {
        item.site = site;
        if (item.site.active_on_sap) {
          this.sapService.getItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP.getId()).subscribe((response) => {
            if (response.status == 200) {
              delete item_SAP['RoutingLink'];
              this.sapService.updateItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP.getId(), item_SAP).subscribe((response_) => {
                if (response_.status > 200 && response_.status < 300) {
                  this.saveItemOnSerionet(this.clearItem(item), parsToSkip).subscribe((response__) => {
                    if (response__.pk) {
                      item.pk = response__.pk;
                      this.processOperationAndPV(item).subscribe((resp) => {
                        observer.next(resp);
                        observer.complete();
                      })
                    }
                    else {
                      observer.next(response__);
                      observer.complete();
                    }
                  })
                }
                else {
                  response_['context']='SAP';
                  observer.next(response_);
                  observer.complete();
                }
              })
            }
            else {
              this.sapService.createItem(this.serviceName_SAP, this.entitySetName_SAP, null, item_SAP).subscribe((response_) => {
                if (response_.status > 200 && response_.status < 300) {
                  item.group = response_['response']['Group'];
                  this.saveItemOnSerionet(this.clearItem(item), parsToSkip).subscribe((response__) => {
                    if (response__.pk) {
                      item.pk = response__.pk;
                      item.routing_operations.forEach((ro: RoutingOperation) => {
                        ro.routing_header = cloneObject(item);
                        ro.routing_header.routing_operations = [];
                      });
                      this.processOperationAndPV(item, true).subscribe((resp) => {
                        observer.next(resp);
                        observer.complete();
                      })
                    }
                    else {
                      observer.next(response__);
                      observer.complete();
                    }
                  })
                }
                else {
                  response_['context']='SAP';
                  observer.next(response_);
                  observer.complete();
                }
              })
            }
          });
        }
        else {
          this.saveItemOnSerionet(this.clearItem(item), parsToSkip).subscribe((response) => {
            if (response.pk) {
              item.pk = response.pk;
              this.processOperationAndPV(item, true).subscribe((resp) => {
                observer.next(resp);
                observer.complete();
              })
            }
            else {
              observer.next(response);
              observer.complete();
            }
          })
        }
      });
    });
  }

  processOperationAndPV(item: Routing, skipSAPForOperations = false) {
    return Observable.create((observer) => {
      let opsToSave = item.routing_operations.filter((ro: RoutingOperation) => {
        return ro.status == "toSave";
      });
      let opsToDelete = item.routing_operations.filter((ro: RoutingOperation) => {
        return ro.status == "toDelete" && ro.pk;
      });
      this.processOperationsToDelete(item, opsToDelete, skipSAPForOperations).subscribe((resp_) => {
        if (resp_.status == 204) {
          this.processOperationsToSave(item, opsToSave, skipSAPForOperations).subscribe((resp) => {
            if (resp.pk) {
              this.processProductionVersions(item, !item.site.active_on_sap).subscribe((resp__) => {
                observer.next(resp__);
                observer.complete();
              })
            }
            else {
              observer.next(resp);
              observer.complete();
            }
          })
        }
        else {
          observer.next(resp_);
          observer.complete();
        }
      })
    });
  }

  processOperationsToSave(r: Routing, ros: RoutingOperation[], skipSAPForOperations = false) {
    return Observable.create((observer) => {
      if (ros.length > 0) {
        ros.forEach((ro: RoutingOperation) => {
          ro.routing_header = r;
        });
        if (!skipSAPForOperations) {
          skipSAPForOperations = !r.site.active_on_sap
        }
        this.saveOperations(ros, 0, skipSAPForOperations, (resp) => {
          observer.next(resp);
          observer.complete();
        });
      }
      else {
        observer.next({pk: 999999});
        observer.complete();
      }
    });
  }

  saveOperations(ros: RoutingOperation[], index: number, skipSAPForOperations: boolean, callback, lastResp?: any) {
    if (ros[index]) {
      let ro = ros[index];
      this.routingsOperationsService.saveRoutingOperation(ro, skipSAPForOperations).subscribe((resp) => {
        if (resp.pk) {
          this.saveOperations(ros, index + 1, skipSAPForOperations, callback, resp);
        }
        else {
          callback(resp);
        }
      })
    }
    else {
      if (!lastResp)
        lastResp = {pk: 999999};
      callback(lastResp);
    }
  }

  processOperationsToDelete(r: Routing, ros: RoutingOperation[], skipSAPForOperations = false) {
    return Observable.create((observer) => {
      if (ros.length > 0) {
        ros.forEach((ro: RoutingOperation) => {
          ro.routing_header = r;
        });
        if (!skipSAPForOperations) {
          skipSAPForOperations = !r.site.active_on_sap
        }
        this.deleteOperations(ros, 0, skipSAPForOperations, (resp) => {
          observer.next(resp);
          observer.complete();
        });
      }
      else {
        observer.next({status: 204});
        observer.complete();
      }
    });
  }

  deleteOperations(ros: RoutingOperation[], index: number, skipSAPForOperations: boolean, callback, lastResp?: any) {
    if (ros[index]) {
      let ro = ros[index];
      this.routingsOperationsService.deleteRoutingOperation(ro, skipSAPForOperations).subscribe((resp) => {
        if (resp.status == 204) {
          this.deleteOperations(ros, index + 1, skipSAPForOperations, callback, resp);
        }
        else {
          callback(resp);
        }
      })
    }
    else {
      if (!lastResp)
        lastResp = {pk: 999999};
      callback(lastResp);
    }
  }

  processProductionVersions(r: Routing, skipSAPForOperations) {
    return Observable.create((observer) => {
      if (r.productionVersionToDelete) {
        r.productionVersionToDelete.routing_header = cloneObject(r);
        this.productionVersionService.deleteRoutingProductionVersion(r.productionVersionToDelete, skipSAPForOperations).subscribe((resp) => {
          if (resp.status == 204) {
            if (r.production_version.bom_header && r.production_version.bom_header.pk) {
              r.production_version.routing_header = cloneObject(r);
              r.production_version.is_active = true;
              this.productionVersionService.saveRoutingProductionVersion(r.production_version, skipSAPForOperations).subscribe((resp_) => {
                observer.next(resp_);
                observer.complete();
              })
            }
            else {
              observer.next({pk: 999999999});
              observer.complete();
            }
          }
          else {
            observer.next(resp);
            observer.complete();
          }
        });
      }
      else {
        if (r.production_version.bom_header && r.production_version.bom_header.pk) {
          r.production_version.routing_header = cloneObject(r);
          r.production_version.is_active = true;
          this.productionVersionService.saveRoutingProductionVersion(r.production_version, skipSAPForOperations).subscribe((resp) => {
            observer.next(resp);
            observer.complete();
          })
        }
        else {
          observer.next({pk: 99999999999});
          observer.complete();
        }
      }
    });
  }

  clearItem(r: Routing) {
    let r_: Routing = cloneObject(r);
    delete r_.routing_operations;
    delete r_.available_pv;
    delete r_.production_version;
    delete r_.productionVersionToDelete;
    delete r_.enable_add_item;
    delete r_.productionVersionBOMpk;
    delete r_.mandatory_pv;
    return r_;
  }
}


