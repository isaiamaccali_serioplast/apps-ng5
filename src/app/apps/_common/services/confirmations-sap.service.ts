import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SAPService} from "../../../shared/sap/sap.service";

declare let sap: any;

export class ConfirmationSAP {
  WorkOrderNumber:string;
  ConfNumber?:number;
  QtyConfirmed:string;
  Uom:string;
  QtyScrapped:string;
  SSCC?:string;
  CreationDate:string;
  CreationTime:string;
  CID:string;
  Operation:string;
  PrinterName:string;
}


@Injectable()
export class ConfirmationSapService {
  entityName = "Confirmation";
  serviceNameSAP = "zworkorders_srv";
  entityNameSAP = "ConfirmationSet";

  constructor(private sapService: SAPService) {
  }

  createConfirmation(item:ConfirmationSAP) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {
        this.sapService.createItem(this.serviceNameSAP,this.entityNameSAP,null,item).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }

  updateConfirmation(item:ConfirmationSAP) {
    return Observable.create((observer) => {
      this.sapService.ready().subscribe(() => {

        let key = "ConfNumber='" + item.ConfNumber + "',WorkOrderNumber='"+item.WorkOrderNumber+"'";
        this.sapService.updateItem(this.serviceNameSAP,this.entityNameSAP,null,key,item).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }

  getConfirmations(query) {
    return Observable.create((observer) => {
      let params = {};
      this.sapService.ready().subscribe(() => {
        if (query) {
          let filters = [];
          if (query.WorkOrderNumber != null) {

            let workOrderNumberFilter = new sap.ui.model.Filter({
              path: 'WorkOrderNumber',
              operator: sap.ui.model.FilterOperator.EQ,
              value1: query.WorkOrderNumber
            });
            filters.push(workOrderNumberFilter);
          }
          params['filters'] = filters;
        }
        this.sapService.getItems(this.serviceNameSAP, this.entityNameSAP, null, params).subscribe((response) => {
          observer.next(response);
          observer.complete();
        });
      });

    });
  }


}



