import {Injectable} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";
import {Observable} from "rxjs";

declare let sap: any;

export class PlantSAP {
  Name: string;
  Code: string;
  Company: string;
  NewFlow: string;
  SalesOrganization: string;
  PurchaseOrganization: string;
}

@Injectable()
export class PlantSAPService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "PlantSet";
    this.serviceName_SAP = "ZUTILITIES_SRV";
  }

  getPlants(query: any) {
    return Observable.create((observer)=>{
      this.sapService.ready().subscribe(()=>{
        let params = {};
        let filters = [];
        if (query.Code != null) {
          let plantFilter = new sap.ui.model.Filter({
            path: 'Code',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.Code
          });
          filters.push(plantFilter);
        }
        if (query.PurchaseOrganization != null) {
          let poFilter = new sap.ui.model.Filter({
            path: 'PurchaseOrganization',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.PurchaseOrganization
          });
          filters.push(poFilter);
        }

        if (query.SalesOrganization != null) {
          let soFilter = new sap.ui.model.Filter({
            path: 'SalesOrganization',
            operator: sap.ui.model.FilterOperator.EQ,
            value1: query.SalesOrganization
          });
          filters.push(soFilter);
        }

        params['filters'] = filters;
        this.getList(params).subscribe((resp)=>{
          observer.next(resp);
          observer.complete();
        });
      })
    })

  }

  getPlant(id:string){
    return this.getItem(id);
  }

}


