import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class AssetSubtypeLink extends GenericObj {
  fieldname:string;
}

@Injectable()
export class AssetSubTypeLinkService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Asset Subtype Links";
    this.serviceURL = "operations/asset-subtype-links/";
  }

  getAssetSubtypeLinks(query?: any) {
    return this.getList(query);
  }

  getAssetSubtypeLink(id: number) {
    return this.getItem(id,true);
  }
}


