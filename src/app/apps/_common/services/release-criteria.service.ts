import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {ControlPlanMeasure} from "./control-plan-measures.service";
import {Criteria} from "./criteria.service";

export class ReleaseCriteria extends GenericObj {
  criteria?:number;
  parameters?:string;
  control_plan_measure?:ControlPlanMeasure;

  criteria_full?:Criteria;
  parameters_list?:any[] = [];
}


@Injectable()
export class ReleaseCriteriaService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "ReleaseCriteria";
    this.serviceURL = "qc/release-criteria";
  }

}


