import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";

export class ProductionType {
  pk:number;
  __str__:string;
  type_id?: {
    pk: number,
    __str__: string
  };
  app: number;
  fieldname: string;
  delta_rel: number;
}

@Injectable()
export class ProductionTypesService extends GenericService{

  fieldName = "production_type";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Production Type";
    this.serviceURL = "materials/productiontypelink/";
  }

  getProductionTypes(query:any) {
    if(!query)
      query = {}
    query['fieldname'] = this.fieldName;
    return this.getList(query);
  }
}



