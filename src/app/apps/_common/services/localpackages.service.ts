import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {ContentType} from "./content-type.service";
import {WorkToolType} from "./worktooltype.service";
import {Site} from "./sites.service";
import {HRProfile} from "./hr-profiles.service";
import {LocalCpnPackageItem} from "./localcpnpackageitem.service";

export class LocalPackageItem extends GenericObj {
  content_type:ContentType;
  object_id: number;
  get_wt_object?:string;
  type: WorkToolType;
  site?: Site;
  reference?: HRProfile;
  label?: string;
  sitecpnpackageitem_set: LocalCpnPackageItem[];
}

@Injectable()
export class LocalPackagesService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Local PackageName Item";
    this.serviceURL = "packages/sitepackageitem/";
  }

  getLocalPackages(query: any) {
    return this.getList(query);
  }

  addLocalPackage(localpackage: LocalPackageItem) {
      return this.saveItem(localpackage);
  }

  removeLocalPackageItem(localpackage: LocalPackageItem) {
    return this.deleteItem(localpackage);
  }

  updatePackageItem(localpackage: LocalPackageItem) {
    return this.saveItem(localpackage);
  }

}



