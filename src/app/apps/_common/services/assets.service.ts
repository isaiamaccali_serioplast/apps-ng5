import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {cloneObject} from "../../../shared/utils";
import {AssetType} from "./asset-types.service";
import {Organization} from "./organization.service";
import {AssetSubtypeLink} from "./asset-subtype-link.service";
import {Workcenter} from "./workcenters.service";
import {StatusLink} from "./status-link.service";
import {Site} from "./sites.service";
import {SPFile} from "./file.service";
import {HRProfile} from "./hr-profiles.service";

export class Asset extends GenericObj {
  type?: AssetType;
  manufacturer?: Organization;
  model?: string;
  name?:string;
  description?: string;
  workcenter?: Workcenter;
  year?: number;
  location?: Site;
  ownership?:Organization;
  bookable?: boolean;
  manufacturer_code?: number;
  sap_code?: string;
  status?: StatusLink;
  photo?: SPFile;
  assigned_to?: HRProfile;
  sub_type?:AssetSubModel;
}

export class AssetSubModel {
  angle?:number;
  ca_max_pressure?:number;
  ca_output?:number;
  clamping_force?:number;
  extruder?:number;
  extruder_diam?:string;
  general_main_switch?:number;
  heating_zones:number;
  installed_electrical_power?:number;
  kva_trafo?:number;
  license_plate?:string;
  license_type?:AssetSubtypeLink;
  manifold?:number;
  mould_dimension?:string;
  mould_dim_1:number;
  mould_dim_2:number;
  mould_pitch?:number;
  neck_orientation:boolean;
  oven_pitch?:number;
  pitch1:number;
  pitch2:number;
  preferential_heating?:boolean;
  pressure_gap?:number;
  shooting_volume?:number;
  software_type?:AssetSubtypeLink;
  sub_type?:AssetSubtypeLink;
  thermic_power?:number;
  throughtput?:number;
  tot_cavities?:number;
  total_heating_power:number;
  version:string;
  water_flowrate?:number;
}

@Injectable()
export class AssetsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Assets";
    this.serviceURL = "operations/assets/";
  }

  getAssets(query?: any) {
    return this.getList(query);
  }

  getAsset(id: number) {
    return this.getItem(id, true);
  }

  saveAsset(a: Asset, skipProcess?:boolean) {
    let a_ = cloneObject(a);
    if(!skipProcess)
      a_.sub_type = GenericService.processItemForSave(a_.sub_type);
    return this.saveItem(a_, ['sub_type']);
  }

  restoreAsset(a: Asset) {
    let a_ = cloneObject(a);
    a_.sub_type = GenericService.processItemForSave(a_.sub_type);
    return this.restoreItem(a,['sub_type']);
  }

  deleteAsset(a: Asset) {
    return this.deleteItem(a);
  }

}


