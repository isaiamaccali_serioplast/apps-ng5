import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";

export class SupplierType {
  value:string;
  label:string;
}

@Injectable()
export class SupplierTypeService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Suppliers Types";
    this.serviceURL = "organizations/suppliers-types/";
  }

  getSuppliersTypes(query: any) {
    return this.getList(query);
  }
}
