import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {EducationType} from "./education-type.service";
import {EducationStudyField} from "./education-study-fields.service";

export class Education extends GenericObj {
  user_id : number;
  date_from: string;
  date_to:string;
  is_employee: boolean;
  avatar:string;
  degree: EducationType;
  study_field :EducationStudyField;
  institution :string;
  country :string;
  city :string;
  final_grade:string;
  description :string;
}

@Injectable()
export class EducationService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Education";
    this.serviceURL = "hrprofiles/education/";
  }

  getEducations(query:any){
    return this.getList(query);
  }

  deleteEducation(p: Education) {
    return this.deleteItem(p);
  }

  saveEducation(p: Education) {
    return this.saveItem(p);
  }
}



