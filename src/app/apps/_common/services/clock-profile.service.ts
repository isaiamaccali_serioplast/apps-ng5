import {GenericObj, GenericService} from "./_generic.service";
import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {cloneObject} from "../../../shared/utils";

export class ClockProfile extends GenericObj {
  name?: string;
  start_time?: string;
  end_time?: string;
  break_period?: boolean;
  start_break_period?: string;
  end_break_period?: string;
  overtime?: boolean;
  start_time_max_delay?: number;
  start_time_permitted_delay?: number;
  break_time_max_delay?: number;
  break_time_permitted_delay?: number;
  min_break_duration?: string;
  tot_work_hours?: number;
  shift?: boolean;
  start_check_period?: number;
  end_check_period?: number;
  color?:string;
}

@Injectable()
export class ClockProfileService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Clock Profiles";
    this.serviceURL = "salaries/clock-profiles/";
  }

  getClockProfiles(query: any) {
    return this.getList(query);
  }

  saveClockProfile(i:ClockProfile){
    return this.saveItem(i);
  }

  getClockProfile(id: number) {
    return this.getItem(id);
  }

  deleteClockProfile(i:ClockProfile) {
    return this.deleteItem(i);
  }

  restoreClockProfile(i:ClockProfile) {
    return this.restoreItem(i);
  }

  clearItem(i:ClockProfile){
    let i_:ClockProfile = cloneObject(i);
    i_.start_time_max_delay = i_.start_time_max_delay*60;
    i_.start_time_permitted_delay = i_.start_time_permitted_delay*60;
    i_.break_time_max_delay = i_.break_time_max_delay*60;
    i_.break_time_permitted_delay = i_.break_time_permitted_delay*60;
    return i_;
  }
}


