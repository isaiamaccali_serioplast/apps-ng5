import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Organization} from "./organization.service";

export class TestMethod extends GenericObj {
  organizations?:Organization;
  standard_type?: GenericObj
}

@Injectable()
export class TestMethodService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Test Method";
    this.serviceURL = "qc/testmethod/";
  }

  getTestMethods(query:any) {
    return this.getList(query);
  }

  getTestMethod(id:number, sType:number) {
    return Observable.create((observer) => {

      this.getTestMethods({
        pk:id,
        stype:sType,
        is_active:true
      }).subscribe((data)=>{
        if (data && data['count'] && data['count'] > 0) {
          observer.next(data['results'][0]);
        }
        else {
          observer.next(data);
        }
        observer.complete();
      })
    });
  }
}

@Pipe({
  name: 'test_method'
})
export class TestMethodPipe implements PipeTransform{

  constructor(private testMethodService:TestMethodService){}

  transform(id:number, sType:number) {
    return this.testMethodService.getTestMethod(id,sType);
  }
}



