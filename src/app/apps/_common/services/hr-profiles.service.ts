import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {HRUser} from "./hr-users.service";
import {SPFile} from "./file.service";

export class HRProfile extends GenericObj {
  user_id: HRUser;
  is_employee: boolean;
  is_service: boolean;
  avatar:SPFile;
  timezone:string;
  tags:ProfileTag[];

  calendar?:any;
}

export class ProfileTag extends GenericObj{
  label: string;
}


@Injectable()
export class HRProfileService extends GenericService {

  constructor(protected serionetService:SerionetService,protected loggingService:LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "HR Profiles";
    this.serviceURL = "hrprofiles/profiles/";
  }

  getHRProfiles(query:any){
    return this.getList(query);
  }

  getEmployeesProfile(query:any){
    query['is_employee']="2";
    return this.getList(query);
  }

  getHrProfile(id:number) {
    return this.getItem(id,false);
  }

  saveHrProfile(item:HRProfile){
    return this.saveItem(item);
  }

  deleteHrProfile(item:HRProfile){
    return this.deleteItem(item);
  }
}



