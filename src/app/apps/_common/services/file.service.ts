import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {SerionetRequestOptions, SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {Observable} from "rxjs";
import {TypeLink} from "./type-link.service";
import {FileManager} from "./file-manager.service";
import {HttpParams} from "@angular/common/http";

export class SPFile extends GenericObj {
  attachment_file?: string;
  content_type?: number;
  field_name?: string;
  'private'?: boolean;
  doc_id?: string;
  version?: string;
  type?: TypeLink;
  notes?: string;
  related_objects?:FileManager[];
}

@Injectable()
export class FileService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "File";
    this.serviceURL = "files/files/";
  }

  getFiles(query?: any) {
    return this.getList(query);
  }

  getFile(id: number) {
    return this.getItem(id);
  }

  saveFile(f:SPFile, input?: any) {
    return Observable.create((observer) => {
      let form = document.createElement("form");
      if(input){
        let clonedInput = input.cloneNode();
        clonedInput.setAttribute("name", "attachment_file");
        form.appendChild(clonedInput);
      }

      let inputData = document.createElement("input");
      inputData.setAttribute("name", "data");
      inputData.value = JSON.stringify(GenericService.processItemForSave(f,['related_objects']));
      form.appendChild(inputData);

      let urlParams = new HttpParams();

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {
        params: urlParams
      };

      const formData = new FormData(form);

      this.serionetService.doPostFile(this.serviceURL, formData, serionetOptions).subscribe((data) => {
        observer.next(data);
        observer.complete();
      }, (response) => {
        this.loggingService.error("Error in posting " + this.entityName + ", status: " + response.status);
        observer.next(response);
        observer.complete();
      });
    });
  }

  deleteFile(f: SPFile) {
    return this.deleteItem(f);
  }
}


@Pipe({
  name: 'file'
})
export class FilePipe implements PipeTransform{

  fileName:string;

  constructor(private fileService:FileService){}

  transform(id:number) {
    return Observable.create((observer)=>{
      if(this.fileName){
        observer.next(this.fileName);
        observer.complete();
      }
      else {
        this.fileService.getFile(id).subscribe((data:SPFile)=>{
          let pieces = data.__str__.split("/");
          this.fileName = pieces[pieces.length - 1];
          observer.next(this.fileName);
          observer.complete();
        });
      }
    });
  }
}
