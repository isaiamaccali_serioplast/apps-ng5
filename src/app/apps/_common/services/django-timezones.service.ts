import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";

export class DjangoTimezone {
  value:string;
  label:string;
}

@Injectable()
export class DjangoTimezoneService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Django Timezone";
    this.serviceURL = "hrprofiles/django-timezones/";
  }

  getDjangoTimezones(query: any) {
    return this.getList(query);
  }
}
