import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {Country} from "./countries.service";

export class Nationality extends GenericObj{
  user_id : number;
  nationality:Country;
  country_id_number?:string;
  country_tax_number?:string;
}

@Injectable()
export class NationalityService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init(){
    this.entityName = "Nationality";
    this.serviceURL = "hrprofiles/nationalities/";
  }

  getNationalities(query:any){
    return this.getList(query);
  }

  deleteNationality(p: Nationality) {
    return this.deleteItem(p);
  }

  saveNationality(p: Nationality) {
    return this.saveItem(p);
  }
}






