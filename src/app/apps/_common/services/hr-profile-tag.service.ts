import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

@Injectable()
export class HrProfileTagService extends GenericService {

  fieldName = "tags";

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "HR Profile TAG";
    this.serviceURL = "hrprofiles/profile-tags/";
  }

  getTags(query: any) {
    return this.getList(query);
  }

  getTag(id: number) {
    return this.getItem(id);
  }
}



