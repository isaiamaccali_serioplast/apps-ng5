import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {MarketSegment} from "./market-segment.service";

export class MarketSegmentLink extends GenericObj{
  sap_code?: string;
  marketsegment_id?:MarketSegment;
  app?: GenericObj;
  delta_rel?: number;
  fieldname: string;
}

@Injectable()
export class MarketSegmentLinksService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Market Segment Links";
    this.serviceURL = "materials/marketsegmentlink/";
  }

  getMarketSegmentLinks(query:any) {
    return this.getList(query);
  }

  getMarketSegmentLink(id:number) {
    return this.getItem(id, true);
  }
}

@Pipe({
  name: 'market_segment_link'
})
export class MarketSegmentLinkPipe implements PipeTransform{

  constructor(private segmentLinkService:MarketSegmentLinksService){}

  transform(id:number) {
    return this.segmentLinkService.getMarketSegmentLink(id);
  }
}




