import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {Observable} from "rxjs";
import {cloneObject} from "../../../shared/utils";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";
import {PropertyType} from "./property-type.service";
import {PropertyName} from "./property-names.service";
import {GenericSortableObj} from "./_generic-sortable.service";
import {Formula} from "./formulas.service";
import {MeasureUnit} from "./measure-unit.service";
import {PropertyAttribute} from "./property-attribute.service";
import {ContentType} from "./content-type.service";
import {PropertySet} from "./property-set.service";

export class Property extends GenericObj {
  type?:PropertyType;
  lsl?: number;
  usl?: number;
  target?: number;
  calculated?:boolean;
  internal?:boolean;
  name?:PropertyName;
  formula?:Formula;
  uom?:MeasureUnit;
  attribute?:PropertyAttribute[];
  parameter_list?:FormulaParameter[];
  content_type?:ContentType;
  object_id?:number;
  note?:string;

  sets?:PropertySet[];
  available_props?:Property[];
}

export class FormulaParameter extends GenericSortableObj{
  property?:number; //property id
  parameter?:number; //property id
}

@Injectable()
export class PropertiesService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Properties";
    this.serviceURL = "qc/properties/";
  }

  getProperties(query:any) {
    return this.getList(query);
  }

  saveProperty(p: Property) {
    return Observable.create((observer) => {

      let p_ = cloneObject(p);

      this.saveItem(p_, ['parameter_list']).subscribe((resp) => {
        observer.next(resp);
        observer.complete();
      });
    })
  }

  getProperty(id:number){
    return this.getItem(id);
  }

  clearItem(p:Property){
    let p_:Property = cloneObject(p);
    if(p_.sets)
      delete p_.sets;
    if(p_.available_props)
      delete p_.available_props;
    return p_;
  }
}

@Pipe({
  name: 'property'
})
export class PropertyPipe implements PipeTransform{

  constructor(private propertyService:PropertiesService){}

  transform(propertyId:number) {

    return this.propertyService.getProperty(propertyId);

  }
}




