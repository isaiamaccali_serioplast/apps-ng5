import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";

@Injectable()
export class TestMethodTypeService extends GenericService{

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Test Method Type";
    this.serviceURL = "qc/testmethodtype/";
  }

  getTestMethodTypes(query:any) {
    return this.getList(query);
  }
}



