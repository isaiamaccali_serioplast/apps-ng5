import {Injectable} from "@angular/core";
import {GenericObj, GenericService} from "./_generic.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {HRProfile} from "./hr-profiles.service";
import {Training} from "./training.service";
import {StatusLink} from "./status-link.service";
import {MeasureUnit} from "./measure-unit.service";
import {PriorityLink} from "./priority-link.service";

export class TrainingUser extends GenericObj {
  user?:HRProfile;
  training?:Training;
  status?:StatusLink;
  time_attendee?:string;
  time_attendee_uom?:MeasureUnit;
  date_completed?:string;
  expire_date?:string;
  final_mark?:PriorityLink;
  evaluation?:string;
  evaluation_date?:string;
  notes?:string;

  user_profile?:HRProfile;
  errorResponse?:any;
}

@Injectable()
export class TrainingUserService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init() {
    this.entityName = "TrainingUser";
    this.serviceURL = "hr-events/training-users/";
  }

  getTrainingUsers(query: any) {
    return this.getList(query);
  }

  getTrainingUser(id: number) {
    return this.getItem(id);
  }

  deleteTrainingUser(trainingUser:TrainingUser){
    return this.deleteItem(trainingUser);
  }

  saveTrainingUser(trainingUser:TrainingUser){
    return this.saveItem(trainingUser);
  }

  restoreTrainingUser(trainingUser:TrainingUser){
    return this.restoreItem(trainingUser);
  }
}



