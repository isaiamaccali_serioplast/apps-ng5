import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericObj, GenericService} from "./_generic.service";

export class MainMaterial extends GenericObj{
  mainMaterial?:string;
  sap_code?:string;
}

@Injectable()
export class MainMaterialsService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService,loggingService);
    this.init();
  }

  init(){
    this.entityName = "Main Material";
    this.serviceURL = "materials/materialsubtype/"
  }

  getMainMaterial(id:number){
    return this.getItem(id)
  }
}

@Pipe({
  name: 'main_material'
})
export class MainMaterialPipe implements PipeTransform{

  constructor(private mainMaterialsService:MainMaterialsService){}

  transform(id:number) {
    return this.mainMaterialsService.getMainMaterial(id);
  }
}



