import {Injectable} from "@angular/core";
import {GenericService} from "./_generic.service";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {StatusLink} from "./status-link.service";

export class WorkcenterStatusMatrix {
  pk: number;
  __str__: string;
  status:number;
  nested_sub_status?:StatusLink[];
}


@Injectable()
export class WorkcenterStatusMatrixService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "WorkcenterStatusMatrix";
    this.serviceURL = "operations/workcenterstatusmatrix/";
  }

  getStatuses(query?: any) {
    return this.getList(query);
  }

}


