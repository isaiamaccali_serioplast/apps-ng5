import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {SAPService} from "../../../shared/sap/sap.service";
import {GenericSAPService} from "./_generic_sap_service";
import {Observable} from "rxjs";

export class PurchaseOrganization {
  PurchaseOrg:string;
  Description:string;
  SupplierLink:{
    results:PurchaseOrganizationSupplier[];
  }
}

export class PurchaseOrganizationSupplier {
  Supplier:string;
  PurchaseOrg:string;
  Name:string;
  Currency:string;
}

@Injectable()
export class PurchaseOrganizationsService extends GenericSAPService {

  constructor(protected sapService:SAPService) {
    super(sapService);
    this.init();
  }

  init() {
    this.entitySetName_SAP = "PurchaseOrganizationSet";
    this.serviceName_SAP = "ZPURCHINFORECORD_SRV";
  }

  getPurchaseOrganizations(query: any, params?:any) {

    return this.getList(params);
  }

  getPurchaseOrganization(id: any, params?:any) {
    return this.getItem(id, params);
  }
}

@Pipe({
  name: 'purchaseOrganization'
})
export class PurchaseOrganizationPipe implements PipeTransform{

  po:PurchaseOrganization = new PurchaseOrganization();

  constructor(private poService:PurchaseOrganizationsService){}

  transform(id: number) {
    if(id){
      return Observable.create((observer)=>{
        this.poService.getPurchaseOrganization("'" + id + "'").subscribe((resp) => {
          if (resp.status == 200) {
            this.po = resp.response;
            observer.next(this.po.PurchaseOrg + " - " + this.po.Description)
          }
        });
      })
    }
    else {
      return id;
    }


  }
}


