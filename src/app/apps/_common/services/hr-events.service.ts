import {Injectable} from "@angular/core";
import {SerionetService} from "../../../shared/serionet/service/serionet.service";
import {LoggingService} from "../../../shared/logging/logging.service";
import {GenericService} from "./_generic.service";
import {HRProfile} from "./hr-profiles.service";

export class HrEventExport {
  pk:number;
  user?: string;
  user_obj?:HRProfile;
  title?: string;
  description?: string;
  location?: string; // google field
  color?: string; // google field
  status?: string; // google field
  start_date?: string;
  start_timezone?: string;
  end_date?: string;
  end_timezone?: string;
  full_day_event?: boolean;
  type?: string;
  sub_type?: string;
  status_event?: string;
  is_active?: boolean;
  attachments?:any;
  calendar_google_id?:string;
  organizer?: string;
  training?:number;

  attendees?:EventAttendee[];
  attendees_calendars?:string[];
}

export class EventAttendee {
  pk?:number;
  user_profile?:HRProfile;
  user?:HRProfile;
  mail?:string;
  organizer?:boolean;
  acceptance?:string;
  is_active?:boolean;
  __str__?:string;
}

@Injectable()
export class HrEventService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
    this.preserveObject = true;
  }

  init() {
    this.entityName = "Event";
    this.serviceURL = "calendars/events/";
  }

  getHrEvents(query: any,format?:string) {
    return this.getList(query,format);
  }

  getHrEvent(id: number) {
    return this.getItem(id);
  }

  saveHrEvent(item:HrEventExport){
    return this.saveItem(item);
  }


}
