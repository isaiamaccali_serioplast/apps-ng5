import {Routes, RouterModule} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {NgModule} from "@angular/core";
import {OrganizationsIndexComponent} from "./components/organizations-index/organizations-index.component";
import {OrganizationViewComponent} from "./components/organization-view/organization-view.component";

// Route Configuration
export const OrganizationsRoutes: Routes = [
  {
    path: '',
    component: OrganizationsIndexComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'detail/:id',
    component: OrganizationViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: OrganizationViewComponent,
    canActivate: [AuthGuard]
  }
];

/**
 * Root route, uses lazy loading to improve performance
 * @type {{path: string; loadChildren: string; canActivate: AuthGuard[]}[]}
 */
export const OrganizationsRoutesRoot: Routes = [
  {
    path: 'organizations',
    loadChildren: './organizations.module.ts#OrganizationsModule',
    //children: packagesRoutes,
    //canActivate: [AuthGuard]
  }
]

@NgModule({
  imports: [RouterModule.forChild(OrganizationsRoutes)],
  exports: [RouterModule],
})
export class OrganizationsRoutingModule {
}
