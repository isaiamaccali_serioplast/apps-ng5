import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {OrganizationsIndexModule} from "./components/organizations-index/organizations-index.module";
import {OrganizationsRoutingModule} from "./organizations.routes";
import {OrganizationViewModule} from "./components/organization-view/organization-view.module";

@NgModule({
  imports: [SharedModule,OrganizationsRoutingModule,OrganizationsIndexModule,OrganizationViewModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class OrganizationsModule {}

