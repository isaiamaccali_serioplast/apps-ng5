import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Supplier, SupplierService} from "../../../_common/services/suppliers.service";
import {Customer, CustomerService} from "../../../_common/services/customers.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {LegalEntityService} from "../../../_common/services/legal-entity.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../shared/utils";
import {AutocompleteComponent} from "../../../../shared/material/autocomplete/autocomplete.component";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {User} from "../../../_common/services/users.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'organization-view',
  templateUrl: 'organization-view.component.html',
  styleUrls: ['organization-view.component.css']
})

export class OrganizationViewComponent extends BaseComponent implements OnInit {
  organization: Organization = null;
  supplierForm: Supplier = null;
  customerForm: Customer = null;

  availableActions = [];
  availableActionsCust = [];
  availableActionsSupp = [];
  users: User[] = [];
  suppliers: Supplier[] = [];
  noItemsMessageSuppliers = this.tr("No suppliers.");
  noItemsMessageCustomers = this.tr("No legal entities.");
  noItemsMessageLegalEntities = this.tr("No legal entities.");
  headerConfCustomers = [
    {
      label: this.tr("SAP Code"),
      valueKey: 'sap_code'
    },
    {
      label: this.tr("SAP Description"),
      valueKey: 'sap_description'
    },
    {
      label: this.tr("Type"),
      valueKey: 'type'
    },
    {
      label: this.tr("Organization"),
      valueKey: 'organization_ref',
      useStr: true
    },
  ];
  headerConfSuppliers = [
    {
      label: this.tr("SAP Code"),
      valueKey: 'sap_code'
    },
    {
      label: this.tr("SAP Description"),
      valueKey: 'sap_description'
    },
    {
      label: this.tr("Type"),
      valueKey: 'type'
    },
    {
      label: this.tr("Organization"),
      valueKey: 'organization_ref',
      useStr: true
    },
  ];
  headerConfLegalEntities = [
    {
      label: this.tr("Name"),
      valueKey: 'name'
    },
    {
      label: this.tr("Code"),
      valueKey: 'code'
    },
    {
      label: this.tr("Country"),
      valueKey: 'country'
    },
    {
      label: this.tr("Organization"),
      valueKey: 'organization_ref',
      useStr: true
    },
  ];

  @ViewChild('customersTable') customersTable: TableWithPagesComponent;
  @ViewChild('suppliersTable') suppliersTable: TableWithPagesComponent;
  @ViewChild('legalentitiesTable') legalentitiesTable: TableWithPagesComponent;

  @ViewChild('SupplierFilterAC') SupplierFilterAC: AutocompleteComponent;
  @ViewChild('CustomerFilterAC') CustomerFilterAC: AutocompleteComponent;

  constructor(private activatedRoute: ActivatedRoute,
              private organizationsservice: OrganizationService,
              private customerservice: CustomerService,
              private supplierservice: SupplierService,
              protected translationService: TranslationService,
              private legalentitiesservice: LegalEntityService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private router:Router) {
    super(translationService, messageService, loadingService);
    //initial valuie for empty orgs
    this.organization = new Organization();
    this.organization.pk = null;
    this.organization.is_serioplast_group = false;
    this.organization.is_supplier = false;
    this.organization.is_customer = false;

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.organizationsservice.getOrganizations({id: params['id'], is_active: true}).subscribe((organization) => {
          this.organization = organization['results'][0];
          // this.customersTable.reload();
          // this.suppliersTable.reload();
          // this.legalentitiesTable.reload();
        })
      }
    });
    this.initTableActionsCustomer();
    this.initTableActionsSupplier();
  }

  initTableActionsCustomer() {
    let actionDeleteOrganization: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteOrganization.click = this.clearOrganizationCustomer.bind(this);
    this.availableActionsCust = [actionDeleteOrganization];
  }

  initTableActionsSupplier() {
    let actionDeleteOrganization: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteOrganization.click = this.clearOrganizationSupplier.bind(this);
    this.availableActionsSupp = [actionDeleteOrganization];
  }

  clearOrganizationCustomer(customer: Customer) {
    customer.organization_ref = null;
    this.customerservice.saveCustomer(customer).subscribe(() => {
      this.customersTable.reload();
    });
  }

  clearOrganizationSupplier(supplier: Supplier) {
    supplier.organization_ref = null;
    this.supplierservice.updateSupplier(supplier).subscribe(() => {
      this.suppliersTable.reload();
    });
  }

  assignCustomer(obj) {
    if (obj) {
      this.customerForm = obj;
      this.customerForm.organization_ref = this.organization;
    } else {
      this.customerForm = null;
    }
  }

  addCustomer(table: TableWithPagesComponent) {
    this.customerservice.saveCustomer(this.customerForm).subscribe(() => {
      table.reload();
      this.customerForm = null;
      this.CustomerFilterAC.reset();
    });

  }

  assignSupplier(obj) {
    if (obj) {
      this.supplierForm = obj;
      this.supplierForm.organization_ref = this.organization;
    } else {
      this.supplierForm = null;
    }
  }

  addSupplier() {
    this.supplierservice.updateSupplier(this.supplierForm).subscribe(() => {
      this.suppliersTable.reload();
      this.supplierForm = null;
      this.SupplierFilterAC.reset();
    });
  }

  createOrganization() {
    this.organizationsservice.updateOrganizations(this.organization).subscribe((risp) => {
      this.organization = risp;
      this.manageRespSave(risp, () => {
        this.router.navigate(["/organizations","detail",risp.pk]);
      });
    });
  }

  loadCustomersFilter(keyword: string) {
    let query = {};
    query['is_active'] = true;
    query['results'] = 5;
    if (keyword) {
      query['search'] = keyword;
    }
    query['exclude_organization'] = this.organization.pk;
    return this.customerservice.getCustomers(query);
  }

  loadSuppliersFilter(keyword: string) {
    let query = {};
    query['is_active'] = true;
    query['results'] = 5;
    if (keyword) {
      query['search'] = keyword;
    }
    query['exclude_organization'] = this.organization.pk;
    return this.supplierservice.getSuppliers(query);

  }

  loadCustomers(page:number,results:number) {
    let query = {};
    query['is_active'] = true;
    query['organization_ref'] = this.organization.pk;
    query['page'] = page;
    query['results'] = results;
    return this.customerservice.getCustomers(query);
  }

  loadSuppliers(page:number,results:number) {
    let query = {};
    query['is_active'] = true;
    query['organization_ref'] = this.organization.pk;
    query['page'] = page;
    query['results'] = results;
    return this.supplierservice.getSuppliers(query)
  }

  loadLegalEntities(page:number,results:number) {
    let query = {};
    query['is_active'] = true;
    query['organization_ref'] = this.organization.pk;
    query['page'] = page;
    query['results'] = results;
    return this.legalentitiesservice.getLegalEntities(query);
  }
}
