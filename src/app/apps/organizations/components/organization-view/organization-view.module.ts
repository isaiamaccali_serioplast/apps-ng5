import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {OrganizationViewComponent} from "./organization-view.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [OrganizationViewComponent],   // components and directives
  exports:[OrganizationViewComponent],
  providers: []                    // services
})
export class OrganizationViewModule {}
