import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {OrganizationsListComponent} from "./organizations-list.component";
import {OrganizationService} from "../../../_common/services/organization.service";
import {HRProfileService} from "../../../_common/services/hr-profiles.service";





@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [OrganizationsListComponent],   // components and directives
  exports:[OrganizationsListComponent],
  providers: [HRProfileService,OrganizationService]                    // services
})
export class OrganizationsListModule { }


