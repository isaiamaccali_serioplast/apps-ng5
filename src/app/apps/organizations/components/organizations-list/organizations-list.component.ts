import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {Router} from "@angular/router";
import {HeaderConf, TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {User} from "../../../_common/services/users.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'organizations-list',
  templateUrl: 'organizations-list.component.html',
  styleUrls: ['organizations-list.component.css']
})

export class OrganizationsListComponent extends BaseComponent implements OnInit {
  organizations: Organization[];
  organizationFilter: Organization;
  organizationsForm: Organization;
  nameTimeout: any;
  active = true;
  users: User[] = [];
  availableActions = [];
  @ViewChild('organizationsTable') organizationsTable: TableWithPagesComponent;
  noItemsMessageOrganizations = this.tr("No organizations.");
  headerConfOrganizations: HeaderConf[] = [
    {
      label: this.tr("Name"),
      valueKey: 'name'
    },
    {
      label: this.tr("Code"),
      valueKey: 'code'
    },
    {
      label: this.tr("Is Serioplast Group"),
      valueKey: 'is_serioplast_group',
      isBoolean: true
    },
    {
      label: this.tr("Is Customer"),
      valueKey: 'is_customer',
      isBoolean: true
    },
    {
      label: this.tr("Is Supplier"),
      valueKey: 'is_supplier',
      isBoolean: true
    },
  ];

  constructor(private router: Router,
              private organizationsservice: OrganizationService,
              protected translationService: TranslationService,
              protected messageService: MessageService,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.organizationFilter = new Organization();
    this.organizationsForm = new Organization();
    this.organizationsForm.is_customer = false;
    this.organizationsForm.is_serioplast_group = false;
    this.organizationsForm.is_supplier = false;
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions() {
    let actionEditOrganization: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditOrganization.click = this.viewOrganization.bind(this);
    let actionDeleteOrganization: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteOrganization.click = this.deleteOrganization.bind(this);
    actionDeleteOrganization.visibilityCondition = (item: Organization) => {
      return item.is_active;
    };
    let actionRestoreOrganization: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreOrganization.click = this.restoreOrganization.bind(this);
    actionRestoreOrganization.visibilityCondition = (item: Organization) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditOrganization, actionRestoreOrganization, actionDeleteOrganization];
  }

  viewOrganization(o: Organization) {
    this.router.navigate(["organizations", "detail", o.pk]);
  }

  loadOrganizations(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.organizationFilter.name)
      query['name'] = this.organizationFilter.name;

    if (this.organizationFilter.is_serioplast_group)
      query['is_serioplast_group'] = this.organizationFilter.is_serioplast_group;

    if (this.organizationFilter.is_supplier)
      query['is_supplier'] = this.organizationFilter.is_supplier;

    if (this.organizationFilter.is_customer)
      query['is_customer'] = this.organizationFilter.is_customer;

    return this.organizationsservice.getOrganizations(query);
  }

  reload() {
    this.organizationsTable.reload();
  }

  reset() {
    setTimeout(()=>{
      this.reload()
    },500);
  }

  createOrganization() {
    this.router.navigate(["organizations", "create"]);
  }

  deleteOrganization(o: Organization) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.organizationsservice.deleteOrganization(o).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.organizationsTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreOrganization(og: Organization) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.organizationsservice.restoreOrganization(og).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.organizationsTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }
}
