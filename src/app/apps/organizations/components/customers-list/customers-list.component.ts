import {Component, OnInit, ViewChild} from "@angular/core";
import {Customer, CustomerService} from "../../../_common/services/customers.service";
import {TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {CustomerType, CustomerTypeService} from "../../../_common/services/customers-types.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'customers-list',
  templateUrl: 'customers-list.component.html',
  styleUrls: ['customers-list.component.css']
})

export class CustomersListComponent extends TranslatableComponent implements OnInit {

  customerFilter: Customer;
  customersTypes: CustomerType[] = [];
  isOrphan: boolean;
  active: boolean = true;
  nameTimeout: any;
  availableActions = [];
  @ViewChild('customersTable') customersTable: TableWithPagesComponent;
  noItemsMessageCustomers = this.tr("No customers.");
  headerConfCustomers = [
    {
      label: this.tr("SAP Code"),
      valueKey: 'sap_code'
    },
    {
      label: this.tr("SAP Description"),
      valueKey: 'sap_description'
    },
    {
      label: this.tr("Type"),
      valueKey: 'type'
    },
    {
      label: this.tr("Organization"),
      valueKey: 'organization_ref',
      useStr: true
    },
  ];

  constructor(private customerservice: CustomerService, private translationservice: TranslationService, private customerstypeservice: CustomerTypeService) {
    super(translationservice);
    this.customerFilter = new Customer();
    this.isOrphan = false;
  }

  ngOnInit() {
    this.loadCustomersTypes();
  }

  loadCustomersTypes() {
    this.customerstypeservice.getCustomersTypes({is_active:true}).subscribe((data)=> {

      for (let i = 0; i < data.length; i++) {
        let customerType = new CustomerType();
        customerType.label = data[i][0];
        customerType.value = data[i][1];
        this.customersTypes[i] = customerType;
      }

      // Remove empty label
      this.customersTypes.shift();

    });
  }

  loadCustomers(page: number, results: number) {
    let query = {};

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.customerFilter.type)
      query['type'] = this.customerFilter.type;

    if (this.isOrphan) {
      query['is_orphan'] = 2;
    }

    if (this.customerFilter.organization_ref)
      query['organization_ref'] = this.customerFilter.organization_ref.pk;

    if (this.customerFilter.sap_code)
      query['sap_code'] = this.customerFilter.sap_code;

    if (this.customerFilter.sap_description)
      query['sap_description'] = this.customerFilter.sap_description;

    return this.customerservice.getCustomers(query);
  }

  /**
   * Filter methods
   */

  reload() {
    this.customersTable.reload();
  }

  reset(){
    setTimeout(()=>{
      this.reload();
    },500)
  }
}
