import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {CustomersListComponent} from "./customers-list.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [CustomersListComponent],   // components and directives
  exports:[CustomersListComponent],
  providers: []                    // services
})
export class CustomersListModule { }


