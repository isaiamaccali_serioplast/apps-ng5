import {Component} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";

@Component({
  selector: 'organizations-index',
  templateUrl: 'organizations-index.component.html',
  styleUrls: ['organizations-index.component.css']
})

export class OrganizationsIndexComponent {


  constructor(private titleService:Title, private sideNavService:SideNavService) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.titleService.setTitle("Serioplast - Organizations");
  }
}
