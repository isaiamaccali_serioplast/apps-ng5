import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {OrganizationsIndexComponent} from "./organizations-index.component";
import {OrganizationsListModule} from "../organizations-list/organizations-list.module";
import {SuppliersListModule} from "../suppliers-list/suppliers-list.module";
import {CustomersListModule} from "../customers-list/customers-list.module";



@NgModule({
  imports: [SharedModule,OrganizationsListModule,SuppliersListModule,CustomersListModule],       // module dependencies
  declarations: [OrganizationsIndexComponent],   // components and directives
  exports:[OrganizationsIndexComponent],
  providers: []                    // services
})
export class OrganizationsIndexModule { }


