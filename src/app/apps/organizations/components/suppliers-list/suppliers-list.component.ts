import {Component, OnInit, ViewChild} from "@angular/core";
import {Supplier, SupplierService} from "../../../_common/services/suppliers.service";
import {TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {SupplierType, SupplierTypeService} from "../../../_common/services/suppliers-types.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";


@Component({
  selector: 'suppliers-list',
  templateUrl: 'suppliers-list.component.html',
  styleUrls: ['suppliers-list.component.css']
})

export class SuppliersListComponent extends TranslatableComponent implements OnInit {
  supplierFilter: Supplier;
  suppliersTypes: SupplierType[] = [];
  isOrphan: boolean;
  active: boolean = true;
  nameTimeout: any;
  availableActions = [];
  @ViewChild('suppliersTable') suppliersTable: TableWithPagesComponent;
  noItemsMessageSuppliers = this.tr("No suppliers.");
  headerConfSuppliers = [
    {
      label: this.tr("SAP Code"),
      valueKey: 'sap_code'
    },
    {
      label: this.tr("SAP Description"),
      valueKey: 'sap_description'
    },
    {
      label: this.tr("Type"),
      valueKey: 'type'
    },
    {
      label: this.tr("Organization"),
      valueKey: 'organization_ref',
      useStr: true
    },
  ];

  constructor(private supplierservice: SupplierService, private translationservice: TranslationService, private suppliertypeservice: SupplierTypeService) {
    super(translationservice);
    this.supplierFilter = new Supplier();
    this.isOrphan = false;
  }

  ngOnInit() {
    this.loadSupplierTypes();
  }

  loadSupplierTypes() {
    this.suppliertypeservice.getSuppliersTypes({is_active:true}).subscribe((data)=> {

      for (let i = 0; i < data.length; i++) {
        let supplierType = new SupplierType();
        supplierType.label = data[i][0];
        supplierType.value = data[i][1];
        this.suppliersTypes[i] = supplierType;
      }

      // Remove empty label
      this.suppliersTypes.shift();

    });
  }

  loadSuppliers(page: number, results: number) {
    let query = {};

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;
    if (this.supplierFilter.type)
      query['type'] = this.supplierFilter.type;

    if (this.isOrphan) {
      // 2 = True on Django RES
      // 3 = False
      query['is_orphan'] = 2;
    }

    if (this.supplierFilter.organization_ref) {
      query['organization_ref'] = this.supplierFilter.organization_ref.pk;
    }


    if (this.supplierFilter.sap_code)
      query['sap_code'] = this.supplierFilter.sap_code;

    if (this.supplierFilter.sap_description)
      query['sap_description'] = this.supplierFilter.sap_description;

    return this.supplierservice.getSuppliers(query);
  }

  reload(){
    this.suppliersTable.reload();
  }

  reset(){
    setTimeout(()=>{
      this.reload();
    },500)
  }
}
