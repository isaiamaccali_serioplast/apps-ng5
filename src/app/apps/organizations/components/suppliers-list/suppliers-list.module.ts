import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SuppliersListComponent} from "./suppliers-list.component";
import {SupplierService} from "../../../_common/services/suppliers.service";
import {SupplierTypeService} from "../../../_common/services/suppliers-types.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SuppliersListComponent],   // components and directives
  exports:[SuppliersListComponent],
  providers: [SupplierService,SupplierTypeService]                    // services
})
export class SuppliersListModule { }


