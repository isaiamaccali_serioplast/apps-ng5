import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {RDDashboardRoutes} from "./rd-dashboard.routes";
import {RDDashboardIndexModule} from "./components/rd-dashboard-index/rd-dashboard-index.module";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(RDDashboardRoutes), RDDashboardIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class RDDashboardModule { }

