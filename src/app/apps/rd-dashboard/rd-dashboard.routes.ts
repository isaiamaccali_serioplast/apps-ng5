import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {RDDashboardIndexComponent} from "./components/rd-dashboard-index/rd-dashboard-index.component";

// Route Configuration
export const RDDashboardRoutes: Routes = [
  {
    path: '',
    component: RDDashboardIndexComponent,
    canActivate: [AuthGuard]
  }
];
