import {Component, OnInit, ViewChild} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {PropertyName, PropertyNamesService} from "../../../_common/services/property-names.service";
import {cloneObject, buildErrorMessage, parseSerionetRespToData} from "../../../../shared/utils";
import {MaterialType, MaterialTypesService} from "../../../_common/services/material-type.service";
import {
  MaterialSubTypeLink,
  MaterialSubtypesLinkService
} from "../../../_common/services/material-subtype-link.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MainMaterialLink, MainMaterialLinksService} from "../../../_common/services/main-material-links.service";
import {MatSnackBar} from "@angular/material";
import {PropertyAttribute, PropertyAttributeService} from "../../../_common/services/property-attribute.service";
import {
  TableRowActions,
  TableWithPagesComponent, TableRowAction
} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {DialogConfirmComponent} from "../../../../shared/components/dialog-confirm/dialog-confirm.component";
import {MaterialSubType} from "../../../_common/services/material-subtype.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-rd-dashboard-index',
  templateUrl: 'rd-dashboard-index.component.html',
  styleUrls: ['rd-dashboard-index.component.css']
})
export class RDDashboardIndexComponent extends BaseComponent implements OnInit {

  propertyName: PropertyName = new PropertyName();
  attribute: PropertyAttribute = new PropertyAttribute();
  materialTypes: MaterialType[] = [];
  materialTypesProducts: MaterialType[] = [];
  materialTypesMainMaterial: MaterialType[] = [];
  materialSubtype: MaterialSubType = new MaterialSubType();
  materialSubtypeLink: MaterialSubTypeLink = new MaterialSubTypeLink();
  mainMaterialLink: MainMaterialLink = new MainMaterialLink();
  propertyNamesCreateOpen = false;
  attributeCreateOpen = false;
  subtypeCreateOpen = false;
  subtypeLinkCreateOpen = false;
  mainMaterialLinkCreateOpen = false;

  propertyNamesActive = true;
  attributesActive = true;
  subtypesActive = true;
  subtypeLinksActive = true;
  mainMaterialLinksActive = true;

  @ViewChild('propertyNamesTable') propertyNamesTable: TableWithPagesComponent;
  noItemsMessagePropertyNames = this.tr("NO_PROPERTY_NAMES_AVAILABLE");
  headerConfPropertyNames = [
    {
      label: this.tr("NAME"),
      valueKey: 'name'
    }
  ];
  availableActionsPropertyNames = [];

  @ViewChild('attributesTable') attributesTable: TableWithPagesComponent;
  noItemsMessageAttributes = this.tr("NO_ATTRIBUTES_AVAILABLE");
  headerConfAttributes = [
    {
      label: this.tr("NAME"),
      valueKey: 'label'
    }
  ];
  availableActionsAttributes = [];

  @ViewChild('subtypeLinkSubtypeDialog') subtypeLinkSubtypeDialog: DialogComponent;
  @ViewChild('mainMaterialLinkSubtypeDialog') mainMaterialLinkSubtypeDialog: DialogComponent;
  @ViewChild('positionDialog') positionDialog: DialogConfirmComponent;

  selectedIndex: number = 0;

  positionToMoveTo = null;

  constructor(private sideNavService: SideNavService, private titleService: Title, private attributesService: PropertyAttributeService,
              private propertyNamesService: PropertyNamesService, private materialTypesService: MaterialTypesService,
              private materialSubtypeLinksService: MaterialSubtypesLinkService, protected translationService: TranslationService,
              protected messageService: MessageService, protected loadingService: LoadingService, private mainMaterialLinkService: MainMaterialLinksService,
              private snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - R&D Dashboard");
  }

  ngOnInit() {
    this.initTableActions();
    this.createAttribute();
    this.loadMaterialTypes();
  }

  initTableActions() {
    let actionEditPN: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditPN.click = this.editPropertyName.bind(this);
    let actionDeletePN: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeletePN.click = this.deletePropertyName.bind(this);
    actionDeletePN.visibilityCondition = (item: PropertyName) => {
      return item.is_active;
    };
    let actionRestorePN: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestorePN.click = this.restorePropertyName.bind(this);
    actionRestorePN.visibilityCondition = (item: PropertyName) => {
      return !item.is_active;
    };
    let actionMoveUpPN: TableRowAction = cloneObject(TableRowActions.MOVEUP);
    actionMoveUpPN.click = this.moveUpPropertyName.bind(this);
    actionMoveUpPN.visibilityCondition = (item: PropertyName, isFirst: boolean, isLast: boolean) => {
      return !isFirst && item.is_active
    };
    let actionMoveDownPN: TableRowAction = cloneObject(TableRowActions.MOVEDOWN);
    actionMoveDownPN.click = this.moveDownPropertyName.bind(this);
    actionMoveDownPN.visibilityCondition = (item: PropertyName, isFirst: boolean, isLast: boolean) => {
      return !isLast && item.is_active
    };
    let actionMoveToPN: TableRowAction = cloneObject(TableRowActions.MOVETO);
    actionMoveToPN.visibilityCondition = (item: PropertyName, isFirst: boolean, isLast: boolean) => {
      return item.is_active
    };
    actionMoveToPN.click = this.moveToPropertyName.bind(this);

    this.availableActionsPropertyNames = [actionMoveUpPN, actionMoveDownPN, actionMoveToPN, actionEditPN, actionDeletePN, actionRestorePN];

    let actionEditAT: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditAT.click = this.editAttribute.bind(this);
    let actionDeleteAT: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteAT.click = this.deleteAttribute.bind(this);
    actionDeleteAT.visibilityCondition = (item: PropertyAttribute) => {
      return item.is_active;
    };
    let actionRestoreAT: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreAT.click = this.restoreAttribute.bind(this);
    actionRestoreAT.visibilityCondition = (item: PropertyAttribute) => {
      return !item.is_active;
    };
    let actionMoveUpAT: TableRowAction = cloneObject(TableRowActions.MOVEUP);
    actionMoveUpAT.click = this.moveUpAttribute.bind(this);
    actionMoveUpAT.visibilityCondition = (item: PropertyAttribute, isFirst: boolean, isLast: boolean) => {
      return !isFirst && item.is_active
    };
    let actionMoveDownAT: TableRowAction = cloneObject(TableRowActions.MOVEDOWN);
    actionMoveDownAT.click = this.moveDownAttribute.bind(this);
    actionMoveDownAT.visibilityCondition = (item: PropertyAttribute, isFirst: boolean, isLast: boolean) => {
      return !isLast && item.is_active
    };
    let actionMoveToAT: TableRowAction = cloneObject(TableRowActions.MOVETO);
    actionMoveToAT.click = this.moveToAttribute.bind(this);
    actionMoveToAT.visibilityCondition = (item: PropertyAttribute, isFirst: boolean, isLast: boolean) => {
      return item.is_active
    };
    this.availableActionsAttributes = [actionMoveUpAT, actionMoveDownAT, actionMoveToAT, actionEditAT, actionDeleteAT, actionRestoreAT];
  }

  loadPropertyNames(page: number, results: number) {
    let query = {};
    query['is_active'] = this.propertyNamesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    return this.propertyNamesService.getPropertyNames(query);
  }

  loadAttributes(page: number, results: number) {
    let query = {};
    query['is_active'] = this.attributesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    return this.attributesService.getAttributes(query);
  }

  loadMaterialTypes() {
    //this.loadingService.show();
    this.materialTypesService.getMaterialTypes({is_active: true}).subscribe((data) => {
      this.materialTypes = parseSerionetRespToData(data);
      this.materialTypesProducts = this.materialTypes.filter((mt) => mt.first_char == 'P' || mt.first_char == 'Q');
    });
  }

  createPropertyName() {
    this.propertyName = new PropertyName();
  }

  createAttribute() {
    this.attribute = new PropertyAttribute();
  }

  editPropertyName(pn: PropertyName) {
    this.propertyName = cloneObject(pn);
    this.propertyNamesCreateOpen = true;
  }

  undoEditPropertyName() {
    this.createPropertyName();
    this.propertyNamesCreateOpen = false;
  }

  editAttribute(a: PropertyAttribute) {
    this.attribute = cloneObject(a);
    this.attributeCreateOpen = true;
  }

  undoEditAttribute() {
    this.createAttribute();
    this.attributeCreateOpen = false;
  }

  deletePropertyName(pn: PropertyName) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: ()=> {
        this.propertyNamesService.deletePropertyName(pn).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.propertyNamesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  deleteAttribute(a: PropertyAttribute) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.attributesService.deleteAttribute(a).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.attributesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  savePropertyName() {
    let displayMsg = false;
    if (this.propertyName.is_active == false) {
      displayMsg = true;
    }
    this.propertyNamesService.savePropertyName(this.propertyName).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createPropertyName();
        this.propertyNamesCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  saveAttribute() {
    let displayMsg = false;
    if (this.attribute.is_active == false) {
      displayMsg = true;
    }
    this.attributesService.saveAttribute(this.attribute).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createAttribute();
        this.attributeCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  undoCreateMainMaterialLink() {
    this.mainMaterialLinkCreateOpen = false;
    this.mainMaterialLink = new MainMaterialLink();
  }

  undoCreateSubtypeLink() {
    this.subtypeLinkCreateOpen = false;
    this.materialSubtypeLink = new MaterialSubTypeLink();
  }

  createMainMaterialLink(ml: MainMaterialLink) {
    this.mainMaterialLink = ml;
    this.mainMaterialLinkCreateOpen = true;
  }

  saveSubtypeLink() {
    let displayMsg = false;
    if (this.materialSubtypeLink.is_active == false) {
      displayMsg = true;
    }
    this.materialSubtypeLinksService.saveMaterialSubtypeLink(this.materialSubtypeLink).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.subtypeLinkCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    });
  }

  saveMainMaterialLink() {
    let displayMsg = false;
    if (this.mainMaterialLink.is_active == false) {
      displayMsg = true;
    }
    this.mainMaterialLinkService.saveMainMaterialLink(this.mainMaterialLink).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.mainMaterialLinkCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    });
  }

  editSubtype(s: MaterialSubType) {
    this.materialSubtype = s;
    this.subtypeCreateOpen = true;
  }

  undoEditSubtype() {
    this.createSubtype();
    this.subtypeCreateOpen = false;
  }

  editSubtypeLink(sl: MaterialSubTypeLink) {
    this.materialSubtypeLink = sl;
    this.subtypeLinkCreateOpen = true;
  }

  editMainMaterialLink(sl: MainMaterialLink) {
    this.mainMaterialLink = sl;
    this.mainMaterialLinkCreateOpen = true;
  }

  restorePropertyName(pn: PropertyName) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.propertyNamesService.restorePropertyName(pn).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.propertyNamesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  restoreAttribute(al: PropertyAttribute) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.attributesService.restoreAttribute(al).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.attributesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  createSubtype() {
    this.materialSubtype = new MaterialSubType();
  }

  selectedSubtypeSubtypeLink(ms: MaterialSubType) {
    let s = this.subtypeLinkSubtypeDialog.onClose.subscribe(() => {
      if (ms) {
        this.materialSubtypeLink.subtype_id = cloneObject(ms);
      }
      else {
        this.materialSubtypeLink.subtype_id = null;
      }
      s.unsubscribe();
    });
    this.subtypeLinkSubtypeDialog.close();
  }

  selectedSubtypeMainMaterialLink(ms: MainMaterialLink) {
    let s = this.mainMaterialLinkSubtypeDialog.onClose.subscribe(() => {
      if (ms) {
        this.mainMaterialLink.subtype_id = cloneObject(ms);
      }
      else {
        this.mainMaterialLink.subtype_id = null;
      }
      s.unsubscribe();
    });
    this.mainMaterialLinkSubtypeDialog.close();
  }

  editMaterialSubtypeLink(msl: MaterialSubTypeLink) {
    this.subtypeLinkCreateOpen = true;
    this.materialSubtypeLink = msl;
  }

  createMaterialSubtypeLink(msl: MaterialSubTypeLink) {
    this.subtypeLinkCreateOpen = true;
    this.materialSubtypeLink = msl;
  }

  moveUpPropertyName(pn: PropertyName) {
    this.propertyNamesService.moveUpItem(pn).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.propertyNamesTable.reload();
      })
    })
  }

  moveDownPropertyName(pn: PropertyName) {
    this.propertyNamesService.moveDownItem(pn).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.propertyNamesTable.reload();
      })
    })
  }

  moveUpAttribute(a: PropertyAttribute) {
    this.attributesService.moveUpItem(a).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.attributesTable.reload();
      })
    })
  }

  moveDownAttribute(a: PropertyAttribute) {
    this.attributesService.moveDownItem(a).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.attributesTable.reload();
      })
    })
  }

  moveToPropertyName(pn: PropertyName) {
    this.positionToMoveTo = pn.delta;
    this.positionDialog.confirmFunction = () => {
      if (this.positionToMoveTo != pn.delta) {
        this.loadingService.show();
        this.propertyNamesService.moveTo(pn, parseInt(this.positionToMoveTo, 10)).subscribe((resp) => {
          this.loadingService.hide();
          this.manageRespSave(resp, () => {
            this.propertyNamesTable.reload();
          })
        })
      }
    };
    this.positionDialog.open();
  }

  moveToAttribute(pa: PropertyAttribute) {
    this.positionToMoveTo = pa.delta;
    this.positionDialog.confirmFunction = () => {
      if (this.positionToMoveTo != pa.delta) {
        this.loadingService.show();
        this.attributesService.moveTo(pa, parseInt(this.positionToMoveTo, 10)).subscribe((resp) => {
          this.loadingService.hide();
          this.manageRespSave(resp, () => {
            this.attributesTable.reload();
          })
        })
      }
    };
    this.positionDialog.open();
  }

  handleTabChange(event) {
    this.selectedIndex = event.index;
  }
}
