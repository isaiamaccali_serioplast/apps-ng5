import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {camelCase, cloneObject, parseSerionetRespToData} from "../../../../../shared/utils";
import {Observable} from "rxjs";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSubTypeLink, MaterialSubtypesLinkService} from "../../../../_common/services/material-subtype-link.service";
import {MatSnackBar} from "@angular/material";
import {MaterialSubType, MaterialSubtypesService} from "../../../../_common/services/material-subtype.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {MaterialType, MaterialTypesService} from "../../../../_common/services/material-type.service";
import {MainMaterialLinksService} from "../../../../_common/services/main-material-links.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-rd-dashboard-subtypes',
  templateUrl: 'rd-dashboard-subtypes.component.html',
  styleUrls: ['rd-dashboard-subtypes.component.css']
})
export class RDDashboardSubtypesComponent extends BaseComponent implements OnInit {

  materialTypes: MaterialType[] = [];
  materialTypesProducts: MaterialType[] = [];

  @Input() complete = true;

  subtypesActive = true;
  subtypeCreateOpen = false;
  materialSubtype: MaterialSubType;

  @ViewChild('subtypesTable') subtypesTable: TableWithPagesComponent;
  noItemsMessageSubtypes = this.tr("NO_SUBTYPES_AVAILABLE");
  headerConfSubtypes = [
    {
      label: this.tr("NAME"),
      valueKey: 'label'
    }
  ];
  availableActionsSubtypes = [];

  @Output() subtypeSelected = new EventEmitter();

  materialTypeSubtypeLink: number;
  materialTypeMainMaterial: number;

  @Input() typeToFilter: number;
  @Input() typeToFilterCategory: string;

  constructor(private materialSubtypesService: MaterialSubtypesService, private materialSubtypeLinksService: MaterialSubtypesLinkService,
              protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private snackBar: MatSnackBar, private materialTypesService: MaterialTypesService,
              private mainMaterialLinkService: MainMaterialLinksService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    if (this.complete) {
      this.headerConfSubtypes.push({
          label: this.tr("MATERIAL_TYPES"),
          valueKey: 'material_types'
        },
        {
          label: this.tr("MAIN_MATERIALS"),
          valueKey: 'main_materials'
        });
    }

    this.initTableActions();
    this.materialTypesService.getMaterialTypes({is_active: true}).subscribe((data) => {
      this.materialTypes = parseSerionetRespToData(data);
      this.materialTypesProducts = this.materialTypes.filter((mt) => mt.first_char == 'P');
    })
  }

  initTableActions() {
    let actionEditST: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditST.click = this.editSubtype.bind(this);
    let actionDeleteST: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteST.click = this.deleteSubtype.bind(this);
    actionDeleteST.visibilityCondition = (item: MaterialSubType) => {
      return item.is_active;
    };
    let actionRestoreST: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreST.click = this.restoreSubtype.bind(this);
    actionRestoreST.visibilityCondition = (item: MaterialSubType) => {
      return !item.is_active;
    };
    if (this.complete)
      this.availableActionsSubtypes = [actionEditST, actionDeleteST, actionRestoreST];
  }

  saveSubtype() {
    let displayMsg = false;
    if (this.materialSubtype.is_active == false) {
      displayMsg = true;
    }
    this.materialSubtypesService.saveMaterialSubtype(this.materialSubtype).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.subtypeCreateOpen = false;
        if (!this.complete)
          this.selectSubtypeFromClick(resp);
        this.createSubtype();
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  editSubtype(s: MaterialSubType) {
    this.materialSubtype = s;
    this.subtypeCreateOpen = true;
  }

  undoEditSubtype() {
    this.createSubtype();
    this.subtypeCreateOpen = false;
  }

  deleteSubtype(s: MaterialSubType) {

    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.materialSubtypesService.deleteMaterialSubtype(s).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.subtypesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreSubtype(s: MaterialSubType) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.materialSubtypesService.restoreMaterialSubtype(s).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.subtypesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  expandDataSubTypes(subtypes: any) {
    return Observable.create((observer) => {

      let expandST = (sts, index, callback) => {
        if (sts[index]) {
          let st = sts[index];
          let query = {is_active: true};
          query['subtype_id__label'] = st.label;
          this.materialSubtypeLinksService.getMaterialSubtypeLinks(query).subscribe((data) => {
            let materialTypes = [];
            if (data && data['count'] && data['count'] > 0) {
              materialTypes = data['results'].filter((s: MaterialSubTypeLink) => {
                return s.fieldname == 'subtype';
              });
            }
            let mainMaterials = [];
            this.mainMaterialLinkService.getMainMaterialLinks(query).subscribe((data) => {
              if (data && data['count'] && data['count'] > 0) {
                mainMaterials = data['results'].filter((s: MaterialSubTypeLink) => {
                  return s.fieldname == 'main_material';
                });
              }

              let material_types_list = "-";
              let main_material_list = "-";
              if (materialTypes.length > 0) {
                let matTypes = materialTypes.map((s: MaterialSubTypeLink) => {
                  return camelCase(s.app.__str__);
                });
                let mat = [];
                mat.push('<ul>');
                matTypes.forEach((m) => {
                  mat.push('<li>' + m + '</li>');
                });
                mat.push('</ul>');
                material_types_list = mat.join("");
              }
              if (mainMaterials.length > 0) {
                let matTypes = mainMaterials.map((s: MaterialSubTypeLink) => {
                  return camelCase(s.app.__str__);
                });
                let mat = [];
                mat.push('<ul>');
                matTypes.forEach((m) => {
                  mat.push('<li>' + m + '</li>');
                });
                mat.push('</ul>');
                main_material_list = mat.join("");
              }
              st['material_types'] = material_types_list;
              st['main_materials'] = main_material_list;
              subtypes[index] = st;
              expandST(sts, index + 1, callback);
            })
          })
        }
        else {
          callback();
        }
      };
      expandST(subtypes, 0, () => {
        observer.next(subtypes);
        observer.complete();
      })
    });
  }

  createSubtype() {
    this.materialSubtype = new MaterialSubType();
  }

  loadMaterialSubtypes(page: number, results: number) {

    let query = {};
    query['is_active'] = this.subtypesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    if (this.materialTypeMainMaterial)
      query['app_fieldname_main_material'] = this.materialTypeMainMaterial;
    if (this.materialTypeSubtypeLink)
      query['app_fieldname_subtype'] = this.materialTypeSubtypeLink;

    if (this.typeToFilter) {
      if (this.typeToFilterCategory == 'subtype') {
        query['not_app_fieldname_subtype'] = this.typeToFilter;
      }
      else if (this.typeToFilterCategory == 'main_material') {
        query['not_app_fieldname_main_material'] = this.typeToFilter;
      }
    }

    return this.materialSubtypesService.getMaterialSubtypes(query);
  }

  selectSubtypeFromClick(ms: MaterialSubType) {
    this.subtypeSelected.emit(ms);
  }

  handleMaterialTypeChangeForm() {
    this.subtypesTable.reload();
  }
}
