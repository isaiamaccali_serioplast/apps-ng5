"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var property_name_1 = require("../../../_common/models/property-name");
var utils_1 = require("../../../../shared/utils");
var material_subtype_1 = require("../../../_common/models/material-subtype");
var material_subtype_link_1 = require("../../../_common/models/material-subtype-link");
var serionet_app_1 = require("../../../../shared/serionet/models/serionet-app");
var main_material_link_1 = require("../../../_common/models/main-material-link");
var property_attribute_1 = require("../../../_common/models/property-attribute");
var RDDashboardIndexComponent = (function () {
    function RDDashboardIndexComponent(sideNavService, titleService, attributesService, propertyNamesService, materialTypesService, materialSubtypesService, materialSubtypeLinksService, translationService, messageService, loadingService, mainMaterialLinkService, snackBar) {
        this.sideNavService = sideNavService;
        this.titleService = titleService;
        this.attributesService = attributesService;
        this.propertyNamesService = propertyNamesService;
        this.materialTypesService = materialTypesService;
        this.materialSubtypesService = materialSubtypesService;
        this.materialSubtypeLinksService = materialSubtypeLinksService;
        this.translationService = translationService;
        this.messageService = messageService;
        this.loadingService = loadingService;
        this.mainMaterialLinkService = mainMaterialLinkService;
        this.snackBar = snackBar;
        this.attribute = new property_attribute_1.PropertyAttribute();
        this.materialTypesMainMaterial = [];
        this.materialSubtype = new material_subtype_1.MaterialSubType();
        this.materialSubtypeLink = new material_subtype_link_1.MaterialSubTypeLink();
        this.mainMaterialLink = new main_material_link_1.MainMaterialLink();
        this.mainMaterialLinks = [];
        this.propertyNamesCreateOpen = false;
        this.attributeCreateOpen = false;
        this.subtypeLinkCreateOpen = false;
        this.propertyNamesActive = true;
        this.attributesActive = true;
        this.subtypesActive = true;
        this.subtypeLinksActive = true;
        this.mainMaterialLinksActive = true;
        this.resultsPerPage = 10;
        this.sideNavService.leftMenuEnabled = false;
        this.sideNavService.leftMenu = false;
        this.sideNavService.appMenuEnabled = true;
        this.titleService.setTitle("Serioplast - R&D Dashboard");
    }
    RDDashboardIndexComponent.prototype.ngOnInit = function () {
        this.propertyNames = [];
        this.attributes = [];
        this.materialTypes = [];
        this.materialSubTypes = [];
        this.materialSubtypeLinks = [];
        this.propertyName = new property_name_1.PropertyName();
        this.createSubtypeLink();
        this.createAttributeLink();
        this.createMainMaterialLink();
        this.loadPropertyNames();
        this.loadAttributes();
        this.loadMaterialTypes();
        this.loadMaterialTypesMainMaterial();
        this.loadMaterialSubtypes();
    };
    RDDashboardIndexComponent.prototype.loadPropertyNames = function (page) {
        var _this = this;
        var query = {};
        query['is_active'] = this.propertyNamesActive;
        query['results'] = this.resultsPerPage;
        if (!page)
            page = 1;
        query['page'] = page;
        this.loadingService.show();
        this.propertyNamesService.getPropertyNames({ is_active: this.propertyNamesActive }).subscribe(function (data) {
            _this.loadingService.hide();
            if (data && data['count'] && data['count'] > 0) {
                _this.propertyNames = data['results'];
                _this.totalPropertyNames = data['count'];
                _this.currentPagePropertyNames = query['page'];
                _this.totalPagesPropertyNames = Math.ceil(data['count'] / _this.resultsPerPage);
                if (_this.currentPagePropertyNames != _this.totalPagesPropertyNames) {
                    _this.intervalPropertyNames = (_this.resultsPerPage * (page - 1) + 1) + "-" + (_this.resultsPerPage * page);
                }
                else {
                    _this.intervalPropertyNames = (Math.floor(_this.totalPropertyNames / _this.resultsPerPage) * _this.resultsPerPage + 1) + "-" + (_this.totalPropertyNames);
                }
            }
            else {
                _this.propertyNames = [];
            }
        });
    };
    RDDashboardIndexComponent.prototype.loadAttributes = function (page) {
        var _this = this;
        var query = {};
        query['is_active'] = this.attributesActive;
        query['results'] = this.resultsPerPage;
        if (!page)
            page = 1;
        query['page'] = page;
        this.loadingService.show();
        this.attributesService.getAttributes(query).subscribe(function (data) {
            _this.loadingService.hide();
            if (data && data['count'] && data['count'] > 0) {
                _this.attributes = data['results'];
                _this.totalAttribute = data['count'];
                _this.currentPageAttribute = query['page'];
                _this.totalPagesAttribute = Math.ceil(data['count'] / _this.resultsPerPage);
                if (_this.currentPageAttribute != _this.totalPagesAttribute) {
                    _this.intervalAttribute = (_this.resultsPerPage * (page - 1) + 1) + "-" + (_this.resultsPerPage * page);
                }
                else {
                    _this.intervalAttribute = (Math.floor(_this.totalAttribute / _this.resultsPerPage) * _this.resultsPerPage + 1) + "-" + (_this.totalAttribute);
                }
            }
            else {
                _this.totalPagesAttribute = 0;
                _this.attributeLinks = [];
            }
        });
    };
    RDDashboardIndexComponent.prototype.loadMaterialTypes = function () {
        var _this = this;
        this.loadingService.show();
        this.materialTypesService.getMaterialTypes({ is_active: true }).subscribe(function (data) {
            _this.loadingService.hide();
            if (data && data['count'] && data['count'] > 0)
                _this.materialTypes = data['results'];
            else
                _this.materialTypes = [];
        });
    };
    RDDashboardIndexComponent.prototype.loadMaterialTypesMainMaterial = function () {
        var _this = this;
        this.loadingService.show();
        this.materialTypesService.getMaterialTypes({ is_active: true, first_char: 'P' }).subscribe(function (data) {
            _this.loadingService.hide();
            if (data && data['count'] && data['count'] > 0)
                _this.materialTypesMainMaterial = data['results'];
            else
                _this.materialTypesMainMaterial = [];
        });
    };
    RDDashboardIndexComponent.prototype.loadMaterialSubtypes = function (page) {
        var _this = this;
        var query = {};
        query['is_active'] = this.subtypesActive;
        query['results'] = this.resultsPerPage;
        if (!page)
            page = 1;
        query['page'] = page;
        this.loadingService.show();
        this.materialSubtypesService.getMaterialSubtypes(query).subscribe(function (data) {
            _this.loadingService.hide();
            if (data && data['count'] && data['count'] > 0) {
                _this.materialSubTypes = data['results'];
                _this.totalSubtype = data['count'];
                _this.currentPageSubtype = query['page'];
                _this.totalPagesSubtype = Math.ceil(data['count'] / _this.resultsPerPage);
                if (_this.currentPageSubtype != _this.totalPagesSubtype) {
                    _this.intervalSubtype = (_this.resultsPerPage * (page - 1) + 1) + "-" + (_this.resultsPerPage * page);
                }
                else {
                    _this.intervalSubtype = (Math.floor(_this.totalSubtype / _this.resultsPerPage) * _this.resultsPerPage + 1) + "-" + (_this.totalSubtype);
                }
            }
            else {
                _this.materialSubTypes = [];
                _this.totalPagesSubtype = 0;
            }
        });
    };
    RDDashboardIndexComponent.prototype.createPropertyName = function () {
        this.propertyName = new property_name_1.PropertyName();
    };
    RDDashboardIndexComponent.prototype.createAttributeLink = function () {
        this.attributeLink = new property_attribute_1.PropertyAttribute();
        this.attributeCreateOpen = true;
    };
    RDDashboardIndexComponent.prototype.editPropertyName = function (pn) {
        this.propertyName = utils_1.cloneObject(pn);
        this.propertyNamesCreateOpen = true;
    };
    RDDashboardIndexComponent.prototype.editAttributeLink = function (a) {
        this.attributeLink = utils_1.cloneObject(a);
        this.attributeLink.type_id.label = this.attributeLink.__str__;
        this.attributeCreateOpen = true;
    };
    RDDashboardIndexComponent.prototype.deletePropertyName = function (pn) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('CONFIRM_DELETE'),
            autoClose: false,
            confirm: function () {
                self.propertyNamesService.deletePropertyName(pn).subscribe(function (resp) {
                    self.handleDelete(resp, function () {
                        self.loadPropertyNames();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.deleteAttributeLink = function (a) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('CONFIRM_DELETE'),
            autoClose: false,
            confirm: function () {
                self.attributesLinkService.deleteAttributeLink(a).subscribe(function (resp) {
                    self.handleDelete(resp, function () {
                        self.loadAttributes();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.savePropertyName = function () {
        var self = this;
        var displayMsg = false;
        if (this.propertyName.is_active == false) {
            displayMsg = true;
        }
        this.propertyNamesService.savePropertyName(this.propertyName).subscribe(function (resp) {
            self.handleSaveOrUpdate(resp, function () {
                self.propertyName = new property_name_1.PropertyName();
                self.loadPropertyNames();
                if (displayMsg)
                    self.snackBar.open(self.tr("ITEM_RESTORED"), null, { duration: 5000 });
            });
        });
    };
    RDDashboardIndexComponent.prototype.saveAttributeLink = function () {
        var self = this;
        var displayMsg = false;
        if (this.attributeLink.is_active == false) {
            displayMsg = true;
        }
        this.attributesLinkService.saveAttributeLink(this.attributeLink).subscribe(function (resp) {
            self.handleSaveOrUpdate(resp, function () {
                self.createAttributeLink();
                self.loadAttributes();
                if (displayMsg)
                    self.snackBar.open(self.tr("ITEM_RESTORED"), null, { duration: 5000 });
            });
        });
    };
    RDDashboardIndexComponent.prototype.handleMaterialTypeChange = function () {
        var materialType = null;
        for (var _i = 0, _a = this.materialTypes; _i < _a.length; _i++) {
            var mt = _a[_i];
            if (mt.pk == this.materialTypePK) {
                materialType = mt;
                break;
            }
        }
        if (materialType) {
            this.materialTypeSubtypeLinks = materialType;
            this.loadMaterialSubtypeLinks();
        }
    };
    RDDashboardIndexComponent.prototype.loadMaterialSubtypeLinks = function (page) {
        var _this = this;
        if (this.materialTypeSubtypeLinks) {
            var self_1 = this;
            this.materialSubtypeLinks = [];
            this.loadingService.show();
            var query_1 = {};
            query_1['is_active'] = this.subtypeLinksActive;
            query_1['results'] = this.resultsPerPage;
            if (!page)
                page = 1;
            query_1['page'] = page;
            query_1['app'] = this.materialTypeSubtypeLinks.content_type;
            this.materialSubtypeLinksService.getMaterialSubtypeLinks(query_1).subscribe(function (data) {
                self_1.loadingService.hide();
                if (data && data['count'] && data['count'] > 0) {
                    _this.materialSubtypeLinks = data['results'];
                    _this.totalSubtypeLink = data['count'];
                    _this.currentPageSubtypeLink = query_1['page'];
                    _this.totalPagesSubtypeLink = Math.ceil(data['count'] / _this.resultsPerPage);
                    if (_this.currentPageSubtypeLink != _this.totalPagesSubtypeLink) {
                        _this.intervalSubtypeLink = (_this.resultsPerPage * (page - 1) + 1) + "-" + (_this.resultsPerPage * page);
                    }
                    else {
                        _this.intervalSubtypeLink = (Math.floor(_this.totalSubtypeLink / _this.resultsPerPage) * _this.resultsPerPage + 1) + "-" + (_this.totalSubtypeLink);
                    }
                }
                else {
                    _this.totalSubtypeLink = 0;
                    _this.materialSubtypeLinks = [];
                }
            });
        }
    };
    RDDashboardIndexComponent.prototype.handleMaterialTypeChangeMainMaterial = function () {
        var materialType = null;
        for (var _i = 0, _a = this.materialTypes; _i < _a.length; _i++) {
            var mt = _a[_i];
            if (mt.pk == this.materialTypePKMainMaterial) {
                materialType = mt;
                break;
            }
        }
        if (materialType) {
            this.materialTypeMainMaterialLinks = materialType;
            this.loadMainMaterialLinks();
        }
    };
    RDDashboardIndexComponent.prototype.loadMainMaterialLinks = function (page) {
        var _this = this;
        var self = this;
        this.materialSubtypeLinks = [];
        this.loadingService.show();
        var query = {};
        query['is_active'] = this.mainMaterialLinksActive;
        query['results'] = this.resultsPerPage;
        if (!page)
            page = 1;
        query['page'] = page;
        query['app'] = this.materialTypeMainMaterialLinks.content_type;
        this.mainMaterialLinkService.getMainMaterialLinks(query).subscribe(function (data) {
            self.loadingService.hide();
            if (data && data['count'] && data['count'] > 0) {
                _this.mainMaterialLinks = data['results'];
                _this.totalMainMaterialLink = data['count'];
                _this.currentPageMainMaterialLink = query['page'];
                _this.totalPagesMainMaterialLink = Math.ceil(data['count'] / _this.resultsPerPage);
                if (_this.currentPageMainMaterialLink != _this.totalPagesMainMaterialLink) {
                    _this.intervalMainMaterialLink = (_this.resultsPerPage * (page - 1) + 1) + "-" + (_this.resultsPerPage * page);
                }
                else {
                    _this.intervalMainMaterialLink = (Math.floor(_this.totalMainMaterialLink / _this.resultsPerPage) * _this.resultsPerPage + 1) + "-" + (_this.totalMainMaterialLink);
                }
            }
            else {
                _this.mainMaterialLinks = [];
            }
        });
    };
    RDDashboardIndexComponent.prototype.handleSubtypeChangeForm = function () {
        var materialSubType = null;
        for (var _i = 0, _a = this.materialSubTypes; _i < _a.length; _i++) {
            var mst = _a[_i];
            if (mst.pk == this.materialSubtypePK) {
                materialSubType = mst;
                break;
            }
        }
        this.materialSubtypeLink.subtype_id = utils_1.cloneObject(materialSubType);
    };
    RDDashboardIndexComponent.prototype.handleSubtypeChangeFormMainMaterial = function () {
        var materialSubType = null;
        for (var _i = 0, _a = this.materialSubTypes; _i < _a.length; _i++) {
            var mst = _a[_i];
            if (mst.pk == this.materialSubtypeMainMaterialPK) {
                materialSubType = mst;
                break;
            }
        }
        this.mainMaterialLink.subtype_id = utils_1.cloneObject(materialSubType);
    };
    RDDashboardIndexComponent.prototype.handleMaterialTypeChangeForm = function () {
        var materialType = null;
        for (var _i = 0, _a = this.materialTypes; _i < _a.length; _i++) {
            var mt = _a[_i];
            if (mt.pk == this.materialTypePKForm) {
                materialType = mt;
                break;
            }
        }
        var app = new serionet_app_1.SerionetApp();
        app.pk = materialType.content_type;
        this.materialSubtypeLink.app = app;
    };
    RDDashboardIndexComponent.prototype.handleMaterialTypeChangeFormMainMaterial = function () {
        var materialType = null;
        for (var _i = 0, _a = this.materialTypes; _i < _a.length; _i++) {
            var mt = _a[_i];
            if (mt.pk == this.materialTypePKFormMainMaterial) {
                materialType = mt;
                break;
            }
        }
        var app = new serionet_app_1.SerionetApp();
        app.pk = materialType.content_type;
        this.mainMaterialLink.app = app;
    };
    RDDashboardIndexComponent.prototype.undoCreateMainMaterialLink = function () {
        this.subtypeLinkCreateOpen = false;
        this.mainMaterialLink = new main_material_link_1.MainMaterialLink();
        this.mainMaterialLink.subtype_id = new material_subtype_1.MaterialSubType();
        this.materialSubtypeMainMaterialPK = null;
        this.materialTypePKFormMainMaterial = null;
    };
    RDDashboardIndexComponent.prototype.undoCreateSubtypeLink = function () {
        this.subtypeLinkCreateOpen = false;
        this.materialSubtypeLink = new material_subtype_link_1.MaterialSubTypeLink();
    };
    RDDashboardIndexComponent.prototype.createSubtypeLink = function () {
        this.materialSubtypeLink = new material_subtype_link_1.MaterialSubTypeLink();
        this.materialSubtypeLink.subtype_id = new material_subtype_1.MaterialSubType();
        this.materialSubtypeLink.fieldname = "subtype";
        this.subtypeLinkCreateOpen = true;
        this.materialSubtypePK = null;
        this.materialTypePKForm = null;
    };
    RDDashboardIndexComponent.prototype.createMainMaterialLink = function () {
        this.mainMaterialLink = new main_material_link_1.MainMaterialLink();
        this.mainMaterialLink.subtype_id = new material_subtype_1.MaterialSubType();
        this.mainMaterialLink.fieldname = "main_material";
        this.subtypeLinkCreateOpen = true;
        this.materialSubtypeMainMaterialPK = null;
        this.materialTypePKFormMainMaterial = null;
    };
    RDDashboardIndexComponent.prototype.saveSubtype = function () {
        var self = this;
        var displayMsg = false;
        if (this.materialSubtype.is_active == false) {
            displayMsg = true;
        }
        this.materialSubtypesService.saveMaterialSubtype(this.materialSubtype).subscribe(function (resp) {
            self.handleSaveOrUpdate(resp, function () {
                self.loadMaterialSubtypes();
                if (self.materialSubtypeLinks.length > 0) {
                    self.handleMaterialTypeChange();
                }
                self.materialSubtype = new material_subtype_1.MaterialSubType();
                if (displayMsg)
                    self.snackBar.open(self.tr("ITEM_RESTORED"), null, { duration: 5000 });
            });
        });
    };
    RDDashboardIndexComponent.prototype.saveSubtypeLink = function () {
        var self = this;
        var displayMsg = false;
        if (this.materialSubtypeLink.is_active == false) {
            displayMsg = true;
        }
        this.materialSubtypeLinksService.saveMaterialSubtypeLink(this.materialSubtypeLink).subscribe(function (resp) {
            self.handleSaveOrUpdate(resp, function () {
                self.handleMaterialTypeChange();
                self.createSubtypeLink();
                if (displayMsg)
                    self.snackBar.open(self.tr("ITEM_RESTORED"), null, { duration: 5000 });
            });
        });
    };
    RDDashboardIndexComponent.prototype.saveMainMaterialLink = function () {
        var self = this;
        var displayMsg = false;
        if (this.mainMaterialLink.is_active == false) {
            displayMsg = true;
        }
        this.mainMaterialLinkService.saveMainMaterialLink(this.mainMaterialLink).subscribe(function (resp) {
            self.handleSaveOrUpdate(resp, function () {
                self.handleMaterialTypeChangeMainMaterial();
                self.createMainMaterialLink();
                if (displayMsg)
                    self.snackBar.open(self.tr("ITEM_RESTORED"), null, { duration: 5000 });
            });
        });
    };
    RDDashboardIndexComponent.prototype.editSubtype = function (s) {
        this.materialSubtype = s;
    };
    RDDashboardIndexComponent.prototype.deleteSubtype = function (s) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('CONFIRM_DELETE'),
            autoClose: false,
            confirm: function () {
                self.materialSubtypesService.deleteMaterialSubtype(s).subscribe(function (resp) {
                    self.handleDelete(resp, function () {
                        self.loadMaterialSubtypes();
                        if (self.materialSubtypeLinks.length > 0) {
                            self.handleMaterialTypeChange();
                        }
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.editSubtypeLink = function (sl) {
        this.materialSubtypeLink = sl;
        this.subtypeLinkCreateOpen = true;
    };
    RDDashboardIndexComponent.prototype.editMainMaterialLink = function (sl) {
        this.mainMaterialLink = sl;
        this.subtypeLinkCreateOpen = true;
    };
    RDDashboardIndexComponent.prototype.deleteSubtypeLink = function (sl) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('CONFIRM_DELETE'),
            autoClose: false,
            confirm: function () {
                self.materialSubtypeLinksService.deleteMaterialSubtypeLink(sl).subscribe(function (resp) {
                    self.handleDelete(resp, function () {
                        self.handleMaterialTypeChange();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.deleteMainMaterialLink = function (sl) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('CONFIRM_DELETE'),
            autoClose: false,
            confirm: function () {
                self.mainMaterialLinkService.deleteMainMaterialLink(sl).subscribe(function (resp) {
                    self.handleDelete(resp, function () {
                        self.handleMaterialTypeChangeMainMaterial();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    RDDashboardIndexComponent.prototype.handleSaveOrUpdate = function (resp, callback) {
        var self = this;
        if (resp.pk) {
            self.messageService.open({
                message: self.tr('SAVE_COMPLETED'),
                title: " ",
                autoClose: false,
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    RDDashboardIndexComponent.prototype.handleDelete = function (resp, callback) {
        var self = this;
        if (resp.status == 204) {
            self.messageService.open({
                title: " ",
                message: self.tr('DELETE_COMPLETED'),
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    RDDashboardIndexComponent.prototype.restorePropertyName = function (pn) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.propertyNamesService.restorePropertyName(pn).subscribe(function (resp) {
                    self.handleSaveOrUpdate(resp, function () {
                        self.loadPropertyNames();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.restoreAttributeLink = function (al) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.attributesLinkService.restoreAttributeLink(al).subscribe(function (resp) {
                    self.handleSaveOrUpdate(resp, function () {
                        self.loadAttributes();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.restoreSubtype = function (s) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.materialSubtypesService.restoreMaterialSubtype(s).subscribe(function (resp) {
                    self.handleSaveOrUpdate(resp, function () {
                        self.loadMaterialSubtypes();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.restoreSubtypeLink = function (sl) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.materialSubtypeLinksService.restoreMaterialSubtypeLink(sl).subscribe(function (resp) {
                    self.handleSaveOrUpdate(resp, function () {
                        self.handleMaterialTypeChange();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.restoreMainMaterialLink = function (ml) {
        var self = this;
        this.messageService.open({
            title: this.tr('ATTENTION'),
            message: this.tr('RESTORE_CONFIRM'),
            confirm: function () {
                self.mainMaterialLinkService.restoreMainMaterialLink(ml).subscribe(function (resp) {
                    self.handleSaveOrUpdate(resp, function () {
                        self.handleMaterialTypeChangeMainMaterial();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    RDDashboardIndexComponent.prototype.goToFirstPagePropertyNames = function () {
        this.currentPagePropertyNames = 1;
        this.loadPropertyNames(this.currentPagePropertyNames);
    };
    RDDashboardIndexComponent.prototype.goToLastPagePropertyNames = function () {
        this.currentPagePropertyNames = this.totalPagesPropertyNames;
        this.loadPropertyNames(this.currentPagePropertyNames);
    };
    RDDashboardIndexComponent.prototype.goToPrevPagePropertyNames = function () {
        if (this.currentPagePropertyNames != 1) {
            this.currentPagePropertyNames--;
            this.loadPropertyNames(this.currentPagePropertyNames);
        }
    };
    RDDashboardIndexComponent.prototype.goToNextPagePropertyNames = function () {
        if (this.currentPagePropertyNames != this.totalPagesPropertyNames) {
            this.currentPagePropertyNames++;
            this.loadPropertyNames(this.currentPagePropertyNames);
        }
    };
    RDDashboardIndexComponent.prototype.goToFirstPageAttributes = function () {
        this.currentPageAttribute = 1;
        this.loadAttributes(this.currentPageAttribute);
    };
    RDDashboardIndexComponent.prototype.goToLastPageAttributes = function () {
        this.currentPageAttribute = this.totalPagesAttribute;
        this.loadAttributes(this.currentPageAttribute);
    };
    RDDashboardIndexComponent.prototype.goToPrevPageAttributes = function () {
        if (this.currentPageAttribute != 1) {
            this.currentPageAttribute--;
            this.loadAttributes(this.currentPageAttribute);
        }
    };
    RDDashboardIndexComponent.prototype.goToNextPageAttributes = function () {
        if (this.currentPageAttribute != this.totalPagesAttribute) {
            this.currentPageAttribute++;
            this.loadAttributes(this.currentPageAttribute);
        }
    };
    RDDashboardIndexComponent.prototype.goToFirstPageSubtype = function () {
        this.currentPageSubtype = 1;
        this.loadMaterialSubtypes(this.currentPageSubtype);
    };
    RDDashboardIndexComponent.prototype.goToLastPageSubtype = function () {
        this.currentPageSubtype = this.totalPagesSubtype;
        this.loadMaterialSubtypes(this.currentPageSubtype);
    };
    RDDashboardIndexComponent.prototype.goToPrevPageSubtype = function () {
        if (this.currentPageSubtype != 1) {
            this.currentPageSubtype--;
            this.loadMaterialSubtypes(this.currentPageSubtype);
        }
    };
    RDDashboardIndexComponent.prototype.goToNextPageSubtype = function () {
        if (this.currentPageSubtype != this.totalPagesSubtype) {
            this.currentPageSubtype++;
            this.loadMaterialSubtypes(this.currentPageSubtype);
        }
    };
    RDDashboardIndexComponent.prototype.goToFirstPageSubtypeLink = function () {
        this.currentPageSubtypeLink = 1;
        this.loadMaterialSubtypeLinks(this.currentPageSubtypeLink);
    };
    RDDashboardIndexComponent.prototype.goToLastPageSubtypeLink = function () {
        this.currentPageSubtypeLink = this.totalPagesSubtypeLink;
        this.loadMaterialSubtypeLinks(this.currentPageSubtypeLink);
    };
    RDDashboardIndexComponent.prototype.goToPrevPageSubtypeLink = function () {
        if (this.currentPageSubtypeLink != 1) {
            this.currentPageSubtypeLink--;
            this.loadMaterialSubtypeLinks(this.currentPageSubtypeLink);
        }
    };
    RDDashboardIndexComponent.prototype.goToNextPageSubtypeLink = function () {
        if (this.currentPageSubtypeLink != this.totalPagesSubtypeLink) {
            this.currentPageSubtypeLink++;
            this.loadMaterialSubtypeLinks(this.currentPageSubtypeLink);
        }
    };
    RDDashboardIndexComponent.prototype.goToFirstPageMainMaterialLink = function () {
        this.currentPageMainMaterialLink = 1;
        this.loadMainMaterialLinks(this.currentPageMainMaterialLink);
    };
    RDDashboardIndexComponent.prototype.goToLastPageMainMaterialLink = function () {
        this.currentPageMainMaterialLink = this.totalPagesMainMaterialLink;
        this.loadMainMaterialLinks(this.currentPageMainMaterialLink);
    };
    RDDashboardIndexComponent.prototype.goToPrevPageMainMaterialLink = function () {
        if (this.currentPageMainMaterialLink != 1) {
            this.currentPageMainMaterialLink--;
            this.loadMainMaterialLinks(this.currentPageMainMaterialLink);
        }
    };
    RDDashboardIndexComponent.prototype.goToNextPageMainMaterialLink = function () {
        if (this.currentPageMainMaterialLink != this.totalPagesMainMaterialLink) {
            this.currentPageMainMaterialLink++;
            this.loadMainMaterialLinks(this.currentPageMainMaterialLink);
        }
    };
    return RDDashboardIndexComponent;
}());
RDDashboardIndexComponent = __decorate([
    core_1.Component({
        selector: 'app-rd-dashboard-index',
        templateUrl: 'rd-dashboard-index.component.html',
        styleUrls: ['rd-dashboard-index.component.css']
    })
], RDDashboardIndexComponent);
exports.RDDashboardIndexComponent = RDDashboardIndexComponent;
