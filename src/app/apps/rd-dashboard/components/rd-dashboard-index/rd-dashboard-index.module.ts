import {NgModule} from "@angular/core";
import {RDDashboardIndexComponent} from "./rd-dashboard-index.component";
import {SharedModule} from "../../../../shared/shared.module";
import {RDDashboardSubtypesComponent} from "./rd-dashboard-subtypes/rd-dashboard-subtypes.component";
import {RDDashboardSubtypesLinksComponent} from "./rd-dashboard-subtypes-links/rd-dashboard-subtypes-links.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [RDDashboardIndexComponent, RDDashboardSubtypesComponent, RDDashboardSubtypesLinksComponent],   // components and directives
  exports:[RDDashboardIndexComponent],
  providers: []                    // services
})
export class RDDashboardIndexModule { }


