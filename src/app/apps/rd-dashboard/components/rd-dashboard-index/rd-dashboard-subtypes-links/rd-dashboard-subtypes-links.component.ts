import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {cloneObject} from "../../../../../shared/utils";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MaterialSubTypeLink, MaterialSubtypesLinkService} from "../../../../_common/services/material-subtype-link.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {MainMaterialLinksService} from "../../../../_common/services/main-material-links.service";
import {MaterialType} from "../../../../_common/services/material-type.service";
import {GenericObj} from "../../../../_common/services/_generic.service";
import {MaterialSubType} from "../../../../_common/services/material-subtype.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'app-rd-dashboard-subtypes-links',
  templateUrl: 'rd-dashboard-subtypes-links.component.html',
  styleUrls: ['rd-dashboard-subtypes-links.component.css']
})
export class RDDashboardSubtypesLinksComponent extends BaseComponent implements OnInit {

  @Input() materialType:MaterialType;
  @Input() fieldname:string;
  @Output() editMaterialSubtypeLink = new EventEmitter();
  @Output() createMaterialSubtypeLink = new EventEmitter();

  subtypeLinksActive = true;

  @ViewChild('table') table:TableWithPagesComponent;

  noItemsMessageSubtypeLinks = this.tr("NO_SUBTYPES_LINKS_AVAILABLE");
  headerConfSubtypeLinks = [
    {
      label:this.tr("SUBTYPE"),
      valueKey:'subtype_id',
      useStr:true
    },
    {
      label:this.tr("SAP_CODE"),
      valueKey:'sap_code'
    }
  ];
  availableActionsSubtypeLinks = [];

  constructor(private materialSubtypeLinksService:MaterialSubtypesLinkService, protected translationService:TranslationService,
              protected messageService:MessageService, private mainMaterialLinkService:MainMaterialLinksService,
              protected loadingService: LoadingService
  ) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions(){
    let actionEditST:TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditST.click = this.editSubtypeLink.bind(this);
    let actionDeleteST:TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteST.click = this.deleteSubtypeLink.bind(this);
    actionDeleteST.visibilityCondition = (item:MaterialSubType) => item.is_active;
    let actionRestoreST:TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreST.click = this.restoreSubtype.bind(this);
    actionRestoreST.visibilityCondition = (item:MaterialSubType) => !item.is_active;
    this.availableActionsSubtypeLinks = [actionEditST,actionDeleteST,actionRestoreST];
  }

  editSubtypeLink(s:MaterialSubTypeLink){
    s.app.__str__ = this.materialType.__str__;
    this.editMaterialSubtypeLink.emit(s);
  }

  deleteSubtypeLink(s:MaterialSubTypeLink){
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () =>{
        this.materialSubtypeLinksService.deleteMaterialSubtypeLink(s).subscribe((resp)=>{
          this.manageRespDelete(resp,() => {
            this.table.reload();
          });
        })
      },
      deny:() => {

      }
    });
  }

  restoreSubtype(s:MaterialSubType){
    this.messageService.open({
      title:this.tr('ATTENTION'),
      message:this.tr('RESTORE_CONFIRM'),
      confirm:() => {
        this.materialSubtypeLinksService.restoreMaterialSubtypeLink(s).subscribe((resp)=>{
          this.manageRespSave(resp, () =>{
            this.table.reload();
          })
        })
      },
      deny:() =>{ }
    });
  }

  createSubtypeLink(){
    let materialSubtypeLink = new MaterialSubTypeLink();
    materialSubtypeLink.app = new GenericObj();
    materialSubtypeLink.app.pk = this.materialType.content_type;
    materialSubtypeLink.app.__str__ = this.materialType.__str__;
    this.createMaterialSubtypeLink.emit(materialSubtypeLink);
  }

  loadMaterialSubtypeLinks(page: number, results: number) {
    let query = {};
    query['is_active'] = this.subtypeLinksActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['app'] = this.materialType.content_type;
    if(this.fieldname=='subtype'){
      return this.materialSubtypeLinksService.getMaterialSubtypeLinks(query);
    }
    else if(this.fieldname=='main_material'){
      return this.mainMaterialLinkService.getMainMaterialLinks(query);
    }
  }

  reload(){
    this.table.reload();
  }
}
