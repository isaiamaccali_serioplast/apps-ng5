import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {PackagesIndexComponent} from "./components/packages-index/packages-index.component";
import {HrEmployeesPersonalInfoComponent} from "./components/hr-employees/components/hr-employees-personalinfo/hr-employees-personalinfo.component";
import {HrEmployeesWorkingContactsComponent} from "./components/hr-employees/components/hr-employees-workingcontacts/hr-employees-workingcontacts.component";
import {HrEmployeesLanguagesComponent} from "./components/hr-employees/components/hr-employees-languages/hr-employees-languages.component";
import {HrEmployeesProfessionalExperiencesComponent} from "./components/hr-employees/components/hr-employees-profexps/hr-employees-profexps.component";
import {HrEmployeesEducationsComponent} from "./components/hr-employees/components/hr-employees-educations/hr-employees-educations.component";
import {HrEmployeesNationalitiesComponent} from "./components/hr-employees/components/hr-employees-nationalities/hr-employees-nationalities.component";
import {HrEmployeesViewComponent} from "./components/hr-employees/components/hr-employees-view/hr-employees-view.component";
import {HrEmployeesBadgesComponent} from "./components/hr-employees/components/hr-employees-badges/hr-employees-badges.component";
import {HrEmployeesContractsComponent} from "./components/hr-employees/components/hr-employees-contracts/hr-employees-contracts.component";
import {HrEmployeesUserClockProfilesComponent} from "./components/hr-employees/components/hr-employees-user-clock-profiles/hr-employees-user-clock-profiles.component";
import {HrEmployeesPositionsComponent} from "./components/hr-employees/components/hr-employees-positions/hr-employees-positions.component";

// Route Configuration
export const hrManagementRoutes: Routes = [
  {
    path: '',
    component: PackagesIndexComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'employees/detail/:id',
    component: HrEmployeesViewComponent,
    children: [
      {path: '', component: HrEmployeesPersonalInfoComponent, outlet: 'personalinfochild'},
      {path: 'workingcontactscomp/:id', component: HrEmployeesWorkingContactsComponent, outlet: 'personalinfochild'},
      {path: 'languagescomp/:id', component: HrEmployeesLanguagesComponent, outlet: 'personalinfochild'},
      {path: 'professionalexperiencescomp/:id', component: HrEmployeesProfessionalExperiencesComponent, outlet: 'personalinfochild'},
      {path: 'educationscomp/:id', component: HrEmployeesEducationsComponent, outlet: 'personalinfochild'},
      {path: 'nationalitiescomp/:id', component: HrEmployeesNationalitiesComponent, outlet: 'personalinfochild'},
      {path: '', component: HrEmployeesBadgesComponent, outlet: 'timeattendancechild'},
      {path: 'badgescomp/:id', component: HrEmployeesBadgesComponent, outlet: 'timeattendancechild'},
      {path: 'ucpcomp/:id', component: HrEmployeesUserClockProfilesComponent, outlet: 'timeattendancechild'},
      {path: '', component: HrEmployeesPositionsComponent, outlet: 'positionschild'},
      {path: '', component: HrEmployeesContractsComponent, outlet: 'contractchild'},
    ]
  },
];
