import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {hrManagementRoutes} from "./hr-management.routes";
import {PackagesIndexModule} from "./components/packages-index/packages-index.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(hrManagementRoutes),PackagesIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class HRManagementModule {}

