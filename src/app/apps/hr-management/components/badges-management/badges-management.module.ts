import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {BadgesManagementComponent} from "./badges-management.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [BadgesManagementComponent],   // components and directives
  exports:[BadgesManagementComponent],
  providers: []                    // services
})
export class BadgesManagementModule { }


