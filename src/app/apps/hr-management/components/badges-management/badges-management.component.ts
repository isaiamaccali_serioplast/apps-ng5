import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {MatSnackBar} from "@angular/material";
import {Badge, BadgeService} from "../../../_common/services/badges.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {Title} from "@angular/platform-browser";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {User} from "../../../_common/services/users.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'badges-management',
  templateUrl: 'badges-management.component.html',
  styleUrls: ['badges-management.component.css']
})

export class BadgesManagementComponent extends BaseComponent implements OnInit {
  badgeCreateOpen: boolean;
  onCreate: boolean;
  currentUser: User = new User();
  badge: Badge = new Badge();
  sites: Site[] = [];
  badgeFilter: Badge;
  nameTimeout: any;
  active = true;
  availableActions = [];
  @ViewChild('badgesTable') badgesTable: TableWithPagesComponent;
  noItemsMessageBadges = this.tr("NO_BADGES");
  headerConfBadges = [
    {
      label: this.tr("BADGE_CODE"),
      valueKey: 'badge_code'
    },
    {
      label: this.tr("ASSIGNED_TO"),
      valueFunction: (item: Badge) => {
        if (item['assigned_to']) {
          return "<a style='color: #4CAF50' href='/hr-management/employees/detail/" + item.assigned_to.pk + "'>" + item.assigned_to.__str__ + "</a>"
        }
        else {
          return null;
        }
      }
    },
    {
      label: this.tr("IS_GUEST"),
      valueKey: 'guest',
    },
    {
      label: this.tr("LOCATION"),
      valueKey: 'location',
      useStr: true,
    }
  ];

  constructor(protected loadingService: LoadingService,
              private loginService: LoginService,
              private badgeService: BadgeService,
              protected translationService: TranslationService,
              private titleService: Title,
              private siteService: SitesService,
              protected messageService: MessageService,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.badgeFilter = new Badge();
    this.badgeCreateOpen = false;
    this.onCreate = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {
    let user = this.loginService.getUserFromStorage();
    this.currentUser = JSON.parse(user);
    this.initTableActions();
    this.loadSites();
  }

  initTableActions() {
    let actionEditBadge: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditBadge.click = this.editBadge.bind(this);
    let actionDeleteBadge: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteBadge.click = this.deleteBadge.bind(this);
    actionDeleteBadge.visibilityCondition = (item: Badge) => {
      return item.is_active;
    };
    let actionRestoreBadge: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreBadge.click = this.restoreBadge.bind(this);
    actionRestoreBadge.visibilityCondition = (item: Badge) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditBadge, actionRestoreBadge, actionDeleteBadge];
  }

  loadSites() {
    let query = {};
    query['is_active'] = true;
    query['user'] = this.currentUser.pk;
    this.siteService.getSites(query).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
    });
  }

  loadBadges(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['badges_by_hr'] = this.currentUser.pk;

    query['is_active'] = this.active;

    if (this.badgeFilter.badge_code)
      query['badge_code_icontains'] = this.badgeFilter.badge_code;

    if (this.badgeFilter.guest)
      query['guest'] = this.badgeFilter.guest;

    if (this.badgeFilter.location)
      query['location'] = this.badgeFilter.location;

    return this.badgeService.getBadges(query);
  }

  deleteBadge(item: Badge) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.badgeService.deleteBadge(item).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.badgesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreBadge(item: Badge) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.badgeService.restoreBadge(item).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.badgesTable.reload();
          })
        })
      },
      deny: () => {
      }
    });
  }

  saveBadge() {
    let displayMsg = false;
    if (this.badge.is_active == false) {
      displayMsg = true;
    }

    this.badgeService.saveBadge(this.badge).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createBadge();
        this.badgeCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    });
  }

  createBadge() {
    this.badge = new Badge();
    this.badgeCreateOpen = true;
    this.onCreate = true;
  }

  editBadge(item: Badge) {
    this.badge = cloneObject(item);
    this.badgeCreateOpen = true;
    this.onCreate = false;
  }

  undoBadge() {
    this.createBadge();
    this.badgeCreateOpen = false;
  }

  reset() {
    setTimeout(() => {
      this.reload();
    }, 500)
  }

  reload() {
    this.badgesTable.reload();
  }

}
