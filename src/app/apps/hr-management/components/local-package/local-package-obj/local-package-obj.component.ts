import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {WorkToolTypeService} from "../../../services/worktooltype.service";
import {PackagesService} from "../../../services/packages.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {LocalCpnPackageItem, LocalCpnPackageItemService} from "../../../../_common/services/localcpnpackageitem.service";
import {LocalPackageItem, LocalPackagesService} from "../../../../_common/services/localpackages.service";
import {fixValue} from "../../../../../shared/utils";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {CompanyPositionName} from "../../../../_common/services/company-position-name.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: "package-obj-list",
  templateUrl: "local-package-obj.component.html",
  styleUrls: ["local-package-obj.component.css"]
})

export class LocalPackageObjComponent extends BaseComponent implements OnInit {
  show: boolean;
  @Input() local_package: LocalPackageItem;
  @Input() cpn: CompanyPositionName[];
  showFormvar: boolean;
  localcpnpackage: LocalCpnPackageItem = new LocalCpnPackageItem();
  @Input() open_package_tab: LocalPackageItem;
  @Output() load_site_package = new EventEmitter();
  @Output() selectPackageTab = new EventEmitter();
  @Output() onEditLocalPackageItem = new EventEmitter<LocalPackageItem>();
  disableToggle: boolean;

  constructor(protected messageService: MessageService,
              private localcpnpackageservice: LocalCpnPackageItemService,
              private localpackageservice: LocalPackagesService,
              protected translationService: TranslationService,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.show = false;
    this.showFormvar = false;
  }

  removePackage(obj) {
    this.messageService.open({
      title: "Delete confirmation",
      message: "Are you sure to disable " + obj.__str__ + " ?",
      confirm: () => {
        this.localpackageservice.removeLocalPackageItem(obj).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.load_site_package.emit();
          })
        });
      },
      deny: () => {
        this.load_site_package.emit();
      }
    });
  }

  enablePackage(obj) {
    this.messageService.open({
      title: "Delete confirmation",
      message: "Are you sure to enable " + obj.__str__ + " ?",
      confirm: () => {
        obj.is_active = true;
        this.localpackageservice.updatePackageItem(obj).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.load_site_package.emit();
          });
        });
      },
      deny: () => {
        this.load_site_package.emit();
      }
    });
  }

  showForm() {
    if (this.showFormvar) {
      this.showFormvar = false
    } else {
      this.localcpnpackage = new LocalCpnPackageItem();
      this.localcpnpackage.site_package = this.local_package;
      this.localcpnpackage.mandatory = false;
      this.localcpnpackage.company_position_name = new CompanyPositionName();
      this.showFormvar = true
    }
  }

  addLocalCpnPackageItem() {
    this.localcpnpackageservice.addLocalCpnPackageItems(this.localcpnpackage).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.load_site_package.emit();
      });
    });
  }

  removeLocalCpnPackageItem(obj) {
    this.messageService.open({
      title: "Delete confirmation",
      message: "Are you sure ?",
      confirm: () => {
        this.localcpnpackageservice.removeCpnPackageItem(obj).subscribe((resp) => {
          this.manageRespDelete(resp, ()=> {
            this.load_site_package.emit();
          });
        });
      },
      deny: () => {
        this.disableToggle = false;
      }
    });
  }

  editLocalPackageItem(localpackage) {
    this.onEditLocalPackageItem.emit(localpackage);
  }

  selectTab(selected_tab: LocalPackageItem) {
    this.selectPackageTab.emit(selected_tab);
  }

  fixCPN(localcpnpackage: LocalCpnPackageItem) {
    fixValue(this.cpn, localcpnpackage, "company_position_name");
  }
}

