import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {WorkToolTypeService} from "../../../services/worktooltype.service";
import {LocalPackageObjComponent} from "./local-package-obj.component";
import {CompanyPositionNameService} from "../../../../hr-dashboard/services/company-position-name.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [LocalPackageObjComponent],   // components and directives
  exports:[LocalPackageObjComponent],
  providers: []                    // services
})
export class LocalPackageObjModule { }


