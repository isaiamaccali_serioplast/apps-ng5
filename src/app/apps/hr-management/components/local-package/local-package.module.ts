import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {LocalPackageComponent} from "./local-package.component";
import {LocalPackageObjModule} from "./local-package-obj/local-package-obj.module";
import {LocalPackageObjCreateModule} from "./local-package-obj-create/local-package-obj-create.module";

@NgModule({
  imports: [SharedModule,LocalPackageObjModule,LocalPackageObjCreateModule],       // module dependencies
  declarations: [LocalPackageComponent],   // components and directives
  exports:[LocalPackageComponent],
  providers: []                    // services
})
export class LocalPackageModule { }


