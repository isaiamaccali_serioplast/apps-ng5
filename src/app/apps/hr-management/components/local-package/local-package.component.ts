import {Component, OnInit} from "@angular/core";
import {LocalPackageItem, LocalPackagesService} from "../../../_common/services/localpackages.service";
import {fixValue, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {CompanyPositionName, CompanyPositionNameService} from "../../../_common/services/company-position-name.service";
import {WorkToolType, WorkToolTypeService} from "../../../_common/services/worktooltype.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {HRProfile} from "../../../_common/services/hr-profiles.service";

@Component({
  selector: 'local-package',
  templateUrl: 'local-package.component.html',
  styleUrls: ['local-package.component.css']
})

export class LocalPackageComponent implements OnInit {
  localpackages: LocalPackageItem[] = [];
  worktools: WorkToolType[] = [];
  open_package_tab_from_child: LocalPackageItem = new LocalPackageItem();
  cpn: CompanyPositionName[] = [];
  selected_site: Site = new Site();
  selected_worktool: WorkToolType = new WorkToolType();
  formShow: boolean;
  activePackages: boolean;
  selectedLocalPackageItem: LocalPackageItem = new LocalPackageItem();
  mode_edit: boolean;
  userSites:Site[] = [];

  constructor(private localpackageservice: LocalPackagesService,
              private companypositionservice: CompanyPositionNameService,
              private loginService:LoginService, private sitesService:SitesService,
              private worktoolservice: WorkToolTypeService, private messageService: MessageService) {
    this.mode_edit = false;
  }

  ngOnInit() {
    this.loadWorkToolTypeByPlant();
    this.activePackages = true;

    this.sitesService.getSites({is_active:true, user:this.loginService.user.pk}).subscribe((resp)=>{
      this.userSites = parseSerionetRespToData(resp);
    })
  }


  reloadPackages() {
    this.open_package_tab_from_child = new LocalPackageItem();
    this.load_site_package();
  }


  loadWorkToolTypeByPlant() {
    let query = {by_plant: true, is_active:true};
    this.worktoolservice.getWorkToolTypes(query).subscribe((response) => {
      this.worktools = parseSerionetRespToData(response);
    });
  }

  load_site_package() {
    let query = {};
    query['is_active'] = this.activePackages;

    if (this.selected_site.pk) {
      query['site'] = this.selected_site.pk;
    }else{
      query['site__in'] = this.userSites.join(",");
    }

    if (this.selected_worktool.pk) {
      query['type'] = this.selected_worktool.pk;
    }

    this.localpackageservice.getLocalPackages(query).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.localpackages = data['results'];
        if (!this.open_package_tab_from_child.pk) {
          this.open_package_tab_from_child = this.localpackages[0];
        }
      }
      else {
        this.localpackages = [];
      }
    });
    this.formShow = false
  }

  createPackage() {
    if (this.selected_worktool.pk) {
      if (this.selected_site && this.selected_site.pk) {
        this.selectedLocalPackageItem = new LocalPackageItem();
        this.selectedLocalPackageItem.reference = new HRProfile();
        this.selectedLocalPackageItem.site = this.selected_site;
        this.formShow = !this.formShow;
      }
      else {
        this.messageService.open({
          title: " ",
          message: "Select a site first"
        })
      }
    }
    else {
      this.messageService.open({
        title: " ",
        message: "Select a worktool first"
      })
    }

  }

  loadCPN() {
    let query = {};
    query['is_active'] = true;

    if(this.selected_site && this.selected_site.pk){
      query['site'] = this.selected_site.pk;
    }

    this.companypositionservice.getCompanyPositionNames(query).subscribe((data) => {
      this.cpn = parseSerionetRespToData(data)
    });
  }

  site_change(s:Site) {
    if(s && s.pk){
      this.selected_site = s;
      this.load_site_package();
      this.loadCPN();
    }
    else {
      this.selected_site = new Site();
    }
  }

  worktool_change() {
    this.loadWorkToolTypeByPlant();
    fixValue(this.worktools, this, 'selected_worktool');
  }

  selectPackageTab(selected_package: LocalPackageItem) {
    this.open_package_tab_from_child = selected_package;
    // reset for edit
    this.mode_edit = false;
    this.selectedLocalPackageItem = new LocalPackageItem();
    this.formShow = false;
  }

  OnEditLocalPackageItem(selectedLocalPackageItem:LocalPackageItem) {
    this.selected_site = selectedLocalPackageItem.site;
    this.selected_worktool = selectedLocalPackageItem.type;
    this.worktool_change();
    this.selectedLocalPackageItem = selectedLocalPackageItem;
    this.formShow = true;
    this.mode_edit = true;
    this.loadCPN();
  }

  undoCreate() {
    this.formShow = false;
  }

  viewAll(){
    this.selected_site = new Site();
    this.selected_worktool = new WorkToolType();
    this.load_site_package();
  }

}

