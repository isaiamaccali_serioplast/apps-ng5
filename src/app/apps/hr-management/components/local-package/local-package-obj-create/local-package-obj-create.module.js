"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var shared_module_1 = require("../../../../../shared/shared.module");
var sites_service_1 = require("../../../../_common/services/sites.service");
var local_package_obj_create_component_1 = require("./local-package-obj-create.component");
var packages_service_1 = require("../../../../hr-dashboard/services/packages-item.service.ts");
var profile_service_1 = require("../../../services/profile.service");
var LocalPackageObjCreateModule = (function () {
    function LocalPackageObjCreateModule() {
    }
    return LocalPackageObjCreateModule;
}());
LocalPackageObjCreateModule = __decorate([
    core_1.NgModule({
        imports: [shared_module_1.SharedModule],
        declarations: [local_package_obj_create_component_1.LocalPackageObjCreateComponent],
        exports: [local_package_obj_create_component_1.LocalPackageObjCreateComponent],
        providers: [sites_service_1.SitesService, packages_service_1.PackagesService, profile_service_1.ProfileService] // services
    })
], LocalPackageObjCreateModule);
exports.LocalPackageObjCreateModule = LocalPackageObjCreateModule;
