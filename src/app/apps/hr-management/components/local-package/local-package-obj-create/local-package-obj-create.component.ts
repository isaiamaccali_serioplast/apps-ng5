import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {LocalPackageItem, LocalPackagesService} from "../../../../_common/services/localpackages.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {HRProfile} from "../../../../_common/services/hr-profiles.service";
import {WorkToolType} from "../../../../_common/services/worktooltype.service";
import {Site} from "../../../../_common/services/sites.service";
import {buildErrorMessage} from "../../../../../shared/utils";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'package-obj-list-create',
  templateUrl: 'local-package-obj-create.component.html',
  styleUrls: ['local-package-obj-create.component.css']
})

export class LocalPackageObjCreateComponent extends TranslatableComponent implements OnInit {
  @Input() localpackageitem: LocalPackageItem = new LocalPackageItem();
  @Input() sites: Site[] = [];
  @Input() selected_site: Site = new Site();
  @Input() selected_worktool: WorkToolType = new WorkToolType();
  @Output() load_site_package = new EventEmitter();
  @Output() undo = new EventEmitter();
  @Input() selectedLocalPackageItem: LocalPackageItem = new LocalPackageItem();
  @Input() mode_edit: boolean;
  reference: HRProfile = new HRProfile();

  constructor(private localpackageitemservice: LocalPackagesService,
              private messageService: MessageService, protected translationService: TranslationService) {
    super(translationService);
    this.mode_edit = false;
  }

  ngOnInit() {
    if (this.mode_edit) {
      this.localpackageitem = this.selectedLocalPackageItem;
      this.selected_site = this.selectedLocalPackageItem.site;
    }
    else {
      this.localpackageitem.reference = new HRProfile();
    }
  }

  selectedReference(user: HRProfile) {
    if (user && user.pk) {
      this.localpackageitem.reference = user;
    }
    else {
      this.localpackageitem.reference = null;
    }
  }

  createLocalPackage() {

    //Fix worktool obj
    this.localpackageitem.content_type = this.selected_worktool.model_reference;
    this.localpackageitem.type = this.selected_worktool;
    this.localpackageitem.site = this.selected_site;

    //Fix sites
    //fixValue(this.sites,this.localpackageitem,'site');

    //Fix user
    //fixValue(this.profiles,this.localpackageitem,'reference');

    if (this.mode_edit) {
      this.localpackageitemservice.updatePackageItem(this.localpackageitem).subscribe((resp) => {
        if (resp.pk) {
          this.messageService.open({
            message: this.tr('SAVE_COMPLETED'),
            title: " ",
            autoClose: false,
            onClose: () => {
              this.localpackageitem = resp['results'];
              this.load_site_package.emit()
            }
          });

        }
        else {
          this.handleBadResponse(resp);
          this.load_site_package.emit()
        }
      });
    } else {
      this.localpackageitemservice.addLocalPackage(this.localpackageitem).subscribe((resp) => {
        if (resp.pk) {
          this.messageService.open({
            message: this.tr('SAVE_COMPLETED'),
            title: " ",
            autoClose: false,
            onClose: () => {
              this.localpackageitem = resp['results'];
              this.load_site_package.emit()
            }
          });
        }
        else {
          this.handleBadResponse(resp);
          this.load_site_package.emit()
        }
      });
    }
  }

  handleBadResponse(resp: any) {
    this.messageService.open({
      title: this.tr("ERROR"),
      error: true,
      message: buildErrorMessage(resp),
      autoClose: false
    });
  }

  undoCreate() {
    this.undo.emit();
  }
}
