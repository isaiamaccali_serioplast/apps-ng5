import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../shared/shared.module";
import {LocalPackageObjCreateComponent} from "./local-package-obj-create.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [LocalPackageObjCreateComponent],   // components and directives
  exports:[LocalPackageObjCreateComponent],
  providers: []                    // services
})
export class LocalPackageObjCreateModule { }


