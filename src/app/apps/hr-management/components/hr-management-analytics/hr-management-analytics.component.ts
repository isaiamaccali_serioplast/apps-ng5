import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {HeaderConf, TableRowAction, TableWithPagesComponent,} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {Site} from "../../../_common/services/sites.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {Title} from "@angular/platform-browser";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../_common/services/has-company-position.service";
import {CompanyPositionName, CompanyPositionNameService} from "../../../_common/services/company-position-name.service";
import {HrEventExport, HrEventService} from "../../../_common/services/hr-events.service";
import {HRProfile, HRProfileService} from "../../../_common/services/hr-profiles.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {AutocompleteComponent} from "../../../../shared/material/autocomplete/autocomplete.component";
import {TypeLink, TypeLinkService} from "../../../_common/services/type-link.service";
import {HREventSubtypesLinkService} from "../../../_common/services/hr-event-subtype-link.service";
import {buildErrorMessage, cloneObject, getUrlDate, getUrlDateTime, parseSerionetRespToData} from "../../../../shared/utils";
import {DialogComponent} from "../../../../shared/material/dialog/dialog.component";
import {User} from "../../../_common/services/users.service";
import {CompanyPosition} from "../../../_common/services/company-position.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

let TABLEACTIONGETATTACHMENTS = new TableRowAction();
TABLEACTIONGETATTACHMENTS.icon = 'visibility';
TABLEACTIONGETATTACHMENTS.tooltip = 'See attachments';

@Component({
  selector: 'hr-management-analytics',
  templateUrl: 'hr-management-analytics.component.html',
  styleUrls: ['hr-management-analytics.component.css']
})

export class HrManagementAnalyticsComponent extends TranslatableComponent implements OnInit {
  onCreate: boolean;
  currentUser: User = new User();
  sitesEvents: Site[] = [];
  eventTypes: TypeLink[] = [];
  eventTypeFilter: number;
  eventSubTypes: TypeLink[] = [];
  eventSubTypeFilter: number;
  siteFilter: number;
  siteEventFilter: number;
  cpnEventFilter: number;
  profileEventFilter: number;
  companyPositionNames: CompanyPositionName[] = [];
  companyPositionNamesEvents: CompanyPositionName[] = [];
  hcpFilter: CompanyPosition = new CompanyPosition();
  nameTimeout: any;
  availableActions = [];
  availableEventsActions = [];
  @ViewChild('hcpTable') hcpTable: TableWithPagesComponent;
  @ViewChild('profileAutocomplete') profileAutocomplete: AutocompleteComponent;
  noItemsMessageHasCompanyPositions = this.tr("NO_EMPLOYEES");
  headerConfHasCompanyPositions: HeaderConf[] = [
    {
      label: this.tr("USER_REF"),
      valueKey: 'user_ref',
      columnId: 'user',
      useStr: true,
    },
    {
      label: this.tr("COMPANY_POSITIONS"),
      valueFunction: (item: HasCompanyPosition) => {

        if (item.company_positions) {
          let cpn_names_ul = "<ol>";
          let cpn_names = item.company_positions.split(",");
          cpn_names.map((obj) => {
            let li = "<li>" + obj + "</li>";
            cpn_names_ul = cpn_names_ul + li;
          });

          cpn_names_ul = cpn_names_ul + "</ol>";
          return cpn_names_ul;
        }

      }
    },
    {
      label: this.tr("STATUS"),
      valueKey: 'status',
      columnId: 'status',
      useStr: true,
    }
  ];

  @ViewChild('hrEventsTable') hrEventsTable: TableWithPagesComponent;
  noItemsMessageEvents = this.tr("NO_EVENTS");
  headerConfEvents: HeaderConf[] = [
    {
      label: this.tr("USER"),
      valueKey: 'user',
    },
    {
      label: this.tr("TITLE"),
      valueKey: 'title',
    },
    {
      label: this.tr("START_DATE"),
      valueFunction: (item: HrEventExport) => {
        if(item.full_day_event){
          return getUrlDate(new Date(item.start_date));
        }
        else {
          return getUrlDateTime(new Date(item.start_date)).split("T").join(" ");
        }
      }
    },
    /*    {
     label: this.tr("START_DATE_TIMEZONE"),
     valueKey: 'start_timezone',
     },*/
    {
      label: this.tr("END_DATE"),
      valueFunction: (item: HrEventExport) => {
        if(item.full_day_event){
          return getUrlDate(new Date(item.end_date));
        }
        else {
          return getUrlDateTime(new Date(item.end_date)).split("T").join(" ");
        }
      }
    },
    /*    {
     label: this.tr("END_DATE_TIMEZONE"),
     valueKey: 'end_timezone',
     },*/
    {
      label: this.tr("STATUS"),
      valueKey: 'status',
    },
    {
      label: this.tr("TYPE"),
      valueKey: 'type',
      // useStr: true,
    },
    {
      label: this.tr("SUBTYPE"),
      valueKey: 'subtype',
      // useStr: true,
    },
    {
      label: this.tr("APPROVAL"),
      valueKey: 'status_event',
      // useStr: true,
    }
  ];
  @ViewChild('currentAttachments') currentAttachments: DialogComponent;
  eventSelected: HrEventExport = new HrEventExport();

  format: string = "xlsx";
  analyticsEmployee: boolean;
  analyticsEvents: boolean;
  montEventFilter: string;

  constructor(private loginService: LoginService,
              private hcpService: HasCompanyPositionService,
              private translationservice: TranslationService,
              private titleService: Title,
              private cpnService: CompanyPositionNameService,
              private hrEventService: HrEventService,
              private hrProfileService: HRProfileService,
              private typeLinkService: TypeLinkService,
              private eventSubtypeLinkService: HREventSubtypesLinkService,
              private loadingService: LoadingService, private messageService: MessageService) {
    super(translationservice);
    this.titleService.setTitle("Serioplast - HR Management Reports");
    this.analyticsEmployee = true;
    this.analyticsEvents = false;
  }

  ngOnInit() {
    this.initCurrentMonth();
    let user = this.loginService.getUserFromStorage();
    this.currentUser = JSON.parse(user);
    this.loadCompanyPositionNames();
  }

  getAttachments(item: HrEventExport) {

    this.eventSelected = cloneObject(item);
    this.currentAttachments.open();
  }

  initTableEventsActions() {

    let actionGetAttachments: TableRowAction = cloneObject(TABLEACTIONGETATTACHMENTS);
    actionGetAttachments.click = this.getAttachments.bind(this);
    actionGetAttachments.visibilityCondition = (item: HrEventExport) => {
      return item.attachments.length > 0;
    };

    this.availableEventsActions = [actionGetAttachments];
  }

  initCurrentMonth() {
    let currentDate = new Date();
    let currentMonth = Number(currentDate.getMonth()) + 1;
    let currentMonthStr = (currentMonth < 10) ? "0" + currentMonth : currentMonth;
    this.montEventFilter = currentDate.getFullYear() + "-" + currentMonthStr;
  }

  viewEmployees() {
    this.analyticsEmployee = true;
    this.analyticsEvents = false;
  }

  viewEvents() {
    this.analyticsEmployee = false;
    this.analyticsEvents = true;
    this.initTableEventsActions();
    this.loadEventTypesLinks();
    this.loadEventSubTypesLinks();
    this.loadCompanyPositionNamesEvents();
  }

  loadEventTypesLinks() {
    let query = {};
    query['is_active'] = true;
    query['app'] = 88; // app calendars, model event
    query['field_name'] = 'type';
    this.typeLinkService.getTypeLinks(query).subscribe((data) => {
      this.eventTypes = parseSerionetRespToData(data);
    });
  }

  loadEventSubTypesLinks() {
    let query = {};
    query['is_active'] = true;

    if (this.eventTypeFilter) {
      let type = this.eventTypes.filter((x) => x.pk == this.eventTypeFilter);
      if (type.length == 1) {
        query['type'] = type[0].pk;
      }
    }

    if (this.siteEventFilter) {
      let country = this.sitesEvents.filter((x) => x.pk == this.siteEventFilter);
      if (country.length == 1) {
        query['country'] = country[0].country;
      }
    }

    this.eventSubtypeLinkService.getEventSubtypeLinks(query).subscribe((data) => {
      this.eventSubTypes = parseSerionetRespToData(data);
    });
  }

  loadCompanyPositionNames() {
    let query = {};
    query['is_active'] = true;

    if (this.siteFilter) {
      query['site'] = this.siteFilter;
    }

    this.cpnService.getCompanyPositionNames(query).subscribe((data) => {
      this.companyPositionNames = parseSerionetRespToData(data);
    });
  }

  loadCompanyPositionNamesEvents() {
    let query = {};
    query['is_active'] = true;

    if (this.siteEventFilter) {
      query['site'] = this.siteEventFilter;
    }

    this.cpnService.getCompanyPositionNames(query).subscribe((data) => {
      this.companyPositionNamesEvents = parseSerionetRespToData(data)
    });
  }

  loadHasCompanyPositions(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    this.setFiltersEmployees(query);
    return this.hcpService.getHasCompanyPositions(query);
  }

  loadHrEvents(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    this.setFiltersEvents(query);

    return this.hrEventService.getHrEvents(query);
  }

  setFiltersEmployees(filters?: any) {

    let query = (filters) ? filters : {};

    query['is_active'] = true;

    query['distinct_user_ref'] = 2;

    // HCP STATUS == EMPLOYEE BY DELTA REL
    query['status__delta_rel'] = 2;
    // SITE THAT HR MANAGER CAN SEE
    query['user_sites'] = this.loginService.user.pk;

    if (this.hcpFilter.site_ref)
      query['company_position__site_ref'] = this.hcpFilter.site_ref.pk;

    if (this.hcpFilter.name)
      query['company_position__name'] = this.hcpFilter.name;

    return query;
  }

  setFiltersEvents(filters?: any) {

    let query = (filters) ? filters : {};

    query['is_active'] = true;
    query['ordering'] = "start_date";
    query['user_sites'] = this.loginService.user.pk;

    if (this.montEventFilter) {
      query['year_month'] = this.montEventFilter;
    }

    if (this.siteEventFilter) {
      query['plant'] = this.siteEventFilter;
    }

    if (this.cpnEventFilter) {
      query['job_title'] = this.cpnEventFilter;
    }

    if (this.profileEventFilter) {
      query['user'] = this.profileEventFilter;
    }

    if (this.eventTypeFilter) {
      query['type'] = this.eventTypeFilter;
    }

    if (this.eventSubTypeFilter) {
      query['sub_type'] = this.eventSubTypeFilter;
    }

    return query;
  }

  EventTypeChange() {
    this.eventSubTypeFilter = null;
    this.loadEventSubTypesLinks();
  }

  locationChange(site: Site) {
    if (site && site.pk) {
      this.siteFilter = site.pk;
      this.siteEventFilter = site.pk;
      this.hcpFilter.site_ref = site;
    }
    else {
      this.siteFilter = null;
      this.siteEventFilter = null;
      this.hcpFilter.site_ref = null;
    }

    this.loadCompanyPositionNames();
  }

  locationEventChange(site: Site) {
    this.siteEventFilter = site.pk;
    this.eventSubTypeFilter = null;
    this.loadCompanyPositionNamesEvents();
    this.loadEventSubTypesLinks();
  }

  loadEmployees(keyword: string) {
    let query = {};
    query['is_active'] = true;

    query['profiles_by_hr'] = this.loginService.user.pk;

    if (keyword) {
      query['full_name_icontains'] = keyword;
    }

    return this.hrProfileService.getEmployeesProfile(query);
  }

  selectedProfileEventFilter(p: HRProfile) {
    if (p && p.pk) {
      this.profileEventFilter = p.pk;
    }
    else {
      this.profileEventFilter = null;
    }
    //  this.hrEventsTable.reload();
  }

  downloadEmployees() {
    this.loadingService.show();
    let query = {};
    this.setFiltersEmployees(query);
    this.hcpService.getHasCompanyPositions(query, this.format).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status != 200) {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  downloadEvents() {
    this.loadingService.show();
    let query = {};
    this.setFiltersEvents(query);
    this.hrEventService.getHrEvents(query, this.format).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status != 200) {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  resetEventsFilter() {
    this.montEventFilter = null;
    this.initCurrentMonth();
    this.siteEventFilter = null;
    this.eventTypeFilter = null;
    this.eventSubTypeFilter = null;
    this.cpnEventFilter = null;
    this.profileAutocomplete.reset();
    this.selectedProfileEventFilter(null);
    this.loadHrEvents(1, 25);
  }

}
