import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {HrManagementAnalyticsComponent} from "./hr-management-analytics.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrManagementAnalyticsComponent],   // components and directives
  exports:[HrManagementAnalyticsComponent],
  providers: []                    // services
})
export class HrManagementAnalyticsModule { }


