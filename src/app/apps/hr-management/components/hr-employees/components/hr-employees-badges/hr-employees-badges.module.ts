import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesBadgesComponent} from "./hr-employees-badges.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesBadgesComponent],   // components and directives
  exports:[HrEmployeesBadgesComponent],
  providers: []                    // services
})
export class HrEmployeesBadgesModule { }


