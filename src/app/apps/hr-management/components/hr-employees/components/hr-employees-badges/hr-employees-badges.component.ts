import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {BadgeUserRef, BadgeUserRefService} from "../../../../../_common/services/badge-user-refs.service";
import {Badge} from "../../../../../_common/services/badges.service";
import {DjangoLanguage, DjangoLanguagesService} from "../../../../../_common/services/django-languages.service";
import {Title} from "@angular/platform-browser";
import {HRUser} from "../../../../../_common/services/hr-users.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-badges',
  templateUrl: 'hr-employees-badges.component.html',
  styleUrls: ['hr-employees-badges.component.css']
})

export class HrEmployeesBadgesComponent extends BaseComponent implements OnInit {

  // current model
  badgeUserRef: BadgeUserRef = new BadgeUserRef();

  // related model or custom list
  badges: Badge[] = [];
  djangolanguages: DjangoLanguage[] = [];
  active = true;

  // table/form settings
  onCreate: boolean;
  badgeUserRefCreateOpen: boolean;
  availableActions = [];
  @ViewChild('badgeUserRefsTable') badgeUserRefsTable: TableWithPagesComponent;
  noItemsMessageBadgeUserRefs = this.tr("NO_BADGES");
  headerConfBadgeUserRefs = [
    {
      label: this.tr("BADGE_CODE_LOCATION"),
      valueKey: 'badge',
      useStr: true
    },
    {
      label: this.tr("START_DATE_BADGE"),
      valueKey: 'start_date',
    },
    {
      label: this.tr("END_DATE_BADGE"),
      valueKey: 'end_date',
    },
    {
      label: this.tr("LANGUAGE_CODE"),
      valueKey: 'language',
    }
  ];

  user_id: string;

  constructor(private activatedRoute: ActivatedRoute,
              private djangolanguageService: DjangoLanguagesService,
              protected translationService: TranslationService,
              private titleService: Title,
              private badgeUserRefservice: BadgeUserRefService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.onCreate = false;
    this.badgeUserRefCreateOpen = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.user_id = params['id'];
        this.loadDjangoLanguages();
        this.initTableActions();
      }
    });
  }

  loadDjangoLanguages() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.djangolanguageService.getDjangoLanguages({}).subscribe((data) => {

          for (let i = 0; i < data.length; i++) {
            let djangoLanguage = new DjangoLanguage();
            djangoLanguage.label = data[i][0];
            djangoLanguage.value = data[i][1];
            this.djangolanguages[i] = djangoLanguage;
          }

          // Remove empty label
          this.djangolanguages.shift();
        });

      }
    });
  }

  initTableActions() {

    let actionEditBadgeUserRef: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditBadgeUserRef.click = this.editBadgeUserRef.bind(this);

    let actionDeleteBadgeUserRef: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteBadgeUserRef.click = this.deleteBadgeUserRef.bind(this);
    actionDeleteBadgeUserRef.visibilityCondition = (item: BadgeUserRef) => {
      return item.is_active;
    };

    let actionRestoreBadgeUserRef: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreBadgeUserRef.click = this.restoreBadgeUserRef.bind(this);
    actionRestoreBadgeUserRef.visibilityCondition = (item: BadgeUserRef) => {
      return !item.is_active;
    };

    this.availableActions = [actionEditBadgeUserRef, actionDeleteBadgeUserRef, actionRestoreBadgeUserRef];
  }

  loadBadgeUserRefs() {

    let query = {};
    query['is_active'] = this.active;
    query['user'] = this.user_id;
    return this.badgeUserRefservice.getBadgeUserRefs(query);
  }

  editBadgeUserRef(pe: BadgeUserRef) {
    this.badgeUserRef = cloneObject(pe);
    this.badgeUserRefCreateOpen = true;
    this.badges.push(this.badgeUserRef.badge);
    this.onCreate = false;
  }

  deleteBadgeUserRef(pe: BadgeUserRef) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.badgeUserRefservice.deleteBadgeUserRef(pe).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.badgeUserRefsTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  createBadgeUserRef() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.badgeUserRef = new BadgeUserRef();
        this.badgeUserRef.user = new HRUser();
        this.badgeUserRef.user.pk = parseInt(params['id'], 10);
        this.badgeUserRef.badge = new Badge();
        this.badgeUserRefCreateOpen = true;
        this.onCreate = true;
      }
    });
  }

  saveBadgeUserRef() {

    let displayMsg = false;
    if (this.badgeUserRef.is_active == false) {
      displayMsg = true;
    }
    // save language
    this.badgeUserRefservice.saveBadgeUserRef(this.badgeUserRef).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.badgeUserRef = resp;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.badgeUserRefCreateOpen = false;
        this.badgeUserRefsTable.reload();
      });
    });

  }

  undoBadgeUserRef() {
    this.createBadgeUserRef();
    this.badgeUserRefCreateOpen = false;
  }

  restoreBadgeUserRef(item: BadgeUserRef) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.badgeUserRefservice.saveBadgeUserRef(item).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.badgeUserRefsTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

}
