import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesContractsComponent} from "./hr-employees-contracts.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesContractsComponent],   // components and directives
  exports:[HrEmployeesContractsComponent],
  providers: []                    // services
})
export class HrEmployeesContractsModule { }


