import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Title} from "@angular/platform-browser";
import {Contract, ContractService} from "../../../../../_common/services/contracts.service";
import {HRUser, HRUserService} from "../../../../../_common/services/hr-users.service";
import {LegalEntity, LegalEntityService} from "../../../../../_common/services/legal-entity.service";
import {DayWeekProfile, DayWeekProfileService} from "../../../../../_common/services/day-week-profile-service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-contracts',
  templateUrl: 'hr-employees-contracts.component.html',
  styleUrls: ['hr-employees-contracts.component.css']
})

export class HrEmployeesContractsComponent extends BaseComponent implements OnInit {

  // current model
  contract: Contract = new Contract();

  // related model or custom list
  // users:HRUser[] = [];
  hierarchicalUsers: HRUser[] = [];
  // functionalUsers: HRUser[] = [];
  active: boolean = true;

  // table/form settings
  onCreate: boolean;
  contractCreateOpen: boolean;
  availableActions = [];
  @ViewChild('contractsTable') contractsTable: TableWithPagesComponent;
  @ViewChild('milestonesTable') milestonesTable: TableWithPagesComponent;

  noItemsMessageContracts = this.tr("NO_CONTRACTS");
  headerConfContracts = [
    {
      label: this.tr("HIERARCHICAL_SUPERIOR"),
      valueKey: 'hierarchical_superior',
      useStr: true
    },
    // {
    //   label: this.tr("FUNCTIONAL_SUPERIOR"),
    //   valueKey: 'functional_superior',
    //   useStr: true
    // },
    {
      label: this.tr("LEGAL_ENTITY"),
      valueKey: 'legal_entity',
      useStr: true
    },
    {
      label: this.tr("START_DATE"),
      valueKey: 'start_date',
    },
    {
      label: this.tr("END_DATE"),
      valueKey: 'end_date',
    },
  ];

  legalEntities: LegalEntity[] = [];
  dayWeekProfiles: DayWeekProfile[] = [];
  user_id: string;

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private titleService: Title,
              private contractservice: ContractService,
              private hruserService: HRUserService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private snackBar: MatSnackBar,
              private legalEntityService: LegalEntityService,
              private dayWeekProfileService: DayWeekProfileService) {
    super(translationService, messageService, loadingService);
    this.onCreate = false;
    this.contractCreateOpen = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.user_id = params['id'];
        this.initTableActions();
      }
    });

    this.legalEntityService.getLegalEntities({is_active: true}).subscribe((data) => {
      this.legalEntities = parseSerionetRespToData(data);
    });

    this.dayWeekProfileService.getDayWeekProfiles({is_active: true}).subscribe((data) => {
      this.dayWeekProfiles = parseSerionetRespToData(data);
    });
  }

  loadHrUsersHier(keyword?: string) {
    let query = {};
    query['is_active'] = true;

    if (keyword) {
      query['search'] = keyword;
    }

    this.hruserService.getHrUsers(query).subscribe((response) => {
      this.hierarchicalUsers = response['results']
    });
  }

  initTableActions() {

    let actionEditContract: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditContract.click = this.editContract.bind(this);

    let actionDeleteContract: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteContract.click = this.deleteContract.bind(this);
    actionDeleteContract.visibilityCondition = (item: Contract) => {
      return item.is_active;
    };

    let actionRestoreContract: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreContract.click = this.restoreContract.bind(this);
    actionRestoreContract.visibilityCondition = (item: Contract) => {
      return !item.is_active;
    };

    this.availableActions = [actionEditContract, actionDeleteContract, actionRestoreContract];
  }

  loadContracts() {

    let query = {};
    query['is_active'] = this.active;
    query['user'] = this.user_id;
    return this.contractservice.getContracts(query);
  }

  selectedHierarchicalSuperior(hierarchicalSuperior: HRUser) {
    if (hierarchicalSuperior && hierarchicalSuperior.pk) {
      this.contract.hierarchical_superior = hierarchicalSuperior;
    }
    else {
      this.contract.hierarchical_superior = null;
    }
  }

  editContract(pe: Contract) {
    this.contract = cloneObject(pe);
    if (!this.contract.milestones)
      this.contract.milestones = [];
    this.contractCreateOpen = true;
    this.onCreate = false;
  }

  deleteContract(pe: Contract) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.contractservice.deleteContract(pe).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.contractsTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  createContract() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.contract = new Contract();
        this.contract.user = new HRUser();
        this.contract.user.pk = parseInt(params['id'], 10);
        this.contractCreateOpen = true;
        this.contract.legal_entity = new LegalEntity();
        this.contract.milestones = [];
        this.onCreate = true;
      }
    });
  }

  saveContract() {
    let displayMsg = false;
    if (this.contract.is_active == false) {
      displayMsg = true;
    }
    this.contractservice.saveContract(this.contract).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.contract = resp;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.contractCreateOpen = false;
        this.contractsTable.reload();
      });
    });

  }

  undoContract() {
    this.createContract();
    this.contractCreateOpen = false;
  }

  restoreContract(item: Contract) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.contractservice.saveContract(item).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.contractsTable.reload();
          })
        })
      },
      deny: () => {
      }
    });
  }
}
