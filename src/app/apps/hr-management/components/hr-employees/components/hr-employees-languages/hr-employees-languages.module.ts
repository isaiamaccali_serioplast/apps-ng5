import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesLanguagesComponent} from "./hr-employees-languages.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesLanguagesComponent],   // components and directives
  exports:[HrEmployeesLanguagesComponent],
  providers: []                    // services
})
export class HrEmployeesLanguagesModule { }


