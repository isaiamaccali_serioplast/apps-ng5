import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../../../shared/utils";
import {Language, LanguageService} from "../../../../../_common/services/language.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar} from "@angular/material";
import {PriorityLink, PriorityLinkService} from "../../../../../_common/services/priority-link.service";
import {DjangoLanguage, DjangoLanguagesService} from "../../../../../_common/services/django-languages.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Title} from "@angular/platform-browser";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-languages',
  templateUrl: 'hr-employees-languages.component.html',
  styleUrls: ['hr-employees-languages.component.css']
})

export class HrEmployeesLanguagesComponent extends BaseComponent implements OnInit {
  language: Language = new Language();
  languages: Language[] = [];
  djangolanguages: DjangoLanguage[] = [];
  levels: PriorityLink[] = [];
  onCreate: boolean;
  languageCreateOpen: boolean;
  availableActions = [];
  @ViewChild('languagesTable') languagesTable: TableWithPagesComponent;
  noItemsMessageLanguages = this.tr("No languages.");
  headerConfLanguages = [
    {
      label: this.tr("Language"),
      valueKey: '__str__'
    },
    {
      label: this.tr("Level"),
      valueKey: 'level',
      useStr: true,
    },
  ];

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private languageservice: LanguageService,
              private djangolanguageservice: DjangoLanguagesService,
              private prioritylinkservice: PriorityLinkService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private titleService: Title,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.onCreate = false;
    this.languageCreateOpen = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.loadLevels();
        this.loadDjangoLanguages();
        this.initTableActions();
      }
    });
  }

  initTableActions() {

    let actionEditLanguage: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditLanguage.click = this.editLanguage.bind(this);

    let actionDeleteLanguage: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteLanguage.click = this.deleteLanguage.bind(this);
    actionDeleteLanguage.visibilityCondition = (item: Language) => {
      return item.is_active;
    };

    this.availableActions = [actionEditLanguage, actionDeleteLanguage];
  }

  /**
   * Languages
   */

  loadLanguages() {
    this.activatedRoute.params.subscribe((params) => {

      if (params['id']) {
        let query = {};
        query['is_active'] = true;
        query['user_id'] = params['id'];
        return this.languageservice.getLanguages(query);
      }
    });
  }

  loadLevels() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        let query = {};
        query['app'] = 11; // model Language
        query['fieldname'] = "level";
        query['is_active'] = true;

        this.prioritylinkservice.getPriorityLinks(query).subscribe((resp) => {
          this.levels = resp['results'];
        })
      }
    });
  }

  loadDjangoLanguages() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.djangolanguageservice.getDjangoLanguages({}).subscribe((data) => {
          this.djangolanguages = data;
        });

      }
    });
  }

  editLanguage(lan: Language) {
    this.language = cloneObject(lan);
    this.languageCreateOpen = true;
    this.onCreate = false;
  }

  deleteLanguage(cpn: Language) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.languageservice.deleteLanguage(cpn).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.languagesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  createLanguage() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.language = new Language();
        this.language.user_id = params['id'];
        this.language.level = new PriorityLink();
        this.languageCreateOpen = true;
        this.onCreate = true;
      }
    });
  }

  saveLanguage() {

    let displayMsg = false;
    if (this.language.is_active == false) {
      displayMsg = true;
    }
    // save language
    this.languageservice.saveLanguage(this.language).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.language = resp;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.languageCreateOpen = false;
        this.languagesTable.reload();
      });
    });

  }

  undoLanguage() {
    this.createLanguage();
    this.languageCreateOpen = false;
  }

}
