import {Component, OnInit} from "@angular/core";
import {UserWorkCalendar, UserWorkCalendarService} from "../../../../../_common/services/user-work-calendar.service";
import {ClockProfileService} from "../../../../../_common/services/clock-profile.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {DomSanitizer} from "@angular/platform-browser";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {UserClockProfileService} from "../../../../../_common/services/user-clock-profile.service";
import {TranslatableComponent} from "../../../../../../shared/components/base-component/translatable.component";

export class UserWorkCalendarPeriod extends UserWorkCalendar {
  start: string;
  end: string;
  uwc: UserWorkCalendar[] = [];
}

@Component({
  selector: 'hr-employees-user-calendar',
  templateUrl: 'hr-employees-user-calendar.component.html',
  styleUrls: ['hr-employees-user-calendar.component.css']
})

export class HrEmployeesUserCalendarComponent extends TranslatableComponent implements OnInit {

  // @Input('profile') hrProfile: HRProfile;
  // @ViewChild(CalendarComponent) myCalendar: CalendarComponent;
  // @ViewChild("cpDialog") cpDialog: DialogComponent;
  //
  // lastStart = "";
  // lastEnd = "";
  // calendarOptions: any = {
  //   height: 'parent',
  //   fixedWeekCount: false,
  //   defaultDate: getUrlDate(new Date(), "-"),
  //   editable: false,
  //   eventLimit: false,
  //   events: [],
  //   header: {
  //     left: 'title',
  //     center: '',
  //     right: 'prev,next'
  //   },
  //   viewRender: (function (view) {
  //     let start = view['intervalStart'].format("YYYYMMDD");
  //     let end = view['intervalEnd'].format("YYYYMMDD");
  //     if ((start != this.lastStart) && (end != this.lastEnd)) {
  //       this.lastStart = start;
  //       this.lastEnd = end;
  //       this.processEvents()
  //     }
  //   }).bind(this),
  //   selectHelper: true,
  //   selectable: true,
  //   select: (function (start, end) {
  //     this.handleSelection(start, end)
  //   }).bind(this),
  //   unselect: (function () {
  //     this.periodToEdit = null;
  //   }).bind(this),
  //   unselectAuto: false
  // };
  //
  // userWorkCalendars: UserWorkCalendar[] = [];
  // userClockProfiles: UserClockProfile[] = [];
  // clockProfiles: ClockProfile[] = [];
  //
  // events = [];
  // periodToEdit: UserWorkCalendarPeriod;
  // hasDataToSave = false;
  //
  // profileToShow:ClockProfile;

  constructor(private userWorkCalendarService: UserWorkCalendarService,
              private loadingService: LoadingService, private domSanitizer: DomSanitizer,
              private messageService: MessageService, protected translationService: TranslationService,
              private userClockProfileService: UserClockProfileService,
              private clockProfilesService: ClockProfileService) {
    super(translationService);
  }

  ngOnInit() {
  }

  // dayRender(date, cell) {
  //   let cellDate = date.format("YYYY-MM-DD");
  //   let uwc = this.userWorkCalendars.filter((uwc) => uwc.date == cellDate);
  //   if (uwc && uwc.length > 0) {
  //     let uwc_ = uwc[0];
  //     if (uwc_.workday) {
  //       cell.css("background-color", "#cdeecb");
  //     }
  //     else {
  //       cell.css("background-color", "#ccc");
  //     }
  //   }
  // }
  //
  // processEvents() {
  //   let query = {};
  //   query['user'] = this.hrProfile.user_id.pk;
  //   query['date__range_0'] = this.myCalendar.fullCalendar('getView')['intervalStart'].format("YYYYMMDD");
  //   query['date__range_1'] = this.myCalendar.fullCalendar('getView')['intervalEnd'].format("YYYYMMDD");
  //   query['mode'] = "mix";
  //
  //   this.loadingService.show();
  //   this.userWorkCalendarService.getUserWorkCalendars(query).subscribe((resp) => {
  //     this.userWorkCalendars = parseSerionetRespToData(resp);
  //     this.userClockProfileService.getUserClockProfiles({is_active: true, user:this.hrProfile.user_id.pk}).subscribe((resp) => {
  //       this.userClockProfiles = parseSerionetRespToData(resp);
  //       let clockProfiles = this.userClockProfiles.map((ucp)=>ucp.clock_profile);
  //
  //       this.clockProfilesService.getClockProfiles({is_active:true}).subscribe((resp)=>{
  //
  //         let cp:ClockProfile[] = parseSerionetRespToData(resp);
  //
  //         this.clockProfiles = clockProfiles.map((prof)=>{
  //           return cp.filter((p)=>p.pk==prof.pk)[0]
  //         });
  //         let self = this;
  //         this.userWorkCalendars.forEach((uwc) => {
  //           if (uwc.clock_profile) {
  //             fixValue(self.clockProfiles, uwc, 'clock_profile');
  //           }
  //           else {
  //             uwc.clock_profile = new ClockProfile();
  //           }
  //         });
  //         this.updateEvents();
  //         this.loadingService.hide();
  //       });
  //     });
  //   });
  // }
  //
  //
  //
  // updateEvents() {
  //   this.events = this.getCalendarEvents();
  //   this.myCalendar.fullCalendar('option', 'dayRender', this.dayRender.bind(this));
  //   this.myCalendar.fullCalendar('removeEventSources');
  //   this.myCalendar.fullCalendar('addEventSource', this.events);
  //   this.myCalendar.fullCalendar('render');
  // }
  //
  // getCalendarEvents() {
  //   this.events = this.userWorkCalendars.map((uwc) => {
  //     let e = {};
  //     e['title'] = (uwc.clock_profile && uwc.clock_profile.pk) ? uwc.clock_profile.__str__ : "-";
  //     e['date'] = uwc.date;
  //     e['allDay'] = true;
  //     e['color'] = (uwc.clock_profile && uwc.clock_profile.pk) ? uwc.clock_profile.color : "white";
  //     e['textColor'] = (uwc.clock_profile && uwc.clock_profile.pk) ? "white" : "black";
  //     e['borderColor'] = (uwc.clock_profile && uwc.clock_profile.pk) ? uwc.clock_profile.color : "black";
  //     return e;
  //   });
  //   return this.events;
  // }
  //
  // getProfileColor(cp: ClockProfile) {
  //   return this.domSanitizer.bypassSecurityTrustStyle("background-color: " + cp.color);
  // }
  //
  // handleSelection(start, end) {
  //   let startDate = start.format("YYYY-MM-DD");
  //   let endDateSelection = end.format("YYYY-MM-DD");
  //   let d = new Date(endDateSelection);
  //   d.setDate(d.getDate() - 1);
  //   let end_ = $.fullCalendar['moment'](d);
  //   let endDate = end_.format("YYYY-MM-DD");
  //   let periodToEdit = new UserWorkCalendarPeriod();
  //   periodToEdit.clock_profile = new ClockProfile();
  //   periodToEdit.workday = true;
  //   periodToEdit.start = startDate;
  //   periodToEdit.end = endDate;
  //   let dates = makeDates(periodToEdit.start, periodToEdit.end);
  //   dates = dates.map((d) => {
  //     let d_ = $.fullCalendar['moment'](d);
  //     return d_.format("YYYY-MM-DD");
  //   });
  //   periodToEdit.uwc = this.userWorkCalendars.filter((uwc) => {
  //     return dates.indexOf(uwc.date) != -1
  //   });
  //
  //   let workdays_selection = arrayDistinct(periodToEdit.uwc.map((uwc)=>uwc.workday));
  //   if(workdays_selection.length==1){
  //     periodToEdit.workday = workdays_selection[0];
  //   }
  //   else {
  //     periodToEdit.workday = true;
  //   }
  //   this.periodToEdit = periodToEdit;
  // }
  //
  // saveEditPeriod() {
  //   this.periodToEdit.uwc.forEach((uwc) => {
  //     uwc.workday = this.periodToEdit.workday;
  //     if(this.periodToEdit.workday){
  //       uwc.clock_profile = this.periodToEdit.clock_profile;
  //     }
  //     else {
  //       uwc.clock_profile = new ClockProfile();
  //     }
  //     this.userWorkCalendars.forEach((uwc_,index)=>{
  //       if(uwc_.date==uwc.date){
  //         this.userWorkCalendars[index] = uwc;
  //         this.userWorkCalendars[index].toSave = true;
  //       }
  //     })
  //   });
  //   this.myCalendar.fullCalendar('unselect');
  //   this.periodToEdit = null;
  //   this.hasDataToSave = true;
  //   this.myCalendar.fullCalendar('option', 'header', {right:''});
  //   this.updateEvents();
  // }
  //
  // fixClockProfile() {
  //   fixValue(this.clockProfiles, this.periodToEdit, 'clock_profile');
  // }
  //
  // undoEditPeriod(){
  //   this.myCalendar.fullCalendar('unselect');
  // }
  //
  // saveData(){
  //   let uwcToSave = this.userWorkCalendars.filter((uwc)=>uwc.toSave);
  //   this.loadingService.show();
  //   this.processUWCToSave(uwcToSave,0, function(){
  //     this.loadingService.hide();
  //     let self = this;
  //     this.messageService.open({
  //       message: this.tr('SAVE_COMPLETED'),
  //       title: " ",
  //       autoClose: false,
  //       onClose: function () {
  //         self.myCalendar.fullCalendar('option', 'header', {right:'prev,next'});
  //         self.hasDataToSave = false;
  //         self.processEvents();
  //       }
  //     });
  //   })
  // }
  //
  // processUWCToSave(uwcToSave:UserWorkCalendar[],index, callback){
  //   if(uwcToSave[index]){
  //     this.userWorkCalendarService.saveUserWorkCalendar(uwcToSave[index]).subscribe((resp)=>{
  //       this.manageRespSave(resp,function(){
  //         this.processUWCToSave(uwcToSave,index+1, callback);
  //       })
  //     })
  //   }
  //   else {
  //     callback.call(this);
  //   }
  // }
  //
  // manageRespSave(resp, callback) {
  //   if (resp.pk) {
  //     callback.call(this);
  //   }
  //   else {
  //     resp.context = "SERIONET";
  //     this.messageService.open({
  //       title: 'Error',
  //       error: true,
  //       message: buildErrorMessage(resp),
  //       autoClose: false
  //     });
  //   }
  // }
  //
  // showCPDetails(cp:ClockProfile){
  //   this.profileToShow = cloneObject(cp);
  //   this.cpDialog.open();
  // }
}

