import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesUserCalendarComponent} from "./hr-employees-user-calendar.component";
// import {CalendarComponent} from "angular2-fullcalendar/src/calendar/calendar";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesUserCalendarComponent],   // components and directives
  exports:[HrEmployeesUserCalendarComponent],
  providers: []                    // services
})
export class HrEmployeesUserCalendarModule { }



