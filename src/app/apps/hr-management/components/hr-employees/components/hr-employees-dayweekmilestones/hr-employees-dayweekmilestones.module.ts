import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesDayWeeksMilestonesComponent} from "./hr-employees-dayweekmilestones.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesDayWeeksMilestonesComponent],   // components and directives
  exports:[HrEmployeesDayWeeksMilestonesComponent],
  providers: []                    // services
})
export class HrEmployeesDayWeekMilestonesModule { }


