import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject, fixValue, getUrlDate, parseSerionetRespToData} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Title} from "@angular/platform-browser";
import {DayWeekProfile, DayWeekProfileService} from "../../../../../_common/services/day-week-profile-service";
import {DayWeekUserMilestone, DayWeekUserMilestoneService} from "../../../../../_common/services/day-week-user-milestone-service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-day-week-milestones',
  templateUrl: 'hr-employees-dayweekmilestones.component.html',
  styleUrls: ['hr-employees-dayweekmilestones.component.css']
})

export class HrEmployeesDayWeeksMilestonesComponent extends BaseComponent implements OnInit {

  @ViewChild('milestonesTable') milestonesTable: TableWithPagesComponent;

  userId: number;
  dayWeekProfiles: DayWeekProfile[] = [];

  milestonesActive = true;
  noMilestonesMessage = this.tr("NO_MILESTONES");
  headerConfMilestones = [
    {
      label: this.tr("DATE"),
      valueKey: 'start_time'
    },
    {
      label: this.tr("PROFILE"),
      valueKey: 'day_week_profile',
      useStr: true
    }
  ];
  availableActionsMilestones = [];
  milestoneInEdit: DayWeekUserMilestone;
  milestoneEditOn = false;

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private titleService: Title,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private dayWeekProfileService: DayWeekProfileService,
              private dayWeekUserMilestoneService: DayWeekUserMilestoneService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.userId = parseInt(params['id'], 10);
      }
    });

    this.dayWeekProfileService.getDayWeekProfiles({is_active: true}).subscribe((data) => {
      this.dayWeekProfiles = parseSerionetRespToData(data);
    });

    this.initTableActions();
  }

  initTableActions() {

    let actionEditMilestone: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditMilestone.click = this.editMilestone.bind(this);
    actionEditMilestone.visibilityCondition = (item: DayWeekUserMilestone) => {
      return item.is_active;
    };

    let actionDeleteMilestone: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteMilestone.click = this.deleteMilestone.bind(this);
    actionDeleteMilestone.visibilityCondition = (item: DayWeekUserMilestone) => {
      return item.is_active;
    };

    let actionRestoreMilestone: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreMilestone.click = this.deleteMilestone.bind(this);
    actionRestoreMilestone.visibilityCondition = (item: DayWeekUserMilestone) => {
      return !item.is_active;
    };

    this.availableActionsMilestones = [actionEditMilestone, actionDeleteMilestone, actionRestoreMilestone];
  }

  getMilestones(page: number, results: number) {
    let query = {};
    query['is_active'] = this.milestonesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;
    query['user'] = this.userId;
    return this.dayWeekUserMilestoneService.getDayWeekUserMilestones(query);
  }

  addMilestone() {
    this.milestoneInEdit = new DayWeekUserMilestone();
    this.milestoneInEdit.user = {pk: this.userId, __str__: ""};
    this.milestoneInEdit.day_week_profile = new DayWeekProfile();
    this.milestoneInEdit.start_time = getUrlDate(new Date(), "-");
    this.milestoneEditOn = true;
  }

  editMilestone(m: DayWeekUserMilestone) {
    this.milestoneInEdit = cloneObject(m);
    this.milestoneEditOn = true;
  }

  deleteMilestone(m: DayWeekUserMilestone) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.dayWeekUserMilestoneService.deleteDayWeekUserMilestone(m).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.milestonesTable.reload();
          })
        })
      },
      deny: () => {
      }
    });
  }

  restoreMilestone(m: DayWeekUserMilestone) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.dayWeekUserMilestoneService.restoreDayWeekUserMilestone(m).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.milestonesActive = true;
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveMilestone() {
    this.dayWeekUserMilestoneService.deleteDayWeekUserMilestone(this.milestoneInEdit).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.milestonesTable.reload();
        this.milestoneInEdit = new DayWeekUserMilestone();
        this.milestoneEditOn = false;
      })
    });
  }

  undoEditMilestone() {
    this.milestoneEditOn = false;
    this.milestoneInEdit = new DayWeekUserMilestone();
  }

  fixDWP() {
    fixValue(this.dayWeekProfiles, this.milestoneInEdit, 'day_week_profile')
  }

}
