import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../../_common/services/has-company-position.service";
import {StatusLink, StatusLinkService} from "../../../../../_common/services/status-link.service";
import {buildErrorMessage, parseSerionetRespToData} from "../../../../../../shared/utils";
import {TranslatableComponent} from "../../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'hr-employees-positions',
  templateUrl: 'hr-employees-positions.component.html',
  styleUrls: ['hr-employees-positions.component.css']
})

export class HrEmployeesPositionsComponent extends TranslatableComponent implements OnInit {

  noItemsMessage = this.tr("NO_AVAILABLE_ACTIVE_POSITIONS");
  availableActions = [];
  headerConf = [
    {
      label: 'Position',
      valueKey: 'company_position',
      useStr: true
    }
  ];
  ready = false;
  userPK = null;
  staffPk = null;

  constructor(private activatedRoute: ActivatedRoute, private statusLinkService: StatusLinkService,
              private messageService: MessageService,
              private loadingService: LoadingService,
              protected translationService: TranslationService,
              private hasCompanyPositionService: HasCompanyPositionService) {
    super(translationService);
  }

  ngOnInit() {
    this.statusLinkService.getStatusLinks({app__model: 'hascompanyposition', is_active: true}).subscribe((data) => {
      let statuses: StatusLink[] = parseSerionetRespToData(data);
      this.staffPk = statuses.find((sl) => sl.delta_rel == 2).pk;
      this.activatedRoute.params.subscribe((params) => {
        if (params['id']) {
          this.userPK = params['id'];
          this.ready = true;
        }
      });
    });
  }

  loadCurrentPositions(page: number, results: number) {
    let query = {
      is_active: true,
      user_ref: this.userPK,
      status_list: this.staffPk,
      page: page,
      results: results
    };
    return this.hasCompanyPositionService.getHasCompanyPositions(query);
  }

  deleteSelectedCP(hcps: HasCompanyPosition[], table: TableWithPagesComponent) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_CLOSE_POSITIONS'),
      autoClose: false,
      confirm: () => {
        let count = hcps.length;
        let cb = () => {
          count--;
          if (count == 0) {
            this.loadingService.hide();
            table.reload();
          }
        };

        this.loadingService.show();

        hcps.forEach((hcp) => {
          this.hasCompanyPositionService.saveHasCompanyPosition(hcp, {run_quit_flow: true}).subscribe((resp) => {
            if (resp.pk) {
              cb();
            }
            else {
              this.loadingService.hide();
              resp.context = "SERIONET";
              this.messageService.open({
                title: 'Error',
                error: true,
                message: buildErrorMessage(resp),
                autoClose: false
              });
            }
          });
        })
      },
      deny: () => {

      }
    });
  }
}
