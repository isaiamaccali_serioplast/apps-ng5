import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesPositionsComponent} from "./hr-employees-positions.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesPositionsComponent],   // components and directives
  exports:[HrEmployeesPositionsComponent],
  providers: []                    // services
})
export class HrEmployeesPositionsModule { }


