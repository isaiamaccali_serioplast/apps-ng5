import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {CountriesService, Country} from "../../../../../_common/services/countries.service";
import {Education, EducationService} from "../../../../../_common/services/education.service";
import {Title} from "@angular/platform-browser";
import {EducationType, EducationTypesService} from "../../../../../_common/services/education-type.service";
import {EducationStudyField, EducationStudyFieldService} from "../../../../../_common/services/education-study-fields.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-educations',
  templateUrl: 'hr-employees-educations.component.html',
  styleUrls: ['hr-employees-educations.component.css']
})

export class HrEmployeesEducationsComponent extends BaseComponent implements OnInit {

  user_id: string;
  // current model
  education: Education = new Education();

  // related model or custom list
  countries: Country[] = [];
  educationsTypes: EducationType[] = [];
  educationStudyFields: EducationStudyField[] = [];

  // table/form settings
  onCreate: boolean;
  educationCreateOpen: boolean;
  availableActions = [];
  @ViewChild('educationsTable') educationsTable: TableWithPagesComponent;
  noItemsMessageEducations = this.tr("No educations");
  headerConfEducations = [
    {
      label: this.tr("Date From"),
      valueKey: 'date_from'
    },
    {
      label: this.tr("Date to"),
      valueKey: 'date_to'
    },
    {
      label: this.tr("Institution"),
      valueKey: 'institution',
    },
    {
      label: this.tr("Degree"),
      valueKey: 'degree',
      useStr: true,
    },
    {
      label: this.tr("Study field"),
      valueKey: 'study_field',
      useStr: true,
    },
    {
      label: this.tr("Final grade"),
      valueKey: 'final_grade'
    },
    {
      label: this.tr("Country"),
      valueKey: 'country',
    },
    {
      label: this.tr("City"),
      valueKey: 'city'
    },
    {
      label: this.tr("Description"),
      valueKey: 'description'
    },
  ];

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private educationservice: EducationService,
              private educationtypeservice: EducationTypesService,
              private educationstudyfieldservice: EducationStudyFieldService,
              private countryService: CountriesService,
              private titleService: Title,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.onCreate = false;
    this.educationCreateOpen = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.user_id = params['id'];
        this.loadCountries();
        this.loadEducationsTypes();
        this.loadEducationStudyFields();
        this.initTableActions();
      }
    });
  }

  loadCountries() {
    this.countryService.getCountries({}).subscribe((data) => {

        for (let i = 0; i < data.length; i++) {
          let country = new Country();
          country.value = data[i][0];
          country.label = data[i][1];
          this.countries[i] = country;
        }
        // Remove empty label
        this.countries.shift();
      }
    )
  }

  loadEducationsTypes() {
    let query = {};
    query['is_active'] = true;

    this.educationtypeservice.getEducationTypes(query).subscribe((data) => {
      this.educationsTypes = parseSerionetRespToData(data);
    });
  }

  loadEducationStudyFields() {
    let query = {};
    query['is_active'] = true;

    this.educationstudyfieldservice.getEducationStudyfields(query).subscribe((data) => {
      this.educationStudyFields = parseSerionetRespToData(data);
    });
  }

  initTableActions() {

    let actionEditEducation: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditEducation.click = this.editEducation.bind(this);

    let actionDeleteEducation: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteEducation.click = this.deleteEducation.bind(this);
    actionDeleteEducation.visibilityCondition = (item: Education) => {
      return item.is_active;
    };

    /*
     let actionRestoreEducation: TableRowAction = cloneObject(TableRowActions.RESTORE);
     actionRestoreEducation.click = this.restoreEducation.bind(this);
     actionRestoreEducation.visibilityCondition = function (item: Education) {
     return !item.is_active;
     };
     */
    this.availableActions = [actionEditEducation, actionDeleteEducation];
  }

  /**
   * Educations
   */

  loadEducations() {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.user_id;
    return this.educationservice.getEducations(query);
  }

  editEducation(pe: Education) {
    this.education = cloneObject(pe);
    this.educationCreateOpen = true;
    this.onCreate = false;
  }

  deleteEducation(pe: Education) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.educationservice.deleteEducation(pe).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.educationsTable.reload();
          });
        })
      },
      deny: () => {
      }
    });
  }

  createEducation() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.education = new Education();
        this.education.user_id = params['id'];
        this.education.degree = new EducationType();
        this.education.study_field = new EducationStudyField();
        this.educationCreateOpen = true;
        this.onCreate = true;
      }
    });
  }

  saveEducation() {
    let displayMsg = false;
    if (this.education.is_active == false) {
      displayMsg = true;
    }
    // save language
    this.educationservice.saveEducation(this.education).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.education = resp;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.educationCreateOpen = false;
        this.educationsTable.reload();
      });
    });

  }

  undoEducation() {
    this.createEducation();
    this.educationCreateOpen = false;
  }

  /**
   * Other
   */

  getAddress(place: any) {
    this.education.city = place['formatted_address'];
  }

}
