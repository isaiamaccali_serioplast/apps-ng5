import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesProfessionalExperiencesComponent} from "./hr-employees-profexps.component";
import {HrEmployeesEducationsComponent} from "./hr-employees-educations.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesEducationsComponent],   // components and directives
  exports:[HrEmployeesEducationsComponent],
  providers: []                    // services
})
export class HrEmployeesEducationsModule { }


