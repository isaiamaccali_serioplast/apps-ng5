import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject, parseSerionetRespToData} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {CountriesService, Country} from "../../../../../_common/services/countries.service";
import {ProfessionalExperience, ProfessionalExperiencesService} from "../../../../../_common/services/professional-experiences.service";
import {PEFunction, PEFunctionService} from "../../../../../_common/services/pefunction.service";
import {PEIndustry, PEIndustryService} from "../../../../../_common/services/peindustry.service";
import {Title} from "@angular/platform-browser";
import {ContractType, ContractTypeService} from "../../../../../_common/services/contract-type.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-profexps',
  templateUrl: 'hr-employees-profexps.component.html',
  styleUrls: ['hr-employees-profexps.component.css']
})

export class HrEmployeesProfessionalExperiencesComponent extends BaseComponent implements OnInit {

  user_id: string;

  // current model
  professionalExperience: ProfessionalExperience = new ProfessionalExperience();

  // related model or custom list
  countries: Country[] = [];
  pefunctions: PEFunction[] = [];
  peindustries: PEIndustry[] = [];
  contract_types: ContractType[] = [];

  // table/form settings
  onCreate: boolean;
  professionalExperienceCreateOpen: boolean;
  availableActions = [];
  @ViewChild('professionalExperiencesTable') professionalExperiencesTable: TableWithPagesComponent;
  noItemsMessageprofessionalExperiences = this.tr("No professional experiences.");
  headerConfprofessionalExperiences = [
    {
      label: this.tr("Company"),
      valueKey: 'company_name'
    },
    {
      label: this.tr("Date From"),
      valueKey: 'date_from'
    },
    {
      label: this.tr("Date to"),
      valueKey: 'date_to'
    },
    {
      label: this.tr("Job Title"),
      valueKey: 'job_title'
    },

    {
      label: this.tr("Country"),
      valueKey: 'country',
    },

    {
      label: this.tr("City"),
      valueKey: 'city'
    },
    {
      label: this.tr("Function"),
      valueKey: 'function',
      useStr: true,
    },
    {
      label: this.tr("Industry"),
      valueKey: 'industry',
      useStr: true,
    },
    {
      label: this.tr("Contract Type"),
      valueKey: 'contract_type',
      useStr: true,
    },
  ];

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private professionalExperienceService: ProfessionalExperiencesService,
              private countryService: CountriesService,
              private pefunctionService: PEFunctionService,
              private peindustryService: PEIndustryService,
              private contractTypeService: ContractTypeService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private titleService: Title,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.onCreate = false;
    this.professionalExperienceCreateOpen = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.user_id = params['id'];
        this.loadCountries();
        this.loadPEfunctions();
        this.loadPEindustries();
        this.loadContractTypes();
        this.initTableActions();
      }
    });
  }

  loadCountries() {
    this.countryService.getCountries({}).subscribe((data) => {

        for (let i = 0; i < data.length; i++) {
          let country = new Country();
          country.value = data[i][0];
          country.label = data[i][1];
          this.countries[i] = country;
        }
        // Remove empty label
        this.countries.shift();
      }
    )
  }

  loadPEfunctions() {
    let query = {};
    query['is_active'] = true;
    query['ordering'] = 'label';

    this.pefunctionService.getPEFunctions(query).subscribe((data) => {
      this.pefunctions = parseSerionetRespToData(data);
    })
  }

  loadPEindustries() {
    let query = {};
    query['is_active'] = true;
    query['ordering'] = 'label';

    this.peindustryService.getPEIndustries(query).subscribe((data) => {
      this.peindustries = parseSerionetRespToData(data);
    })
  }

  loadContractTypes() {
    let query = {};
    query['is_active'] = true;
    query['ordering'] = 'organization_ref';

    this.contractTypeService.getContractTypes(query).subscribe((data) => {
      this.contract_types = parseSerionetRespToData(data);
    })
  }

  initTableActions() {

    let actionEditProfessionalExperience: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditProfessionalExperience.click = this.editProfessionalExperience.bind(this);

    let actionDeleteProfessionalExperience: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteProfessionalExperience.click = this.deleteProfessionalExperience.bind(this);
    actionDeleteProfessionalExperience.visibilityCondition = (item: ProfessionalExperience) => {
      return item.is_active;
    };

    this.availableActions = [actionEditProfessionalExperience, actionDeleteProfessionalExperience];
  }

  /**
   * ProfessionalExperiences
   */

  loadProfessionalExperiences() {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.user_id;
    return this.professionalExperienceService.getProfessionalExperiences(query);
  }

  editProfessionalExperience(pe: ProfessionalExperience) {
    this.professionalExperience = cloneObject(pe);
    this.professionalExperienceCreateOpen = true;
    this.onCreate = false;
  }

  deleteProfessionalExperience(pe: ProfessionalExperience) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.professionalExperienceService.deleteProfessionalExperience(pe).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.professionalExperiencesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  createProfessionalExperience() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.professionalExperience = new ProfessionalExperience();
        this.professionalExperience.user_id = params['id'];
        this.professionalExperience.contract_type = new ContractType();
        this.professionalExperience.function = new PEFunction();
        this.professionalExperience.industry = new PEIndustry();
        this.professionalExperienceCreateOpen = true;
        this.onCreate = true;
      }
    });
  }

  saveProfessionalExperience() {

    let displayMsg = false;
    if (this.professionalExperience.is_active == false) {
      displayMsg = true;
    }
    // save language
    this.professionalExperienceService.saveProfessionalExperience(this.professionalExperience).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.professionalExperience = resp;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.professionalExperienceCreateOpen = false;
        this.professionalExperiencesTable.reload();
      });
    });

  }

  undoProfessionalExperience() {
    this.createProfessionalExperience();
    this.professionalExperienceCreateOpen = false;
  }

  /**
   * Other
   */

  getAddress(place: any) {
    this.professionalExperience.city = place['formatted_address'];
  }

}
