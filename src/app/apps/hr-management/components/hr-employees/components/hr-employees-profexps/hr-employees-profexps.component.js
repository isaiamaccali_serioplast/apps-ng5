"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var utils_1 = require("../../../../shared/utils");
var HrEmployeesViewComponent = (function () {
    function HrEmployeesViewComponent(activatedRoute, translationservice, employeeservice, messageservice) {
        this.activatedRoute = activatedRoute;
        this.translationservice = translationservice;
        this.employeeservice = employeeservice;
        this.messageservice = messageservice;
    }
    HrEmployeesViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.employeeservice.getEmployeesProfile({ 'user_id': params['id'] }).subscribe(function (hrprofile) {
                    _this.hrprofile = hrprofile['results'][0];
                });
            }
        });
    };
    HrEmployeesViewComponent.prototype.handleSaveOrUpdate = function (resp, callback) {
        var self = this;
        if (resp.pk) {
            self.messageservice.open({
                message: self.tr('SAVE_COMPLETED'),
                title: " ",
                autoClose: false,
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageservice.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    HrEmployeesViewComponent.prototype.tr = function (value) {
        return this.translationservice.translate(value);
    };
    return HrEmployeesViewComponent;
}());
HrEmployeesViewComponent = __decorate([
    core_1.Component({
        selector: 'organization-view',
        templateUrl: 'hr-employees-languages.component.html',
        styleUrls: ['hr-employees-languages.component.css']
    })
], HrEmployeesViewComponent);
exports.HrEmployeesViewComponent = HrEmployeesViewComponent;
