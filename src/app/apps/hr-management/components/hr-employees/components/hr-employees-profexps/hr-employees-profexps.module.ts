import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesProfessionalExperiencesComponent} from "./hr-employees-profexps.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesProfessionalExperiencesComponent],   // components and directives
  exports:[HrEmployeesProfessionalExperiencesComponent],
  providers: []                    // services
})
export class HrEmployeesProfessionalExperiencesModule { }


