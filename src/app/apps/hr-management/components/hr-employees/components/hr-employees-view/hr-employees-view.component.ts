import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {HRProfile, HRProfileService} from "../../../../../_common/services/hr-profiles.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar, MatTabGroup} from "@angular/material";
import {DjangoTimezoneService} from "../../../../../_common/services/django-timezones.service";
import {Title} from "@angular/platform-browser";
import {ContentTypeService} from "../../../../../_common/services/content-type.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-view',
  templateUrl: 'hr-employees-view.component.html',
  styleUrls: ['hr-employees-view.component.css']
})

export class HrEmployeesViewComponent extends BaseComponent implements OnInit {
  hrprofile: HRProfile = new HRProfile();
  @ViewChild('avatar') avatarInput: HTMLInputElement;
  @ViewChild('tab') tab: MatTabGroup;
  profileCT: number;

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private employeeservice: HRProfileService,
              private djangotimezoneservice: DjangoTimezoneService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private titleService: Title,
              private  snackBar: MatSnackBar,
              private contentTypeService: ContentTypeService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {
    this.init();
  }

  init() {

    this.contentTypeService.getContentTypes({model: 'profile', is_active: true}).subscribe((resp) => {
      if (resp && resp.count > 0) {
        this.profileCT = resp.results[0].pk;
      }
    });

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {

        this.employeeservice.getHrProfile(params['id']).subscribe((resp) => {
          this.hrprofile = resp;
        });

      }
    });
  }

  loadDjangoTimezones(keyword?: string) {
    let query = {};

    if (keyword) {
      query['search'] = keyword;
    }

    query['results'] = 5;

    return this.djangotimezoneservice.getDjangoTimezones(query);
  }

  selectedTimezone(timezone: any) {
    if (timezone) {
      this.hrprofile.timezone = timezone;
    }
    else {
      this.hrprofile.timezone = null;
    }
  }

  updateHrProfile() {
    let displayMsg = false;

    this.employeeservice.saveHrProfile(this.hrprofile).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  showCalendar = false;

  selectTabChange() {
    this.showCalendar = this.tab.selectedIndex == 3;
  }
}
