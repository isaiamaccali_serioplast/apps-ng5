import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesViewComponent} from "./hr-employees-view.component";
import {HrEmployeesLanguagesModule} from "../hr-employees-languages/hr-employees-languages.module";
import {HrEmployeesProfessionalExperiencesModule} from "../hr-employees-profexps/hr-employees-profexps.module";
import {HrEmployeesEducationsModule} from "../hr-employees-educations/hr-employees-educations.module";
import {HrEmployeesNationalitiesModule} from "../hr-employees-nationalities/hr-employees-nationalities.module";
import {HrEmployeesPersonalInfoModule} from "../hr-employees-personalinfo/hr-employees-personalinfo.module";
import {HrEmployeesWorkingContactsModule} from "../hr-employees-workingcontacts/hr-employees-workingcontacts.module";
import {HrEmployeesBadgesModule} from "../hr-employees-badges/hr-employees-badges.module";
import {HrEmployeesContractsModule} from "../hr-employees-contracts/hr-employees-contracts.module";
import {HrEmployeesUserCalendarModule} from "../hr-employees-user-calendar/hr-employees-user-calendar.module";
import {HrEmployeesDayWeekMilestonesModule} from "../hr-employees-dayweekmilestones/hr-employees-dayweekmilestones.module";
import {HrEmployeesUserClockProfilesModule} from "../hr-employees-user-clock-profiles/hr-employees-user-clock-profiles.module";
import {HrEmployeesPositionsModule} from "../hr-employees-positions/hr-employees-positions.module";

@NgModule({
  imports: [
    SharedModule,
    HrEmployeesPersonalInfoModule,
    HrEmployeesWorkingContactsModule,
    HrEmployeesLanguagesModule,
    HrEmployeesProfessionalExperiencesModule,
    HrEmployeesEducationsModule,
    HrEmployeesNationalitiesModule,
    HrEmployeesBadgesModule,
    HrEmployeesContractsModule,
    HrEmployeesUserCalendarModule,
    HrEmployeesDayWeekMilestonesModule,
    HrEmployeesUserClockProfilesModule,
    HrEmployeesPositionsModule
  ],       // module dependencies
  declarations: [HrEmployeesViewComponent],   // components and directives
  exports: [HrEmployeesViewComponent],
  providers: []                    // services
})
export class HrEmployeesViewModule {
}


