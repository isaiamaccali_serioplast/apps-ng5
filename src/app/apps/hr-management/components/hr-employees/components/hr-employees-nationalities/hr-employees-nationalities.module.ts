import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesNationalitiesComponent} from "./hr-employees-nationalities.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesNationalitiesComponent],   // components and directives
  exports:[HrEmployeesNationalitiesComponent],
  providers: []                    // services
})
export class HrEmployeesNationalitiesModule { }


