import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject} from "../../../../../../shared/utils";
import {MatSnackBar} from "@angular/material";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {CountriesService, Country} from "../../../../../_common/services/countries.service";
import {Nationality, NationalityService} from "../../../../../_common/services/nationality.service";
import {Title} from "@angular/platform-browser";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-nationalities',
  templateUrl: 'hr-employees-nationalities.component.html',
  styleUrls: ['hr-employees-nationalities.component.css']
})

export class HrEmployeesNationalitiesComponent extends BaseComponent implements OnInit {

  user_id: string;

  // current model
  nationality: Nationality = new Nationality();

  // related model or custom list
  countries: Country[] = [];

  // table/form settings
  onCreate: boolean;
  nationalityCreateOpen: boolean;
  availableActions = [];
  @ViewChild('nationalitiesTable') nationalitiesTable: TableWithPagesComponent;
  noItemsMessageNationalities = this.tr("No nationalities");
  headerConfNationalities = [
    {
      columnId: 'nationality',
      label: this.tr("Nationality"),
      valueKey: '__str__',
    },
    {
      columnId: 'country_id',
      label: this.tr("Country ID Number"),
      valueKey: 'country_id_number'
    },
    {
      columnId: 'country_tax',
      label: this.tr("Country Tax Number"),
      valueKey: 'country_tax_number'
    },
  ];

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private nationalityservice: NationalityService,
              private countryService: CountriesService,
              protected messageService: MessageService,
              private titleService: Title,
              private  snackBar: MatSnackBar,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.onCreate = false;
    this.nationalityCreateOpen = false;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.user_id = params['id'];
        this.loadCountries();
        this.initTableActions();
      }
    });
  }

  loadCountries() {
    this.countryService.getCountries({}).subscribe((data) => {
        this.countries = data;
      }
    )
  }

  initTableActions() {

    let actionEditNationality: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditNationality.click = this.editNationality.bind(this);

    let actionDeleteNationality: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteNationality.click = this.deleteNationality.bind(this);
    actionDeleteNationality.visibilityCondition = (item: Nationality) => {
      return item.is_active;
    };

    /*
     let actionRestoreNationality: TableRowAction = cloneObject(TableRowActions.RESTORE);
     actionRestoreNationality.click = this.restoreNationality.bind(this);
     actionRestoreNationality.visibilityCondition = function (item: Nationality) {
     return !item.is_active;
     };
     */
    this.availableActions = [actionEditNationality, actionDeleteNationality];
  }

  loadNationalities() {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.user_id;
    return this.nationalityservice.getNationalities(query);

  }

  editNationality(pe: Nationality) {
    this.nationality = cloneObject(pe);
    this.nationalityCreateOpen = true;
    this.onCreate = false;
  }

  deleteNationality(pe: Nationality) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.nationalityservice.deleteNationality(pe).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.nationalitiesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  createNationality() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.nationality = new Nationality();
        this.nationality.user_id = params['id'];
        this.nationalityCreateOpen = true;
        this.onCreate = true;
      }
    });
  }

  saveNationality() {
    let displayMsg = false;
    if (this.nationality.is_active == false) {
      displayMsg = true;
    }
    // save language
    this.nationalityservice.saveNationality(this.nationality).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.nationality = resp;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.nationalityCreateOpen = false;
        this.nationalitiesTable.reload();
      });
    });

  }

  undoNationality() {
    this.createNationality();
    this.nationalityCreateOpen = false;
  }

}
