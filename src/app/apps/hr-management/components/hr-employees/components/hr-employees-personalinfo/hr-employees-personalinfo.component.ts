import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {parseSerionetRespToData} from "../../../../../../shared/utils";
import {PersonalInfo, PersonalInfoService} from "../../../../../_common/services/personalinfo.service";
import {Sex, SexService} from "../../../../../_common/services/sex.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar} from "@angular/material";
import {CountriesService, Country} from "../../../../../_common/services/countries.service";
import {FileService, SPFile} from "../../../../../_common/services/file.service";
import {Title} from "@angular/platform-browser";
import {ContentTypeService} from "../../../../../_common/services/content-type.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-personalinfo',
  templateUrl: 'hr-employees-personalinfo.component.html',
  styleUrls: ['hr-employees-personalinfo.component.css']
})

export class HrEmployeesPersonalInfoComponent extends BaseComponent implements OnInit {
  personalinfo: PersonalInfo;
  @ViewChild('cv') cvInput: HTMLInputElement;

  // related model or custom list
  countries: Country[] = [];
  sex_list: Sex[] = [];
  personalInfoCT: number;

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private personalinfoservice: PersonalInfoService,
              private fileService: FileService,
              private countryService: CountriesService,
              private sexservice: SexService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private titleService: Title,
              private snackBar: MatSnackBar,
              private contentTypeService: ContentTypeService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.loadCountries();
    this.loadSex();

    this.contentTypeService.getContentTypes({model: 'personalinfo', is_active: true}).subscribe((resp) => {
      if (resp && resp.count > 0) {
        this.personalInfoCT = resp.results[0].pk;
      }
    });

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.loadPersonalInfo(params['id']);
      }
    });
  }

  loadCountries() {
    this.countryService.getCountries({}).subscribe((data) => {
        this.countries = data;
      }
    )
  }

  loadSex() {
    this.sexservice.getSex({is_active: true}).subscribe((data) => {
      this.sex_list = parseSerionetRespToData(data)
    });
  }

  loadPersonalInfo(id: number) {
    this.loadingService.show();
    this.personalinfoservice.getPersonalInfo(id).subscribe((personalinfo) => {
      this.personalinfo = personalinfo;
      this.loadingService.hide();
    });
  }

  getAddress(place: any) {
    this.personalinfo.home_address = place['formatted_address'];
  }

  updatePersonalInfo() {
    let displayMsg = false;

    this.personalinfoservice.updatePersonalInfo(this.personalinfo).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });

      this.init();
    })
  }

  uploadCvPersonalInfo() {
    this.loadingService.show();
    let f = new SPFile();
    f.field_name = 'cv';
    f.content_type = this.personalInfoCT;
    this.fileService.saveFile(f, this.cvInput['nativeElement']).subscribe((data) => {
      this.personalinfo.cv = data;
      this.loadingService.hide();
    })
  }

  deleteCvPersonalInfo() {
    this.personalinfo.cv = null;
  }

}
