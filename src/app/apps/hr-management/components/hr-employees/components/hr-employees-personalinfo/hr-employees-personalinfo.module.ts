import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesPersonalInfoComponent} from "./hr-employees-personalinfo.component";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesPersonalInfoComponent],   // components and directives
  exports: [HrEmployeesPersonalInfoComponent],
  providers: []                    // services
})
export class HrEmployeesPersonalInfoModule {
}


