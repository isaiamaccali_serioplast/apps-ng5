import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {Router} from "@angular/router";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {HRProfile, HRProfileService} from "../../../../../_common/services/hr-profiles.service";
import {cloneObject} from "../../../../../../shared/utils";
import {LoginService} from "../../../../../../shared/login/service/login.service";
import {Title} from "@angular/platform-browser";
import {MatSnackBar} from "@angular/material";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-list',
  templateUrl: 'hr-employees-list.component.html',
  styleUrls: ['hr-employees-list.component.css']
})

export class HrEmployeesListComponent extends BaseComponent implements OnInit {
  hrprofiles: HRProfile[];
  hrprofileFilter: HRProfile = new HRProfile();
  nameTimeout: any;
  availableActions = [];
  @ViewChild('employeesTable') employeesTable: TableWithPagesComponent;
  noItemsMessageEmployees = this.tr("NO_EMPLOYEES");
  headerConfhremployees = [
    {
      columnId: 'name',
      label: this.tr("NAME"),
      valueFunction: (item: HRProfile)=> {
        return "<img class='circle' style='height: 30px;' src=" + item.avatar.__str__ + " >" + " " + item.__str__
      }
    },
    {
      columnId: 'id',
      label: this.tr("ID"),
      valueFunction: (item: HRProfile) => {
        return item.user_id.pk;
      }
    },
  ];

  constructor(private router: Router,
              private loginService: LoginService,
              private hrprofilesService: HRProfileService,
              protected translationService: TranslationService,
              private titleService: Title,
              protected messageService: MessageService,
              private snackBar: MatSnackBar,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {
    this.initTableActions();
  }

  initTableActions() {
    let actionEditHrProfile: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditHrProfile.click = this.viewEmployee.bind(this);

    let actionDeleteHrProfile: TableRowAction = new TableRowAction();
    actionDeleteHrProfile.icon = "exit_to_app";
    actionDeleteHrProfile.tooltip = "Close all user positions";
    actionDeleteHrProfile.click = this.deleteHrProfile.bind(this);
    actionDeleteHrProfile.visibilityCondition = (item: HRProfile) => {
      return item.is_active;
    };

    let actionRestoreHrProfile: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreHrProfile.click = this.saveHrProfile.bind(this);
    actionRestoreHrProfile.visibilityCondition = (item: HRProfile) => {
      return !item.is_active;
    };

    this.availableActions = [actionEditHrProfile, actionDeleteHrProfile, actionRestoreHrProfile];
  }

  viewEmployee(p: HRProfile) {
    this.router.navigate(["hr-management", "employees", "detail", p.pk]);
  }

  loadEmployees(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;
    query['is_active'] = true;

    query['profiles_by_hr'] = this.loginService.user.pk;

    if (this.hrprofileFilter.__str__)
      query['full_name_icontains'] = this.hrprofileFilter.__str__;

    return this.hrprofilesService.getEmployeesProfile(query);
  }

  deleteHrProfile(hrprofile: HRProfile) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.hrprofilesService.deleteHrProfile(hrprofile).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.employeesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  saveHrProfile(hrprofile: HRProfile) {

    let displayMsg = false;
    if (hrprofile.is_active == false) {
      displayMsg = true;
    }
    let hrp = cloneObject(hrprofile);
    hrp.is_active = true;
    // save language
    this.hrprofilesService.saveHrProfile(hrp).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 2000});
        this.employeesTable.reload();
      });
    });

  }

  reset() {
    setTimeout(() => {
      this.reload();
    }, 500)
  }

  reload() {
    this.employeesTable.reload();
  }
}
