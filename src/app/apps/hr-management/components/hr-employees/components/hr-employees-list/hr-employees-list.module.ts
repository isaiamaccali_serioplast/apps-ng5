import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesListComponent} from "./hr-employees-list.component";
import {HrEmployeesViewModule} from "../hr-employees-view/hr-employees-view.module";

@NgModule({
  imports: [SharedModule,HrEmployeesViewModule],       // module dependencies
  declarations: [HrEmployeesListComponent],   // components and directives
  exports:[HrEmployeesListComponent],
  providers: []                    // services
})
export class HrEmployeesListModule { }


