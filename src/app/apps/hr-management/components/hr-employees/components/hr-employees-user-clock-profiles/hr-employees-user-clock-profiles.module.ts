import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesDayWeeksMilestonesComponent} from "./hr-employees-dayweekmilestones.component";
import {HrEmployeesUserClockProfilesComponent} from "./hr-employees-user-clock-profiles.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesUserClockProfilesComponent],   // components and directives
  exports:[HrEmployeesUserClockProfilesComponent],
  providers: []                    // services
})
export class HrEmployeesUserClockProfilesModule { }


