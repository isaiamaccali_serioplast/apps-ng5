import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {cloneObject, fixValue, parseSerionetRespToData} from "../../../../../../shared/utils";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Title} from "@angular/platform-browser";
import {ClockProfile, ClockProfileService} from "../../../../../_common/services/clock-profile.service";
import {UserClockProfile, UserClockProfileService} from "../../../../../_common/services/user-clock-profile.service";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-user-clock-profiles',
  templateUrl: 'hr-employees-user-clock-profiles.component.html',
  styleUrls: ['hr-employees-user-clock-profiles.component.css']
})

export class HrEmployeesUserClockProfilesComponent extends BaseComponent implements OnInit {

  @ViewChild('profilesTable') profilesTable: TableWithPagesComponent;

  userId: number;
  clockProfiles: ClockProfile[] = [];

  profilesActive = true;
  noItemsMessage = this.tr("NO_PROFILES");
  headerConf = [
    {
      label: this.tr("PROFILE"),
      valueKey: 'clock_profile',
      useStr: true
    }
  ];
  availableActions = [];
  profileInEdit: UserClockProfile;
  profileEditOn = false;

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private titleService: Title,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private clockProfileService: ClockProfileService,
              private userClockProfileService: UserClockProfileService) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        this.userId = parseInt(params['id'], 10);
      }
    });

    this.clockProfileService.getClockProfiles({is_active: true}).subscribe((data) => {
      this.clockProfiles = parseSerionetRespToData(data);
    });

    this.initTableActions();
  }

  initTableActions() {

    let actionEditProfile: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditProfile.click = this.editProfile.bind(this);
    actionEditProfile.visibilityCondition = (item: UserClockProfile) => {
      return item.is_active;
    };

    let actionDeleteProfile: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteProfile.click = this.deleteProfile.bind(this);
    actionDeleteProfile.visibilityCondition = (item: UserClockProfile) => {
      return item.is_active;
    };

    let actionRestoreProfile: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreProfile.click = this.restoreProfile.bind(this);
    actionRestoreProfile.visibilityCondition = (item: UserClockProfile) => {
      return !item.is_active;
    };

    this.availableActions = [actionEditProfile, actionDeleteProfile, actionRestoreProfile];
  }

  getProfiles(page: number, results: number) {
    let query = {};
    query['is_active'] = this.profilesActive;
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;
    query['user'] = this.userId;
    return this.userClockProfileService.getUserClockProfiles(query);
  }

  addProfile() {
    this.profileInEdit = new UserClockProfile();
    this.profileInEdit.user = {pk: this.userId, __str__: ""};
    this.profileInEdit.clock_profile = new ClockProfile();
    this.profileEditOn = true;
  }

  editProfile(m: UserClockProfile) {
    this.profileInEdit = cloneObject(m);
    this.profileEditOn = true;
  }

  deleteProfile(m: UserClockProfile) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.userClockProfileService.deleteUserClockProfile(m).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.profilesTable.reload();
          })
        })
      },
      deny: () => {}
    });
  }

  restoreProfile(m: UserClockProfile) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.userClockProfileService.restoreUserClockProfile(m).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.profilesActive = true;
          })
        })
      },
      deny: () => {}
    });
  }

  saveProfile() {
    this.userClockProfileService.saveUserClockProfile(this.profileInEdit).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.profilesTable.reload();
        this.profileInEdit = new UserClockProfile();
        this.profileEditOn = false;
      })
    });
  }

  undoEditProfile() {
    this.profileEditOn = false;
    this.profileInEdit = new UserClockProfile();
  }

  fixCP() {
    fixValue(this.clockProfiles, this.profileInEdit, 'clock_profile')
  }

}
