import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../../../shared/shared.module";
import {HrEmployeesViewComponent} from "./hr-employees-view.component";
import {HrEmployeesWorkingContactsComponent} from "./hr-employees-workingcontacts.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [HrEmployeesWorkingContactsComponent],   // components and directives
  exports: [HrEmployeesWorkingContactsComponent],
  providers: []                    // services
})
export class HrEmployeesWorkingContactsModule {
}


