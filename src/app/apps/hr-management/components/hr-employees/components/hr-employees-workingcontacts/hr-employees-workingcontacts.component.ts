import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../../shared/material/message/message.service";
import {WorkingContact, WorkingContactsService} from "../../../../../_common/services/workingcontacts.service";
import {LoadingService} from "../../../../../../shared/loading/loading.service";
import {MatSnackBar} from "@angular/material";
import {Title} from "@angular/platform-browser";
import {BaseComponent} from "../../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'hr-employees-workingcontacts',
  templateUrl: 'hr-employees-workingcontacts.component.html',
  styleUrls: ['hr-employees-workingcontacts.component.css']
})

export class HrEmployeesWorkingContactsComponent extends BaseComponent implements OnInit {
  workingcontacts: WorkingContact;

  constructor(private activatedRoute: ActivatedRoute,
              protected translationService: TranslationService,
              private workingcontactsservice: WorkingContactsService,
              protected messageService: MessageService,
              protected loadingService: LoadingService,
              private titleService: Title,
              private  snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {
    this.loadWorkingContacts();
  }

  loadWorkingContacts() {
    this.activatedRoute.params.subscribe((params) => {
      this.loadingService.show();
      if (params['id']) {
        this.workingcontactsservice.getWorkingContact(params['id']).subscribe((workingcontacts) => {
          this.workingcontacts = workingcontacts;
          this.loadingService.hide();
        })
      }
    });
  }

  saveWorkingContact() {
    let displayMsg = false;

    this.workingcontactsservice.saveWorkingContact(this.workingcontacts).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  getAddress(place: any) {
    if (place && place['formatted_address'])
      this.workingcontacts.work_address = place['formatted_address'];
    else
      this.workingcontacts.work_address = "";
  }

}
