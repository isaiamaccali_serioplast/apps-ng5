import {Component, OnInit, ViewChild} from "@angular/core";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Title} from "@angular/platform-browser";
import {MatTabGroup} from "@angular/material";

@Component({
  selector: 'app-packages-index',
  templateUrl: 'packages-index.component.html',
  styleUrls: ['packages-index.component.css']
})

export class PackagesIndexComponent implements OnInit {

  @ViewChild('tab') tab:MatTabGroup;
  selectedIndex = 0;

  constructor(private sideNavService: SideNavService,
              private titleService: Title
  ) {
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - HR Management");
  }

  ngOnInit() {}

  tabChange(){
    this.selectedIndex = this.tab.selectedIndex;
  }
}
