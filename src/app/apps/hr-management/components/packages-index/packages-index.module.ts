import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {PackagesIndexComponent} from "./packages-index.component";
import {LocalPackageModule} from "../local-package/local-package.module";
import {HrEmployeesListModule} from "../hr-employees/components/hr-employees-list/hr-employees-list.module";
import {BadgesManagementModule} from "../badges-management/badges-management.module";
import {HrManagementAnalyticsModule} from "../hr-management-analytics/hr-management-analytics.module";


@NgModule({
  imports: [SharedModule,LocalPackageModule,HrEmployeesListModule,BadgesManagementModule,HrManagementAnalyticsModule],       // module dependencies
  declarations: [PackagesIndexComponent],   // components and directives
  exports:[PackagesIndexComponent],
  providers: []                    // services
})
export class PackagesIndexModule { }


