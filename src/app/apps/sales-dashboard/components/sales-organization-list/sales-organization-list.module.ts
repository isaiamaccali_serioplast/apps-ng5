import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SalesOrganizationListComponent} from "./sales-organization-list.component";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SalesOrganizationListComponent],   // components and directives
  exports:[SalesOrganizationListComponent],
  providers: []                    // services
})
export class SalesOrganizationListModule { }


