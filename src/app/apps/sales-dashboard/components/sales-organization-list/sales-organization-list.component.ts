import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {
  TableWithPagesComponent,
  TableRowActions, TableRowAction
} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {MatSnackBar} from "@angular/material";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {SalesOrganization, SalesOrganizationService} from "../../../_common/services/sales-organization.service";
import {LegalEntity, LegalEntityService} from "../../../_common/services/legal-entity.service";
import {Title} from "@angular/platform-browser";
import {AutocompleteComponent} from "../../../../shared/material/autocomplete/autocomplete.component";
import {HRProfile, HRProfileService} from "../../../_common/services/hr-profiles.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {Department, DepartmentService} from "../../../_common/services/department.service";
import {Area, AreasService} from "../../../_common/services/areas.service";

export class LegalEntityCheckbox {
  legal_entity: LegalEntity;
  checked: boolean;
}

@Component({
  selector: 'sales-organization-list',
  templateUrl: 'sales-organization-list.component.html',
  styleUrls: ['sales-organization-list.component.css']
})

export class SalesOrganizationListComponent extends BaseComponent implements OnInit {
  salesOrganizationCreateOpen: boolean;
  onCreate: boolean;
  salesOrganization: SalesOrganization = new SalesOrganization();
  organizations: Organization[] = [];
  salesUsers: HRProfile[] = [];
  @ViewChild('organizationAcFilter') organizationAcFilter: AutocompleteComponent;
  legalEntitiesCheckBoxes: LegalEntityCheckbox[] = [];
  allLegalEntities: boolean = true;
  legalEntitiesChecked: boolean = false;
  legal_entities: LegalEntity[] = [];
  salesOrganizationFilter: SalesOrganization = new SalesOrganization();
  nameTimeout: any;
  availableActions = [];
  @ViewChild('salesOrganizationsTable') salesOrganizationsTable: TableWithPagesComponent;
  noItemsMessagesalesOrganizations = this.tr("NO_SALES_ORGANIZATIONS");
  headerConfsalesOrganizations = [
    {
      label: this.tr("SALES_REF"),
      valueKey: 'user',
      useStr: true
    },
    {
      label: this.tr("ORGANIZATION"),
      valueKey: 'organization',
      useStr: true
    },
    {
      label: this.tr("LEGAL_ENTITIES"),
      valueFunction: (item: SalesOrganization) => {
        if (item.legal_entities) {
          if (item.legal_entities.length == 0) {
            return "All"
          } else {
            return item.legal_entities.map((l) => l.code).join("; ");
          }
        }
      }
    },
  ];
  active = true;

  constructor(private organizationservice: OrganizationService,
              private salesOrganizationService: SalesOrganizationService,
              private legalEntityService: LegalEntityService,
              protected translationService: TranslationService,
              private hrProfileService: HRProfileService,
              protected messageService: MessageService,
              private titleService: Title,
              private snackBar: MatSnackBar,
              protected loadingService:LoadingService,
              protected departmentsService:DepartmentService,
              private areasService:AreasService) {
    super(translationService, messageService, loadingService);
    this.salesOrganizationCreateOpen = false;
    this.onCreate = false;
    this.titleService.setTitle("Serioplast - Sales Dashboard");
    this.salesOrganizationFilter.organization = new Organization();
  }

  ngOnInit() {
    this.loadLegalEntities();
    this.loadSalesUsers();
    this.initTableActions();
  }

  loadSalesUsers() {

    this.departmentsService.getDeparments({code:'BD', is_active:true}).subscribe((resp)=>{
      let deps:Department[] = parseSerionetRespToData(resp);
      if(deps.length>0){
        this.areasService.getAreas({department:deps[0].pk, code:'Sales'}).subscribe((resp)=>{
          let areas:Area[] = parseSerionetRespToData(resp);
          let query = {};
          query['is_active'] = true;
          query['department'] = deps[0].pk;
          // query['area'] = areas[0].pk; //sarà così, non c'è il filtro ora
          query['is_employee'] = 2; // is employee

          this.hrProfileService.getHRProfiles(query).subscribe((data) => {
            this.salesUsers = parseSerionetRespToData(data);
          })
        });

      }
    })

  }

  loadLegalEntities() {
    let query = {};
    query['is_active'] = true;

    this.legalEntityService.getLegalEntities(query).subscribe((data) => {
      this.legal_entities = parseSerionetRespToData(data);
    })
  }

  initSalesOrganizationLegalEntities() {
    this.legalEntitiesCheckBoxes = [];
    for (let i = 0; i < this.legal_entities.length - 1; i++) {
      let legalEntityCheckbox = new LegalEntityCheckbox();
      legalEntityCheckbox.legal_entity = this.legal_entities[i];
      legalEntityCheckbox.checked = this.isLegalEntityEnabled(this.legal_entities[i]);
      this.legalEntitiesCheckBoxes.push(legalEntityCheckbox);
    }

  }

  isLegalEntityEnabled(le: LegalEntity) {
    if (this.salesOrganization.legal_entities && this.salesOrganization.legal_entities.length > 0) {
      return this.salesOrganization.legal_entities.map((le: LegalEntity) => {
        return le.pk
      }).indexOf(le.pk) != -1;
    }

    return false;
  }

  initTableActions() {
    let actionEditSalesOrganization: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditSalesOrganization.click = this.editSalesOrganization.bind(this);
    let actionDeleteSalesOrganization: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteSalesOrganization.click = this.deleteSalesOrganization.bind(this);
    actionDeleteSalesOrganization.visibilityCondition = (item: SalesOrganization) => {
      return item.is_active;
    };
    let actionRestoreSalesOrganization: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreSalesOrganization.click = this.restoreSalesOrganization.bind(this);
    actionRestoreSalesOrganization.visibilityCondition = (item: SalesOrganization) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditSalesOrganization, actionRestoreSalesOrganization, actionDeleteSalesOrganization];
  }

  loadSalesOrganizations(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.salesOrganizationFilter.user)
      query['user'] = this.salesOrganizationFilter.user;

    if (this.salesOrganizationFilter.organization)
      query['organization'] = this.salesOrganizationFilter.organization.pk;

    if (this.salesOrganizationFilter.legal_entities)
      query['legal_entities'] = this.salesOrganizationFilter.legal_entities;

    return this.salesOrganizationService.getSalesOrganizations(query);
  }

  organizationRefChange() {
    this.salesOrganizationsTable.reload();
  }

  deleteSalesOrganization(so: SalesOrganization) {

    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.salesOrganizationService.deleteSalesOrganization(so).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.salesOrganizationsTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreSalesOrganization(so: SalesOrganization) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.salesOrganizationService.saveSalesOrganization(so).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.salesOrganizationsTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveSalesOrganization() {
    let displayMsg = false;
    if (this.salesOrganization.is_active == false) {
      displayMsg = true;
    }

    if (this.allLegalEntities) {
      this.salesOrganization.legal_entities = [];

    } else {
      let legalEntitiesChecked = [];
      if (this.legalEntitiesCheckBoxes.length > 0) {
        legalEntitiesChecked = this.legalEntitiesCheckBoxes.map((lc: LegalEntityCheckbox) => {
          return (lc.checked) ? lc.legal_entity : null;
        });
        legalEntitiesChecked = legalEntitiesChecked.filter((x) => x != null);
      }

      this.salesOrganization.legal_entities = legalEntitiesChecked;
    }

    this.salesOrganizationService.saveSalesOrganization(this.salesOrganization).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createSalesOrganization();
        this.salesOrganizationCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  createSalesOrganization() {
    this.salesOrganization = new SalesOrganization();
    this.salesOrganization.legal_entities = null;
    this.salesOrganization.user = new HRProfile();
    this.allLegalEntities = true;
    this.salesOrganization.organization = new Organization();
    this.initSalesOrganizationLegalEntities();
    this.salesOrganizationCreateOpen = true;
    this.onCreate = true;
  }

  editSalesOrganization(so: SalesOrganization) {
    this.salesOrganization = cloneObject(so);
    this.allLegalEntities = (this.salesOrganization.legal_entities.length == 0);
    this.initSalesOrganizationLegalEntities();
    this.isLegalEntitiesChecked();
    this.salesOrganizationCreateOpen = true;
    this.onCreate = false;
  }

  undoEditSalesOrganization() {
    this.createSalesOrganization();
    this.salesOrganizationCreateOpen = false;
  }

  selectedOrganization(organization: Organization) {
    if (organization && organization.pk) {
      this.salesOrganization.organization = organization;
    }
    else {
      this.salesOrganization.organization = null;
    }
  }

  selectedOrganizationFilter(organization: Organization) {
    if (organization && organization.pk) {
      this.salesOrganizationFilter.organization = organization;
    }
    else {
      this.salesOrganizationFilter.organization = null;
    }

    //this.loadSalesOrganizations(1, 15); for filters
  }

  isAllLegalEntities() {
    this.allLegalEntities = !(this.legalEntitiesCheckBoxes.filter((x) => x.checked).length > 0)
    this.legalEntitiesChecked = !this.allLegalEntities;
  }

  isLegalEntitiesChecked() {
    this.legalEntitiesChecked = (this.legalEntitiesCheckBoxes.filter((x) => x.checked).length > 0);
  }

  reload(){
    this.salesOrganizationsTable.reload();
  }

  reset() {
    this.salesOrganizationFilter.organization = new Organization();
    setTimeout(()=>{
      this.reload();
    })
  }
}
