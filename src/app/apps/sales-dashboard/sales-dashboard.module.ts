import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {SalesOrganizationListModule} from "./components/sales-organization-list/sales-organization-list.module";
import {SalesDashboardRoutingModule} from "./sales-dashboard.routes";

@NgModule({
  imports: [SharedModule,SalesDashboardRoutingModule,SalesOrganizationListModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class SalesDashboardModule {}

