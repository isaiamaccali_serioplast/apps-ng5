import {Routes, RouterModule} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {NgModule} from "@angular/core";
import {SalesOrganizationListComponent} from "./components/sales-organization-list/sales-organization-list.component";


// Route Configuration
export const SalesDashboardRoutes: Routes = [
  {
    path: '',
    component: SalesOrganizationListComponent,
    canActivate: [AuthGuard],
  },
];

/**
 * Root route, uses lazy loading to improve performance
 */
export const SalesDashboardRoutesRoot: Routes = [
  {
    path: 'sales-dashboard',
    loadChildren: './sales-dashboard.module.ts#SalesDashboardModule'
    //children: packagesRoutes,
    //canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(SalesDashboardRoutes)],
  exports: [RouterModule],
})
export class SalesDashboardRoutingModule {
}
