import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {AuthGuard} from "../../auth-guard";
import {SpcIndexComponent} from "./components/spc-index/spc-index.component";
import {SpcWOViewComponent} from "./components/spc-wo-view/spc-wo-view.component";

// Route Configuration
export const SpcRoutes: Routes = [
  {
    path: '',
    component: SpcIndexComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'woview/:id',
    component: SpcWOViewComponent,
    canActivate: [AuthGuard],
  }
];

/**
 * Root route, uses lazy loading to improve performance
 */
export const SpcRoutesRoot: Routes = [
  {
    path: 'spc',
    loadChildren: './spc.module.ts#MesModule'
    //children: packagesRoutes,
    //canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(SpcRoutes)],
  exports: [RouterModule],
})
export class SpcRoutingModule {
}
