import {Component, OnInit, ViewChild} from "@angular/core";
import {Workcenter, WorkcentersService} from "../../../_common/services/workcenters.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {Router} from "@angular/router";
import {OperationSAP, WorkOrderSAP, WorkOrdersService} from "../../../_common/services/workorders.service";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Observable} from "rxjs/src/core/linq";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {MatSnackBar} from "@angular/material";
import {Title} from "@angular/platform-browser";
import {DialogConfirmComponent} from "../../../../shared/components/dialog-confirm/dialog-confirm.component";
import {AppPlantUser} from "../../../_common/services/app-plant-users.service";
import {Site} from "../../../_common/services/sites.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'spc-index',
  templateUrl: 'spc-index.component.html',
  styleUrls: ['spc-index.component.css']
})

export class SpcIndexComponent extends TranslatableComponent implements OnInit {
  licenses: AppPlantUser[] = [];
  workcenters: Workcenter[] = [];
  operations: OperationSAP[] = [];
  workorders: WorkOrderSAP[] = [];
  sites: Site[] = [];
  selected_site: number;
  selected_wo: WorkOrderSAP = new WorkOrderSAP();
  selected_wo_id: string;
  selected_wc: string;
  element: 4;
  bool: boolean;
  released: boolean;
  released_status: string;
  selected_site_obj: Site;
  ready = false;
  DEFAULT_CID = "99999";
  @ViewChild('popupMessageIndex') popupMessageIndex: DialogConfirmComponent;
  popupMessageResponse: string = "";
  popupHeaderTitle: string = "";
  popupMessageHeaderColor: string = "#4CAF50";
  ERROR_HEADER_COLOR: string = "#af504c";
  SUCCESS_HEADER_COLOR: string = "#4CAF50";

  constructor(private sideNavService: SideNavService, private workcentersService: WorkcentersService,
              private loginService: LoginService, private router: Router,
              private workOrdersService: WorkOrdersService, private loadingservice: LoadingService,
              protected translationService: TranslationService, private titleService: Title,
              private snackBar: MatSnackBar) {
    super(translationService);
    this.ready = true;
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.titleService.setTitle("Serioplast - SPC");
    this.DEFAULT_CID = (this.loginService.user.badge_code) ? this.loginService.user.badge_code : this.DEFAULT_CID;
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.bool = false;
    this.setReleaseView(false);
  }

  getCols() {
    if (this.workorders.length <= 2) {
      return 2;
    }
    else if (this.workorders.length == 3) {
      return 3;
    }
    else if (this.workorders.length == 4) {
      return 4;
    }
    else if (this.workorders.length > 4 && this.workorders.length <= 9) {
      return 3;
    }
    else if (this.workorders.length > 9 && this.workorders.length <= 12) {
      return 4;
    }
    else if (this.workorders.length > 12 && this.workorders.length <= 20) {
      return 5;
    }
    else if (this.workorders.length > 20 && this.workorders.length <= 24) {
      return 6;
    }
    else if (this.workorders.length > 24) {
      return 7;
    }

  }


  changeSite(obj: Site) {
    this.selected_site_obj = obj;
    this.selected_site = obj.pk;
    this.loadWorkOrders(this.selected_site_obj);
    this.selected_wc = "";
    this.selected_wo_id = "";
  }

  changeWc(wc) {
    this.selected_wc = wc.sap_code;
    this.selected_wo_id = "";
    console.log("test");
  }


  changeReleased() {
    this.setReleaseView(!this.released);
    this.loadWorkOrders(this.selected_site_obj);
  }

  setReleaseView(bol: boolean) {
    if (bol) {
      this.released = true;
      this.released_status = "RELEASED";
    }
    else {
      this.released = false;
      this.released_status = "WORKING";
    }
  }

  loadWorkOrders(obj: Site) {
    let doload = (query) => {
      this.loadingservice.show();
      this.workOrdersService.getWorkOrders(query).subscribe((response) => {
        if (response['status'] == 200) {
          if (response['response']['results'].length == 0) {
            let message = this.tr("NO_WORKORDERS");
            this.openSnackBar(message, ['warning']);
          }
          this.workorders = response['response']['results'];

        } else {
          this.workorders = [];
          this.errorResponsePopup(response["message"]);
        }
        this.loadingservice.hide();
      });
    };

    let query = {};
    console.log(obj.code);
    query['Plant'] = obj.code;
    query['Status'] = this.released_status;
    doload(query);

  }

  viewWo(wo: WorkOrderSAP) {
    this.router.navigate(["spc", "woview", wo.WorkOrderNumber], {queryParams: {site: this.selected_site}});
  }

  getWorkCenters(wo: WorkOrderSAP) {
    if (wo.WOperationLink && wo.WOperationLink.results) {
      return wo.WOperationLink.results.map((op: OperationSAP) =>{
        return op.WorkCenter;
      }).join("; ")

    } else {
      return '';
    }
  }

  loadWorkCenters(obj: Site) {
    let query = [];
    query['get_location'] = obj.pk;
    this.workcentersService.getWorkcenters(query).subscribe((response) => {
      if (response["results"]) {
        this.workcenters = response['results'];
      }
    });
  }


  noLicense() {
    this.loadingservice.hide();
    this.logout();
  }

  logout() {
    this.loginService.doLogout().first().subscribe((logoutResponse) => {
      this.router.navigate(["/login"]);
    });
  }


  /**
   * Remove from site without related SAP license (AppPlantUser model on Serionet)
   */



  openSnackBar(message, classes) {
    this.snackBar.open(message, null, {duration: 5000, panelClass: classes});
  }


  emptyFunc() {
  }

  successResponsePopup(message: string) {
    this.popupMessageResponse = message;
    this.popupMessageHeaderColor = this.SUCCESS_HEADER_COLOR;
    this.popupHeaderTitle = this.tr("SUCCESS");
    this.popupMessageIndex.open();
  }

  errorResponsePopup(message: string) {
    this.popupMessageResponse = message;
    this.popupMessageHeaderColor = this.ERROR_HEADER_COLOR;
    this.popupHeaderTitle = this.tr("ERROR");
    this.popupMessageIndex.open();
  }
}
