import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SpcIndexComponent} from "./spc-index.component";



@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SpcIndexComponent],   // components and directives
  exports:[SpcIndexComponent],
  providers: []                    // services
})
export class SpcIndexModule { }


