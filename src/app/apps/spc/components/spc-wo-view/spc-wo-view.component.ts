import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {WorkOrderSAP, WorkOrdersService} from "../../../_common/services/workorders.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {ControlPlan, ControlPlansService} from "../../../_common/services/control-plans.service";
import {
  TableRowAction,
  TableRowActions,
  TableWithPagesComponent
} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {Material, MaterialsService} from "../../../_common/services/materials.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {ControlPlanMeasure, ControlPlanMeasuresService} from "../../../_common/services/control-plan-measures.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {ContentType, ContentTypeService} from "../../../_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../_common/services/status-link.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {
  MeasureSession, MeasureSessionSample,
  MeasureSessionService
} from "../../../_common/services/measure-session.service";
import {Workcenter, WorkcentersService} from "../../../_common/services/workcenters.service";
import {WorkcenterStatusMatrixService} from "../../../_common/services/workcenter-status-matrix.service";
import {FrequencyService} from "../../../_common/services/frequency.service";
import {PropertySet} from "../../../_common/services/property-set.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'spc-wo-view',
  templateUrl: 'spc-wo-view.component.html',
  styleUrls: ['spc-wo-view.component.css']
})

export class SpcWOViewComponent extends TranslatableComponent implements OnInit {

  @ViewChild('controlPlansTable') controlPlansTable: TableWithPagesComponent;
  noItemsMessageControlPlans = this.tr("NO_CONTROL_PLANS_AVAILABLE");
  headerConfControlPlans = [];
  availableActionsControlPlans = [];

  @ViewChild('sessionTable') sessionTable: TableWithPagesComponent;
  noItemsMessageSessions = this.tr("NO_SESSIONS_AVAILABLE");
  headerConfControlSessions = [];
  availableActionsControlSessions = [];

  selectedSite: Site;
  workorder: WorkOrderSAP;
  material = new Material();
  selectedControlPlan: ControlPlan;
  sessionBatch: string;
  controlPlans: ControlPlan[] = [];

  newMeasureSession: MeasureSession;

  bool = false;
  propertySet: PropertySet;
  // newMeasureSessionCavity: MeasureSessionCavity[] = [];
  newControlPlanSessionMeasures: ControlPlanMeasure[] = [];
  newMeasureSessionSample: MeasureSessionSample[] = [];

  change_status = false;
  wcAvailableStatusesLink: StatusLink[] = [];
  wcAvailableSubStatusesLink: StatusLink[] = [];
  statusLinkSessionWorking: StatusLink = new StatusLink();
  selectedStatus: number;
  selectedSubStatus: number;

  serionetWorkcenters: Workcenter[] = [];

  constructor(private activatedRoute: ActivatedRoute,
              private workordersservice: WorkOrdersService,
              private loadingservice: LoadingService, private controlPlanService: ControlPlansService,
              private controlPlanMeasuresService: ControlPlanMeasuresService,
              protected translationService: TranslationService,
              private sessionsService: MeasureSessionService,
              private materialService: MaterialsService, private siteService: SitesService,
              private contentTypeService: ContentTypeService,
              private statusLinkService: StatusLinkService,
              private messageService: MessageService,
              private workcenterService: WorkcentersService,
              private workcenterstatusmatrixService: WorkcenterStatusMatrixService,
              private frequencyService: FrequencyService) {
    super(translationService)
  }

  ngOnInit() {
    this.headerConfControlSessions = [
      {
        label: this.tr("DATE"),
        valueKey: 'date',
      },
      {
        label: this.tr("BATCH"),
        valueKey: 'batch',
      },
      {
        label: this.tr("NOTES"),
        valueKey: 'notes',
      }
    ];
    this.activatedRoute.params.subscribe((params) => {
      if (params['id']) {
        let wo_number = params['id'];
        this.loadWorkOrder(wo_number);

      }
    });
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['site']) {
        let currentSite = params['site'];
        this.siteService.getSite(parseInt(currentSite, 10)).subscribe((site) => {
          this.selectedSite = site
        });
      }
    });

    this.contentTypeService.getContentTypes({model: 'measuresession', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {

            data['results'].forEach((sl: StatusLink) => {
              if (sl.delta_rel == 1)
                this.statusLinkSessionWorking = sl;
            });
          }
        });
      }
    });
    this.initTableActions();
    this.loadStatusLink();

  }

  initTableActions() {
    let actionAddSession: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionAddSession.click = this.addSession.bind(this);
    actionAddSession.tooltip = "Add new Session";
    actionAddSession.icon = 'add';
    this.availableActionsControlPlans = [actionAddSession];
  }

  loadSessions(page: number, results: number) {
    //Load previus Session of WO
    console.log(this.workorder);
    let batch_list;
    let query;
    if (this.workorder.WBatchLink.results.length > 0) {
      batch_list = this.workorder.WBatchLink.results.map((b) => b.BatchNumber).join("|");
      this.sessionBatch = this.workorder.WBatchLink.results[0].BatchNumber;
      query = {
        is_active: true,
        ordering: '-date',
        results: results,
        batch_list: batch_list
      };
    }
    else {
      query = {
        results: 0,
      };

    }

    console.log(this.sessionBatch);
    if (!page) page = 1;
    query['page'] = page;
    return this.sessionsService.getSessions(query);

  }

  addSession(cp: ControlPlan) {

    this.messageService.open({
      title: ' ',
      error: false,
      message: "Are you sure you want to create a new session?",
      confirm: () => {
        this.selectedControlPlan = cp;
        //create the new Measure Session OBJ.
        this.newMeasureSession = new MeasureSession();
        //TEMP DATA - Session value;
        this.newMeasureSession.site = this.selectedSite;
        this.newMeasureSession.material = this.material;
        this.newMeasureSession.control_plan = cp;
        this.newMeasureSession.status = cloneObject(this.statusLinkSessionWorking);

        //TODO
        this.newMeasureSession.batch = this.sessionBatch;

        // let tempmss = new MeasureSessionCavity();
        // tempmss.object_id = 458;
        // tempmss.content_type = 121;
        // this.newMeasureSessionCavity.push(tempmss);
        // this.newMeasureSession.measuresessioncavity_set = this.newMeasureSessionCavity;

        // this.newMeasureSessionSample.samplemeasure_set = newSampleMeasureSet;
        this.newMeasureSession.measuresessionsample_set = this.newMeasureSessionSample;

        this.newMeasureSession.controlplansessionmeasure_set = this.newControlPlanSessionMeasures;
        this.loadControlPlansMeasure();
      },
      deny: () => {
      }
    });
  }

  backFromSession() {
    this.newMeasureSession = null;
    this.bool = false;
  }

  frq_check(second: number) {
    if (second < 0) {
      return "Outdated from " + Math.round(Math.abs(second) / 60) + " minutes";
    }
    else {
      return "in " + Math.round(Math.abs(second) / 60) + " minutes";
    }
  }

  controlPlanRowColor(cp: ControlPlan) {
    if (cp.time_from_last < 0) {
      return "tomato";
    }
    if (cp.time_from_last > 0 && cp.time_from_last <= 600) {
      return "yellow";
    }
    if (cp.time_from_last > 600) {
      return "";
    }

  }

  loadControlPlans() {
    let queryMat = {code: this.workorder.Material, is_active: true, type__delta_rel: 1};
    this.materialService.getMaterials(queryMat).subscribe((data) => {
      if (data["results"]) {
        this.material = data['results'][0];
      }
      let query = {
        site: this.selectedSite.pk,
        material: this.material.pk,
        type__delta_rel: 1,
        is_active: true,
        order_by: "-time_from_last"
      };
      console.log(this.material.description);
      this.controlPlanService.getControlPlans(query).subscribe((data) => {

        data['results'] = data['results'].sort((a, b) => {
          return (a.time_to_last > b.time_to_last) ? 1 : ((b.time_to_last > a.time_to_last) ? -1 : 0);
        });
        this.controlPlans = parseSerionetRespToData(data);
      });
    });

  }

  loadControlPlansMeasure() {

    let query = {control_plan: this.selectedControlPlan.pk, is_active: true};
    this.controlPlanMeasuresService.getControlPlanMeasures(query).subscribe((data) => {
      if (data["results"]) {

        let cpm_session: ControlPlanMeasure[] = data['results'];
        //duplicate the controlplanmeasure in controlplan session measure structure

        let delta = 0;
        console.log(cpm_session.length);
        for (let cpm of cpm_session) {
          delete cpm['pk'];
          cpm.delta = delta;
          delta++;
          this.newMeasureSession.controlplansessionmeasure_set.push(cpm);
        }

        console.log(this.newMeasureSession);
        this.sessionsService.saveSession(this.newMeasureSession).subscribe((resp) => {
          if (resp.pk) {
            this.newMeasureSession = resp;
            this.bool = true;
          }
          else {
            resp.context = "SERIONET";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        });
      }
    });
  }

  save() {
    console.log('creating session');
    let s: MeasureSession = cloneObject(this.newMeasureSession);
    s.measuresessionsample_set = [];

    this.sessionsService.saveSession(s).subscribe((response) => {
      this.newMeasureSession.pk = response['pk'];

      // let tempCavity = new MeasureSessionCavity();
      // tempCavity = response['measuresessioncavity_set'][0];
      // this.newMeasureSession.measuresessioncavity_set[0].pk = tempCavity.pk;
      // this.newMeasureSession.measuresessionsample_set[0].cavity = tempCavity;
      // console.log(this.newMeasureSession);
      this.save_samplemeasure()
    });
  }

  save_samplemeasure() {
    this.sessionsService.saveSession(this.newMeasureSession).subscribe((response) => {
      this.newMeasureSession = null;
      this.bool = false;
    });
  }

  loadWorkOrder(wo_number: string) {
    this.loadingservice.show();
    this.workordersservice.getWorkOrder(wo_number).subscribe((response) => {
      if (response.status == 200) {
        this.workorder = response['response'];
        this.loadSerionetWorkcenters();
        this.loadControlPlans();
      }
      this.loadingservice.hide();
    });
  }

  loadSerionetWorkcenters() {
    if (this.workorder.WOperationLink.results.length > 0) {
      let sap_code_list = [];
      for (let item of this.workorder.WOperationLink.results) {
        sap_code_list.push(item.WorkCenter)
      }
      this.workcenterService.getWorkcenters({sap_code__in: sap_code_list.join('|')}).subscribe((data) => {
        this.serionetWorkcenters = parseSerionetRespToData(data);

      });
    }
  }

  loadStatusLink() {
    this.contentTypeService.getContentTypes({model: 'workcenter', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        //load all possbile status
        this.statusLinkService.getStatusLinks({app: ct.pk, fieldname: 'status'}).subscribe((data) => {
          this.wcAvailableStatusesLink = parseSerionetRespToData(data);
        });

      }
    });
  }

  loadSubStatus() {
    console.log(this.selectedStatus);
    this.workcenterstatusmatrixService.getStatuses({status: this.selectedStatus}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.wcAvailableSubStatusesLink = data['results'][0]['nested_sub_status'];
      }
    });
  }

  changeStatusSend(wc: Workcenter) {
    if (this.selectedStatus) {
      this.loadingservice.show();
      this.workcenterService.newStatus(wc, this.selectedStatus, this.selectedSubStatus).subscribe((risp) => {
        this.loadSerionetWorkcenters();
        this.loadingservice.hide();
      });
    }
    this.change_status = false;
    this.selectedSubStatus = this.selectedStatus = null;
  }

  changeStatus() {
    this.change_status = true;
  }

  closeStatus() {
    this.change_status = false;
    this.selectedSubStatus = this.selectedStatus = null;
  }

  logout() {
    //TODO
  }

  back() {
    //TODO
  }
}
