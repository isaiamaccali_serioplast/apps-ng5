import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {WorkOrdersService} from "../../../_common/services/workorders.service";
import {WHMovementSapService} from "../../../_common/services/whmovements-sap.service";
import {SpcWOViewComponent} from "./spc-wo-view.component";
import {SessionDetailModule} from "../../../../shared/components/session-detail/session-detail.module";

@NgModule({
  imports: [SharedModule,SessionDetailModule],       // module dependencies
  declarations: [SpcWOViewComponent],   // components and directives
  exports:[SpcWOViewComponent],
  providers: [
    WorkOrdersService]                    // services
})
export class SpcWOViewModule { }


