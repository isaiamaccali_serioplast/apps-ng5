import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {SpcIndexModule} from "./components/spc-index/spc-index.module";
import {SpcRoutingModule} from "./spc.routes";
import {SpcWOViewModule} from "./components/spc-wo-view/spc-wo-view.module";

@NgModule({
  imports: [SharedModule,SpcRoutingModule,SpcIndexModule,SpcWOViewModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class SPCModule{}

