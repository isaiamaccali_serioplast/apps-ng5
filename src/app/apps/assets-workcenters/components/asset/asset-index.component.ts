import {Component} from "@angular/core";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {Asset, AssetsService} from "../../../_common/services/assets.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'asset-index',
  templateUrl: 'asset-index.component.html',
  styleUrls: ['asset-index.component.css']
})

export class AssetIndexComponent extends TranslatableComponent{

  editMode = false;
  createMode = false;
  assetToEdit:Asset;
  loading = false;

  constructor(private messageService:MessageService, protected translationService:TranslationService,
              private assetService:AssetsService, private loadingService:LoadingService) {
    super(translationService)
  }

  createAsset(a:Asset){
    this.assetToEdit = a;
    this.createMode = true;
    this.editMode = true;
  }

  editAsset(a:Asset){
    this.assetToEdit = a;
    this.createMode = false;
    this.editMode = true;
  }

  deleteAsset(a:Asset){
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: ()=>{
        this.loadingService.show();
        this.assetService.deleteAsset(a).subscribe((resp)=>{
          this.loadingService.hide();
          if (resp.status == 204) {
            this.loading = true;
            setTimeout(()=>{
              this.loading = false;
            },500)
          }
          else {
            resp.context = "SERIONET";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      },
      deny: ()=>{}
    })
  }

  restoreAsset(a:Asset){
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_RESTORE'),
      autoClose: false,
      confirm: ()=>{
        this.loadingService.show();
        this.assetService.restoreAsset(a).subscribe((resp)=>{
          this.loadingService.hide();
          if (resp.pk) {
            this.loading = true;
            setTimeout(()=>{
              this.loading = false;
            },500)
          }
          else {
            resp.context = "SERIONET";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      },
      deny: ()=>{}
    })
  }

  back(){
    this.editMode = false;
  }
}
