import {NgModule} from "@angular/core";
import {AssetEditComponent} from "./asset-edit.component";
import {SharedModule} from "../../../../../shared/shared.module";
import {AssetsCredentialsAuthorizationsService} from "../../../services/assets-credentials-authorizations.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [AssetEditComponent],   // components and directives
  exports:[AssetEditComponent],
  providers: [AssetsCredentialsAuthorizationsService]                    // services
})
export class AssetEditModule { }


