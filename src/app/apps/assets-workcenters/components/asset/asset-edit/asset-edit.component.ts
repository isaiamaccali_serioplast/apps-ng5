import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {Asset, AssetsService, AssetSubModel} from "../../../../_common/services/assets.service";
import {_getAssetType, ASSET_TYPES, AssetType, AssetTypesService} from "../../../../_common/services/asset-types.service";
import {AssetSubtypeLink, AssetSubTypeLinkService} from "../../../../_common/services/asset-subtype-link.service";
import {StatusLink, StatusLinkService} from "../../../../_common/services/status-link.service";
import {cloneObject, fixValue, parseSerionetRespToData} from "../../../../../shared/utils";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {FileService} from "../../../../_common/services/file.service";
import {AssetUser, AssetUsersService, AssetUserType, AssetUserTypeService} from "../../../../_common/services/asset-user.service";
import {NetworkInterface, NetworkInterfacesService, NetworkInterfaceType, NetworkInterfaceTypeService, SocketService} from "../../../../_common/services/network-interface.service";
import {ContentTypeService} from "../../../../_common/services/content-type.service";
import {Organization} from "../../../../_common/services/organization.service";
import {WorkcentersService} from "../../../../_common/services/workcenters.service";
import {HRProfile} from "../../../../_common/services/hr-profiles.service";
import {HeaderConf, TableRowAction, TableRowActions} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../../_common/services/has-company-position.service";
import {LoginService} from "../../../../../shared/login/service/login.service";
import {AssetsCredentialsAuthorizationsService, IT_JOB_TITLES} from "../../../services/assets-credentials-authorizations.service";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

const SubModelConfigurationFieldType = {
  STRING: 'string',
  NUMBER: 'number',
  MULTIPLE: 'multiple',
  BOOLEAN: 'boolean',
  AUTOCOMPLETE: 'autocomplete'
};

class SubModelConfigurationField {
  label: string;
  model: any;
  assignModel: any;
  type: string; //one of SubModelConfigurationType
  options?: any[];
  source?: any[];
  sourceFunction?: any;
  displayKey?: string;
}

class SubModelConfiguration {
  fields?: SubModelConfigurationField[];
}

@Component({
  selector: 'asset-edit',
  templateUrl: 'asset-edit.component.html',
  styleUrls: ['asset-edit.component.css']
})
export class AssetEditComponent extends BaseComponent implements OnInit {

  @Output('backEvent') backEvent = new EventEmitter();
  @ViewChild('photo') photoInput: HTMLInputElement;

  @Input('assetToEdit') asset: Asset = new Asset();
  @Input('createMode') createMode = false;

  assetTypes: AssetType[] = [];
  statusLinks: StatusLink[] = [];

  ASSET_TYPES_APP = ASSET_TYPES;
  ASSET_TYPES_APP_KEYS: string[] = [];
  assetSubtypeLinks: AssetSubtypeLink[] = [];

  assetSubModelConfiguration = {};

  assetAppPk: number;

  sites: Site[] = [];

  assetUserTypes: AssetUserType[] = [];
  networkInterfaceTypes: NetworkInterfaceType[] = [];

  hardwareAppPk: number;
  softwareTypes: AssetSubtypeLink[] = [];
  licenseTypes: AssetSubtypeLink[] = [];
  networkInterfaces: NetworkInterface[] = [];
  assetUsers: AssetUser[] = [];

  networkInterface: NetworkInterface;
  networkInterfacesEditOn = false;

  assetUser: AssetUser;
  assetUserEditOn = false;

  locations: Site[] = [];
  ready = false;

  availableActionsNI: TableRowAction[] = [];
  headerConfNI: HeaderConf[] = [];
  noItemsMessageNI = this.tr('NO_NETWORK_INTERFACES_AVAILABLE');

  availableActionsAU: TableRowAction[] = [];
  headerConfAU: HeaderConf[] = [];
  noItemsMessageAU = this.tr('NO_ASSET_USERS_AVAILABLE');
  hasCompanyPositions: HasCompanyPosition[] = [];
  handleCredentials = false;

  SubModelConfigurationFieldType = SubModelConfigurationFieldType;

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private sitesService: SitesService,
              private assetsService: AssetsService,
              private assetTypeService: AssetTypesService, private statusLinkService: StatusLinkService,
              private assetSubtypeLinkService: AssetSubTypeLinkService, private fileService: FileService,
              private assetUsersService: AssetUsersService, private assetUserTypeService: AssetUserTypeService,
              private networkInterfacesService: NetworkInterfacesService, private networkInterfaceTypeService: NetworkInterfaceTypeService,
              private contentTypeService: ContentTypeService, private workcentersService: WorkcentersService,
              private hasCompanyPositionService: HasCompanyPositionService, private loginService: LoginService,
              private assetsCredentialsAuthorizationsService: AssetsCredentialsAuthorizationsService) {
    super(translationService, messageService, loadingService);
    this.ASSET_TYPES_APP_KEYS = Object.keys(this.ASSET_TYPES_APP);
    this.SubModelConfigurationFieldType = SubModelConfigurationFieldType;
  }

  ngOnInit() {

    let count = 0;
    let callback = () => {
      count--;
      if (count == 0) {
        this.hasCompanyPositionService.getHasCompanyPositions({currents_for_user: this.loginService.user.pk, is_active: true}).subscribe((response) => {
          if (response['results'] && response['results'].length > 0) {
            this.hasCompanyPositions = response['results'];
            this.handleCredentials = this.assetsCredentialsAuthorizationsService.isAuthorized(this.hasCompanyPositions, IT_JOB_TITLES);
            this.loadingService.hide();
            this.ready = true;
          }
        });

      }
    };

    this.loadingService.show();

    this.sitesService.getSites({is_active: true}).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
      callback();
    });
    count++;

    this.assetTypeService.getAssetTypes({is_active: true, ordering: 'name'}).subscribe((data) => {
      this.assetTypes = parseSerionetRespToData(data);
      if (this.asset.pk) {
        this.initAssetToEdit(() => {
          callback();
        });
      }
      else {
        this.initAssetSubtypeConfiguration();
        callback();
      }
    });
    count++;

    this.statusLinkService.getStatusLinks({app: 126, fieldname: 'status', is_active: true}).subscribe((data) => {
      this.statusLinks = parseSerionetRespToData(data);
      callback();
    });
    count++;

    this.assetUserTypeService.getAssetUserTypes({is_active: true}).subscribe((data) => {
      this.assetUserTypes = parseSerionetRespToData(data);
      callback();
    });
    count++;

    this.networkInterfaceTypeService.getNetworkInterfaceType({is_active: true}).subscribe((data) => {
      this.networkInterfaceTypes = parseSerionetRespToData(data);
      callback();
    });
    count++;

    this.contentTypeService.getContentTypes({is_active: true, model: 'asset'}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.assetAppPk = data['results'][0]['pk'];
        callback();
      }
    });
    count++;

    this.contentTypeService.getContentTypes({is_active: true, model: 'hardware'}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.hardwareAppPk = data['results'][0]['pk'];
        callback();
      }
    });
    count++;

    this.contentTypeService.getContentTypes({is_active: true, model: 'software'}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let app = data['results'][0]['pk'];
        this.assetSubtypeLinkService.getAssetSubtypeLinks({
          is_active: true,
          app: app,
          fieldname: 'software_type'
        }).subscribe((data) => {
          let astl: AssetSubtypeLink[] = parseSerionetRespToData(data);
          this.softwareTypes = astl.filter((a) => a.fieldname == 'software_type');
          this.licenseTypes = astl.filter((a) => a.fieldname == 'license_type');
          callback();
        });
      }
    });
    count++;

    this.headerConfNI = [
      {
        columnId: 'type',
        valueKey: 'type',
        label: 'Type',
        useStr: true
      },
      {
        columnId: 'ip_address',
        valueKey: 'ip_address',
        label: 'IP Address',
      },
      {
        columnId: 'mac_address',
        valueKey: 'mac_address',
        label: 'Mac Address',
      },
      {
        columnId: 'socket_services',
        label: 'Socket Services',
        valueFunction: (ni) => this.printSocketServices(ni)
      }
    ];

    let niEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
    niEdit.click = (ni) => {
      this.editNetworkInterface(ni)
    };
    let niDelete: TableRowAction = cloneObject(TableRowActions.DELETE);
    niDelete.click = (ni) => {
      this.removeNetworkInterface(ni)
    };
    this.availableActionsNI = [niEdit, niDelete];

    this.headerConfAU = [
      {
        columnId: 'type',
        valueKey: 'type',
        label: 'Type',
        useStr: true
      },
      {
        columnId: 'username',
        valueKey: 'user',
        label: 'Username',
      },
      {
        columnId: 'password',
        valueKey: 'password',
        label: 'Password',
      }
    ];

    let auEdit: TableRowAction = cloneObject(TableRowActions.EDIT);
    auEdit.click = (au) => {
      this.editAssetUser(au)
    };
    let auDelete: TableRowAction = cloneObject(TableRowActions.DELETE);
    auDelete.click = (au) => {
      this.deleteAssetUser(au)
    };
    this.availableActionsAU = [auEdit, auDelete];

  }

  initAssetToEdit(callback?: any) {
    fixValue(this.assetTypes, this.asset, 'type');

    if (!this.asset.ownership)
      this.asset.ownership = new Organization();

    this.assetSubtypeLinkService.getAssetSubtypeLinks({
      app: this.asset.type.model_reference.pk,
      is_active: true
    }).subscribe((data) => {
      let st = parseSerionetRespToData(data);
      if (!this.asset.sub_type.sub_type)
        this.asset.sub_type.sub_type = new AssetSubtypeLink();
      else {
        st.forEach((s) => {
          if (s.pk == this.asset.sub_type.sub_type)
            this.asset.sub_type.sub_type = s;
        });
      }

      if (!this.asset.sub_type.license_type)
        this.asset.sub_type.license_type = new AssetSubtypeLink();
      else {
        st.forEach((s) => {
          if (s.pk == this.asset.sub_type.license_type)
            this.asset.sub_type.license_type = s;
        });
      }

      if (!this.asset.sub_type.software_type)
        this.asset.sub_type.software_type = new AssetSubtypeLink();
      else {
        st.forEach((s) => {
          if (s.pk == this.asset.sub_type.software_type)
            this.asset.sub_type.software_type = s;
        });
      }

      this.initAssetSubtypeConfiguration();
      this.assetSubModelConfiguration[this.getAssetType(this.asset)].fields.forEach((f: SubModelConfigurationField) => {
        if (f.label == this.tr("SUBTYPE")) {
          f.options = st;
          if (this.getAssetType(this.asset) == ASSET_TYPES.HARDWARE) {
            f.model = this.asset.sub_type.sub_type;
          }
          else {
            f.model = this.asset.sub_type.sub_type.pk
          }
        }
        if (f.type == SubModelConfigurationFieldType.BOOLEAN && !f.model) {
          f.assignModel(false);
          f.model = false;
        }

      });

      if (this.asset.workcenter && this.asset.workcenter.pk) {
        this.workcentersService.getWorkcenter(parseInt(this.asset.workcenter.pk + "", 10)).subscribe((wc) => {
          this.asset.workcenter = wc;
        });
      }

      if (this.asset.photo && this.asset.photo.pk) {
        this.fileService.getFile(parseInt(this.asset.photo.pk + "", 10)).subscribe((photo) => {
          this.asset.photo = photo;
          callback();

        });
      }
      else {
        callback();
      }
      this.loadAssetUsers();
      this.loadAssetNetworkInterfaces();
    });
  }

  initAssetSubtypeConfiguration() {

    let angleConf = this.createSubModelConf({
      label: "ANGLE", key: 'angle',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let caMPConf = this.createSubModelConf({
      label: "CA_MAX_PRESSURE", key: 'ca_max_pressure',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let caOutputConf = this.createSubModelConf({
      label: "CA_OUTPUT", key: 'ca_output',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let clampForceConf = this.createSubModelConf({
      label: "CLAMPING_FORCE", key: 'clamping_force',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let extruderConf = this.createSubModelConf({
      label: "EXTRUDER", key: 'extruder',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let extruderDiamConf = this.createSubModelConf({
      label: "EXTRUDER_DIAM", key: 'extruder_diam',
      type: SubModelConfigurationFieldType.STRING
    });

    let genManSwiConf = this.createSubModelConf({
      label: "GENERAL_MAIN_SWITCH", key: 'general_main_switch',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let heatingZonesConf = this.createSubModelConf({
      label: "HEATING_ZONES", key: 'heating_zones',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let instElPoConf = this.createSubModelConf({
      label: "INSTALLED_ELECTRICAL_POWER", key: 'installed_electrical_power',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let kvaConf = this.createSubModelConf({
      label: "KVA_TRAFO", key: 'kva_trafo',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let licensePlateConf = this.createSubModelConf({
      label: "LICENSE_PLATE", key: 'license_plate',
      type: SubModelConfigurationFieldType.STRING
    });

    let licenseTypeConf = new SubModelConfigurationField();
    licenseTypeConf.label = this.tr("LICENSE_TYPE");
    licenseTypeConf.assignModel = (model) => {
      if (!this.asset.sub_type.license_type) {
        this.asset.sub_type.license_type = new AssetSubtypeLink();
      }
      this.asset.sub_type.license_type.pk = model;
    };
    licenseTypeConf.type = SubModelConfigurationFieldType.MULTIPLE;
    licenseTypeConf.options = this.licenseTypes;
    if (!this.asset.sub_type.license_type) {
      this.asset.sub_type.license_type = new AssetSubtypeLink();
    }
    licenseTypeConf.model = this.asset.sub_type.license_type.pk;

    let manifoldConf = this.createSubModelConf({
      label: "MANIFOLD", key: 'manifold',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let mouldDimConf = this.createSubModelConf({
      label: "MOULD_DIMENSION", key: 'mould_dimension',
      type: SubModelConfigurationFieldType.STRING
    });

    let mouldPitchConf = this.createSubModelConf({
      label: "MOULD_PITCH", key: 'mould_pitch',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let mouldPlatesDim1Conf = this.createSubModelConf({
      label: "MOULD_PLATES_DIM1", key: 'mould_dim_1',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let mouldPlatesDim2Conf = this.createSubModelConf({
      label: "MOULD_PLATES_DIM2", key: 'mould_dim_2',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let neckOrConf = this.createSubModelConf({
      label: "NECK_ORIENTATION", key: 'neck_orientation',
      type: SubModelConfigurationFieldType.BOOLEAN
    });

    let ovenPitchConf = this.createSubModelConf({
      label: "OVEN_PITCH", key: 'oven_pitch',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let pitch1Conf = this.createSubModelConf({
      label: "PITCH1", key: 'pitch1',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let pitch2Conf = this.createSubModelConf({
      label: "PITCH2", key: 'pitch2',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let prefHeatingConf = this.createSubModelConf({
      label: "PREFERENTIAL_HEATING", key: 'preferential_heating',
      type: SubModelConfigurationFieldType.BOOLEAN
    });

    let pressureGapConf = this.createSubModelConf({
      label: "PRESSURE_GAP", key: 'pressure_gap',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let shootingVolumeConf = this.createSubModelConf({
      label: "SHOOTING_VOLUME", key: 'shooting_volume',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let softwareTypeConf = new SubModelConfigurationField();
    softwareTypeConf.label = this.tr("SOFTWARE_TYPE");
    softwareTypeConf.assignModel = (model) => {
      if (!this.asset.sub_type.software_type) {
        this.asset.sub_type.software_type = new AssetSubtypeLink();
      }
      this.asset.sub_type.software_type.pk = model;
    };
    softwareTypeConf.type = SubModelConfigurationFieldType.MULTIPLE;
    softwareTypeConf.options = this.softwareTypes;
    if (!this.asset.sub_type.software_type) {
      this.asset.sub_type.software_type = new AssetSubtypeLink();
    }
    softwareTypeConf.model = this.asset.sub_type.software_type.pk;

    let subTypeConf = new SubModelConfigurationField();
    subTypeConf.label = this.tr("SUBTYPE");
    subTypeConf.assignModel = (model) => {
      this.asset.sub_type.sub_type.pk = model;
    };
    subTypeConf.type = SubModelConfigurationFieldType.MULTIPLE;
    subTypeConf.options = [];

    let subTypeHardwareConf = new SubModelConfigurationField();
    subTypeHardwareConf.label = this.tr("SUBTYPE");
    subTypeHardwareConf.assignModel = (model) => {
      this.asset.sub_type.sub_type = model;
    };
    subTypeHardwareConf.type = SubModelConfigurationFieldType.AUTOCOMPLETE;
    subTypeHardwareConf.source = [];
    subTypeHardwareConf.sourceFunction = (keyword) => {
      this.assetSubtypeLinkService.getAssetSubtypeLinks({
        type_id__label: keyword,
        results: 5,
        is_active: true,
        app: this.hardwareAppPk
      }).subscribe((data) => {
        subTypeHardwareConf.source = parseSerionetRespToData(data);
      })
    };
    subTypeHardwareConf.displayKey = '__str__';

    let thermicPowerConf = this.createSubModelConf({
      label: "THERMIC_POWER", key: 'thermic_power',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let throughtputConf = this.createSubModelConf({
      label: "THROUGHTPUT", key: 'throughtput',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let totCavConf = this.createSubModelConf({
      label: "TOT_CAVITIES", key: 'tot_cavities',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let totHPConf = this.createSubModelConf({
      label: "TOT_HEATING_POWER", key: 'total_heating_power',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let versionConf = this.createSubModelConf({
      label: "VERSION", key: 'version',
      type: SubModelConfigurationFieldType.STRING,
    });

    let waterFlowRateConf = this.createSubModelConf({
      label: "WATER_FLOWRATE", key: 'water_flowrate',
      type: SubModelConfigurationFieldType.NUMBER
    });

    let onlySubTypeConf = new SubModelConfiguration();
    onlySubTypeConf.fields = [subTypeConf];

    this.assetSubModelConfiguration[ASSET_TYPES.ASSEMBLYING_MACHINE] = onlySubTypeConf;

    this.assetSubModelConfiguration[ASSET_TYPES.BUILDING] = onlySubTypeConf;

    let cimConf = new SubModelConfiguration();
    cimConf.fields = [mouldPlatesDim1Conf, mouldPlatesDim2Conf, clampForceConf,
      shootingVolumeConf, extruderConf, extruderDiamConf];
    this.assetSubModelConfiguration[ASSET_TYPES.CIM_MACHINE] = cimConf;

    let decConf = new SubModelConfiguration();
    decConf.fields = [subTypeConf, throughtputConf];
    this.assetSubModelConfiguration[ASSET_TYPES.DECORATION_MACHINE] = decConf;

    let ebmConf = new SubModelConfiguration();
    ebmConf.fields = [subTypeConf, manifoldConf, totCavConf, mouldPitchConf,
      angleConf, extruderConf, extruderDiamConf, mouldDimConf];
    this.assetSubModelConfiguration[ASSET_TYPES.EBM_MACHINE] = ebmConf;

    this.assetSubModelConfiguration[ASSET_TYPES.HANDLING_MACHINE] = onlySubTypeConf;

    let hardwareConf = new SubModelConfiguration();
    hardwareConf.fields = [subTypeHardwareConf];
    this.assetSubModelConfiguration[ASSET_TYPES.HARDWARE] = hardwareConf;

    let hotRunnerConf = new SubModelConfiguration();
    hotRunnerConf.fields = [subTypeConf, pitch1Conf, pitch2Conf, heatingZonesConf, totHPConf];
    this.assetSubModelConfiguration[ASSET_TYPES.HOT_RUNNER] = hotRunnerConf;

    let isbmConf = new SubModelConfiguration();
    isbmConf.fields = [shootingVolumeConf, extruderConf, extruderDiamConf];
    this.assetSubModelConfiguration[ASSET_TYPES.ISBM_MACHINE] = isbmConf;

    this.assetSubModelConfiguration[ASSET_TYPES.LABORATORY_EQUIPMENT] = onlySubTypeConf;

    this.assetSubModelConfiguration[ASSET_TYPES.PALLETIZATION_MACHINE] = onlySubTypeConf;

    this.assetSubModelConfiguration[ASSET_TYPES.PIM_MACHINE] = cimConf; //Same configuration

    this.assetSubModelConfiguration[ASSET_TYPES.PRODUCTION_ANCILLARIES] = onlySubTypeConf;

    let sbmConf = new SubModelConfiguration();
    sbmConf.fields = [subTypeConf, ovenPitchConf, mouldPitchConf, prefHeatingConf, neckOrConf];
    this.assetSubModelConfiguration[ASSET_TYPES.SBM_MACHINE] = sbmConf;

    let softConf = new SubModelConfiguration();
    softConf.fields = [subTypeConf, versionConf, licenseTypeConf, softwareTypeConf];
    this.assetSubModelConfiguration[ASSET_TYPES.SOFTWARE] = softConf;

    this.assetSubModelConfiguration[ASSET_TYPES.TESTING_MACHINE] = onlySubTypeConf;

    this.assetSubModelConfiguration[ASSET_TYPES.TOOLING_MACHINE] = onlySubTypeConf;

    this.assetSubModelConfiguration[ASSET_TYPES.TRIMMING_MACHINE] = onlySubTypeConf;

    let utConf = new SubModelConfiguration();
    utConf.fields = [subTypeConf, instElPoConf, caOutputConf, caMPConf, thermicPowerConf,
      waterFlowRateConf, pressureGapConf, kvaConf, genManSwiConf];

    this.assetSubModelConfiguration[ASSET_TYPES.UTILITIES] = utConf;

    let vehicleConf = new SubModelConfiguration();
    vehicleConf.fields = [subTypeConf, licensePlateConf];
    this.assetSubModelConfiguration[ASSET_TYPES.VEHICLES] = vehicleConf;
  }

  createSubModelConf(conf) {
    let sbConf = new SubModelConfigurationField();
    sbConf.label = this.tr(conf['label']);
    sbConf.model = this.asset.sub_type[conf['key']];
    sbConf.assignModel = (model) => {
      this.asset.sub_type[conf['key']] = model;
    };
    sbConf.type = conf['type'];
    if (sbConf.type == SubModelConfigurationFieldType.BOOLEAN) {
      if (!sbConf.model) {
        sbConf.model = false;
        this.asset.sub_type[conf['key']] = false
      }
    }
    return sbConf;
  }

  handleAssetTypeChange(callback?: any) {
    fixValue(this.assetTypes, this.asset, "type");
    this.asset.sub_type = new AssetSubModel();
    this.asset.sub_type.sub_type = new AssetSubtypeLink();
    this.assetSubtypeLinks = [];
    this.assetSubtypeLinkService.getAssetSubtypeLinks({
      app: this.asset.type.model_reference.pk,
      is_active: true,
      fieldname: 'sub_type'
    }).subscribe((data) => {
      let st = parseSerionetRespToData(data);
      this.assetSubModelConfiguration[this.getAssetType(this.asset)].fields.forEach((f: SubModelConfigurationField) => {
        if (f.label == this.tr("SUBTYPE")) {
          this.asset.sub_type.sub_type = new AssetSubtypeLink();
          f.model = this.asset.sub_type.sub_type.pk;
          f.options = st;
        }
        if (f.type == SubModelConfigurationFieldType.BOOLEAN) {
          f.assignModel(false);
          f.model = false;
        }
      });
      if (callback)
        callback();
    });
  }

  saveAsset() {
    this.loadingService.show();
    this.assetsService.saveAsset(this.asset).subscribe((response) => {
      this.loadingService.hide();
      this.manageRespSave(response, () => {
        if (this.handleCredentials) {
          this.networkInterfaces.forEach((ni) => {
            ni.asset_id = response;
          });
          this.processNetworkInterfaces(() => {
            this.assetUsers.forEach((au) => {
              au.asset_id = response;
            });
            this.processAssetUsers(() => {
              this.messageService.open({
                message: this.tr('SAVE_COMPLETED'),
                title: " ",
                autoClose: false,
                onClose: () => {
                  this.backEvent.emit();
                }
              });
            })
          })
        }
        else {
          this.backEvent.emit();
        }
      }, this.handleCredentials)
    })
  }

  processNetworkInterfaces(callback) {
    let niToSave = this.networkInterfaces.filter((ni) => {
      return ni.status != null;
    });
    if (niToSave.length > 0) {
      this.saveOrDeleteNi(niToSave, 0, callback);
    }
    else {
      callback();
    }
  }

  saveOrDeleteNi(niToSave, index, callback) {
    if (niToSave[index]) {
      let ni = niToSave[index];
      if (ni.status == 'toDelete') {
        if (ni.newly_created) {
          //creato ed eliminato, non faccio nulla
          this.saveOrDeleteNi(niToSave, index + 1, callback)
        }
        else {
          this.networkInterfacesService.deleteNetworkInterface(ni).subscribe((resp) => {
            this.manageRespDelete(resp, () => {
              this.saveOrDeleteNi(niToSave, index + 1, callback)
            }, true)
          });
        }
      }
      else {
        if (ni.newly_created) {
          delete ni['pk'];
          delete ni['newly_created'];
        }
        delete ni['status'];
        this.networkInterfacesService.saveNetworkInterface(ni).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.saveOrDeleteNi(niToSave, index + 1, callback)
          }, true)
        });
      }
    }
    else {
      callback();
    }
  }

  processAssetUsers(callback) {
    let auToSave = this.assetUsers.filter((au) => {
      return au.status !== null;
    });
    if (auToSave.length > 0) {
      this.saveOrDeleteAu(auToSave, 0, callback);
    }
    else {
      callback();
    }
  }

  saveOrDeleteAu(auToSave, index, callback) {
    if (auToSave[index]) {
      let au = auToSave[index];
      if (au.status == 'toDelete') {
        if (au.newly_created) {
          //creato ed eliminato, non faccio nulla
          this.saveOrDeleteNi(auToSave, index + 1, callback)
        }
        else {
          this.assetUsersService.deleteAssetUser(au).subscribe((resp) => {
            this.manageRespDelete(resp, () => {
              this.saveOrDeleteAu(auToSave, index + 1, callback)
            }, true)
          });
        }
      }
      else {
        if (au.newly_created) {
          delete au['pk'];
          delete au['newly_created'];
        }
        delete au['status'];
        this.assetUsersService.saveAssetUser(au).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.saveOrDeleteAu(auToSave, index + 1, callback)
          }, true)
        });
      }
    }
    else {
      callback();
    }
  }

  undoEditAsset() {
    this.backEvent.emit();
  }

  getAssetType(a: Asset) {
    return _getAssetType(a);
  }

  editNetworkInterface(ni: NetworkInterface) {
    this.networkInterface = cloneObject(ni);
    if (!this.networkInterface.status)
      this.networkInterface.status = "toEdit";
    this.networkInterfacesEditOn = true;
  }

  removeNetworkInterface(ni: NetworkInterface) {
    ni.status = "toDelete";
  }

  createNetworkInterface() {
    this.networkInterface = new NetworkInterface();
    this.networkInterface.type = new NetworkInterfaceType();
    this.networkInterface.socketservice_set = [];
    this.networkInterface.status = "toAdd";
    this.networkInterfacesEditOn = true;
    this.networkInterface.pk = (new Date()).getTime(); //per individuarlo nell'array
    this.networkInterface.newly_created = true;
  }

  handleNetworkTypeChange() {
    fixValue(this.networkInterfaceTypes, this.networkInterface, "type");
  }

  undoNetworkInterface() {
    this.networkInterface = new NetworkInterface();
    this.networkInterfacesEditOn = false;
  }

  saveNetworkInterface() {
    let index = null;
    this.networkInterface.socketservice_set = this.networkInterface.socketservice_set.filter((s) => {
      return s.service_name && s.port;
    });
    this.networkInterfaces.forEach((ni, i) => {
      if (ni.pk == this.networkInterface.pk) {
        index = i;
      }
    });
    if (index != null) {
      this.networkInterfaces[index] = cloneObject(this.networkInterface);
    }
    else {
      this.networkInterfaces.push(cloneObject(this.networkInterface))
    }
    this.networkInterfacesEditOn = false;
  }

  editAssetUser(au) {
    this.assetUser = cloneObject(au);
    this.assetUserEditOn = true;
    this.assetUser.status = "toEdit";
  }

  deleteAssetUser(au: AssetUser) {
    au.status = "toDelete";
  }

  createAssetUser() {
    this.assetUser = new AssetUser();
    this.assetUser.type = new AssetUserType();
    this.assetUserEditOn = true;
    this.assetUser.newly_created = true;
    this.assetUser.pk = (new Date()).getTime();
    this.assetUser.status = "toAdd";
  }

  undoAssetUser() {
    this.assetUser = new AssetUser();
    this.assetUserEditOn = false;
  }

  saveAssetUser() {
    let index = null;
    this.assetUsers.forEach((au, i) => {
      if (au.pk == this.assetUser.pk) {
        index = i;
      }
    });
    if (index != null) {
      this.assetUsers[index] = cloneObject(this.assetUser);
    }
    else {
      this.assetUsers.push(cloneObject(this.assetUser))
    }
    this.assetUserEditOn = false;
  }

  handleAssetUserTypeChange() {
    fixValue(this.assetUserTypes, this.assetUser, "type");
  }

  loadAssetNetworkInterfaces() {
    this.networkInterfacesService.getNetworkInterfaces({is_active: true, asset_id: this.asset.pk}).subscribe((data) => {
      this.networkInterfaces = parseSerionetRespToData(data);
    });
  }

  loadAssetUsers() {
    this.assetUsersService.getAssetUsers({is_active: true, asset_id: this.asset.pk}).subscribe((data) => {
      this.assetUsers = parseSerionetRespToData(data);
    });
  }

  printSocketServices(ni: NetworkInterface) {
    if (!ni.socketservice_set || ni.socketservice_set.length == 0) {
      return "-"
    }
    else {
      return ni.socketservice_set.map((s) => {
        if (s.__str__) {
          return s.__str__;
        }
        else {
          return s.service_name + " (" + s.port + ")";
        }
      }).join(" - ");
    }
  }

  addSocketToInterface(ni: NetworkInterface) {
    ni.socketservice_set.push(new SocketService());
  }

  canAddSocket(ni) {
    let hasEmptySocket = false;
    ni.socketservice_set.forEach((s) => {
      if (!s.service_name || !s.port)
        hasEmptySocket = true;
    });
    return !hasEmptySocket;
  }

  getActiveNI(ni: NetworkInterface[]) {
    return ni.filter((n) => {
      return n.status != "toDelete";
    })
  }

  getActiveAU(au: AssetUser[]) {
    return au.filter((n) => {
      return n.status != "toDelete";
    })
  }

  checkName() {
    if (!this.asset.pk) {
      if (this.asset.manufacturer.pk && this.asset.model) {
        this.asset.name = this.asset.manufacturer.__str__ + " " + this.asset.model;
      }
    }
  }

  ownerShipSelected(o: Organization) {
    this.asset.ownership = o;
  }

  manufacturerSelected(o: Organization) {
    this.asset.manufacturer = o;
    this.checkName();
  }

  selectedUserAsset(u: HRProfile) {
    this.asset.assigned_to = u;
  }
}
