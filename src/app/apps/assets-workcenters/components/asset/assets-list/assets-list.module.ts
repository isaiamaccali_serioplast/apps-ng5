import {NgModule} from "@angular/core";
import {AssetsListComponent} from "./assets-list.component";
import {SharedModule} from "../../../../../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [AssetsListComponent],   // components and directives
  exports:[AssetsListComponent],
  providers: []                    // services
})
export class AssetsListModule { }


