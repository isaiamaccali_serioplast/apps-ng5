import {Component, EventEmitter, OnInit, Output, ViewChild} from "@angular/core";
import {AssetType, AssetTypesService} from "../../../../_common/services/asset-types.service";
import {HeaderConf, TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {AssetSubtypeLink, AssetSubTypeLinkService} from "../../../../_common/services/asset-subtype-link.service";
import {cloneObject, parseSerionetRespToData} from "../../../../../shared/utils";
import {Asset, AssetsService, AssetSubModel} from "../../../../_common/services/assets.service";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {ContentTypeService} from "../../../../_common/services/content-type.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {Organization} from "../../../../_common/services/organization.service";
import {StatusLink} from "../../../../_common/services/status-link.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'assets-list',
  templateUrl: 'assets-list.component.html',
  styleUrls: ['assets-list.component.css']
})
export class AssetsListComponent extends TranslatableComponent implements OnInit {

  @Output('createEvent') createEvent = new EventEmitter();
  @Output('editEvent') editEvent = new EventEmitter();
  @Output('deleteEvent') deleteEvent = new EventEmitter();
  @Output('restoreEvent') restoreEvent = new EventEmitter();
  assetTypes: AssetType[] = [];
  assetTypePK: number;
  assetSubtypes: AssetSubtypeLink[] = [];
  assetSubtypePK: number;
  assetActive = true;
  @ViewChild('assetTable') assetTable: TableWithPagesComponent;
  availableActionsAssets = [];
  noItemsMessageAssets = this.tr("NO_ASSETS_AVAILABLE");
  headerConfAssets: HeaderConf[] = [
    {
      label: this.tr("TYPE"),
      columnId: 'type',
      valueKey: 'type',
      useStr: true
    },
    {
      label: this.tr("NAME"),
      columnId: 'name',
      valueKey: 'name'
    },
    {
      label: this.tr("LOCATION"),
      columnId: 'location',
      valueKey: 'location',
      useStr: true
    }
  ];

  text_search_asset: string;

  sitesAssetFilter: Site[] = [];
  siteAssetFilter: number;

  assetSubtypeLinks: AssetSubtypeLink[] = [];

  assetToShow: Asset;
  @ViewChild('assetViewDialog') assetViewDialog: DialogComponent;

  assetAppPk: number;

  constructor(private sitesService: SitesService, private assetTypeService: AssetTypesService,
              private assetSubtypeLinkService: AssetSubTypeLinkService, private contentTypeService: ContentTypeService,
              protected translationService: TranslationService,
              private assetsService: AssetsService) {
    super(translationService)
  }

  ngOnInit() {

    this.sitesService.getSites({is_active: true}).subscribe((data) => {
      this.sitesAssetFilter = parseSerionetRespToData((data));
      this.assetTypeService.getAssetTypes({is_active: true, ordering: 'name'}).subscribe((data) => {
        this.assetTypes = parseSerionetRespToData(data);
        this.initTableActions();
      });
    });

    this.contentTypeService.getContentTypes({is_active: true, model: 'asset'}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.assetAppPk = data['results'][0]['pk'];
      }
    });
  }

  initTableActions() {
    let actionEditAsset: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditAsset.click = this.editAsset.bind(this);
    let actionDeleteAsset: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteAsset.click = this.deleteAsset.bind(this);
    actionDeleteAsset.visibilityCondition = (item: Asset) => {
      return item.is_active;
    };
    let actionRestoreAsset: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreAsset.click = this.restoreAsset.bind(this);
    actionRestoreAsset.visibilityCondition = (item: Asset) => {
      return !item.is_active;
    };
    let actionViewAsset: TableRowAction = cloneObject(TableRowActions.VIEW);
    actionViewAsset.click = this.viewAsset.bind(this);
    this.availableActionsAssets = [actionEditAsset, actionDeleteAsset, actionRestoreAsset, actionViewAsset];
  }

  handleAssetTypeFilterChange() {
    this.assetSubtypeLinks = [];
    let modelPk = null;
    this.assetTypes.forEach((at) => {
      if (at.pk == this.assetTypePK) {
        modelPk = at.model_reference.pk;
      }
    });
    this.assetSubtypes = [];
    this.assetSubtypePK = null;
    this.assetSubtypeLinkService.getAssetSubtypeLinks({
      app: modelPk,
      is_active: true,
      fieldname: 'sub_type'
    }).subscribe((data) => {
      this.assetSubtypes = parseSerionetRespToData(data);
    });
    this.assetTable.reload();
  }

  handleAssetSubtypeFilterChange() {
    this.assetTable.reload();
  }

  assetSearchTimeout: any;

  assetSearchChange() {
    if (this.assetSearchTimeout)
      clearTimeout(this.assetSearchTimeout);
    this.assetSearchTimeout = setTimeout(() => {
      this.assetTable.reload();
    }, 500)
  }

  editAsset(a: Asset) {
    this.editEvent.emit(a);
  }

  deleteAsset(a: Asset) {
    this.deleteEvent.emit(a);
  }

  restoreAsset(a: Asset) {
    this.restoreEvent.emit(a);
  }

  loadAssets(page, nResults) {
    let query = {};
    query['results'] = nResults;
    if (!page) page = 1;
    query['page'] = page;
    query['is_active'] = this.assetActive;

    if (this.assetTypePK && this.assetSubtypePK) {
      query['getby_subtype'] = this.assetTypePK + "|" + this.assetSubtypePK;
    }
    else {
      if (this.assetTypePK)
        query['type'] = this.assetTypePK;
    }
    if (this.siteAssetFilter)
      query['location'] = this.siteAssetFilter;
    if (this.text_search_asset)
      query['search'] = this.text_search_asset || null;

    query['ordering'] = 'type,name';

    // this.loadingService.show();
    return this.assetsService.getAssets(query)
    //   .subscribe((data) => {
    //   this.loadingService.hide();
    //   // this.assetTable.handleResp(data);
    // })
  }

  createAsset() {
    let a = new Asset();
    a.manufacturer = new Organization();
    a.ownership = new Organization();
    a.status = new StatusLink();
    a.type = new AssetType();
    a.sub_type = new AssetSubModel();
    a.location = new Site();
    this.createEvent.emit(a);
  }

  viewAsset(a: Asset) {
    this.assetToShow = a;
    this.assetViewDialog.open();
  }
}
