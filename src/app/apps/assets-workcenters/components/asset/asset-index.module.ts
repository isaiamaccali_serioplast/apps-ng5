import {NgModule} from "@angular/core";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {AssetsListModule} from "./assets-list/assets-list.module";
import {AssetIndexComponent} from "./asset-index.component";
import {SharedModule} from "../../../../shared/shared.module";
import {AssetEditModule} from "./asset-edit/asset-edit.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule,AssetsListModule,AssetEditModule],       // module dependencies
  declarations: [AssetIndexComponent],   // components and directives
  exports:[AssetIndexComponent],
  providers: []                    // services
})
export class AssetIndexModule { }


