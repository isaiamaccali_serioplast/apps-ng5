import {NgModule} from "@angular/core";
import {AssetsWorkcenterIndexComponent} from "./assets-workcenters-index.component";
import {SharedModule} from "../../../../shared/shared.module";
import {AssetIndexModule} from "../asset/asset-index.module";
import {WorkcentersIndexModule} from "../workcenter/workcenters-index.module";

@NgModule({
  imports: [SharedModule, AssetIndexModule, WorkcentersIndexModule],       // module dependencies
  declarations: [AssetsWorkcenterIndexComponent],   // components and directives
  exports:[AssetsWorkcenterIndexComponent],
  providers: []                    // services
})
export class AssetsWorkcenterIndexModule { }


