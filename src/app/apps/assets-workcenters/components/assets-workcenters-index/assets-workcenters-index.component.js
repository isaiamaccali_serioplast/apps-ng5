"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var asset_1 = require("../../../_common/models/asset");
var workcenter_1 = require("../../../_common/models/workcenter");
var asset_type_1 = require("../../../_common/models/asset-type");
var organization_1 = require("../../../_common/models/organization");
var technology_link_1 = require("../../../_common/models/technology-link");
var utils_1 = require("../../../../shared/utils");
var table_with_pages_component_1 = require("../../../../shared/material/table-with-pages/table-with-pages.component");
var workcenter_location_1 = require("../../../_common/models/workcenter-location");
var status_link_1 = require("../../../_common/models/status-link");
var asset_subtype_link_1 = require("../../../_common/models/asset-subtype-link");
var asset_submodel_1 = require("../../../_common/models/asset-submodel");
var SubModelConfigurationFieldType = {
    'STRING': 'string',
    'NUMBER': 'number',
    'MULTIPLE': 'multiple',
    'BOOLEAN': 'boolean'
};
var SubModelConfigurationField = (function () {
    function SubModelConfigurationField() {
    }
    return SubModelConfigurationField;
}());
var SubModelConfiguration = (function () {
    function SubModelConfiguration() {
    }
    return SubModelConfiguration;
}());
var AssetsWorkcenterIndexComponent = (function () {
    function AssetsWorkcenterIndexComponent(sideNavService, titleService, translationService, messageService, loadingService, workcentersService, technologyLinkService, sitesService, assetsService, workcenterLocationsService, hrProfileService, assetTypeService, organizationService, statusLinkService, assetSubtypeLinkService) {
        this.sideNavService = sideNavService;
        this.titleService = titleService;
        this.translationService = translationService;
        this.messageService = messageService;
        this.loadingService = loadingService;
        this.workcentersService = workcentersService;
        this.technologyLinkService = technologyLinkService;
        this.sitesService = sitesService;
        this.assetsService = assetsService;
        this.workcenterLocationsService = workcenterLocationsService;
        this.hrProfileService = hrProfileService;
        this.assetTypeService = assetTypeService;
        this.organizationService = organizationService;
        this.statusLinkService = statusLinkService;
        this.assetSubtypeLinkService = assetSubtypeLinkService;
        this.assetActive = true;
        this.availableActionsAssets = [];
        this.noItemsMessageAssets = this.tr("NO_ASSETS_AVAILABLE");
        this.headerConfAssets = [
            {
                label: this.tr("TYPE"),
                valueKey: 'type',
                useStr: true
            },
            {
                label: this.tr("DESCRIPTION"),
                valueKey: 'description'
            },
            {
                label: this.tr("MODEL"),
                valueKey: 'model'
            },
            {
                label: this.tr("SAP_CODE"),
                valueKey: 'sap_code'
            },
            {
                label: this.tr("LOCATION"),
                valueKey: 'location',
                useStr: true
            },
            {
                label: this.tr("STATUS"),
                valueKey: 'status',
                useStr: true
            }
        ];
        this.sitesAssetFilter = [];
        this.availableActionsWorkcenters = [];
        this.noItemsMessageWorkcenters = this.tr("NO_WORKCENTER_AVAILABLE");
        this.headerConfWorkcenters = [
            {
                label: this.tr("DESCRIPTION"),
                valueKey: 'description'
            },
            {
                label: this.tr("SAP_CODE"),
                valueKey: 'sap_code'
            },
            {
                label: this.tr("TECHNOLOGY"),
                valueKey: 'technology',
                useStr: true
            }
        ];
        this.asset = new asset_1.Asset();
        this.assets = [];
        this.workcenterLocations = [];
        this.workcenterLocationsInactive = [];
        this.technologies = [];
        this.sites = [];
        this.sitesMap = {};
        this.workcentersActive = true;
        this.workcentersEditOn = false;
        this.locations = [];
        this.locations_asset = [];
        this.assetEditOn = false;
        this.assetTypes = [];
        this.workcenterLocationToRemove = [];
        this.workcenterLocationToAdd = [];
        this.workcenters_asset = [];
        this.users_asset = [];
        this.manufacturers = [];
        this.organizations = [];
        this.statusLinks = [];
        this.ASSET_TYPES_APP = asset_type_1.ASSET_TYPES;
        this.ASSET_TYPES_APP_KEYS = [];
        this.assetSubtypeLinks = [];
        this.assetSubModelConfiguration = {};
        this.SubModelConfigurationFieldType = {};
        this.sideNavService.leftMenuEnabled = false;
        this.sideNavService.leftMenu = false;
        this.sideNavService.appMenuEnabled = true;
        this.titleService.setTitle("Serioplast - Assets & Workcenters");
        this.ASSET_TYPES_APP_KEYS = Object.keys(this.ASSET_TYPES_APP);
        this.SubModelConfigurationFieldType = SubModelConfigurationFieldType;
    }
    AssetsWorkcenterIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initAsset();
        this.initWorkcenter();
        this.technologyLinkService.getTechnologyLinks({ is_active: true, app_name: 'workcenter' }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.technologies = data['results'];
            }
        });
        this.sitesService.getSitesList({}).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.sites = data['results'];
                var self_1 = _this;
                _this.sites.forEach(function (s) {
                    self_1.sitesMap[s.pk] = s;
                });
            }
        });
        this.assetTypeService.getAssetTypes({ is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.assetTypes = data['results'];
            }
        });
        this.organizationService.getOrganizations({ is_active: true, is_supplier: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.manufacturers = data['results'];
            }
        });
        this.organizationService.getOrganizations({ is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.organizations = data['results'];
            }
        });
        this.initTableActions();
        this.statusLinkService.getStatusLinks({ app: 126, fieldname: 'status' }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.statusLinks = data['results'];
            }
        });
        this.initAssetSubtypeConfiguration();
    };
    AssetsWorkcenterIndexComponent.prototype.getFieldName = function (f) {
        return f.label.split(" ").join("_").toLowerCase();
    };
    AssetsWorkcenterIndexComponent.prototype.initAssetSubtypeConfiguration = function () {
        var subTypeConf = new SubModelConfigurationField();
        subTypeConf.label = this.tr("SUBTYPE");
        subTypeConf.model = this.asset.sub_type.sub_type.pk;
        subTypeConf.type = SubModelConfigurationFieldType.MULTIPLE;
        subTypeConf.options = this.assetSubtypeLinks;
        var onlySubTypeConf = new SubModelConfiguration();
        var onlySubTypeConfFields = [];
        onlySubTypeConf.fields = onlySubTypeConfFields;
        onlySubTypeConfFields.push(subTypeConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.ASSEMBLYING_MACHINE] = onlySubTypeConf;
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.BUILDING] = onlySubTypeConf;
        var carConf = new SubModelConfiguration();
        var carConfFields = [];
        carConf.fields = carConfFields;
        var lpConf = new SubModelConfigurationField();
        lpConf.label = this.tr("LICENSE_PLATE");
        lpConf.model = this.asset.sub_type.license_plate;
        lpConf.type = SubModelConfigurationFieldType.STRING;
        carConfFields.push(lpConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.CAR] = carConf;
        var cimConf = new SubModelConfiguration();
        var cimConfFields = [];
        cimConf.fields = cimConfFields;
        var mouldDimConf = new SubModelConfigurationField();
        mouldDimConf.label = this.tr("MOULD_DIMENSION");
        mouldDimConf.model = this.asset.sub_type.mould_dim;
        mouldDimConf.type = SubModelConfigurationFieldType.STRING;
        cimConfFields.push(mouldDimConf);
        var clampForceConf = new SubModelConfigurationField();
        clampForceConf.label = this.tr("CLAMPING_FORCE");
        clampForceConf.model = this.asset.sub_type.clamping_force;
        clampForceConf.type = SubModelConfigurationFieldType.NUMBER;
        cimConfFields.push(clampForceConf);
        var shootingVolumeConf = new SubModelConfigurationField();
        shootingVolumeConf.label = this.tr("SHOOTING_VOLUME");
        shootingVolumeConf.model = this.asset.sub_type.shooting_volume;
        shootingVolumeConf.type = SubModelConfigurationFieldType.NUMBER;
        cimConfFields.push(shootingVolumeConf);
        var extruderConf = new SubModelConfigurationField();
        extruderConf.label = this.tr("EXTRUDER");
        extruderConf.model = this.asset.sub_type.extruder;
        extruderConf.type = SubModelConfigurationFieldType.NUMBER;
        cimConfFields.push(extruderConf);
        var extruderDiamConf = new SubModelConfigurationField();
        extruderDiamConf.label = this.tr("EXTRUDER_DIAM");
        extruderDiamConf.model = this.asset.sub_type.extruder_diam;
        extruderDiamConf.type = SubModelConfigurationFieldType.STRING;
        cimConfFields.push(extruderDiamConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.CIM_MACHINE] = cimConf;
        var decConf = new SubModelConfiguration();
        var decConfFields = [];
        decConf.fields = decConfFields;
        decConfFields.push(subTypeConf);
        var throughtputConf = new SubModelConfigurationField();
        throughtputConf.label = this.tr("THROUGHTPUT");
        throughtputConf.model = this.asset.sub_type.throughtput;
        throughtputConf.type = SubModelConfigurationFieldType.NUMBER;
        decConfFields.push(throughtputConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.DECORATION_MACHINE] = cimConf;
        var ebmConf = new SubModelConfiguration();
        var ebmConfFields = [];
        ebmConf.fields = decConfFields;
        ebmConfFields.push(subTypeConf);
        var manifoldConf = new SubModelConfigurationField();
        manifoldConf.label = this.tr("MANIFOLD");
        manifoldConf.model = this.asset.sub_type.manifold;
        manifoldConf.type = SubModelConfigurationFieldType.NUMBER;
        ebmConfFields.push(manifoldConf);
        var totCavConf = new SubModelConfigurationField();
        totCavConf.label = this.tr("TOT_CAVITES");
        totCavConf.model = this.asset.sub_type.tot_cavities;
        totCavConf.type = SubModelConfigurationFieldType.NUMBER;
        ebmConfFields.push(totCavConf);
        var mouldPitchConf = new SubModelConfigurationField();
        mouldPitchConf.label = this.tr("MOULD_PITCH");
        mouldPitchConf.model = this.asset.sub_type.mould_pitch;
        mouldPitchConf.type = SubModelConfigurationFieldType.NUMBER;
        ebmConfFields.push(mouldPitchConf);
        var angleConf = new SubModelConfigurationField();
        angleConf.label = this.tr("ANGLE");
        angleConf.model = this.asset.sub_type.angle;
        angleConf.type = SubModelConfigurationFieldType.NUMBER;
        ebmConfFields.push(angleConf);
        ebmConfFields.push(extruderConf);
        ebmConfFields.push(extruderDiamConf);
        ebmConfFields.push(mouldDimConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.EBM_MACHINE] = ebmConf;
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.HANDLING_MACHINE] = onlySubTypeConf;
        var hardwareConf = new SubModelConfiguration();
        var hardwareConfFields = [];
        hardwareConf.fields = hardwareConfFields;
        var macAddressConf = new SubModelConfigurationField();
        macAddressConf.label = this.tr("MAC_ADDRESS");
        macAddressConf.model = this.asset.sub_type.mac_address;
        macAddressConf.type = SubModelConfigurationFieldType.STRING;
        hardwareConfFields.push(macAddressConf);
        var ipAddressConf = new SubModelConfigurationField();
        ipAddressConf.label = this.tr("IP_ADDRESS");
        ipAddressConf.model = this.asset.sub_type.ip_address;
        ipAddressConf.type = SubModelConfigurationFieldType.STRING;
        hardwareConfFields.push(ipAddressConf);
        var noteConf = new SubModelConfigurationField();
        noteConf.label = this.tr("NOTE");
        noteConf.model = this.asset.sub_type.note;
        noteConf.type = SubModelConfigurationFieldType.STRING;
        hardwareConfFields.push(noteConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.HARDWARE] = hardwareConf;
        var isbmConf = new SubModelConfiguration();
        var isbmConfFields = [];
        isbmConf.fields = isbmConfFields;
        isbmConfFields.push(shootingVolumeConf);
        isbmConfFields.push(extruderConf);
        isbmConfFields.push(extruderDiamConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.ISBM_MACHINE] = isbmConf;
        var labToolConf = new SubModelConfiguration();
        var labToolConfFields = [];
        labToolConf.fields = labToolConfFields;
        //TODO MISSING TYPE?
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.LAB_TOOL] = labToolConf;
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.PALLETIZATION_MACHINE] = onlySubTypeConf;
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.PIM_MACHINE] = cimConf; //Same configuration
        var sbmConf = new SubModelConfiguration();
        var sbmConfFields = [];
        sbmConf.fields = sbmConfFields;
        sbmConfFields.push(subTypeConf);
        var ovenPitchConf = new SubModelConfigurationField();
        ovenPitchConf.label = this.tr("OVEN_PITCH");
        ovenPitchConf.model = this.asset.sub_type.oven_pitch;
        ovenPitchConf.type = SubModelConfigurationFieldType.NUMBER;
        sbmConfFields.push(ovenPitchConf);
        sbmConfFields.push(mouldPitchConf);
        var prefHeatConf = new SubModelConfigurationField();
        prefHeatConf.label = this.tr("PREFERENTIAL_HEATING");
        prefHeatConf.model = this.asset.sub_type.preferential_heating;
        prefHeatConf.type = SubModelConfigurationFieldType.BOOLEAN;
        sbmConfFields.push(prefHeatConf);
        var neckOrConf = new SubModelConfigurationField();
        neckOrConf.label = this.tr("NECK_ORIENTATION");
        neckOrConf.model = this.asset.sub_type.neck_orientation;
        neckOrConf.type = SubModelConfigurationFieldType.BOOLEAN;
        sbmConfFields.push(neckOrConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.SBM_MACHINE] = sbmConf;
        var softConf = new SubModelConfiguration();
        var softConfFields = [];
        softConf.fields = softConfFields;
        //TODO MISSING TYPE?
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.SOFTWARE] = softConf;
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.TESTING_MACHINE] = onlySubTypeConf;
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.TRIMMING_MACHINE] = onlySubTypeConf;
        var utConf = new SubModelConfiguration();
        var utConfFields = [];
        utConf.fields = utConfFields;
        utConfFields.push(subTypeConf);
        var instElPoConf = new SubModelConfigurationField();
        instElPoConf.label = this.tr("INSTALLED_ELECTRICAL_POWER");
        instElPoConf.model = this.asset.sub_type.installed_electrical_power;
        instElPoConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(instElPoConf);
        var caOutputConf = new SubModelConfigurationField();
        caOutputConf.label = this.tr("CA_OUTPUT");
        caOutputConf.model = this.asset.sub_type.ca_output;
        caOutputConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(caOutputConf);
        var caMPConf = new SubModelConfigurationField();
        caMPConf.label = this.tr("CA_MAX_PRESSURE");
        caMPConf.model = this.asset.sub_type.ca_max_pressure;
        caMPConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(caMPConf);
        var thermicConf = new SubModelConfigurationField();
        thermicConf.label = this.tr("THERMIC_POWER");
        thermicConf.model = this.asset.sub_type.thermic_power;
        thermicConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(thermicConf);
        var waterFlowRateConf = new SubModelConfigurationField();
        waterFlowRateConf.label = this.tr("WATER_FLOWRATE");
        waterFlowRateConf.model = this.asset.sub_type.water_flowrate;
        waterFlowRateConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(waterFlowRateConf);
        var pressureGapConf = new SubModelConfigurationField();
        pressureGapConf.label = this.tr("WATER_FLOWRATE");
        pressureGapConf.model = this.asset.sub_type.pressure_gap;
        pressureGapConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(pressureGapConf);
        var kvaConf = new SubModelConfigurationField();
        kvaConf.label = this.tr("KVA_TRAFO");
        kvaConf.model = this.asset.sub_type.kva_trafo;
        kvaConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(kvaConf);
        var genManSwiConf = new SubModelConfigurationField();
        genManSwiConf.label = this.tr("GENERAL_MAIN_SWITCH");
        genManSwiConf.model = this.asset.sub_type.general_main_switch;
        genManSwiConf.type = SubModelConfigurationFieldType.NUMBER;
        utConfFields.push(genManSwiConf);
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.UTILITIES] = utConf;
        var utRAConf = new SubModelConfiguration();
        var utRAConfFields = [];
        utRAConf.fields = utRAConfFields;
        //TODO MISSING TYPE?
        this.assetSubModelConfiguration[asset_type_1.ASSET_TYPES.UTILITIES_RESIN_ADDITIVES] = utRAConf;
    };
    AssetsWorkcenterIndexComponent.prototype.initTableActions = function () {
        var actionEditAsset = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.EDIT);
        actionEditAsset.click = this.editAsset.bind(this);
        var actionDeleteAsset = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.DELETE);
        actionDeleteAsset.click = this.deleteAsset.bind(this);
        actionDeleteAsset.visibilityCondition = function (item) { return item.is_active; };
        // let actionRestoreAsset:TableRowAction = cloneObject(TableRowActions.RESTORE);
        // actionRestoreAsset.click = this.restoreAsset.bind(this);
        // actionRestoreAsset.visibilityCondition = function(item:Asset){return !item.is_active;};
        this.availableActionsAssets = [actionEditAsset, actionDeleteAsset,];
        var actionEditWorkcenter = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.EDIT);
        actionEditWorkcenter.click = this.editWorkcenter.bind(this);
        var actionDeleteWorkcenter = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.DELETE);
        actionDeleteWorkcenter.click = this.deleteWorkcenter.bind(this);
        actionDeleteWorkcenter.visibilityCondition = function (item) { return item.is_active; };
        var actionRestoreWorkcenter = utils_1.cloneObject(table_with_pages_component_1.TableRowActions.RESTORE);
        actionRestoreWorkcenter.click = this.restoreWorkcenter.bind(this);
        actionRestoreWorkcenter.visibilityCondition = function (item) { return !item.is_active; };
        this.availableActionsWorkcenters = [actionEditWorkcenter, actionDeleteWorkcenter, actionRestoreWorkcenter];
    };
    AssetsWorkcenterIndexComponent.prototype.initAsset = function () {
        this.asset = new asset_1.Asset();
        this.asset.type = new asset_type_1.AssetType();
        this.asset.manufacturer = new organization_1.Organization();
        // this.asset.workcenter = new Workcenter();
        // this.asset.location = new Site();
        this.asset.ownership = new organization_1.Organization();
        this.asset.status = new status_link_1.StatusLink();
        this.asset.sub_type = new asset_submodel_1.AssetSubModel();
        this.asset.sub_type.sub_type = new asset_subtype_link_1.AssetSubtypeLink();
    };
    AssetsWorkcenterIndexComponent.prototype.handleAssetTypeFilterChange = function () {
        this.assetTable.reload();
    };
    AssetsWorkcenterIndexComponent.prototype.handleAssetTypeChange = function () {
        var _this = this;
        utils_1.fixValue(this.assetTypes, this.asset, "type");
        this.asset.sub_type = new asset_submodel_1.AssetSubModel();
        this.asset.sub_type.sub_type = new asset_subtype_link_1.AssetSubtypeLink();
        this.assetSubtypeLinks = [];
        this.assetSubtypeLinkService.getAssetSubtypeLinks({ app: this.asset.type.model_reference.pk, is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.assetSubtypeLinks = data['results'];
            }
        });
    };
    AssetsWorkcenterIndexComponent.prototype.assetSearchChange = function () {
        if (this.assetSearchTimeout)
            clearTimeout(this.assetSearchTimeout);
        var self = this;
        this.assetSearchTimeout = setTimeout(function () {
            self.assetTable.reload();
        }, 500);
    };
    AssetsWorkcenterIndexComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    AssetsWorkcenterIndexComponent.prototype.saveAsset = function () {
        var _this = this;
        this.loadingService.show();
        this.assetsService.saveAsset(this.asset).subscribe(function (response) {
            _this.loadingService.hide();
            var self = _this;
            _this.handleSaveOrUpdate(response, function () {
                self.assetEditOn = false;
                self.initAsset();
            });
        });
    };
    AssetsWorkcenterIndexComponent.prototype.editAsset = function (a) {
        this.asset = utils_1.cloneObject(a);
        this.assetEditOn = true;
    };
    AssetsWorkcenterIndexComponent.prototype.deleteAsset = function (a) {
        var self = this;
        this.messageService.open({
            title: this.tr("ATTENTION"),
            message: this.tr("CONFIRM_DELETE"),
            confirm: function () {
                self.loadingService.show();
                self.assetsService.deleteAsset(a).subscribe(function (resp) {
                    self.loadingService.hide();
                    self.handleDelete(resp, function () {
                        self.assetTable.reload();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    AssetsWorkcenterIndexComponent.prototype.editWorkcenter = function (w) {
        var _this = this;
        var workcenter = utils_1.cloneObject(w);
        if (!workcenter.technology)
            workcenter.technology = new technology_link_1.TechnologyLink();
        this.workcenter = workcenter;
        this.workcentersEditOn = true;
        this.workcenterLocationsService.getWorkcenterLocations({ workcenter_id__id: this.workcenter.pk, is_active: true }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.workcenterLocations = data['results'];
            }
            else
                _this.workcenterLocations = [];
        });
        this.workcenterLocationsService.getWorkcenterLocations({ workcenter_id__id: this.workcenter.pk, is_active: false }).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.workcenterLocationsInactive = data['results'];
            }
            else
                _this.workcenterLocationsInactive = [];
        });
    };
    AssetsWorkcenterIndexComponent.prototype.initWorkcenter = function () {
        var workcenter = new workcenter_1.Workcenter();
        workcenter.technology = new technology_link_1.TechnologyLink();
        this.workcenter = workcenter;
    };
    AssetsWorkcenterIndexComponent.prototype.deleteWorkcenter = function (w) {
        var self = this;
        this.messageService.open({
            title: this.tr("ATTENTION"),
            message: this.tr("CONFIRM_DELETE"),
            confirm: function () {
                self.loadingService.show();
                self.workcentersService.deleteWorkcenter(w).subscribe(function (resp) {
                    self.loadingService.hide();
                    self.handleDelete(resp, function () {
                        self.workCentersTable.reload();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    AssetsWorkcenterIndexComponent.prototype.restoreWorkcenter = function (w) {
        var self = this;
        this.messageService.open({
            title: this.tr("ATTENTION"),
            message: this.tr("RESTORE_CONFIRM"),
            confirm: function () {
                self.loadingService.show();
                self.workcentersService.restoreWorkcenter(w).subscribe(function (resp) {
                    self.loadingService.hide();
                    self.handleSaveOrUpdate(resp, function () {
                        self.workCentersTable.reload();
                    });
                });
            },
            deny: function () {
            }
        });
    };
    AssetsWorkcenterIndexComponent.prototype.loadWorkcenters = function (page, nResults) {
        var _this = this;
        var query = {};
        query['results'] = nResults;
        if (!page)
            page = 1;
        query['page'] = page;
        query['is_active'] = this.workcentersActive;
        this.loadingService.show();
        this.workcentersService.getWorkcenters(query).subscribe(function (data) {
            _this.loadingService.hide();
            _this.workCentersTable.handleResp(data);
        });
    };
    AssetsWorkcenterIndexComponent.prototype.loadAssets = function (page, nResults) {
        var _this = this;
        var query = {};
        query['results'] = nResults;
        if (!page)
            page = 1;
        query['page'] = page;
        query['is_active'] = this.assetActive;
        query['type'] = this.assetTypePK;
        query['location'] = this.siteAssetFilter;
        query['search'] = this.text_search_asset;
        this.loadingService.show();
        this.assetsService.getAssets(query).subscribe(function (data) {
            _this.loadingService.hide();
            _this.assetTable.handleResp(data);
        });
    };
    AssetsWorkcenterIndexComponent.prototype.saveWorkcenter = function () {
        var self = this;
        this.loadingService.show();
        this.workcentersService.saveWorkcenter(this.workcenter).subscribe(function (resp) {
            self.handleSaveOrUpdate(resp, function () {
                self.workcenterLocationToAdd.forEach(function (wcl) {
                    wcl.workcenter_id.pk = resp.pk;
                });
                self.saveWorkcenterLocations(function (resp_) {
                    self.loadingService.hide();
                    self.handleSaveOrUpdate(resp_, function () {
                        self.initWorkcenter();
                        self.workcentersEditOn = false;
                    });
                });
            }, true);
        });
    };
    AssetsWorkcenterIndexComponent.prototype.handleTechnologyChange = function () {
        utils_1.fixValue(this.technologies, this.workcenter, 'technology');
    };
    AssetsWorkcenterIndexComponent.prototype.handleDelete = function (resp, callback) {
        var self = this;
        if (resp.status == 204) {
            self.messageService.open({
                title: " ",
                message: self.tr('DELETE_COMPLETED'),
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    AssetsWorkcenterIndexComponent.prototype.handleSaveOrUpdate = function (resp, callback, skipMessage) {
        var self = this;
        if (resp.pk) {
            if (!skipMessage) {
                self.messageService.open({
                    message: self.tr('SAVE_COMPLETED'),
                    title: " ",
                    autoClose: false,
                    onClose: function () {
                        if (callback)
                            callback();
                    }
                });
            }
            else {
                if (callback)
                    callback();
            }
        }
        else {
            resp.context = "SERIONET";
            this.messageService.open({
                title: 'Error',
                error: true,
                message: utils_1.buildErrorMessage(resp),
                autoClose: false
            });
        }
    };
    AssetsWorkcenterIndexComponent.prototype.createWorkcenter = function () {
        this.initWorkcenter();
        this.workcentersEditOn = true;
    };
    AssetsWorkcenterIndexComponent.prototype.undoEditWorkcenter = function () {
        this.initWorkcenter();
        this.workcentersEditOn = false;
    };
    AssetsWorkcenterIndexComponent.prototype.getSitesToAddAsset = function (keyword) {
        var _this = this;
        this.sitesService.getSitesList({ name_icontains: keyword, results: 5, is_active: true }).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.locations_asset = data['results'];
        });
    };
    AssetsWorkcenterIndexComponent.prototype.getSitesToAdd = function (keyword) {
        var _this = this;
        this.sitesService.getSitesList({ name_icontains: keyword, results: 5, is_active: true }).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.locations = data['results'];
        });
    };
    AssetsWorkcenterIndexComponent.prototype.searchAssetsSitesFilter = function (keyword) {
        var _this = this;
        this.sitesService.getSitesList({ name_icontains: keyword, results: 5, is_active: true }).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.sitesAssetFilter = data['results'];
            else
                _this.sitesAssetFilter = [];
        });
    };
    AssetsWorkcenterIndexComponent.prototype.getWorkcentersAsset = function (keyword) {
        var _this = this;
        this.workcentersService.getWorkcenters({ name_icontains: keyword, results: 5, is_active: true }).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.workcenters_asset = data['results'];
        });
    };
    AssetsWorkcenterIndexComponent.prototype.selectedWorkcenterAsset = function (w) {
        if (w && w.pk) {
            this.asset.workcenter = w;
        }
        else {
            this.asset.workcenter = null;
        }
    };
    AssetsWorkcenterIndexComponent.prototype.getUsersAsset = function (keyword) {
        var _this = this;
        this.hrProfileService.getHRProfiles({ full_name_icontains: keyword, results: 5, is_active: true, is_employee: true }).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.users_asset = data['results'];
        });
    };
    AssetsWorkcenterIndexComponent.prototype.selectedUserAsset = function (hrp) {
        if (hrp && hrp.pk) {
            this.asset.assigned_to = hrp;
        }
        else {
            this.asset.assigned_to = null;
        }
    };
    AssetsWorkcenterIndexComponent.prototype.selectedSiteAssetFilter = function (s) {
        if (s && s.pk) {
            this.siteAssetFilter = s.pk;
        }
        else {
            this.siteAssetFilter = null;
        }
        this.assetTable.reload();
    };
    AssetsWorkcenterIndexComponent.prototype.selectedLocation = function (s) {
        if (s && s.pk) {
            this.siteToAdd = s;
        }
        else {
            this.siteToAdd = null;
        }
    };
    AssetsWorkcenterIndexComponent.prototype.selectedLocationAsset = function (s) {
        if (s && s.pk) {
            this.asset.location = s;
        }
        else {
            this.asset.location = null;
        }
    };
    AssetsWorkcenterIndexComponent.prototype.createWorkcenterLocation = function () {
        var wcl = new workcenter_location_1.WorkcenterLocation();
        wcl.location = this.siteToAdd;
        wcl.workcenter_id = this.workcenter;
        if (this.workcenterLocations.length > 0) {
            var act = this.workcenterLocations.splice(0, 1)[0];
            this.workcenterLocationToRemove.push(act);
            this.workcenterLocationsInactive.push(act);
        }
        this.workcenterLocations.push(wcl);
        this.workcenterLocationToAdd.push(wcl);
        this.siteToAdd = null;
    };
    AssetsWorkcenterIndexComponent.prototype.removeWorkcenterLocation = function (wcl) {
        this.workcenterLocations.splice(this.workcenterLocations.indexOf(wcl), 1);
        if (wcl.pk) {
            if (this.workcenterLocationToRemove.indexOf(wcl) == -1)
                this.workcenterLocationToRemove.push(wcl);
        }
        if (this.workcenterLocationsInactive.indexOf(wcl) == -1)
            this.workcenterLocationsInactive.push(wcl);
    };
    AssetsWorkcenterIndexComponent.prototype.createAsset = function () {
        this.initAsset();
        this.assetEditOn = true;
    };
    AssetsWorkcenterIndexComponent.prototype.undoEditAsset = function () {
        this.initAsset();
        this.assetEditOn = false;
    };
    AssetsWorkcenterIndexComponent.prototype.restoreWorkcenterLocation = function (wcl) {
        if (this.workcenterLocations.length > 0) {
            var act = this.workcenterLocations.splice(0, 1)[0];
            this.workcenterLocationToRemove.push(act);
            this.workcenterLocationsInactive.push(act);
        }
        this.workcenterLocationsInactive.splice(this.workcenterLocationsInactive.indexOf(wcl), 1);
        this.workcenterLocations.push(wcl);
        this.workcenterLocationToAdd.push(wcl);
    };
    AssetsWorkcenterIndexComponent.prototype.saveWorkcenterLocations = function (callback) {
        var self = this;
        self.saveWorkcenterLocationsToDelete(function (response) {
            if (response.status == 204) {
                self.saveWorkcenterLocationsToCreate(function (response_) {
                    if (response_.pk) {
                        callback.call(null, response_);
                    }
                    else {
                        callback.call(null, response_);
                    }
                });
            }
            else {
                callback.call(null, response);
            }
        });
    };
    AssetsWorkcenterIndexComponent.prototype.saveWorkcenterLocationsToDelete = function (callback) {
        this.deleteWorkcenterLocations(0, callback);
    };
    AssetsWorkcenterIndexComponent.prototype.deleteWorkcenterLocations = function (index, callback, lastResponse) {
        var _this = this;
        if (this.workcenterLocationToRemove[index]) {
            this.workcenterLocationsService.deleteWorkcenterLocation(this.workcenterLocationToRemove[index]).subscribe(function (response) {
                if (response.status == 204) {
                    _this.deleteWorkcenterLocations(index + 1, callback, response);
                }
                else {
                    callback.call(null, response);
                }
            });
        }
        else {
            if (!lastResponse)
                lastResponse = { status: 204 };
            this.workcenterLocationToRemove = [];
            callback(lastResponse);
        }
    };
    AssetsWorkcenterIndexComponent.prototype.saveWorkcenterLocationsToCreate = function (callback) {
        this.addWorkcenterLocations(0, callback);
    };
    AssetsWorkcenterIndexComponent.prototype.addWorkcenterLocations = function (index, callback, lastResponse) {
        var _this = this;
        if (this.workcenterLocationToAdd[index]) {
            this.workcenterLocationsService.saveWorkcenterLocation(this.workcenterLocationToAdd[index]).subscribe(function (response) {
                if (response.pk) {
                    _this.addWorkcenterLocations(index + 1, callback, response);
                }
                else {
                    callback.call(null, response);
                }
            });
        }
        else {
            if (!lastResponse)
                lastResponse = { pk: 999 };
            this.workcenterLocationToAdd = [];
            callback(lastResponse);
        }
    };
    AssetsWorkcenterIndexComponent.prototype.handleManufacturerChange = function () {
        utils_1.fixValue(this.manufacturers, this.asset, 'manufacturer');
    };
    AssetsWorkcenterIndexComponent.prototype.getAssetType = function (a) {
        return asset_type_1.getAssetType(a);
    };
    AssetsWorkcenterIndexComponent.prototype.assetHasSubtypeOnly = function (a) {
        var at = asset_type_1.getAssetType(a);
        return at == asset_type_1.ASSET_TYPES.BUILDING ||
            at == asset_type_1.ASSET_TYPES.PALLETIZATION_MACHINE ||
            at == asset_type_1.ASSET_TYPES.TESTING_MACHINE ||
            at == asset_type_1.ASSET_TYPES.TRIMMING_MACHINE ||
            at == asset_type_1.ASSET_TYPES.ASSEMBLYING_MACHINE ||
            at == asset_type_1.ASSET_TYPES.HANDLING_MACHINE;
    };
    return AssetsWorkcenterIndexComponent;
}());
__decorate([
    core_1.ViewChild('assetTable')
], AssetsWorkcenterIndexComponent.prototype, "assetTable", void 0);
__decorate([
    core_1.ViewChild('workcentersTable')
], AssetsWorkcenterIndexComponent.prototype, "workCentersTable", void 0);
AssetsWorkcenterIndexComponent = __decorate([
    core_1.Component({
        selector: 'app-assets-workcenter-index',
        templateUrl: 'assets-workcenters-index.component.html',
        styleUrls: ['assets-workcenters-index.component.css']
    })
], AssetsWorkcenterIndexComponent);
exports.AssetsWorkcenterIndexComponent = AssetsWorkcenterIndexComponent;
