import {Component, ViewChild} from "@angular/core";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Title} from "@angular/platform-browser";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {MatSnackBar, MatTabGroup} from "@angular/material";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {ConfigurationService} from "../../../../shared/serionet/service/configuration.service";
import {Router} from "@angular/router";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'app-assets-workcenter-index',
  templateUrl: 'assets-workcenters-index.component.html',
  styleUrls: ['assets-workcenters-index.component.css']
})
export class AssetsWorkcenterIndexComponent extends TranslatableComponent{

  @ViewChild('tab') tab: MatTabGroup;
  selectedIndex = 0;

  constructor(private sideNavService: SideNavService, private titleService: Title,
              protected translationService: TranslationService, private appPlantUsersService: AppPlantUsersService,
              private confService: ConfigurationService, private router: Router, private snackBar: MatSnackBar) {
    super(translationService);
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.sideNavService.appMenuEnabled = true;
    this.titleService.setTitle("Serioplast - Assets & Workcenters");

    let query = {
      app__app_root: 'assets-workcenters',
      is_active: true
    };

    this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
      if (response['results'] && response['results'].length == 1) {
        this.confService.set("SAP_USER", response['results'][0].user, true);
        this.confService.set("SAP_PASSWORD", response['results'][0].password, true);
      } else {
        this.router.navigate(["/index"]);
        this.snackBar.open("NO_SAP_LICENCE_FOR_APP_ASSETS", null, {duration: 5000, panelClass: ['error']});
      }
    });
  }

  tabChange() {
    this.selectedIndex = this.tab.selectedIndex;
  }
}
