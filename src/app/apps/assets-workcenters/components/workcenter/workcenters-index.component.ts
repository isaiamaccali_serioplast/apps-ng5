import {Component} from "@angular/core";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {buildErrorMessage} from "../../../../shared/utils";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {Workcenter, WorkcentersService} from "../../../_common/services/workcenters.service";
import {TechnologyLink} from "../../../_common/services/technology-link.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'workcenters-index',
  templateUrl: 'workcenters-index.component.html',
  styleUrls: ['workcenters-index.component.css']
})

export class WorkcentersIndexComponent extends TranslatableComponent{

  editMode = false;
  createMode = false;
  wcToEdit:Workcenter;
  loading = false;

  constructor(private messageService:MessageService, protected translationService:TranslationService,
              private workcentersService:WorkcentersService, private loadingService:LoadingService) {
    super(translationService);
  }

  createWorkcenter(wc:Workcenter){
    this.wcToEdit = wc;
    this.createMode = true;
    this.editMode = true;
  }

  editWorkcenter(wc:Workcenter){
    this.wcToEdit = wc;
    this.createMode = false;
    this.editMode = true;
  }

  deleteWorkcenter(wc:Workcenter){
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: ()=>{
        this.loadingService.show();
        this.workcentersService.deleteWorkcenter(wc).subscribe((resp)=>{
          this.loadingService.hide();
          if (resp.status == 204) {
            this.loading = true;
            setTimeout(()=>{
              this.loading = false;
            },500)
          }
          else {
            resp.context = "SERIONET";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(resp),
              autoClose: false
            });
          }
        })
      },
      deny: ()=>{}
    })
  }

  restoreWorkcenter(wc:Workcenter){
    if(!wc.technology)
      wc.technology = new TechnologyLink();
    wc.is_active = true;
    this.editWorkcenter(wc);
  }

  back(){
    this.editMode = false;
  }
}
