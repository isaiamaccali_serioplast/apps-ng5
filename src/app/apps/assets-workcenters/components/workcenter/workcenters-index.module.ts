import {NgModule} from "@angular/core";
import {WorkcentersIndexComponent} from "./workcenters-index.component";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {WorkcentersListModule} from "./workcenters-list/workcenters-list.module";
import {SharedModule} from "../../../../shared/shared.module";
import {WorkcenterEditModule} from "./workcenter-edit/workcenter-edit.module";

@NgModule({
  imports: [SharedModule, SerioplastCommonModule,WorkcentersListModule,WorkcenterEditModule],       // module dependencies
  declarations: [WorkcentersIndexComponent],   // components and directives
  exports:[WorkcentersIndexComponent],
  providers: []                    // services
})
export class WorkcentersIndexModule { }


