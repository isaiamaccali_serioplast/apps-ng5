import {NgModule} from "@angular/core";
import {AssetsWorkcenterIndexComponent} from "./assets-workcenters-index.component";
import {WorkcentersListComponent} from "./workcenters-list.component";
import {SharedModule} from "../../../../../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [WorkcentersListComponent],   // components and directives
  exports:[WorkcentersListComponent],
  providers: []                    // services
})
export class WorkcentersListModule { }


