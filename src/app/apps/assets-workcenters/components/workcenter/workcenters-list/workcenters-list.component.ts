import {Component, EventEmitter, OnInit, Output, ViewChild} from "@angular/core";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../../shared/material/table-with-pages/table-with-pages.component";
import {Workcenter, WorkcentersService} from "../../../../_common/services/workcenters.service";
import {TechnologyLink, TechnologyLinkService} from "../../../../_common/services/technology-link.service";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {cloneObject, parseSerionetRespToData} from "../../../../../shared/utils";
import {Observable} from "rxjs";
import {WorkcenterLocation, WorkcenterLocationsService} from "../../../../_common/services/workcenter-locations.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {TechnologyService} from "../../../../_common/services/technology.service";
import {StatusLink} from "../../../../_common/services/status-link.service";
import {TranslatableComponent} from "../../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'workcenters-list',
  templateUrl: 'workcenters-list.component.html',
  styleUrls: ['workcenters-list.component.css']
})
export class WorkcentersListComponent extends TranslatableComponent implements OnInit {

  @ViewChild('workcentersTable') workCentersTable: TableWithPagesComponent;
  @Output('createEvent') createEvent = new EventEmitter();
  @Output('editEvent') editEvent = new EventEmitter();
  @Output('deleteEvent') deleteEvent = new EventEmitter();
  @Output('restoreEvent') restoreEvent = new EventEmitter();
  availableActionsWorkcenters = [];
  noItemsMessageWorkcenters = this.tr("NO_WORKCENTERS_AVAILABLE");
  headerConfWorkcenters = [
    {
      columnId: 'sapcode',
      label: this.tr("SAP_CODE"),
      valueKey: 'sap_code'
    },
    {
      columnId: 'description',
      label: this.tr("DESCRIPTION"),
      valueKey: 'description'
    },
    {
      columnId: 'technology',
      label: this.tr("TECHNOLOGY"),
      valueKey: 'technology',
      useStr: true
    },
    {
      columnId: 'current_location',
      label: this.tr("CURRENT_LOCATION"),
      valueFunction: (item: Workcenter) => {
        if (item.locations) {
          let content = [];
          content.push("<ul>");
          content = content.concat(item.locations.map((l) => {
            return "<li>" + l.__str__ + "</li>"
          }));
          content.push("</ul>");
          return content.join("");
        }
      }
    }
  ];

  technologyLinks: TechnologyLink[] = [];
  sites: Site[] = [];
  siteWorkcenterFilter: number;
  workcentersActive = true;
  textFilterWC: string = "";
  technologyFilterWC: number;

  constructor(protected translationService: TranslationService, private workcentersService: WorkcentersService,
              private technologyLinkService: TechnologyLinkService,
              private sitesService: SitesService, private workcenterLocationsService: WorkcenterLocationsService,
              private technologyService: TechnologyService) {
    super(translationService);
  }

  ngOnInit() {
    this.technologyLinkService.getTechnologyLinks({is_active: true, app_name: 'workcenter'}).subscribe((data) => {
      this.technologyLinks = parseSerionetRespToData(data);
      this.technologyLinks.forEach((tl) => {
        this.technologyService.getTechnology(tl.technology_id.pk).subscribe((t_) => {
          tl.technology_id = t_;
        })
      })
    });
    this.sitesService.getSites({is_active: true}).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
    });

    this.initTableActions();
  }

  initTableActions() {
    let actionEditWorkcenter: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditWorkcenter.click = this.editWorkcenter.bind(this);
    let actionDeleteWorkcenter: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteWorkcenter.click = this.deleteWorkcenter.bind(this);
    actionDeleteWorkcenter.visibilityCondition = (item: Workcenter) => {
      return item.is_active;
    };
    let actionRestoreWorkcenter: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreWorkcenter.click = this.restoreWorkcenter.bind(this);
    actionRestoreWorkcenter.visibilityCondition = (item: Workcenter) => {
      return !item.is_active;
    };
    this.availableActionsWorkcenters = [actionEditWorkcenter, actionDeleteWorkcenter, actionRestoreWorkcenter];
  }

  editWorkcenter(w: Workcenter) {
    this.editEvent.emit(w);
  }

  deleteWorkcenter(w: Workcenter) {
    this.deleteEvent.emit(w);
  }

  restoreWorkcenter(w: Workcenter) {
    this.restoreEvent.emit(w);
  }

  loadWorkcenters(page, nResults) {
    let query = {};
    query['results'] = nResults;
    if (!page) page = 1;
    query['page'] = page;
    query['is_active'] = this.workcentersActive;
    if (this.textFilterWC)
      query['search'] = this.textFilterWC;
    if (this.technologyFilterWC)
      query['technology__id'] = this.technologyFilterWC;
    if (this.siteWorkcenterFilter)
      query['get_location'] = this.siteWorkcenterFilter;

    query['ordering'] = 'description';

    return this.workcentersService.getWorkcenters(query);
  }

  createWorkcenter() {
    let w = new Workcenter();
    w.status = new StatusLink();
    w.technology = new TechnologyLink();
    this.createEvent.emit(w);
  }

  expandDataWorkcenters(workcenters: Workcenter[]) {
    return Observable.create((observer) => {
      let expandWC = (wcs, index, callback) => {
        if (wcs[index]) {
          let wc = wcs[index];
          let query = {};
          query['workcenter_id__id'] = wc.pk;
          query['is_active'] = true;

          this.workcenterLocationsService.getWorkcenterLocations(query).subscribe((data) => {
            if (data && data['count'] && data['count'] > 0) {
              if (data['results'][0]['location'])
                wc.locations = data['results'].map((wcl: WorkcenterLocation) => {
                  return wcl.location
                });
              else
                wc.locations = [];
              wcs[index] = wc;
              expandWC(wcs, index + 1, callback);
            }
            else {
              expandWC(wcs, index + 1, callback);
            }
          });
        }
        else {
          callback();
        }
      };
      expandWC(workcenters, 0, () => {
        observer.next(workcenters);
        observer.complete();
      })
    });
  }

  workcenterSearchTimeout = null;

  handleTextFilterWorkcenterChange() {
    if (this.workcenterSearchTimeout)
      clearTimeout(this.workcenterSearchTimeout);
    this.workcenterSearchTimeout = setTimeout(() => {
      this.workCentersTable.reload();
    }, 500)
  }

  handleTechnologyFilterChange() {
    this.workCentersTable.reload();
  }
}
