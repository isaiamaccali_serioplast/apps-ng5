import {NgModule} from "@angular/core";
import {AssetsWorkcenterIndexComponent} from "./assets-workcenters-index.component";
import {WorkcenterEditComponent} from "./workcenter-edit.component";
import {SharedModule} from "../../../../../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [WorkcenterEditComponent],   // components and directives
  exports:[WorkcenterEditComponent],
  providers: []                    // services
})
export class WorkcenterEditModule { }


