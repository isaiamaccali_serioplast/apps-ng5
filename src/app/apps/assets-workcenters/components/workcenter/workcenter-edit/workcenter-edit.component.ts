import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {Workcenter, WorkcentersService} from "../../../../_common/services/workcenters.service";
import {
  WorkcenterLocation,
  WorkcenterLocationsService
} from "../../../../_common/services/workcenter-locations.service";
import {TechnologyLink, TechnologyLinkService} from "../../../../_common/services/technology-link.service";
import {Site, SitesService} from "../../../../_common/services/sites.service";
import {Asset, AssetsService} from "../../../../_common/services/assets.service";
import {TranslationService} from "../../../../../shared/i18n/translation.service";
import {MessageService} from "../../../../../shared/material/message/message.service";
import {LoadingService} from "../../../../../shared/loading/loading.service";
import {TechnologyService} from "../../../../_common/services/technology.service";
import {buildErrorMessage, fixValue, parseSerionetRespToData} from "../../../../../shared/utils";
import {MatSnackBar} from "@angular/material";
import {DialogComponent} from "../../../../../shared/material/dialog/dialog.component";
import {BaseComponent} from "../../../../../shared/components/base-component/base.component";

@Component({
  selector: 'workcenter-edit',
  templateUrl: 'workcenter-edit.component.html',
  styleUrls: ['workcenter-edit.component.css']
})
export class WorkcenterEditComponent extends BaseComponent implements OnInit {

  @Output('backEvent') backEvent = new EventEmitter();
  @Input('workcenterToEdit') workcenter: Workcenter = new Workcenter();
  @Input('createMode') createMode = false;
  @ViewChild('placeWCL') placeWCL: DialogComponent;

  workcenterLocations: WorkcenterLocation[] = [];
  technologyLinks: TechnologyLink[] = [];
  sites: Site[] = [];

  siteToAdd: Site = new Site();

  workcenterAssets: Asset[] = [];
  workcentersAssetsToAdd: Asset[] = [];
  workcentersAssetsToRemove: Asset[] = [];

  assetToAdd: Asset = new Asset();

  wclToLocate: WorkcenterLocation;

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private workcentersService: WorkcentersService,
              private technologyLinkService: TechnologyLinkService, private sitesService: SitesService,
              private assetsService: AssetsService, private workcenterLocationsService: WorkcenterLocationsService,
              private technologyService: TechnologyService, private snackBar: MatSnackBar) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    this.technologyLinkService.getTechnologyLinks({is_active: true, app_name: 'workcenter'}).subscribe((data) => {
      this.technologyLinks = parseSerionetRespToData(data);
      this.technologyLinks.forEach((tl) => {
        this.technologyService.getTechnology(tl.technology_id.pk).subscribe((t_) => {
          tl.technology_id = t_;
        })
      })
    });
    this.sitesService.getSites({is_active: true}).subscribe((data) => {
      this.sites = parseSerionetRespToData(data);
    });

    if (this.workcenter.pk) {
      this.workcenterLocationsService.getWorkcenterLocations({
        workcenter_id__id: this.workcenter.pk
      }).subscribe((data) => {
        this.workcenterLocations = parseSerionetRespToData(data);
        this.workcenterLocations.forEach((wcl: WorkcenterLocation) => {
          if (wcl.is_active)
            wcl.status = "active";
          else
            wcl.status = "inactive";
        })
      });

      this.assetsService.getAssets({
        workcenter: this.workcenter.pk,
        is_active: true
      }).subscribe((data) => {
        this.workcenterAssets = parseSerionetRespToData(data);
        this.workcentersAssetsToAdd = this.workcenterAssets.slice(0);
      });
    }
  }

  saveWorkcenter() {
    if (this.getActiveWCL().length == 0) {
      this.messageService.open({
        title: this.tr("ATTENTION"),
        message: this.tr('SELECT_LOCATION')
      });
    } else {
      this.loadingService.show();

      let isCreate = (this.workcenter.pk == null);
      if (isCreate)
        this.workcenter.newly_created = true;

      if (this.workcenter.technology && this.workcenter.technology.pk) {
        fixValue(this.technologyLinks, this.workcenter, 'technology');
        this.technologyService.getTechnology(this.workcenter.technology.technology_id.pk).subscribe((data) => {
          this.workcenter.technology.technology_id = data;
          this.doSaveWorkcenter();
        })
      }
      else {
        this.doSaveWorkcenter();
      }
    }
  }

  doSaveWorkcenter() {
    this.workcentersService.saveWorkcenter(this.workcenter).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.workcenter.pk = resp.pk;
        this.workcenterLocations.forEach((wcl) => {
          wcl.workcenter_id = this.workcenter;
        });
        this.saveWorkcenterLocations((resp_) => {
          this.manageRespSave(resp_, () => {
            this.saveWorkcenterAssets((resp_) => {
              this.loadingService.hide();
              this.manageRespSave(resp_, () => {
                this.backEvent.emit();
              });
            })
          }, true);
        })
      }, true);
    })
  }

  handleTechnologyChange() {
    fixValue(this.technologyLinks, this.workcenter, 'technology');
  }

  undoEditWorkcenter() {
    this.backEvent.emit();
  }

  getSitesToAdd(keyword: string) {
    return this.sitesService.getSites({name_icontains: keyword, results: 5, is_active: true})
  }

  selectedLocation(s: Site) {
    if (s && s.pk) {
      this.siteToAdd = s;
    }
    else {
      this.siteToAdd = new Site();
    }
  }

  createWorkcenterLocation() {
    let wcl = new WorkcenterLocation();
    wcl.location = this.siteToAdd;
    wcl.workcenter_id = this.workcenter;
    wcl.status = "toAdd";
    this.workcenterLocations.push(wcl);
    this.siteToAdd = new Site();
  }

  removeWorkcenterLocation(wcl: WorkcenterLocation) {
    wcl.status = "toRemove";
  }

  restoreWorkcenterLocation(wcl: WorkcenterLocation) {
    wcl.status = "toRestore";
  }

  saveWorkcenterLocations(callback: any) {
    this.saveWorkcenterLocationsToDelete((response) => {
      if (response.status == 204) {
        this.saveWorkcenterLocationsToCreate((response_) => {
          if (response_.pk) {
            callback.call(null, response_);
          }
          else {
            callback.call(null, response_);
          }
        })
      }
      else {
        callback.call(null, response);
      }
    })
  }

  saveWorkcenterLocationsToDelete(callback: any) {
    let wcls = this.workcenterLocations.filter((wcl: WorkcenterLocation) => {
      return (wcl.status == "toRemove") && wcl.pk;
    });
    this.deleteWorkcenterLocations(wcls, 0, callback);
  }

  deleteWorkcenterLocations(wcls, index, callback, lastResponse?: any) {
    if (wcls[index] && wcls[index].pk) {
      this.workcenterLocationsService.deleteWorkcenterLocation(wcls[index]).subscribe((response) => {
        if (response.status == 204) {
          this.deleteWorkcenterLocations(wcls, index + 1, callback, response);
        }
        else {
          callback.call(null, response);
        }
      });
    }
    else {
      if (!lastResponse)
        lastResponse = {status: 204};
      callback(lastResponse);
    }
  }

  saveWorkcenterLocationsToCreate(callback: any) {
    let wcls = this.workcenterLocations.filter((wcl: WorkcenterLocation) => {
      return (wcl.status == "toAdd") || (wcl.status == "toRestore") || (wcl.status == "active");
    });
    this.addWorkcenterLocations(wcls, 0, callback);
  }

  addWorkcenterLocations(wcls, index, callback, lastResponse?: any) {
    if (wcls[index]) {
      this.workcenterLocationsService.saveWorkcenterLocation(wcls[index]).subscribe((response) => {
        if (response.pk) {
          wcls[index]['pk'] = response.pk;
          this.addWorkcenterLocations(wcls, index + 1, callback, response);
        }
        else {
          callback.call(null, response);
          if (this.workcenter.newly_created) {
            // è una create, faccio una rollback sulle wcl create in caso di errore
            this.loadingService.hide();
            if (!response.context)
              response.context = "SERIONET";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: buildErrorMessage(response),
              autoClose: false,
              onClose: () => {
                this.rollbackWorkcenterLocations(wcls);
              }
            });
          }
        }
      });
    }
    else {
      if (!lastResponse)
        lastResponse = {pk: 999};
      callback(lastResponse);
    }
  }

  rollbackWorkcenterLocations(wcls: WorkcenterLocation[]) {
    let addedWCL = wcls.filter((wcl) => {
      return wcl.pk != null;
    });
    this.deleteWorkcenterLocations(addedWCL, 0, (resp) => {
      this.manageRespDelete(resp, () => {
        addedWCL.forEach((wcl) => {
          if (wcl.pk)
            delete wcl['pk'];
        });
        this.workcentersService.deleteItemSerionet(this.workcenter, {delete_force: 1}).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            if (this.workcenter.pk)
              delete this.workcenter['pk'];
            this.snackBar.open(this.tr("SAVE_ROLLBACKED"), null, {duration: 5000});
          });
        })
      });
    })
  };

  getAssetsToAdd(keyword: string) {
    return this.assetsService.getAssets({search: keyword, results: 5, is_active: true});
  }

  selectedAsset(a: Asset) {
    if (a && a.pk) {
      this.assetToAdd = a;
    }
    else {
      this.assetToAdd = new Asset();
    }
  }

  addAssetToWorkcenter() {
    if (this.containsWithPk(this.workcenterAssets, this.assetToAdd) == -1)
      this.workcenterAssets.push(this.assetToAdd);
    if (this.containsWithPk(this.workcentersAssetsToRemove, this.assetToAdd) != -1)
      this.workcentersAssetsToRemove.splice(this.containsWithPk(this.workcentersAssetsToRemove, this.assetToAdd), 1);
    if (this.containsWithPk(this.workcentersAssetsToAdd, this.assetToAdd) == -1) {
      this.workcentersAssetsToAdd.push(this.assetToAdd);
    }
    else {
      this.snackBar.open(this.tr("ASSET_ALREADY_SET"), null, {duration: 5000});
    }
    this.assetToAdd = new Asset();
  }

  removeWorkcenterAsset(a: Asset) {
    this.workcenterAssets.splice(this.containsWithPk(this.workcenterAssets, a), 1);
    if (a.workcenter.pk == this.workcenter.pk) {
      if (this.containsWithPk(this.workcentersAssetsToRemove, a) == -1)
        this.workcentersAssetsToRemove.push(a);
    }
    if (this.containsWithPk(this.workcentersAssetsToAdd, a) != -1)
      this.workcentersAssetsToAdd.splice(this.containsWithPk(this.workcentersAssetsToAdd, a), 1);
  }

  containsWithPk(list, object) {
    return list.map((i) => {
      return i.pk;
    }).indexOf(object.pk);
  }

  saveWorkcenterAssets(callback: any) {
    this.saveWorkcenterAssetsToDelete((response) => {
      if (response.status == 204) {
        this.saveWorkcenterAssetsToAdd((response_) => {
          if (response_.pk) {
            callback.call(null, response_);
          }
          else {
            callback.call(null, response_);
          }
        })
      }
      else {
        callback.call(null, response);
      }
    })
  }

  saveWorkcenterAssetsToDelete(callback: any) {
    this.deleteWorkcenterAsset(0, callback);
  }

  deleteWorkcenterAsset(index, callback, lastResponse?: any) {
    if (this.workcentersAssetsToRemove[index]) {
      if (this.workcentersAssetsToRemove[index].workcenter.pk == this.workcenter.pk) {
        this.workcentersAssetsToRemove[index].workcenter = new Workcenter();
        this.workcentersAssetsToRemove[index].workcenter.pk = null;
        this.assetsService.saveAsset(this.workcentersAssetsToRemove[index], true).subscribe((response) => {
          if (response.status == 204) {
            this.deleteWorkcenterAsset(index + 1, callback, response);
          }
          else {
            callback.call(null, response);
          }
        });
      }
      else {
        this.deleteWorkcenterAsset(index + 1, callback, lastResponse);
      }
    }
    else {
      if (!lastResponse)
        lastResponse = {status: 204};
      this.workcentersAssetsToRemove = [];
      callback(lastResponse);
    }
  }

  saveWorkcenterAssetsToAdd(callback: any) {
    this.addWorkcenterAsset(0, callback);
  }

  addWorkcenterAsset(index, callback, lastResponse?: any) {
    if (this.workcentersAssetsToAdd[index]) {
      if (!this.workcentersAssetsToAdd[index].workcenter || (this.workcentersAssetsToAdd[index].workcenter.pk != this.workcenter.pk)) {
        this.workcentersAssetsToAdd[index].workcenter = this.workcenter;
        this.assetsService.saveAsset(this.workcentersAssetsToAdd[index], true).subscribe((response) => {
          if (response.pk) {
            this.addWorkcenterAsset(index + 1, callback, response);
          }
          else {
            callback.call(null, response);
          }
        });
      }
      else {
        this.addWorkcenterAsset(index + 1, callback, lastResponse);
      }
    }
    else {
      if (!lastResponse)
        lastResponse = {pk: 999};
      this.workcentersAssetsToAdd = [];
      callback(lastResponse);
    }
  }

  enableWorkcenterLocationRestore() {

    return this.getActiveWorkcenterLocations().length < 2;
  }

  getActiveWorkcenterLocations() {
    return this.workcenterLocations.filter((wcl) => {
      return wcl.status == 'active' || wcl.status == 'toAdd' || wcl.status == 'toRestore';
    });
  }

  checkSapCode() {
    if (this.workcenter.sap_code) {
      this.workcentersService.getWorkcenters({sap_code: this.workcenter.sap_code}).subscribe((data) => {
        if (data && data['count'] && data['count'] > 0) {
          this.workcenter.sap_code = "";
          this.snackBar.open(this.tr("SAP_CODE_ALREADY_IN_USE"), null, {duration: 5000});
        }
        else {
          this.workcenter.sap_code = this.workcenter.sap_code.toUpperCase();
        }
      })
    }
  }

  getActiveWCL() {
    return this.workcenterLocations.filter((s) => {
      return s.status == 'active' || s.status == 'toAdd' || s.status == 'toRestore'
    })
  }

  resetAssetToAdd() {
    this.assetToAdd = new Asset();
  }

  resetLocationToAdd() {
    this.siteToAdd = new Site();
  }

  placeWorkcenterLocation(wcl: WorkcenterLocation) {

    this.wclToLocate = wcl;
    this.placeWCL.open();
  }

  noSiteMap() {
    this.placeWCL.close();
    this.snackBar.open(this.tr("NO_SITE_MAP_FOR_THIS_PLANT"), null, {duration: 5000});
  }
}
