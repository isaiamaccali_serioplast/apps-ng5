import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AssetsWorkcenterRoutes} from "./assets-workcenters.routes";
import {AssetsWorkcenterIndexModule} from "./components/assets-workcenters-index/assets-workcenters-index.module";
import {AssetsCredentialsAuthorizationsService} from "./services/assets-credentials-authorizations.service";

@NgModule({
  imports: [SharedModule, RouterModule.forChild(AssetsWorkcenterRoutes), AssetsWorkcenterIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: [AssetsCredentialsAuthorizationsService]                    // services
})
export class AssetsWorkcentersModule { }

