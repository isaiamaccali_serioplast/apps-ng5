import {Routes} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {AssetsWorkcenterIndexComponent} from "./components/assets-workcenters-index/assets-workcenters-index.component";

export const AssetsWorkcenterRoutesRoot: Routes = [
  {
    path: 'assets-workcenters',
    loadChildren: './assets-workcenters.module.ts#AssetsWorkcentersModule',
    canActivate:[AuthGuard]
  }
];

// Route Configuration
export const AssetsWorkcenterRoutes: Routes = [
  {
    path: '',
    component: AssetsWorkcenterIndexComponent,
    canActivate:[AuthGuard]
  }
];


