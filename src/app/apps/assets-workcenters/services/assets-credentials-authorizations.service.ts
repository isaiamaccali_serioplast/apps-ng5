import {Injectable} from "@angular/core";
import {HasCompanyPosition} from "../../_common/services/has-company-position.service";

export const DEVELOPERS = [
  1, //Serioplast Software Developer
  10, //Software Development Manager
];

export const IT_JOB_TITLES = [
  3, //IT Manager
  9, //IT Engineer
];

@Injectable()
export class AssetsCredentialsAuthorizationsService {

  constructor() {
  }

  isAuthorized(hasCompanyPositions: HasCompanyPosition[], allowedCompanyPositionNames: number[]) {

    let userCompanyPositionNames = hasCompanyPositions.map((item: HasCompanyPosition)=> {
      return item.get_company_position_obj.name.pk;
    });

    return DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 ||
      userCompanyPositionNames.filter((item) => allowedCompanyPositionNames.indexOf(item) !== -1).length > 0;
  }
}
