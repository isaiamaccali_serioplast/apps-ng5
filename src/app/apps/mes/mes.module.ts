import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {MesRoutingModule} from "./mes.routes";
import {MesIndexModule} from "./components/mes-index/mes-index.module";
import {MesWOViewModule} from "./components/mes-wo-view/mes-wo-view.module";

@NgModule({
  imports: [SharedModule,MesRoutingModule,MesIndexModule,MesWOViewModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class MesModule{}

