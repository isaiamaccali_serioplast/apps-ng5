import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {MesIndexComponent} from "./components/mes-index/mes-index.component";
import {MesWOViewComponent} from "./components/mes-wo-view/mes-wo-view.component";
import {AuthGuard} from "../../auth-guard";

// Route Configuration
export const MesRoutes: Routes = [
  {
    path: '',
    component: MesIndexComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'wowiev/:id',
    component: MesWOViewComponent,
    canActivate: [AuthGuard],
  }
];

/**
 * Root route, uses lazy loading to improve performance
 */
export const MesRoutesRoot: Routes = [
  {
    path: 'mes',
    loadChildren: './mes.module.ts#MesModule'
    //children: packagesRoutes,
    //canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(MesRoutes)],
  exports: [RouterModule],
})
export class MesRoutingModule {
}
