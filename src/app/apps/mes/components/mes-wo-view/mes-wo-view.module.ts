import {NgModule} from "@angular/core";
import {MesIndexComponent} from "./mes-index.component";
import {SharedModule} from "../../../../shared/shared.module";
import {MesWOViewComponent} from "./mes-wo-view.component";
import {WorkOrdersService} from "../../../_common/services/workorders.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [MesWOViewComponent],   // components and directives
  exports:[MesWOViewComponent],
  providers: [
    WorkOrdersService]                    // services
})
export class MesWOViewModule { }


