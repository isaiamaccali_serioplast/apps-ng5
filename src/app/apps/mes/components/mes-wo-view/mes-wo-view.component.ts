import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {ComponentSAP, OperationSAP, WorkOrderSAP, WorkOrdersService} from "../../../_common/services/workorders.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {ConfirmationSAP, ConfirmationSapService} from "../../../_common/services/confirmations-sap.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {PrinterSAP, PrinterSapService} from "../../../_common/services/printers-sap.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {HandlingUnitSap, HandlingUnitSapService} from "../../../_common/services/handling-units-sap.service";
import {Title} from "@angular/platform-browser";
import {DialogConfirmComponent} from "../../../../shared/components/dialog-confirm/dialog-confirm.component";
import {buildErrorMessage, cloneObject} from "../../../../shared/utils";
import {HandlingUnitsQualitySapService, HUQuality} from "../../../_common/services/handling-units-quality-sap.service";
import {HULabel, LabelHandlingUnitsQualitySapService} from "../../../_common/services/label-hu-quality-sap.service";
import {WorkorderBatch, WorkorderBatchSapService} from "../../../_common/services/workorder-batches-sap.service";
import {PackagingSap, PackagingSapService} from "../../../_common/services/packaging-sap.service";
import {Site} from "../../../_common/services/sites.service";
import {CookieService} from "ngx-cookie";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'mes-wo-view',
  templateUrl: 'mes-wo-view.component.html',
  styleUrls: ['mes-wo-view.component.css']
})

export class MesWOViewComponent extends TranslatableComponent implements OnInit {
  operations: OperationSAP[];
  printers: PrinterSAP[] = [];
  @Input() workorder: WorkOrderSAP = new WorkOrderSAP();
  @Input() currentSite: number;
  @Input() siteList: Site[] = [];
  currentPlant: string;
  selected_ops: OperationSAP;
  @ViewChild('confirmationModalConfirm') confirmationModalConfirm: DialogConfirmComponent;
  @ViewChild('batchModalConfirm') batchModalConfirm: DialogConfirmComponent;
  @ViewChild('rePrintModalConfirm') rePrintModalConfirm: DialogConfirmComponent;
  @ViewChild('packModalConfirm') packModalConfirm: DialogConfirmComponent;
  @ViewChild('unpackModalConfirm') unpackModalConfirm: DialogConfirmComponent;
  @ViewChild('popupMessage') popupMessage: DialogConfirmComponent;
  @ViewChild('printDialog') printDialog: DialogConfirmComponent;
  popupMessageResponse: string = "";
  popupHeaderTitle: string = "";
  popupMessageHeaderColor: string = "#4CAF50";
  ERROR_HEADER_COLOR: string = "#af504c";
  SUCCESS_HEADER_COLOR: string = "#4CAF50";
  confirmation: ConfirmationSAP = new ConfirmationSAP();
  confirmations: ConfirmationSAP[] = [];
  rePrintConfirmation: ConfirmationSAP = new ConfirmationSAP();
  batch: WorkorderBatch = new WorkorderBatch();
  hu: HandlingUnitSap = new HandlingUnitSap();
  component: ComponentSAP = new ComponentSAP();
  packagings: PackagingSap[] = [];
  selectedPackaging: PackagingSap = new PackagingSap();
  print: boolean = false;
  printerName: string;
  quantityConfirmed: number;
  DEFAULT_CID = "99999";
  huLabels: HULabel[] = [];
  huToPrint: HUQuality = new HUQuality();

  constructor(private loginservice: LoginService,
              private workordersservice: WorkOrdersService,
              private confirmationSapService: ConfirmationSapService,
              private batchSapService: WorkorderBatchSapService,
              private printerSapService: PrinterSapService,
              private handlingUnitSapService: HandlingUnitSapService,
              private packagingSapService: PackagingSapService,
              private messageService: MessageService,
              private loadingservice: LoadingService,
              private translationservice: TranslationService,
              private huQualityService: HandlingUnitsQualitySapService,
              private labelService: LabelHandlingUnitsQualitySapService,
              private sapPrintersService: PrinterSapService,
              private titleService: Title,
              private cookieService: CookieService) {
    super(translationservice);
    this.titleService.setTitle("Serioplast - MES");
    this.DEFAULT_CID = (this.loginservice.user.badge_code) ? this.loginservice.user.badge_code : this.DEFAULT_CID;
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.loadWorkOrder();
    this.loadConfirmations();
    this.loadPrinters();
    this.setCurrentPlant();
  }

  loadPrinters() {
    let query = {};
    query['Plant'] = this.workorder.Plant;
    query['Thermal'] = true;
    query['RFID'] = false;
    this.printerSapService.getPrinters(query).subscribe((response) => {
      if (response.status == 200) {
        this.printers = response['response']['results'];
        this.printerName = this.getDefaultPrinter(this.printers);
      }
      else {
        response.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.tr('NO_PRINTERS_AVAILABLE'),
          autoClose: false
        });
      }
    });
  }

  getDefaultPrinter(printers: PrinterSAP[]) {
    return (printers.length > 0 && printers.length == 1) ? printers[0].Name : this.getDefaultPrinterFromCookie();
  }

  getDefaultPrinterFromCookie() {

    let preferredPrinter = this.cookieService.get('preferredPrinter');
    return (preferredPrinter != null) ? JSON.parse(preferredPrinter).Name : null;

  }

  setLocalStoragePreferredPrinter(preferedPrinter: PrinterSAP) {
    this.cookieService.put('preferredPrinter', JSON.stringify(preferedPrinter));
  }

  loadWorkOrder() {
    this.loadingservice.show();
    if (this.workorder) {
      this.workordersservice.getWorkOrder(this.workorder.WorkOrderNumber).subscribe((response) => {
        this.loadingservice.hide();
        if (response.status == 200) {
          this.workorder = response['response'];
          if (this.workorder.DefaultQuantity != null && this.workorder.DefaultQuantity > 0) {
            this.confirmation.QtyConfirmed = this.workorder.DefaultQuantity.toString();
            this.quantityConfirmed = Number(this.confirmation.QtyConfirmed);
          } else {
            this.confirmation.QtyConfirmed = Number(0).toString();
            this.quantityConfirmed = Number(0);
          }
        }
        else {
          response.context = "SAP";
          this.messageService.open({
            title: 'Error',
            error: true,
            message: buildErrorMessage(response),
            autoClose: false
          });
        }
      });

    }
  }

  openConfirmationModalDialog(ops, print: boolean) {
    this.print = print;
    this.confirmationModalConfirm.open();
    this.selected_ops = ops;
    this.confirmation.WorkOrderNumber = this.selected_ops.WorkOrderNumber;
    this.confirmation.Operation = this.selected_ops.OpNumber;
    this.confirmation.QtyScrapped = Number(0).toExponential();
    this.confirmation.PrinterName = this.printerName;
    this.confirmation.CID = this.DEFAULT_CID;
  }

  saveConfirmation() {
    this.confirmation.QtyConfirmed = Number(this.quantityConfirmed).toExponential();
    this.confirmation.PrinterName = this.printerName;
    this.loadingservice.show();

    this.confirmationSapService.createConfirmation(this.confirmation).subscribe((response) => {
      if (response["status"] == 201) {
        this.successResponsePopup(this.tr("CONFIRMATION_SAVED"));

      } else {
        this.errorResponsePopup(response["message"]);
      }
      this.loadingservice.hide();
      this.init();
    });
  }

  openBatchModalDialog(component: ComponentSAP) {

    this.batchModalConfirm.open();
    this.batch.ComponentCode = component.ComponentCode;
    this.batch.WorkOrderNumber = component.WorkOrderNumber;
    this.batch.Plant = component.Plant;
    this.batch.StorLoc = component.StorLoc;
  }

  saveBatch() {
    this.loadingservice.show();

    this.batchSapService.createBatch(this.batch).subscribe((response) => {
      if (response["status"] == 201) {
        this.successResponsePopup(this.tr("BATCH_SAVED"));
      } else {
        this.errorResponsePopup(response["message"]);
      }

      this.loadingservice.hide();
      this.ngOnInit();
    });
  }

  openReprintModal(confirmation: ConfirmationSAP, print: boolean) {
    this.print = print;
    this.rePrintModalConfirm.open();
    this.rePrintConfirmation = confirmation;
    this.rePrintConfirmation.CID = this.DEFAULT_CID;
    this.rePrintConfirmation.PrinterName = this.printerName;
    this.rePrintConfirmation.Operation = (!this.rePrintConfirmation.Operation) ? "0000" : this.rePrintConfirmation.Operation;
  }

  rePrint() {
    this.rePrintConfirmation.PrinterName = this.printerName;
    let creationDate = this.rePrintConfirmation.CreationDate.replace(/-/g, "");
    let creationTime = this.rePrintConfirmation.CreationTime.replace(/:/g, "");
    this.rePrintConfirmation.CreationDate = creationDate;
    this.rePrintConfirmation.CreationTime = creationTime;

    this.loadingservice.show();
    this.confirmationSapService.updateConfirmation(this.rePrintConfirmation).subscribe((response) => {
      if (response["status"] == 204) {
        this.successResponsePopup(this.tr("REPRINT_SUCCESS"));
      } else {
        this.errorResponsePopup(response["message"]);
      }
      this.loadingservice.hide();
      this.init();
    });

  }

  openPackModalDialog(component: ComponentSAP) {
    this.component = component;
    this.getPackagings();
  }

  getPackagings() {

    this.selectedPackaging = new PackagingSap();

    this.packagingSapService.getPackagings({
      Material: this.component.ComponentCode,
      Plant: this.currentPlant
    }).subscribe((response) => {

      if (response["status"] == 200 && response["response"]["results"]) {
        this.packagings = response["response"]["results"];
        this.selectedPackaging = (this.packagings.length == 1) ? this.packagings[0] : new PackagingSap();

        // Open Modal only if at least one palletization
        if (this.selectedPackaging.Palletization || this.packagings.length > 1) {
          this.packModalConfirm.open();
        }

      } else {
        this.errorResponsePopup(response["message"]);
      }

    });

  }

  loadConfirmations() {

    this.confirmationSapService.getConfirmations({
      WorkOrderNumber: this.workorder.WorkOrderNumber,
    }).subscribe((response) => {

      if (response["status"] == 200 && response["response"]["results"]) {
        this.confirmations = response["response"]["results"];

      } else {
        this.errorResponsePopup(response["message"]);
      }

    });

  }

  openUnpackModalDialog(component: ComponentSAP) {
    this.component = component;
    this.unpackModalConfirm.open();
  }

  setCurrentPlant() {

    if (this.siteList && this.currentSite) {
      let site = this.siteList.filter((x) => x.pk == this.currentSite);
      this.currentPlant = (site.length == 1) ? site[0].code : "";
    } else {
      this.errorResponsePopup(this.tr("CURRENT_PLANT_ERROR"));
      this.init();
    }
  }

  saveRepack() {
    this.packModalConfirm.close();
    this.hu.WorkOrder = this.workorder.WorkOrderNumber;
    this.hu.Material = this.component.ComponentCode;
    this.hu.Palletization = this.selectedPackaging.Palletization;
    this.hu.Plant = this.currentPlant;
    this.hu.CID = this.DEFAULT_CID;

    this.loadingservice.show();

    this.handlingUnitSapService.createHandlingUnit(this.hu).subscribe((response) => {

      if (response["status"] == 201) {

        let huReturned: HandlingUnitSap = response['response'];
        this.huQualityService.getHandlingUnitQuality("'" + huReturned.SSCC + "'").subscribe((response) => {
          this.huToPrint = response['response'];
          this.sapPrintersService.getPrinters({Plant: this.huToPrint.Plant, Thermal: true, RFID: false}).subscribe((response) => {
            this.printers = response['response']['results'];
            this.labelService.getLabelsHandlingUnitsQuality({
              Plant: this.hu.Plant,
              SSCC: this.huToPrint.SSCC
            }).subscribe((response) => {
              this.huLabels = response['response']['results'];
              this.huToPrint.HULabelLink = [new HULabel()];
              this.loadingservice.hide();
              this.openDialogPrint();
            })
          });
        });
      }
      else {
        this.errorResponsePopup(response["message"]);
        this.resetAll();
      }
    });
  }

  openDialogPrint() {
    this.printDialog.open();
  }

  doPrintHU() {
    this.loadingservice.show();
    this.huQualityService.createHandlingUnitsQuality(this.huToPrint).subscribe((resp) => {
      this.loadingservice.hide();
      this.printDialog.close();
      if (resp.status == 201) {
        this.successResponsePopup(this.tr("REQUESTED_COMPLETED"));
      }
      else {
        this.errorResponsePopup(resp["message"]);
      }
      this.resetAll();
    });
  };

  handleLabelChange() {
    for (let l of this.huLabels) {
      if (this.huToPrint.HULabelLink[0].LabelName == l.LabelName) {
        //lo clono in modo da poter modificare le copie senza problemi
        this.huToPrint.HULabelLink[0] = cloneObject(l);
        this.huToPrint.HULabelLink[0].SSCC = this.huToPrint.SSCC;
      }
    }
  }

  unPack() {
    this.loadingservice.show();

    this.handlingUnitSapService.getHandlingUnit(this.hu.SSCC).subscribe((response) => {
      if (response["status"] == 200) {

        // SET HU WITH RESPONSE
        this.hu = response["response"];

        // CHECK HU WAREHOUSE - WORKORDER WAREHOUSE
        if ((this.hu.Warehouse && this.workorder.Warehouse) && (this.hu.Warehouse.charAt(3) == this.workorder.Warehouse.charAt(3))) {

          // CHECK HU MATERIAL with COMPONENT CODE and HU PLANT WITH CURRENT PLANT
          if (this.hu.Material == this.component.ComponentCode && this.currentPlant && this.hu.Plant == this.currentPlant) {
            // PUT FOR UNPACK

            this.hu.WorkOrder = this.workorder.WorkOrderNumber;
            this.handlingUnitSapService.updateHandlingUnit(this.hu).subscribe((response) => {

              if (response["status"] == 204) {
                this.successResponsePopup(this.tr("UNPACK_SAVED"));
              } else {
                this.errorResponsePopup(response["message"]);
              }
              this.resetAll();
            });
          } else {
            this.errorResponsePopup(this.tr("HU_MATERIAL_AND_PLANT_NOT_MATCH"));
            this.resetAll();
          }

        } else {
          this.errorResponsePopup(this.tr("HU_NOT_EXIST_IN_WAREHOUSE"));
          this.resetAll();
        }

      } else {
        this.errorResponsePopup(response["message"]);
        this.resetAll();
      }
    });
  }

  resetAll() {
    this.hu = new HandlingUnitSap();
    this.huToPrint = new HUQuality();
    this.huLabels = [];
    this.component = new ComponentSAP();
    this.selectedPackaging = new PackagingSap();
    this.packagings = [];
    this.loadingservice.hide();
    this.ngOnInit();
  }

  emptyFunc() {
  }

  successResponsePopup(message: string) {
    this.popupMessageResponse = message;
    this.popupMessageHeaderColor = this.SUCCESS_HEADER_COLOR;
    this.popupHeaderTitle = this.tr("SUCCESS");
    this.popupMessage.open();
  }

  errorResponsePopup(message: string) {
    this.popupMessageResponse = message;
    this.popupMessageHeaderColor = this.ERROR_HEADER_COLOR;
    this.popupHeaderTitle = this.tr("ERROR");
    this.popupMessage.open();
  }
}
