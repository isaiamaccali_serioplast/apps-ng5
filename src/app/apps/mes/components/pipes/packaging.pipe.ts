import {PipeTransform, Pipe} from "@angular/core";
import {WorkOrderSAP} from "../../../_common/services/workorders.service";
import {PackagingSap, PackagingSapService} from "../../../_common/services/packaging-sap.service";

@Pipe({
  name: 'packagingDescription',
  pure:false
})


export class PackagingPipe implements PipeTransform{

  packaging = new PackagingSap();
  querying = false;

  constructor(private packingSapService:PackagingSapService){

  }

  transform(wo:WorkOrderSAP) {
    if(wo && wo.PackIns){
      this.packaging.Palletization = wo.PackIns;
      if(!this.packaging.Description && !this.querying) {
        this.querying = true;
        let id = "Palletization='"+wo.PackIns+"',Plant='"+wo.Plant+"'";
        this.packingSapService.getPackaging(id).subscribe((response)=>{
          if (response["status"] == 200 && response["response"]) {
            this.packaging = response["response"];
            this.querying = false
          }
        });
      }
      return (this.packaging.Description? this.packaging.Description: "");
    }
    else {
      return "-";
    }

  }
}
