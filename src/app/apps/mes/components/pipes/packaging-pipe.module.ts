import {NgModule} from "@angular/core";
import {PackagingPipe} from "./packaging.pipe";

@NgModule({
  declarations: [PackagingPipe],   // components and directives,
  exports:[PackagingPipe],
  providers: []                    // services
})
export class PackagingPipeModule { }
