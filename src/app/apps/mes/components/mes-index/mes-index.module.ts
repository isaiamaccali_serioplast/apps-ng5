import {NgModule} from "@angular/core";
import {MesIndexComponent} from "./mes-index.component";
import {SharedModule} from "../../../../shared/shared.module";
import {MesWOViewModule} from "../mes-wo-view/mes-wo-view.module";
import {PackagingPipeModule} from "../pipes/packaging-pipe.module";

@NgModule({
  imports: [SharedModule,MesWOViewModule,PackagingPipeModule],       // module dependencies
  declarations: [MesIndexComponent],   // components and directives
  exports:[MesIndexComponent],
  providers: []                    // services
})
export class MesIndexModule { }


