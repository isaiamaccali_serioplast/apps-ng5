import {Component, OnInit, ViewChild} from "@angular/core";
import {Workcenter, WorkcentersService} from "../../../_common/services/workcenters.service";
import {LoginService} from "../../../../shared/login/service/login.service";
import {ActivatedRoute, Router} from "@angular/router";
import {OperationSAP, WorkOrderSAP, WorkOrdersService} from "../../../_common/services/workorders.service";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {Observable} from "rxjs/src/core/linq";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {ConfirmationSAP, ConfirmationSapService} from "../../../_common/services/confirmations-sap.service";
import {MatSnackBar} from "@angular/material";
import {Title} from "@angular/platform-browser";
import {DialogConfirmComponent} from "../../../../shared/components/dialog-confirm/dialog-confirm.component";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {UserSitesComponent} from "../../../../shared/components/user-sites/user-sites.component";
import {parseSerionetRespToData} from "../../../../shared/utils";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";

@Component({
  selector: 'mes-index',
  templateUrl: 'mes-index.component.html',
  styleUrls: ['mes-index.component.css']
})

export class MesIndexComponent extends TranslatableComponent implements OnInit {
  workcenters: Workcenter[] = [];
  operations: OperationSAP[] = [];
  workorders: WorkOrderSAP[] = [];
  selected_site: Site;
  selected_wo: WorkOrderSAP = new WorkOrderSAP();
  selected_wo_id: string;
  selected_wc: string;
  bool: boolean;
  released: boolean;
  released_status: string;
  ready = true;
  DEFAULT_CID = "99999";
  @ViewChild('popupMessageIndex') popupMessageIndex: DialogConfirmComponent;
  @ViewChild('siteSelector') siteSelector: UserSitesComponent;
  popupMessageResponse: string = "";
  popupHeaderTitle: string = "";
  popupMessageHeaderColor: string = "#4CAF50";
  ERROR_HEADER_COLOR: string = "#af504c";
  SUCCESS_HEADER_COLOR: string = "#4CAF50";
  activateUserSiteComponent: boolean = false;

  constructor(private sideNavService: SideNavService, private workcentersService: WorkcentersService,
              private confirmationSapService: ConfirmationSapService, private loginService: LoginService,
              private router: Router, private workOrdersService: WorkOrdersService,
              private activatedRoute: ActivatedRoute, private messageService: MessageService,
              private loadingservice: LoadingService, protected translationService: TranslationService,
              private sitesService: SitesService, private titleService: Title,
              private snackBar: MatSnackBar) {
    super(translationService);
    this.ready = true;
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.titleService.setTitle("Serioplast - MES");
    this.DEFAULT_CID = (this.loginService.user.badge_code) ? this.loginService.user.badge_code : this.DEFAULT_CID;
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.bool = false;
    this.setReleaseView(false);

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['site']) {
        this.selected_site = new Site();
        this.selected_site.pk = parseInt(params['site'], 10);
        this.activateUserSiteComponent = true;
        this.selected_wc = params['wc'];
        this.selected_wo_id = params['wo'];
        this.initList();
      } else {
        this.activateUserSiteComponent = true;
      }
    });
  }

  initList() {

    this.loadWorkCenters();
    //this.loadPackagings();

    if (this.selected_wo_id) {
      this.selected_wo = null;
      this.bool = false;
      let query = {WorkOrderNumber: this.selected_wo_id, Status: this.released_status};

      this.workOrdersService.getWorkOrders(query).subscribe((response) => {

        if (response['status'] == 200) {
          if (response['response']['results'].length == 1) {
            this.workorders = response['response']['results'];
            this.selected_wo = this.workorders[0];
            this.bool = true;
          } else {
            this.errorResponsePopup(this.tr("WORK_ORDER_NOT_FOUND"));
          }
        } else {
          this.workorders = [];
          this.errorResponsePopup(response["message"]);
        }
      });
    }
    else {
      this.loadWorkOrders();
    }
  }

  changeReleased() {
    this.setReleaseView(!this.released);
    this.loadWorkOrders();
  }

  openWo(wo: WorkOrderSAP) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('OPEN_WO_WARNING'),
      autoClose: false,
      confirm: () => {
        this.saveConfirmation(wo, true);
      },
      deny: () => {
      }
    });
  }

  setReleaseView(bol: boolean) {
    if (bol) {
      this.released = true;
      this.released_status = "RELEASED";
    }
    else {
      this.released = false;
      this.released_status = "WORKING";
    }
  }

  closeWo() {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CLOSE_WO_WARNING'),
      autoClose: false,
      confirm: () => {
        this.saveConfirmation(this.selected_wo, false);
        this.selected_wo = null;
        this.selected_wc = "";
        this.selected_wo_id = "";
        this.changeRoute()
      },
      deny: () => {
      }
    });
  }

  saveConfirmation(worder: WorkOrderSAP, open: boolean) {

    let confirmation = new ConfirmationSAP();
    confirmation.WorkOrderNumber = worder.WorkOrderNumber;
    confirmation.QtyConfirmed = Number(0).toExponential();
    confirmation.QtyScrapped = Number(0).toExponential();
    confirmation.CID = this.DEFAULT_CID;
    let message = "CLOSE_WORK_ORDER";

    if (worder && worder.WOperationLink && worder.WOperationLink['results'].length > 0) {
      if (!open) {
        // On close workorder (take last operation)
        let index = worder.WOperationLink['results'].length - 1;
        let operation = worder.WOperationLink['results'][index];
        confirmation.Operation = operation.OpNumber;
      } else {
        // On Open workorder (take first operation)
        confirmation.Operation = worder.WOperationLink['results'][0].OpNumber;
        message = "OPEN_WORK_ORDER"
      }

      this.loadingservice.show();

      this.confirmationSapService.createConfirmation(confirmation).subscribe((response) => {
        if (response["status"] == 201) {
          this.successResponsePopup(this.tr(message));
        } else {
          this.errorResponsePopup(response["message"]);
        }

        this.loadingservice.hide();
        this.init();
      });

    } else {
      this.errorResponsePopup(this.tr("CANNOT_OPEN_OR_CLOSE_WO_NO_OPS"));
    }
  }

  loadWorkCenters() {
    let query = {is_active: true};
    query['get_location'] = this.selected_site.pk;
    this.workcentersService.getWorkcenters(query).subscribe((response) => {
      this.workcenters = parseSerionetRespToData(response);
    });
  }

  changeWo(wo: WorkOrderSAP) {
    this.selected_wo_id = wo.WorkOrderNumber;
    this.bool = true;
    this.setReleaseView(false);
    this.changeRoute();
  }

  loadAll() {
    this.selected_wc = "";
    this.selected_wo_id = "";
    this.setReleaseView(false);
    this.changeRoute();
  }

  changeSite(s: Site) {
    if (s && s.pk) {
      this.selected_site = s;
      //this.initList();
    }
    else {
      this.selected_site = new Site();
    }
    this.selected_wc = "";
    this.selected_wo_id = "";
    this.workorders = [];
    this.changeRoute();
  }

  changeWc(wc) {
    this.selected_wc = wc.sap_code;
    this.selected_wo_id = "";
    this.setReleaseView(false);
    this.changeRoute();
  }

  loadWorkOrders() {
    let doload = (query) => {
      this.loadingservice.show();
      this.workOrdersService.getWorkOrders(query).subscribe((response) => {
        if (response['status'] == 200) {
          if (response['response']['results'].length == 0) {

            let message = "";

            if (query['Status'] == 'WORKING') {
              message = this.tr("NO_WORKING_WORKORDERS");
            } else {
              message = this.tr("NO_RELEASED_WORKORDERS");
            }

            this.openSnackBar(message, ['warning']);
          }
          this.workorders = response['response']['results'];
          this.bool = false;
        } else {
          this.workorders = [];
          this.errorResponsePopup(response["message"]);
        }
        this.loadingservice.hide();
      });
    };

    let query = {};
    query['Status'] = this.released_status;
    if (this.selected_wc) {
      query['WorkCenter'] = this.selected_wc;
    }
    else {
      this.bool = false;
    }

    if (this.selected_site.code) {
      query['Plant'] = this.selected_site.code;
      doload(query);
    } else {
      this.sitesService.getSite(this.selected_site.pk).subscribe((response) => {
        this.selected_site = response;
        query['Plant'] = this.selected_site.code;
        doload(query);
      });
    }

  }

  logout() {
    this.loginService.doLogout().first().subscribe(() => {
      this.router.navigate(["/login"]);
    });
  }

  changeRoute() {
    let urlParams = {};
    if (this.selected_site) {
      urlParams["site"] = this.selected_site.pk;
    }
    if (this.selected_wo_id) {
      urlParams["wo"] = this.selected_wo_id;
    }
    if (this.selected_wc) {
      urlParams["wc"] = this.selected_wc;
    }
    let extras = {
      queryParams: urlParams
    };
    this.router.navigate(["mes"], extras);
  }

  openSnackBar(message, classes) {
    this.snackBar.open(message, null, {duration: 5000, panelClass: classes});
  }

  getWorkCenters(wo: WorkOrderSAP) {
    if (wo.WOperationLink && wo.WOperationLink.results) {
      return wo.WOperationLink.results.map((op: OperationSAP) => {
        return op.WorkCenter
      }).join("; ")
    } else {
      return '';
    }
  }

  emptyFunc() {
  }

  successResponsePopup(message: string) {
    this.popupMessageResponse = message;
    this.popupMessageHeaderColor = this.SUCCESS_HEADER_COLOR;
    this.popupHeaderTitle = this.tr("SUCCESS");
    this.popupMessageIndex.open();
  }

  errorResponsePopup(message: string) {
    this.popupMessageResponse = message;
    this.popupMessageHeaderColor = this.ERROR_HEADER_COLOR;
    this.popupHeaderTitle = this.tr("ERROR");
    this.popupMessageIndex.open();
  }
}
