import {Routes, RouterModule} from "@angular/router";
import {AuthGuard} from "../../auth-guard";
import {NgModule} from "@angular/core";
import {SitesIndexComponent} from "./components/sites-index/sites-index.component";


// Route Configuration
export const sitesRoutes: Routes = [
  {
    path: '',
    component: SitesIndexComponent,
    canActivate: [AuthGuard],

    /*children: [
      {
        path: '',
        pathMatch:'full',
        component: WorkToolTypeComponent,
        //canActivate: [AuthGuard],
      },
    ]*/

  }
];

/**
 * Root route, uses lazy loading to improve performance
 * @type {{path: string; loadChildren: string; canActivate: AuthGuard[]}[]}
 */
export const SitesRoutesRoot: Routes = [
  {
    path: 'sites',
    loadChildren: './sites.module.ts#SitesModule',
    //children: packagesRoutes,
    //canActivate: [AuthGuard]
  }
]

@NgModule({
  imports: [RouterModule.forChild(sitesRoutes)],
  exports: [RouterModule],
})
export class SitesRoutingModule { }
