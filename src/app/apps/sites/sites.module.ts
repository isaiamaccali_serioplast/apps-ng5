import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {SitesRoutingModule} from "./sites.routes";
import {SitesIndexModule} from "./components/sites-index/sites-index.module";

@NgModule({
  imports: [SharedModule,SitesRoutingModule,SitesIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class SitesModule {}

