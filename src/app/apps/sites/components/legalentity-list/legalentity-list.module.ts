import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {LegalEntityService} from "../../../_common/services/legal-entity.service";
import {LegalEntityListComponent} from "./legalentity-list.component";
import {CountriesService} from "../../../_common/services/countries.service";
import {OrganizationService} from "../../../_common/services/organization.service";


@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [LegalEntityListComponent],   // components and directives
  exports:[LegalEntityListComponent],
  providers: [LegalEntityService,CountriesService,OrganizationService]                    // services
})
export class LegalEntityListModule { }


