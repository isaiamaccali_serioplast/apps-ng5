import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {LegalEntity, LegalEntityService} from "../../../_common/services/legal-entity.service";
import {CountriesService, Country} from "../../../_common/services/countries.service";
import {MatSnackBar} from "@angular/material";
import {Organization, OrganizationService} from "../../../_common/services/organization.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'legalentity-list',
  templateUrl: 'legalentity-list.component.html',
  styleUrls: ['legalentity-list.component.css']
})

export class LegalEntityListComponent extends BaseComponent implements OnInit {
  legalEntityCreateOpen: boolean;
  onCreate: boolean;
  legalEntity: LegalEntity = new LegalEntity();
  countries: Country[] = [];
  organizations: Organization[] = [];
  legalEntityFilter: LegalEntity;
  nameTimeout: any;
  active: boolean = true;
  availableActions = [];
  @ViewChild('legalEntitiesTable') legalEntitiesTable: TableWithPagesComponent;
  noItemsMessageLegalEntities = this.tr("No legal entities.");
  headerConfLegalEntities = [
    {
      label: this.tr("Name"),
      valueKey: 'name'
    },
    {
      label: this.tr("Code"),
      valueKey: 'code'
    },
    {
      label: this.tr("Country"),
      valueKey: 'country'
    },
    {
      label: this.tr("Organization"),
      valueKey: 'organization_ref',
      useStr: true,
    },
  ];

  constructor(private countryservice: CountriesService,
              private organizationservice: OrganizationService,
              private legalentityservice: LegalEntityService,
              protected translationService: TranslationService,
              protected messageService: MessageService,
              private snackBar: MatSnackBar,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.legalEntityFilter = new LegalEntity();
    this.legalEntityFilter.organization_ref = new Organization();
    this.legalEntityCreateOpen = false;
    this.onCreate = false;
  }

  ngOnInit() {
    this.loadCountries();
    this.loadOrganizations();
    this.initTableActions();
  }

  loadCountries() {
    this.countryservice.getCountries({}).subscribe((data) => {

        for (let i = 0; i < data.length; i++) {
          let country = new Country();
          country.value = data[i][0];
          country.label = data[i][1];
          this.countries[i] = country;
        }
        // Remove empty label
        this.countries.shift();
      }
    )
  }

  loadOrganizations() {
    let query = {};
    query['is_active'] = true;
    query['is_serioplast_group'] = true;

    this.organizationservice.getOrganizations(query).subscribe((data) => {
      this.organizations = parseSerionetRespToData(data);
    });
  }

  initTableActions() {
    let actionEditLegalEntity: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditLegalEntity.click = this.editLegalEntity.bind(this);
    let actionDeleteLegalEntity: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteLegalEntity.click = this.deleteLegalEntity.bind(this);
    actionDeleteLegalEntity.visibilityCondition = (item: LegalEntity) => {
      return item.is_active;
    };
    let actionRestoreLegalEntity: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreLegalEntity.click = this.restoreLegalEntity.bind(this);
    actionRestoreLegalEntity.visibilityCondition = (item: LegalEntity) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditLegalEntity, actionRestoreLegalEntity, actionDeleteLegalEntity];
  }

  loadLegalEntities(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.legalEntityFilter.name)
      query['name'] = this.legalEntityFilter.name;

    if (this.legalEntityFilter.code)
      query['code'] = this.legalEntityFilter.code;

    if (this.legalEntityFilter.country)
      query['country'] = this.legalEntityFilter.country;

    if (this.legalEntityFilter.organization_ref && this.legalEntityFilter.organization_ref.pk)
      query['organization_ref'] = this.legalEntityFilter.organization_ref.pk;

    return this.legalentityservice.getLegalEntities(query);
  }

  reload(){
    this.legalEntitiesTable.reload();
  }

  reset(){
    setTimeout(()=>{
      this.reload();
    },500)
  }

  deleteLegalEntity(le: LegalEntity) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.legalentityservice.deleteLegalEntiy(le).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.legalEntitiesTable.reload();
          });
        })
      },
      deny: () => {

      }
    });
  }

  restoreLegalEntity(le: LegalEntity) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.legalentityservice.restoreLegalEntity(le).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.legalEntitiesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveLegalEntity() {
    let displayMsg = false;
    if (this.legalEntity.is_active == false) {
      displayMsg = true;
    }
    this.loadingService.show();
    this.legalentityservice.saveLegalEntity(this.legalEntity).subscribe((resp) => {
      this.loadingService.hide();
      this.manageRespSave(resp, () => {
        this.createLegalEntity();
        this.legalEntityCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  createLegalEntity() {
    this.legalEntity = new LegalEntity();
    this.legalEntity.organization_ref = new Organization();
    this.legalEntityCreateOpen = true;
    this.onCreate = true;
  }

  editLegalEntity(le: LegalEntity) {
    this.legalEntity = cloneObject(le);
    this.legalEntityCreateOpen = true;
    this.onCreate = false;
  }

  undoEditLegalEntity() {
    this.createLegalEntity();
    this.legalEntityCreateOpen = false;
  }
}
