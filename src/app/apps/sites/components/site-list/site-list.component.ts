import {Component, OnInit, ViewChild} from "@angular/core";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {HeaderConf, TableRowAction, TableRowActions, TableWithPagesComponent} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {cloneObject, parseSerionetRespToData} from "../../../../shared/utils";
import {MessageService} from "../../../../shared/material/message/message.service";
import {Site, SitesService} from "../../../_common/services/sites.service";
import {LegalEntity, LegalEntityService} from "../../../_common/services/legal-entity.service";
import {MatSnackBar} from "@angular/material";
import {OrganizationLevel, OrganizationLevelService} from "../../../_common/services/organization-level.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {BaseComponent} from "../../../../shared/components/base-component/base.component";

@Component({
  selector: 'site-list',
  templateUrl: 'site-list.component.html',
  styleUrls: ['site-list.component.css']
})

export class SiteListComponent extends BaseComponent implements OnInit {
  sites: Site[];
  site: Site = new Site();
  legalEntities: LegalEntity[] = [];
  orgLevels: OrganizationLevel[] = [];
  siteFilter: Site;
  nameTimeout: any;
  siteCreateOpen: boolean;
  active: boolean = true;
  onCreate: boolean;
  availableActions = [];
  @ViewChild('sitesTable') sitesTable: TableWithPagesComponent;
  noItemsMessageSites = this.tr("No sites.");
  headerConfSites: HeaderConf[] = [
    {
      label: this.tr("Name"),
      valueKey: 'name'
    },
    {
      label: this.tr("Code"),
      valueKey: 'code',
      columnId: 'code'
    },
    {
      label: this.tr("Address"),
      valueKey: 'address'
    },
    {
      label: this.tr("Legal Entity"),
      valueKey: 'legalentity_ref',
      useStr: true,
    },
    {
      label: this.tr("Organization Level"),
      valueKey: 'org_level',
      useStr: true,
    },
    {
      label: this.tr("Active on SAP"),
      valueKey: 'active_on_sap',
      columnId:'activeSAP',
      isBoolean: true
    }
  ];

  constructor(private sitesservice: SitesService,
              private orglevelservice: OrganizationLevelService,
              private legalentityservice: LegalEntityService,
              protected translationService: TranslationService,
              private  snackBar: MatSnackBar,
              protected messageService: MessageService,
              protected loadingService: LoadingService) {
    super(translationService, messageService, loadingService);
    this.siteFilter = new Site();
    this.siteCreateOpen = false;
    this.onCreate = false;
  }

  ngOnInit() {
    this.initTableActions();
    this.loadLegalEntities();
    this.loadOrganizationLevels();
  }

  getAddress(place: any) {
    console.log(place);
    this.site.address = place['formatted_address'];
    let location = place['geometry']['location'];
    this.site.lat = location.lat();
    this.site.lng = location.lng();
  }

  loadLegalEntities() {
    let query = {};
    query['is_active'] = true;

    this.legalentityservice.getLegalEntities(query).subscribe((data) => {
      this.legalEntities = parseSerionetRespToData(data);
    })
  }

  loadOrganizationLevels() {
    let query = {};
    query['is_active'] = true;

    this.orglevelservice.getOrganizationLevels(query).subscribe((data) => {
      this.orgLevels = parseSerionetRespToData(data);
    })
  }

  initTableActions() {
    let actionEditSite: TableRowAction = cloneObject(TableRowActions.EDIT);
    actionEditSite.click = this.editSite.bind(this);
    let actionDeleteSite: TableRowAction = cloneObject(TableRowActions.DELETE);
    actionDeleteSite.click = this.deleteSite.bind(this);
    actionDeleteSite.visibilityCondition = (item: Site) => {
      return item.is_active;
    };
    let actionRestoreSite: TableRowAction = cloneObject(TableRowActions.RESTORE);
    actionRestoreSite.click = this.restoreSite.bind(this);
    actionRestoreSite.visibilityCondition = (item: Site) => {
      return !item.is_active;
    };
    this.availableActions = [actionEditSite, actionRestoreSite, actionDeleteSite];
  }

  loadSites(page: number, results: number) {
    let query = {};
    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    query['is_active'] = this.active;

    if (this.siteFilter.name)
      query['name_icontains'] = this.siteFilter.name;

    if (this.siteFilter.code)
      query['code'] = this.siteFilter.code;

    if (this.siteFilter.legalentity_ref)
      query['legalentity_ref'] = this.siteFilter.legalentity_ref;

    if (this.siteFilter.org_level)
      query['org_level'] = this.siteFilter.org_level;

    if (this.siteFilter.active_on_sap)
      query['active_on_sap'] = this.siteFilter.active_on_sap;

    return this.sitesservice.getSites(query);
  }

  deleteSite(s: Site) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.sitesservice.deleteSite(s).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.sitesTable.reload();
          });
        })
      },
      deny: () => {
      }
    });
  }

  restoreSite(s: Site) {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM'),
      confirm: () => {
        this.sitesservice.restoreSite(s).subscribe((resp) => {
          this.manageRespSave(resp, () => {
            this.sitesTable.reload();
          })
        })
      },
      deny: () => {

      }
    });
  }

  saveSite() {
    let displayMsg = false;
    if (this.site.is_active == false) {
      displayMsg = true;
    }
    this.sitesservice.saveSite(this.site).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.createSite();
        this.siteCreateOpen = false;
        if (displayMsg)
          this.snackBar.open(this.tr("ITEM_RESTORED"), null, {duration: 5000});
      });
    })
  }

  createSite() {
    this.site = new Site();
    this.siteCreateOpen = true;
    this.onCreate = true;
  }

  editSite(s: Site) {
    this.site = cloneObject(s);
    this.siteCreateOpen = true;
    this.onCreate = false;
  }

  undoEditSite() {
    this.createSite();
    this.siteCreateOpen = false;
  }

  reload(){
    this.sitesTable.reload()
  }

  reset(){
    setTimeout(()=>{
      this.reload();
    },500)
  }

}
