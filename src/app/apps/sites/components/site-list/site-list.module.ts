import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {LegalEntityService} from "../../../_common/services/legal-entity.service";
import {LegalEntityListComponent} from "./legalentity-list.component";
import {SiteListComponent} from "./site-list.component";
import {SitesService} from "../../../_common/services/sites.service";
import {OrganizationLevelService} from "../../../_common/services/organization-level.service";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [SiteListComponent],   // components and directives
  exports:[SiteListComponent],
  providers: [SitesService,LegalEntityService,OrganizationLevelService]                    // services
})
export class SiteListModule { }


