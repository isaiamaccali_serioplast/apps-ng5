import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SitesIndexComponent} from "./sites-index.component";
import {LegalEntityListModule} from "../legalentity-list/legalentity-list.module";
import {SiteListModule} from "../site-list/site-list.module";

@NgModule({
  imports: [SharedModule,LegalEntityListModule,SiteListModule],       // module dependencies
  declarations: [SitesIndexComponent],   // components and directives
  exports:[SitesIndexComponent],
  providers: []                    // services
})
export class SitesIndexModule { }


