import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'sites-index',
  templateUrl: 'sites-index.component.html',
  styleUrls: ['sites-index.component.css']
})

export class SitesIndexComponent implements OnInit {
  selectedIndex = 0;

  constructor(private titleService: Title) {
    this.titleService.setTitle("Serioplast - Sites");
  }


  ngOnInit() {
  }

  handleTabChange(event){
    this.selectedIndex = event.index;
  }


}
