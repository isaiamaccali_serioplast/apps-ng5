import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {SerioplastCommonModule} from "../../../_common/common.module";
import {RfidTraysMaterialsListComponent} from "./materials-list.component";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [RfidTraysMaterialsListComponent],   // components and directives
  exports:[RfidTraysMaterialsListComponent],
  providers: []                    // services
})
export class RfidMaterialsListModule { }


