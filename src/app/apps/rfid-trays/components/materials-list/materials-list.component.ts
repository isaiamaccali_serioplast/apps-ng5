import {Component, EventEmitter, OnInit, Output, ViewChild, Input} from "@angular/core";
import {Material, MaterialsService} from "../../../_common/services/materials.service";
import {Site} from "../../../_common/services/sites.service";
import {MaterialType, MaterialTypesService} from "../../../_common/services/material-type.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {
  HeaderConf, TableRowAction,
  TableWithPagesComponent
} from "../../../../shared/material/table-with-pages/table-with-pages.component";
import {UserSitesComponent} from "../../../../shared/components/user-sites/user-sites.component";
import {DialogConfirmComponent} from "../../../../shared/components/dialog-confirm/dialog-confirm.component";
import {PrinterSapService, PrinterSAP} from "../../../_common/services/printers-sap.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {RFIDTagService, RFIDTag} from "../../../_common/services/rfid-tag.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {TranslatableComponent} from "../../../../shared/components/base-component/translatable.component";


@Component({
  selector: 'app-materials-list-rfid-trays',
  templateUrl: 'materials-list.component.html',
  styleUrls: ['materials-list.component.css']
})
export class RfidTraysMaterialsListComponent extends TranslatableComponent implements OnInit {

  @Input('')
  @ViewChild('materialsTable') materialsTable: TableWithPagesComponent;
  materials: Material[];
  currentPage: number;
  totalPages: number;
  material: Material;
  site: Site;
  materialType: MaterialType;
  materialTypes: MaterialType[];
  sites: Site[];
  code_or_description: string;
  valuated = true;
  isActive = true;
  loading = true;
  resultsPerPage = 10;
  interval: string;
  total: number;
  @Output('materialSelected') materialSelected = new EventEmitter();
  @ViewChild('siteSelector') siteSelector: UserSitesComponent;
  @ViewChild('printDialog') printDialog: DialogConfirmComponent;
  printer: string;
  printers: PrinterSAP[] = [];
  copies: number;

  noItemsMessage = this.tr("NO_MATERIALS_FOUND");
  headerConf: HeaderConf[] = [
    {
      label: this.tr("CODE"),
      valueKey: 'code'
    },
    {
      label: this.tr("DESCRIPTION"),
      valueKey: 'description'
    },
    {
      label: this.tr("TYPE"),
      valueKey: 'material_type',
      useStr: true
    }
  ];
  availableActions = [];


  constructor(private materialsService: MaterialsService,
              private loadingService: LoadingService,
              private materialTypesService: MaterialTypesService,
              private messageService: MessageService,
              private printerSapService: PrinterSapService,
              private rfidTagService: RFIDTagService,
              protected translationService: TranslationService) {
    super(translationService);
    this.materialTypes = [];
    this.materialType = null;
    this.site = new Site();
    this.sites = [];
  }

  ngOnInit() {
    this.materials = [];
    this.materialTypesService.getMaterialTypes({is_active: true, ordering: 'description'}).subscribe((data) => {
      if (data['count'] && data['count'] > 0) {
        this.materialTypes = data['results'].sort((a, b) => {
          return a.pk - b.pk;
        });
      }
    });

    this.availableActions = [];
  }

  loadMaterials(page?: number, results?: number) {

    let query = {};
    if (this.site.pk)
      query['site_set'] = this.site.pk;
    if (this.materialType)
      query['material_type'] = this.materialType;
    if (this.code_or_description)
      query['search'] = this.code_or_description;
    if (this.valuated != null)
      query['valuated'] = this.valuated;
    if (this.isActive != null)
      query['is_active'] = this.isActive;

    query['ordering'] = 'code';

    query['results'] = results;
    if (!page) page = 1;
    query['page'] = page;

    return this.materialsService.getMaterials(query);
  }

  changeSite(s: Site) {
    if (s && s.pk) {
      this.site = s;
      this.loadPrinters();
    }
    else {
      this.site = new Site();
    }
  }

  openDialogPrint() {
    this.printDialog.open();
  }

  loadPrinters() {
    let query = {};
    query['Plant'] = this.site.code;
    query['RFID'] = true;
    this.printerSapService.getPrinters(query).subscribe((response) => {
      if (response.status == 200) {
        this.printers = response['response']['results'];
        //this.printerName = this.getDefaultPrinter(this.printers);
      }
      else {
        response.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.tr('NO_PRINTERS_AVAILABLE'),
          autoClose: false
        });
      }
    });
  }

  printRFIDTags() {

    this.loadingService.show();
    let materialsToRfidTag = <Material[]>this.materialsTable.getSelectedItems();
    let i = materialsToRfidTag.length - 1;

    let callback = () => {
      if (i < 0) {
        this.loadingService.hide();
        this.printDialog.close();
        this.messageService.open({
          title: 'SUCCESS',
          message:materialsToRfidTag.length + " RFID_TAGS_PRINT_SUCCESS",
          autoClose: false
        });
      } else {

        let rfidTag = new RFIDTag();
        rfidTag.Material = materialsToRfidTag[i].code;
        rfidTag.Plant = this.site.code;
        rfidTag.Copies = this.copies;
        rfidTag.Printer = this.printer;

        this.rfidTagService.saveRFIDTag(rfidTag).subscribe((response) => {
          if (response.status == 201) {
            i = i - 1;
            callback();
          }
          else {
            this.printDialog.close();
            response.context = "SAP";
            this.messageService.open({
              title: 'Error',
              error: true,
              message: response["message"],
              autoClose: false
            });
          }
        });
      }
    };

    callback();

  }

}
