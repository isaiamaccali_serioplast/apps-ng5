import {Component, OnInit} from "@angular/core";
import {SideNavService} from "../../../../shared/sidenav/service/sidenav.service";
import {LoadingService} from "../../../../shared/loading/loading.service";
import {MessageService} from "../../../../shared/material/message/message.service";
import {TranslationService} from "../../../../shared/i18n/translation.service";
import {MatSnackBar} from "@angular/material";
import {Title} from "@angular/platform-browser";
import {ConfigurationService} from "../../../../shared/serionet/service/configuration.service";
import {AppPlantUsersService} from "../../../_common/services/app-plant-users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'rfid-trays-index',
  templateUrl: 'rfid-trays-index.component.html',
  styleUrls: ['rfid-trays-index.component.css']
})

export class RfidTraysIndexComponent implements OnInit {
  ready = true;

  constructor(private sideNavService: SideNavService,
              private router: Router,
              private messageService: MessageService,
              protected translationService: TranslationService,
              private appPlantUsersService:AppPlantUsersService,
              private confService:ConfigurationService,
              private titleService: Title,
              private snackBar: MatSnackBar) {
    this.ready = true;
    this.sideNavService.leftMenuEnabled = false;
    this.sideNavService.leftMenu = false;
    this.titleService.setTitle("Serioplast - RFID Trays");
  }

  ngOnInit() {
    let query = {
      app__app_root: 'rfid-trays',
    };

    this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
      if (response['results'] && response['results'].length == 1) {
        this.confService.set("SAP_USER",response['results'][0].user, true);
        this.confService.set("SAP_PASSWORD",response['results'][0].password, true);
      }else{
        this.router.navigate(["/"]);
        this.snackBar.open("NO_SAP_LICENCE_FOR_APP_RFID_TRAYS", null, {duration: 5000, panelClass: ['error']});
      }
    });

  }


}
