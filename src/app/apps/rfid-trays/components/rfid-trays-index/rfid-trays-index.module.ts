import {NgModule} from "@angular/core";
import {SharedModule} from "../../../../shared/shared.module";
import {RfidTraysIndexComponent} from "./rfid-trays-index.component";
import {RfidMaterialsListModule} from "../materials-list/materials-list.module";

@NgModule({
  imports: [SharedModule,RfidMaterialsListModule],       // module dependencies
  declarations: [RfidTraysIndexComponent],   // components and directives
  exports:[RfidTraysIndexComponent],
  providers: []                    // services
})
export class RfidTraysIndexModule { }


