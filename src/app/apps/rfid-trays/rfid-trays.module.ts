import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {RfidTraysRoutingModule} from "./rfid-trays.routes";
import {RfidTraysIndexModule} from "./components/rfid-trays-index/rfid-trays-index.module";

@NgModule({
  imports: [SharedModule,RfidTraysRoutingModule,RfidTraysIndexModule],       // module dependencies
  declarations: [],   // components and directives
  providers: []                    // services
})
export class RfidTraysModule{}

