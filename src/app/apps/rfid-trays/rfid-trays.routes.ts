import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {AuthGuard} from "../../auth-guard";
import {RfidTraysIndexComponent} from "./components/rfid-trays-index/rfid-trays-index.component";

// Route Configuration
export const RfidTraysRoutes: Routes = [
  {
    path: '',
    component: RfidTraysIndexComponent,
    canActivate: [AuthGuard],
  }
];

/**
 * Root route, uses lazy loading to improve performance
 */
export const RfidTraysRoutesRoot: Routes = [
  {
    path: 'rfid-trays',
    loadChildren: './rfid-trays.module.ts#RfidTraysModule'
    //children: packagesRoutes,
    //canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(RfidTraysRoutes)],
  exports: [RouterModule],
})
export class RfidTraysRoutingModule {
}
