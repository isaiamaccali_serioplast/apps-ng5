"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* Imports and re-exports all common modules, to prevent pointless repetitions*/
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var angular2_material_module_1 = require("./material/angular2-material.module");
var router_1 = require("@angular/router");
var i18n_module_1 = require("./i18n/i18n.module");
var markdown_module_1 = require("./markdown/markdown.module");
var common_1 = require("@angular/common");
var settings_module_1 = require("./settings/component/settings.module");
var language_selector_module_1 = require("./i18n/components/language-selector/language-selector.module");
var user_module_1 = require("./user/component/user.module");
var dialog_module_1 = require("./material/dialog/dialog.module");
var frequency_module_1 = require("./pipes/frequency.module");
var message_module_1 = require("./material/message/message.module");
var chip_autocomplete_module_1 = require("./material/chip-autocomplete/chip-autocomplete.module");
var pk_transformer_module_1 = require("./pk-transformer/pk-transformer.module");
var idle_module_1 = require("./idle/idle.module");
var camelcase_module_1 = require("./pipes/camelcase.module");
var table_with_pages_module_1 = require("./material/table-with-pages/table-with-pages.module");
var autocomplete_module_1 = require("./material/autocomplete/autocomplete.module");
var property_module_1 = require("./property/property.module");
var sanitizehtml_module_1 = require("./pipes/sanitizehtml.module");
var accordion_module_1 = require("./material/accordion/accordion.module");
var googlemaps_geolocation_module_1 = require("./googlemaps/googlemaps-geolocation.module");
var end_cutter_module_1 = require("./pipes/end-cutter.module");
var thousands_separator_module_1 = require("./pipes/thousands-separator.module");
var save_undo_buttons_module_1 = require("./components/save-undo-buttons/save-undo-buttons.module");
var stars_module_1 = require("./pipes/stars.module");
var material_selector_module_1 = require("./material-selector/material-selector.module");
var dialog_confirm_module_1 = require("./components/dialog-confirm/dialog-confirm.module");
var user_sites_with_sap_module_1 = require("./components/user-sites-with-sap/user-sites-with-sap.module");
var stepper_module_1 = require("./material/stepper/stepper.module");
var modules = [
    accordion_module_1.AccordionModule,
    angular2_material_module_1.Angular2MaterialModule,
    camelcase_module_1.CamelCaseModule,
    chip_autocomplete_module_1.ChipAutocompleteModule,
    common_1.CommonModule,
    message_module_1.ConfirmModule,
    dialog_confirm_module_1.DialogConfirmModule,
    dialog_module_1.DialogModule,
    end_cutter_module_1.EndCutterModule,
    thousands_separator_module_1.ExpThousandsSeparatorModule,
    forms_1.FormsModule,
    frequency_module_1.FrequencyModule,
    googlemaps_geolocation_module_1.GoogleMapsGeolocationModule,
    i18n_module_1.I18NModule,
    idle_module_1.IdleModule,
    language_selector_module_1.LanguageSelectorModule,
    markdown_module_1.MarkdownModule,
    material_selector_module_1.MaterialSelectorModule,
    pk_transformer_module_1.PkTransformerModule,
    property_module_1.PropertyModule,
    router_1.RouterModule,
    sanitizehtml_module_1.SanitizeHTMLModule,
    save_undo_buttons_module_1.SaveUndoButtonsModule,
    settings_module_1.SettingsModule,
    autocomplete_module_1.SPAutocompleteModule,
    stars_module_1.StarsModule,
    stepper_module_1.StepperModule,
    table_with_pages_module_1.TableWithPagesModule,
    user_module_1.UsersModule,
    user_sites_with_sap_module_1.UserSitesWithSAPModule
];
var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    core_1.NgModule({
        imports: modules,
        exports: modules
    })
], SharedModule);
exports.SharedModule = SharedModule;
