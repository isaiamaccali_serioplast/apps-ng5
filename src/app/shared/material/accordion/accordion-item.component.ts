import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

@Component({
  selector: 'sp-accordion-item',
  templateUrl: 'accordion-item.component.html',
  styleUrls: ['accordion-item.component.css']
})
export class AccordionItemComponent implements OnInit {

  opened: boolean = false;
  index: number;
  value: string = "";
  @Output() onOpen = new EventEmitter();
  @Output() onClose = new EventEmitter();
  @Input('disabled') disabled = false;

  constructor() {
  }

  ngOnInit() {

  }

  open() {
    this.opened = true;
    this.onOpen.emit(this.index);
  }

  close() {
    this.opened = false;
    this.onClose.emit(this.index);
  }

  toggleAccordion() {
    if (!this.disabled) {
      if (this.opened)
        this.close();
      else
        this.open();
    }

  }
}
