import {NgModule} from "@angular/core";
import {
  AccordionComponent, AccordionItemComponent,
  AccordionItemTitleComponent
} from "./accordion.component";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MatExpansionModule} from "@angular/material";
import {AccordionItemContentComponent} from "./accordion.component";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule, MatExpansionModule],       // module dependencies
  declarations: [AccordionComponent,AccordionItemComponent, AccordionItemContentComponent, AccordionItemTitleComponent],   // components and directives
  exports:[AccordionComponent,AccordionItemComponent, AccordionItemContentComponent, AccordionItemTitleComponent],
  providers: []                    // services
})
export class AccordionModule { }

