"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var directives_1 = require("@angular/core/src/metadata/directives");
var AccordionItemTitleComponent = (function () {
    function AccordionItemTitleComponent() {
        this.open = false;
    }
    AccordionItemTitleComponent.prototype.ngOnInit = function () {
    };
    return AccordionItemTitleComponent;
}());
AccordionItemTitleComponent = __decorate([
    core_1.Component({
        selector: 'sp-accordion-item-title',
        template: '<ng-content select="*"></ng-content>'
    })
], AccordionItemTitleComponent);
exports.AccordionItemTitleComponent = AccordionItemTitleComponent;
var AccordionItemContentComponent = (function () {
    function AccordionItemContentComponent() {
        this.open = false;
    }
    AccordionItemContentComponent.prototype.ngOnInit = function () {
    };
    return AccordionItemContentComponent;
}());
AccordionItemContentComponent = __decorate([
    core_1.Component({
        selector: 'sp-accordion-item-content',
        template: '<ng-content select="*"></ng-content>'
    })
], AccordionItemContentComponent);
exports.AccordionItemContentComponent = AccordionItemContentComponent;
var AccordionItemComponent = (function () {
    function AccordionItemComponent() {
        this.opened = false;
        this.value = "";
        this.onOpen = new core_1.EventEmitter();
        this.onClose = new core_1.EventEmitter();
        this.disabled = false;
    }
    AccordionItemComponent.prototype.ngOnInit = function () {
    };
    AccordionItemComponent.prototype.open = function () {
        this.opened = true;
        this.onOpen.emit(this.index);
    };
    AccordionItemComponent.prototype.close = function () {
        this.opened = false;
        this.onClose.emit(this.index);
    };
    AccordionItemComponent.prototype.toggleAccordion = function () {
        if (!this.disabled) {
            if (this.opened)
                this.close();
            else
                this.open();
        }
    };
    return AccordionItemComponent;
}());
__decorate([
    core_1.Output()
], AccordionItemComponent.prototype, "onOpen", void 0);
__decorate([
    core_1.Output()
], AccordionItemComponent.prototype, "onClose", void 0);
__decorate([
    directives_1.Input('disabled')
], AccordionItemComponent.prototype, "disabled", void 0);
AccordionItemComponent = __decorate([
    core_1.Component({
        selector: 'sp-accordion-item',
        templateUrl: 'accordion-item.component.html',
        styleUrls: ['accordion-item.component.css']
    })
], AccordionItemComponent);
exports.AccordionItemComponent = AccordionItemComponent;
var AccordionComponent = (function () {
    function AccordionComponent() {
        this.multiple = false;
    }
    AccordionComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var self = this;
        this.items.forEach(function (i, i_) {
            i.onOpen.subscribe(function (index) {
                self.handleOpen(index);
            });
            i.index = i_;
        });
        this.items.changes.subscribe(function (data) {
            _this.items.forEach(function (i, i_) {
                i.onOpen.subscribe(function (index) {
                    self.handleOpen(index);
                });
                i.index = i_;
            });
        });
    };
    AccordionComponent.prototype.handleOpen = function (itemIndex) {
        if (!this.multiple) {
            this.items.forEach(function (i, index) {
                if (index != itemIndex) {
                    i.close();
                }
            });
        }
    };
    return AccordionComponent;
}());
__decorate([
    core_1.ContentChildren(AccordionItemComponent)
], AccordionComponent.prototype, "items", void 0);
__decorate([
    directives_1.Input('multiple')
], AccordionComponent.prototype, "multiple", void 0);
AccordionComponent = __decorate([
    core_1.Component({
        selector: 'sp-accordion',
        templateUrl: 'accordion.component.html',
        styleUrls: ['accordion.component.css']
    })
], AccordionComponent);
exports.AccordionComponent = AccordionComponent;
