import {Component, OnInit, QueryList, ContentChildren, EventEmitter, Output, Input} from "@angular/core";

@Component({
  selector: 'sp-accordion-item-title',
  template: '<ng-content select="*"></ng-content>'
})
export class AccordionItemTitleComponent implements OnInit {

  open:boolean = false;

  constructor() {}

  ngOnInit() {

  }

}

@Component({
  selector: 'sp-accordion-item-content',
  template: '<ng-content select="*"></ng-content>'
})
export class AccordionItemContentComponent implements OnInit {

  open:boolean = false;


  constructor() {}

  ngOnInit() {

  }

}

@Component({
  selector: 'sp-accordion-item',
  templateUrl: 'accordion-item.component.html',
  styleUrls: ['accordion-item.component.css']
})
export class AccordionItemComponent implements OnInit {

  opened:boolean = false;
  index:number;
  value:string = "";
  @Output() onOpen = new EventEmitter();
  @Output() onClose = new EventEmitter();
  @Input('disabled') disabled = false;

  constructor() {}

  ngOnInit() {

  }

  open(){
    this.opened = true;
    this.onOpen.emit(this.index);
  }

  close(){
    this.opened = false;
    this.onClose.emit(this.index);
  }

  toggleAccordion(){
    if(!this.disabled){
      if(this.opened)
        this.close();
      else
        this.open();
    }

  }

}

@Component({
  selector: 'sp-accordion',
  templateUrl: 'accordion.component.html',
  styleUrls: ['accordion.component.css']
})
export class AccordionComponent{

  step = 0;

  @ContentChildren(AccordionItemComponent) items:QueryList<AccordionItemComponent>;
  @Input('multiple') multiple:boolean = false;
  // @Input('selectedItem') selectedItem:number ;
  // @Input('disabled') disabled = false;

  constructor() {}

  setStep(index: number) {
    this.step = index;
  }
}


