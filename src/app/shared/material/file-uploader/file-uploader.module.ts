import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {I18NModule} from "../../i18n/i18n.module";
import {MatButtonModule, MatIconModule, MatInputModule} from "@angular/material";
import {FileUploaderComponent} from "./file-uploader.component";

@NgModule({
  imports: [MatInputModule, FormsModule, CommonModule, I18NModule, MatIconModule, MatButtonModule],       // module dependencies
  declarations: [FileUploaderComponent],   // components and directives
  exports:[FileUploaderComponent],
  providers: []                    // services
})
export class FileUploaderModule { }

