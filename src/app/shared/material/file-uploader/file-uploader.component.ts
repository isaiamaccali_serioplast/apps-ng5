import {Component, forwardRef, Input, ViewChild} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import {FileService, SPFile} from "../../../apps/_common/services/file.service";
import {MessageService} from "../message/message.service";
import {buildErrorMessage} from "../../utils";

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FileUploaderComponent),
  multi: true
};

@Component({
  selector: 'file-uploader',
  templateUrl: 'file-uploader.component.html',
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class FileUploaderComponent implements ControlValueAccessor {

  @Input('label') label:string;
  @Input('contentType') contentType:number;
  @Input('fieldName') fieldName:string = "";
  @Input('mode') mode:string = 'image';
  @Input('hidePreview') hidePreview = false;

  @ViewChild('input') input: HTMLInputElement;

  //The internal data model
  private innerValue: any = '';

  //Placeholders for the callbacks which are later providesd
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  constructor(private fileService:FileService, private messageService:MessageService){}

  //get accessor
  get value(): any {
    return this.innerValue;
  };

  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  deleteImage(){
    this.value = null;
  }

  uploadImage() {
    if (!this.value) {
      this.value = new SPFile();
      this.value.content_type = this.contentType;
      this.value.field_name = "photo";
    }
    this.fileService.saveFile(this.value, this.input['nativeElement']).subscribe((data) => {
      if (data.pk) {
        this.value = data;
      }
      else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(data),
          autoClose: false
        });
      }
    })
  }

  extractFileName(filePath) {
    let pieces = filePath.split("/");
    return pieces[pieces.length - 1];
  }

}
