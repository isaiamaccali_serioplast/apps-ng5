"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var AutocompleteComponent = (function () {
    function AutocompleteComponent(myElement) {
        this.query = '';
        this.resetOnSelection = false;
        this.valueChanged = new core_1.EventEmitter();
        //The internal data model
        this.innerValue = '';
        this.valueSelected = false;
        this.elementRef = myElement;
    }
    AutocompleteComponent.prototype.ngOnInit = function () {
        if (this.initialValue) {
            this.innerValue = this.initialValue;
            this.query = this.getDisplay(this.innerValue);
            this.valueChanged.emit(this.innerValue);
            this.valueSelected = true;
        }
        if (!this.minChars)
            this.minChars = 3;
    };
    AutocompleteComponent.prototype.updateSource = function () {
        this.valueSelected = false;
        if (this.updateTimeout)
            clearTimeout(this.updateTimeout);
        var self = this;
        this.updateTimeout = setTimeout(function () {
            if (self.query !== "") {
                if (self.query.length >= self.minChars)
                    self.sourceFunction(self.query);
                else {
                    self.source = [];
                }
            }
            else {
                self.valueChanged.emit(null);
                self.source = [];
            }
        }, 200);
    };
    AutocompleteComponent.prototype.select = function (item) {
        if (this.resetOnSelection)
            this.query = "";
        else
            this.query = this.getDisplay(item);
        this.setModel(item);
        this.source = [];
        this.valueSelected = true;
    };
    AutocompleteComponent.prototype.setModel = function (item) {
        if (this.valueKey) {
            this.innerValue = item[this.valueKey];
        }
        else {
            this.innerValue = item;
        }
        this.valueChanged.emit(this.innerValue);
    };
    AutocompleteComponent.prototype.getDisplay = function (item) {
        if (this.displayKey) {
            return item[this.displayKey];
        }
        else {
            if (this.displayFunc) {
                return this.displayFunc(item);
            }
            else {
                return item;
            }
        }
    };
    AutocompleteComponent.prototype.handleClick = function (event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside) {
            this.source = [];
            if (!this.valueSelected)
                this.query = "";
        }
    };
    AutocompleteComponent.prototype.ngOnChanges = function (changes) {
        if (changes['source'])
            this.source = changes['source'].currentValue;
        if (changes['displayKey'])
            this.displayKey = changes['displayKey'].currentValue;
        if (changes['initialValue']) {
            this.initialValue = changes['initialValue'].currentValue;
            if (this.initialValue) {
                this.innerValue = this.initialValue;
                this.query = this.getDisplay(this.innerValue);
                this.valueChanged.emit(this.innerValue);
                this.valueSelected = true;
            }
            else {
                if (changes['initialValue'].previousValue && !changes['initialValue'].currentValue) {
                    this.innerValue = null;
                    this.query = "";
                    this.valueChanged.emit(null);
                    this.valueSelected = false;
                }
            }
        }
    };
    AutocompleteComponent.prototype.reset = function () {
        this.query = "";
    };
    return AutocompleteComponent;
}());
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "source", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "minChars", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "sourceFunction", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "initialValue", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "valueKey", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "displayKey", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "displayFunc", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "label", void 0);
__decorate([
    core_1.Input()
], AutocompleteComponent.prototype, "resetOnSelection", void 0);
__decorate([
    core_1.Output()
], AutocompleteComponent.prototype, "valueChanged", void 0);
__decorate([
    core_1.HostListener('document:click', ['$event'])
], AutocompleteComponent.prototype, "handleClick", null);
AutocompleteComponent = __decorate([
    core_1.Component({
        selector: 'sp-autocomplete',
        templateUrl: 'autocomplete.component.html',
        styleUrls: ['autocomplete.component.css']
    })
], AutocompleteComponent);
exports.AutocompleteComponent = AutocompleteComponent;
