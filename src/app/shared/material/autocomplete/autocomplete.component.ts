import {Component, Input, Output, EventEmitter, SimpleChange} from "@angular/core";
import {FormControl} from "@angular/forms";
import {TranslationService} from "../../i18n/translation.service";
import {startWith, switchMap} from "rxjs/operators";
import {Observable} from "rxjs";
import {parseSerionetRespToData} from "../../utils";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import {TranslatableComponent} from "../../components/base-component/translatable.component";

@Component({
  selector: 'sp-autocomplete',
  templateUrl: 'autocomplete.component.html',
  styleUrls: ['autocomplete.component.css']
})
export class AutocompleteComponent extends TranslatableComponent {

  @Input() source;
  @Input() minChars = 3;
  @Input() sourceFunction;
  @Input() initialValue;
  @Input() valueKey;
  @Input() displayKey = '__str__';
  @Input() displayFunc;
  @Input() label;
  @Input() resetOnSelection = false;
  @Input() noJSON = false;

  @Output() valueChanged = new EventEmitter();

  innerValue: any = '';

  stateCtrl: FormControl;
  data: Observable<any[]>;

  constructor(protected translationService: TranslationService) {
    super(translationService);
    this.stateCtrl = new FormControl();

  }

  ngOnInit(){
    this.data = this.stateCtrl.valueChanges
      .pipe(
        startWith(''),
        switchMap((keyword) => {
            return Observable.create((observer) => {
              if (keyword && (keyword.length >= this.minChars)) {
                this.sourceFunction(keyword).subscribe((resp) => {
                  if(this.noJSON){
                    observer.next(resp);
                  }
                  else {
                    observer.next(parseSerionetRespToData(resp));
                  }

                  observer.complete();
                });
              }
              else {
                if(this.initialValue){
                  this.stateCtrl.setValue(this.initialValue);
                  this.valueChanged.emit(this.initialValue);
                  observer.next([this.initialValue]);
                }
                else {
                  this.valueChanged.emit(null);
                  observer.next([]);
                }
                observer.complete();
              }
            })
          }
        )
      );
  }

  getDisplay(item: any) {
    if (this.displayKey) {
      return item[this.displayKey];
    }
    else {
      if (this.displayFunc) {
        return this.displayFunc(item);
      }
      else {
        return item
      }
    }
  }


  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['displayKey'])
      this.displayKey = changes['displayKey'].currentValue;
    if (changes['minChars'])
      this.minChars = changes['minChars'].currentValue;
    if (changes['initialValue']) {
      this.initialValue = changes['initialValue'].currentValue;
      if (this.initialValue) {
        this.innerValue = this.initialValue;
        this.stateCtrl.setValue(this.getDisplay(this.initialValue));
        this.valueChanged.emit(this.innerValue);
      }
      else {
        if (changes['initialValue'].previousValue && !changes['initialValue'].currentValue) {
          this.innerValue = null;
          this.stateCtrl.setValue("");
          this.valueChanged.emit(null);
        }
      }
    }
  }

  optionSelected(event:MatAutocompleteSelectedEvent){
    let item = event.option.value;
    this.stateCtrl.setValue(this.getDisplay(item));
    this.setModel(item);
  }

  setModel(item: any) {
    if (this.valueKey) {
      this.innerValue = item[this.valueKey];
    }
    else {
      this.innerValue = item;
    }
    this.valueChanged.emit(this.innerValue)
  }

  reset() {
    this.stateCtrl.setValue("");
  }

}
