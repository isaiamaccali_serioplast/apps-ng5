"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ng_module_1 = require("@angular/core/src/metadata/ng_module");
var angular2_material_module_1 = require("../angular2-material.module");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var autocomplete_component_1 = require("./autocomplete.component");
var i18n_module_1 = require("../../i18n/i18n.module");
var SPAutocompleteModule = (function () {
    function SPAutocompleteModule() {
    }
    return SPAutocompleteModule;
}());
SPAutocompleteModule = __decorate([
    ng_module_1.NgModule({
        imports: [angular2_material_module_1.Angular2MaterialModule, forms_1.FormsModule, common_1.CommonModule, i18n_module_1.I18NModule],
        declarations: [autocomplete_component_1.AutocompleteComponent],
        exports: [autocomplete_component_1.AutocompleteComponent],
        providers: [] // services
    })
], SPAutocompleteModule);
exports.SPAutocompleteModule = SPAutocompleteModule;
