import {NgModule} from "@angular/core";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {AutocompleteComponent} from "./autocomplete.component";
import {I18NModule} from "../../i18n/i18n.module";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule, I18NModule],       // module dependencies
  declarations: [AutocompleteComponent],   // components and directives
  exports:[AutocompleteComponent],
  providers: []                    // services
})
export class SPAutocompleteModule { }

