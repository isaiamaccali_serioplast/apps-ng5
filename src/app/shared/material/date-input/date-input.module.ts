import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {I18NModule} from "../../i18n/i18n.module";
import {DateInputComponent} from "./date-input.component";
import {MatDatepickerModule, MatInputModule} from "@angular/material";

@NgModule({
  imports: [MatDatepickerModule,MatInputModule, FormsModule, CommonModule, I18NModule],       // module dependencies
  declarations: [DateInputComponent],   // components and directives
  exports:[DateInputComponent],
  providers: []                    // services
})
export class DateInputModule { }

