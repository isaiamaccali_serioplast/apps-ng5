import {NativeDateAdapter} from "@angular/material";
import {getUrlDate, sapDateToUrlDate} from "../../utils";

export class SPDateAdapter extends NativeDateAdapter {
  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      return new Date(Number(str[2]), Number(str[1])-1, Number(str[0]), 12);
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }

  deserialize(value: any): Date | null {
    if(value){
      return new Date(value);
    }
    else
      return null;
  }

  format(date: Date, displayFormat: any){
    return getUrlDate(date);
  }
}

export class SAPDateAdapter extends NativeDateAdapter {
  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      return new Date(Number(str[2]), Number(str[1])-1, Number(str[0]), 12);
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }

  format(date: Date, displayFormat: any){
    return getUrlDate(date);
  }

  deserialize(value: any): Date | null {
    if(value){
      if (typeof value === 'string'){
        return new Date(sapDateToUrlDate(value));
      }
      else
        return value;
    }
    else
      return null;
  }
}
