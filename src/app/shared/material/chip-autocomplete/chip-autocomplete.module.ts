import {NgModule} from "@angular/core";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {ChipAutocompleteComponent} from "./chip-autocomplete.component";
import {SPAutocompleteModule} from "../autocomplete/autocomplete.module";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule, SPAutocompleteModule],       // module dependencies
  declarations: [ChipAutocompleteComponent],   // components and directives
  exports:[ChipAutocompleteComponent],
  providers: []                    // services
})
export class ChipAutocompleteModule { }

