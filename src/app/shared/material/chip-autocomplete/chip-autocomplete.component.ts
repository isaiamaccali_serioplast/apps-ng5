import {Component, Input, OnInit, SimpleChange} from "@angular/core";

@Component({
  selector: 'sp-chip-autocomplete',
  templateUrl: 'chip-autocomplete.component.html',
  styleUrls: ['chip-autocomplete.component.css']
})
export class ChipAutocompleteComponent implements OnInit {

  @Input() source;
  @Input() minChars;
  @Input() sourceFunction;
  @Input() initialValue;
  @Input() displayKey;
  @Input() label;

  @Input() model = [];


  constructor() {}

  ngOnInit() {}

  ngOnChanges(change:SimpleChange) {
    for(let prop in change){
      if(change.hasOwnProperty(prop)){
        if(prop=='source'){
          this.source = change['source'].currentValue;
        }
        if(prop=='model'){
          this.model = change['model'].currentValue;
        }
      }
    }
  }

  remove(item:any){
    this.model.splice(this.model.indexOf(item),1);
  }

  selectedValue(item){
    if(item){
      this.model.push(item);
    }
  }
}
