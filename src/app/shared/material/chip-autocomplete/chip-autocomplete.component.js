"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var directives_1 = require("@angular/core/src/metadata/directives");
var ChipAutocompleteComponent = (function () {
    function ChipAutocompleteComponent() {
        this.model = [];
    }
    ChipAutocompleteComponent.prototype.ngOnInit = function () { };
    ChipAutocompleteComponent.prototype.ngOnChanges = function (change) {
        for (var prop in change) {
            if (change.hasOwnProperty(prop)) {
                if (prop == 'source') {
                    this.source = change['source'].currentValue;
                }
                if (prop == 'model') {
                    this.model = change['model'].currentValue;
                }
            }
        }
    };
    ChipAutocompleteComponent.prototype.remove = function (item) {
        this.model.splice(this.model.indexOf(item), 1);
    };
    ChipAutocompleteComponent.prototype.selectedValue = function (item) {
        if (item) {
            this.model.push(item);
        }
    };
    return ChipAutocompleteComponent;
}());
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "source", void 0);
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "minChars", void 0);
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "sourceFunction", void 0);
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "initialValue", void 0);
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "displayKey", void 0);
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "label", void 0);
__decorate([
    directives_1.Input()
], ChipAutocompleteComponent.prototype, "model", void 0);
ChipAutocompleteComponent = __decorate([
    core_1.Component({
        selector: 'sp-chip-autocomplete',
        templateUrl: 'chip-autocomplete.component.html',
        styleUrls: ['chip-autocomplete.component.css']
    })
], ChipAutocompleteComponent);
exports.ChipAutocompleteComponent = ChipAutocompleteComponent;
