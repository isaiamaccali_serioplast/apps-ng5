import {Component, OnInit, QueryList, ContentChildren, EventEmitter, Output, Input, AfterViewInit} from "@angular/core";

@Component({
  selector: 'sp-stepper-item-title',
  template: '<ng-content select="*"></ng-content>'
})
export class StepperItemTitleComponent implements OnInit {

  open: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }
}

@Component({
  selector: 'sp-stepper-item-content-active',
  template: '<ng-content select="*"></ng-content>'
})
export class StepperItemContentActiveComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

@Component({
  selector: 'sp-stepper-item-content-inactive',
  template: '<ng-content select="*"></ng-content>'
})
export class StepperItemContentInactiveComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

@Component({
  selector: 'sp-stepper-item',
  templateUrl: 'stepper-item.component.html',
  styleUrls: ['stepper-item.component.css']
})
export class StepperItemComponent implements OnInit {

  index: number;
  globalIndex: number;
  ready = false;

  @Input() nextCondition: any;
  @Output() onConfirm = new EventEmitter();
  @Output() onPrevious = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    // setTimeout(()=>{
    //   this.ready = true;
    // },500)
    // this.ready = true;
  }

  confirm() {
    this.onConfirm.emit(this.index);
  }

  previous() {
    this.onPrevious.emit(this.index);
  }

  getClass() {
    if (this.globalIndex != null && this.index != null)
      return this.globalIndex >= this.index ? 'active' : 'inactive';
  }

  isReady() {
    return this.ready;
  }
}

@Component({
  selector: 'sp-stepper',
  templateUrl: 'stepper.component.html',
  styleUrls: ['stepper.component.css']
})
export class StepperComponent implements AfterViewInit {

  @ContentChildren(StepperItemComponent) items: QueryList<StepperItemComponent>;

  globalIndex = 0;

  constructor() {
  }

  ngAfterViewInit() {
    this.items.forEach((i: StepperItemComponent, i_) => {
      i.index = i_;
      i.globalIndex = this.globalIndex;
      i.onConfirm.subscribe((index) => {
        this.globalIndex++;
        this.items.forEach((i: StepperItemComponent, i_) => {
          i.globalIndex = this.globalIndex;
        });
      });
      i.onPrevious.subscribe((index) => {
        this.globalIndex--;
        this.items.forEach((i: StepperItemComponent, i_) => {
          i.globalIndex = this.globalIndex;
        });
      });
      i.index = i_;
      setTimeout(() => {
        i.ready = true;
      }, 500)

    });
  }

  setIndex(index: number) {
    this.globalIndex = index;
    this.items.forEach((i: StepperItemComponent, i_) => {
      i.globalIndex = this.globalIndex;
    });
  }

  reset() {
    this.setIndex(0);
  }

  reCheck() {
    this.items.forEach((i: StepperItemComponent, i_) => {
      let prevCond = i.nextCondition;
      i.nextCondition = null;
      i.nextCondition = prevCond;
    });
  }
}
