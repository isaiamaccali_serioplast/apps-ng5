"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var StepperItemTitleComponent = (function () {
    function StepperItemTitleComponent() {
        this.open = false;
    }
    StepperItemTitleComponent.prototype.ngOnInit = function () { };
    return StepperItemTitleComponent;
}());
StepperItemTitleComponent = __decorate([
    core_1.Component({
        selector: 'sp-stepper-item-title',
        template: '<ng-content select="*"></ng-content>'
    })
], StepperItemTitleComponent);
exports.StepperItemTitleComponent = StepperItemTitleComponent;
var StepperItemContentActiveComponent = (function () {
    function StepperItemContentActiveComponent() {
    }
    StepperItemContentActiveComponent.prototype.ngOnInit = function () { };
    return StepperItemContentActiveComponent;
}());
StepperItemContentActiveComponent = __decorate([
    core_1.Component({
        selector: 'sp-stepper-item-content-active',
        template: '<ng-content select="*"></ng-content>'
    })
], StepperItemContentActiveComponent);
exports.StepperItemContentActiveComponent = StepperItemContentActiveComponent;
var StepperItemContentInactiveComponent = (function () {
    function StepperItemContentInactiveComponent() {
    }
    StepperItemContentInactiveComponent.prototype.ngOnInit = function () { };
    return StepperItemContentInactiveComponent;
}());
StepperItemContentInactiveComponent = __decorate([
    core_1.Component({
        selector: 'sp-stepper-item-content-inactive',
        template: '<ng-content select="*"></ng-content>'
    })
], StepperItemContentInactiveComponent);
exports.StepperItemContentInactiveComponent = StepperItemContentInactiveComponent;
var StepperItemComponent = (function () {
    function StepperItemComponent() {
        this.onConfirm = new core_1.EventEmitter();
        this.onPrevious = new core_1.EventEmitter();
    }
    StepperItemComponent.prototype.ngOnInit = function () { };
    StepperItemComponent.prototype.confirm = function () {
        this.onConfirm.emit(this.index);
    };
    StepperItemComponent.prototype.previous = function () {
        this.onPrevious.emit(this.index);
    };
    return StepperItemComponent;
}());
__decorate([
    core_1.Input()
], StepperItemComponent.prototype, "nextCondition", void 0);
__decorate([
    core_1.Output()
], StepperItemComponent.prototype, "onConfirm", void 0);
__decorate([
    core_1.Output()
], StepperItemComponent.prototype, "onPrevious", void 0);
StepperItemComponent = __decorate([
    core_1.Component({
        selector: 'sp-stepper-item',
        templateUrl: 'stepper-item.component.html',
        styleUrls: ['stepper-item.component.css']
    })
], StepperItemComponent);
exports.StepperItemComponent = StepperItemComponent;
var StepperComponent = (function () {
    function StepperComponent() {
        this.globalIndex = 0;
    }
    StepperComponent.prototype.ngAfterViewInit = function () {
        var self = this;
        this.items.forEach(function (i, i_) {
            i.index = i_;
            i.globalIndex = self.globalIndex;
            i.onConfirm.subscribe(function (index) {
                self.globalIndex++;
                self.items.forEach(function (i, i_) {
                    i.globalIndex = self.globalIndex;
                });
            });
            i.onPrevious.subscribe(function (index) {
                self.globalIndex--;
                self.items.forEach(function (i, i_) {
                    i.globalIndex = self.globalIndex;
                });
            });
            i.index = i_;
        });
    };
    return StepperComponent;
}());
__decorate([
    core_1.ContentChildren(StepperItemComponent)
], StepperComponent.prototype, "items", void 0);
StepperComponent = __decorate([
    core_1.Component({
        selector: 'sp-stepper',
        templateUrl: 'stepper.component.html',
        styleUrls: ['stepper.component.css']
    })
], StepperComponent);
exports.StepperComponent = StepperComponent;
