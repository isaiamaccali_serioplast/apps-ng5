import {NgModule} from "@angular/core";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {
  StepperComponent, StepperItemComponent, StepperItemContentActiveComponent,
  StepperItemContentInactiveComponent, StepperItemTitleComponent
} from "./stepper.component";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule],       // module dependencies
  declarations: [StepperComponent,StepperItemComponent, StepperItemContentActiveComponent, StepperItemContentInactiveComponent,StepperItemTitleComponent],   // components and directives
  exports:[StepperComponent,StepperItemComponent, StepperItemContentActiveComponent, StepperItemContentInactiveComponent, StepperItemTitleComponent],
  providers: []                    // services
})
export class StepperModule { }

