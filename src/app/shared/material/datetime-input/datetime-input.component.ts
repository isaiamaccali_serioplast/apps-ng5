import {Component, forwardRef, Input} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import {noop} from "rxjs";

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DateTimeInputComponent),
  multi: true
};

@Component({
  selector: 'datetime-input',
  templateUrl: 'datetime-input.component.html',
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class DateTimeInputComponent implements ControlValueAccessor {

  @Input('placeholder') label:string;
  @Input('disabled') disabled = false;
  @Input('min') min:any;
  @Input('max') max:any;

  date:Date;
  time = "00:00";

  //The internal data model
  private innerValue: any = '';

  //Placeholders for the callbacks which are later providesd
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  //get accessor
  get value(): any {
    return this.innerValue;
  };

  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      if(v){
        let d = new Date(v);
        this.date = d;
        this.time = d.getHours()+":"+d.getMinutes();
      }
      this.onChangeCallback(v);
    }
  }

  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
      if(value){
        let d = new Date(value);
        this.date = d;
        this.time = d.getHours()+":"+d.getMinutes();
      }
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  updateValue(){
    if(this.date && this.time){
      let spl = this.time.split(":");
      this.date.setHours(parseInt(spl[0],10),parseInt(spl[1],10),0,0);
      this.value = new Date(this.date);
    }
  }

}
