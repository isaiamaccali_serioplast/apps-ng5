import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {I18NModule} from "../../i18n/i18n.module";
import {MatDatepickerModule, MatInputModule} from "@angular/material";
import {DateTimeInputComponent} from "./datetime-input.component";
// import {AmazingTimePickerModule} from "amazing-time-picker";

@NgModule({
  imports: [MatDatepickerModule,MatInputModule, FormsModule, CommonModule, I18NModule],       // module dependencies
  declarations: [DateTimeInputComponent],   // components and directives
  exports:[DateTimeInputComponent],
  providers: []                    // services
})
export class DateTimeInputModule { }

