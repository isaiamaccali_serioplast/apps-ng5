import {NgModule} from "@angular/core";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {TableWithPagesComponent} from "./table-with-pages.component";
import {I18NModule} from "../../i18n/i18n.module";
import {SanitizeHTMLModule} from "../../pipes/sanitizehtml.module";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule, I18NModule, SanitizeHTMLModule],       // module dependencies
  declarations: [TableWithPagesComponent],   // components and directives
  exports:[TableWithPagesComponent],
  providers: []                    // services
})
export class TableWithPagesModule { }

