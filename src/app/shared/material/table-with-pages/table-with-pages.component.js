"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var directives_1 = require("@angular/core/src/metadata/directives");
var HeaderConf = (function () {
    function HeaderConf() {
        this.active = true;
        this.mandatory = true;
    }
    return HeaderConf;
}());
exports.HeaderConf = HeaderConf;
var TableRowAction = (function () {
    function TableRowAction() {
    }
    return TableRowAction;
}());
exports.TableRowAction = TableRowAction;
var TABLEACTIONVIEW = new TableRowAction();
TABLEACTIONVIEW.icon = 'visibility';
TABLEACTIONVIEW.tooltip = 'View';
var TABLEACTIONEDIT = new TableRowAction();
TABLEACTIONEDIT.icon = 'edit';
TABLEACTIONEDIT.tooltip = 'Edit';
var TABLEACTIONDELETE = new TableRowAction();
TABLEACTIONDELETE.icon = 'delete';
TABLEACTIONDELETE.tooltip = 'Delete';
var TABLEACTIONRESTORE = new TableRowAction();
TABLEACTIONRESTORE.icon = 'open_in_browser';
TABLEACTIONRESTORE.tooltip = 'Restore';
exports.TableRowActions = {
    VIEW: TABLEACTIONVIEW,
    EDIT: TABLEACTIONEDIT,
    DELETE: TABLEACTIONDELETE,
    RESTORE: TABLEACTIONRESTORE
};
var TableWithPagesComponent = (function () {
    function TableWithPagesComponent() {
        this.blankValue = "-";
        this.noItemsMessage = "";
        this.headerConf = [];
        this.nResults = null;
        this.small = false;
        this.clickable = false;
        this.rowclick = new core_1.EventEmitter();
        this.items = [];
        this.firstLoad = false;
        this.loading = false;
    }
    TableWithPagesComponent.prototype.ngOnInit = function () {
    };
    TableWithPagesComponent.prototype.ngOnChanges = function (change) {
        for (var prop in change) {
            if (change.hasOwnProperty(prop)) {
                if (prop == 'noItemsMessage') {
                    this.noItemsMessage = change['noItemsMessage'].currentValue;
                }
                if (prop == 'headerConf') {
                    this.headerConf = change['headerConf'].currentValue;
                }
                if (prop == 'availableActions') {
                    this.availableActions = change['availableActions'].currentValue;
                }
                if (prop == 'getData') {
                    this.getData = change['getData'].currentValue;
                }
                if (prop == 'nResults') {
                    this.nResults = change['nResults'].currentValue;
                }
            }
        }
        if (this.headerConf.length > 0 && this.getData) {
            if (!this.firstLoad) {
                this.firstLoad = true;
                this.reload();
            }
        }
    };
    TableWithPagesComponent.prototype.getValue = function (item, headerConf) {
        var returnValue = "";
        if (headerConf.valueKey) {
            if (headerConf.useStr) {
                returnValue = item[headerConf.valueKey] ? (item[headerConf.valueKey]['__str__'] || this.blankValue) : this.blankValue;
            }
            else {
                returnValue = item[headerConf.valueKey] || this.blankValue;
            }
        }
        else if (headerConf.valueFunction) {
            returnValue = headerConf.valueFunction(item) || this.blankValue;
        }
        else {
            returnValue = this.blankValue;
        }
        return returnValue;
    };
    TableWithPagesComponent.prototype.goToFirstPage = function () {
        this.currentPage = 1;
        this.retrieveData();
    };
    TableWithPagesComponent.prototype.goToLastPage = function () {
        this.currentPage = this.totalPages;
        this.retrieveData();
    };
    TableWithPagesComponent.prototype.goToPrevPage = function () {
        if (this.currentPage != 1) {
            this.currentPage--;
            this.retrieveData();
        }
    };
    TableWithPagesComponent.prototype.goToNextPage = function () {
        if (this.currentPage != this.totalPages) {
            this.currentPage++;
            this.retrieveData();
        }
    };
    TableWithPagesComponent.prototype.handleResp = function (data) {
        if (data && data['count'] && data['count'] > 0) {
            this.total = data['count'];
            if (this.expandData) {
                if (this.expandDataAsync) {
                    var self_1 = this;
                    this.expandData(data['results']).subscribe(function (items) {
                        self_1.loading = false;
                        self_1.items = items;
                    });
                }
                else {
                    this.loading = false;
                    this.items = this.expandData(data['results']);
                }
            }
            else {
                this.loading = false;
                this.items = data['results'];
            }
            var page = this.currentPage;
            this.totalPages = Math.ceil(data['count'] / this.nResults);
            if (this.currentPage != this.totalPages) {
                this.interval = (this.nResults * (page - 1) + 1) + "-" + (this.nResults * page);
            }
            else {
                this.interval = (Math.floor(this.total / this.nResults) * this.nResults + 1) + "-" + (this.total);
            }
        }
        else {
            this.loading = false;
            this.items = [];
        }
    };
    TableWithPagesComponent.prototype.handleRespSap = function (data) {
        if (data && data['response'] && data['response']['results'] && data['response']['results'].length > 0) {
            this.total = data['response']['results'].length;
            if (this.expandData) {
                if (this.expandDataAsync) {
                    var self_2 = this;
                    this.expandData(data['response']['results']).subscribe(function (items) {
                        self_2.loading = false;
                        self_2.items = items;
                    });
                }
                else {
                    this.loading = false;
                    this.items = this.expandData(data['response']['results']);
                }
            }
            else {
                this.loading = false;
                this.items = data['response']['results'];
            }
            var page = this.currentPage;
            this.totalPages = Math.ceil(data['response']['results'].length / this.nResults);
            if (this.currentPage != this.totalPages) {
                this.interval = (this.nResults * (page - 1) + 1) + "-" + (this.nResults * page);
            }
            else {
                this.interval = (Math.floor(this.total / this.nResults) * this.nResults + 1) + "-" + (this.total);
            }
        }
        else {
            this.loading = false;
            this.items = [];
        }
    };
    TableWithPagesComponent.prototype.reload = function () {
        this.currentPage = 1;
        this.retrieveData();
    };
    TableWithPagesComponent.prototype.retrieveData = function () {
        this.loading = true;
        this.getData(this.currentPage, this.nResults);
    };
    TableWithPagesComponent.prototype.callClickAction = function (a, i) {
        if (a.click) {
            a.click(i);
        }
    };
    TableWithPagesComponent.prototype.innerClickRow = function (item) {
        if (this.clickable)
            this.rowclick.emit(item);
    };
    return TableWithPagesComponent;
}());
__decorate([
    core_1.ViewChild("table")
], TableWithPagesComponent.prototype, "table", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "noItemsMessage", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "headerConf", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "availableActions", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "getData", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "expandData", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "expandDataAsync", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "nResults", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "small", void 0);
__decorate([
    directives_1.Input()
], TableWithPagesComponent.prototype, "clickable", void 0);
__decorate([
    core_1.Output()
], TableWithPagesComponent.prototype, "rowclick", void 0);
TableWithPagesComponent = __decorate([
    core_1.Component({
        selector: 'sp-table-with-pages',
        templateUrl: 'table-with-pages.component.html',
        styleUrls: ['table-with-pages.component.css']
    })
], TableWithPagesComponent);
exports.TableWithPagesComponent = TableWithPagesComponent;
