import {Component, EventEmitter, Input, OnInit, Output, SimpleChange, ViewChild} from "@angular/core";
import {MatPaginator, MatSort, MatTableDataSource, PageEvent} from "@angular/material";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {Observable, of as observableOf, merge, Subscription} from "rxjs";
import {parseSerionetRespToData} from "../../utils";

export class HeaderConf {
  columnId?:string;
  valueKey?: string;
  valueFunction?: any;
  label?: string;
  useStr?: boolean;
  active?: boolean = true;
  mandatory?: boolean = true;
  isBoolean?:boolean = false;
}

export class TableRowAction {
  icon: string;
  visibilityCondition?: any;
  click?: any; //function
  tooltip?: string;
}

let TABLEACTIONVIEW = new TableRowAction();
TABLEACTIONVIEW.icon = 'visibility';
TABLEACTIONVIEW.tooltip = 'View';

let TABLEACTIONEDIT = new TableRowAction();
TABLEACTIONEDIT.icon = 'edit';
TABLEACTIONEDIT.tooltip = 'Edit';

let TABLEACTIONDELETE = new TableRowAction();
TABLEACTIONDELETE.icon = 'delete';
TABLEACTIONDELETE.tooltip = 'Delete';

let TABLEACTIONRESTORE = new TableRowAction();
TABLEACTIONRESTORE.icon = 'open_in_browser';
TABLEACTIONRESTORE.tooltip = 'Restore';

let TABLEACTIONMOVEUP = new TableRowAction();
TABLEACTIONMOVEUP.icon = 'arrow_upward';
TABLEACTIONMOVEUP.tooltip = 'Move up';

let TABLEACTIONMOVEDOWN = new TableRowAction();
TABLEACTIONMOVEDOWN.icon = 'arrow_downward';
TABLEACTIONMOVEDOWN.tooltip = 'Move down';

let TABLEACTIONMOVETO = new TableRowAction();
TABLEACTIONMOVETO.icon = 'low_priority';
TABLEACTIONMOVETO.tooltip = 'Move to';

export const TableRowActions: any = {
  VIEW: TABLEACTIONVIEW,
  EDIT: TABLEACTIONEDIT,
  DELETE: TABLEACTIONDELETE,
  RESTORE: TABLEACTIONRESTORE,
  MOVEUP: TABLEACTIONMOVEUP,
  MOVEDOWN: TABLEACTIONMOVEDOWN,
  MOVETO: TABLEACTIONMOVETO
};

@Component({
  selector: 'sp-table-with-pages',
  templateUrl: 'table-with-pages.component.html',
  styleUrls: ['table-with-pages.component.css']
})
export class TableWithPagesComponent implements OnInit {

  @Input() mode: string = "serionet";
  @Input() noItemsMessage: string = "";
  @Input() headerConf: HeaderConf[] = [];
  @Input() availableActions: TableRowAction[];
  @Input() getData: any;
  @Input() expandData: any;
  @Input() expandDataAsync: boolean;
  @Input() nResults = 20;
  @Input() small: boolean = false;
  @Input() clickable = false;
  @Output() rowclick = new EventEmitter();
  @Input() selectable = false;
  @Input() ready = true;
  @Input() showIndex = false;
  @Input() indexOnLeft = true;
  @Input() renderOnly = false;
  @Input() data:any[] = [];
  @Input() getRowClass:any;
  @Input() showPaginator = true;

  total: number;
  blankValue = "-";
  firstLoad = false;

  displayedColumns = [];
  dataSource = new MatTableDataSource();

  resultsLength = 0;
  loading = true;
  selectAll = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(){
    this.headerConf.forEach((hc,index)=>{
      if(!hc.columnId)
        hc.columnId = index+"";
    });
    this.displayedColumns = this.getActiveHC().map((hc)=>hc.columnId);
    if(this.availableActions.length>0)
      this.displayedColumns.push('actions');

    if(this.selectable)
      this.displayedColumns = ['select'].concat(this.displayedColumns);

    this.checkShowIndex();
    if(this.ready)
      this.init();
  }

  checkShowIndex(){
    if(this.showIndex){
      if(this.displayedColumns.indexOf("index")==-1){
        if(this.indexOnLeft){
          this.displayedColumns = ['index'].concat(this.displayedColumns);
        }
        else {
          this.displayedColumns.push('index');
        }
      }
    }
    else {
      if(this.displayedColumns.indexOf("index")!=-1){
        this.displayedColumns.splice(this.displayedColumns.indexOf("index"),1);
      }
    }
  }

  getActiveHC(){
    return this.headerConf.filter((hc)=>hc.active!=false);
  }

  initialized = false;
  pageObserver:Subscription;

  init() {
    if(!this.initialized){
      this.initialized = true;
      this.pageObserver = merge(/*this.sort.sortChange,*/ this.paginator.page)
        .pipe(
          // startWith({pageIndex:0,pageSize:this.nResults}),
          switchMap((data:PageEvent) => {
            this.loading = true;
            this.dataSource.data = [];
            if(this.renderOnly){
              return this.getDataRender(data);
            }
            else {
              return this.getData(data.pageIndex+1,data.pageSize)
            }
          }),
          map((data) => {
            if(this.renderOnly){
              return data;
            }
            else {
              if(this.mode=='serionet')
                return this.handleRespSerionet(data);
              else
                return this.handleRespSAP(data);
            }
          }),
          catchError(() => {
            this.loading = false;
            return observableOf([]);
          })
        ).subscribe(
        (data) => {
          if (this.expandData) {
            if (this.expandDataAsync) {
              this.expandData(data).subscribe((items) => {
                this.loading = false;
                this.dataSource.data = items;
              });
            }
            else {
              this.loading = false;
              this.dataSource.data = this.expandData(data);
            }
          }
          else {
            this.loading = false;
            this.dataSource.data = data;
          }
        }
      );
    }
  }

  getDataRender(data){
    return Observable.create((observer)=>{
      let items = [];
      if(data.pageSize){
        let start = (data.pageSize * (data.pageIndex));
        let end = start+data.pageSize;
        items = this.data.slice(start,end);
      }
      else {
        items = this.data;
      }
      this.resultsLength = this.data.length;
      this.showPaginator = this.resultsLength>this.nResults;
      observer.next(items);
      observer.complete();
    })
  }

  getValue(item: any, headerConf: HeaderConf) {
    let returnValue = "";
    if (headerConf.valueKey) {
      if (headerConf.useStr) {
        returnValue = item[headerConf.valueKey] ? (item[headerConf.valueKey]['__str__'] || this.blankValue) : this.blankValue;
      }
      else {
        if (headerConf.isBoolean) {
          returnValue = item[headerConf.valueKey]?"<i class='material-icons green-text'>done</i>" : "<i class='material-icons red-text'>clear</i>";
        }
        else {
          returnValue = item[headerConf.valueKey] || this.blankValue;
        }
      }

    }
    else if (headerConf.valueFunction) {
      if (headerConf.isBoolean) {
        returnValue =  headerConf.valueFunction(item)?"<i class='material-icons green-text'>done</i>" : "<i class='material-icons red-text'>clear</i>";
      }
      else {
        returnValue = headerConf.valueFunction(item) || this.blankValue;
      }
    }
    else {
      returnValue = this.blankValue;
    }

    return returnValue
  }

  callClickAction(a: TableRowAction, i) {
    if (a.click) {
      a.click(i);
    }
  }

  callClassAction(i){
    if(this.getRowClass)
      return this.getRowClass.call(this.getRowClass.context,i);
  }

  innerClickRow(item: any) {
    if (this.clickable)
      this.rowclick.emit(item);
  }

  reload() {
    this.paginator.pageIndex = 0;
    let pe = new PageEvent();
    pe.pageSize = this.nResults;
    pe.pageIndex = 0;
    this.paginator.page.next(pe);
  }

  getSelectedItems(){
    if(this.dataSource.data)
      return this.dataSource.data.filter((i)=>i['selected']);
    else
      return [];
  }

  ngOnChanges(change: SimpleChange) {

    let forceReload = false;
    for (let prop in change) {
      if (change.hasOwnProperty(prop)) {
        if (prop == 'noItemsMessage') {
          this.noItemsMessage = change['noItemsMessage'].currentValue;
        }
        if (prop == 'headerConf') {
          this.headerConf = change['headerConf'].currentValue;
        }
        if (prop == 'availableActions') {
          this.availableActions = change['availableActions'].currentValue;
          if(this.displayedColumns.indexOf('actions')==-1)
            this.displayedColumns.push('actions');
        }
        if (prop == 'getData') {
          this.getData = change['getData'].currentValue;
        }
        if (prop == 'nResults') {
          this.nResults = change['nResults'].currentValue;
          forceReload = true;
        }
        if (prop == 'ready') {
          this.ready = change['ready'].currentValue;
          if(this.ready)
            this.init();
        }
        if (prop == 'showIndex') {
          this.showIndex = change['showIndex'].currentValue;
          this.checkShowIndex();
        }
        if (prop == 'data') {
          if(JSON.stringify(this.data)!=JSON.stringify(change['data'].currentValue)){
            this.data = change['data'].currentValue;
            forceReload = true;
          }
        }
      }
    }

    if (this.headerConf.length > 0 && (this.getData||(this.data&&this.renderOnly)) && this.ready) {
      if (!this.firstLoad) {
        this.firstLoad = true;
        this.init();
        this.reload();
      }
      else {
        if(forceReload){
          this.reload();
        }
      }
    }
  }

  handleRespSerionet(data) {
    this.resultsLength = data.count;
    this.showPaginator = this.resultsLength>this.nResults;
    return parseSerionetRespToData(data);
  }

  handleRespSAP(data) {
    this.resultsLength = data['response']['__count']|| data['response']['results'].length;
    this.showPaginator = this.resultsLength>this.nResults;
    return data['response']['results'];
  }

  toggleSelectAll(){
    if(this.selectAll){
      this.dataSource.data.forEach((i)=>i['selected']=true);
    }
    else {
      this.dataSource.data.forEach((i)=>i['selected']=false);
    }
  }

  setPage(page){}

  getPage(){}

  getItems():any[]{
    return this.dataSource.data;
  }
}
