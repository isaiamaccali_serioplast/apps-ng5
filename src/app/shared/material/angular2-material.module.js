"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Module used to share the angular2-material modules (and temporarily angular2-materialize
 */
var core_1 = require("@angular/core");
require("materialize-css");
require("angular2-materialize");
var angular2_materialize_1 = require("angular2-materialize");
var material_1 = require("@angular/material");
var Angular2MaterialModule = (function () {
    function Angular2MaterialModule() {
    }
    return Angular2MaterialModule;
}());
Angular2MaterialModule = __decorate([
    core_1.NgModule({
        imports: [angular2_materialize_1.MaterializeModule, material_1.MaterialModule.forRoot()],
        exports: [angular2_materialize_1.MaterializeModule, material_1.MaterialModule],
        providers: [] // services
    })
], Angular2MaterialModule);
exports.Angular2MaterialModule = Angular2MaterialModule;
