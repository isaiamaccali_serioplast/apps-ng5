import {Component, Output, EventEmitter, Input, ViewChild, TemplateRef, SimpleChange} from "@angular/core";
import {MatDialog, MatDialogRef} from "@angular/material";

@Component({
  selector: 'sp-dialog',
  templateUrl: 'dialog.component.html',
  styleUrls: ['dialog.component.css']
})
export class DialogComponent{

  @Input('dialogClass') dialogClass:string;
  @Input('behind') behind = false;
  @Input('onTop') onTop = false;
  @Input('nopadding') nopadding = false;
  @Input() closeByOverlayClick:boolean = true;
  @Output() onClose= new EventEmitter();
  @Output() onOpen = new EventEmitter();
  @ViewChild('template') template: TemplateRef<any>;

  opened = false;

  dialogRef:any;

  constructor(private dialog: MatDialog) {}

  ngOnInit() {}

  open(){

    this.dialogRef = this.dialog.open(this.template, {
      disableClose:!this.closeByOverlayClick,
      panelClass:this.dialogClass?this.dialogClass:(this.nopadding?"no-padding-dialog":""),
      maxHeight:'90%',
      maxWidth:'90%'
    });
    this.dialogRef.afterOpen().subscribe(()=>{
      this.opened = true;
      this.onOpen.emit();
    });

    this.dialogRef.afterClosed().subscribe(() => {
      this.opened = false;
      this.onClose.emit();
    });
  }

  close(){
    if(this.dialogRef)
      this.dialogRef.close();
  }

  ngOnChanges(change: SimpleChange) {
    for (let prop in change) {
      if (change.hasOwnProperty(prop)) {
        if (prop == 'closeByOverlayClick') {
          this.closeByOverlayClick = change['closeByOverlayClick'].currentValue;
        }
      }
    }
  }

}
