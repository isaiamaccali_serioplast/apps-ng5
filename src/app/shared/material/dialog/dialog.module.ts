import {NgModule} from "@angular/core";
import {DialogComponent} from "./dialog.component";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule],       // module dependencies
  declarations: [DialogComponent],   // components and directives
  exports:[DialogComponent],
  providers: []                    // services
})
export class DialogModule { }

