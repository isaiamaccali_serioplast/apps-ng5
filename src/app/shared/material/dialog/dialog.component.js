"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var di_1 = require("@angular/core/src/metadata/di");
var directives_1 = require("@angular/core/src/metadata/directives");
var DialogComponent = (function () {
    function DialogComponent(idleService) {
        this.idleService = idleService;
        this.opened = false;
        this.hide = true;
        this.behind = false;
        this.onClose = new core_1.EventEmitter();
        this.onOpen = new core_1.EventEmitter();
        if (!document.querySelector(".dialogs-container")) {
            var container = document.createElement("div");
            container.setAttribute("class", "dialogs-container");
            document.querySelector("body").appendChild(container);
        }
        this.closeByOverlayClick = true;
    }
    DialogComponent.prototype.ngOnInit = function () {
    };
    DialogComponent.prototype.ngAfterViewInit = function () {
        var container = this.container.nativeElement;
        document.querySelector(".dialogs-container").appendChild(container);
    };
    DialogComponent.prototype.open = function () {
        this.hide = false;
        var self = this;
        this.idleService.registerDialog(this);
        setTimeout(function () {
            self.opened = true;
            if (self.onOpen)
                self.onOpen.emit(true);
        }, 500);
    };
    DialogComponent.prototype.close = function () {
        this.opened = false;
        var self = this;
        this.idleService.unregisterDialog();
        setTimeout(function () {
            self.hide = true;
            if (self.onClose) {
                setTimeout(function () {
                    self.onClose.emit(true);
                }, 100);
            }
        }, 500);
    };
    return DialogComponent;
}());
__decorate([
    directives_1.Input('dialogClass')
], DialogComponent.prototype, "class_", void 0);
__decorate([
    directives_1.Input('behind')
], DialogComponent.prototype, "behind", void 0);
__decorate([
    di_1.ViewChild('myContainer')
], DialogComponent.prototype, "container", void 0);
__decorate([
    directives_1.Input()
], DialogComponent.prototype, "nopadding", void 0);
__decorate([
    directives_1.Input()
], DialogComponent.prototype, "closeByOverlayClick", void 0);
__decorate([
    core_1.Output()
], DialogComponent.prototype, "onClose", void 0);
__decorate([
    core_1.Output()
], DialogComponent.prototype, "onOpen", void 0);
DialogComponent = __decorate([
    core_1.Component({
        selector: 'sp-dialog',
        templateUrl: 'dialog.component.html',
        styleUrls: ['dialog.component.css']
    })
], DialogComponent);
exports.DialogComponent = DialogComponent;
