import {NgModule} from "@angular/core";
import {MessageComponent} from "./message.component";
import {Angular2MaterialModule} from "../angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {DialogModule} from "../dialog/dialog.module";
import {I18NModule} from "../../i18n/i18n.module";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule, CommonModule, DialogModule, I18NModule],       // module dependencies
  declarations: [MessageComponent],   // components and directives
  exports:[MessageComponent],
  providers: []                    // services
})
export class ConfirmModule { }

