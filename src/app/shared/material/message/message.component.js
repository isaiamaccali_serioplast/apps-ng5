"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var di_1 = require("@angular/core/src/metadata/di");
var MessageComponent = (function () {
    function MessageComponent(confirmService) {
        this.confirmService = confirmService;
        this.error = false;
        this.confirmService.register(this);
    }
    MessageComponent.prototype.setOptions = function (options) {
        this.confirmStatus = null;
        if (options['error'] && !options['confirm'])
            options['confirm'] = function () { };
        this.title = options['title'] || null;
        this.message = options['message'] || null;
        this.confirmFunction = options['confirm'] || null;
        this.denyFunction = options['deny'] || null;
        this.onOpenFunction = options['onOpen'] || null;
        this.onCloseFunction = options['onClose'] || null;
        this.autoClose = options['autoClose'] || false;
        this.error = options['error'] || false;
    };
    MessageComponent.prototype.open = function () {
        this.confirmDialog.open();
        if (!this.confirmFunction || this.autoClose) {
            var self_1 = this;
            setTimeout(function () {
                self_1.close();
            }, 3000);
        }
    };
    MessageComponent.prototype.close = function () {
        this.confirmDialog.close();
    };
    MessageComponent.prototype.confirm = function () {
        this.confirmStatus = true;
        this.close();
    };
    MessageComponent.prototype.deny = function () {
        this.confirmStatus = false;
        this.close();
    };
    MessageComponent.prototype.onOpenDialog = function () {
        if (this.onOpenFunction)
            this.onOpenFunction();
    };
    MessageComponent.prototype.onCloseDialog = function () {
        if (this.confirmStatus == true) {
            if (this.confirmFunction) {
                this.confirmFunction();
            }
        }
        else {
            if (this.confirmStatus == false) {
                if (this.denyFunction) {
                    this.denyFunction();
                }
            }
        }
        if (this.onCloseFunction)
            this.onCloseFunction();
    };
    return MessageComponent;
}());
__decorate([
    di_1.ViewChild('confirmDialog')
], MessageComponent.prototype, "confirmDialog", void 0);
MessageComponent = __decorate([
    core_1.Component({
        selector: 'sp-confirm',
        templateUrl: 'message.component.html',
        styleUrls: ['message.component.css']
    })
], MessageComponent);
exports.MessageComponent = MessageComponent;
