import {Component, ViewChild} from "@angular/core";
import {DialogComponent} from "../dialog/dialog.component";
import {MessageService} from "./message.service";
import {LoadingService} from "../../loading/loading.service";

@Component({
  selector: 'sp-confirm',
  templateUrl: 'message.component.html',
  styleUrls: ['message.component.css']
})
export class MessageComponent {

  @ViewChild('confirmDialog') confirmDialog:DialogComponent;

  title:string;
  message:string;
  confirmFunction:any;
  denyFunction:any;
  error = false;
  onOpenFunction:any;
  onCloseFunction:any;
  autoClose:boolean;
  confirmStatus:boolean;
  closeByOverlayClick = true;

  constructor(private confirmService:MessageService, private loadingService:LoadingService){
    this.confirmService.register(this);
  }

  setOptions(options:any){
    this.confirmStatus = null;
    if(options['error'] && !options['confirm'])
      options['confirm'] = ()=>{};
    this.title = options['title']||null;
    this.message = options['message']||null;
    this.confirmFunction = options['confirm']||null;
    this.denyFunction = options['deny']||null;
    this.onOpenFunction = options['onOpen']||null;
    this.onCloseFunction = options['onClose']||null;
    this.autoClose = options['autoClose']||false;
    this.error = options['error']||false;
    this.closeByOverlayClick = options['closeByOverlayClick']||true;

  }

  open(){
    this.loadingService.forceHide();
    this.confirmDialog.open();
    if(!this.confirmFunction||this.autoClose){
      setTimeout(() =>{
        this.close();
      },3000)
    }
  }

  close(){
    this.confirmDialog.close();
  }

  confirm(){
    this.confirmStatus = true;
    this.close();
  }

  deny(){
    this.confirmStatus = false;
    this.close();

  }

  onOpenDialog(){
    if(this.onOpenFunction)
      this.onOpenFunction();
  }

  onCloseDialog(){
    if(this.confirmStatus == true){
      if(this.confirmFunction){
        this.confirmFunction();
      }
    }
    else {
      if(this.confirmStatus == false){
        if(this.denyFunction){
          this.denyFunction();
        }
      }
    }
    if(this.onCloseFunction)
      this.onCloseFunction();
  }
}
