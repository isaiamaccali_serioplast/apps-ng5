/**
 * Service used to open confirm dialog
 */
import {Injectable} from "@angular/core";
import {MessageComponent} from "./message.component";

@Injectable()
export class MessageService{

  component:MessageComponent;

  constructor() {}

  register(confirmComponent:MessageComponent){
    this.component = confirmComponent;
  }

  open(options:any){
    if(this.component){
      this.component.setOptions(options);
      this.component.open();
    }
  }
}
