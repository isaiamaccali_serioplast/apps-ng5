import {Component, OnInit} from "@angular/core";
import {TranslationService} from "../../translation.service";

@Component({
  selector: 'sp-language-selector',
  templateUrl: 'language-selector.component.html',
  styleUrls: ['language-selector.component.css']
})
export class LanguageSelectorComponent implements OnInit {

  language:string;

  constructor(protected translationService:TranslationService) {
    this.language = this.translationService.language;
  }

  ngOnInit() {

  }

  changeLanguage(){
    this.translationService.setLanguage(this.language);

    //Fix to regenerate language select (conflict with materializecss)
    let al = this.translationService.availableLanguages;
    this.translationService.availableLanguages = [];
    setTimeout(()=>{
      this.translationService.availableLanguages = al;
    },0);
  }


}
