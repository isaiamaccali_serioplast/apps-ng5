import {NgModule} from "@angular/core";
import {LanguageSelectorComponent} from "./language-selector.component";
import {Angular2MaterialModule} from "../../../material/angular2-material.module";
import {I18NModule} from "../../i18n.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [Angular2MaterialModule, FormsModule,I18NModule, CommonModule],       // module dependencies
  declarations: [LanguageSelectorComponent],   // components and directives
  exports:[LanguageSelectorComponent],
  providers: []                    // services
})
export class LanguageSelectorModule { }

