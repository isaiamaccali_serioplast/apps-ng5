"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var LanguageSelectorComponent = (function () {
    function LanguageSelectorComponent(translationService) {
        this.translationService = translationService;
        this.language = this.translationService.language;
    }
    LanguageSelectorComponent.prototype.ngOnInit = function () {
    };
    LanguageSelectorComponent.prototype.changeLanguage = function () {
        this.translationService.setLanguage(this.language);
        //Fix to regenerate language select (conflict with materializecss)
        var al = this.translationService.availableLanguages;
        this.translationService.availableLanguages = [];
        var self = this;
        setTimeout(function () {
            self.translationService.availableLanguages = al;
        }, 0);
    };
    return LanguageSelectorComponent;
}());
LanguageSelectorComponent = __decorate([
    core_1.Component({
        selector: 'sp-language-selector',
        templateUrl: 'language-selector.component.html',
        styleUrls: ['language-selector.component.css']
    })
], LanguageSelectorComponent);
exports.LanguageSelectorComponent = LanguageSelectorComponent;
