"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ng_module_1 = require("@angular/core/src/metadata/ng_module");
var language_selector_component_1 = require("./language-selector.component");
var angular2_material_module_1 = require("../../../material/angular2-material.module");
var i18n_module_1 = require("../../i18n.module");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var LanguageSelectorModule = (function () {
    function LanguageSelectorModule() {
    }
    return LanguageSelectorModule;
}());
LanguageSelectorModule = __decorate([
    ng_module_1.NgModule({
        imports: [angular2_material_module_1.Angular2MaterialModule, forms_1.FormsModule, i18n_module_1.I18NModule, common_1.CommonModule],
        declarations: [language_selector_component_1.LanguageSelectorComponent],
        exports: [language_selector_component_1.LanguageSelectorComponent],
        providers: [] // services
    })
], LanguageSelectorModule);
exports.LanguageSelectorModule = LanguageSelectorModule;
