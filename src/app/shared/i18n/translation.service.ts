import {EventEmitter, Injectable} from "@angular/core";
import {DatePipe} from "@angular/common";
import {DictionariesService} from "./dictionaries.service";
import {User} from "../../apps/_common/services/users.service";

@Injectable()
export class TranslationService {

  language:string;
  availableLanguages:any;

  constructor(private dictionariesService:DictionariesService) {
    this.availableLanguages = [
      {
        code: 'en-US',
        desc: 'ENGLISH_US',
        desc_short: 'ENGLISH_US_SHORT',
        flagClass: 'flag-us'
      },
      {
        code: 'it-IT',
        desc: 'ITALIAN',
        desc_short: 'ITALIAN_SHORT',
        flagClass: 'flag-it'
      },
      {
        code: 'sl-SI',
        desc: 'SLOVENIAN',
        desc_short: 'SLOVENIAN_SHORT',
        flagClass: 'flag-sl'
      },
      {
        code: 'hu-HU',
        desc: 'HUNGARIAN',
        desc_short: 'HUNGARIAN_SHORT',
        flagClass: 'flag-hu'
      }
    ];

    //We use the browser to detect the language to use as default
    // let browserLanguage = navigator.language;
    // for(let lang of this.availableLanguages){
    //   if(lang.code.indexOf(browserLanguage)==0){
    //     this.language = lang.code;
    //     break;
    //   }
    // }
    if(this.language==null){
      this.language = 'en-US';
    }
  }

  onLanguageChange = new EventEmitter();

  setLanguage(language:string){
    if(this.dictionariesService.DICTIONARIES[language]){
      this.language = language;
    }
    else {
      this.language = 'en-US';
    }
    this.onLanguageChange.emit();
  }

  setLanguageFromUser(u:User){
    let badge_language = u.badge_language;
    if(badge_language){
      let langs = this.availableLanguages.map((l)=>l.code.split("-")[0]);
      if(langs.indexOf(badge_language)!=-1){
        let index = langs.indexOf(badge_language);
        this.setLanguage(this.availableLanguages[index].code);
      }
      else {
        this.setLanguage("en-US")
      }

    }
    else {
      this.setLanguage("en-US")
    }
  }

  translate(value:string, params?:any, mode?:string, modeOptions?:any):string{
    if(mode){
      if(mode=='date'){
        let pattern = (modeOptions?(modeOptions['pattern']?modeOptions['pattern']:'mediumDate'):'mediumDate');
        let localeDate: DatePipe = new DatePipe(this.language);
        return localeDate.transform(value, pattern);
      }
      else {
        return value;
      }
    }
    else {
      let translatedValue = this.dictionariesService.DICTIONARIES[this.language][value]||this.dictionariesService.DICTIONARIES['en-US'][value]||value
      if(params && params.length>0){
        let vars = params[0];
        for(let key in vars){
          if(vars.hasOwnProperty(key))
            translatedValue = translatedValue.split("{{"+key+"}}").join(vars[key]);
        }
      }
      return translatedValue;
    }
  }
}
