import {Component, Input} from "@angular/core";
import {TranslationService} from "./translation.service";

@Component({
  selector: 'sp-ng-translate',
  template: '{{translate()}}'
})
export class TranslationComponent {

  @Input() value;
  @Input() params;
  @Input() mode:string;
  @Input() modeOptions:any;
  translation:string;

  constructor(protected translationService: TranslationService) {
    this.translationService.onLanguageChange.subscribe(()=>{
      this.updateTranslation()
    })
  }

  translate() {
    if(!this.translation){
      this.updateTranslation();
    }
    return this.translation;
  }

  updateTranslation(){
    this.translation = this.translationService.translate(this.value, this.params, this.mode, this.modeOptions);
  }
}
