"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var TranslationComponent = (function () {
    function TranslationComponent(translationService) {
        this.translationService = translationService;
    }
    TranslationComponent.prototype.translate = function () {
        return this.translationService.translate(this.value, this.params, this.mode, this.modeOptions);
    };
    return TranslationComponent;
}());
__decorate([
    core_1.Input()
], TranslationComponent.prototype, "value", void 0);
__decorate([
    core_1.Input()
], TranslationComponent.prototype, "params", void 0);
__decorate([
    core_1.Input()
], TranslationComponent.prototype, "mode", void 0);
__decorate([
    core_1.Input()
], TranslationComponent.prototype, "modeOptions", void 0);
TranslationComponent = __decorate([
    core_1.Component({
        selector: 'sp-ng-translate',
        template: '{{translate()}}'
    })
], TranslationComponent);
exports.TranslationComponent = TranslationComponent;
