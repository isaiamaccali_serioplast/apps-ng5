import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetRequestOptions, SerionetService} from "../serionet/service/serionet.service";
import {AppsService} from "../../apps/apps.service";
import {isNullOrUndefined} from "util";
import {parseSerionetRespToData} from "../utils";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class DictionariesService {

  DICTIONARIES = {
    'en-US': {},
    'it-IT': {},
    'sl-SI':{},
    'hu-HU':{}
  };

  APP_DICTIONARIES_LOADED = {};
  baseDictLoaded = false;

  constructor(private serionetService: SerionetService, private appsService: AppsService) {
  }

  getDictionary(app: string) {
    return Observable.create((observer) => {

      if (!isNullOrUndefined(this.APP_DICTIONARIES_LOADED[app])) {
        observer.next(true);
        observer.complete();
      }
      else {
        let urlParams = new HttpParams();
        let baseDictLoading = false;
        if (app && app != 'base') {
          let appData = this.appsService.getAppData(app);
          urlParams = urlParams.set('app', appData['pk']);
          baseDictLoading = false;
        }
        else {
          urlParams = urlParams.set('common', 2 + "");
          baseDictLoading = true;
        }

        let serionetOptions = new SerionetRequestOptions();
        serionetOptions.options = {
          params: urlParams
        };

        if(!this.baseDictLoaded && !baseDictLoading){
          //trying to load an app dict without the base one
          let urlParams_ = new HttpParams();
          urlParams_ = urlParams_.set('common', 2 + "");

          let serionetOptions_ = new SerionetRequestOptions();
          serionetOptions.options = {
            params: urlParams_
          };

          this.serionetService.doGet("core/translations", serionetOptions_).subscribe((data) => {
            let items = parseSerionetRespToData(data);
            this.baseDictLoaded = true;
            this.processDictItems(items);
            this.serionetService.doGet("core/translations", serionetOptions).subscribe((data) => {
              if (data && data['count'] && data['count'] > 0) {
                let items = parseSerionetRespToData(data);
                this.APP_DICTIONARIES_LOADED[app] = items;
                if(baseDictLoading)
                  this.baseDictLoaded = true;
                this.processDictItems(items);
                observer.next(data);
                observer.complete();
              }
              else {
                observer.next(data);
                observer.complete();
              }

            },(resp)=>{
              observer.next(resp);
              observer.complete();
            });
          },(resp)=>{
            observer.next(resp);
            observer.complete();
          });
        }
        else {
          this.serionetService.doGet("core/translations", serionetOptions).subscribe((data) => {
            if (data && data['count'] && data['count'] > 0) {
              let items = data['results'];
              this.APP_DICTIONARIES_LOADED[app] = items;
              if(baseDictLoading)
                this.baseDictLoaded = true;
              this.processDictItems(items);
              observer.next(data);
              observer.complete();
            }
            else {
              observer.next(data);
              observer.complete();
            }
          },(resp)=>{
            observer.next(resp);
            observer.complete();
          });
        }
      }
    })
  }

  processDictItems(items:any[]){
    items.forEach((item) => {
      let label = item['label'];
      this.DICTIONARIES['en-US'][label] = item['en_us'];
      this.DICTIONARIES['it-IT'][label] = item['it_it'];
      this.DICTIONARIES['sl-SI'][label] = item['sl_si'];
      this.DICTIONARIES['hu-HU'][label] = item['hu_hu'];
    });
  }
}
