import {NgModule} from "@angular/core";
import {TranslationComponent} from "./translation.component";

@NgModule({
  imports:[],
  declarations: [TranslationComponent],   // components and directives
  providers: [] ,                   // services
  exports:[TranslationComponent]
})
export class I18NModule { }
