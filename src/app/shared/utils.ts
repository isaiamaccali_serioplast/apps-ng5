export const cloneObject = (obj) => {
  if (obj === null || typeof(obj) !== 'object' || 'isActiveClone' in obj)
    return obj;

  // console.log(obj);
  let temp = new obj.constructor();

  if (obj.getTime && obj.getTime()) {
    temp.setTime(obj.getTime());
  }
  else {
    for (let key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        obj['isActiveClone'] = null;
        temp[key] = cloneObject(obj[key]);
        delete obj['isActiveClone'];
      }
    }
  }

  return temp;
};

export const moveUpElementInList = (element, list) => {
  let currentIndex = list.indexOf(element);
  list.splice(currentIndex, 1);
  list.splice(currentIndex - 1, 0, element);
  return list;
};

export const moveDownElementInList = (element, list) => {
  let currentIndex = list.indexOf(element);
  list.splice(currentIndex, 1);
  list.splice(currentIndex + 1, 0, element);
  return list;
};

export const removeElementFromList = (element, list) => {
  let currentIndex = list.indexOf(element);
  list.splice(currentIndex, 1);
};

export const isElementInList = (element, list) => {
  let currentIndex = list.indexOf(element);
  return currentIndex != -1;
};

export const isEmpty = (obj) => {
  if (obj instanceof Object)
    return Object.keys(obj).length === 0;
  else {
    return obj == null;
  }
};

export const getUrlDate = (date: Date, separator: string = "-") => {

  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let monthToPrint = formatNumber(2, month);
  let day = date.getDate();
  let dayToPrint = formatNumber(2, day);
  return [year, monthToPrint, dayToPrint].join(separator)
};

export const getUrlTime = (date: Date, separator: string = ":") => {

  let hours = formatNumber(2, date.getHours());
  let minutes = formatNumber(2, date.getMinutes());
  let seconds = formatNumber(2, date.getSeconds());
  return [hours, minutes, seconds].join(separator)
};

export const getUrlDateTime = (date: Date, separator: string = "-") => {
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let dayHours = date.getHours();
  let dayMinutes = date.getMinutes();
  return [year, formatNumber(2, month), formatNumber(2, day)].join(separator) + ["T", formatNumber(2, dayHours), ":", formatNumber(2, dayMinutes)].join("")
};

export const formatDate = (date: Date) => {
  let monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  return date.getDate() + " " + monthNames[date.getMonth()] + ", " + date.getFullYear();
};

export const monthName = (monthNumber: number) => {
  let monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  return monthNames[monthNumber];
};

export const dayName = (dayNumber: number) => {
  let dayNames = ["sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  return dayNames[dayNumber];
};

export const buildErrorMessage = (resp: any) => {
  let content = [];
  content.push("Status: " + resp.status);
  content.push("Context: " + resp.context);
  content.push("Error: " + JSON.stringify(resp.message));
  return content.join("<br/>");
};

export const fixValue = (data: any[], itemParent: any, key: string) => {
  for (let i of data) {
    if (i.pk == itemParent[key].pk) {
      itemParent[key] = cloneObject(i);
    }
  }
};

export const formatNumber = (numberDigits: number, n: number) => {
  for (let i = 1; i < numberDigits; i++) {
    if (n < Math.pow(10, i)) {
      let numberOfZeros = numberDigits - i;
      let nextNumberString = [];
      for (let l = 0; l < numberOfZeros; l++) {
        nextNumberString.push("0");
      }
      nextNumberString.push(n);
      return nextNumberString.join("");
    }
  }
  return n + "";
};

export const camelCase = (text) => {
  let splitStr = text.toLowerCase().split(' ');
  for (let i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(' ');
};

export const arrayGroupBy = (array: any[], key: string, usePK?: boolean) => {
  let result = {};
  let keys = arrayDistinct(array.map((i) => {
    if (!usePK)
      return i[key];
    else
      return i[key]['pk'];
  }));
  if (keys.length > 0) {
    for (let keyValue of keys) {
      result[keyValue] = array.filter((i) => {
        if (!usePK)
          return i[key] == keyValue;
        else
          return i[key]['pk'] == keyValue
      });
    }
  }
  return result;
};

export const arrayDistinct = (a: any[]) => {
  let onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  return a.filter(onlyUnique);
};

export const arrayDiff = (a1, a2) => {
  return a1.filter((i) => {
    return a2.indexOf(i) < 0;
  });
};

export const arrayIntersect = (a1, a2) => {
  return a1.filter((i) => {
    return a2.indexOf(i) >= 0;
  });
};

export const randomString = (length) => {
  return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
};

export const average = (...params: number[]) => {
  let list: number[] = [].slice.call(params);
  return list.reduce((acc, val) => {
    return acc + val;
  }) / list.length;
};

export const parseSerionetRespToData = (resp) => {
  let data = [];
  if (resp && resp['count'] && resp['count'] > 0) {
    data = resp['results'];
  }
  return data;
};

export const sapDateToUrlDate = (value: string) => {
  return [value.substring(0, 4), value.substring(4, 6), value.substring(6, 8)].join("-");
};

export const randomColor = () => {
  let max = 0xffffff;
  return '#' + Math.round(Math.random() * max).toString(16);
};

export const randomColorMaterial = () => {
  let defaultPalette = {
    // Red, Pink, Purple, Deep Purple, Indigo, Blue, Light Blue, Cyan, Teal, Green, Light Green, Lime, Yellow, Amber, Orange, Deep Orange, Brown, Grey, Blue Grey
    50: ['#FFEBEE', '#FCE4EC', '#F3E5F5', '#EDE7F6', '#E8EAF6', '#E3F2FD', '#E1F5FE', '#E0F7FA', '#E0F2F1', '#E8F5E9', '#F1F8E9', '#F9FBE7', '#FFFDE7', '#FFF8E1', '#FFF3E0', '#FBE9E7', '#EFEBE9', '#FAFAFA', '#ECEFF1'],
    100: ['#FFCDD2', '#F8BBD0', '#E1BEE7', '#D1C4E9', '#C5CAE9', '#BBDEFB', '#B3E5FC', '#B2EBF2', '#B2DFDB', '#C8E6C9', '#DCEDC8', '#F0F4C3', '#FFF9C4', '#FFECB3', '#FFE0B2', '#FFCCBC', '#D7CCC8', '#F5F5F5', '#CFD8DC'],
    200: ['#EF9A9A', '#F48FB1', '#CE93D8', '#B39DDB', '#9FA8DA', '#90CAF9', '#81D4FA', '#80DEEA', '#80CBC4', '#A5D6A7', '#C5E1A5', '#E6EE9C', '#FFF59D', '#FFE082', '#FFCC80', '#FFAB91', '#BCAAA4', '#EEEEEE', '#B0BEC5'],
    300: ['#E57373', '#F06292', '#BA68C8', '#9575CD', '#7986CB', '#64B5F6', '#4FC3F7', '#4DD0E1', '#4DB6AC', '#81C784', '#AED581', '#DCE775', '#FFF176', '#FFD54F', '#FFB74D', '#FF8A65', '#A1887F', '#E0E0E0', '#90A4AE'],
    400: ['#EF5350', '#EC407A', '#AB47BC', '#7E57C2', '#5C6BC0', '#42A5F5', '#29B6F6', '#26C6DA', '#26A69A', '#66BB6A', '#9CCC65', '#D4E157', '#FFEE58', '#FFCA28', '#FFA726', '#FF7043', '#8D6E63', '#BDBDBD', '#78909C'],
    500: ['#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#03A9F4', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B'],
    600: ['#E53935', '#D81B60', '#8E24AA', '#5E35B1', '#3949AB', '#1E88E5', '#039BE5', '#00ACC1', '#00897B', '#43A047', '#7CB342', '#C0CA33', '#FDD835', '#FFB300', '#FB8C00', '#F4511E', '#6D4C41', '#757575', '#546E7A'],
    700: ['#D32F2F', '#C2185B', '#7B1FA2', '#512DA8', '#303F9F', '#1976D2', '#0288D1', '#0097A7', '#00796B', '#388E3C', '#689F38', '#AFB42B', '#FBC02D', '#FFA000', '#F57C00', '#E64A19', '#5D4037', '#616161', '#455A64'],
    800: ['#C62828', '#AD1457', '#6A1B9A', '#4527A0', '#283593', '#1565C0', '#0277BD', '#00838F', '#00695C', '#2E7D32', '#558B2F', '#9E9D24', '#F9A825', '#FF8F00', '#EF6C00', '#D84315', '#4E342E', '#424242', '#37474F'],
    900: ['#B71C1C', '#880E4F', '#4A148C', '#311B92', '#1A237E', '#0D47A1', '#01579B', '#006064', '#004D40', '#1B5E20', '#33691E', '#827717', '#F57F17', '#FF6F00', '#E65100', '#BF360C', '#3E2723', '#212121', '#263238'],
    A100: ['#FF8A80', '#FF80AB', '#EA80FC', '#B388FF', '#8C9EFF', '#82B1FF', '#80D8FF', '#84FFFF', '#A7FFEB', '#B9F6CA', '#CCFF90', '#F4FF81', '#FFFF8D', '#FFE57F', '#FFD180', '#FF9E80'],
    A200: ['#FF5252', '#FF4081', '#E040FB', '#7C4DFF', '#536DFE', '#448AFF', '#40C4FF', '#18FFFF', '#64FFDA', '#69F0AE', '#B2FF59', '#EEFF41', '#FFFF00', '#FFD740', '#FFAB40', '#FF6E40'],
    A400: ['#FF1744', '#F50057', '#D500F9', '#651FFF', '#3D5AFE', '#2979FF', '#00B0FF', '#00E5FF', '#1DE9B6', '#00E676', '#76FF03', '#C6FF00', '#FFEA00', '#FFC400', '#FF9100', '#FF3D00'],
    A700: ['#D50000', '#C51162', '#AA00FF', '#6200EA', '#304FFE', '#2962FF', '#0091EA', '#00B8D4', '#00BFA5', '#00C853', '#64DD17', '#AEEA00', '#FFD600', '#FFAB00', '#FF6D00', '#DD2C00']
  };

  let shadesNumber = Object.keys(defaultPalette).length;
  let key = Object.keys(defaultPalette)[Math.floor(Math.random() * (shadesNumber))];
  let shade = defaultPalette[key];
  let colorsNumber = shade.length;
  let colorKey = Math.floor(Math.random() * (colorsNumber));
  return shade[colorKey];
};

export const arrayAreDifferent = (a1, a2) => {
  return arrayDiff(a1, a2).length != 0 || arrayDiff(a2, a1).length != 0;
};

export const truncateWithEllipsis = (value: string, args: string[]) => {
  let limit = args.length > 0 ? parseInt(args[0], 10) : 10;
  let trail = args.length > 1 ? args[1] : '...';
  return value.length > limit ? value.substring(0, limit) + trail : value;
};

export const calcPercentage = (amount: number, target: number, digits = 0) => {
  let ratio = amount / target;
  let perc = ratio * 100;
  return Math.floor(perc * Math.pow(10, digits)) / Math.pow(10, digits) + "%";
};

export const sortArrayAlphabetically = (array: any[], key1?: string, key2?: string) => {
  array.sort((a, b) => {
    let textA = (key1 ? key2 ? a[key1][key2] : a[key1] : a).toUpperCase();
    let textB = (key1 ? key2 ? b[key1][key2] : b[key1] : b).toUpperCase();
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  });
};

export const aggregate = (parent: any, data: any, keys: string[], index: number) => {
  if (keys[index]) {
    data = arrayGroupBy(data, keys[index], false);

    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        data[key] = aggregate(data[key], data[key], keys, index + 1);
      }
    }
    parent = data;
  }
  return parent;
};

export const sumByKey = (list, key) => {
  let total = 0;
  list.forEach((i) => {
    total += i[key];
  });
  return total;
};

export const makeDates = (start: string, end: string) => {
  let startDate = new Date(Date.parse(start));
  let endDate = new Date(Date.parse(end));
  let timeDiff = endDate.getTime() - startDate.getTime();
  if (timeDiff == 0) {
    return [startDate];
  }
  else {
    let diffDays = (timeDiff / (24 * 60 * 60 * 1000)) + 1;
    let dates = [];
    for (let i = 0; i < diffDays; i++) {
      let d = new Date(startDate.getTime());
      d.setDate(d.getDate() + i);
      dates.push(d);
    }
    return dates;
  }
};

// a and b are javascript Date objects
export const dateDiffInDays = (a, b) => {
  // Discard the time and time-zone information.
  let utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  let utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  let _MS_PER_DAY = 1000 * 60 * 60 * 24;
  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
};

export const splitLongStringInGroups = (s: string, nCharsMax: number) => {
  let words = s.split(" ").reverse();
  let groups = [];
  let count = 0;
  let group = [];
  while (words.length > 0) {
    let word = words.pop();
    if ((count + word.length + (group.length - 1)) < nCharsMax) {
      group.push(word);
      count += word.length;
    }
    else {
      words.push(word);
      groups.push(cloneObject(group));
      group = [];
      count = 0;
    }
  }
  if (group.length > 0)
    groups.push(group);

  return groups.map((g) => g.join(" "));
};

export const areDateAligned = (start_date: string, end_date: string) => {
  let d1 = new Date(start_date);
  let d2 = new Date(end_date);
  return d2.getTime() - d1.getTime() >= 0;
};
