import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {DialogConfirmComponent} from "./dialog-confirm.component";
import {DialogModule} from "../../material/dialog/dialog.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule,DialogModule],       // module dependencies
  declarations: [DialogConfirmComponent],   // components and directives
  exports:[DialogConfirmComponent],
  providers: []                    // services
})
export class DialogConfirmModule { }

