import {Component, ViewChild, Input} from "@angular/core";
import {DialogComponent} from "../../material/dialog/dialog.component";

@Component({
  selector: 'sp-dialog-confirm',
  templateUrl: 'dialog-confirm.component.html',
  styleUrls:['dialog-confirm.component.css']
})
export class DialogConfirmComponent {

  @Input() title:string;
  @Input() headerColor:string = "#4CAF50";
  @Input() denyFunction:any;
  @Input() confirmFunction:any;
  @ViewChild('dialog') dialog: DialogComponent;

  constructor(){
  }

  open(){
    this.dialog.open();
  }

  close(){
    this.dialog.close();
  }

  deny() {
    if (this.denyFunction) {
      this.denyFunction.call();
      this.close();
    }
  }

  confirm(){
    if(this.confirmFunction){
      this.confirmFunction.call();
      this.close();
    }

  }


}
