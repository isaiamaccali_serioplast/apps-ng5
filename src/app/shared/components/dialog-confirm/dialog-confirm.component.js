"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var DialogConfirmComponent = (function () {
    function DialogConfirmComponent() {
        this.headerColor = "#4CAF50";
    }
    DialogConfirmComponent.prototype.open = function () {
        this.dialog.open();
    };
    DialogConfirmComponent.prototype.close = function () {
        this.dialog.close();
    };
    DialogConfirmComponent.prototype.deny = function () {
        if (this.denyFunction) {
            this.denyFunction.call();
            this.close();
        }
    };
    DialogConfirmComponent.prototype.confirm = function () {
        if (this.confirmFunction) {
            this.confirmFunction.call();
            this.close();
        }
    };
    return DialogConfirmComponent;
}());
__decorate([
    core_1.Input()
], DialogConfirmComponent.prototype, "title", void 0);
__decorate([
    core_1.Input()
], DialogConfirmComponent.prototype, "headerColor", void 0);
__decorate([
    core_1.Input()
], DialogConfirmComponent.prototype, "denyFunction", void 0);
__decorate([
    core_1.Input()
], DialogConfirmComponent.prototype, "confirmFunction", void 0);
__decorate([
    core_1.ViewChild('dialog')
], DialogConfirmComponent.prototype, "dialog", void 0);
DialogConfirmComponent = __decorate([
    core_1.Component({
        selector: 'sp-dialog-confirm',
        templateUrl: 'dialog-confirm.component.html',
        styleUrls: ['dialog-confirm.component.css']
    })
], DialogConfirmComponent);
exports.DialogConfirmComponent = DialogConfirmComponent;
