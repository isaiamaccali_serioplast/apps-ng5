import {
  AfterViewChecked,
  Component,
  ContentChildren,
  ElementRef,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  ViewRef
} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'complex-tooltip',
  templateUrl:'complex-tooltip.component.html',
  styleUrls:['complex-tooltip.component.css']
})
export class ComplexTooltipComponent implements AfterViewChecked{

  @ContentChildren(TemplateRef) templatesInput:QueryList<TemplateRef<any>>;
  @ViewChild('container', {read: ViewContainerRef}) vc: ViewContainerRef;
  show_ = false;
  hidden = true;
  templateInput:TemplateRef<any>;
  top = 0;
  left = 0;
  marginTop = "0";
  marginLeft = "0";
  view: ViewRef;
  rect:any;
  direction:string;

  constructor(public viewContainerRef: ViewContainerRef, private domSanitizer:DomSanitizer, public elementRef:ElementRef) {}

  ngAfterViewInit(){
    this.templateInput = this.templatesInput.first;
  }

  timeout = null;

  ngAfterViewChecked(){
    if(this.show_){
      if(this.timeout){
        clearTimeout(this.timeout);
      }
      this.hidden = true;
      this.timeout = setTimeout(()=>{

        this.setTooltipPosition(this.rect, this.direction);
        this.hidden = false;
      },100)
    }
  }

  show(item, rect, direction){
    this.setItem(item,rect,direction);
    this.show_ = true;
  }

  hide(){
    this.show_ = false;
    this.viewContainerRef.detach(this.vc.indexOf(this.view));
    this.vc.remove(this.vc.indexOf(this.view));
  }

  getPosition(){
    return this.domSanitizer.bypassSecurityTrustStyle("top:"+this.top+"px;left:"+this.left+"px;margin-top:"+this.marginTop+"px;margin-left:"+this.marginLeft);
  }

  setItem(item:any, rect, direction){
    this.view = this.viewContainerRef.createEmbeddedView(this.templateInput,{item: item});
    this.rect = rect;
    this.direction = direction;
    this.vc.insert(this.view);
  }

  setTooltipPosition(rect, direction){
    switch(direction){
      case 'bottom':
      default:
        this.marginLeft = "-"+(this.elementRef.nativeElement.querySelector(".complex-tooltip-container").getBoundingClientRect().width/2)+"px";
        this.marginTop = "0";
        this.top = rect.y + rect.height+10;
        this.left = rect.x+ rect.width/2;
        break;
    }
  }
}
