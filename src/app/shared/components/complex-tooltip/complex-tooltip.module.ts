import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ComplexTooltipDirective} from "./complex-tooltip.directive";
import {ComplexTooltipComponent} from "./complex-tooltip.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, FormsModule],       // module dependencies
  declarations: [ComplexTooltipDirective,ComplexTooltipComponent],   // components and directives
  exports:[ComplexTooltipDirective,ComplexTooltipComponent],
  providers: []                    // services
})
export class ComplexTooltipModule { }


