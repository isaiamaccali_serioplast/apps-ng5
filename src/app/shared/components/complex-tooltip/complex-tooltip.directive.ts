import {Directive, ElementRef, HostListener, Input, ViewRef} from "@angular/core";
import {ComplexTooltipComponent} from "./complex-tooltip.component";

@Directive({
  selector: '[complexTooltip]'
})
export class ComplexTooltipDirective {

  @Input('template') tooltip: ComplexTooltipComponent;
  @Input('direction') direction: 'bottom';
  @Input('item') item: any;
  view: ViewRef;

  constructor(public elementRef: ElementRef) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.tooltip.show(this.item, this.elementRef.nativeElement.getBoundingClientRect(), this.direction);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.tooltip.hide();
  }


}
