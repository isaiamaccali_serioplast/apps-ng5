import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SAPIconComponent} from "./sap-icon.component";

@NgModule({
  imports: [CommonModule,FormsModule],       // module dependencies
  declarations: [SAPIconComponent],   // components and directives
  exports:[SAPIconComponent],
  providers: []                    // services
})
export class SAPIconModule { }

