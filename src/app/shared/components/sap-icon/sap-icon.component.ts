import {AfterViewInit, Component, Input} from "@angular/core";
import {SAPService} from "../../sap/sap.service";
import {randomString} from "../../utils";

declare let sap: any;

@Component({
  selector: 'sap-icon',
  templateUrl: 'sap-icon.component.html',
  styleUrls: ['sap-icon.component.css']
})
export class SAPIconComponent implements AfterViewInit{

  @Input() iconName:string;
  @Input() iconClass:string = "";
  id: string;

  constructor(private sapService: SAPService) {
    this.id = (new Date()).getTime() + "_sap_icon" + randomString(5);
  }

  ngAfterViewInit() {
    this.sapService.ready().subscribe(() => {
      let i = new sap.ui.core.Icon({
        src: "sap-icon://" + this.iconName
      });
      if (document.getElementById(this.id)){
        document.getElementById(this.id).innerHTML = "";
      }
      setTimeout(()=>{
        i.placeAt(this.id);
      },500)

    });
  }
}
