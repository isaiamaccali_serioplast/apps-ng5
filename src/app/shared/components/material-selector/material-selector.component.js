"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var utils_1 = require("../../utils");
var MaterialSelectorComponent = (function () {
    function MaterialSelectorComponent(materialTypeService, materialsService, translationService) {
        this.materialTypeService = materialTypeService;
        this.materialsService = materialsService;
        this.translationService = translationService;
        this.materialTypes = [];
        this.materials = [];
        this.additionalQuery = {};
        this.additionalQueryTypes = {};
        this.label = 'Material';
        this.hideType = false;
        this.selectedMaterial = new core_1.EventEmitter();
    }
    MaterialSelectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        var query = { is_active: true };
        query = Object.assign(query, this.additionalQueryTypes);
        this.materialTypeService.getMaterialTypes(query).subscribe(function (data) {
            if (data && data['count'] && data['count'] > 0) {
                _this.materialTypes = data['results'];
                if (!utils_1.isEmpty(_this.additionalQueryTypes)) {
                    _this.materialType = _this.materialTypes[0].pk;
                }
            }
        });
    };
    MaterialSelectorComponent.prototype.ngOnChanges = function (changes) {
        if (changes['additionalQuery'])
            this.additionalQuery = changes['additionalQuery'].currentValue;
        if (changes['additionalQueryTypes'])
            this.additionalQueryTypes = changes['additionalQueryTypes'].currentValue;
        if (changes['initialMaterial']) {
            this.initialMaterial = changes['initialMaterial'].currentValue;
            if (this.initialMaterial && this.initialMaterial.material_type)
                this.materialType = this.initialMaterial.material_type.pk;
        }
    };
    MaterialSelectorComponent.prototype.searchMaterials = function (keyword) {
        var _this = this;
        var query = {
            search: keyword,
            results: 5,
            material_type: this.materialType
        };
        query = Object.assign(query, this.additionalQuery);
        this.materialsService.getMaterials(query).subscribe(function (data) {
            if (data['count'] && data['count'] > 0)
                _this.materials = data['results'];
            else
                _this.materials = [];
        });
    };
    MaterialSelectorComponent.prototype.clickedMaterial = function (m) {
        if (m && m.pk) {
            this.materialType = m.material_type.pk;
            this.selectedMaterial.emit(m);
        }
        else {
            this.selectedMaterial.emit(null);
        }
    };
    MaterialSelectorComponent.prototype.handleTypeChange = function () {
        this.materials = [];
        this.selectedMaterial.emit(null);
    };
    MaterialSelectorComponent.prototype.tr = function (value) {
        return this.translationService.translate(value);
    };
    MaterialSelectorComponent.prototype.getMaterialDesc = function (m) {
        return m.code + " - " + m.description;
    };
    return MaterialSelectorComponent;
}());
__decorate([
    core_1.Input('additionalQuery')
], MaterialSelectorComponent.prototype, "additionalQuery", void 0);
__decorate([
    core_1.Input('additionalQueryTypes')
], MaterialSelectorComponent.prototype, "additionalQueryTypes", void 0);
__decorate([
    core_1.Input('initialValue')
], MaterialSelectorComponent.prototype, "initialMaterial", void 0);
__decorate([
    core_1.Input('label')
], MaterialSelectorComponent.prototype, "label", void 0);
__decorate([
    core_1.Input('hideType')
], MaterialSelectorComponent.prototype, "hideType", void 0);
__decorate([
    core_1.Output()
], MaterialSelectorComponent.prototype, "selectedMaterial", void 0);
MaterialSelectorComponent = __decorate([
    core_1.Component({
        selector: 'material-selector',
        templateUrl: 'src/app/shared/components/material-selector/material-selector.component.html',
        styleUrls: ['src/app/shared/components/material-selector/material-selector.component.css']
    })
], MaterialSelectorComponent);
exports.MaterialSelectorComponent = MaterialSelectorComponent;
