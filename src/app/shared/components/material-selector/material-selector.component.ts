import {Component, Input, Output, EventEmitter, SimpleChange} from "@angular/core";
import {MaterialType, MaterialTypesService} from "../../../apps/_common/services/material-type.service";
import {Material, MaterialsService} from "../../../apps/_common/services/materials.service";
import {TranslationService} from "../../i18n/translation.service";
import {isEmpty, parseSerionetRespToData} from "../../utils";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'material-selector',
  templateUrl: 'material-selector.component.html',
  styleUrls: ['material-selector.component.css']
})
export class MaterialSelectorComponent extends TranslatableComponent {

  materialTypes: MaterialType[] = [];
  materialType: number;
  materials: Material[] = [];
  @Input('additionalQuery') additionalQuery: any = {};
  @Input('additionalQueryTypes') additionalQueryTypes: any = {};
  @Input('initialValue') initialMaterial: Material;
  @Input('label') label = 'Material';
  @Input('hideType') hideType = false;
  @Output() selectedMaterial = new EventEmitter();

  constructor(private materialTypeService: MaterialTypesService, private materialsService: MaterialsService,
              protected translationService: TranslationService) {
    super(translationService);
  }


  ngOnInit() {
    let query = {is_active: true, ordering:'description'};
    query = Object.assign(query, this.additionalQueryTypes);
    this.materialTypeService.getMaterialTypes(query).subscribe((data) => {
      this.materialTypes = parseSerionetRespToData(data);
      if(!isEmpty(this.additionalQueryTypes)){
        this.materialType = this.materialTypes[0].pk
      }
    })
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if (changes['additionalQuery'])
      this.additionalQuery = changes['additionalQuery'].currentValue;
    if (changes['additionalQueryTypes']){
      this.additionalQueryTypes = changes['additionalQueryTypes'].currentValue;
      this.ngOnInit();
    }
    if (changes['initialMaterial']){
      this.initialMaterial = changes['initialMaterial'].currentValue;
      if(this.initialMaterial && this.initialMaterial.material_type)
        this.materialType = this.initialMaterial.material_type.pk;
    }
  }

  searchMaterials(keyword: string) {
    let query = {
      search: keyword,
      results: 5,
      is_active:true
    };

    if(this.materialType)
      query['material_type'] = this.materialType;

    query = Object.assign(query, this.additionalQuery);
    return this.materialsService.getMaterials(query)
  }

  clickedMaterial(m: Material) {
    if(m && m.pk){
      this.materialType = m.material_type.pk;
      this.selectedMaterial.emit(m);
    }
    else {
      this.selectedMaterial.emit(null);
    }


  }

  handleTypeChange() {
    this.materials = [];
    this.selectedMaterial.emit(null);
  }

  getMaterialDesc(m: Material) {
    return m.code + " - " + m.description;
  }
}
