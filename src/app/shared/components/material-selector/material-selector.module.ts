import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {MaterialSelectorComponent} from "./material-selector.component";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {SPAutocompleteModule} from "../../material/autocomplete/autocomplete.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule,SerioplastCommonModule, SPAutocompleteModule],       // module dependencies
  declarations: [MaterialSelectorComponent],   // components and directives
  exports:[MaterialSelectorComponent],
  providers: []                    // services
})
export class MaterialSelectorModule { }

