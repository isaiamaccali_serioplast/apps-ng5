import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {ControlPlan} from "../../../apps/_common/services/control-plans.service";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {WorkorderSerionet, WorkordersSerionetService} from "../../../apps/_common/services/workorders-serionet.service";
import {Routing} from "../../../apps/_common/services/routings.service";
import {RoutingProductionVersion} from "../../../apps/_common/services/routing-production-version-service";
import {PackingInstruction} from "../../../apps/_common/services/packing-instructions.service";
import {TypeLink} from "../../../apps/_common/services/type-link.service";
import {BOM} from "../../../apps/_common/services/bom.service";
import {Site} from "../../../apps/_common/services/sites.service";
import {LoadingService} from "../../loading/loading.service";
import {ContentTypeService} from "../../../apps/_common/services/content-type.service";
import {MessageService} from "../../material/message/message.service";
import {TranslationService} from "../../i18n/translation.service";
import {DomSanitizer} from "@angular/platform-browser";
import {buildErrorMessage, dateDiffInDays, fixValue, getUrlDate, parseSerionetRespToData, sapDateToUrlDate} from "../../utils";
import {StatusLink, StatusLinkService} from "../../../apps/_common/services/status-link.service";
import {HasCompanyPosition, HasCompanyPositionService} from "../../../apps/_common/services/has-company-position.service";
import {LoginService} from "../../login/service/login.service";
import {WorkorderLabelSAP, WorkorderLabelSapService} from "../../../apps/_common/services/workorder-label-sap.service";
import {BaseComponent} from "../base-component/base.component";

export class ControlPlanStructure {
  cp?: ControlPlan;
  active?: boolean;
}

@Component({
  selector: 'workorder-view-shared',
  templateUrl: 'workorder-view-shared.component.html',
  styleUrls: ['workorder-view-shared.component.css']
})
export class WorkorderViewSharedComponent extends BaseComponent {

  @Input('workorder') workorder: WorkorderSerionet;
  @Input('allowActions') allowActions = true;
  @Input('backFunction') backFunction: any;
  @Output('edit') editFunction = new EventEmitter();
  @ViewChild('pvShowDialog') pvShowDialog: DialogComponent;
  @ViewChild('routingShowDialog') routingShowDialog: DialogComponent;
  @ViewChild('bomShowDialog') bomShowDialog: DialogComponent;
  @ViewChild('labelSelectionDialog') labelSelectionDialog: DialogComponent;

  routings: Routing[] = [];
  productionVersions: RoutingProductionVersion[] = [];
  packingInstructions: PackingInstruction[] = [];
  types: TypeLink[] = [];
  controlPlanStructures: ControlPlanStructure[] = [];
  boms: BOM[] = [];
  siteFilter: Site;
  workorderCT: number;
  statuses: StatusLink[] = [];

  ready = false;
  hasCompanyPositions: HasCompanyPosition[] = [];
  availableLabels: WorkorderLabelSAP[] = [];

  constructor(protected loadingService: LoadingService, private workorderSerionetService: WorkordersSerionetService,
              private contentTypeService: ContentTypeService, protected messageService: MessageService,
              protected translationService: TranslationService, private sanitizer: DomSanitizer,
              private hasCompanyPositionService: HasCompanyPositionService, private loginService: LoginService,
              private workorderLabelSapService: WorkorderLabelSapService, private statusLinkService: StatusLinkService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {

    let count = 0;
    let cb = () => {
      count--;
      if (count == 0) {
        this.contentTypeService.getContentTypes({model: 'workorder'}).subscribe((resp) => {
          let ct = parseSerionetRespToData(resp)[0];
          this.workorderCT = ct.pk;
          this.ready = true;
        })
      }
    };

    this.hasCompanyPositionService.getHasCompanyPositions({
      currents_for_user: this.loginService.user.pk,
      is_active: true
    }).subscribe((response) => {
      this.hasCompanyPositions = parseSerionetRespToData(response);
      cb();
    });
    count++;

    this.statusLinkService.getStatusLinks({app__model: 'workorder', is_active: true}).subscribe((resp) => {
      this.statuses = parseSerionetRespToData(resp);
      cb();
    });
    count++;

  }

  canDeleteWorkorder() {
    //Condizione di edit/delete: dev'essere creato e lo stato in Draft o Created.
    return this.workorder.pk && ([0, 1].indexOf(this.workorder.status.delta_rel) != -1) && this.canEdit();
  }

  deleteWorkorder() {
    this.messageService.open({
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE'),
      autoClose: false,
      confirm: () => {
        this.workorderSerionetService.deleteWorkorderSerionet(this.workorder).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.back();
          })
        })
      },
      deny: () => {
      }
    });
  }

  setPV() {
    fixValue(this.routings, this.workorder, 'routing');
    if (this.workorder.routing.production_version && this.workorder.routing.production_version.pk) {
      this.workorder.production_version.pk = this.workorder.routing.production_version.pk;
      fixValue(this.productionVersions, this.workorder, 'production_version');
    }

  }

  setRouting() {
    fixValue(this.productionVersions, this.workorder, 'production_version');
    this.workorder.routing = this.workorder.production_version.routing_header;
    fixValue(this.routings, this.workorder, 'routing');
  }

  showSelectedPV() {
    this.pvShowDialog.open();
  }

  showBOM() {
    this.bomShowDialog.open();
  }

  showSelectedRouting() {
    this.routingShowDialog.open();
  }

  back() {
    this.backFunction();
  }

  getWorkorderStyle() {
    if (this.workorder.status && this.workorder.status.status_id && this.workorder.status.status_id.color)
      return this.sanitizer.bypassSecurityTrustStyle('background-color: ' + this.workorder.status.status_id.color + '; padding:0 20px');
  }

  handlePackingInstructionChange() {
    this.workorder.bulk_packing_instruction = !this.workorder.packing_instruction.pk;
  }

  editWorkorder() {
    this.editFunction.emit();
  }

  canAdvanceStatus() {
    //E' possibile muoversi solo da draft a created e da completed a close completed, il resto è gestito dal MES o da SAP
    return [0, 1, 5].indexOf(this.workorder.status.delta_rel) != -1 && this.canEdit()
  }

  advanceStatus() {
    this.messageService.open({
      title: 'Confirm',
      message: this.buildAdvanceMessage(),
      confirm: () => {
        if (this.workorder.status.delta_rel == 0) {
          if (this.isOkByType(this.workorder)) {
            if (this.workorder.packing_instruction && this.workorder.packing_instruction.pk > -1)
              this.handleLabelsAndAdvance();
            else {
              this.makeWoReleased();
            }
          }
          else {
            this.messageService.open({
              title: 'Error',
              error: true,
              message: "Missing mandatory data to advance",
              autoClose: false
            });
          }

        }
        else {
          this.loadingService.show();
          this.workorderSerionetService.saveWorkorderSerionet(this.workorder, {destination_sap_status: this.workorder.status.delta_rel + 1}).subscribe((resp) => {
            this.loadingService.hide();
            this.manageRespSave(resp, () => {
              this.reload();
            })
          })
        }

      },
      deny: () => {

      }
    });
  }

  handleLabelsAndAdvance() {
    this.loadingService.show();
    this.workorderLabelSapService.getLabels({Plant: this.workorder.site.code}).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.status == 200) {
        this.availableLabels = resp.response.results;
        if (this.workorder.bulk_packing_instruction || !this.workorder.packing_instruction || !this.workorder.packing_instruction.pk) {
          this.availableLabels = this.availableLabels.filter((l) => l.Name == 'ZMES_BATCH_LABEL')
        }
        this.labelSelectionDialog.open();
      }
      else {
        resp.context = "SAP";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  makeWoReleased() {
    if (this.labelSelectionDialog.opened) {
      this.labelSelectionDialog.close();
      this.workorder.labels = this.availableLabels.filter((l) => l.Preferred);
      this.workorder.labels.forEach((l) => {
        if (!l.Plant)
          l.Plant = " ";
        if (!l.WorkOrder)
          l.WorkOrder = " ";
      });
    }

    this.loadingService.show();
    this.workorderSerionetService.saveWorkorderSerionet(this.workorder, {destination_sap_status: this.workorder.status.delta_rel + 1}).subscribe((resp) => {
      this.loadingService.hide();
      this.manageRespSave(resp, () => {
        this.reload();
      })
    })
  }

  reload() {
    this.workorderSerionetService.getWorkorderSerionet(this.workorder.pk).subscribe((resp) => {
      this.workorder = resp;
      this.ready = false;
      this.ngOnInit();
    })
  }

  buildAdvanceMessage() {
    let newStatus = this.statuses.find((sl) => sl.delta_rel == (this.workorder.status.delta_rel + 1));
    return "Are you sure you want to change the status of this workorder to <b>" + newStatus.__str__ + "</b>?";
  }

  static DEVELOPERS = [
    1, //Serioplast Software Developer
    10, //Software Development Manager
    7, //ERP Manager
    8 //ERP Support
  ];

  /*  CREATE, CHANGE STATUS e UPDATE:
  - R&D Logistics manager e logistic assistant (per gli ordini di tipo Sampling)
  - Production managers and assistants, logistic managers and assistant (per gli ordini di tipo Production)*/

  static SAMPLING_EDITORS = [
    48, //R&D Logistics manager
    162, //R&D Logistics assistant
  ];

  static PRODUCTION_EDITORS = [
    65, //Production managers
    66, //Production assistants
    72, //Logistic manager
    73  //Logistic assistant
  ];

  canEdit() {
    if (this.workorder.type.delta_rel == 0) {
      return this.isAuthorizedEditSampling();
    }
    else {
      return this.isAuthorizedEditProduction();
    }
  }

  isAuthorizedEditSampling() {
    let userCompanyPositionNames = this.hasCompanyPositions.map((item: HasCompanyPosition) => {
      return item.get_company_position_obj.name.pk;
    });
    return WorkorderViewSharedComponent.DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 ||
      userCompanyPositionNames.filter((item) => WorkorderViewSharedComponent.SAMPLING_EDITORS.indexOf(item) !== -1).length > 0;
  }

  isAuthorizedEditProduction() {
    let userCompanyPositionNames = this.hasCompanyPositions.map((item: HasCompanyPosition) => {
      return item.get_company_position_obj.name.pk;
    });
    return WorkorderViewSharedComponent.DEVELOPERS.filter((item) => userCompanyPositionNames.indexOf(item) !== -1).length > 0 ||
      userCompanyPositionNames.filter((item) => WorkorderViewSharedComponent.PRODUCTION_EDITORS.indexOf(item) !== -1).length > 0;
  }

  calcActualEndDate() {
    let d = new Date(sapDateToUrlDate(this.workorder.sap_actual_start));
    d.setDate(d.getDate() + dateDiffInDays(new Date(this.workorder.planned_start), new Date(this.workorder.planned_end)));
    return getUrlDate(d);
  }

  isOkByType(wo: WorkorderSerionet) {
    if (wo.type.delta_rel == 0) {
      //Sampling
      return wo.control_plans.length > 0 && wo.wbs && wo.wbs.pk
    }
    else if (wo.type.delta_rel == 1) {
      //Production
      return true;
    }
  };
}


