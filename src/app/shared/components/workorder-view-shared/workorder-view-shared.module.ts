import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {I18NModule} from "../../i18n/i18n.module";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {BomDisplayModule} from "../bom-display/bom-display.module";
import {RoutingDisplayModule} from "../routing-display/routing-display.module";
import {WorkorderViewSharedComponent} from "./workorder-view-shared.component";
import {UsersModule} from "../user/user.module";
import {GenericCommentsModule} from "../generic-comments/generic-comments.module";
import {AttachmentsModule} from "../attachments/attachments.module";
import {DialogModule} from "../../material/dialog/dialog.module";
import {SAPIconModule} from "../sap-icon/sap-icon.module";
import {FormsModule} from "@angular/forms";
import {ExpThousandsSeparatorModule} from "../../pipes/thousands-separator.module";
import {SapDateModule} from "../../pipes/sap-date.module";
import {FixedFieldModule} from "../fixed-field/fixed-field.module";

@NgModule({
  imports: [CommonModule, FormsModule, SerioplastCommonModule, I18NModule, Angular2MaterialModule, BomDisplayModule,
    RoutingDisplayModule,UsersModule, GenericCommentsModule, AttachmentsModule, DialogModule, SAPIconModule,ExpThousandsSeparatorModule,SapDateModule, FixedFieldModule],       // module dependencies
  declarations: [WorkorderViewSharedComponent],   // components and directives
  exports:[WorkorderViewSharedComponent],
  providers: []                    // services
})
export class WorkorderViewSharedModule { }


