import {Component, Input} from "@angular/core";
import {BOM} from "../../../apps/_common/services/bom.service";

@Component({
  selector: 'bom-display',
  templateUrl: 'bom-display.component.html',
  styleUrls: ['bom-display.component.css']
})
export class BomDisplayComponent{

  @Input('bom') bom:BOM;

  constructor() {}

  ngOnInit() {

  }

}


