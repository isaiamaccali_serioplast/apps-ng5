import {NgModule} from "@angular/core";
import {BomDisplayComponent} from "./bom-display.component";
import {I18NModule} from "../../i18n/i18n.module";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";

@NgModule({
  imports: [I18NModule,CommonModule, Angular2MaterialModule],       // module dependencies
  declarations: [BomDisplayComponent],   // components and directives
  exports:[BomDisplayComponent],
  providers: []                    // services
})
export class BomDisplayModule { }


