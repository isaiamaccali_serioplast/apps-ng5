import {TranslationService} from "../../i18n/translation.service";
import {MessageService} from "../../material/message/message.service";
import {LoadingService} from "../../loading/loading.service";
import {buildErrorMessage} from "../../utils";

export class TranslatableComponent{

  constructor(protected translationService:TranslationService) {}

  ngOnInit() {

  }

  tr(value:string, params?:any){
    return this.translationService.translate(value, params, null, null);
  }

}


