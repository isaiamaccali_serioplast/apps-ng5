import {TranslationService} from "../../i18n/translation.service";
import {MessageService} from "../../material/message/message.service";
import {LoadingService} from "../../loading/loading.service";
import {buildErrorMessage} from "../../utils";
import {TranslatableComponent} from "./translatable.component";

export class BaseComponent extends TranslatableComponent {

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService) {
    super(translationService);
  }

  ngOnInit() {

  }

  manageRespDelete(resp, callback, skipMessage?: boolean,) {
    if (resp.status == 204) {
      if (!skipMessage) {
        this.messageService.open({
          title: " ",
          message: this.tr('DELETE_COMPLETED'),
          onClose: callback
        });
      }
      else {
        if (callback)
          callback();
      }
    }
    else {
      this.loadingService.forceHide();
      if (!resp.context)
        resp.context = "SERIONET";
      this.messageService.open({
        title: 'Error',
        error: true,
        message: buildErrorMessage(resp),
        autoClose: false
      });
    }
  }

  manageRespSave(resp, successCallback, skipMessage?: boolean, errorCallback?: any) {
    if (resp.pk) {
      if (!skipMessage) {
        this.messageService.open({
          message: this.tr('SAVE_COMPLETED'),
          title: " ",
          autoClose: false,
          onClose: successCallback
        });
      }
      else {
        if (successCallback)
          successCallback();
      }
    }
    else {
      this.loadingService.hide();
      if (!resp.context)
        resp.context = "SERIONET";
      this.messageService.open({
        title: 'Error',
        error: true,
        message: buildErrorMessage(resp),
        autoClose: false
      });
      if (errorCallback) {
        errorCallback.call(null);
      }
    }
  }

  parseFloat(n) {
    return parseFloat(n);
  }

  parseInt(n) {
    return parseInt(n, 10);
  }

}


