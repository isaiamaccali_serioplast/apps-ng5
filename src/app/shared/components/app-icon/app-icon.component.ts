import {Component, Input} from "@angular/core";
import {SAPService} from "../../sap/sap.service";
import {randomString} from "../../utils";
import {App} from "../../../apps/apps.service";

declare let sap: any;

@Component({
  selector: 'app-icon',
  templateUrl: 'app-icon.component.html',
  styleUrls: ['app-icon.component.css']
})
export class AppIconComponent {

  @Input() app:App;
  id: string;

  constructor(private sapService:SAPService) {
    this.id = (new Date()).getTime()+"_sap_icon"+randomString(5);
  }

  ngAfterViewInit() {
    if(this.app.icon_type && this.app.icon_type=='sap'){
      this.sapService.ready().subscribe(()=>{
        let i = new sap.ui.core.Icon({
          src: "sap-icon://"+this.app.icon_name
        });
        if(document.getElementById(this.id))
          document.getElementById(this.id).innerHTML = "";
        i.placeAt(this.id);
      });
    }
  }
}
