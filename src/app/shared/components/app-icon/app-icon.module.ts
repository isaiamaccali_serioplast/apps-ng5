import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {AppIconComponent} from "./app-icon.component";
import {Angular2MaterialModule} from "../../material/angular2-material.module";

@NgModule({
  imports: [CommonModule,FormsModule,Angular2MaterialModule],       // module dependencies
  declarations: [AppIconComponent],   // components and directives
  exports:[AppIconComponent],
  providers: []                    // services
})
export class AppIconModule { }

