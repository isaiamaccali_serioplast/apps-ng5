import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {PeriodTimelineComponent} from "./period-timeline.component";
import {NgModule} from "@angular/core";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [PeriodTimelineComponent],   // components and directives
  exports:[PeriodTimelineComponent],
  providers: []                    // services
})
export class PeriodTimelineModule { }

