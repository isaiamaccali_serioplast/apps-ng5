import {Component, Input} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {TimeLineEvent} from "../event-timeline/event-timeline.component";
import {formatNumber} from "../../utils";

export class PeriodRow {
  name: string;
  event: TimeLineEvent
}

@Component({
  selector: 'sp-period-timeline',
  templateUrl: 'period-timeline.component.html',
  styleUrls: ['period-timeline.component.css']
})
export class PeriodTimelineComponent {
  @Input() periodRows: PeriodRow[] = [];
  @Input() big = false;
  displayDate: string;
  startMsView: number;
  endMsView: number;
  years:TimeLineEvent[] = [];

  constructor(private domSanitizer:DomSanitizer) {

  }

  ngOnInit() {

    this.periodRows.forEach((pr) => {
      if(pr.event){
        pr.event.start.setHours(0);
        pr.event.start.setMinutes(0);
        pr.event.start.setSeconds(0);
        pr.event.start.setMilliseconds(0);
        pr.event.end.setHours(23);
        pr.event.end.setMinutes(59);
        pr.event.end.setSeconds(59);
        pr.event.end.setMilliseconds(0);
      }
    });

    this.startMsView = Math.min.apply(Math,this.periodRows.filter((ps)=>ps.event).map((pr)=>pr.event.start.getTime()));
    this.endMsView = Math.max.apply(Math,this.periodRows.filter((ps)=>ps.event).map((pr)=>pr.event.end.getTime()));

    let dateStart = new Date(this.startMsView);
    let dateEnd = new Date(this.endMsView);

    if(dateStart.getFullYear()==dateEnd.getFullYear()){
      let year = new TimeLineEvent();
      year.title = dateStart.getFullYear()+"";
      year.start = dateStart;
      year.end = dateEnd;
      this.years.push(year);
    }
    else {
      let yearsDiff = dateEnd.getFullYear()-dateStart.getFullYear();
      for(let i=0; i<=yearsDiff; i++){
        let year = new TimeLineEvent();
        year.title = (dateStart.getFullYear()+i)+"";
        if(i==0){
          year.start = dateStart;
        }
        else{
          year.start = new Date(dateStart.getFullYear()+i,0,1,0,0,0);
        }

        if(i==yearsDiff){
          year.end = dateEnd;
        }
        else {
          year.end = new Date(dateStart.getFullYear()+i,11,31,23,59,59);
        }
        this.years.push(year);
      }
    }
  }

  getEventStyle(e:TimeLineEvent, rowIndex:number){
    let top = (15*rowIndex)+15;
    let left = ((e.start.getTime()-this.startMsView)/(this.endMsView-this.startMsView))*100;
    let width = ((e.end.getTime()-e.start.getTime())/(this.endMsView-this.startMsView))*100;
    return this.domSanitizer.bypassSecurityTrustStyle("top: "+top+"px; left:"+left+"%; width:"+width+"%; background-color:"+e.color);
  }

  getTimelineStyle(){
    let rowHeight= 15;
    let width = this.big?"calc(100% - 150px)":"100%";
    let margin = 0;
    return this.domSanitizer.bypassSecurityTrustStyle("height:"+(rowHeight*this.periodRows.length+15)+"px;width:"+width+"; margin-left:"+margin+"px");
  }

  getPeriodDescription(pr:PeriodRow){
    return this.dateFormat(pr.event.start)+" -> "+this.dateFormat(pr.event.end);
  }

  dateFormat(date){
    let year = (date.getFullYear()+"").substring(2);
    let month = date.getMonth() + 1;
    let monthToPrint = formatNumber(2,month);
    let day = date.getDate();
    let dayToPrint = formatNumber(2,day);
    return [year, monthToPrint, dayToPrint].reverse().join("-")
  }

  getYearStyle(y:TimeLineEvent){

    let top = 0;
    let left = ((y.start.getTime()-this.startMsView)/(this.endMsView-this.startMsView))*100;
    let width = ((y.end.getTime()-y.start.getTime())/(this.endMsView-this.startMsView))*100;
    return this.domSanitizer.bypassSecurityTrustStyle("top: "+top+"px; left:"+left+"%; width:"+width+"%;");
  }

  getYearLabelClass(y:TimeLineEvent){
    let width = ((y.end.getTime()-y.start.getTime())/(this.endMsView-this.startMsView))*100;
    if(width<15)
      return "invisible";
  }
}
