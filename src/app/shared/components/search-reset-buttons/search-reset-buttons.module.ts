import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {SearchResetButtonsComponent} from "./search-reset-buttons.component";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [SearchResetButtonsComponent],   // components and directives
  exports:[SearchResetButtonsComponent],
  providers: []                    // services
})
export class SearchResetButtonsModule { }

