import {Component, Output, EventEmitter, Input, SimpleChange} from "@angular/core";

@Component({
  selector: 'sp-search-reset',
  templateUrl: 'search-reset-buttons.component.html'
})
export class SearchResetButtonsComponent {

  @Input('searchCondition') searchCondition:boolean = true;
  @Output('search') search = new EventEmitter();
  @Output('reset') reset = new EventEmitter();

  constructor(){

  }

  doSearch(){
    this.search.emit();
  }

  doReset(){
    this.reset.emit();
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if(changes['searchCondition']){
      this.searchCondition = changes['searchCondition'].currentValue;
    }
  }


}
