/**
 * Module used to share the angular2-material modules (and temporarily angular2-materialize
 */
import {NgModule} from "@angular/core";
import {GooglePlacesAutocompleteDirective} from "./autocomplete-directive/google-places-autocomplete.directive";

@NgModule({
  imports: [],
  declarations: [GooglePlacesAutocompleteDirective],   // components and directives
  exports: [GooglePlacesAutocompleteDirective],       // module dependencies
  providers: []                    // services
})
export class GoogleMapsModule { }

