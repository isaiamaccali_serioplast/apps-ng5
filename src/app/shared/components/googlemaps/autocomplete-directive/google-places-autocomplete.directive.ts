import {AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output} from "@angular/core";

declare let google: any;

@Directive({
  selector: '[google-places-autocomplete]'
})
export class GooglePlacesAutocompleteDirective implements AfterViewInit{

  @Input('types') types = ['geocode'];
  @Output('placeChanged') placeChanged = new EventEmitter();
  ac: any;

  constructor(public elementRef: ElementRef) {

  }

  ngAfterViewInit(){
    this.ac = new google.maps.places.Autocomplete(this.elementRef.nativeElement);
    this.ac.setTypes(this.types);
    this.ac.addListener('place_changed', ()=>{
      console.log('ippo')
      this.placeChanged.emit(this.ac.getPlace());
    })
  }

}
