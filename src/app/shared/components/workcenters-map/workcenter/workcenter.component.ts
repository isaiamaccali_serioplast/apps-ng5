import {Component, Input, OnInit} from "@angular/core";
import {Workcenter, WorkcentersService} from "../../../../apps/_common/services/workcenters.service";
import {TechnologyLinkService} from "../../../../apps/_common/services/technology-link.service";
import {TechnologyService} from "../../../../apps/_common/services/technology.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'sp-workcenter',
  templateUrl: 'workcenter.component.html',
  styleUrls:['workcenter.component.css']
})
export class WorkcenterComponent implements OnInit{

  @Input('wc') workcenter:Workcenter;
  ready = false;

  constructor(private workcentersService:WorkcentersService, private technologyLinkService:TechnologyLinkService,
              private technologyService:TechnologyService, private sanitizer:DomSanitizer){

  }

  ngOnInit(){
    this.workcentersService.getWorkcenter(this.workcenter.pk).subscribe((resp)=>{
      this.workcenter = resp;
      this.technologyLinkService.getTechnologyLink(this.workcenter.technology.pk).subscribe((resp) => {
        this.workcenter.technology = resp;
        this.technologyService.getTechnology(this.workcenter.technology.technology_id.pk).subscribe((resp) => {
          this.workcenter.technology.technology_id = resp;
          this.ready = true;
        })
      })
    })
  }

  getIconByTechnology(){
    let tecSapCode = this.workcenter.technology.technology_id.sap_code;
    let iconsPath = "/assets/img/workcenters-icons/";
    let iconName = "";
    let iconExt = " 64.jpg";
    switch(tecSapCode){
      case 'EBM':
        iconName = "EBM";
        break;
      case 'PIM':
        iconName = "PIM";
        break;
      case 'CIM':
        iconName = "CIM";
        break;
      case 'ISBM':
        iconName = "ISBM";
        break;
      case 'SBM':
        iconName = "SBM";
        break;
      case 'DECO':
        iconName = "DECORATION";
        break;
      case 'TRIM':
        iconName = "TRIMMING";
        break;
      case 'TOOL':
        iconName = "TOOLING";
        break;
      case 'ASMB':
        iconName = "ASSEMBLING";
        break;
      case 'MILL':
      case 'TURN':
      case 'GRIN':
        iconName = "TOOLING";
        break;
      default:
        iconName = "";
    }
    /**
     * UNUSED: MAKING
     */
    if(iconName){
      return iconsPath+iconName+iconExt;
    }
    else {
      return null;
    }
  }

  getStyle(){
    let status_color = "green";
    //TODO capire dove recuperare il colore
    return this.sanitizer.bypassSecurityTrustStyle(" -webkit-box-shadow: 0 0 5px 2px "+status_color+";\n" +
      "    -moz-box-shadow: 0 0 5px 2px "+status_color+";\n" +
      "    box-shadow: 0 0 5px 2px "+status_color+"; background:white")

  }

  getTooltip(){
    return this.workcenter.description;
  }
}
