import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {WorkcentersMapComponent} from "./workcenters-map.component";
import {WorkcenterComponent} from "./workcenter/workcenter.component";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [WorkcentersMapComponent,WorkcenterComponent],   // components and directives
  exports:[WorkcentersMapComponent],
  providers: []                    // services
})
export class WorkcentersMapModule { }

