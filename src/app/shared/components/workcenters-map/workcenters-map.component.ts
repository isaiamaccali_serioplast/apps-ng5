import {Component, EventEmitter, Input, OnInit, Output, SimpleChange} from "@angular/core";
import {
  WorkcenterLocation,
  WorkcenterLocationsService
} from "../../../apps/_common/services/workcenter-locations.service";
import {Site, SitesService} from "../../../apps/_common/services/sites.service";
import {cloneObject, parseSerionetRespToData} from "../../utils";
import {DomSanitizer} from "@angular/platform-browser";
import {LoadingService} from "../../loading/loading.service";

@Component({
  selector: 'sp-workcenters-map',
  templateUrl: 'workcenters-map.component.html',
  styleUrls: ['workcenters-map.component.css']
})
export class WorkcentersMapComponent implements OnInit {

  @Input('site') site: Site;
  @Input('wcl') wcl: WorkcenterLocation;
  @Output('noSiteMap') noSiteMap = new EventEmitter();

  wclocations: WorkcenterLocation[] = [];

  editMode = false;
  ready = false;

  saveSite = false;

  constructor(private sitesService: SitesService, private workcenterLocationService: WorkcenterLocationsService,
              private domSanitizer: DomSanitizer, private loadingService: LoadingService) {}

  ngOnInit() {
    this.init();
  }

  init() {
    this.saveSite = false;
    if (this.site && this.site.pk) {
      this.editMode = !!(this.wcl && this.wcl.pk);
      this.sitesService.getSite(this.site.pk).subscribe((resp) => {
        this.site = resp;

        if (this.site.site_map && this.site.site_map.pk) {
          this.workcenterLocationService.getWorkcenterLocations({
            is_active: true,
            location: this.site.pk
          }).subscribe((resp) => {
            this.wclocations = parseSerionetRespToData(resp);
            if (this.editMode) {
              let match = this.wclocations.find((wcl) => wcl.pk == this.wcl.pk);
              if (match.top == null || match.left == null) {
                match.top = 0;
                match.left = 0;
              }
              this.wcl = match;
            }
            this.ready = true;
          })
        }
        else {
          this.noSiteMap.emit();
        }
      });
    }
  }

  getPositionedWorkcenters() {
    return this.wclocations.filter((wc) => wc.top != null && wc.left != null);
  }

  getUnpositionedWorkcenters() {
    return this.wclocations.filter((wc) => wc.top == null || wc.left == null);
  }

  getWorkcenterScale() {
    let pix = 64 * (1 - this.site.workcenter_scale) / 2;
    return this.domSanitizer.bypassSecurityTrustStyle("transform: scale(" + this.site.workcenter_scale + ") translate(" + pix + "px," + pix + "px)");
  }

  getWorkcenterScaleAndPosition(wcl: WorkcenterLocation) {
    let pix = 64 * (1 - this.site.workcenter_scale) / 2;
    return this.domSanitizer.bypassSecurityTrustStyle("transform: scale(" + this.site.workcenter_scale + ") translate(" + pix + "px," + pix + "px); top:" + wcl.top + "%;left:" + wcl.left + "%");
  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['site'])
      this.site = changes['site'].currentValue;

  }

  setSaveSite() {
    this.saveSite = true;
  }

  save() {
    let saveWCL = () => {
      this.loadingService.show();
      this.workcenterLocationService.saveWorkcenterLocationOnSerionet(cloneObject(this.wcl)).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.pk){
          this.ready = false;
          this.init();
        }

      })
    };
    if (this.saveSite) {
      this.loadingService.show();
      this.sitesService.saveSite(cloneObject(this.site)).subscribe((resp) => {
        this.loadingService.hide();
        if (resp.pk)
          saveWCL();
      })
    }
    else {
      saveWCL();
    }
  }
}
