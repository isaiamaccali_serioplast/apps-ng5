import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {TranslationService} from "../../i18n/translation.service";
import {Organization, OrganizationService} from "../../../apps/_common/services/organization.service";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {HeaderConf} from "../../material/table-with-pages/table-with-pages.component";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'organization-search',
  templateUrl: 'organization-search.component.html',
  styleUrls: ['organization-search.component.css']
})
export class OrganizationSearchComponent extends TranslatableComponent{

  @Input('organization') organization:Organization = new Organization();
  @Input('label') label = this.tr('ORGANIZATION');
  @Input('small') small = false;
  @Input('supplierOnly') supplierOnly = false;
  @Output('organizationSelected') organizationSelected = new EventEmitter<Organization>();
  @ViewChild('dialog') dialog:DialogComponent;

  headerConfOrganization:HeaderConf[] = [
    {
      columnId:'name',
      valueKey:'name',
      label:"Name"
    },
    {
      columnId:'code',
      valueKey:'code',
      label:"Code"
    },
    {
      columnId:'supplier',
      valueKey:'is_supplier',
      label:"Supplier",
      isBoolean:true
    },
    {
      columnId:'customer',
      valueKey:'is_customer',
      label:"Customer",
      isBoolean:true
    }
  ];

  search:string;

  constructor(protected translationService: TranslationService, private organizationsService:OrganizationService) {
    super(translationService);
  }

  reset(){
    this.organization = new Organization();
    this.organizationSelected.emit(this.organization);
  }

  openDialog(){
    this.search = "";
    this.dialog.open();
  }

  selectOrganization(o:Organization){
    this.dialog.close();
    this.organization = o;
    this.organizationSelected.emit(this.organization);
  }

  getOrganizations(page:number,results:number){
    let query = {
      page:page,
      results:results,
      ordering:'name',
      is_active:true
    };

    if(this.search){
      query['search'] = this.search;
    }
    if(this.supplierOnly){
      query['is_supplier'] = true;
    }

    return this.organizationsService.getOrganizations(query);
  }

}
