import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SaveUndoButtonsComponent} from "./save-undo-buttons.component";
import {Angular2MaterialModule} from "../../material/angular2-material.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [SaveUndoButtonsComponent],   // components and directives
  exports:[SaveUndoButtonsComponent],
  providers: []                    // services
})
export class SaveUndoButtonsModule { }

