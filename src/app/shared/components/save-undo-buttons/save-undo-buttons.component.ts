import {Component, Output, EventEmitter, Input, SimpleChange} from "@angular/core";

@Component({
  selector: 'sp-save-undo',
  templateUrl: 'save-undo-buttons.component.html',
  styleUrls:['save-undo-buttons.component.css']
})
export class SaveUndoButtonsComponent {

  @Input('saveCondition') saveCondition:boolean = true;
  @Input('undoCondition') undoCondition:boolean = true;
  @Output('save') save = new EventEmitter();
  @Output('undo') undo = new EventEmitter();

  @Input('savePartial') savePartial:boolean = false;

  constructor(){

  }

  doSave(){
    this.save.emit();
  }

  doUndo(){
    this.undo.emit();
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if(changes['saveCondition']){
      this.saveCondition = changes['saveCondition'].currentValue;
    }
    if(changes['undoCondition']){
      this.undoCondition = changes['undoCondition'].currentValue;
    }
  }


}
