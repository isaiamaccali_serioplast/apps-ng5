"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ng_module_1 = require("@angular/core/src/metadata/ng_module");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var save_undo_buttons_component_1 = require("./save-undo-buttons.component");
var angular2_material_module_1 = require("../../material/angular2-material.module");
var SaveUndoButtonsModule = (function () {
    function SaveUndoButtonsModule() {
    }
    return SaveUndoButtonsModule;
}());
SaveUndoButtonsModule = __decorate([
    ng_module_1.NgModule({
        imports: [common_1.CommonModule, forms_1.FormsModule, angular2_material_module_1.Angular2MaterialModule],
        declarations: [save_undo_buttons_component_1.SaveUndoButtonsComponent],
        exports: [save_undo_buttons_component_1.SaveUndoButtonsComponent],
        providers: [] // services
    })
], SaveUndoButtonsModule);
exports.SaveUndoButtonsModule = SaveUndoButtonsModule;
