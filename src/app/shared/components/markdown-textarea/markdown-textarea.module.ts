import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {DialogConfirmComponent} from "./dialog-confirm.component";
import {AttachmentsComponent} from "./attachments.component";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {MarkdownTextareaComponent} from "./markdown-textarea.component";
import {FixedFieldModule} from "../fixed-field/fixed-field.module";
import {MarkdownPipeModule} from "../../pipes/markdown.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule, SerioplastCommonModule, FixedFieldModule,MarkdownPipeModule],       // module dependencies
  declarations: [MarkdownTextareaComponent],   // components and directives
  exports:[MarkdownTextareaComponent],
  providers: []                    // services
})
export class MarkdownTextareaModule { }

