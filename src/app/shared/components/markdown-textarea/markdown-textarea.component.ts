import {Component, Input, OnInit} from "@angular/core";
import {TranslationService} from "../../i18n/translation.service";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'markdown-textarea',
  templateUrl: 'markdown-textarea.component.html',
  styleUrls: ['markdown-textarea.component.css']
})
export class MarkdownTextareaComponent extends TranslatableComponent implements OnInit {

  @Input() object: any;
  @Input() field: string;
  @Input() label: string;
  @Input() vertical = false;

  showPreview = true;

  constructor(protected translationService: TranslationService) {
    super(translationService);
  }

  ngOnInit() {
  }

  timeout = null;

  refresh(){
    // if(this.timeout)
    //   clearTimeout(this.timeout);
    // this.timeout = setTimeout(()=>{
    //   this.showPreview = false;
    //   setTimeout(()=>{
    //     this.showPreview = true;
    //   },100)
    // },100)
  }


}
