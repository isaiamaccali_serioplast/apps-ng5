import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {EventTimelineComponent} from "./event-timeline.component";
import {NgModule} from "@angular/core";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [EventTimelineComponent],   // components and directives
  exports:[EventTimelineComponent],
  providers: []                    // services
})
export class EventTimelineModule { }

