import {Component, Input} from "@angular/core";
import {monthName, dayName, formatNumber} from "../../utils";
import {DomSanitizer} from "@angular/platform-browser";

export class Column {
  cssClass: string;
  header: string;
  datetime: Date;
}

export class TimeLineEvent {
  start: Date;
  end: Date;
  color: string;
  title?: string;
}

export class EventsRow {
  name: string;
  events: TimeLineEvent[]
}


@Component({
  selector: 'sp-event-timeline',
  templateUrl: 'event-timeline.component.html',
  styleUrls: ['event-timeline.component.css']
})
export class EventTimelineComponent {
  @Input() period: string = "week";
  @Input() date: Date = new Date();
  @Input() eventRows: EventsRow[] = [];
  @Input() adjustToFit = false;
  columns: Column[] = [];
  displayDate: string;
  startMsView: number;
  endMsView: number;


  constructor(private domSanitizer:DomSanitizer) {

  }

  ngOnInit() {}

  previousPeriod() {
    switch (this.period) {
      case 'day':
        this.date.setDate(this.date.getDate() - 1);
        break;
      case 'week':
        this.date.setDate(this.date.getDate() - this.date.getDay() - 7);
        break;
      case 'month':
        this.date.setDate(1);
        this.date.setMonth(this.date.getMonth() - 1);
        break;
      case 'year':
        this.date.setDate(1);
        this.date.setFullYear(this.date.getFullYear() - 1);
        break;
      default:
        break;
    }
    this.periodChange();
  }

  nextPeriod() {
    switch (this.period) {
      case 'day':
        this.date.setDate(this.date.getDate() + 1);
        break;
      case 'week':
        this.date.setDate(this.date.getDate() - this.date.getDay() + 7);
        break;
      case 'month':
        this.date.setDate(1);
        this.date.setMonth(this.date.getMonth() + 1);
        break;
      case 'year':
        this.date.setDate(1);
        this.date.setFullYear(this.date.getFullYear() + 1);
        break;
      default:
        break;
    }
    this.periodChange();
  }

  getDateDisplay() {
    switch (this.period) {
      case 'day':
        this.displayDate = this.date.getDate() + " " + monthName(this.date.getMonth()) + " " + this.date.getFullYear();
        break;
      case 'week':
        let first = this.date.getDate() - this.date.getDay(); // First day is the day of the month - the day of the week
        let last = first + 6; // last day is the first day + 6
        let firstday = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
        let lastday = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
        firstday.setDate(first);
        lastday.setDate(last);
        if (firstday.getMonth() != lastday.getMonth()) {
          if (firstday.getFullYear() != lastday.getFullYear()) {
            this.displayDate = firstday.getDate() + " " + monthName(firstday.getMonth()) + " " + firstday.getFullYear() + " - " + lastday.getDate() + " " + monthName(lastday.getMonth()) + " " + " " + firstday.getFullYear();
          } else {
            this.displayDate = firstday.getDate() + " " + monthName(firstday.getMonth()) + " - " + lastday.getDate() + " " + monthName(lastday.getMonth()) + " " + this.date.getFullYear();
          }
        } else {
          this.displayDate = firstday.getDate() + " - " + lastday.getDate() + " " + monthName(lastday.getMonth()) + " " + this.date.getFullYear();
        }

        break;
      case 'month':
        this.displayDate = monthName(this.date.getMonth()) + " " + this.date.getFullYear();
        break;
      case 'year':
        this.displayDate = this.date.getFullYear()+"";
        break;
      default:
        break;
    }
  }

  periodChange() {

    this.columns = [];
    let d = new Date(this.date.getTime());
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);

    switch (this.period) {
      case 'day':
        for (let i = 0; i < 12; i++) {
          let c = new Column();
          c.cssClass = "day";
          let dt = new Date(d.getTime());
          c.datetime = dt;
          c.header = formatNumber(2, dt.getHours()) + ":" + formatNumber(2, dt.getMinutes());
          this.columns.push(c);
          d.setHours(d.getHours() + 2)
        }

        break;
      case 'week':
        d.setDate(d.getDate() - d.getDay());
        for (let i = 0; i < 7; i++) {
          let c = new Column();
          c.cssClass = "week";
          c.header = dayName(i);
          c.datetime = new Date(d.getTime());
          this.columns.push(c);
          d.setDate(d.getDate() + 1)
        }

        break;
      case 'month':
        d.setDate(1);
        for (let i = 0; i < 16; i++) {
          let c = new Column();
          c.cssClass = "month";
          let dt = new Date(d.getTime());
          c.datetime = dt;
          c.header = dt.getDate().toString();
          this.columns.push(c);
          d.setDate(d.getDate() + 2)
        }
        this.date.setDate(1);
        break;
      case 'year':
        d.setDate(1);
        d.setMonth(0);
        for (let i = 0; i < 12; i++) {
          let c = new Column();
          c.cssClass = "year";
          let dt = new Date(d.getTime());
          c.datetime = dt;
          c.header = monthName(i);
          this.columns.push(c);
          d.setMonth(d.getMonth() + 1)
        }
        this.date.setDate(1);
        break;
      default:
        break;
    }

    this.getDateDisplay();
    this.startMsView = this.columns[0].datetime.getTime();
    this.endMsView = d.getTime();
  }

  getEventStyle(e:TimeLineEvent, rowIndex:number){
    let top = 30*rowIndex;
    let left = ((e.start.getTime()-this.startMsView)/(this.endMsView-this.startMsView))*100;
    let width = ((e.end.getTime()-e.start.getTime())/(this.endMsView-this.startMsView))*100;

    return this.domSanitizer.bypassSecurityTrustStyle("top: "+top+"px; left:"+left+"%; width:"+width+"%; background-color:"+e.color);
  }

  getTimelineStyle(){
    return this.domSanitizer.bypassSecurityTrustStyle("height:"+(30*this.eventRows.length)+"px;");
  }
}
