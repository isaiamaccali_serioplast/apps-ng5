import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {DialogConfirmComponent} from "./dialog-confirm.component";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {ReportsComponent} from "./reports.component";
import {DialogModule} from "../../material/dialog/dialog.module";
import {StepperModule} from "../../material/stepper/stepper.module";
import {I18NModule} from "../../i18n/i18n.module";
import {SanitizeHTMLModule} from "../../pipes/sanitizehtml.module";
import {FixedFieldModule} from "../fixed-field/fixed-field.module";

@NgModule({
  imports: [CommonModule, FormsModule, Angular2MaterialModule, SerioplastCommonModule, DialogModule, StepperModule, I18NModule,SanitizeHTMLModule, FixedFieldModule],       // module dependencies
  declarations: [ReportsComponent],   // components and directives
  exports: [ReportsComponent],
  providers: []                    // services
})
export class ReportsModule {
}

