import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {parseSerionetRespToData, cloneObject} from "../../utils";
import {LoadingService} from "../../loading/loading.service";
import {MessageService} from "../../material/message/message.service";
import {TranslationService} from "../../i18n/translation.service";
import {
  ReportService, Report, Dataset, ReportConfig,
  InputFilter
} from "../../../apps/_common/services/reports.service";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'sp-reports',
  templateUrl: 'reports.component.html',
  styleUrls: ['reports.component.css']
})
export class ReportsComponent extends TranslatableComponent implements OnInit {

  @Input() reportsConfig: ReportConfig[] = [];
  @Input() tags: number[] = []; // pk of external objects
  @Input() args: any = {};

  reports: Report[] = [];
  @ViewChild('reportsDialog') reportsDialog: DialogComponent;
  selectedReport: Report = new Report();
  limit: number = 15;
  filters: InputFilter[] = [];
  parameters: InputFilter[] = [];

  constructor(private reportService: ReportService,
              private loadingService: LoadingService,
              private messageService: MessageService,
              protected translationService: TranslationService) {
    super(translationService);
  }

  ngOnInit() {
    // this.init();
  }

  init(){
    this.selectedReport = new Report();
    this.reports = [];
    this.loadReports();
  }

  handleReportChange() {
  }

  step1ProceedCondition() {
    return this.selectedReport && this.selectedReport.pk;
  }

  step2ProceedCondition(){
   /* let params:InputFilter[] = cloneObject(this.parameters);
    let required_params = params.filter((p) => p.required);

    for(let rp of required_params){
      if (!rp.value){
        return false
      }
    }*/

    return true;
  }

  step1ProceedConfirm() {
    this.selectedReport.datasets = [];
    this.setReportParameters();
  }

  step2ProceedConfirm() {
    this.selectedReport.datasets = [];
    this.setReportConfig();
  }

  setReportParameters() {
    let reportConfig = cloneObject(this.reportsConfig);
    let selectedReportConfig = reportConfig.filter((x) => x.python_func == this.selectedReport.python_func);
    if (selectedReportConfig.length == 1){
      this.parameters = (selectedReportConfig[0].params)? selectedReportConfig[0].params : [];
    }
  }

  setReportConfig() {
    let reportConfig = cloneObject(this.reportsConfig);
    let selectedReportConfig = reportConfig.filter((x) => x.python_func == this.selectedReport.python_func);
    if (selectedReportConfig.length == 1){
      this.filters = (selectedReportConfig[0].filters)? selectedReportConfig[0].filters : [];
    }
  }

  checkVisibleFilters(){
    // for view reset button
    for(let f of this.filters){
      if (f.visible){
        return true;
      }
    }

    return false
  }

  loadRestViewsChoicesFilters() {

    let choicesFilters = [];

    for (let f of this.filters) {
      if (f['type'] == 'select') {
        f['results'] = [];
        choicesFilters.push(f);
      }
    }

    this.loadingService.show();
    let count = choicesFilters.length;

    let callback = () => {
      count--;
      if (count == -1) {
        this.loadingService.hide();
      }
      else {
        this.reportService.getDynamicList(choicesFilters[count].serviceURL, choicesFilters[count].query).subscribe((resp) => {
          choicesFilters[count].count = (resp['count']) ? resp['count'] : 0;
          choicesFilters[count].results = parseSerionetRespToData(resp);
          for (let f of this.filters) {
            if (choicesFilters[count]['name'] == f['name']) {
              f = choicesFilters[count];
            }
          }
          callback();
        });
      }
    };

    callback();
  }

  loadReports() {
    let query = {is_active: true};
    if (this.tags.length > 0) {
      query['tag__in'] = this.tags;
    }

    this.reportService.getReports(query).subscribe((resp) => {
      this.reports = parseSerionetRespToData(resp);
      this.reportsDialog.open();
    })
  }

  loadDatasets() {
    this.selectedReport.datasets = [];

    this.loadingService.show();
    this.reportService.getReports({
      is_preview: true,
      python_func: this.selectedReport.python_func,
      args: this.getArgs()
    }).subscribe((resp) => {

      if (resp["report_response"]) {
        let reportPreview = resp["report_response"];
        this.selectedReport.datasets = (reportPreview.datasets) ? reportPreview.datasets : [];
      } else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: resp["error"],
          autoClose: false
        });
      }


      this.loadingService.hide();

    })
  }

  resetDatasets() {
    this.setReportConfig();
    this.loadDatasets();
  }

  getArgs() {
    let args = {};
    for (let f of this.parameters) {
      if (f["value"]) {
        if(f["value"]["pk"]){
          args[f["name"]] = f["value"]["pk"];
        }else{
          args[f["name"]] = f["value"];
        }
      } else {
        if (f["type"] == 'datetimerange' && f["value1"] && f["value2"]) {
          args[f["name"]] = [f["value1"], f["value2"]];
        }
      }
    }

    for (let f of this.filters) {
      if (f["value"]) {
        args[f["name"]] = f["value"];
      } else {
        if (f["type"] == 'datetimerange' && f["value1"] && f["value2"]) {
          args[f["name"]] = [f["value1"], f["value2"]];
        }
      }
    }

    return JSON.stringify(args)
  }

  downloadReport(report: Report) {
    this.loadingService.show();
    this.reportService.getReports({
      generate: true,
      python_func: report.python_func,
      args: this.getArgs()
    }).subscribe((resp) => {
      this.loadingService.hide();
      if (resp["report_response"]) {
        let link = document.createElement("a");
        link.href = resp["report_response"];
        link.download = resp["report_response"].split(-1);
        link.click();
      } else {
        this.messageService.open({
          title: 'Error',
          error: true,
          message: resp["error"],
          autoClose: false
        });
      }
    })
  }

  getValue(item, field) {
    return item[field];
  }

  downloadDataset(dataset: Dataset) {
    if (dataset.file_url) {
      window.location.assign(dataset.file_url);
    } else {
      this.messageService.open({
        title: 'Error',
        error: true,
        message: "No download",
        autoClose: false
      });
    }

  }

  step2Previous() {
    this.selectedReport = new Report();
  }

}
