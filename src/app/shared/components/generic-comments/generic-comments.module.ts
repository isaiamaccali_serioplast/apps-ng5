import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {CommonModule} from "@angular/common";
import {GenericCommentAddModule} from "./generic-comment-add/generic-comment-add.module";
import {GenericCommentListModule} from "./generic-comment-list/generic-comment-list.module";
import {GenericCommentDetailModule} from "./generic-comment-detail/generic-comment-detail.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule,GenericCommentAddModule,GenericCommentListModule,GenericCommentDetailModule],       // module dependencies
  declarations: [],   // components and directives
  exports:[GenericCommentAddModule,GenericCommentListModule,GenericCommentDetailModule],
  providers: []                    // services
})
export class GenericCommentsModule { }
