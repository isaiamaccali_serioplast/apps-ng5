import {NgModule} from "@angular/core";
import {GenericCommentAddComponent} from "./generic-comment-add.component";
import {I18NModule} from "../../../i18n/i18n.module";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../../material/angular2-material.module";
import {FormsModule} from "@angular/forms";


@NgModule({
  imports: [I18NModule, CommonModule, FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [GenericCommentAddComponent],   // components and directives
  exports:[GenericCommentAddComponent],
  providers: []                    // services
})
export class GenericCommentAddModule { }


