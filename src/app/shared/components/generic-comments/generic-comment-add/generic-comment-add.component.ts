import {Component, OnInit, Input} from "@angular/core";
import {CommentsService, SerionetComment} from "../../../../apps/_common/services/comments.service";
import {MessageService} from "../../../material/message/message.service";
import {buildErrorMessage} from "../../../utils";
import {TranslationService} from "../../../i18n/translation.service";
import {TranslatableComponent} from "../../base-component/translatable.component";


@Component({
  selector: 'sp-generic-comment-add',
  templateUrl: 'generic-comment-add.component.html',
  styleUrls: ['generic-comment-add.component.css'],
  providers: []
})
export class GenericCommentAddComponent extends TranslatableComponent implements OnInit {

  comment: SerionetComment;
  @Input() contentType:number;
  @Input() objectId:number;
  @Input() reload;

  constructor(private commentService: CommentsService, private messageService:MessageService,
              protected translationService:TranslationService) {
    super(translationService);
    this.comment = new SerionetComment();
  }

  ngOnInit() {
    this.comment.object_id = this.objectId;
    this.comment.content_type = this.contentType;
  }

  addComment() {
    this.commentService.saveComment(this.comment).subscribe((resp)=> {
      if(resp.pk){
        this.comment = new SerionetComment();
        this.comment.object_id = this.objectId;
        this.comment.content_type = this.contentType;
        this.reload();
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }

    })
  }

}
