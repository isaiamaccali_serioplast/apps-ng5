import {NgModule} from "@angular/core";
import {GenericCommentDetailComponent} from "./generic-comment-detail.component";
import {UsersModule} from "../../user/user.module";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {Angular2MaterialModule} from "../../../material/angular2-material.module";
import {MarkdownPipeModule} from "../../../pipes/markdown.module";

@NgModule({
  imports: [UsersModule, CommonModule,FormsModule,Angular2MaterialModule, MarkdownPipeModule],       // module dependencies
  declarations: [GenericCommentDetailComponent],   // components and directives
  exports: [GenericCommentDetailComponent],
  providers: []                    // services
})
export class GenericCommentDetailModule {
}


