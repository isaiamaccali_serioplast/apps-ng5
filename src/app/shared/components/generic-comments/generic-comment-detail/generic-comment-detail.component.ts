import {Component, OnInit, Input} from "@angular/core";
import {SerionetComment} from "../../../../apps/_common/services/comments.service";

@Component({
  selector: 'sp-generic-comment-detail',
  templateUrl: 'generic-comment-detail.component.html',
  styleUrls: ['generic-comment-detail.component.css']
})
export class GenericCommentDetailComponent implements OnInit {

  @Input() comment: SerionetComment;

  constructor() {}

  ngOnInit() {}

}
