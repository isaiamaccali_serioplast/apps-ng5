import {NgModule} from "@angular/core";
import {GenericCommentListComponent} from "./generic-comment-list.component";
import {GenericCommentDetailModule} from "../generic-comment-detail/generic-comment-detail.module";
import {GenericCommentAddModule} from "../generic-comment-add/generic-comment-add.module";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";


@NgModule({
  imports: [GenericCommentDetailModule, GenericCommentAddModule, CommonModule, FormsModule],       // module dependencies
  declarations: [GenericCommentListComponent],   // components and directives
  exports:[GenericCommentListComponent],
  providers: []                    // services
})
export class GenericCommentListModule { }

