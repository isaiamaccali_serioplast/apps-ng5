import {Component, OnInit, Input, SimpleChange} from "@angular/core";
import {CommentsService, SerionetComment} from "../../../../apps/_common/services/comments.service";
import {parseSerionetRespToData} from "../../../utils";

@Component({
  selector: 'sp-generic-comment-list',
  templateUrl: 'generic-comment-list.component.html',
  styleUrls: ['generic-comment-list.component.css']
})
export class GenericCommentListComponent implements OnInit {

  @Input() contentType: number;
  @Input() objectId:number;
  @Input() allowAdd = true;
  comments:SerionetComment[] = [];

  constructor(private commentsService: CommentsService) {}

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.commentsService.getComments({object_id:this.objectId,content_type:this.contentType, ordering:'created_on'}).subscribe((data) => {
      this.comments = parseSerionetRespToData(data);
    });
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if(changes['objectId']){
      this.objectId = changes['objectId'].currentValue;
      this.reload();
    }
  }

}
