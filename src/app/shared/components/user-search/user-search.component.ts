import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {TranslationService} from "../../i18n/translation.service";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {HeaderConf, TableWithPagesComponent} from "../../material/table-with-pages/table-with-pages.component";
import {HRProfile, HRProfileService} from "../../../apps/_common/services/hr-profiles.service";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'user-search',
  templateUrl: 'user-search.component.html',
  styleUrls: ['user-search.component.css']
})
export class UserSearchComponent extends TranslatableComponent{

  @Input('user') user:HRProfile = new HRProfile();
  @Input('additionalQuery') additionalQuery = {};
  @Input('resetOnSelection') resetOnSelection = false;
  @Input('label') label = this.tr('USER');
  @Input('employeeOnly') employeeOnly = true;
  @Output('userSelected') userSelected = new EventEmitter<HRProfile>();
  @ViewChild('dialog') dialog:DialogComponent;
  @ViewChild('table') table:TableWithPagesComponent;

  headerConf:HeaderConf[] = [
    {
      columnId:'user',
      valueKey:'__str__',
      label:"Name"
    }
  ];

  search:string;

  constructor(protected translationService: TranslationService, private hrProfileService:HRProfileService) {
    super(translationService);
  }

  ngOnInit(){
    if(!this.user)
      this.user = new HRProfile();
  }

  reset(){
    this.user = new HRProfile();
    this.userSelected.emit(this.user);
  }

  openDialog(){
    this.search = "";
    this.dialog.open();
  }

  select(u:HRProfile){
    this.dialog.close();
    this.user = u;
    this.userSelected.emit(u);
    if(this.resetOnSelection)
      this.user = new HRProfile();
  }

  getUsers(page:number,results:number){
    let query = {
      page:page,
      results:results,
      ordering:'name',
      is_active: true
    };

    if(this.employeeOnly)
      query['is_employee'] = 2;

    if(this.search){
      query['full_name_icontains'] = this.search;
    }

    if(this.additionalQuery && Object.keys(this.additionalQuery).length>0)
    Object.assign(query,this.additionalQuery);

    return this.hrProfileService.getHRProfiles(query);
  }

  timeout:any;
  doSearchByInput(){
    if(this.timeout)
      clearTimeout(this.timeout);
    this.timeout = setTimeout(()=>{
      this.table.reload();
    },500)
  }
}
