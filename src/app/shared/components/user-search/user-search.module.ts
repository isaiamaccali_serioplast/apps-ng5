import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {UserSearchComponent} from "./user-search.component";
import {I18NModule} from "../../i18n/i18n.module";
import {TableWithPagesModule} from "../../material/table-with-pages/table-with-pages.module";
import {DialogModule} from "../../material/dialog/dialog.module";
import {FixedFieldModule} from "../fixed-field/fixed-field.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule,SerioplastCommonModule, I18NModule, TableWithPagesModule, DialogModule, FixedFieldModule],       // module dependencies
  declarations: [UserSearchComponent],   // components and directives
  exports:[UserSearchComponent],
  providers: []                    // services
})
export class UserSearchModule { }

