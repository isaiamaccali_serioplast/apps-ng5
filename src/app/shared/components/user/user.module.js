"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var angular2_material_module_1 = require("../../material/angular2-material.module");
var user_component_1 = require("./user.component");
var users_service_1 = require("./service/users.service");
var common_1 = require("@angular/common");
var UsersModule = (function () {
    function UsersModule() {
    }
    return UsersModule;
}());
UsersModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule, angular2_material_module_1.Angular2MaterialModule],
        declarations: [user_component_1.UserComponent],
        exports: [user_component_1.UserComponent],
        providers: [users_service_1.SerionetUsersService] // services
    })
], UsersModule);
exports.UsersModule = UsersModule;
