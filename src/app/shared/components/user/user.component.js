/**
 * User component. Used to show the users info (avatar and name) in a standard way
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var UserComponent = (function () {
    function UserComponent(usersService) {
        this.usersService = usersService;
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.getUserData(this.userPK).subscribe(function (user) {
            _this.user = user;
        });
    };
    return UserComponent;
}());
__decorate([
    core_1.Input()
], UserComponent.prototype, "userPK", void 0);
UserComponent = __decorate([
    core_1.Component({
        selector: 'sp-user',
        templateUrl: 'src/app/shared/components/user/user.component.html',
        styleUrls: ['src/app/shared/components/user/user.component.css']
    })
], UserComponent);
exports.UserComponent = UserComponent;
