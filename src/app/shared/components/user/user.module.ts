import {NgModule} from "@angular/core";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {UserComponent} from "./user.component";
import {CommonModule} from "@angular/common";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";


@NgModule({
  imports: [CommonModule,Angular2MaterialModule, SerioplastCommonModule],       // module dependencies
  declarations: [UserComponent],   // components and directives
  exports:[UserComponent],
  providers: []                    // services
})
export class UsersModule { }

