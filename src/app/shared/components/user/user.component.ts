/**
 * User component. Used to show the users info (avatar and name) in a standard way
 */

import {Component, Input} from "@angular/core";
import {User, UsersService} from "../../../apps/_common/services/users.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'sp-user',
  templateUrl: 'user.component.html',
  styleUrls: ['user.component.css']
})
export class UserComponent {

  @Input() userPK: number;
  @Input() small: boolean = false;
  user:User;
  style = "";
  hideAvatar = false;

  constructor(private usersService:UsersService, private domSanitizer:DomSanitizer) {}

  ngOnInit(){
    this.usersService.getUserData(this.userPK).subscribe((user)=>{
      this.user = user;
      this.style = "width:100%; height: 100%;border-radius:50%; background-size:cover; background-repeat:no-repeat;background-image:url("+this.user.avatar+");"
    })
  }

  getImageAsBackground(){
    return this.domSanitizer.bypassSecurityTrustStyle(this.style);
  }

  hideImg(){
    this.hideAvatar = true;
  }

}
