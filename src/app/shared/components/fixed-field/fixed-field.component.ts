import {Component, Input, SimpleChange} from "@angular/core";

@Component({
  selector: 'fixed-field',
  templateUrl: 'fixed-field.component.html',
  styleUrls:['fixed-field.component.css']
})
export class FixedFieldComponent {

  @Input() value:any;
  @Input() label:string;
  @Input() useMarkdown = false;
  @Input() useContent = false;
  @Input() isBoolean = false;
  @Input() noMargin = false;

  constructor() {
  }

  ngOnInit(){

  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['label'])
      this.label = changes['label'].currentValue;
    if (changes['value'])
      this.value = changes['value'].currentValue;
    if (changes['useMarkdown'])
      this.useMarkdown = changes['useMarkdown'].currentValue;
    if (changes['isBoolean'])
      this.isBoolean = changes['isBoolean'].currentValue;
  }
}
