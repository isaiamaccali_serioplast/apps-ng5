import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {FixedFieldComponent} from "./fixed-field.component";
import {MarkdownPipeModule} from "../../pipes/markdown.module";

@NgModule({
  imports: [CommonModule,FormsModule,Angular2MaterialModule, MarkdownPipeModule],       // module dependencies
  declarations: [FixedFieldComponent],   // components and directives
  exports:[FixedFieldComponent],
  providers: []                    // services
})
export class FixedFieldModule { }

