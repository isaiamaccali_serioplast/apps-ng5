import {Component, Input, OnInit} from "@angular/core";
import {PropertiesService, Property} from "../../../apps/_common/services/properties.service";

@Component({
  selector: 'sp-property',
  templateUrl: 'property.component.html'
})
export class PropertyComponent implements OnInit{

  @Input() propertyPK:number;
  @Input() property:Property;
  @Input() activateDialog = false;

  constructor(private propertyService:PropertiesService){

  }

  ngOnInit(){
    if(!this.property||!this.property.pk && this.propertyPK){
      this.propertyService.getProperty(this.propertyPK).subscribe((p)=>{
        this.property = p;
      })
    }
    else {

    }
  }

}
