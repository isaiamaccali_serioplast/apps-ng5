import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {PropertyComponent} from "./property.component";
import {CommonModule} from "@angular/common";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {DialogModule} from "../../material/dialog/dialog.module";
import {Angular2MaterialModule} from "../../material/angular2-material.module";

@NgModule({
  imports: [CommonModule,FormsModule, SerioplastCommonModule, DialogModule, Angular2MaterialModule],       // module dependencies
  declarations: [PropertyComponent],   // components and directives
  exports:[PropertyComponent],
  providers: []                    // services
})
export class PropertyModule { }

