"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ng_module_1 = require("@angular/core/src/metadata/ng_module");
var forms_1 = require("@angular/forms");
var property_component_1 = require("./property.component");
var common_1 = require("@angular/common");
var common_module_1 = require("../../../apps/_common/common.module");
var PropertyModule = (function () {
    function PropertyModule() {
    }
    return PropertyModule;
}());
PropertyModule = __decorate([
    ng_module_1.NgModule({
        imports: [common_1.CommonModule, forms_1.FormsModule, common_module_1.SerioplastCommonModule],
        declarations: [property_component_1.PropertyComponent],
        exports: [property_component_1.PropertyComponent],
        providers: [] // services
    })
], PropertyModule);
exports.PropertyModule = PropertyModule;
