import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {DialogConfirmComponent} from "./dialog-confirm.component";
import {AttachmentsComponent} from "./attachments.component";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {I18NModule} from "../../i18n/i18n.module";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule, SerioplastCommonModule, I18NModule],       // module dependencies
  declarations: [AttachmentsComponent],   // components and directives
  exports:[AttachmentsComponent],
  providers: []                    // services
})
export class AttachmentsModule { }

