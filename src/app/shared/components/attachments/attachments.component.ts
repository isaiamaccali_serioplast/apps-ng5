import {Component, Input, OnInit, SimpleChange, ViewChild} from "@angular/core";
import {FileManager, FileManagerService} from "../../../apps/_common/services/file-manager.service";
import {parseSerionetRespToData} from "../../utils";
import {FileService, SPFile} from "../../../apps/_common/services/file.service";
import {LoadingService} from "../../loading/loading.service";
import {MessageService} from "../../material/message/message.service";
import {TranslationService} from "../../i18n/translation.service";
import {BaseComponent} from "../base-component/base.component";

@Component({
  selector: 'sp-attachments',
  templateUrl: 'attachments.component.html',
  styleUrls: ['attachments.component.css']
})
export class AttachmentsComponent extends BaseComponent implements OnInit {

  @Input() contentType: number;
  @Input() label: string;
  @Input() objectId: number;
  @Input() allowAdd = true;
  @ViewChild('fileInput') fileInput: HTMLInputElement;
  @ViewChild('fileName') fileName: HTMLInputElement;
  attachments: FileManager[] = [];
  file: SPFile = new SPFile();

  constructor(private fileManagerService: FileManagerService, private fileService: FileService,
              protected loadingService: LoadingService, protected messageService: MessageService,
              protected translationService: TranslationService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.fileManagerService.getFileManagers({content_type: this.contentType, object_id: this.objectId, is_active: true, ordering: 'created_on', explode_all: true}).subscribe((resp) => {
      this.attachments = parseSerionetRespToData(resp);
    })
  }

  deleteAttachment(a: FileManager) {
    this.messageService.open({
      title: this.tr('CONFIRM'),
      message: this.tr('CONFIRM_DELETE'),
      confirm: () => {
        this.loadingService.show();
        this.fileManagerService.deleteFileManager(a).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.fileService.getFile(a.filepath.pk).subscribe((resp) => {
              if (resp.pk) {
                let f: SPFile = resp;
                if (f.related_objects.length == 0) {
                  this.fileService.deleteFile(a.filepath).subscribe((resp) => {
                    this.manageRespDelete(resp, () => {
                      this.loadingService.hide();
                      this.reload();
                    })
                  })
                }
                else {
                  this.loadingService.hide();
                  this.messageService.open({
                    title: " ",
                    message: this.tr('DELETE_COMPLETED'),
                    onClose: () => {
                      this.reload();
                    }
                  });
                }
              }
            });
          }, true)
        })
      },
      deny: () => {
      }
    });
  }

  uploadFile() {
    if (this.fileInput['nativeElement'].value) {
      this.loadingService.show();
      let f = new SPFile();
      f.field_name = 'attachment';
      f.content_type = this.contentType;
      let fm = new FileManager();
      fm.is_active = true;
      fm.object_id = this.objectId;
      fm.content_type = this.contentType;
      this.file.related_objects = [fm];
      this.fileService.saveFile(this.file, this.fileInput['nativeElement']).subscribe((data) => {
        this.manageRespSave(data, () => {
          this.loadingService.hide();
          this.fileInput['nativeElement'].value = null;
          this.fileName['nativeElement'].value = "";
          this.file = new SPFile();
          this.fileIsSet = false;
          this.fileAdd = false;
          this.reload();
        })
      })
    }
  }

  extractFileName(filePath) {
    let pieces = filePath.split("/");
    return pieces[pieces.length - 1];
  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['objectId']) {
      this.objectId = changes['objectId'].currentValue;
      this.reload();
    }
  }

  fileIsSet = false;

  setFile() {
    this.fileIsSet = true;
  }

  fileAdd = false;

  addFile() {
    this.fileAdd = true;
  }

  undoFile() {
    this.fileAdd = false;
  }
}
