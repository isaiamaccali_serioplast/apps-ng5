import {Component, Input, SimpleChange} from "@angular/core";

@Component({
  selector: 'pk-tr',
  templateUrl: 'pk-transformer.component.html'
})
export class PkTransformerComponent {

  @Input() source:any;

  ready = false;
  constructor(){

  }

  ngOnInit(){
    this.ready = true;
  }

  ngOnChanges(changes: {[ propName: string]: SimpleChange}) {
    if(changes['source'])
      this.source = changes['source'].currentValue;

  }


}
