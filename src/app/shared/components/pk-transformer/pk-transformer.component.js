"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var PkTransformerComponent = (function () {
    function PkTransformerComponent() {
        this.ready = false;
    }
    PkTransformerComponent.prototype.ngOnInit = function () {
        this.ready = true;
    };
    PkTransformerComponent.prototype.ngOnChanges = function (changes) {
        if (changes['source'])
            this.source = changes['source'].currentValue;
    };
    return PkTransformerComponent;
}());
__decorate([
    core_1.Input()
], PkTransformerComponent.prototype, "source", void 0);
PkTransformerComponent = __decorate([
    core_1.Component({
        selector: 'pk-tr',
        templateUrl: 'src/app/shared/components/pk-transformer/pk-transformer.component.html'
    })
], PkTransformerComponent);
exports.PkTransformerComponent = PkTransformerComponent;
