import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {PkTransformerComponent} from "./pk-transformer.component";

@NgModule({
  imports: [CommonModule, FormsModule],       // module dependencies
  declarations: [PkTransformerComponent],   // components and directives
  exports:[PkTransformerComponent],
  providers: []                    // services
})
export class PkTransformerModule { }

