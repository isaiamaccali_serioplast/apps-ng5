import {Component, OnInit, ViewChild, Input} from "@angular/core";
import {TranslationService} from "../../i18n/translation.service";
import {PersonalInfo, PersonalInfoService} from "../../../apps/_common/services/personalinfo.service";
import {LoadingService} from "../../loading/loading.service";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {EducationService} from "../../../apps/_common/services/education.service";
import {ProfessionalExperiencesService} from "../../../apps/_common/services/professional-experiences.service";
import {NationalityService} from "../../../apps/_common/services/nationality.service";
import {WorkingContactsService} from "../../../apps/_common/services/workingcontacts.service";
import {LanguageService} from "../../../apps/_common/services/language.service";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'modal-personalinfo',
  templateUrl: 'modal-personalinfo.component.html',
  styleUrls: ['modal-personalinfo.component.css']
})

export class ModalPersonalInfoComponent extends TranslatableComponent implements OnInit {
  personalinfo: PersonalInfo = new PersonalInfo();
  @Input() label: string;
  @Input() object_id: number;
  @ViewChild('personalInfoModal') personalInfoModal: DialogComponent;

  noItemsMessageEducations = this.tr("No educations");
  headerConfEducations = [
    {
      label: this.tr("Date From"),
      valueKey: 'date_from'
    },
    {
      label: this.tr("Date to"),
      valueKey: 'date_to'
    },
    {
      label: this.tr("Institution"),
      valueKey: 'institution',
    },
    {
      label: this.tr("Degree"),
      valueKey: 'degree',
      useStr: true,
    },
    {
      label: this.tr("Study field"),
      valueKey: 'study_field',
      useStr: true,
    },
    {
      label: this.tr("Final grade"),
      valueKey: 'final_grade'
    },
    {
      label: this.tr("Country"),
      valueKey: 'country',
    },
    {
      label: this.tr("City"),
      valueKey: 'city'
    },
    {
      label: this.tr("Description"),
      valueKey: 'description'
    },
  ];

  noItemsMessageProfessionalExperiences = this.tr("No professional experiences.");
  headerConfProfessionalExperiences = [
    {
      label: this.tr("Company"),
      valueKey: 'company_name'
    },
    {
      label: this.tr("Date From"),
      valueKey: 'date_from'
    },
    {
      label: this.tr("Date to"),
      valueKey: 'date_to'
    },
    {
      label: this.tr("Job Title"),
      valueKey: 'job_title'
    },

    {
      label: this.tr("Country"),
      valueKey: 'country',
    },

    {
      label: this.tr("City"),
      valueKey: 'city'
    },
    {
      label: this.tr("Function"),
      valueKey: 'function',
      useStr: true,
    },
    {
      label: this.tr("Industry"),
      valueKey: 'industry',
      useStr: true,
    },
    {
      label: this.tr("Contract Type"),
      valueKey: 'contract_type',
      useStr: true,
    },
  ];

  noItemsMessageNationalities = this.tr("No nationalities");
  headerConfNationalities = [
    {
      label: this.tr("Nationality"),
      valueKey: '__str__',
    },
    {
      label: this.tr("Country ID Number"),
      valueKey: 'country_id_number'
    },
    {
      label: this.tr("Country Tax Number"),
      valueKey: 'country_tax_number'
    },
  ];

  noItemsMessageWorkingContacts = this.tr("No working contacts");
  headerConfWorkingContacts  = [
    {
      label: this.tr("Work Mobile Phone"),
      valueKey: 'work_mobile_phone',
    },
    {
      label: this.tr("Work Landline Phone"),
      valueKey: 'work_landline_phone'
    },
    {
      label: this.tr("Work E-Mail"),
      valueKey: 'work_email'
    },
    {
      label: this.tr("Work Address"),
      valueKey: 'work_address'
    },
  ];

  noItemsMessageLanguages = this.tr("No languages.");
  headerConfLanguages = [
    {
      label: this.tr("Language"),
      valueKey: '__str__'
    },
    {
      label: this.tr("Level"),
      valueKey: 'level',
      useStr: true,
    },
  ];

  constructor(protected translationService: TranslationService, private personalInfoService: PersonalInfoService,
              private loadingService: LoadingService, private educationService:EducationService,
              private professionalExperienceService:ProfessionalExperiencesService, private nationalityService:NationalityService,
              private workingContactsService:WorkingContactsService, private languagesService:LanguageService
  ) {
    super(translationService);
  }

  ngOnInit() {
  }

  loadPersonalInfo(id: number) {
    this.loadingService.show();
    this.personalInfoService.getPersonalInfo(id).subscribe((personalinfo) => {
      this.personalinfo = personalinfo;
      this.loadingService.hide();
    });
  }

  openModal(){
    this.loadPersonalInfo(this.object_id);
    this.personalInfoModal.open();
  }

  /**
   * Educations
   */

  loadEducations(page:number, results:number) {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.object_id;
    query['page'] = page;
    query['results'] = results;
    return this.educationService.getEducations(query);
  }

  /**
   * ProfessionalExperiences
   */

  loadProfessionalExperiences(page:number, results:number) {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.object_id;
    query['page'] = page;
    query['results'] = results;
    return this.professionalExperienceService.getProfessionalExperiences(query);
  }

  /**
   * Nationalities
   */

  loadNationalities(page:number, results:number) {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.object_id;
    query['page'] = page;
    query['results'] = results;
    return this.nationalityService.getNationalities(query);
  }

  /**
   * Working Contacts
   */

  loadWorkingContacts(page:number, results:number) {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.object_id;
    query['page'] = page;
    query['results'] = results;
    return this.workingContactsService.getWorkingContacts(query);
  }

  /**
   * Languages
   */

  loadLanguages(page:number, results:number) {
    let query = {};
    query['is_active'] = true;
    query['user_id'] = this.object_id;
    query['page'] = page;
    query['results'] = results;
    return this.languagesService.getLanguages(query);
  }
}
