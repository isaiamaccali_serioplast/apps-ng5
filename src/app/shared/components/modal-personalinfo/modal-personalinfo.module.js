"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var shared_module_1 = require("../../shared.module");
var hr_profiles_service_1 = require("../../../apps/_common/services/hr-profiles.service");
var hr_employees_view_component_1 = require("./hr-employees-view.component");
var HrEmployeesViewModule = (function () {
    function HrEmployeesViewModule() {
    }
    return HrEmployeesViewModule;
}());
HrEmployeesViewModule = __decorate([
    core_1.NgModule({
        imports: [shared_module_1.SharedModule],
        declarations: [hr_employees_view_component_1.HrEmployeesViewComponent],
        exports: [hr_employees_view_component_1.HrEmployeesViewComponent],
        providers: [hr_profiles_service_1.HRProfileService] // services
    })
], HrEmployeesViewModule);
exports.HrEmployeesViewModule = HrEmployeesViewModule;
