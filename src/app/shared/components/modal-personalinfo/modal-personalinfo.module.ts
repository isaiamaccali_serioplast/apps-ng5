
import {SharedModule} from "../../shared.module";
import {ModalPersonalInfoComponent} from "./modal-personalinfo.component";
import {NgModule} from "@angular/core";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [ModalPersonalInfoComponent],   // components and directives
  exports: [ModalPersonalInfoComponent],
  providers: []                    // services
})
export class ModalPersonalInfoModule {
}


