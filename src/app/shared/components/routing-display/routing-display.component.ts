import {Component, Input} from "@angular/core";
import {Routing} from "../../../apps/_common/services/routings.service";

@Component({
  selector: 'routing-display',
  templateUrl: 'routing-display.component.html',
  styleUrls: ['routing-display.component.css']
})
export class RoutingDisplayComponent{

  @Input('routing') routing:Routing;

  constructor() {}

  ngOnInit() {

  }

}


