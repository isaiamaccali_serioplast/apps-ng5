import {NgModule} from "@angular/core";
import {RoutingDisplayComponent} from "./routing-display.component";
import {I18NModule} from "../../i18n/i18n.module";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";

@NgModule({
  imports: [I18NModule,CommonModule,Angular2MaterialModule],       // module dependencies
  declarations: [RoutingDisplayComponent],   // components and directives
  exports:[RoutingDisplayComponent],
  providers: []                    // services
})
export class RoutingDisplayModule { }


