import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {TranslationService} from "../../i18n/translation.service";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {HeaderConf, TableWithPagesComponent} from "../../material/table-with-pages/table-with-pages.component";
import {ProjectTask, ProjectTasksService} from "../../../apps/_common/services/project-task.service";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'wbs-search',
  templateUrl: 'wbs-search.component.html',
  styleUrls: ['wbs-search.component.css']
})
export class WBSSearchComponent extends TranslatableComponent {

  @Input('wbs') task:ProjectTask = new ProjectTask();
  @Input('resetOnSelection') resetOnSelection = false;
  @Input('label') label = this.tr('WBS');
  @Input('additionalQuery') additionalQuery = {};
  @Output('wbsSelected') wbsSelected = new EventEmitter<ProjectTask>();
  @ViewChild('dialog') dialog:DialogComponent;
  @ViewChild('table') table:TableWithPagesComponent;

  headerConf:HeaderConf[] = [
    {
      valueKey:'project',
      useStr:true,
      label:"Project"
    },
    {
      valueKey:'name',
      label:"Name"
    },
    {
      valueKey:'sap_name',
      label:"Code"
    },
    {
      valueKey:'status',
      useStr:true,
      label:"Status"
    }

  ];

  search:string;

  constructor(protected translationService: TranslationService, private projectTasksService:ProjectTasksService) {
    super(translationService);
  }

  ngOnInit(){
    if(!this.task)
      this.task = new ProjectTask();
  }

  reset(){
    this.task = new ProjectTask();
    this.wbsSelected.emit(this.task);
  }

  openDialog(){
    this.search = "";
    this.dialog.open();
  }

  select(t:ProjectTask){
    this.dialog.close();
    this.task = t;
    this.wbsSelected.emit(t);
    if(this.resetOnSelection)
      this.task = new ProjectTask();
  }

  getTasks(page:number,results:number){
    let query = {
      page:page,
      results:results,
      ordering:'project__name',
      no_hierarchy:true,
      is_active: true,
    };

    if(this.additionalQuery){
      Object.assign(query, this.additionalQuery);
    }

    if(this.search)
      query['search'] = this.search;

    return this.projectTasksService.getProjectTasks(query);
  }

  timeout:any;
  doSearchByInput(){
    if(this.timeout)
      clearTimeout(this.timeout);
    this.timeout = setTimeout(()=>{
      this.table.reload();
    },500)
  }
}
