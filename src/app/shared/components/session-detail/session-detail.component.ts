import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {DialogComponent} from "../../material/dialog/dialog.component";
import {GroupingFactor, MeasureSession, MeasureSessionSample, MeasureSessionSampleFactorLevel, MeasureSessionService, SampleMeasure} from "../../../apps/_common/services/measure-session.service";
import {MessageService} from "../../material/message/message.service";
import {PropertiesService, Property} from "../../../apps/_common/services/properties.service";
import {ContentType, ContentTypeService} from "../../../apps/_common/services/content-type.service";
import {StatusLink, StatusLinkService} from "../../../apps/_common/services/status-link.service";
import {ControlPlanMeasure, ControlPlanMeasuresService} from "../../../apps/_common/services/control-plan-measures.service";
import {arrayDiff, arrayDistinct, buildErrorMessage, cloneObject, parseSerionetRespToData} from "../../utils";
import {TranslationService} from "../../i18n/translation.service";
import {LoadingService} from "../../loading/loading.service";
import {FactorLevel, FactorLevelsService} from "../../../apps/_common/services/factor-levels.service";
import {CMMParser, CMMParsersService} from "../../../apps/_common/services/cmm-parsers.service";
import {CMMFileParserService, CMMParserOutput, CMMParserOutputMatch, CMMParserOutputMatchProperty, CMMParserOutputMatchSample} from "../../../apps/_common/services/cmm-file-parser.service";
import {AppsService} from "../../../apps/apps.service";
import {SerionetGenericObjectService} from "../../../apps/_common/services/serionet-generic-objects.service";
import {PropertySetService} from "../../../apps/_common/services/property-set.service";
import {ReportsComponent} from "../reports/reports.component";
import {TranslatableComponent} from "../base-component/translatable.component";

class ImportError {
  property: number;
  comb: any[];
  overflow: number;
}

@Component({
  selector: 'sp-session-detail',
  templateUrl: 'session-detail.component.html',
  styleUrls: ['session-detail.component.css']
})
export class SessionDetailComponent extends TranslatableComponent implements OnInit {

  @Input('session') sessionInput: MeasureSession;
  @Input() viewMode: boolean = false;
  @Input() hideHeader = false;
  @Input() hideSide = false;
  @Input() hideFresh = false;

  @ViewChild('sampleNotes') sampleNotes: DialogComponent;
  @ViewChild('measureNotes') measureNotes: DialogComponent;
  @ViewChild('sampleGroupingFactors') sampleGroupingFactors: DialogComponent;
  @ViewChild('factorLevelsDialog') factorLevelsDialog: DialogComponent;
  @ViewChild('importDialog') importDialog: DialogComponent;
  @ViewChild('importMatchDialog') importMatchDialog: DialogComponent;
  @ViewChild('measuresDiv') measuresDiv: HTMLDivElement;
  @ViewChild('importInput') importInput: HTMLInputElement;
  @ViewChild('fileForm') fileForm: HTMLFormElement;
  @ViewChild('sampleGroupingFactorsShow') sampleGroupingFactorsShow: DialogComponent;
  @ViewChild('editTypeDialog') editTypeDialog: DialogComponent;
  @ViewChild('saveTypeDialog') saveTypeDialog: DialogComponent;
  @ViewChild('dialogAfterSave') dialogAfterSave: DialogComponent;
  @ViewChild('reportsComponent') reportsComponent: ReportsComponent;
  @ViewChild('dialogErrorsImport') dialogErrorsImport: DialogComponent;

  @Output() back = new EventEmitter();

  session: MeasureSession;
  sampleToAnnotate: MeasureSessionSample;
  measureToAnnotate: SampleMeasure;
  measureToAnnotateSample: MeasureSessionSample;
  edited: boolean = false;
  overrideMode: boolean = false;
  showOverride = false;
  statusLinkSessionCompleted = new StatusLink();
  statusLinkSessionWorking = new StatusLink();
  statusLinkSessionClosedCompleted = new StatusLink();
  showNotes = false;
  panelCollapsed = false;
  sampleToIdentify: MeasureSessionSample;
  sampleFactors: MeasureSessionSampleFactorLevel[][] = [];

  parsers: CMMParser[] = [];
  selectedParser: number;
  importResponse: CMMParserOutput;
  match: CMMParserOutputMatch;

  sampleToShow: MeasureSessionSample;
  reportTags: number[] = [];
  reportsConfig: any = [];

  allCompleted = false;

  importErrors: ImportError[] = [];

  constructor(private sessionsService: MeasureSessionService, private messageService: MessageService,
              private propertyService: PropertiesService, private propertySetService: PropertySetService,
              private contentTypeService: ContentTypeService, private statusLinkService: StatusLinkService,
              private controlPlanMeasureService: ControlPlanMeasuresService, protected translationService: TranslationService,
              private loadingService: LoadingService, private cd: ChangeDetectorRef, private factorLevelsService: FactorLevelsService,
              private parserService: CMMParsersService, private fileParserService: CMMFileParserService,
              private serionetGenericObjService: SerionetGenericObjectService,
              private appsService: AppsService) {
    super(translationService);
  }

  ngOnInit() {

    let count = 2;
    this.loadingService.show();
    let callback = () => {
      count--;
      if (count == 0) {
        let fixDataStatus = () => {
          this.session.measuresessionsample_set.forEach((s) => {
            this.checkSampleAsIdentified(s);
          });
          this.session.controlplansessionmeasure_set.forEach((cpm) => {
            this.calcGlobalPropertyStatus(cpm.property.pk);
          })
        };
        if (this.sessionInput.pk) {
          this.sessionsService.getSession(this.sessionInput.pk).subscribe((s) => {
            this.fixSession(s, () => {
              fixDataStatus();
              this.loadingService.hide();
            });
          });
        }
        else {
          this.fixSession(this.sessionInput, () => {
            fixDataStatus();
            this.loadingService.hide();
          });
        }
      }
    };

    this.parserService.getCMMParsers({is_active: true}).subscribe((resp) => {
      this.parsers = parseSerionetRespToData(resp);
      callback();
    });

    this.contentTypeService.getContentTypes({model: 'measuresession', is_active: true}).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let ct: ContentType = data['results'][0];
        this.statusLinkService.getStatusLinks({app: ct.pk, is_active: true}).subscribe((data) => {
          if (data && data['count'] && data['count'] > 0) {
            data['results'].forEach((sl: StatusLink) => {
              if (sl.delta_rel == 0)
                this.statusLinkSessionWorking = sl;
              else if (sl.delta_rel == 1)
                this.statusLinkSessionCompleted = sl;
              else if (sl.delta_rel == 2)
                this.statusLinkSessionClosedCompleted = sl;
            });
            callback();
          }
        });
      }
    });
  }

  fixSession(session: MeasureSession, fixSessionCallback?: any) {
    if (session.status.pk == this.statusLinkSessionWorking.pk) {
      session.status = this.statusLinkSessionWorking;
    } else if (session.status.pk == this.statusLinkSessionCompleted.pk) {
      session.status = this.statusLinkSessionCompleted;
    } else if (session.status.pk == this.statusLinkSessionClosedCompleted.pk) {
      session.status = this.statusLinkSessionClosedCompleted;
    }

    this.controlPlanMeasureService.getControlPlanMeasures({
      control_plan: session.control_plan.pk,
      is_active: true
    }).subscribe((data) => {
      let measures = parseSerionetRespToData(data);

      measures.sort((a: ControlPlanMeasure, b: ControlPlanMeasure) => {
        return a.delta - b.delta;
      });
      let count = measures.length;
      let callback = () => {
        count--;
        if (count == 0) {
          if (measures.length > 0) {
            session.controlplansessionmeasure_set = measures;

            if (session.measuresessionsample_set && session.measuresessionsample_set.length > 0) {
              session.measuresessionsample_set.sort((mss1, mss2) => {
                return mss1.index - mss2.index
              });
              session.measuresessionsample_set.forEach((s: MeasureSessionSample) => {
                let current_measures: SampleMeasure[] = cloneObject(s.samplemeasure_set);
                s.samplemeasure_set = [];
                session.controlplansessionmeasure_set.forEach((cpm) => {
                  let m = new SampleMeasure();
                  m.property = cpm.property;
                  if (!m.property.calculated) {
                    let match = current_measures.filter((sm: SampleMeasure) => {
                      return m.property.pk == parseInt(sm.property + "", 10)
                    });
                    if (match && match.length > 0) {
                      let savedMeasure = match[0];
                      m.pk = savedMeasure.pk;
                      m.value = savedMeasure.value;
                      m.overridden_value = savedMeasure.overridden_value;
                      m.notes = savedMeasure.notes;
                      m.delta = cpm.delta;
                    }
                    s.samplemeasure_set.push(m);
                  }
                });
                let selectedFL = s.measuresessionsamplefactorlevel_set;
                s.measuresessionsamplefactorlevel_set = [];
                session.groupingfactor_set.forEach((gf) => {
                  let flPKs = gf.factorlevel_set.map((fl) => fl.pk);
                  let found = false;
                  selectedFL.forEach((sfl) => {
                    if (flPKs.indexOf(sfl.factor_level) != -1) {
                      s.measuresessionsamplefactorlevel_set.push(sfl);
                      found = true;
                    }
                  });
                  if (!found) {
                    let sfl = new MeasureSessionSampleFactorLevel();
                    s.measuresessionsamplefactorlevel_set.push(sfl);
                  }
                })
              });
            }
            let samples = session.measuresessionsample_set;
            session.measuresessionsample_set = [];
            this.session = session;
            this.computeFactors();
            this.checkFactorLevelUsage();
            this.loadingService.show();
            this.addSamplesBatch(samples, 0, () => {
              this.loadingService.hide();
              if (this.session.measuresessionsample_set.length == 0) {
                this.initializeSamples(this.session);
              }
              this.checkFactorLevelUsage();
              if (fixSessionCallback)
                fixSessionCallback.call(this);
            })
          }
          else {
            this.messageService.open({
              title: this.tr('ERROR'),
              error: true,
              message: this.tr('INVALID_CONTROL_PLAN'),
              autoClose: true,
              onClose: () => {
                this.back.emit();
              }
            });
          }

        }
      };
      measures.forEach((m: ControlPlanMeasure) => {
        delete m.pk;
        delete m.__str__;
        delete m.control_plan;
        session.controlplansessionmeasure_set.push(m);
        this.propertySetService.getPropertySets({
          material: session.material.pk,
          property: m.property.pk,
          is_active: true
        }).subscribe((resp) => {
          let matchingPS = parseSerionetRespToData(resp);
          if (matchingPS.length > 0) {
            this.propertyService.getProperty(m.property.pk).subscribe((p: Property) => {
              m.property = p;
              callback();
            })
          }
          else {
            measures.splice(measures.indexOf(m), 1);
            callback();
          }
        });

      });
    })
  }

  initializeSamples(session: MeasureSession) {
    let repeats = session.controlplansessionmeasure_set.filter((cpm) => !cpm.property.calculated).map((cpm) => cpm.repeats);
    let min_repeats = Math.max.apply(Math, repeats);
    let nCavities = this.getFactorsCombinationNumber();
    let nSamplesToCreate = nCavities * min_repeats;
    this.loadingService.show();
    this.processInitialSamples(0, nSamplesToCreate);
  }

  processInitialSamples(index: number, nSamples) {
    if (index < nSamples) {
      this.addColumn(true);
      if (index % 20 == 0) {
        setTimeout(() => {
          this.processInitialSamples(index + 1, nSamples);
        }, 200)
      }
      else {
        this.processInitialSamples(index + 1, nSamples);
      }

    }
    else {
      this.loadingService.hide();
    }

  }

  addSamplesBatch(samples, index, callback) {
    if (samples[index]) {
      setTimeout(() => {
        this.session.measuresessionsample_set.push(samples[index]);
        this.addSamplesBatch(samples, index + 1, callback);
      }, 100)
    }
    else {
      callback.call(this);
    }
  }

  saveAndSuspend() {
    this.save();
  }

  saveAndArchive() {
    if (this.session.status.pk == this.statusLinkSessionWorking.pk) {
      this.messageService.open({
        title: this.tr('ATTENTION'),
        message: this.tr('SESSION_MARKED_AS_CLOSED_COMPLETED'),
        confirm: () => {
          this.session.status = cloneObject(this.statusLinkSessionClosedCompleted);
          this.save();
        },
        deny: () => {
        }
      });
    }
    else {
      this.session.status = cloneObject(this.statusLinkSessionClosedCompleted);
      this.save();
    }
  }

  save() {
    this.saveTypeDialog.close();
    this.loadingService.show();
    this.sessionsService.saveSession(this.session).subscribe((resp) => {
      this.loadingService.hide();
      if (resp.pk) {

        if (!this.session.pk)
          this.session.pk = resp.pk;
        this.dialogAfterSave.open();
        // this.back.emit();
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    });
  }

  addColumn(forceAdd?: boolean) {
    if (this.allowAdd() || forceAdd) {
      let sample = new MeasureSessionSample();
      sample.samplemeasure_set = [];
      sample.is_active = true;
      if (this.session.measuresessionsample_set.length > 0)
        sample.index = Math.max.apply(Math, this.session.measuresessionsample_set.map((s) => s.index)) + 1;
      else
        sample.index = 1;
      for (let m of this.session.controlplansessionmeasure_set) {
        let measure = new SampleMeasure();
        measure.property = m.property;
        if (!m.property.calculated)
          sample.samplemeasure_set.push(measure);
      }
      sample.measuresessionsamplefactorlevel_set = [];
      this.session.groupingfactor_set.forEach(() => {
        let mssfl = new MeasureSessionSampleFactorLevel();
        sample.measuresessionsamplefactorlevel_set.push(mssfl);
      });
      this.session.measuresessionsample_set.push(sample);
      this.cd.detectChanges();
      setTimeout(() => {
        if (!forceAdd) {
          let div = this.measuresDiv ['nativeElement'];
          div.scrollLeft = div.scrollWidth + div.offsetWidth;
        }
      }, 200)
    }
  }

  allowAdd() {
    let samples = this.getActiveSamples();
    if (samples.length == 0) {
      return true;
    }
    else {
      let lastSample = samples[samples.length - 1];
      return this.isSampleIdentified(lastSample);
    }
  }

  calcInsertedMeasures(factorLevelSet: MeasureSessionSampleFactorLevel[], pId: number) {
    let count = 0;
    if (factorLevelSet) {
      for (let s of this.getSimilarSamples(factorLevelSet)) {
        count = this.countMeasures(count, s, pId);
      }
    }
    else {
      for (let s of this.getActiveSamples()) {
        count = this.countMeasures(count, s, pId);
      }
    }
    return count;
  }

  getSimilarSamples(factorLevelSet: MeasureSessionSampleFactorLevel[]) {
    let samples = this.getActiveSamples();
    return samples.filter((s: MeasureSessionSample) => {
      let match = true;
      if (this.session.groupingfactor_set.length > 0) {
        s.measuresessionsamplefactorlevel_set.forEach((sfl, index) => {
          if (sfl.factor_level != factorLevelSet[index].factor_level) {
            match = false;
          }
        });
      }
      return match;
    });
  }

  countMeasures(count: number, s: MeasureSessionSample, pId: number) {
    for (let m of s.samplemeasure_set) {
      if (m.property.pk == pId) {
        if (m.value != null) {
          count++;
        }
      }
    }
    return count;
  }

  checkPropertyCompleted(pId: number) {
    return this.session.controlplansessionmeasure_set.filter((cpm) => cpm.property.pk == pId)[0].is_complete;
    // if (!this.checkPropertyOverflow(pId)) {
    //   let isCompleted = true;
    //   for (let m of this.session.controlplansessionmeasure_set) {
    //     if (!m.property.calculated) {
    //       if (m.property.pk == pId) {
    //         let inserted = this.calcInsertedMeasures(null, m.property.pk);
    //         if (inserted < (m.repeats * this.getFactorsCombinationNumber())) {
    //           isCompleted = false;
    //         }
    //       }
    //     }
    //   }
    //   return isCompleted;
    // }
    // else {
    //   return false;
    // }

  }

  checkAllCompleted() {

    if (this.session.status.pk == this.statusLinkSessionClosedCompleted.pk) {
      this.allCompleted = true;
    }
    else {

      let allCompleted = true;

      this.session.controlplansessionmeasure_set.forEach((cpm) => {
        if (!cpm.property.calculated && !cpm.is_complete)
          allCompleted = false;
      });

      //
      //
      // let incompletedMeasures = this.session.controlplansessionmeasure_set.length;
      // if (incompletedMeasures == 0) {
      //   return false;
      // }
      // else {
      //   for (let m of this.session.controlplansessionmeasure_set) {
      //     if (!m.property.calculated) {
      //       if ((this.calcInsertedMeasures(null, m.property.pk) >= m.repeats * (this.getFactorsCombinationNumber()))) {
      //         incompletedMeasures--;
      //       }
      //     }
      //     else {
      //       incompletedMeasures--;
      //     }
      //   }
      //   if (incompletedMeasures == 0) {
      //     this.session.status = this.statusLinkSessionCompleted;
      //   }
      //   else {
      //     this.session.status = this.statusLinkSessionWorking;
      //   }

      this.allCompleted = allCompleted;

      this.checkSessionStatus();
      // }
    }
    // return this.allCompleted;
  }

  disableInput(s: MeasureSessionSample, m: SampleMeasure) {
    if (this.session.groupingfactor_set.length == 0) {
      return false;
    }
    else {
      if (this.isSampleIdentified(s)) {
        if (this.checkPropertyCompleted(m.property.pk) && m.value == null) {
          return true;
        }
        else {
          if (this.checkPropertyOverflow(m.property.pk)) {
            return false;
          }
          else {
            let repeats = 0;
            for (let cm of this.session.controlplansessionmeasure_set) {
              if (cm.property.pk == m.property.pk) {
                repeats = cm.repeats;
              }
            }
            let inserted = this.calcInsertedMeasures(s.measuresessionsamplefactorlevel_set, m.property.pk);

            if (inserted > repeats) {
              return true;
            }
            else {
              return ((inserted == repeats) && (m.value == null));
            }
          }

        }
      }
      else {
        return true;
      }
    }
  }

  edit() {
    this.editTypeDialog.open();
  }

  saveSession() {
    if (this.session.status.delta_rel != 2)
      this.saveTypeDialog.open();
    else {
      this.save();
    }
  }

  editStandard() {
    this.editTypeDialog.close();
    this.viewMode = false;
  }

  editOverride() {
    this.editTypeDialog.close();

    this.overrideMode = true;
    this.showOverride = true;
    this.viewMode = false;
  }

  askOverride(m: SampleMeasure) {
    if (this.overrideMode && !m.overridden_value) {
      this.messageService.open({
        confirm: () => {
          this.showOverride = true;
          m.overridden_value = m.value;
        },
        deny: () => {
        },
        title: 'ATTENTION',
        message: 'CONFIRM_OVERRIDE'
      });
    }
  }

  updateModel(value: any, m: SampleMeasure) {
    if (this.showOverride) {
      m.overridden_value = value;
    }
    else {
      m.value = value;
    }
    this.edited = true;
    this.calcGlobalPropertyStatus(m.property.pk);
    this.checkAllCompleted();
  }

  isShowingOverride(m: SampleMeasure) {
    return this.showOverride && m.overridden_value != null;
  }

  annotateSample(s: MeasureSessionSample) {
    this.sampleToAnnotate = s;
    this.sampleNotes.open();
  }

  annotateMeasure(m: SampleMeasure, s: MeasureSessionSample) {
    this.measureToAnnotate = m;
    this.measureToAnnotateSample = s;
    this.measureNotes.open();
  }

  clearMeasureToAnnotate() {
    this.measureToAnnotate = null;
    this.measureToAnnotateSample = null;
  }

  clearSampleToAnnotate() {
    this.sampleToAnnotate = null;
  }

  parseInt(n) {
    return parseInt(n, 10);
  }

  toFixed(n) {
    return n.toFixed(3);
  }

  doBack() {
    if (!this.viewMode) {
      this.messageService.open({
        confirm: () => {
          this.back.emit();
        },
        deny: () => {
        },
        title: this.tr('ATTENTION'),
        message: this.tr('CONFIRM_BACK_TO_SESSION_LIST')
      });
    }
    else {
      this.back.emit();
    }

  }

  deleteSample(s: MeasureSessionSample) {
    this.messageService.open({
      confirm: () => {
        s.is_active = false;
        this.refreshStatus();
      },
      deny: () => {
      },
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE')
    });
  }

  restoreSample(s: MeasureSessionSample) {
    this.messageService.open({
      confirm: () => {
        s.is_active = true;
        this.refreshStatus();
      },
      deny: () => {
      },
      title: this.tr('ATTENTION'),
      message: this.tr('RESTORE_CONFIRM')
    });
  }

  refreshStatus() {
    this.session.controlplansessionmeasure_set.forEach((cpm) => {
      this.calcGlobalPropertyStatus(cpm.property.pk);
    });
    this.checkAllCompleted();
  }

  getActiveSamples() {
    return this.session.measuresessionsample_set.filter((s) => s.is_active);
  }

  checkPropertyOverflow(propertyPK: number) {
    return this.session.controlplansessionmeasure_set.filter((cpm) => cpm.property.pk == propertyPK)[0].is_overflow;
    // let inserted = 0;
    // let total = 0;
    // let overFlows = false;
    // for (let m of this.session.controlplansessionmeasure_set) {
    //   if (!m.property.calculated) {
    //     if (m.property.pk == propertyPK) {
    //       total = m.repeats;
    //       this.sampleFactors.forEach((f) => {
    //         inserted = this.calcInsertedMeasures(f, propertyPK);
    //         if (inserted > total) {
    //           overFlows = true;
    //         }
    //       });
    //     }
    //   }
    // }
    // return overFlows;
  }

  canSave() {
    if (this.session) {
      let canS = true;
      for (let m of this.session.controlplansessionmeasure_set) {
        if (!m.property.calculated) {
          // let ov = this.checkPropertyOverflow(m.property.pk);
          if (m.is_overflow) {
            canS = false;
          }
        }
      }
      return canS;
    }
  }

  calcPropertyStatus(propertyPK: number, repeats: number, fls: MeasureSessionSampleFactorLevel[]) {
    let totalMeasures = 0;
    let totalOverflow = 0;
    let measuresTotal;
    if (!fls) {
      this.sampleFactors.forEach((f) => {
        let insertedMeasures = this.calcInsertedMeasures(f, propertyPK);
        if (insertedMeasures <= repeats) {
          totalMeasures += insertedMeasures;
        }
        else {
          totalMeasures += repeats;
          totalOverflow += (insertedMeasures - repeats);
        }
      });
      measuresTotal = this.getFactorsCombinationNumber() * repeats;
    }
    else {
      let insertedMeasures = this.calcInsertedMeasures(fls, propertyPK);
      if (insertedMeasures <= repeats) {
        totalMeasures += insertedMeasures;
      }
      else {
        totalMeasures += repeats;
        totalOverflow += (insertedMeasures - repeats);
      }
      measuresTotal = repeats;
    }

    return totalMeasures + "/" + measuresTotal + (totalOverflow > 0 ? " (" + (totalOverflow + "/" + measuresTotal) + " overflow)" : '')
  }

  calcGlobalPropertyStatus(propertyPK: number) {
    let match = this.session.controlplansessionmeasure_set.filter((cpm) => cpm.property.pk == propertyPK)[0];
    let repeats = match.repeats;
    let totalMeasures = 0;
    let totalOverflow = 0;
    this.sampleFactors.forEach((f) => {
      let insertedMeasures = this.calcInsertedMeasures(f, propertyPK);
      if (insertedMeasures <= repeats) {
        totalMeasures += insertedMeasures;
      }
      else {
        totalMeasures += repeats;
        totalOverflow += (insertedMeasures - repeats);
      }
    });
    let measuresTotal = this.getFactorsCombinationNumber() * repeats;

    match.progress = totalMeasures + "/" + measuresTotal;
    match.overflow = (totalOverflow > 0 ? " (" + (totalOverflow + "/" + measuresTotal) + " overflow)" : '');

    match.is_complete = totalMeasures >= measuresTotal;
    match.is_overflow = totalOverflow > 0
  }

  checkSessionStatus() {
    if (this.session.status.pk != this.statusLinkSessionClosedCompleted.pk) {
      let isCompleted = true;
      this.session.controlplansessionmeasure_set.forEach((m) => {
        if (!m.property.calculated && !m.is_complete)
          isCompleted = false;
      });
      if (isCompleted) {
        this.session.status = this.statusLinkSessionCompleted;
      }
      else {
        this.session.status = this.statusLinkSessionWorking;
      }
    }
  }

  identifySample(s: MeasureSessionSample) {
    this.sampleToIdentify = s;
    this.sampleGroupingFactors.onClose.subscribe(() => {
      this.checkFactorLevelUsage();
    });
    this.sampleGroupingFactors.open();
  }

  checkFactorLevelUsage() {
    this.session.groupingfactor_set.forEach((gf) => {
      gf.factorlevel_set.forEach((fl, i) => {
        if (!fl.pk) {
          gf.factorlevel_set.splice(i, 1);
        }
        else {
          let inUse = false;
          this.session.measuresessionsample_set.forEach((s) => {
            s.measuresessionsamplefactorlevel_set.forEach((sfl) => {
              if (sfl.factor_level == fl.pk) {
                inUse = true;
              }
            });
          });
          fl.inUse = inUse;
        }
        fl.inEdit = false;
        fl.originalName = "";
      })
    });
  }

  isSampleIdentified(s: MeasureSessionSample) {
    let factors = s.measuresessionsamplefactorlevel_set.filter((sl) => sl.factor_level != null).length;
    return factors == this.session.groupingfactor_set.length;
  }

  getFactorsCombinationNumber() {
    let n = 1;
    this.session.groupingfactor_set.forEach((gf) => {
      n *= (gf.factorlevel_set.filter((fl) => fl.active).length);
    });
    return n;
  }

  computeFactors() {
    let gf: GroupingFactor[] = cloneObject(this.session.groupingfactor_set);
    let combinations: FactorLevel[][] = [];
    let groupingIndexes = {};
    gf.forEach((g) => {
      groupingIndexes[g.name] = 0;
      g.factorlevel_set = g.factorlevel_set.filter((fl) => fl.active);
    });
    this.processCombinations(gf, groupingIndexes, combinations);
    this.sampleFactors = combinations.map((c) => {
      return c.map((c_) => {
        let msfl = new MeasureSessionSampleFactorLevel();
        msfl.factor_level = c_.pk;
        return msfl;
      })
    })
  }

  processCombinations(groups: GroupingFactor[], indexStructure: any, combinations: FactorLevel[][]) {
    let tuple = [];
    for (let g of groups) {
      let fl = g.factorlevel_set[indexStructure[g.name]];
      if (fl) {
        tuple.push(fl);
      }
      else {
        let groupIndex = groups.indexOf(g);
        indexStructure[(groups[groupIndex - 1]).name]++;
        for (let i = groupIndex; i < groups.length; i++) {
          indexStructure[groups[i].name] = 0;
        }
      }
    }
    if (tuple.length == groups.length) {
      combinations.push(tuple);
      let lastGroup = groups[groups.length - 1];
      indexStructure[lastGroup.name]++;
    }
    if (indexStructure[groups[0].name] < groups[0].factorlevel_set.length)
      this.processCombinations(groups, indexStructure, combinations);
  }

  showFactorLevels() {
    this.factorLevelsDialog.onClose.subscribe(() => {
      this.checkFactorLevelUsage();
      this.computeFactors();
    });
    this.factorLevelsDialog.open();
  }

  getFactorCombinationRepresentation(comb: MeasureSessionSampleFactorLevel[]) {
    let rep = [];
    comb.forEach((mfl: MeasureSessionSampleFactorLevel) => {
      this.session.groupingfactor_set.forEach((gf) => {
        gf.factorlevel_set.forEach((fl) => {
          if (fl.pk == mfl.factor_level) {
            let r = "<b>" + gf.name + "</b>: " + fl.name;
            rep.push(r);
          }
        })
      })
    });
    return rep.join("; ");
  }

  addFactorLevel(gf: GroupingFactor) {
    let fl = new FactorLevel();
    fl.grouping_factor = gf;
    fl.active = true;
    fl.inUse = false;
    fl.inEdit = true;
    gf.factorlevel_set.push(fl);
  }

  saveFL(gf: GroupingFactor, fl: FactorLevel) {
    this.factorLevelsService.saveFactorLevel(fl).subscribe((resp) => {
      if (resp.pk) {
        gf.factorlevel_set.forEach((f, i) => {
          if (f.name == fl.name) {
            gf.factorlevel_set[i] = resp;
          }
        });
      }
      else {
        resp.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(resp),
          autoClose: false
        });
      }
    })
  }

  undoFLEdit(gf: GroupingFactor, fl: FactorLevel) {
    if (!fl.pk) {
      gf.factorlevel_set.splice(gf.factorlevel_set.indexOf(fl), 1);
    }
    else {
      fl.name = fl.originalName;
      delete fl.originalName;
    }
    fl.inEdit = false;
  }

  checkFactorLevels(gf: GroupingFactor) {
    let activeFactors = gf.factorlevel_set.filter((fl) => fl.active);
    if (activeFactors.length == 1) {
      activeFactors[0].inUse = true;
    }
    else {
      this.checkFactorLevelUsage();
    }
    this.computeFactors();
  }

  editFL(fl: FactorLevel) {
    fl.originalName = fl.name + "";
    fl.inEdit = true
  }

  openImportDialog() {
    this.fileForm['nativeElement'].reset();
    this.selectedParser = null;
    this.importDialog.open();
  }

  uploadFileToParse() {
    this.loadingService.show();
    this.fileParserService.parseFile(this.importInput, this.selectedParser).subscribe((data) => {
      this.loadingService.hide();
      if (data && data.row) {
        this.importResponse = data;
        this.importDialog.close();

        let match = new CMMParserOutputMatch();
        match.properties = [];
        match.samples = [];

        let props = this.getProperties();
        props.forEach((p, i) => {
          let m = new CMMParserOutputMatchProperty();
          m.propertyPK = p.property.pk;
          m.index = i;
          match.properties.push(m);
        });

        let samples = this.getSamples();

        samples.forEach((s) => {
          let m = new CMMParserOutputMatchSample();
          m.sampleIndex = s.index;
          m.index = s.index;
          match.samples.push(m);
        });

        this.match = match;

        this.importMatchDialog.open();
      }
      else {
        data.context = "SERIONET";
        this.messageService.open({
          title: 'Error',
          error: true,
          message: buildErrorMessage(data),
          autoClose: false
        });
      }

    })
  }

  writeImportData() {
    if (this.verifyImportData()) {
      this.importMatchDialog.close();
      this.match.samples.forEach((sm) => {
        if (sm.sampleName) {
          let s = this.session.measuresessionsample_set.filter((s) => s.index == sm.sampleIndex)[0];
          if (s) {
            let sampleData = this.importResponse.data[this.importResponse.col.indexOf(sm.sampleName)];
            let sampleMeasures = s.samplemeasure_set;
            this.match.properties.forEach((pm) => {
              if (pm.propName) {
                let match = sampleMeasures.filter((m) => m.property.pk == pm.propertyPK);
                if (match) {
                  let m = match[0];
                  if (m) {
                    let type = this.session.controlplansessionmeasure_set.find((cpm) => cpm.property.pk == pm.propertyPK).property.type.__str__;
                    let readValue = sampleData[this.importResponse.row.indexOf(pm.propName)];
                    m.value = (type == 'Boolean') ? this.toFixed(readValue) : readValue;
                    this.calcGlobalPropertyStatus(pm.propertyPK);
                  }
                }
              }
            })
          }
        }
      })
      this.checkAllCompleted();
    }
  }

  verifyImportData() {
    //Controllo properties
    let repeatsArray = this.match.properties
      .filter((pm) => {
        return pm.propName;
      })
      .map((pm) => pm.propertyPK)
      .map((ppk) => this.session.controlplansessionmeasure_set.filter((cpm) => cpm.property.pk == ppk)[0].repeats);

    if (arrayDistinct(repeatsArray).length > 1) {
      //le proprietà selezionate hanno repeats differenti, la selezione è invalida
      this.messageService.open({
        title: 'Error',
        error: true,
        message: this.tr("INVALID_PROPERTIES_IMPORT"),
        autoClose: false
      });
      return false;
    }
    else {
      let samples = this.match.samples
        .filter((sm) => {
          return sm.sampleName;
        })
        .map((sm) => sm.sampleIndex)
        .map((si) => this.session.measuresessionsample_set.filter((s) => s.index == si)[0]);

      let similarSamplesCount = [];

      this.sampleFactors.forEach((sflComb) => {
        let matchingSamples = samples.filter((s: MeasureSessionSample) => {
          let match = true;
          if (this.session.groupingfactor_set.length > 0) {
            s.measuresessionsamplefactorlevel_set.forEach((sfl, index) => {
              if (sfl.factor_level != sflComb[index].factor_level) {
                match = false;
              }
            });
          }
          return match;
        });
        similarSamplesCount.push(matchingSamples.length);
      });

      let repeats = repeatsArray[0];

      let okSamples = similarSamplesCount.filter((c) => c <= repeats); //quanti gruppi hanno una cadinalità minore o uguale alle ripetizioni attese

      if (okSamples.length == similarSamplesCount.length) {
        //Estraggo i campioni matchati
        let samples = this.match.samples
          .filter((sm) => {
            return sm.sampleName;
          })
          .map((sm) => this.session.measuresessionsample_set.filter((s) => s.index == sm.sampleIndex)[0]);

        //Ne estraggo le definizioni
        let selectedSamplesComb = [];
        samples.forEach((s) => {
          let fact = s.measuresessionsamplefactorlevel_set;
          let fl = fact.map((fl) => fl.factor_level);
          selectedSamplesComb.push(fl);
        });

        let samplesIndexes = samples.map((s) => s.index);

        //Faccio una distinct
        selectedSamplesComb.forEach((c, index) => {
          selectedSamplesComb.forEach((c_, index_) => {
            if (arrayDiff(c, c_).length == 0 && (index != index_)) {
              selectedSamplesComb.splice(index, 1);
            }
          })
        });

        selectedSamplesComb = selectedSamplesComb.map((comb) => comb.map((c) => {
          return {factor_level: c}
        }));

        let selectedProps = this.match.properties
          .filter((pm) => {
            return pm.propName;
          })
          .map((pm) => pm.propertyPK);

        this.importErrors = [];
        selectedSamplesComb.forEach((comb) => {
          selectedProps.forEach((pPK) => {
            let ss = this.getSimilarSamples(comb);
            let count = 0;
            ss.forEach((s) => {
              if (samplesIndexes.indexOf(s.index) == -1) {
                let m = s.samplemeasure_set.filter((m) => m.property.pk == pPK)[0];
                if (m.value)
                  count++;
              }
            });
            //count è il numero di misure inserite nello stesso gruppo oltre quelli selezionati
            let cpRepeats = this.session.controlplansessionmeasure_set.filter((cpm) => cpm.property.pk == pPK)[0].repeats;
            if ((count + repeats) > cpRepeats) {
              this.importErrors.push({property: pPK, comb: comb, overflow: (count + repeats - cpRepeats)});
            }
          });
        });

        if (this.importErrors.length == 0) {
          return true;
        }
        else {
          this.openErrorsImport();
          return false;
        }
      }
      else {
        //Almeno un tipo di campioni selezionati ha una cardinalità maggiore alle ripetizioni previste,
        //l'importazione causerebbe sicuramente un overflow

        this.messageService.open({
          title: 'Error',
          error: true,
          message: this.tr("INVALID_SAMPLES_IMPORT"),
          autoClose: false
        });
        return false;
      }
    }
  }

  openErrorsImport() {
    this.dialogErrorsImport.open();
  }

  getUnmatchedProperties() {
    return this.importResponse.row.filter((p) => {
      let setPKS = this.match.properties.map((p_) => p_.propName).filter((n) => n != null);
      return setPKS.indexOf(p) == -1;
    });
  }

  getProperties() {
    return this.session.controlplansessionmeasure_set.filter((cpm) => !cpm.property.calculated);
  }

  getUnIdentifiedSamplesNumber() {
    return this.session.measuresessionsample_set.filter((sample) => {
      return !this.isSampleIdentified(sample)
    }).length;
  }

  getUnmatchedSamples() {
    return this.importResponse.col.filter((s) => {
      let setPKS = this.match.samples.map((s) => s.sampleName).filter((n) => n != null);
      return setPKS.indexOf(s) == -1;
    });
  }

  getSamples() {
    return this.session.measuresessionsample_set.filter((sample) => {
      return this.isSampleIdentified(sample);
    });
  }

  showSampleDetails(sampleIndex: number) {
    this.sampleToShow = this.session.measuresessionsample_set.filter((s) => s.index == sampleIndex)[0];
    this.sampleGroupingFactorsShow.open();
  }

  //Funzione usata per ottimizzare il rendering delle colonne
  trackBySampleId(index, item: MeasureSessionSample) {
    return item.index;
  }

  stay() {
    let u = this.dialogAfterSave.onClose.subscribe(() => {
      u.unsubscribe();
      if (this.session.status.pk != this.statusLinkSessionClosedCompleted.pk) {

        this.loadingService.show();
        this.sessionsService.getSession(this.session.pk).subscribe((session) => {
          this.loadingService.hide();
          this.fixSession(session, () => {
            this.editStandard();
          });
        });
      }
      else {
        this.viewMode = true;
      }
    });
    this.dialogAfterSave.close();
  }

  isSessionWorking() {
    return this.session.status.pk == this.statusLinkSessionWorking.pk;
  }

  backToList() {
    let u = this.dialogAfterSave.onClose.subscribe(() => {
      u.unsubscribe();
      this.back.emit();
    });
    this.dialogAfterSave.close();
  }

  setReportsArgs(s) {
    this.reportTags = [];
    this.reportsConfig = [];
    let externalAppObj = this.appsService.getAppData("seriolab");
    // TAGS = [Seriolab]
    this.serionetGenericObjService.getSerionetGenericObjects({
      content_type: 110,
      object_id: externalAppObj['pk'],
      is_active: true
    }).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        this.reportTags.push(data['results'][0]['pk']);
      }

      let SERIOLAB_SESSION_STANDARD_REPORT = {
        python_func: 'SeriolabSessionStandard',
        params:
          [
            {
              name: "main_grouping_factor",
              label: "Select grouping factor for analysis",
              disabled: false,
              type: "select",
              value: null,
              value_field_display: 'name', // default __str__
              results: s.groupingfactor_set,
              required: true,
              visible: true,
            }
          ],
        filters:
          [
            {
              name: "sample__session",
              label: "measuresession__id",
              is_disabled: false,
              type: "text",
              value: s.pk,
              visible: false,
            },
            {
              name: "control_plan",
              label: "control_plan__id",
              is_disabled: false,
              type: "text",
              value: s.control_plan.pk,
              visible: false,
            },
            {
              name: "sample__session__external",
              label: "measuresession__external",
              is_disabled: false,
              type: "boolean",
              value: false,
              visible: false,
            },
            {
              name: "created_on__range",
              label1: "created_on (start)",
              label2: "created_on (end)",
              is_disabled: false,
              type: "datetimerange",
              value1: "",
              value2: "",
              visible: false,
            },
          ],
      };
      let SERIOLAB_SESSION_INTERNAL_REPORT = {
        python_func: 'SeriolabInternalReport',
        params:
          [
            {
              name: "main_grouping_factor",
              label: "Select grouping factor for analysis",
              disabled: false,
              type: "select",
              value: null,
              value_field_display: 'name', // default __str__
              results: s.groupingfactor_set,
              required: true,
              visible: true,
            }
          ],
        filters:
          [
            {
              name: "sample__session",
              label: "measuresession__id",
              is_disabled: false,
              type: "text",
              value: s.pk,
              visible: false,
            },
            {
              name: "control_plan",
              label: "control_plan__id",
              is_disabled: false,
              type: "text",
              value: s.control_plan.pk,
              visible: false,
            },
            {
              name: "sample__session__external",
              label: "measuresession__external",
              is_disabled: false,
              type: "boolean",
              value: false,
              visible: false,
            },
            {
              name: "created_on__range",
              label1: "created_on (start)",
              label2: "created_on (end)",
              is_disabled: false,
              type: "datetimerange",
              value1: "",
              value2: "",
              visible: false,
            },
          ],
      };

      this.reportsConfig = [SERIOLAB_SESSION_STANDARD_REPORT, SERIOLAB_SESSION_INTERNAL_REPORT];
      this.reportsComponent.init();
    });
  }

  canShowImportButton() {
    return (this.session.status.delta_rel == 0 || this.session.status.delta_rel == 1) && this.canSave() && this.getUnIdentifiedSamplesNumber() == 0 && this.session.measuresessionsample_set.length > 0
  }

  canShowOverrideButton() {
    return this.session.status.delta_rel == 2;
  }

  canShowReports() {
    return this.session && this.session.pk && this.reportTags.length > 0 && this.reportsConfig.length > 0 && this.session.status.delta_rel == 2;
  }

  getSelectValue(m: SampleMeasure) {
    if (this.showOverride) {
      if (m.overridden_value != null)
        return m.overridden_value;
      else
        return m.value;
    }
    else {
      return m.value;
    }
  }

  getCombinationAsString(comb) {
    return comb.map((c) => {
      let asString = "";
      this.session.groupingfactor_set.forEach((gf) => {
        gf.factorlevel_set.forEach((fl) => {
          if (fl.pk == c.factor_level) {
            asString = gf.name + " - " + fl.name;
          }
        })
        //c.factor_level
      });
      return asString;
    }).join("; ")
  }

  keyPressMeasureEvent(event: KeyboardEvent) {
    if (event.code == 'Tab' || event.code == 'Enter') {
      event.preventDefault();
      event.stopImmediatePropagation();
      event.stopPropagation();
    }
    try {
      let cell = event.target['parentElement'].parentElement.parentElement;
      let column: HTMLDivElement = cell.parentElement;
      let cellIndex = [].slice.call(column.querySelectorAll(".cell")).indexOf(event.target['parentElement'].parentElement.parentElement);
      let nextCell = null;
      if (event.code == 'Tab') {

        let container = column.parentElement;
        let columns = [].slice.call(container.querySelectorAll(".column"));
        let columnIndex = columns.indexOf(column);
        let nextColumn = columns[event.shiftKey ? (columnIndex - 1) : (columnIndex + 1)];
        if (nextColumn) {
          nextCell = [].slice.call(nextColumn.querySelectorAll(".cell"))[cellIndex]
        }
      }
      else if (event.code == 'Enter') {
        nextCell = [].slice.call(column.querySelectorAll(".cell"))[cellIndex + 1];
      }

      if (nextCell) {
        if (nextCell.querySelector("input"))
          nextCell.querySelector("input").focus();
      }
    }
    catch (e) {
      console.log(e);
    }
  }

  checkSampleAsIdentified(s: MeasureSessionSample) {
    let isIdentified = true;
    s.measuresessionsamplefactorlevel_set.forEach((fl) => {
      if (!fl.factor_level)
        isIdentified = false;
    });
    s.identified = isIdentified;
  }

  // buildSampleTooltip(s:MeasureSessionSample){
  //   return "ssadsad /r/n <br/>nsjasdjdn   asdasd asd asd asd sad asd  dfdsf"
  // }
  //
  // tooltipTop = null;
  // tooltipLeft = null;
  //
  // handleMouseOverHeader($event){
  //   $event.stopImmediatePropagation();
  //   $event.stopPropagation();
  //   this.tooltipTop = $event.target.offsetTop;
  //   this.tooltipLeft = $event.target.offsetLeft;
  // }
  //
  // getTooltipStyle(){
  //   if(this.tooltipTop && this.tooltipLeft)
  //     return this.sanitizer.bypassSecurityTrustStyle("{top:"+this.tooltipTop+"px,left:"+this.tooltipLeft+"px}");
  // }
}

