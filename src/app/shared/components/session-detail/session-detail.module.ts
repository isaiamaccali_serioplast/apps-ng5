import {NgModule} from "@angular/core";
import {SessionDetailComponent} from "./session-detail.component";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {SharedModule} from "../../shared.module";


@NgModule({
  imports: [SharedModule, SerioplastCommonModule],       // module dependencies
  declarations: [SessionDetailComponent],   // components and directives
  exports:[SessionDetailComponent],
  providers: []                    // services
})
export class SessionDetailModule { }


