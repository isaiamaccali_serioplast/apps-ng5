import {Injectable} from "@angular/core";
import {GenericServiceWithSAP} from "./_generic_with_sap.service";
import {GenericObj, GenericService} from "../../../apps/_common/services/_generic.service";
import {SerionetService} from "../../serionet/service/serionet.service";
import {LoggingService} from "../../logging/logging.service";
import {cloneObject} from "../../utils";

export class Todo extends GenericObj{
  note?: string;
  content_type?: number;
  object_id?: number;
  done?:boolean;

  editing = false;
}

@Injectable()
export class TodosService extends GenericService {

  constructor(protected serionetService: SerionetService, protected loggingService: LoggingService) {
    super(serionetService, loggingService);
    this.init();
  }

  init() {
    this.entityName = "Todo";
    this.serviceURL = "core/todo-list/";
  }

  getTodos(query: any) {
    return this.getList(query);
  }

  saveTodo(i:Todo){
    return this.saveItem(i);
  }

  deleteTodo(i:Todo){
    return this.deleteItem(i);
  }

  clearItem(i:Todo){
    let i_:Todo = cloneObject(i);
    delete i_.editing;
    return i_;
  }
}



