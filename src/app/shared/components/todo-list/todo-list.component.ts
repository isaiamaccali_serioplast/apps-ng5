import {Component, Input, OnInit, SimpleChange} from "@angular/core";
import {parseSerionetRespToData} from "../../utils";
import {LoadingService} from "../../loading/loading.service";
import {MessageService} from "../../material/message/message.service";
import {TranslationService} from "../../i18n/translation.service";
import {Todo, TodosService} from "./todo-list.service";
import {BaseComponent} from "../base-component/base.component";

@Component({
  selector: 'sp-todo-list',
  templateUrl: 'todo-list.component.html',
  styleUrls: ['todo-list.component.css']
})
export class TodoListComponent extends BaseComponent implements OnInit {

  @Input() contentType: number;
  @Input() objectId: number;
  @Input() allowAdd = true;
  todos: Todo[] = [];
  todo: Todo = new Todo();

  constructor(protected translationService: TranslationService, protected messageService: MessageService,
              protected loadingService: LoadingService, private todosService: TodosService) {
    super(translationService, messageService, loadingService);
  }

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.todosService.getTodos({is_active: true, content_type: this.contentType, object_id: this.objectId}).subscribe((resp) => {
      this.todos = parseSerionetRespToData(resp);
    })
  }

  markTodoAsDone(t: Todo) {
    t.done = true;
    this.todosService.saveTodo(t).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.reload();
      }, true)
    })
  }

  markTodoAsUndone(t: Todo) {
    t.done = false;
    this.todosService.saveTodo(t).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.reload();
      }, true)
    })
  }

  editTodo(t: Todo) {
    this.todos.forEach((t) => t.editing = false);
    t.editing = true;
  }

  updateTodo(t: Todo) {
    t.editing = false;
    this.todosService.saveTodo(t).subscribe((resp) => {
      this.manageRespSave(resp, () => {
        this.reload();
      }, true)
    })
  }

  deleteTodo(t: Todo) {

    this.messageService.open({
      confirm: () => {
        this.todosService.deleteTodo(t).subscribe((resp) => {
          this.manageRespDelete(resp, () => {
            this.reload();
          })
        })
      },
      deny: () => {

      },
      title: this.tr('ATTENTION'),
      message: this.tr('CONFIRM_DELETE')
    });

  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['objectId']) {
      this.objectId = changes['objectId'].currentValue;
      this.reload();
    }
  }

  insertNewTodo() {
    if (this.todo.note && this.todo.note.length > 0) {
      this.todo.content_type = this.contentType;
      this.todo.object_id = this.objectId;
      this.todosService.saveTodo(this.todo).subscribe((resp) => {
        this.manageRespSave(resp, () => {
          this.reload();
          this.todo = new Todo();
        }, true)
      });
      return false;
    }
    else {
      return false;
    }
  }
}
