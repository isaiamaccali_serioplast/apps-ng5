import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {SerioplastCommonModule} from "../../../apps/_common/common.module";
import {TodoListComponent} from "./todo-list.component";
import {TodosService} from "./todo-list.service";

@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule, SerioplastCommonModule],       // module dependencies
  declarations: [TodoListComponent],   // components and directives
  exports:[TodoListComponent],
  providers: [TodosService]                    // services
})
export class TodoListModule { }

