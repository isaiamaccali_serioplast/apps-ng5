import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {UserSitesWithSAPComponent} from "./user-sites-with-sap.component";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {UserSitesComponent} from "./user-sites.component";


@NgModule({
  imports: [CommonModule,FormsModule, Angular2MaterialModule],       // module dependencies
  declarations: [UserSitesComponent],   // components and directives
  exports:[UserSitesComponent],
  providers: []                    // services
})
export class UserSitesModule { }

