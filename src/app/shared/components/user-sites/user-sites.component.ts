import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange} from "@angular/core";
import {TranslationService} from "../../i18n/translation.service";
import {arrayIntersect, parseSerionetRespToData} from "../../utils";
import {AppPlantUser, AppPlantUsersService} from "../../../apps/_common/services/app-plant-users.service";
import {ConfigurationService} from "../../serionet/service/configuration.service";
import {LoginService} from "../../login/service/login.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";
import {Site, SitesService} from "../../../apps/_common/services/sites.service";
import {TranslatableComponent} from "../base-component/translatable.component";

@Component({
  selector: 'user-sites',
  templateUrl: 'user-sites.component.html',
  styleUrls: ['user-sites.component.css']
})
export class UserSitesComponent extends TranslatableComponent implements OnInit, OnChanges {

  sitePk:number;
  public sites: Site[] = [];
  licenses: AppPlantUser[] = [];

  @Input('label') label = this.tr("SITE");
  @Input('allowEmpty') allowEmpty = false;
  @Input('prepopulate') prepopulate = true; //Ha senso solo con l'allow empty. Setta o meno il valore iniziale (altrimenti setta vuoto)
  @Input('additionalQuery') additionalQuery: any = null;
  @Input('appName') appName: string;
  @Input('initialValue') initialValue: Site;

  @Output('siteChange') siteChange = new EventEmitter();

  constructor(protected translationService: TranslationService, private appPlantUsersService: AppPlantUsersService,
              private confService: ConfigurationService, private loginService: LoginService,
              private router: Router, private snackBar: MatSnackBar, private sitesService: SitesService) {
    super(translationService);
  }

  ngOnInit() {

    let query = {
      user: this.loginService.user.pk,
      is_active: true
    };

    if(this.additionalQuery){
      query = Object.assign(query,this.additionalQuery);
    }

    this.sitesService.getSites(query).subscribe((data) => {
      if (data && data['count'] && data['count'] > 0) {
        let sites:Site[] = data['results'];
        let sites_pk = sites.map((x) => x.pk);
        let manageInit = () => {
          if (this.initialValue) {
            this.setSite(this.initialValue.pk);
          }
          else {
            if (this.prepopulate){
              let query_ = {
                user_strict: this.loginService.user.pk,
                is_active: true
              };

              if(this.additionalQuery){
                query_ = Object.assign(query_,this.additionalQuery);
              }

              this.sitesService.getSites(query_).subscribe((resp) => {
                let strictUserSites:Site[] = parseSerionetRespToData(resp);
                if (strictUserSites.length > 0) {
                  if(this.sites.find((s)=>s.pk==strictUserSites[0].pk)){
                    this.setSite(strictUserSites[0].pk);
                  }
                  else {
                    this.setSite(this.sites[0].pk);
                  }
                }
              })
            }
            else {
              if(this.allowEmpty) {
                this.setSite(null);
              }
              else {
                this.setSite(this.sites[0].pk);
              }
            }
          }
        };

        if (this.appName) {
          let query = {
            app__app_root: this.appName,
            site__in: sites.map((x) => x.pk).join(","),
            is_active: true
          };

          this.appPlantUsersService.getAppPlantUsersList(query).subscribe((response) => {
            this.licenses = parseSerionetRespToData(response);
            if (this.licenses.length > 0) {
              let sites_licenses = this.licenses.map((l) => l.site);
              let availableSites = arrayIntersect(sites_licenses,sites_pk);
              this.sites = availableSites.map((pk)=>sites.find((s)=>s.pk==pk));
              manageInit();
            }
            else {
              this.router.navigate(["/index"]);
              this.snackBar.open(this.tr("NO_SAP_LICENCE_FOR_APP"), null, {duration: 5000, panelClass: ['error']});
            }
          });
        }
        else {
          this.sites = sites;
          manageInit();
        }
      }
    });
  }

  setSite(s: number) {
    if(s){
      this.sitePk = s;
      if(this.appName){
        let matchingLicense = this.licenses.find((l)=>l.site==this.sitePk);
        if(matchingLicense){
          this.confService.set("SAP_USER", matchingLicense.user, true);
          this.confService.set("SAP_PASSWORD", matchingLicense.password, true);
          this.siteChange.emit(this.sites.find((s_)=>s_.pk==this.sitePk));
        }
        else {
          this.router.navigate(["/index"]);
          this.snackBar.open(this.tr("NO_SAP_LICENCE_FOR_APP"), null, {duration: 5000, panelClass: ['error']});
        }
      }
      else {
        this.siteChange.emit(this.sites.find((s)=>s.pk==this.sitePk));
      }
    }
    else {
      this.sitePk = null;
    }
  }

  ngOnChanges(changes: { [ propName: string]: SimpleChange }) {
    if (changes['initialValue']) {
      let site = changes['initialValue'].currentValue;
      if (site && site.pk) {
        if(this.sites.length>0){
          this.setSite(site);
        }
      }
    }
  }
}
