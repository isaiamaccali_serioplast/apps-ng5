"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var LoggingService = (function () {
    function LoggingService() {
        this.level = 2;
    }
    LoggingService.prototype.error = function (message) {
        this.log(message, 1);
    };
    LoggingService.prototype.warn = function (message) {
        this.log(message, 2);
    };
    LoggingService.prototype.info = function (message) {
        this.log(message, 3);
    };
    LoggingService.prototype.log = function (message, level) {
        if (level <= this.level) {
            if (level === 1) {
                window['console'].error((new Date()).toUTCString() + ": " + message);
            }
            else if (level === 2) {
                window['console'].warn((new Date()).toUTCString() + ": " + message);
            }
            else {
                window['console'].log((new Date()).toUTCString() + ": " + message);
            }
        }
    };
    return LoggingService;
}());
LoggingService = __decorate([
    core_1.Injectable()
], LoggingService);
exports.LoggingService = LoggingService;
