import {Injectable} from "@angular/core";

@Injectable()
export class LoggingService {

  level = 2;

  constructor() {
  }

  error(message: string) {
    this.log(message, 1);
  }

  warn(message: string) {
    this.log(message, 2);
  }

  info(message: string) {
    this.log(message, 3);
  }

  private log(message: string, level: number) {
    if (level <= this.level) {
      if (level === 1) {
        window['console'].error((new Date()).toUTCString() + ": " + message);
      }
      else if (level === 2) {
        window['console'].warn((new Date()).toUTCString() + ": " + message);
      }
      else {
        window['console'].log((new Date()).toUTCString() + ": " + message);
      }
    }
  }
}
