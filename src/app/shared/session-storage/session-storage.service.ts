import {Injectable} from "@angular/core";

@Injectable()
export class SessionStorageService {

  constructor() {}

  ngOnInit(){
    this.clear();
  }

  storeData(entity:string, data:any){
    let entityDataString = window.sessionStorage.getItem(entity);
    let entityData = {};
    if(entityDataString){
      entityData = JSON.parse(entityDataString)
    }

    data.forEach((item)=>{

      entityData[item['pk']]=item;
    });
    window.sessionStorage.setItem(entity, JSON.stringify(entityData));
  }

  storeItem(entity:string, item:any){
    let entityDataString = window.sessionStorage.getItem(entity);
    let entityData = {};
    if(entityDataString){
      entityData = JSON.parse(entityDataString)
    }
    entityData[item['pk']]=item;

    window.sessionStorage.setItem(entity, JSON.stringify(entityData));
  }

  deleteItem(entity:string, item:any){
    let entityDataString = window.sessionStorage.getItem(entity);
    let entityData = {};
    if(entityDataString){
      entityData = JSON.parse(entityDataString)
    }
    if(entityData[item['pk']])
      delete entityData[item['pk']];

    window.sessionStorage.setItem(entity, JSON.stringify(entityData));
  }

  getItem(entity:string, itemId:number){
    let entityDataString = window.sessionStorage.getItem(entity);
    if(!entityDataString){
      return null;
    }
    else {
      let entityData = JSON.parse(entityDataString)
      return entityData[itemId];
    }
  }

  clear(){
    window.sessionStorage.clear();
  }
}
