"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var SessionStorageService = (function () {
    function SessionStorageService() {
    }
    SessionStorageService.prototype.ngOnInit = function () {
        this.clear();
    };
    SessionStorageService.prototype.storeData = function (entity, data) {
        var entityDataString = window.sessionStorage.getItem(entity);
        var entityData = {};
        if (entityDataString) {
            entityData = JSON.parse(entityDataString);
        }
        data.forEach(function (item) {
            entityData[item['pk']] = item;
        });
        window.sessionStorage.setItem(entity, JSON.stringify(entityData));
    };
    SessionStorageService.prototype.storeItem = function (entity, item) {
        var entityDataString = window.sessionStorage.getItem(entity);
        var entityData = {};
        if (entityDataString) {
            entityData = JSON.parse(entityDataString);
        }
        entityData[item['pk']] = item;
        window.sessionStorage.setItem(entity, JSON.stringify(entityData));
    };
    SessionStorageService.prototype.deleteItem = function (entity, item) {
        var entityDataString = window.sessionStorage.getItem(entity);
        var entityData = {};
        if (entityDataString) {
            entityData = JSON.parse(entityDataString);
        }
        if (entityData[item['pk']])
            delete entityData[item['pk']];
        window.sessionStorage.setItem(entity, JSON.stringify(entityData));
    };
    SessionStorageService.prototype.getItem = function (entity, itemId) {
        var entityDataString = window.sessionStorage.getItem(entity);
        if (!entityDataString) {
            return null;
        }
        else {
            var entityData = JSON.parse(entityDataString);
            return entityData[itemId];
        }
    };
    SessionStorageService.prototype.clear = function () {
        window.sessionStorage.clear();
    };
    return SessionStorageService;
}());
SessionStorageService = __decorate([
    core_1.Injectable()
], SessionStorageService);
exports.SessionStorageService = SessionStorageService;
