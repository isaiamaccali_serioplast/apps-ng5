import {NgModule} from "@angular/core";
import {TranslationComponent} from "./translation.component";
import {IdleService} from "./idle.service";
import {SimpleExpiry} from "ng2-idle";

@NgModule({
  imports:[],
  declarations: [],   // components and directives
  providers: [] ,                   // services
  exports:[]
})
export class IdleModule { }
