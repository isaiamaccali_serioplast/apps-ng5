"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var ng2_idle_1 = require("ng2-idle");
var IdleService = (function (_super) {
    __extends(IdleService, _super);
    function IdleService(expiry, loginService) {
        var _this = _super.call(this, expiry) || this;
        _this.loginService = loginService;
        _this.timeoutSeconds = 300;
        _this.idleListeners = [];
        return _this;
    }
    IdleService.prototype.ngOnInit = function () {
        this.stop();
    };
    IdleService.prototype.stop = function () {
        for (var _i = 0, _a = this.idleListeners; _i < _a.length; _i++) {
            var l = _a[_i];
            l.unsubscribe();
        }
    };
    IdleService.prototype.start = function () {
        // sets an idle timeout of 5 seconds, for testing purposes.
        var idle = this;
        idle.setIdle(this.timeoutSeconds - 5);
        // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
        idle.setTimeout(5);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        idle.setInterrupts(ng2_idle_1.DEFAULT_INTERRUPTSOURCES);
        // Subscribe to idle events. Add your logic on how the application should respond, such as displaying
        // a warning dialog onIdleStart, or redirecting to logout page onTImeout, etc.
        var l1 = idle.onIdleStart.subscribe(function () {
        });
        var l2 = idle.onIdleEnd.subscribe(function () {
        });
        var l3 = idle.onTimeoutWarning.subscribe(function (countdown) {
        });
        var self = this;
        var l4 = idle.onTimeout.subscribe(function () {
            if (self.dialog)
                self.dialog.close();
            idle.stop();
            // this.loginService.doLogout().first().subscribe((response) => {
            //   self.router.navigate(["login"]);
            // })
        });
        this.idleListeners = [l1, l2, l3, l4];
        idle.watch();
    };
    IdleService.prototype.setTimeoutSeconds = function (seconds) {
        this.stop();
        this.timeoutSeconds = seconds;
        this.start();
    };
    IdleService.prototype.setRouter = function (router) {
        this.router = router;
    };
    IdleService.prototype.registerDialog = function (dialog) {
        this.dialog = dialog;
    };
    IdleService.prototype.unregisterDialog = function () {
        this.dialog = null;
    };
    return IdleService;
}(ng2_idle_1.Idle));
IdleService = __decorate([
    core_1.Injectable()
], IdleService);
exports.IdleService = IdleService;
