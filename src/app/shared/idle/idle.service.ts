import {Injectable} from "@angular/core";
// import {Idle, SimpleExpiry, DEFAULT_INTERRUPTSOURCES} from "ng2-idle";
import {Subscriber} from "rxjs";
import {Router} from "@angular/router";
import {DialogComponent} from "../material/dialog/dialog.component";


@Injectable()
// export class IdleService extends Idle {
export class IdleService {

  idleListeners: Subscriber<any>[];
  timeoutSeconds = 300;
  router: Router;
  dialog:DialogComponent;

  // constructor(expiry: SimpleExpiry, private loginService: LoginService) {
  constructor() {

    // super(expiry);
    this.idleListeners = [];
  }

  ngOnInit() {
    this.stop();
  }

  stop() {
    for (let l of this.idleListeners) {
      l.unsubscribe();
    }
  }

  start() {
   //TODO
  }

  setTimeoutSeconds(seconds: number) {
    this.stop();
    this.timeoutSeconds = seconds;
    this.start();
  }

  public setRouter(router: Router) {
    this.router = router;
  }

  public registerDialog(dialog:DialogComponent){
    this.dialog = dialog;
  }

  public unregisterDialog(){
    this.dialog = null;
  }
}
