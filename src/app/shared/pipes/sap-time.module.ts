import {NgModule} from "@angular/core";
import {SapTimePipe} from "./sap-time.pipe";

@NgModule({
  declarations: [SapTimePipe],   // components and directives,
  exports:[SapTimePipe],
  providers: []                    // services
})
export class SapTimeModule { }
