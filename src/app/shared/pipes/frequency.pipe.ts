import {PipeTransform, Pipe} from "@angular/core";

import {TranslationService} from "../i18n/translation.service";

@Pipe({
  name: 'frequency'
})
export class FrequencyPipe implements PipeTransform{

  constructor(protected translationService:TranslationService){}

  tr(value){
    return this.translationService.translate(value);
  }

  transform(fr:number) {
    //It assumes that the frequency is measured in hours
    let hours = Math.floor(fr/3600000);

    let days = Math.floor(hours/24);
    let remainingHours = hours%24;

    let stringToReturn = [];
    if(days>0){
      if(days==1){
        stringToReturn.push(days + " " + this.tr('DAY'))
      }
      else {
        stringToReturn.push(days + " " + this.tr('DAYS'))
      }
    }

    if(remainingHours>0){
      if(remainingHours==1){
        stringToReturn.push(remainingHours + " " + this.tr('HOUR'))
      }
      else {
        stringToReturn.push(remainingHours + " " + this.tr('HOURS'))
      }
    }

    return stringToReturn.join(", ")


  }
}
