"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var directives_1 = require("@angular/core/src/metadata/directives");
var FrequencyPipe = (function () {
    function FrequencyPipe(translationService) {
        this.translationService = translationService;
    }
    FrequencyPipe.prototype.transform = function (fr) {
        //It assumes that the frequency is measured in hours
        var hours = Math.floor(fr / 3600000);
        var days = Math.floor(hours / 24);
        var remainingHours = hours % 24;
        var stringToReturn = [];
        if (days > 0) {
            if (days == 1) {
                stringToReturn.push(days + " " + this.translationService.translate('DAY', null, null, null));
            }
            else {
                stringToReturn.push(days + " " + this.translationService.translate('DAYS', null, null, null));
            }
        }
        if (remainingHours > 0) {
            if (remainingHours == 1) {
                stringToReturn.push(remainingHours + " " + this.translationService.translate('HOUR', null, null, null));
            }
            else {
                stringToReturn.push(remainingHours + " " + this.translationService.translate('HOURS', null, null, null));
            }
        }
        return stringToReturn.join(", ");
    };
    return FrequencyPipe;
}());
FrequencyPipe = __decorate([
    directives_1.Pipe({
        name: 'frequency'
    })
], FrequencyPipe);
exports.FrequencyPipe = FrequencyPipe;
