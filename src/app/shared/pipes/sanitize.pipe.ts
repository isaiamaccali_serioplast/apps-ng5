import {SafeHtml, DomSanitizer} from "@angular/platform-browser";
import {PipeTransform, Pipe, SecurityContext} from "@angular/core";

@Pipe({
  name: 'sanitizeHtml'
})
export class SanitizeHtml implements PipeTransform  {

  constructor(private _sanitizer: DomSanitizer){}

  transform(v: string) : SafeHtml {
    return this._sanitizer.bypassSecurityTrustHtml(v);
  }
}
