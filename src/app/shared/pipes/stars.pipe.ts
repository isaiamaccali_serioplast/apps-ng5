import {PipeTransform, Pipe} from "@angular/core";

import {camelCase} from "../utils";

@Pipe({
  name: 'stars'
})
export class StarsPipe implements PipeTransform{

  constructor(){}

  transform(value:number, scale:number = 0.5) {
    if(value<=(scale*10)){
      let starsArray = [];
      let rounded = parseFloat((Math.round(value / scale) * scale).toFixed(1));
      if(parseInt(rounded+"",10)==rounded){
        for(let i=0; i<rounded; i++){
         starsArray.push("<i class='material-icons yellow-text'>star</i>")
        }
        for(let i=0; i<((scale*10)-rounded); i++){
          starsArray.push("<i class='material-icons yellow-text'>star_border</i>")
        }
      }
      else {
        let full = Math.floor(rounded);
        for(let i=0; i<full; i++){
          starsArray.push("<i class='material-icons yellow-text'>star</i>")
        }
        starsArray.push("<i class='material-icons yellow-text'>star_half<i>")
        for(let i=0; i<((scale*10)-rounded-1); i++){
          starsArray.push("<i class='material-icons yellow-text'>star_border</i>")
        }
      }
      return starsArray.join("");
    }
    else {
      return 'Invalid';
    }



  }
}
