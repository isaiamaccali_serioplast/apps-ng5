import {PipeTransform, Pipe} from "@angular/core";

import {camelCase} from "../utils";

@Pipe({
  name: 'camelcase'
})
export class CamelCasePipe implements PipeTransform{

  constructor(){}

  transform(text:string) {

    return camelCase(text);
  }
}
