import {PipeTransform, Pipe} from "@angular/core";


@Pipe({
  name: 'endCutter'
})
export class EndCutterPipe implements PipeTransform {
  transform(text: string, nChars = 7) {

    if (text.length > nChars) {
      return text.substr(text.length - nChars)
    }
    else
      return text;
  }
}

