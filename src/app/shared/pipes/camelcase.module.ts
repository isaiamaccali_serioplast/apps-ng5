import {NgModule} from "@angular/core";
import {CamelCasePipe} from "./camelcase.pipe";

@NgModule({
  declarations: [CamelCasePipe],   // components and directives,
  exports:[CamelCasePipe],
  providers: []                    // services
})
export class CamelCaseModule { }
