import {Pipe, PipeTransform} from '@angular/core';

declare let sap: any;

@Pipe({
  name: 'sapTime'
})

export class SapTimePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if(value){
      let timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({pattern: "HH:mm:ss"});

      let TZOffsetMs = new Date(0).getTimezoneOffset()*60*1000;
      let timeStr = timeFormat.format(new Date(value.ms + TZOffsetMs));  //11:00 AM
      return timeStr;
    }
    else return "-";
  }
}
