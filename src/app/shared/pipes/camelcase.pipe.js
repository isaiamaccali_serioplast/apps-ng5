"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var directives_1 = require("@angular/core/src/metadata/directives");
var utils_1 = require("../utils");
var CamelCasePipe = (function () {
    function CamelCasePipe() {
    }
    CamelCasePipe.prototype.transform = function (text) {
        return utils_1.camelCase(text);
    };
    return CamelCasePipe;
}());
CamelCasePipe = __decorate([
    directives_1.Pipe({
        name: 'camelcase'
    })
], CamelCasePipe);
exports.CamelCasePipe = CamelCasePipe;
