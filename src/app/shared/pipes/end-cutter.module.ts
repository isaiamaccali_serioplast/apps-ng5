import {NgModule} from "@angular/core";
import {EndCutterPipe} from "./end-cutter.pipe";

@NgModule({
  declarations: [EndCutterPipe],   // components and directives,
  exports:[EndCutterPipe],
  providers: []                    // services
})
export class EndCutterModule { }
