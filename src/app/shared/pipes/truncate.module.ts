import {NgModule} from "@angular/core";
import {TruncatePipe} from "./truncate.pipe";

@NgModule({
  declarations: [TruncatePipe],   // components and directives,
  exports:[TruncatePipe],
  providers: []                    // services
})
export class TruncateModule { }
