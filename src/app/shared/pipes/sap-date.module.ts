import {NgModule} from "@angular/core";
import {SapDatePipe} from "./sap-date.pipe";

@NgModule({
  declarations: [SapDatePipe],   // components and directives,
  exports:[SapDatePipe],
  providers: []                    // services
})
export class SapDateModule { }
