import {PipeTransform, Pipe} from "@angular/core";


@Pipe({
  name: 'ExpThousandsSeparator'
})
export class ExpThousandsSeparatorPipe implements PipeTransform{
  transform(n:number) {
    let integerPart = Math.floor(n);
    let decimalPart = n-integerPart;
    let decimalPartFixed = (decimalPart == 0)? null : decimalPart.toFixed(3);
    return Number(integerPart).toLocaleString('fr-FR')+(decimalPartFixed?"."+(decimalPartFixed+"").substring(2):'');
  }
}
