import {NgModule} from "@angular/core";
import {ExpThousandsSeparatorPipe} from "./thousands-separator.pipe";

@NgModule({
  declarations: [ExpThousandsSeparatorPipe],   // components and directives,
  exports:[ExpThousandsSeparatorPipe],
  providers: []                    // services
})
export class ExpThousandsSeparatorModule { }
