import {NgModule} from "@angular/core";
import {FilenamePipe} from "./filename.pipe";

@NgModule({
  declarations: [FilenamePipe],   // components and directives,
  exports:[FilenamePipe],
  providers: []                    // services
})
export class FilenameModule { }
