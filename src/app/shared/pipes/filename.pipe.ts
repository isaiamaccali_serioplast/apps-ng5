import {PipeTransform, Pipe} from "@angular/core";


@Pipe({
  name: 'filename'
})
export class FilenamePipe implements PipeTransform{
  transform(fileObject:any) {
    let filenameSplit = fileObject.__str__.split("/");
    return filenameSplit.pop();
  }
}
