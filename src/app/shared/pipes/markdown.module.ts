import {NgModule} from "@angular/core";
import {MarkdownPipe} from "./markdown.pipe";

@NgModule({
  declarations: [MarkdownPipe],   // components and directives,
  exports:[MarkdownPipe],
  providers: []                    // services
})
export class MarkdownPipeModule { }
