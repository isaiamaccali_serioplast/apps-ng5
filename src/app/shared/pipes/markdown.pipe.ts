import {PipeTransform, Pipe} from "@angular/core";
import marked = require("marked");

// declare let marked;

@Pipe({
  name: 'markdown'
})
export class MarkdownPipe implements PipeTransform{

  constructor(){}

  transform(text:string) {
    return marked(text,{sanitize:false});

    // this.markdownService.setMarkedOptions({sanitize:false});
    // return this.markdownService.compile(text);
  }
}
