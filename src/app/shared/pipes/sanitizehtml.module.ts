import {NgModule} from "@angular/core";
import {SanitizeHtml} from "./sanitize.pipe";

@NgModule({
  declarations: [SanitizeHtml],   // components and directives,
  exports:[SanitizeHtml],
  providers: []                    // services
})
export class SanitizeHTMLModule { }
