import {NgModule} from "@angular/core";
import {FrequencyPipe} from "./frequency.pipe";

@NgModule({
  declarations: [FrequencyPipe],   // components and directives,
  exports:[FrequencyPipe],
  providers: []                    // services
})
export class FrequencyModule { }
