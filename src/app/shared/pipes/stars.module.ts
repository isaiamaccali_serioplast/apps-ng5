import {NgModule} from "@angular/core";
import {StarsPipe} from "./stars.pipe";

@NgModule({
  declarations: [StarsPipe],   // components and directives,
  exports:[StarsPipe],
  providers: []                    // services
})
export class StarsModule { }
