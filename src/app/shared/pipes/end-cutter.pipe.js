"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var directives_1 = require("@angular/core/src/metadata/directives");
var EndCutterPipe = (function () {
    function EndCutterPipe() {
    }
    EndCutterPipe.prototype.transform = function (text, nChars) {
        if (nChars === void 0) { nChars = 7; }
        if (text.length > nChars) {
            return text.substr(text.length - nChars);
        }
        else
            return text;
    };
    return EndCutterPipe;
}());
EndCutterPipe = __decorate([
    directives_1.Pipe({
        name: 'endCutter'
    })
], EndCutterPipe);
exports.EndCutterPipe = EndCutterPipe;
