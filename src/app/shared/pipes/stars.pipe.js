"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var directives_1 = require("@angular/core/src/metadata/directives");
var StarsPipe = (function () {
    function StarsPipe() {
    }
    StarsPipe.prototype.transform = function (value, scale) {
        if (scale === void 0) { scale = 0.5; }
        if (value <= (scale * 10)) {
            var starsArray = [];
            var rounded = parseFloat((Math.round(value / scale) * scale).toFixed(1));
            if (parseInt(rounded + "", 10) == rounded) {
                for (var i = 0; i < rounded; i++) {
                    starsArray.push("<i class='material-icons'>star</i>");
                }
                for (var i = 0; i < ((scale * 10) - rounded); i++) {
                    starsArray.push("<i class='material-icons'>star_border</i>");
                }
            }
            else {
                var full = Math.floor(rounded);
                for (var i = 0; i < full; i++) {
                    starsArray.push("<i class='material-icons'>star</i>");
                }
                starsArray.push("<i class='material-icons'>star_half<i>");
                for (var i = 0; i < ((scale * 10) - rounded - 1); i++) {
                    starsArray.push("<i class='material-icons'>star_border</i>");
                }
            }
            return starsArray.join("");
        }
        else {
            return 'Invalid';
        }
    };
    return StarsPipe;
}());
StarsPipe = __decorate([
    directives_1.Pipe({
        name: 'stars'
    })
], StarsPipe);
exports.StarsPipe = StarsPipe;
