/**
 * Login component. Used in the login page to enter username/password or badge, and reused in the top toolbar to logout
 */

import {LoginResponse, LoginService} from "../../service/login.service";
import {ConfigurationService} from "../../../serionet/service/configuration.service";
import {SideNavService} from "../../../sidenav/service/sidenav.service";
import {AppComponent} from "../../../../app.component";
import {TranslationService} from "../../../i18n/translation.service";
import {LoadingService} from "../../../loading/loading.service";
import {MessageService} from "../../../material/message/message.service";
import {User} from "../../../../apps/_common/services/users.service";
import {Component, Input, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {TranslatableComponent} from "../../../components/base-component/translatable.component";

@Component({
  selector: 'sp-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent extends TranslatableComponent implements OnInit {

  user: User;
  userIsLogged: boolean = false;
  errorState: boolean = false;
  badgeLogin: boolean = true;
  server: string;
  errorMessage: string;
  @Input() app: AppComponent;

  constructor(private loginService: LoginService, private router: Router, private titleService: Title,
              private confService: ConfigurationService, private sideNavService: SideNavService,
              protected translationService: TranslationService, private loadingService: LoadingService,
              private messageService: MessageService) {
    super(translationService);
    this.user = this.loginService.user;
    this.userIsLogged = this.loginService.userIsLogged;
    this.titleService.setTitle('Login - Serioplast');
    this.server = this.confService.get('SERVER');
  }

  ngOnInit() {
    if (this.loginService.userIsLogged) {
      this.user = this.loginService.user;
      if (this.app) {
        this.app.reloadApps();
      }
      let appName = window.location.pathname.split("/")[1].split("?")[0];
      if(!appName||appName=='login')
        this.router.navigate(["/index"]);
    }
    else {
      this.sideNavService.appMenuEnabled = false;
    }
  }


  modeChange() {
    this.user = new User();
  }

  doLogin() {
    try {
      this.loadingService.show();
      this.loginService.doLogin(this.user).subscribe((loginResponse) => this.handleLoginResponse(loginResponse));
      return false;
    }
    catch(e){
      console.log(e);
    }

  }

  doLogout() {
    this.loginService.doLogout().subscribe((logoutResponse) => this.handleLogoutResponse(logoutResponse));
    return false;
  }

  handleLoginResponse(loginResponse: LoginResponse) {
    this.loadingService.hide();
    if (loginResponse.status == 0) {
      this.errorState = false;
      this.user = loginResponse.user;
      // if(this.user.sites && this.user.sites.length>0){
      if (this.app) {
        this.app.reloadApps();
      }
      this.router.navigate(["/"]);
      // }
      // else {
      //   loginResponse.status = 1;
      //   this.errorMessage = "No sites connected to the user, access not allowed";
      //   this.errorState = true;
      //   this.user = new User();
      //   this.loginService.userIsLogged = false;
      //   this.userIsLogged = false;
      //   if(this.app){
      //     this.app.closeAppList();
      //     this.app.reset();
      //   }
      // }

    }
    else {
      this.errorMessage = loginResponse.message;
      this.errorState = true;
    }
  }

  handleLogoutResponse(logoutResponse: LoginResponse) {
    if (logoutResponse.status != 0) {
      this.messageService.open({
        error: true,
        title: this.tr("ERROR"),
        message: this.tr("LOGOUT_ERROR")
      })
    }
    this.user = new User();
    this.userIsLogged = false;
    this.app.closeAppList();
    this.app.reset();
    this.router.navigate(["/login"]);
  }

}
