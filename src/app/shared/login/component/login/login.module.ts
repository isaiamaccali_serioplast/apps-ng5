import {NgModule} from "@angular/core";
import {LoginComponent} from "./login.component";
import {LoginService} from "../../service/login.service";
import {SharedModule} from "../../../shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [LoginComponent],   // components and directives
  exports:[LoginComponent],
  providers: []                    // services
})
export class LoginModule { }

