/**
 * Login service, used to log on to Serionet and share the logged user data between the components
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var user_1 = require("../../user/models/user");
var loginResponse_1 = require("../models/loginResponse");
var rxjs_1 = require("rxjs");
var logoutResponse_1 = require("../models/logoutResponse");
var http_1 = require("@angular/http");
var LoginService = (function () {
    function LoginService(cookieService, serionetService, usersService, sessionStorageService, appsService, confService, translationService) {
        var _this = this;
        this.cookieService = cookieService;
        this.serionetService = serionetService;
        this.usersService = usersService;
        this.sessionStorageService = sessionStorageService;
        this.appsService = appsService;
        this.confService = confService;
        this.translationService = translationService;
        this.cookieAuthName = 'auth';
        this.userIsLogged = false;
        this.user = new user_1.User();
        //If the login information were stored in the cookies, the component behaves accordingly
        if (this.getCookieLogged() && this.getCookieLogged() != "") {
            var comp = this.getCookieLogged().split("|");
            //The cookie contains the auth token and the user id
            if (comp && comp.length == 2) {
                this.serionetService.setAuthToken(comp[0]);
                this.userIsLogged = true;
                var user = this.getUserFromStorage();
                if (user) {
                    this.user = JSON.parse(user);
                    this.setUserConf(this.user);
                }
                else {
                    this.usersService.getUserData(parseInt(comp[1], 10)).subscribe(function (user) {
                        _this.user = user;
                        _this.setUserConf(user);
                    });
                }
            }
        }
    }
    LoginService.prototype.doLogin = function (user) {
        var _this = this;
        var self = this;
        var loginResponse = new loginResponse_1.LoginResponse();
        return rxjs_1.Observable.create(function (observer) {
            var options = new http_1.RequestOptions();
            _this.serionetService.doPost("login/", JSON.stringify(user), options).then(function (r) {
                var at = r['token'];
                _this.serionetService.setAuthToken(at);
                _this.usersService.getUserData(r['id']).first().subscribe(function (user) {
                    self.userIsLogged = true;
                    loginResponse.status = 0;
                    self.user = user;
                    loginResponse.user = user;
                    self.setCookieLogged();
                    self.setUserConf(user);
                    observer.next(loginResponse);
                    observer.complete();
                });
            }).catch(function (r) {
                var message = "Login error";
                if (r.message) {
                    message = r.message;
                    try {
                        if (JSON.parse(r.message)['non_field_errors']) {
                            message = JSON.parse(r.message)['non_field_errors'].join("; ");
                        }
                    }
                    catch (e) { }
                }
                loginResponse.status = 1;
                loginResponse.message = message;
                self.userIsLogged = false;
                self.user = new user_1.User();
                self.removeCookieLogged();
                self.resetUserConf();
                observer.next(loginResponse);
                observer.complete();
            });
        });
    };
    LoginService.prototype.doLogout = function () {
        var _this = this;
        var self = this;
        var logoutResponse = new logoutResponse_1.LogoutResponse();
        return rxjs_1.Observable.create(function (observer) {
            var options = new http_1.RequestOptions();
            _this.serionetService.doPost("logout/", {}, options).then(function (r) {
                _this.serionetService.setAuthToken(null);
                self.userIsLogged = false;
                self.resetUserConf();
                self.user = new user_1.User();
                self.removeCookieLogged();
                self.sessionStorageService.clear();
                self.appsService.authorizedApps = {};
                logoutResponse.status = 0;
                observer.next(logoutResponse);
                observer.complete();
            }).catch(function () {
                logoutResponse.status = 1;
                //any way, I clean all up
                _this.serionetService.setAuthToken(null);
                self.userIsLogged = false;
                self.resetUserConf();
                self.user = new user_1.User();
                self.removeCookieLogged();
                self.sessionStorageService.clear();
                self.appsService.authorizedApps = {};
                observer.next(logoutResponse);
                observer.complete();
            });
        });
    };
    LoginService.prototype.getCookieLogged = function () {
        return this.cookieService.get(this.cookieAuthName);
    };
    LoginService.prototype.getUserFromStorage = function () {
        return window.localStorage.getItem("userData");
    };
    LoginService.prototype.setCookieLogged = function () {
        window.localStorage.setItem("userData", JSON.stringify(this.user));
        this.cookieService.put(this.cookieAuthName, this.serionetService.getAuthToken() + "|" + this.user.pk);
    };
    LoginService.prototype.removeCookieLogged = function () {
        this.cookieService.remove(this.cookieAuthName);
        window.localStorage.clear();
    };
    LoginService.prototype.setUserConf = function (user) {
        this.confService.set("SAP_USER", "SERVICE01"); //TODO mettere quelli veri
        this.confService.set("SAP_PASSWORD", "S3r10pl4st"); //TODO mettere quelli veri
        this.translationService.setLanguageFromUser(user); //TODO mettere quelli veri
    };
    LoginService.prototype.resetUserConf = function () {
        this.confService.set("SAP_USER", ""); //TODO mettere quelli veri
        this.confService.set("SAP_PASSWORD", ""); //TODO mettere quelli veri
        this.translationService.setLanguage("");
    };
    return LoginService;
}());
LoginService = __decorate([
    core_1.Injectable()
], LoginService);
exports.LoginService = LoginService;
