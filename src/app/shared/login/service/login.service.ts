/**
 * Login service, used to log on to Serionet and share the logged user data between the components
 */

import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {SerionetRequestOptions, SerionetService} from "../../serionet/service/serionet.service";
import {SessionStorageService} from "../../session-storage/session-storage.service";
import {AppsService} from "../../../apps/apps.service";
import {ConfigurationService} from "../../serionet/service/configuration.service";
import {TranslationService} from "../../i18n/translation.service";
import {User, UsersService} from "../../../apps/_common/services/users.service";
import {CookieService} from "ngx-cookie";

export class LoginResponse {
  status: number;
  message: string;
  user: User
}

export class LogoutResponse {
  status: number;
  message: string;
}

@Injectable()
export class LoginService {

  userIsLogged: boolean;
  user:User;
  cookieAuthName = 'auth';

  constructor(private cookieService:CookieService,
              private serionetService:SerionetService,
              private usersService:UsersService,
              private sessionStorageService:SessionStorageService,
              private appsService:AppsService,
              private confService:ConfigurationService,
              protected translationService:TranslationService
  ) {
    this.userIsLogged = false;

    this.user = new User();
    //If the login information were stored in the cookies, the component behaves accordingly
    if(this.getCookieLogged() && this.getCookieLogged()!=""){
      let comp = this.getCookieLogged().split("|");
      //The cookie contains the auth token and the user id
      if(comp && comp.length==2){
        this.serionetService.setAuthToken(comp[0]);
        this.userIsLogged = true;
        let user = this.getUserFromStorage();
        if(user){
          this.user = JSON.parse(user);
          this.setUserConf(this.user);
        }
        else {
          this.usersService.getUserData(parseInt(comp[1],10)).subscribe((user)=> {
            this.user = user;
            this.setUserConf(user);
          });
        }
      }
    }
  }

  doLogin(user:User){
    let loginResponse = new LoginResponse();

    return Observable.create((observer) => {

      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {};

      this.serionetService.doPost("login/",JSON.stringify(user),serionetOptions).subscribe((r) => {
        let at = r['token'];
        this.serionetService.setAuthToken(at);
        this.usersService.getUserData(r['id']).subscribe((user)=>{
          this.userIsLogged = true;
          loginResponse.status = 0;
          this.user = user;
          loginResponse.user = user;
          this.setCookieLogged();
          this.setUserConf(user);
          observer.next(loginResponse);
          observer.complete();
        },(r)=>{
          let message = "Login error";
          if(r.message){
            message = r.message;
            try {
              if(JSON.parse(r.message)['non_field_errors']){
                message = JSON.parse(r.message)['non_field_errors'].join("; ");
              }
            }
            catch(e){}
          }
          loginResponse.status = 1;
          loginResponse.message = message;
          this.userIsLogged = false;
          this.user = new User();
          this.removeCookieLogged();
          this.resetUserConf();
          observer.next(loginResponse);
          observer.complete();
        })
      },(r)=> {
        let message = "Login error";
        if(r.message){
          message = r.message;
          try {
            if(JSON.parse(r.message)['non_field_errors']){
              message = JSON.parse(r.message)['non_field_errors'].join("; ");
            }
          }
          catch(e){}
        }
        loginResponse.status = 1;
        loginResponse.message = message;
        this.userIsLogged = false;
        this.user = new User();
        this.removeCookieLogged();
        this.resetUserConf();
        observer.next(loginResponse);
        observer.complete();
      });
    });
  }

  doLogout(){
    let logoutResponse = new LogoutResponse();

    return Observable.create((observer) => {
      let serionetOptions = new SerionetRequestOptions();
      serionetOptions.options = {};

      this.serionetService.doPost("logout/",{},serionetOptions).subscribe(() => {
        this.serionetService.setAuthToken(null);
        this.userIsLogged = false;
        this.resetUserConf();
        this.user = new User();
        this.removeCookieLogged();
        this.sessionStorageService.clear();
        this.appsService.authorizedApps = {};
        logoutResponse.status = 0;
        observer.next(logoutResponse);
        observer.complete();
      }, ()=> {
        logoutResponse.status = 1;
        //any way, I clean all up
        this.serionetService.setAuthToken(null);
        this.userIsLogged = false;
        this.resetUserConf();
        this.user = new User();
        this.removeCookieLogged();
        this.sessionStorageService.clear();
        this.appsService.authorizedApps = {};
        observer.next(logoutResponse);
        observer.complete();
      });
    });
  }

  getCookieLogged(){
    return this.cookieService.get(this.cookieAuthName);
  }

  getUserFromStorage(){
    return window.localStorage.getItem("userData");
  }

  setCookieLogged(){
    window.localStorage.setItem("userData",JSON.stringify(this.user));
    this.cookieService.put(this.cookieAuthName,this.serionetService.getAuthToken()+"|"+this.user.pk);
  }

  removeCookieLogged(){
    this.cookieService.remove(this.cookieAuthName);
    window.localStorage.clear();
  }

  setUserConf(user:User){
    this.confService.set("SAP_USER","SERVICE01"); //TODO mettere quelli veri
    this.confService.set("SAP_PASSWORD","S3r10pl4st"); //TODO mettere quelli veri
    this.translationService.setLanguageFromUser(user) //TODO mettere quelli veri
  }

  resetUserConf(){
    this.confService.set("SAP_USER",""); //TODO mettere quelli veri
    this.confService.set("SAP_PASSWORD",""); //TODO mettere quelli veri
    this.translationService.setLanguage("")
  }

}
