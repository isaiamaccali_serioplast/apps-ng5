/**
 * Module used to share the common Serionet services.
 */
import {NgModule} from "@angular/core";
import {LoginService} from "./login.service";


@NgModule({
  imports:[],
  providers: [LoginService]                    // services
})
export class LoginModule { }
