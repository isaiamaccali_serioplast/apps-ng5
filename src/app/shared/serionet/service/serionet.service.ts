
import {throwError as observableThrowError, Observable} from 'rxjs';
import {ConfigurationService} from "./configuration.service";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {catchError, map, timeoutWith} from "rxjs/operators";

export class SerionetRequestOptions  {
  options?:any;
  noCT:boolean = false;
  timeout = 30000;
}

@Injectable()
export class SerionetService {

  authToken: string;
  baseURL: string;

  constructor(private conf: ConfigurationService, private http: HttpClient) {
    this.baseURL = this.conf.get("SERVER") + "/api/"
  }

  doGet(service: String, serionetOptions: SerionetRequestOptions):Observable<any> {
    let options = serionetOptions.options;
    // let noCT = serionetOptions.noCT;
    let timeout = serionetOptions.timeout;

    if (this.authorizeRequest(options)) {

      let isDownload = false;

      let urlParams = new HttpParams();
      if (options.params) {
        let s = <HttpParams> options.params;
        if (!s.get('custom_format')) {
          s = s.set('format', 'json');
        }
        else {
          isDownload = true;
        }
        options.params = s;
      }
      else {
        options.params = urlParams;
        if (urlParams.get('custom_format')) {
          isDownload = true;
        }
      }

      if (!isDownload) {
        return this.http
          .get<any>(this.baseURL + service, options)
          .pipe(
            timeoutWith(timeout, this.handleTimeout()),
            catchError(this.handleError)
          );
      }
      else {

        options.responseType = 'blob';
        return this.http
          .get<Object>(this.baseURL + service, options)
          // .timeoutWith(150000, Observable.defer(() => Observable.throw(new Error("Timeout in request"))))
          .pipe(
            map((resp)=>{
              let par:URLSearchParams = options.params;
              let parsedUrl = new URL(this.baseURL + service);
              let s = <HttpParams> options.params;
              let extension = s.get("custom_format");
              let file = window.URL.createObjectURL(resp);
              let link = document.createElement("a");
              let urlSplitted = parsedUrl.toString().split("/");
              let filename = urlSplitted[urlSplitted.length - 2];
              filename = filename + "-" + par.toString();
              link.download = filename + "." + extension;
              link.href = file;
              link.click();
              return {status:200}
            }),
            timeoutWith(timeout, this.handleTimeout()),
            catchError(this.handleError)
          );
      }
    }
    else {
      return observableThrowError("Authorize token missing");
    }
  }

  doPost(service: String, body: any, serionetOptions: SerionetRequestOptions):Observable<any> {
    let options = serionetOptions.options;
    let noCT = serionetOptions.noCT;
    let timeout = serionetOptions.timeout;

    this.authorizeRequest(options);
    let headers = new HttpHeaders();
    if (options.headers) {
      headers = options.headers;
    }
    else {
      options.headers = headers;
    }
    if(!noCT)
      headers = headers.set("Content-Type", "application/json");
    options.headers = headers;
    let urlParams = new HttpParams();
    if (options.search) {
      let s = <HttpParams> options.search;
      s.set('format', 'json');
      options.search = s;
    }
    else {
      options.search = urlParams;
      urlParams.set('format', 'json');
    }

    return this.http.post<any>(this.baseURL + service, body, options).pipe(
      timeoutWith(timeout, this.handleTimeout()),
      catchError(this.handleError)
    );
  }

  doPostFile(service: String, body: any, serionetOptions: SerionetRequestOptions):Observable<any> {

    let options = serionetOptions.options;
    // let noCT = serionetOptions.noCT;
    let timeout = serionetOptions.timeout;

    this.authorizeRequest(options);
    let urlParams = new HttpParams();
    if (options.search) {
      options.search = <HttpParams> options.search;
    }
    else {
      options.search = urlParams;
    }

    return this.http.post(this.baseURL + service, body, options).pipe(
      timeoutWith(timeout, this.handleTimeout()),
      catchError(this.handleError)
    );
  }

  doDelete(service: String, serionetOptions: SerionetRequestOptions):Observable<any> {
    let options = serionetOptions.options;
    // let noCT = serionetOptions.noCT;
    let timeout = serionetOptions.timeout;

    if (this.authorizeRequest(options)) {
      let headers = new HttpHeaders();
      if (options.headers) {
        headers = options.headers;
      }
      else {
        options.headers = headers;
      }
      headers.append("Content-Type", "application/json");
      let urlParams = new HttpParams();
      if (options.search) {
        let s = <HttpParams> options.search;
        s.set('format', 'json');
        options.search = s;
      }
      else {
        options.search = urlParams;
        urlParams.set('format', 'json');
      }

      options.observe = "response";

      return this.http.delete<any>(this.baseURL + service, options).pipe(
        timeoutWith(timeout, this.handleTimeout()),
        catchError(this.handleError)
      );
    }
    else {
      return observableThrowError("Authorize token missing");
    }
  }

  doPut(service: String, body: any, serionetOptions: SerionetRequestOptions):Observable<any> {
    let options = serionetOptions.options;
    // let noCT = serionetOptions.noCT;
    let timeout = serionetOptions.timeout;
    this.authorizeRequest(options);
    let headers = new HttpHeaders();
    if (options.headers) {
      headers = options.headers;
    }
    else {
      options.headers = headers;
    }
    headers.append("Content-Type", "application/json");
    let urlParams = new HttpParams();
    if (options.search) {
      let s = <HttpParams> options.search;
      s.set('format', 'json');
      options.search = s;
    }
    else {
      options.search = urlParams;
      urlParams.set('format', 'json');
    }

    return this.http
      .put<any>(this.baseURL + service, body, options).pipe(
        timeoutWith(timeout, this.handleTimeout()),
        catchError(this.handleError)
      );
  }

  setAuthToken(token: string) {
    this.authToken = token;
  }

  getAuthToken() {
    return this.authToken;
  }

  authorizeRequest(options: any) {
    if (this.getAuthToken()) {
      let at = this.getAuthToken();
      let headers = new HttpHeaders();
      if (options && options.headers) {
        headers = options.headers;
      }
      headers = headers.append("Authorization", "Token " + at);
      options.headers = headers;
      return true;
    }
    else {
      return false;
    }
  }

  handleTimeout() {
    let resp = {};
    resp['status'] = 9000;
    resp['context'] = "SERIONET";
    resp['message'] = "Timeout in request";
    return Observable.create((observer)=>{
      observer.next(resp);
      observer.complete();
    })
  }

  handleError(error: any) {
    let resp = {};
    resp['status'] = error.status;
    resp['message'] = error.error;
    return Observable.create((observer)=>{
      observer.next(resp);
      observer.complete();
    })
  }
}


