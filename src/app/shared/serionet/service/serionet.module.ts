/**
 * Module used to share the common Serionet services.
 */
import {NgModule} from "@angular/core";
// import {HTTP_INTERCEPTORS} from "@angular/common/http";
// import {DEFAULT_TIMEOUT, defaultTimeout, TimeoutInterceptor} from "./serionet.service";

@NgModule({
  imports:[],
  providers: [
    // [{ provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true }],
    // [{ provide: DEFAULT_TIMEOUT, useValue: defaultTimeout }]
  ]                    // services
})
export class SerionetModule { }
