"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Service used to share configuration
 */
var core_1 = require("@angular/core");
var ConfigurationService = (function () {
    function ConfigurationService() {
        /*
         For localHost please check follow config on settings.py (Django):
         CORS_ORIGIN_ALLOW_ALL = True
         CSRF_COOKIE_DOMAIN = "127.0.0.1:8000"
         */
        this.localHost = "127.0.0.1:8000";
        this.devDomain = "apps-dev.serioplast.com";
        this.devHost = "serionet-dev.serioplast.com";
        this.testDomain = "apps-test.serioplast.com";
        this.testHost = "serionet-test.serioplast.com";
        this.prodDomain = "apps.serioplast.com";
        this.prodHost = "serionet.serioplast.com";
        this.httpProtocol = "https://";
        this.config = {};
        var SERVER = this.devHost;
        //let SERVER = this.localHost;
        if (SERVER == this.localHost) {
            this.httpProtocol = "http://";
        }
        if (document.domain) {
            if (document.domain.indexOf(this.testDomain) != -1) {
                SERVER = this.testHost;
            }
            else {
                if (document.domain.indexOf(this.prodDomain) != -1) {
                    SERVER = this.prodHost;
                }
                else if (document.domain.indexOf(this.devDomain) != -1) {
                    SERVER = this.devHost;
                }
            }
        }
        this.set('SERVER', this.httpProtocol + SERVER);
    }
    ConfigurationService.prototype.get = function (property) {
        return this.config[property];
    };
    ConfigurationService.prototype.set = function (property, value) {
        this.config[property] = value;
    };
    ConfigurationService.prototype.getHeaderColor = function () {
        var curr_server = this.get('SERVER');
        switch (curr_server) {
            case this.httpProtocol + this.prodHost:
                return "#4CAF50";
            case this.httpProtocol + this.testHost:
                return "tomato";
            case this.httpProtocol + this.devHost:
                return "#ffd600";
            case this.httpProtocol + this.localHost:
                return "#00B0FF";
            default:
                return "#ffd600";
        }
    };
    return ConfigurationService;
}());
ConfigurationService = __decorate([
    core_1.Injectable()
], ConfigurationService);
exports.ConfigurationService = ConfigurationService;
