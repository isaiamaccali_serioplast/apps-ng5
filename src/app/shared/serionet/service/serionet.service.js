"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var http_1 = require("@angular/http");
var rxjs_1 = require("rxjs");
var core_1 = require("@angular/core");
var SerionetService = (function () {
    function SerionetService(conf, http) {
        this.conf = conf;
        this.http = http;
        this.baseURL = this.conf.get("SERVER") + "/api/";
    }
    SerionetService.prototype.doPost = function (service, body, options) {
        this.authorizeRequest(options);
        var headers = new http_1.Headers();
        if (options.headers) {
            headers = options.headers;
        }
        else {
            options.headers = headers;
        }
        headers.append("Content-Type", "application/json");
        var urlParams = new http_1.URLSearchParams();
        if (options.search) {
            var s = options.search;
            s.set('format', 'json');
            options.search = s;
        }
        else {
            options.search = urlParams;
            urlParams.set('format', 'json');
        }
        return this.http
            .post(this.baseURL + service, body, options)
            .timeoutWith(30000, rxjs_1.Observable.defer(function () { return rxjs_1.Observable.throw(new Error("Timeout in request")); }))
            .map(function (r) { return r.json(); })
            .catch(this.handleError)
            .toPromise();
    };
    SerionetService.prototype.doPostFile = function (service, body, options) {
        this.authorizeRequest(options);
        var urlParams = new http_1.URLSearchParams();
        if (options.search) {
            options.search = options.search;
        }
        else {
            options.search = urlParams;
        }
        return this.http
            .post(this.baseURL + service, body, options)
            .timeoutWith(30000, rxjs_1.Observable.defer(function () { return rxjs_1.Observable.throw(new Error("Timeout in request")); }))
            .map(function (r) { return r.json(); })
            .catch(this.handleError)
            .toPromise();
    };
    SerionetService.prototype.handleTimeout = function () {
        var resp = {};
        resp['status'] = 9000;
        resp['context'] = "SERIONET";
        resp['message'] = "Timeout in request";
        return rxjs_1.Observable.throw(resp);
    };
    SerionetService.prototype.handleError = function (error) {
        var resp = {};
        resp['status'] = error.status;
        resp['message'] = error._body;
        return rxjs_1.Observable.throw(resp);
    };
    SerionetService.prototype.doGet = function (service, options) {
        if (this.authorizeRequest(options)) {
            var isDownload = false;
            var urlParams = new http_1.URLSearchParams();
            if (options.search) {
                var s = options.search;
                if (!s.get('custom_format')) {
                    s.set('format', 'json');
                }
                else {
                    isDownload = true;
                }
                options.search = s;
            }
            else {
                options.search = urlParams;
                if (!urlParams.get('custom_format')) {
                    urlParams.set('format', 'json');
                }
                else {
                    isDownload = true;
                }
            }
            if (!isDownload) {
                return this.http
                    .get(this.baseURL + service, options)
                    .timeoutWith(30000, rxjs_1.Observable.defer(function () { return rxjs_1.Observable.throw(new Error("Timeout in request")); }))
                    .map(function (r) { return r.json(); })
                    .catch(this.handleError)
                    .toPromise();
            }
            else {
                options.responseType = http_1.ResponseContentType.Blob;
                var p = this.http
                    .get(this.baseURL + service, options)
                    .timeoutWith(150000, rxjs_1.Observable.defer(function () { return rxjs_1.Observable.throw(new Error("Timeout in request")); }))
                    .catch(this.handleError)
                    .toPromise();
                p.then(function (r) {
                    var parsedUrl = new URL(r.url);
                    /* TODO IN THE FUTURE
          
                     let parsedUrlParams = new URLSearchParams (parsedUrl.search);
          
                     let filenameUrlParams = "";
          
                     let i = parsedUrlParams.paramsMap.entries();
                     let proceed = true;
          
                     while (proceed){
                     let entry = i.next();
                     proceed = !entry.done;
                     }*/
                    var s = options.search;
                    var extension = s.get("custom_format");
                    var type = r.headers.get("content-type").split(";")[0];
                    var data = new Blob([r['_body']], { type: type });
                    var file = window.URL.createObjectURL(data);
                    var link = document.createElement("a");
                    var urlSplitted = r.url.split("/");
                    var filename = urlSplitted[urlSplitted.length - 2];
                    filename = filename + "-" + parsedUrl.search.split("?").join("");
                    link.download = filename + "." + extension;
                    link.href = file;
                    link.click();
                });
                return p;
            }
        }
        else {
            return Promise.reject("Authorize token missing");
        }
    };
    SerionetService.prototype.setAuthToken = function (token) {
        this.authToken = token;
    };
    SerionetService.prototype.getAuthToken = function () {
        return this.authToken;
    };
    SerionetService.prototype.authorizeRequest = function (options) {
        if (this.getAuthToken()) {
            var at = this.getAuthToken();
            var headers = new http_1.Headers();
            if (options.headers) {
                headers = options.headers;
            }
            else {
                options.headers = headers;
            }
            headers.append("Authorization", "Token " + at);
            return true;
        }
        else {
            return false;
        }
    };
    SerionetService.prototype.doDelete = function (service, options) {
        if (this.authorizeRequest(options)) {
            var headers = new http_1.Headers();
            if (options.headers) {
                headers = options.headers;
            }
            else {
                options.headers = headers;
            }
            headers.append("Content-Type", "application/json");
            var urlParams = new http_1.URLSearchParams();
            if (options.search) {
                var s = options.search;
                s.set('format', 'json');
                options.search = s;
            }
            else {
                options.search = urlParams;
                urlParams.set('format', 'json');
            }
            return this.http
                .delete(this.baseURL + service, options)
                .catch(this.handleError)
                .toPromise();
        }
        else {
            return Promise.reject("Authorize token missing");
        }
    };
    SerionetService.prototype.doPut = function (service, body, options) {
        this.authorizeRequest(options);
        var headers = new http_1.Headers();
        if (options.headers) {
            headers = options.headers;
        }
        else {
            options.headers = headers;
        }
        headers.append("Content-Type", "application/json");
        var urlParams = new http_1.URLSearchParams();
        if (options.search) {
            var s = options.search;
            s.set('format', 'json');
            options.search = s;
        }
        else {
            options.search = urlParams;
            urlParams.set('format', 'json');
        }
        return this.http
            .put(this.baseURL + service, body, options)
            .map(function (r) { return r.json(); })
            .catch(this.handleError)
            .toPromise();
    };
    return SerionetService;
}());
SerionetService = __decorate([
    core_1.Injectable()
], SerionetService);
exports.SerionetService = SerionetService;
