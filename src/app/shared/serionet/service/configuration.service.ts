/**
 * Service used to share configuration
 */
import {Injectable} from "@angular/core";

@Injectable()
export class ConfigurationService {

  /*
   For localHost please check follow config on settings.py (Django):
   CORS_ORIGIN_ALLOW_ALL = True
   CSRF_COOKIE_DOMAIN = "127.0.0.1:8000"
   */
  localHost = "127.0.0.1:8000";

  devDomain = "apps-dev.serioplast.com";
  devHost = "serionet-dev.serioplast.com";
  testDomain = "apps-test.serioplast.com";
  testHost = "serionet-test.serioplast.com";
  qaDomain = "apps-qa.serioplast.com";
  qaHost = "serionet-qa.serioplast.com";
  prodDomain = "apps.serioplast.com";
  prodHost = "serionet.serioplast.com";
  httpProtocol = "https://";

  config: any;

  constructor() {
    this.config = {};
    let SERVER = this.devHost;
    //let SERVER = this.localHost;
    if (SERVER == this.localHost) {
      this.httpProtocol = "http://";
    }
    if (document.domain) {
      if (document.domain.indexOf(this.testDomain) != -1) {
        SERVER = this.testHost;
      }
      else {
        if (document.domain.indexOf(this.prodDomain) != -1) {
          SERVER = this.prodHost;
        }
        else if (document.domain.indexOf(this.devDomain) != -1) {
          SERVER = this.devHost;
        }
        else if (document.domain.indexOf(this.qaDomain) != -1) {
          SERVER = this.qaHost;
        }
      }
    }
    this.set('SERVER', this.httpProtocol + SERVER);
  }

  get(property: string, useSessionStorage = false) {
    if(!useSessionStorage){
      return this.config[property];
    }
    else {
      return window.sessionStorage.getItem(property);
    }

  }

  public set(property: string, value: any, useSessionStorage = false) {
    if(!useSessionStorage){
      this.config[property] = value ;
    }
    else {
      window.sessionStorage.setItem(property, value);
    }

  }

  getHeaderColor(){
    let curr_server = this.get('SERVER');
    switch(curr_server){
      case this.httpProtocol + this.prodHost:
        return "#4CAF50";
      case this.httpProtocol + this.qaHost:
        return "purple";
      case this.httpProtocol + this.testHost:
        return "tomato";
      case this.httpProtocol + this.devHost:
        return "#ffd600";
      case this.httpProtocol + this.localHost:
        return "#00B0FF";
      default:
        return "#ffd600"
    }
  }
}
