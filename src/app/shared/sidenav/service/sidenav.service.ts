import {Injectable} from "@angular/core";

/**
 * This service is used to control the opening of the right and left menus from outside the applications
 * (to make everything simplier, the menus are contained inside the application and controlled both from
 * outside and inside the app itself)
 */
@Injectable()
export class SideNavService {

  leftMenu: boolean;
  leftMenuEnabled = true;
  rightMenu:boolean;
  rightMenuEnabled = true;
  appMenuEnabled = true;

  constructor() {}

}
