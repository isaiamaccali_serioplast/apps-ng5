import {Injectable} from "@angular/core";
import {LoadingComponent} from "./loading.component";

@Injectable()
export class LoadingService {

  openedLoading = 0;
  component: LoadingComponent;

  constructor() {
  }

  register(loadingComponent: LoadingComponent) {
    this.component = loadingComponent;
  }

  show() {
    this.openedLoading++;
    if (this.openedLoading == 1) {
      this.component.show();
    }
  }

  hide() {
    if(this.openedLoading>0)
      this.openedLoading--;
    if (this.openedLoading == 0) {
      this.component.hide();
    }
  }

  forceHide(){
    this.openedLoading = 0;
    this.component.hide();
  }
}
