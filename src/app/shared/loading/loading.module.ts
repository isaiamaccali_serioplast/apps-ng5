import {NgModule} from "@angular/core";
import {LoadingComponent} from "./loading.component";
import {Angular2MaterialModule} from "../material/angular2-material.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [Angular2MaterialModule, CommonModule, FormsModule],       // module dependencies
  declarations: [LoadingComponent],   // components and directives
  exports: [LoadingComponent],
  providers: []                    // services
})
export class LoadingModule { }
