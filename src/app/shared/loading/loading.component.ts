import {Component} from "@angular/core";
import {LoadingService} from "./loading.service";

@Component({
  selector: 'sp-loading',
  templateUrl: 'loading.component.html',
  styleUrls: ['loading.component.css']
})
export class LoadingComponent {

  public isVisible = false;

  constructor(private loadingService:LoadingService){
    this.loadingService.register(this);
  }

  show(){
    this.isVisible = true;
  }

  hide(){
    this.isVisible = false;
  }
}
