/* Imports and re-exports all common modules, to prevent pointless repetitions*/
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {Angular2MaterialModule} from "./material/angular2-material.module";
import {RouterModule} from "@angular/router";
import {I18NModule} from "./i18n/i18n.module";
import {CommonModule} from "@angular/common";
import {SettingsModule} from "./settings/component/settings.module";
import {LanguageSelectorModule} from "./i18n/components/language-selector/language-selector.module";
import {UsersModule} from "./components/user/user.module";
import {DialogModule} from "./material/dialog/dialog.module";
import {FrequencyModule} from "./pipes/frequency.module";
import {ConfirmModule} from "./material/message/message.module";
import {ChipAutocompleteModule} from "./material/chip-autocomplete/chip-autocomplete.module";
import {DragDropModule} from "./material/drag-drop/drag-drop.module";
import {PkTransformerModule} from "./components/pk-transformer/pk-transformer.module";
import {CamelCaseModule} from "./pipes/camelcase.module";
import {TableWithPagesModule} from "./material/table-with-pages/table-with-pages.module";
import {SPAutocompleteModule} from "./material/autocomplete/autocomplete.module";
import {PropertyModule} from "./components/property/property.module";
import {SanitizeHTMLModule} from "./pipes/sanitizehtml.module";
import {AccordionModule} from "./material/accordion/accordion.module";
import {EndCutterModule} from "./pipes/end-cutter.module";
import {ExpThousandsSeparatorModule} from "./pipes/thousands-separator.module";
import {SaveUndoButtonsModule} from "./components/save-undo-buttons/save-undo-buttons.module";
import {StarsModule} from "./pipes/stars.module";
import {MaterialSelectorModule} from "./components/material-selector/material-selector.module";
import {DialogConfirmModule} from "./components/dialog-confirm/dialog-confirm.module";
import {StepperModule} from "./material/stepper/stepper.module";
import {EventTimelineModule} from "./components/event-timeline/event-timeline.module";
import {FilenameModule} from "./pipes/filename.module";
import {GenericCommentsModule} from "./components/generic-comments/generic-comments.module";
import {AttachmentsModule} from "./components/attachments/attachments.module";
import {SapDateModule} from "./pipes/sap-date.module";
import {SapTimeModule} from "./pipes/sap-time.module";
import {TruncateModule} from "./pipes/truncate.module";
import {MarkdownTextareaModule} from "./components/markdown-textarea/markdown-textarea.module";
import {AppIconModule} from "./components/app-icon/app-icon.module";
import {PeriodTimelineModule} from "./components/period-timeline/period-timeline.module";
import {TodoListModule} from "./components/todo-list/todo-list.module";
import {SAPIconModule} from "./components/sap-icon/sap-icon.module";
import {ReportsModule} from "./components/reports/reports.module";
import {LoginModule} from "./login/service/login.module";
import {OrganizationSearchModule} from "./components/organization-search/organization-search.module";
import {UserSearchModule} from "./components/user-search/user-search.module";
import {SearchResetButtonsModule} from "./components/search-reset-buttons/search-reset-buttons.module";
// import {NguiMapModule} from "@ngui/map";
// import {MarkdownModule} from "ngx-md";
import {WorkcentersMapModule} from "./components/workcenters-map/workcenters-map.module";
import {WBSSearchModule} from "./components/wbs-search/wbs-search.module";
import {RoutingDisplayModule} from "./components/routing-display/routing-display.module";
import {BomDisplayModule} from "./components/bom-display/bom-display.module";
import {WorkorderViewSharedModule} from "./components/workorder-view-shared/workorder-view-shared.module";
import {FixedFieldModule} from "./components/fixed-field/fixed-field.module";
import {MarkdownPipeModule} from "./pipes/markdown.module";
import {ImageUploaderModule} from "./material/image-uploader/image-uploader.module";
import {FileUploaderModule} from "./material/file-uploader/file-uploader.module";
import {UserSitesModule} from "./components/user-sites/user-sites.module";
import {GoogleMapsModule} from "./components/googlemaps/google-maps.module";

let modules = [
  AccordionModule,
  Angular2MaterialModule,
  AppIconModule,
  AttachmentsModule,
  BomDisplayModule,
  CamelCaseModule,
  ChipAutocompleteModule,
  CommonModule,
  ConfirmModule,
  DialogConfirmModule,
  DialogModule,
  EndCutterModule,
  EventTimelineModule,
  ExpThousandsSeparatorModule,
  FilenameModule,
  FixedFieldModule,
  FormsModule,
  FrequencyModule,
  GenericCommentsModule,
  GoogleMapsModule,
  I18NModule,
  FileUploaderModule,
  // IdleModule,
  LanguageSelectorModule,
  LoginModule,
  // MarkdownModule,
  MarkdownTextareaModule,
  MarkdownPipeModule,
  MaterialSelectorModule,
  // NguiMapModule,
  OrganizationSearchModule,
  PeriodTimelineModule,
  PkTransformerModule,
  PropertyModule,
  ReportsModule,
  RouterModule,
  RoutingDisplayModule,
  SanitizeHTMLModule,
  SapDateModule,
  SAPIconModule,
  SapTimeModule,
  SaveUndoButtonsModule,
  SearchResetButtonsModule,
  SettingsModule,
  SPAutocompleteModule,
  StarsModule,
  StepperModule,
  TableWithPagesModule,
  TodoListModule,
  TruncateModule,
  UsersModule,
  UserSearchModule,
  UserSitesModule,
  // UserSitesModule,
  // UserSitesWithSAPModule,
  WBSSearchModule,
  WorkorderViewSharedModule,
  WorkcentersMapModule
];

@NgModule({
  imports: modules,
  exports: modules,
  providers:[]
})
export class SharedModule {
}
