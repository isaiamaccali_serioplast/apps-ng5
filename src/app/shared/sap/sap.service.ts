import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {randomString} from "../utils";
import {ConfigurationService} from "../serionet/service/configuration.service";
import {CookieService} from "ngx-cookie";

declare let sap: any;

@Injectable()
export class SAPService {

  private initialized = false;
  private BASE_URL = "";
  user: string;
  password: string;

  constructor(private confService: ConfigurationService, private cookieService: CookieService) {
    this.BASE_URL = "/sap/opu/odata/sap/";
    this.initialize();
  }

  initialize(callback?: any) {
    this.initialized = true;
    if (callback)
      callback();
  }

  getItems(serviceName: string, entityName: string, headers: any, requestParams?: any, useBatch?: boolean) {
    let params = ["/" + entityName];
    return this.communicateWithSap(serviceName, 'read', params, headers, requestParams, useBatch);
  }

  getItem(serviceName: string, entityName: string, headers: any, id: string, requestParams?: any, useBatch?: boolean) {
    let params = ["/" + entityName + ("(" + id + ")")];
    return this.communicateWithSap(serviceName, 'read', params, headers, requestParams, useBatch);
  }

  createItem(serviceName: string, entityName: string, headers: any, item: any, requestParams?: any, useBatch?: boolean) {
    let params = ["/" + entityName, item];
    return this.communicateWithSap(serviceName, 'create', params, headers, requestParams, useBatch);
  }

  updateItem(serviceName: string, entityName: string, headers: any, id: string, item: any, requestParams?: any, useBatch?: boolean) {
    let params = ["/" + entityName + ("(" + id + ")"), item];
    return this.communicateWithSap(serviceName, 'update', params, headers, requestParams, useBatch);
  }

  deleteItem(serviceName: string, entityName: string, headers: any, id: string, requestParams?: any, useBatch?: boolean) {
    let params = ["/" + entityName + ("(" + id + ")")];
    return this.communicateWithSap(serviceName, 'remove', params, headers, requestParams, useBatch);
  }

  static parseSapResponse(response, oData?) {
    let statusCode = parseInt(response['statusCode'], 10);
    let resp = {};
    resp['status'] = statusCode;
    if (statusCode >= 200 && statusCode < 300) {
      resp['response'] = oData;
      resp['serverResponse'] = response;
    }
    else {
      try {
        resp['message'] = JSON.parse(response.responseText).error.message.value;
      }
      catch (e) {
        resp['message'] = response.responseText
      }
    }
    return resp;
  }

  communicateWithSap(serviceName: string, method: string, params: any[], headers?: any, requestParams?: any, useBatch?: boolean) {
    this.removeSAPCookieLogged();
    this.user = this.confService.get("SAP_USER", true);
    this.password = this.confService.get("SAP_PASSWORD", true);

    return Observable.create((observer) => {

      let _saveData = (serviceName: string, params: any[], headers: any) => {

        let timeout = setTimeout(()=> {
          observer.next({status: 9000, context: 'SAP', message: 'Request timeout'});
          observer.complete();
        }, 60000);

        // let doLogout = (callback) => {
        //   this.http
        //     .get("/sap/public/bc/icf/logoff", null)
        //     .timeoutWith(30000, Observable.defer(() => SerionetService.handleTimeout()))
        //     .catch(SerionetService.handleError)
        //     .toPromise().then(() => {
        //     //in order to prevent conflicts
        //     this.cookieService.remove("SAP_SESSIONID_SPD_100");
        //     this.cookieService.remove("SAP_SESSIONID_SPT_100");
        //     this.cookieService.remove("SAP_SESSIONID_SPP_100");
        //     this.cookieService.remove("MYSAPSSO2");
        //     callback();
        //   })
        // };

        // let doFakeLogin = (callback) => {
        //   let oModelDummy = new sap.ui.model.odata.v2.ODataModel(
        //     this.BASE_URL + serviceName + "/",
        //     {
        //       user: 'dummy',
        //       password: 'dummy'
        //     }
        //   );
        //
        //   oModelDummy.attachMetadataFailed(null, () => {
        //     oModelDummy.destroy();
        //     if (oModelDummy)
        //       oModelDummy = null;
        //     callback();
        //   });
        //
        //   oModelDummy.attachMetadataLoaded(null, () => {
        //     oModelDummy.destroy();
        //     if (oModelDummy)
        //       oModelDummy = null;
        //     callback();
        //   });
        // };

        let doActualCall = () => {

          this.cookieService.remove("SAP_SESSIONID_SPD_100");
          this.cookieService.remove("SAP_SESSIONID_SPT_100");
          this.cookieService.remove("SAP_SESSIONID_SPP_100");
          this.cookieService.remove("MYSAPSSO2");

          let doCall = ()=>{
            let oModel = new sap.ui.model.odata.v2.ODataModel(
              this.BASE_URL + serviceName + "/",
              {
                user: this.user,
                password: this.password,
                headers: headers
              }
            );
            if (useBatch)
              oModel.setUseBatch(true);
            else
              oModel.setUseBatch(false);

            oModel.attachMetadataFailed(null, () => {

                oModel.destroy();
                let resp = {
                  status: 2000,
                  message: "Error in retreiving model metadata"
                };
                observer.next(resp);
                observer.complete();
              },
              null);

            oModel.attachMetadataLoaded(null, () => {
              let callbacks = {
                success: (oData, oResponse) => {
                  if (timeout)
                    clearTimeout(timeout);
                  observer.next(SAPService.parseSapResponse(oResponse, oData));
                  observer.complete();
                },
                error: (oResponse) => {
                  if (timeout)
                    clearTimeout(timeout);
                  observer.next(SAPService.parseSapResponse(oResponse));
                  observer.complete();
                  if (oModel)
                    oModel = null;
                }
              };
              if (!requestParams)
                requestParams = {};
              if (!requestParams.urlParameters)
                requestParams.urlParameters = {};
              requestParams.urlParameters[randomString(15)] = randomString(15);

              params.push(Object.assign(requestParams, callbacks));

              oModel[method].apply(oModel, params);
            }, null);
          };

          // let attempts = 20;
          // let interval = setInterval(()=>{
          //   attempts--;
          //   console.log(attempts);
          //   if(!this.cookieService.get('MYSAPSSO2')||attempts==0){
          //     clearInterval(interval);
          doCall();
          //   }
          // },100);
        };

        // doLogout(() => {
        //   doFakeLogin(() => {
        doActualCall();
          // })
        // })
      };

      if (this.initialized) {
        return _saveData(serviceName, params, headers);
      }
      else {
        this.initialize(() => {
          return _saveData(serviceName, params, headers);
        })
      }
    });
  }

  ready() {
    return Observable.create((observer) => {

      if (this.initialized) {
        observer.next(true);
        observer.complete();
      }
      else {
        this.initialize(()=> {
          observer.next(true);
          observer.complete();
        })
      }
    });
  }

  removeSAPCookieLogged() {
    this.cookieService.remove("SAP_SESSIONID_SPD_100");
    this.cookieService.remove("SAP_SESSIONID_SPT_100");
    this.cookieService.remove("SAP_SESSIONID_SPP_100");
  }

}
