"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ng_module_1 = require("@angular/core/src/metadata/ng_module");
var settings_component_1 = require("./settings.component");
var angular2_material_module_1 = require("../../material/angular2-material.module");
var dialog_module_1 = require("../../material/dialog/dialog.module");
var SettingsModule = (function () {
    function SettingsModule() {
    }
    return SettingsModule;
}());
SettingsModule = __decorate([
    ng_module_1.NgModule({
        imports: [angular2_material_module_1.Angular2MaterialModule, dialog_module_1.DialogModule],
        declarations: [settings_component_1.SettingsComponent],
        exports: [settings_component_1.SettingsComponent],
        providers: [] // services
    })
], SettingsModule);
exports.SettingsModule = SettingsModule;
