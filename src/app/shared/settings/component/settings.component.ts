import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'sp-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.css']
})
export class SettingsComponent implements OnInit {

  opened:boolean = false;

  constructor() {

  }

  ngOnInit() {

  }


  open(){
    this.opened = true;
  }

  close(){
    this.opened = false;
  }

}
