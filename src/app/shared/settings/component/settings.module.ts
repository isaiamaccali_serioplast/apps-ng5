import {NgModule} from "@angular/core";
import {SettingsComponent} from "./settings.component";
import {Angular2MaterialModule} from "../../material/angular2-material.module";
import {DialogModule} from "../../material/dialog/dialog.module";

@NgModule({
  imports: [Angular2MaterialModule, DialogModule],       // module dependencies
  declarations: [SettingsComponent],   // components and directives
  exports:[SettingsComponent],
  providers: []                    // services
})
export class SettingsModule { }

