import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from "./index/index.component";
import {LoginComponent} from "./shared/login/component/login/login.component";
import {AuthGuard} from "./auth-guard";
import {ModuleWithProviders} from "@angular/core";

let routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'index',
    component: IndexComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'assets-workcenters',
    loadChildren: 'app/apps/assets-workcenters/assets-workcenters.module#AssetsWorkcentersModule',
    canActivate:[AuthGuard]
  },
  {
    path: 'deck',
    loadChildren: 'app/apps/deck/deck.module#DeckModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'guest',
    loadChildren: 'app/apps/guest/guest.module#GuestModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'hr-dashboard',
    loadChildren: 'app/apps/hr-dashboard/hr-dashboard.module#HRDashboardModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'hr-management',
    loadChildren: 'app/apps/hr-management/hr-management.module#HRManagementModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'hr-recruitment',
    loadChildren: 'app/apps/hr-recruitment/hr-recruitment.module#HRRecruitmentModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'hr-trainings',
    loadChildren: 'app/apps/hr-trainings/hr-trainings.module#HRTrainingsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'materials',
    loadChildren: 'app/apps/materials/materials.module#MaterialsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'mes',
    loadChildren: 'app/apps/mes/mes.module#MesModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'organizations',
    loadChildren: 'app/apps/organizations/organizations.module#OrganizationsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'post-it',
    loadChildren: 'app/apps/post-it/post-it.module#PostItModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'procurement-pricelist',
    loadChildren: 'app/apps/procurement-pricelist/procurement-pricelist.module#ProcurementPriceListModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'production-planning',
    loadChildren: 'app/apps/production-planning/production-planning.module#ProductionPlanningModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'project-management',
    loadChildren: 'app/apps/project-management/project-management.module#ProjectManagementModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'quality-stock',
    loadChildren: 'app/apps/quality-stock/quality-stock.module#QualityStockModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'rd-dashboard',
    loadChildren: 'app/apps/rd-dashboard/rd-dashboard.module#RDDashboardModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'rfid-trays',
    loadChildren: 'app/apps/rfid-trays/rfid-trays.module#RfidTraysModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'routings',
    loadChildren: 'app/apps/routings/routings.module#RoutingsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'sales-dashboard',
    loadChildren: 'app/apps/sales-dashboard/sales-dashboard.module#SalesDashboardModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'sales-pricelist',
    loadChildren: 'app/apps/sales-pricelist/sales-pricelist.module#SalesPricelistModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'seriolab',
    loadChildren: 'app/apps/seriolab/seriolab.module#SeriolabModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'sites',
    loadChildren: 'app/apps/sites/sites.module#SitesModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'spc',
    loadChildren: 'app/apps/spc/spc.module#SPCModule',
    canActivate: [AuthGuard]
  }


];

export const routing:ModuleWithProviders = RouterModule.forRoot(routes);
