/**
 * Class used to block route access in case the user is not logged
 */

import {Injectable} from "@angular/core";
import {LoginService} from "./shared/login/service/login.service";
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {AppsService} from "./apps/apps.service";
import {DictionariesService} from "./shared/i18n/dictionaries.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private loginService: LoginService, private router: Router, private appsService: AppsService, private dictionariesService: DictionariesService) {

  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return Observable.create((observer) => {

        if (this.loginService.userIsLogged) {

          let appName = state.url.split("/")[1].split("?")[0];
          if (appName && appName !== 'login' && appName !== 'index') {
            this.appsService.ready().subscribe(() => {
              this.appsService.isAppAvailableForUser(appName).subscribe((isAvailable) => {
                if (isAvailable==true) {
                  this.dictionariesService.getDictionary(appName).subscribe(() => {
                    observer.next(true);
                    observer.complete();
                  },(resp)=>{
                  })
                }
                else {
                  this.router.navigate(["/index"]);
                  observer.next(false);
                  observer.complete();
                }
              });
            })
          }
          else {
            observer.next(true);
            observer.complete();
          }
        }
        else {
          this.router.navigate(['/login']);
          observer.next(false);
          observer.complete();
        }
      }
    );
  }
}
