import {NgModule} from "@angular/core";
import {IndexComponent} from "./index.component";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  imports: [SharedModule],       // module dependencies
  declarations: [ IndexComponent],   // components and directives
  providers: []                    // services
})
export class IndexModule { }
