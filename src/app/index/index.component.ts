/**
 * Index is used to show the available apps
 */
import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {SideNavService} from "../shared/sidenav/service/sidenav.service";
import {App, AppsService} from "../apps/apps.service";
import {MessageService} from "../shared/material/message/message.service";
import {TranslationService} from "../shared/i18n/translation.service";
import {parseSerionetRespToData} from "../shared/utils";
import {TranslatableComponent} from "../shared/components/base-component/translatable.component";

@Component({
  selector: 'sp-index',
  templateUrl: 'index.component.html',
  styleUrls: ['index.component.css']
})
export class IndexComponent extends TranslatableComponent implements OnInit {

  apps: App[];

  constructor(private titleService: Title, private appsService: AppsService,
              private sidenavService:SideNavService, private messageService:MessageService, protected translationService:TranslationService) {
    super(translationService);
    this.apps = [];
    this.sidenavService.leftMenuEnabled = false;
    this.sidenavService.appMenuEnabled = false;
  }

  ngOnInit() {
    this.titleService.setTitle('Apps - Serioplast');
    this.appsService.getAvailableApps().subscribe((data)=>{
      this.apps = parseSerionetRespToData(data);
      if(this.apps.length==0){
        this.messageService.open({
          error:true,
          title:this.tr("ERROR"),
          message:this.tr("ERROR_IN_RETREIVING_APPS")
        })
      }
    });
  }
}
