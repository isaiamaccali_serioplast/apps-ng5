import {ApplicationRef, NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import {appRouter} from "./apps/apps.routes";

/*
 * Platform and Environment providers/directives/pipes
 */
import { environment } from 'environments/environment';
import {routing} from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { XLargeDirective } from './home/x-large';
import { DevModuleModule } from './+dev-module';

import '../styles/styles.scss';
import '../styles/headings.css';
import {SharedModule} from "./shared/shared.module";
import {LoadingModule} from "./shared/loading/loading.module";
import {SerionetModule} from "./shared/serionet/service/serionet.module";
import {SerioplastCommonModule} from "./apps/_common/common.module";
import {AppsModule} from "./apps/apps.module";
import {IdleModule} from "./shared/idle/idle.module";
import {IndexModule} from "./index/index.module";
import {LoggingService} from "./shared/logging/logging.service";
import {LoadingService} from "./shared/loading/loading.service";
import {ConfigurationService} from "./shared/serionet/service/configuration.service";
import {AppsService} from 'app/apps/apps.service';
import {SerionetService} from "./shared/serionet/service/serionet.service";
import {SideNavService} from "./shared/sidenav/service/sidenav.service";
import {TranslationService} from 'app/shared/i18n/translation.service';
import {DictionariesService} from 'app/shared/i18n/dictionaries.service';
import {MessageService} from "./shared/material/message/message.service";
import {SessionStorageService} from "./shared/session-storage/session-storage.service";
import {LoginService} from "./shared/login/service/login.service";
import {AuthGuard} from "./auth-guard";
import {IdleService} from "./shared/idle/idle.service";
// import {SimpleExpiry} from "ng2-idle";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {LoginModule} from "./shared/login/component/login/login.module";
import {registerLocaleData} from "@angular/common";
// import {BrowserDomAdapter} from "@angular/platform-browser/src/browser/browser_adapter";
import localeIt from '@angular/common/locales/it';
import localeEn from '@angular/common/locales/en';
import localeSl from '@angular/common/locales/sl';
// import {NguiMapModule} from "@ngui/map";
import {CookieModule, CookieService} from "ngx-cookie";

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent
  ],
  imports: [ // import Angular's modules
    SharedModule,
    LoadingModule,
    BrowserModule,
    LoginModule,
    routing,
    SerionetModule,
    SerioplastCommonModule,
    AppsModule,
    IndexModule,
    IdleModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    // NguiMapModule.forRoot({apiUrl: 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBiFbflThaqa_Ia6-AMhL2XEs9D_Mr_TaM'}),
    CookieModule.forRoot()
    // appRouter
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    APP_PROVIDERS,
    LoggingService,
    LoadingService,
    ConfigurationService,
    AppsService,
    SerionetService,
    SideNavService,
    Title,
    TranslationService,
    DictionariesService,
    MessageService,
    // BrowserDomAdapter,
    SessionStorageService,
    LoginService,
    AuthGuard,
    // SimpleExpiry,
    IdleService,
    HttpClient
  ]
})
export class AppModule {}

registerLocaleData(localeIt,localeEn);
registerLocaleData(localeSl);
